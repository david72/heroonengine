//#####################################################################################################################################################################################
// Heroon Engine SDK V2
// Copyright (C) 2017 ActifGames, LLC. All rights reserved
// contact@heroonengine.actifgames.com
//
// Programmer: David Pellier
// Revision  : 21/06/1017
//#######################################################################################################################################################################################
class PlayerHealtBars {

	constructor()
	{
		this.path = SceneManager.getFolderPath();		
	}

	createElementForPlayer()
	{	
		// Statut bar for player	
		this.divAttributeBarsPlayer = UI.Window({width: "240px", height: "80px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP, left: 15, top: 15, radius: 1, background: "transparent", zIndex: 2});	
		UI.Image("Statut_player", {URLImage: this.path+"skins/Statut_barre_player.png", width: "240px", height: "80px"}, this.divAttributeBarsPlayer);	
		this.divJaugeBarsPlayerHealth = UI.Window({background: "red", width: "153px", height: "20px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP, left: 98, top: 55, radius: 10, zIndex: 1}, null, this.divAttributeBarsPlayer);	
		this.divJaugeBarsPlayerMagic = UI.Window({background: "blue", width: "153px", height: "20px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP, left: 79, top: 75, radius: 10, zIndex: 1}, null, this.divAttributeBarsPlayer);	
		
		UI.Image("Avatar_Player", {URLImage: null, width: "75px", height: "75px"}, this.divAttributeBarsPlayer);
	}

	createElementForEnemy()
	{		
		// Statut bar for enemy		
		this.divAttributeBarsEnemy = UI.Window({width: "240px", height: "80px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP, left: 15, top: 137, radius: 1, background: "transparent", zIndex: 2});	
		UI.Image("Statut_enemy", {URLImage: this.path+"skins/Statut_barre_ennemy.png", width: "240px", height: "80px"}, this.divAttributeBarsEnemy);	
		UI.Image("crane", {URLImage: this.path+"skins/Crane.png", width: "23px", height: "21px", left: 67.5, top: 5}, this.divAttributeBarsEnemy);
		this.divJaugeBarsEnemyHealth = UI.Window({background: "red", width: "153px", height: "20px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP, left: 98, top: 175, radius: 10, zIndex: 1}, null, this.divAttributeBarsEnemy);	
		this.divJaugeBarsEnemyMagic = UI.Window({background: "blue", width: "153px", height: "20px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP, left: 79, top: 195, radius: 10, zIndex: 1}, null, this.divAttributeBarsEnemy);	
	}

	visibility(typeActor, isVisible)
	{
		if(typeActor == "enemy" || typeActor == null) {
			if(isVisible == false) {
				this.divAttributeBarsEnemy.isVisible = this.divJaugeBarsEnemyHealth.isVisible =  this.divJaugeBarsEnemyMagic.isVisible = false;
			} else {
				this.divAttributeBarsEnemy.isVisible = this.divJaugeBarsEnemyHealth.isVisible =  this.divJaugeBarsEnemyMagic.isVisible = true;
			}
		}
		if(typeActor == "player" || typeActor == null) {
			if(isVisible == false) {
				this.divAttributeBarsPlayer.isVisible = this.divJaugeBarsPlayerHealth.isVisible =  this.divJaugeBarsPlayerMagic.isVisible = false;				
			} else {
				this.divAttributeBarsPlayer.isVisible = this.divJaugeBarsPlayerHealth.isVisible =  this.divJaugeBarsPlayerMagic.isVisible = true;
			}
		}
	}

}