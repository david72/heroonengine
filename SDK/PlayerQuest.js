//#####################################################################################################################################################################################
// Heroon Engine SDK V2
// Copyright (C) 2017 ActifGames, LLC. All rights reserved
// contact@heroonengine.actifgames.com
//
// Programmer: David Pellier
// Revision  : 21/06/1017
//#######################################################################################################################################################################################
class PlayerQuest {

	constructor()
	{
		this.path = SceneManager.getFolderPath();
	}

	createElement()
	{		
		// Event for drag
		let start = (coordinates) => {
			UI.dragStart(coordinates, this.divQuest);
		};
		
		let stop = function() {
			UI.dragStop();
		};
		
		let drag = (coordinates) => {
			UI.dragMove(coordinates, this.divQuest);
			UI.isDraggable(game.UI,
				(coordinates) => {
					UI.dragMove(coordinates, this.divQuest);
				});
		};	
		
		// Fenetre draggable
		this.divQuest = UI.Window({width: "326px", height: "512px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER, radius: 1},
		{title: menuGame.lang.quest, align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER, height: "50px", top: 12, radius: 1, size: 18},
		null, null, start, stop, drag);			
		
		// Image de fond de l'inventaire
		UI.Image("Quest", {URLImage: this.path+"skins/Window.png", width: "326px", height: "512px"}, this.divQuest);
		this.divQuest.isVisible = false;		
	}

	switchVisibility() {
		if(this.divQuest.isVisible == true) {
			this.divQuest.isVisible = false;
		} else {
			this.divQuest.isVisible = true;
		}
	}

}