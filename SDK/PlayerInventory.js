//#####################################################################################################################################################################################
// Heroon Engine SDK V2
// Copyright (C) 2017 ActifGames, LLC. All rights reserved
// contact@heroonengine.actifgames.com
//
// Programmer: David Pellier
// Revision  : 22/06/1017
//#######################################################################################################################################################################################
class PlayerInventory {

	constructor(actor, scene)
	{
		this.path = SceneManager.getFolderPath();
		this.actor = actor;
		this.scene = scene;		
		this.spaceLeft = 15;
		this.spaceTop = 350;
		this.nbrSlot = 32;
		this.divEmptySlot = [];
	}

	createElement()
	{			
		// Event for drag
		let start = (coordinates) => {
			UI.dragStart(coordinates, this.divInventory);
		};
		
		let stop = function() {
			UI.dragStop();
		};
		
		let drag = (coordinates) => {
			UI.dragMove(coordinates, this.divInventory);
			UI.isDraggable(game.UI,
				(coordinates) => {
					UI.dragMove(coordinates, this.divInventory);
				});
		};	
		
		// Fenetre draggable
		this.divInventory = UI.Window({width: "325px", height: "552px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER, left: -6, top: 100, radius: 1},
		{title: menuGame.lang.inventory, align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER, height: "50px", top: 12, radius: 1, size: 18},
		null, null, start, stop, drag);			
		
		// Image de fond de l'inventaire
		UI.Image("Inventory", {URLImage: this.path+"skins/Inventary.png", width: "325px", height: "552px"}, this.divInventory);		
		this.divInventory.isVisible = false;
		
		// Slot player
		this.slotPlayer();
		
		// Slot inventory
		this.slotInventory();		
	}
	
	slotPlayer() {		
		
		//this.divSlotHead.ondragend = () => { this.addToBone("Head", this); };		
		this.divSlotHead = UI.Button("Head", "", {width: "34px", height: "34px", left: "15px", top: "87px", background: "transparent", radius: 1}, null, this.divInventory, null, null, null, null, null);
		
		//this.divSlotRing.ondragend = () => { this.addToBone("Finger", this); };
		this.divSlotRing = UI.Button("Finger", "", {width: "34px", height: "34px", left: "15px", top: "137px", background: "transparent", radius: 1}, null, this.divInventory, null, null, null, null, null);
		
		//this.divSlotShield.ondragend = () => { this.addToBone("R_Forearm", this); };
		this.divSlotShield = UI.Button("R_Forearm", "", {width: "34px", height: "34px", left: "15px", top: "187px", background: "transparent", radius: 1}, null, this.divInventory, null, null, null, null, null);
		
		//this.divSlotSword.ondragend = () => { this.addToBone("L_Hand", this); };
		this.divSlotSword = UI.Button("L_Hand", "", {width: "34px", height: "34px", left: "15px", top: "237px", background: "transparent", radius: 1}, null, this.divInventory, null, null, null, null, null);
		
		//this.divSlotHand.ondragend = () => { this.addToBone("Hand", this); };
		this.divSlotHand = UI.Button("Hand", "", {width: "34px", height: "34px", left: "15px", top: "291px", background: "transparent", radius: 1}, null, this.divInventory, null, null, null, null, null);
				
		//this.divSlotCollar.ondragend = () => { this.addToBone("Neck", this); };
		this.divSlotCollar = UI.Button("Neck", "", {width: "34px", height: "34px", left: "276.8px", top: "87px", background: "transparent", radius: 1}, null, this.divInventory, null, null, null, null, null);
		
		//this.divSlotBody.ondragend = () => { this.addToBone("Chest", this); };
		this.divSlotBody = UI.Button("Chest", "", {width: "34px", height: "34px", left: "276.8px", top: "137px", background: "transparent", radius: 1}, null, this.divInventory, null, null, null, null, null);
		
		//this.divSlotBelt.ondragend = () => { this.addToBone("Finger", this); };
		this.divSlotBelt = UI.Button("Finger", "", {width: "34px", height: "34px", left: "276.8px", top: "187px", background: "transparent", radius: 1}, null, this.divInventory, null, null, null, null, null);
		
		//this.divSlotForearm.ondragend = () => { this.addToBone("Forearm", this); };
		this.divSlotForearm = UI.Button("Forearm", "", {width: "34px", height: "34px", left: "276.8px", top: "237px", background: "transparent", radius: 1}, null, this.divInventory, null, null, null, null, null);
		
		//this.divSlotShoe.ondragend = () => { this.addToBone("Foot", this); };
		this.divSlotShoe = UI.Button("Foot", "", {width: "34px", height: "34px", left: "276.8px", top: "291px", background: "transparent", radius: 1}, null, this.divInventory, null, null, null, null, null);
				
		/*	
		let elements = document.querySelectorAll('.draggable');        
        for(let i = 0; i < elements.length; i++) {
			ActionBar.applyDragEvents(elements[i]); // Paramètres élément déplaçables
        }       
        let droppers = document.querySelectorAll('.dropper');        
        for(let i = 0; i < droppers.length; i++) {			
			ActionBar.applyDropEvents(droppers[i]); // Evénements aux zones de drop
        }	
		*/
	}
	
	slotInventory() {		
		let line = 7;
		for(let index = 0; index < this.nbrSlot; index++)
		{
			//this.divEmptySlot[index].ondragend = () => { this.removeToBone(this); };			
			this.divEmptySlot[index] = UI.Button("node_"+index, "", {width: "34px", height: "34px", left: this.spaceLeft+"px", top: this.spaceTop+"px", background: "transparent", radius: 1}, null, this.divInventory, null, null, null, null, null);
			
			this.spaceLeft += 37.4;
			if(index == line) {
				this.spaceTop += 37;
				this.spaceLeft = 15;
				line += 8;
			}
		}
	}
	
	addToBone(boneName, obj)
	{
		let boneIndex = this.actor.skeletons.getBoneIndexByName(boneName);
		let mesh = this.scene.getMeshByName(obj.title);
		let meshToAttach = mesh.clone(mesh.name+"_"+this.actor.name);
		meshToAttach.attachToBone(this.actor.skeletons.bones[boneIndex], this.actor);
	}
	
	removeToBone(obj)
	{
		let meshToDetach = this.scene.getMeshByName(obj.title) || null;
		if(meshToDetach) {
			meshToDetach.detachFromBone();
			meshToDetach.dispose();
		}
	}
	
	switchVisibility() {
		if(this.divInventory.isVisible == true) {
			this.divInventory.isVisible = false;
		} else {
			this.divInventory.isVisible = true;
		}
	}
}