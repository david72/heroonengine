//#####################################################################################################################################################################################
// Heroon Engine SDK
// Copyright (C) 2017 ActifGames, LLC. All rights reserved
// contact@heroonengine.actifgames.com
//
// Programmer: David Pellier
// Revision  : 21/06/1017
//#######################################################################################################################################################################################
class PlayerMap {

	constructor(cameraMiniMap, scene, engine)
	{
		this.path = SceneManager.getFolderPath();
		this.camera = scene.activeCameras[1] || cameraMiniMap; // Camera for minimap (FreeCamera)			
		this.scene = scene; // The scene game
		this.engine = engine; // The engine 3D		
		
		//Position et dimention du viewport de la minimap
		const viewport = {X: 0.865, Y: 0.73, Width: 0.120, Height: 0.249};
		// Viewport of the camera minimap (FreeCamera)		
		this.camera.viewport = new BABYLON.Viewport(viewport.X, viewport.Y, viewport.Width, viewport.Height);
	}
	
	createBorderMiniMapRectangle()
	{		
		// Minimap rectangle			
		this.divBorder = UI.Window({width: "250px", height: "250px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP, left: -6, top: 15, radius: 1, background: "transparent", zIndex: 1});
		UI.Image("Inventory", {URLImage: this.path+"skins/Mini-map.png", width: "250px", height: "250px", zIndex: 2}, this.divBorder);		
		
		UI.Button("ZoomOut", "", {width: "34px", height: "30px", left: "220px", top: "173px", background: "transparent", radius: 17, zIndex: 3}, null, this.divBorder, null, null, () => {
			if(this.camera.position.y < 25) {
				this.camera.position.y += 1;
			}
		});
		UI.Button("ZoomIn", "", {width: "34px", height: "30px", left: "220px", top: "205px", background: "transparent", radius: 17, zIndex: 3}, null, this.divBorder, null, null, () => {
			if(this.camera.position.y > 5) { 
				this.camera.position.y -= 1;
			}
		});
		
		// Resise viewport if resize browser
		window.onresize = () => {
			let width = this.engine.getRenderWidth(), height = this.engine.getRenderHeight();
			const staticValue = {Width: 220, Height: 222, Right: 36, Top: 29};
			this.scene.registerBeforeRender(() => {								
				this.camera.viewport = new BABYLON.Viewport((1 - (staticValue.Width + staticValue.Right) / width), (1 - (staticValue.Height + staticValue.Top) / height), (staticValue.Width / width), (staticValue.Height / height));
			});	
		};
	}
	
	createBorderMiniMapRound()
	{	
		// Minimap round		
		this.divBorder = UI.Window({width: "250px", height: "250px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP, left: -15, top: 15, background: "transparent"}, null, null, null);
		UI.Image("Inventory", {URLImage: this.path+"skins/Mini-map-round.png", width: "250px", height: "250px"}, this.divBorder);		
		UI.Button("ZoomOut", "", {width: "34px", height: "30px", left: "220px", top: "90px", background: "transparent", radius: 17}, null, this.divBorder, null, null, () => {
			if(this.camera.position.y < 25) {
				this.camera.position.y += 1;
			}	
		});
		UI.Button("ZoomIn", "", {width: "34px", height: "30px", left: "220px", top: "130px", background: "transparent", radius: 17}, null, this.divBorder, null, null, () => {
			if(this.camera.position.y > 5) {
				this.camera.position.y -= 1;
			}	
		});
			
		// Mask for the viewport round		
		let layer = new BABYLON.Layer("RoundMask", ""+this.path+"skins/roundmask.png", this.scene, true);	
		layer.layerMask = 2;
		layer.alphaTest = true;	
		layer.onBeforeRender = () => {
			if(this.scene.activeCameras[1] == this.camera){
				this.engine.setColorWrite(false);
				this.engine.setDepthBuffer(true);
			}
		};
		layer.onAfterRender = () => {
			this.engine.setColorWrite(true);			
		};
		// Resise viewport if resize browser
		window.onresize = () => {
			let width = this.engine.getRenderWidth(), height = this.engine.getRenderHeight();
			const staticValue = {Width: 230, Height: 237, Right: 30, Top: 22};	
			this.scene.registerBeforeRender(() => {	
				this.camera.viewport = new BABYLON.Viewport((1 - (staticValue.Width + staticValue.Right) / width), (1 - (staticValue.Height + staticValue.Top) / height), (staticValue.Width / width), (staticValue.Height / height));
			});
		};
	}

	createElement()
	{
		//this.createBorderMiniMapRectangle(); // MiniMap rectangle			
		this.createBorderMiniMapRound(); // MiniMap round (By Default)
	}

}