//#####################################################################################################################################################################################
// Heroon Engine SDK V2
// Copyright (C) 2017 ActifGames, LLC. All rights reserved
// contact@heroonengine.actifgames.com
//
// Programmer: David Pellier
// Revision  : 22/06/1017
//#######################################################################################################################################################################################
class GameMenu {

	constructor()
	{
		this.path = SceneManager.getFolderPath();	
		
		this.lang = null;
		this.getLang =  GameMenu.getCookie("HEGame-Lang") || "French";	
		$.ajaxSetup({ async: false});	
		$.getJSON(this.path+'Lang/'+this.getLang+'.lng.json', (data) => { this.lang = data; });
		$.ajaxSetup({ async: true});	
	}
	
	static getCookie(name) {
		if(document.cookie.length == 0) return null;
		let value = "; " + document.cookie;
		let parts = value.split("; " + name + "=");
		if (parts.length == 2) return parts.pop().split(";").shift();
	}
	
	static setCookie(name, value, days) {
		let expires = "";
		if (days) {
			let date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			expires = "; expires="+date.toGMTString();
		}		
		document.cookie = name+"="+value+expires+"; path=/";
	}
}