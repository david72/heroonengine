//#####################################################################################################################################################################################
// Heroon Engine SDK V2
// Copyright (C) 2017 ActifGames, LLC. All rights reserved
// contact@heroonengine.actifgames.com
//
// Programmer: David Pellier
// Revision  : 22/06/1017
//#######################################################################################################################################################################################
class OptionsGame {

	constructor()
	{
		this.path = SceneManager.getFolderPath();		
	}

	createElement()
	{		
		let options = this.getOptions();
		this.options = this.Window({align: GUI.HORIZONTAL_ALIGNMENT_CENTER, verticalAlign: GUI.VERTICAL_ALIGNMENT_CENTER, width: 400, height: 400}, {title: "Options", align: GUI.HORIZONTAL_ALIGNMENT_CENTER});
		this.options.isVisible = false;		
		this.graphique = this.Checkbox("Qualiter graphique HD (Prochaine session)", {left: 15, top: 35, width: 350, isChecked: (options.isHD || false)}, (value) =>  {
			this.isHD = value;
		}, this.options, null);
		this.optimize = this.Checkbox("Optimisation Octree", {left: 15, top: 65, width: 350, isChecked: (options.isOctree || false)}, (value) =>  {
			if(value == true) {
				
			}
			this.isOctree = value;			
		}, this.options, null);
		this.webVR = this.Checkbox("WebVR Camera", {left: 15, top: 95, width: 350, isChecked: (options.WebVR || false)}, (value) =>  {			
			if(value == true) {
				
			} else{
				
			}
			this.WebVR = value;		
		}, this.options, null);
		this.Anaglyphe = this.Checkbox("Anaglyphe Camera", {left: 15, top: 125, width: 350, isChecked: (options.isAnaglyphe || false)}, (value) =>  {
				if(value == true) {
					
				} else{
						
				}
			this.isAnaglyphe = value;
		}, this.options, null);		
		this.FXAA = this.Checkbox("Post-Process FXAA", {left: 15, top: 155, width: 350, isChecked: (options.isFXAA || true)}, (value) =>  {
			let process = new BABYLON.FxaaPostProcess("FXAA", 1.0, global.game.control.camera, BABYLON.Texture.BILINEAR_SAMPLINGMODE, global.game.engine, true);
			if(value == true) {
				
			} else {
				
			}
			this.isFXAA = value;
		}, this.options, null);			
		this.Slider({text:"Volume Musique :", align: GUI.HORIZONTAL_ALIGNMENT_CENTER, verticalAlign: GUI.VERTICAL_ALIGNMENT_TOP, top: 185, width: 350, value: (options.valueVolumeMusic || 50)}, (value) => {			
			BABYLON.Engine.audioEngine.setGlobalVolume(value / 100);
		}, this.options, null);		
		this.Slider({text:"Volume Sons :", align: GUI.HORIZONTAL_ALIGNMENT_CENTER, verticalAlign: GUI.VERTICAL_ALIGNMENT_TOP, top: 215, width: 350, value: (options.valueVolumeSound || 75)}, (value) => {
			global.sound.volumeSound = (value / 100);
			global.sound.sound.setVolume(value / 100);			
		}, this.options, null);		
		this.Button("AppliquerOptions", "Appliquer", {align: GUI.HORIZONTAL_ALIGNMENT_CENTER, verticalAlign: GUI.VERTICAL_ALIGNMENT_BOTTOM, top: -15}, null, this.options, null, () => {
			this.options.isVisible = false;	
			this.saveOptions();
		});
	}
	
	getOptions()
	{
		let options = {};
		$.ajaxSetup({ async: false});
		$.getJSON("data/options.json", function(data) {
			options = data;
		});
		$.ajaxSetup({ async: true});
		return options;
	}
	
	saveOptions()
	{					
		$.ajaxSetup({ async: false});
		$.ajax({type: "POST", url: "php/save_options.php", data: "isHD="+this.isHD+"&isOctree="+this.isOctree+"&WebVR="+this.WebVR+"&isAnaglyphe="+this.isAnaglyphe+"&isFXAA="+this.isFXAA+"&valueVolumeMusic="+BABYLON.Engine.audioEngine.getGlobalVolume()+"&valueVolumeSound="+global.sound.volumeSound});
		$.ajaxSetup({ async: true});
	}

	switchVisibility() {
		if(this.options.isVisible == true) {
			this.options.isVisible = false;
		} else {
			this.options.isVisible = true;
		}
	}

}