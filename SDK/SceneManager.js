//#####################################################################################################################################################################################
// Heroon Engine SDK V2
// Copyright (C) 2017 ActifGames, LLC. All rights reserved
// contact@heroonengine.actifgames.com
//
// Programmer: David Pellier
// Revision  : 19/06/2017
//#######################################################################################################################################################################################
class SceneManager {

	constructor()
	{ 
		this.path = null;		
		this.UI = UI.createUIManager("UIGAME", true, 1);	
	}
	
	static setFolderPath(path = ".\/")
	{
		this.path = path;
	}
	
	static getFolderPath()
	{
		return this.path;
	}

	// Optimizer de scene pour garder toujour des FPS éléver et verifie toutes les X millisedonde
	// Desactive certainne fonctionnalité du moteur de facons automatique si les FPS descende en dessous de X veleur dans un ordre de priorité.
	// Cette fonction EngineOptimizer est utiliser par le moteur de jeu pour les plus petite configuration de PC de facons a rendre le jeux jouable.
	static EngineOptimizer()
	{   
		let result = new BABYLON.SceneOptimizerOptions(60, 3000); // (avoir 60 FPS, verifier tout les 3 secondes)
		
		result.optimizations.push(new BABYLON.ShadowsOptimization(0)); // Priorité 0		
		result.optimizations.push(new BABYLON.LensFlaresOptimization(1)); // Priorité 1		
		result.optimizations.push(new BABYLON.PostProcessesOptimization(2)); // Priorité 2	
		result.optimizations.push(new BABYLON.TextureOptimization(3, 512));	 // Priorité 3, texture size 512 sur les textures superieur à 512, 1024 ...
		result.optimizations.push(new BABYLON.TextureOptimization(4, 256));	 // Priorité 4, texture size 256 sur les textures superieur 256 ...	
		result.optimizations.push(new BABYLON.ParticlesOptimization(5)); // Priorité 5		
		result.optimizations.push(new BABYLON.RenderTargetsOptimization(6)); // Priorité 6 	
		result.optimizations.push(new BABYLON.HardwareScalingOptimization(7, 2)); // Priorité 7, HardwareScaling 2
		result.optimizations.push(new BABYLON.HardwareScalingOptimization(8, 4)); // Priorité 8, HardwareScaling 4
		
		return result;	
	}	
}