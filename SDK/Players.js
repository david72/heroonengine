//#####################################################################################################################################################################################
// Heroon Engine SDK V2
// Copyright (C) 2017 ActifGames, LLC. All rights reserved
// contact@heroonengine.actifgames.com
//
// Programmer: David Pellier
// Revision  : 21/06/1017
//#######################################################################################################################################################################################
class Players {

	constructor(id)
	{
		this.path = SceneManager.getFolderPath();		
		this.id = id;
		this.content = document.body;
	}

	createElement()
	{		
		// Event for drag
		let start = (coordinates) => {
			UI.dragStart(coordinates, this.divPlayer);
		};
		
		let stop = function() {
			UI.dragStop();
		};
		
		let drag = (coordinates) => {
			UI.dragMove(coordinates, this.divPlayer);
			UI.isDraggable(game.UI,
				(coordinates) => {
					UI.dragMove(coordinates, this.divPlayer);
				});
		};	
		
		// Fenetre draggable
		this.divPlayer = UI.Window({width: "326px", height: "512px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER, radius: 1},
		{title: menuGame.lang.player, align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER, height: "50px", top: 12, radius: 1, size: 18},
		null, null, start, stop, drag);			
		
		// Image de fond de l'inventaire
		UI.Image("Player", {URLImage: this.path+"skins/Window.png", width: "326px", height: "512px"}, this.divPlayer);
		this.divPlayer.isVisible = false;		
	}

	switchVisibility() {
		if(this.divPlayer.isVisible == true) {
			this.divPlayer.isVisible = false;
		} else {
			this.divPlayer.isVisible = true;
		}
	}

}