//#####################################################################################################################################################################################
// Heroon Engine SDK V2
// Copyright (C) 2017 ActifGames, LLC. All rights reserved
// contact@heroonengine.actifgames.com
//
// Programmer: David Pellier
// Revision  : 22/06/1017
//#######################################################################################################################################################################################
class PlayerSelector {

	constructor(user)
	{			
		this.content = document.body;
		this.path = SceneManager.getFolderPath();
		this.data = null;
		this.dataActor = null;
		this.pseudo = user;
		this.idUser = 0;
		this.userActor = null;
		this.userInfo = null;
		this.projet = null;
		this.nbrRace = 1;
		this.nbrClass = 1;

		this.listeRace = [];
		this.listeClasse = [];

		$.ajaxSetup({ async: false});
		$.getJSON(this.path+"game data/projet.json", (data) => {
			this.projet = data;
		});
		$.getJSON(this.path+"game data/users.json", (data) => {
			let countUsers = Object.keys(data["users"]).length - 1;
			for(let i = 0; i <= countUsers; i++) {
				if(data["users"][countUsers]["pseudo"] == this.pseudo) {
					this.idUser = countUsers;
					this.userActor = data["users"][this.idUser]["actors"];
					this.userInfo = data["users"][this.idUser]["info"];
					break;
				}
			}
		});
		$.ajax({type: "POST", url: 'nbrRaceAndClass.php',
			success: (msg) => {
				let liste = msg.split(";");
				for(let i = 0, count = liste.length; i < count; i++) {
					let type = liste[i].split(".");
					this.listeRace.push(type[0]);
					this.listeClasse.push(type[1]);
				}
				this.nbrRace = this.listeRace.length;
				this.nbrClass = this.listeClasse.length;
			}
		});
		$.ajaxSetup({ async: true});

		this.optionsListeActor = [];
		this.actor = null;
		this.hair = null;
		this.beard = null;
		this.armure = null;
		this.materialsHeadCurrent = 0;
		this.materialsTenuCurrent = 0;
		this.raceCurrent = 0;
		this.classCurrent = 0;
		this.hairCurrent = 0;
		this.beardCurrent = 0;

		this.actorSelected = null;
		this.currentNameRace = "race";
		this.currentNameClass = "classe";
		this.currentSex = "man";
		this.currentBeardMesh = "";
		this.currentHairMesh = "";
		this.currentBeardColor = "";
		this.currentHairColor = "";
		this.currentHead = "";
		this.currentClothe = "";
		this.valueSelectColorHair = "black";
		this.valueSelectColorBeard = "black";
	}

	createElementBox()
	{	
		this.divPlayerBoxSelect = UI.Window({width: "280px", height: "230px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER, left: 15, radius: 1});		
		UI.Image("PlayerBox", {URLImage: this.path+"skins/Body-big.png", width: "280px", height: "230px"}, this.divPlayerBoxSelect);
		this.divPlayerBoxSelect.isVisible = false;		
		UI.Button("createCharacter", "Créer un personnage", {width: "280px", height: "30px", left: "15px", top: "15px"}, null, null, null, null, () => { this.divPlayerBoxSelect.isVisible = true; this.createElementButton(); });
	}
	
	listeCharacter()
	{
		this.divCHaracterSelect = UI.Window({width: "280px", height: "55px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP, top: 55, left: 15, radius: 1, background: "transparent"});
		this.group = UI.PanelGroup(this.divCHaracterSelect);		
		if(this.userActor) {
			let countActor = Object.keys(this.userActor).length - 1;
			let top = 15;
			for(let i = 0; i <= countActor; i++) {				
				this.optionsListeActor[i+1] = UI.Button(this.userActor[i]["name"], this.userActor[i]["name"], {width: "280px", height: "30px", top: top+"px"}, null, null, this.group, null, () => {					
					this.actorSelected = this.userActor[i];
					this.currentNameRace = this.actorSelected.race;
					this.currentNameClass = this.actorSelected.classe;					
					this.createCharacter(this.actorSelected, this.actorSelected.genre);
					this.divPlayerInputNameActor.value = this.actorSelected.name;
				});
				top += 40;
				this.divCHaracterSelect.height = top;
			}
		}			
	}

	createElementText()
	{	
		UI.Text("Race : ", {left: 10, top: 15, width: "100px", height: "25px", zIndex: 1}, this.divPlayerBoxSelect);
		UI.Text("Genre : ", {left: 10, top: 45, width: "100px", height: "25px", zIndex: 1}, this.divPlayerBoxSelect);
		UI.Text("Classe : ", {left: 10, top: 75, width: "100px", height: "25px", zIndex: 1}, this.divPlayerBoxSelect);
		UI.Text("Visage : ", {left: 10, top: 105, width: "100px", height: "25px", zIndex: 1}, this.divPlayerBoxSelect);
		UI.Text("Chevelure : ", {left: 10, top: 135, width: "100px", height: "25px", zIndex: 1}, this.divPlayerBoxSelect);
		UI.Text("Vêtement : ", {left: 10, top: 165, width: "100px", height: "25px", zIndex: 1}, this.divPlayerBoxSelect);
		UI.Text("Barbe : ", {left: 10, top: 195, width: "100px", height: "25px", zIndex: 1}, this.divPlayerBoxSelect);
	}

	createElementButton()
	{	
		// Button Previous		
		UI.Button("race", "", {width: "25px", height: "25px", left: "110px", top: "10px"}, this.path+"skins/btnPrecedent.png", this.divPlayerBoxSelect, null, null, () => { this.previous("race"); });
		UI.Button("race", "", {width: "25px", height: "25px", left: "110px", top: "40px"}, this.path+"skins/btnPrecedent.png", this.divPlayerBoxSelect, null, null, () => { this.previous("genre"); });
		UI.Button("race", "", {width: "25px", height: "25px", left: "110px", top: "70px"}, this.path+"skins/btnPrecedent.png", this.divPlayerBoxSelect, null, null, () => { this.previous("class"); });
		UI.Button("race", "", {width: "25px", height: "25px", left: "110px", top: "100px"}, this.path+"skins/btnPrecedent.png", this.divPlayerBoxSelect, null, null, () => { this.previous("head"); });
		UI.Button("race", "", {width: "25px", height: "25px", left: "110px", top: "130px"}, this.path+"skins/btnPrecedent.png", this.divPlayerBoxSelect, null, null, () => { this.previous("hair"); });
		UI.Button("race", "", {width: "25px", height: "25px", left: "110px", top: "160px"}, this.path+"skins/btnPrecedent.png", this.divPlayerBoxSelect, null, null, () => { this.previous("clothes"); });
		UI.Button("race", "", {width: "25px", height: "25px", left: "110px", top: "190px"}, this.path+"skins/btnPrecedent.png", this.divPlayerBoxSelect, null, null, () => { this.previous("beard"); });

		// Button Next		
		UI.Button("race", "", {width: "25px", height: "25px", left: "140px", top: "10px"}, this.path+"skins/btnSuivant.png", this.divPlayerBoxSelect, null, null, () => { this.next("race"); });
		UI.Button("race", "", {width: "25px", height: "25px", left: "140px", top: "40px"}, this.path+"skins/btnSuivant.png", this.divPlayerBoxSelect, null, null, () => { this.next("genre"); });
		UI.Button("race", "", {width: "25px", height: "25px", left: "140px", top: "70px"}, this.path+"skins/btnSuivant.png", this.divPlayerBoxSelect, null, null, () => { this.next("class"); });
		UI.Button("race", "", {width: "25px", height: "25px", left: "140px", top: "100px"}, this.path+"skins/btnSuivant.png", this.divPlayerBoxSelect, null, null, () => { this.next("head"); });
		UI.Button("race", "", {width: "25px", height: "25px", left: "140px", top: "130px"}, this.path+"skins/btnSuivant.png", this.divPlayerBoxSelect, null, null, () => { this.next("hair"); });
		UI.Button("race", "", {width: "25px", height: "25px", left: "140px", top: "160px"}, this.path+"skins/btnSuivant.png", this.divPlayerBoxSelect, null, null, () => { this.next("clothes"); });
		UI.Button("race", "", {width: "25px", height: "25px", left: "140px", top: "190px"}, this.path+"skins/btnSuivant.png", this.divPlayerBoxSelect, null, null, () => { this.next("beard"); });		
		
		let hairValueChangedCallback = (value) => {
			if(this.valueSelectColorHair) {
				this.valueSelectColorHair.valueColor = this.valueSelectColorHair.diffuseColor;
				this.valueSelectColorHair.diffuseColor.copyFrom(value);
			}
		};
		let beardValueChangedCallback = (value) => {			
			if(this.valueSelectColorBeard) {
				this.valueSelectColorBeard.valueColor = this.valueSelectColorBeard.diffuseColor;
				this.valueSelectColorBeard.diffuseColor.copyFrom(value);
			}
		};	
		
		this.colorHair = UI.ColorPicker({left: 45, top: 200, align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER}, null, null, hairValueChangedCallback);
		this.colorHair.isVisible = false;
		this.colorBeard = UI.ColorPicker({left: 45, top: 200, align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER}, null, null, beardValueChangedCallback);
		this.colorBeard.isVisible = false;
		
		UI.Button("PickerColorHair", "Couleur", {width: "80px", height: "25px", left: "170px", top: "130px"}, null, this.divPlayerBoxSelect, null, null, () => {			
			if(this.colorHair.isVisible == false) { 
				if(this.colorBeard.isVisible == true) this.colorBeard.isVisible = false;
				this.colorHair.isVisible = true;
			} else {
				this.colorHair.isVisible = false;
			}
		});
		UI.Button("PickerColorBeard", "Couleur", {width: "80px", height: "25px", left: "170px", top: "190px"}, null, this.divPlayerBoxSelect, null, null, () => { 			
			if(this.colorBeard.isVisible == false) { 
				if(this.colorHair.isVisible == true) this.colorHair.isVisible = false;
				this.colorBeard.isVisible = true;
			} else {
				this.colorBeard.isVisible = false;
			}
		});
	}

	createElementInputNameActor()
	{
		this.divPlayerInputNameActor = document.createElement("input");
		this.divPlayerInputNameActor.placeholder = "Nom de l'acteur";
		this.divPlayerInputNameActor.style.color = "white";
		this.divPlayerInputNameActor.style.bottom = "22px";
		this.divPlayerInputNameActor.style.right = "540px";
		this.divPlayerInputNameActor.style.width = "250px";
		this.divPlayerInputNameActor.style.height = "20px";
		this.divPlayerInputNameActor.style.position = "absolute";		
	}

	createElementButtonValidateActor()
	{		
		let onclick = () => {
			let countActor = Object.keys(this.userActor).length;
			let CheminProject = document.location.href;
			let repertoirPublic = this.projet["update"]["public_folder"];
			let repertoirBeta = this.projet["update"]["beta_folder"];
			let limit = true;
			if(CheminProject.indexOf("Teste") > 0) { // Si on est dans le repertoir de Teste dev
				limit = false;
			} else if(parseInt(this.projet["update"]["nbrActor_beta"]) > countActor && CheminProject.indexOf(repertoirBeta) > 0) { // Si on est dans le repertoir beta
				limit = true;
			} else if(parseInt(this.projet["update"]["nbrActor_public"]) > countActor && CheminProject.indexOf(repertoirPublic) > 0) { // Si on est dans le repertoir public
				limit = true;
			}
			if(limit == true) {
				if(this.divPlayerInputNameActor.value == "") {
					jAlert("Vous devez donner un nom à l'acteur!", "Attention");
				} else {
					let nameActor = this.divPlayerInputNameActor.value,
						animType = this.dataActor.animation_type,
						raceActor = this.currentNameRace,
						genreActor = this.currentSex,
						classeActor = this.currentNameClass,
						headActor = this.currentHead,
						hairActor = this.currentHairMesh,
						hairActorColor = this.currentHairColor,
						beardActor = this.currentBeardMesh,
						beardActorColor = this.currentBeardColor;	
						clothesActor = this.currentClothe;						
					let countOptionListeActor = this.optionsListeActor.length;
					this.optionsListeActor[countOptionListeActor] = document.createElement("option");
					this.optionsListeActor[countOptionListeActor].value = nameActor;
					this.optionsListeActor[countOptionListeActor].innerHTML = nameActor;
					this.selectActor.appendChild(this.optionsListeActor[countOptionListeActor]);				
					$.ajax({type: "POST", url: 'save_actor.php', data: "root="+this.path+"&id="+this.idUser+"&animType="+animType+"&user="+this.pseudo+"&name="+nameActor+"&race="+raceActor+"&genre="+genreActor+"&class="+classeActor+"&head="+headActor+"&hair="+hairActor+"&hairColor="+hairActorColor+"&beard="+beardActor+"&beardColor="+beardActorColor+"&clothes="+clothesActor+"&attributeListe="+attribute.listeAttribute.join(";")+"&valueAttributeListe="+attribute.listeValueAttribute.join(";")});
				}
			} else {
				jAlert("Vous avez atteind la limite du nombre d'acteur à créer pour ce compte", "Attention");
			}
		};		
		UI.Button("validateActor", "Créer l'acteur", {width: "250px", height: "40px", left: -280, top: -15, align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_BOTTOM}, null, null, null, null, onclick);
	}

	createElementButtonEnterGameWithActor()
	{		
		let onclick = () => {
			if(this.divPlayerInputNameActor.value == "" && this.valueSelectActor == null) {
				jAlert("Vous devez créer ou selectionner un acteur!", "Attention");
			} else {
				location.href="game.php?nameActor="+this.divPlayerInputNameActor.value;
			}
		};		
		UI.Button("validateActor", "Entrer en jeu", {width: "250px", height: "40px", left: -15, top: -15, align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_BOTTOM}, null, null, null, null, onclick);
	}

	createElements()
	{
		this.createElementBox();
		this.listeCharacter();
		this.createElementText();		
		this.createElementInputNameActor();
		this.createElementButtonValidateActor();
		this.createElementButtonEnterGameWithActor();
		this.content.appendChild(this.divPlayerInputNameActor);
	}

	previous(type)
	{
		switch(type) {
			case "head": // Têtes
				if(this.materialsHeadCurrent > 0) {
					this.materialsHeadCurrent -= 1;
					this.changeActor(type, this.materialsHeadCurrent);
				}
			break;
			case "clothes":	// Vetement
				if(this.materialsTenuCurrent > 0) {
					this.materialsTenuCurrent -= 1;
					this.changeActor(type, this.materialsTenuCurrent);
				}
			break;
			case "race":
				if(this.raceCurrent > 0) {
					this.raceCurrent -= 1;
					this.changeActor(type, this.raceCurrent);
				}
			break;
			case "genre":  //Sex
				if(this.currentSex == "man") {
					this.currentSex = "woman";
					this.changeActor(type, this.currentSex);
				} else {
					this.currentSex = "man";
					this.changeActor(type, this.currentSex);
				}
			break;
			case "class":
				if(this.classCurrent > 0) {
					this.classCurrent -= 1;
					this.changeActor(type, this.classCurrent);
				}
			break;
			case "hair": //Cheveux
				if(this.hairCurrent > 0) {
					this.hairCurrent -= 1;
					this.changeActor(type, this.hairCurrent);
				}
			break;
			case "beard": // barbe
				if(this.beardCurrent > 0) {
					this.beardCurrent -= 1;
					this.changeActor(type, this.beardCurrent);
				}
			break;
		}
	}

	next(type)
	{
		let count = 0;
		switch(type) {
			case "head": // Tête
				count = Object.keys(this.data['apparence']["texture_face"]).length;
				if(this.materialsHeadCurrent < count) {
					this.materialsHeadCurrent += 1;
					this.changeActor(type, this.materialsHeadCurrent);
				}
			break;
			case "clothes":	// Vetement
				count = Object.keys(this.data['apparence']["texture_tenu"]).length;
				if(this.materialsTenuCurrent < count) {
					this.materialsTenuCurrent += 1;
					this.changeActor(type, this.materialsTenuCurrent);
				}
			break;
			case "race":
				let nbrRace = this.nbrRace;
				if(this.raceCurrent < nbrRace) {
					this.raceCurrent += 1;
					this.changeActor(type, this.raceCurrent);
				}
			break;
			case "genre":  //Sex
				if(this.currentSex == "man") {
					this.currentSex = "woman";
					this.changeActor(type, this.currentSex);
				} else {
					this.currentSex = "man";
					this.changeActor(type, this.currentSex);
				}
			break;
			case "class":
				let nbrClass = this.nbrClass;
				if(this.classCurrent < nbrClass) {
					this.classCurrent += 1;
					this.changeActor(type, this.classCurrent);
				}
			break;
			case "hair": //Cheveux
				count = Object.keys(this.data['apparence']["hair"]).length;
				if(this.hairCurrent < count) {
					this.hairCurrent += 1;
					this.changeActor(type, this.hairCurrent);
				}
			break;
			case "beard": // barbe
				count = Object.keys(this.data['apparence']["beard"]).length;
				if(this.beardCurrent < count) {
					this.beardCurrent += 1;
					this.changeActor(type, this.beardCurrent);
				}
			break;
		}
	}

	changeActor(type, currentIncrementOrDecrement)
	{
		switch(type) {
			case "head": // Tête
				this.actor.material.subMaterials[0].diffuseTexture = new BABYLON.Texture(this.data['apparence']['texture_face'][""+currentIncrementOrDecrement+""]["diffuse"], this.scene);
				if(this.data['apparence']['texture_face'][""+currentIncrementOrDecrement+""]["bump"] != "")
					this.actor.material.subMaterials[0].bumpTexture = new BABYLON.Texture(this.data['apparence']['texture_face'][""+currentIncrementOrDecrement+""]["bump"], this.scene);
				if(this.data['apparence']['texture_face'][""+currentIncrementOrDecrement+""]["specular"] != "")
					this.actor.material.subMaterials[0].specularTexture = new BABYLON.Texture(this.data['apparence']['texture_face'][""+currentIncrementOrDecrement+""]["specular"], this.scene);
				this.currentHead = this.actor.material.subMaterials[0];		
			break;
			case "clothes":	// Vetement
				if(this.materialsTenuCurrent > 0) {
					this.materialsTenuCurrent -= 1;
					this.actor.material.subMaterials[1].diffuseTexture = new BABYLON.Texture(this.data['apparence']['texture_tenu'][""+currentIncrementOrDecrement+""]["diffuse"], this.scene);
					if(this.data['apparence']['texture_tenu'][""+currentIncrementOrDecrement+""]["bump"] != "")
						this.actor.material.subMaterials[1].bumpTexture = new BABYLON.Texture(this.data['apparence']['texture_tenu'][""+currentIncrementOrDecrement+""]["bump"], this.scene);
					if(this.data['apparence']['texture_tenu'][""+currentIncrementOrDecrement+""]["specular"] != "")
						this.actor.material.subMaterials[1].specularTexture = new BABYLON.Texture(this.data['apparence']['texture_tenu'][""+currentIncrementOrDecrement+""]["specular"], this.scene);
				}
				this.currentClothe = this.actor.material.subMaterials[1];
			break;
			case "race":
				for(let i = 0, count = this.listeRace.length; i < count; i++) {
					if(this.listeRace[currentIncrementOrDecrement]) {
						this.currentNameRace = this.listeRace[currentIncrementOrDecrement];
						break;
					}
				}
				if(this.actor) this.actor.dispose();
				this.createCharacter();
			break;			
			case "class":
				for(let i = 0, count = this.listeClasse.length; i < count; i++) {
					if(this.listeClasse[currentIncrementOrDecrement]) {
						this.currentNameClass = this.listeClasse[currentIncrementOrDecrement];
						break;
					}
				}
				if(this.actor) this.actor.dispose();
				this.createCharacter();
			break;
			case "genre": //Sex
				this.currentSex = currentIncrementOrDecrement;
				if(this.actor) this.actor.dispose();
				this.createCharacter(null, this.currentSex);

			break;
			case "hair": //Cheveux
				this.currentHairMesh = this.data['apparence']["hair"][currentIncrementOrDecrement]["mesh"];
				this.currentHairColor = this.valueSelectColorHair;
				if(this.hair) this.hair.dispose();
				this.addHairCharacter(this.currentHairMesh, this.currentHairColor);
			break;
			case "beard": // barbe
				this.currentBeardMesh = this.data['apparence']["beard"][currentIncrementOrDecrement]["mesh"];
				this.currentBeardColor = this.valueSelectColorBeard;
				if(this.beard) this.beard.dispose();
				this.addBeardCharacter(this.currentBeardMesh, this.currentBeardColor);
			break;
		}
	}

	createScene()
	{	
		let sizeSkyBox = 512; // size skybox

		// Add canvas
		$("#ContentCharacterSet").append('<canvas id="scene" style="width:100%;height:100%;touch-action:none;background-color:#6394ed;z-index:1;"></canvas>');
		this.canvas = document.getElementById("scene");// Recuperation de l'élement canvas par son ID

		// init engine
		this.engine = new BABYLON.Engine(this.canvas, true);// Creation du moteur 3D pour le canvas
		this.engine.enableOfflineSupport = false;
		BABYLON.SceneLoader.ShowLoadingScreen = false;

		// init scene
		this.scene = new BABYLON.Scene(this.engine);		
		this.scene.clearColor = new BABYLON.Color3(0.39, 0.58, 0.93);
		this.scene.ambientColor = new BABYLON.Color3(0.33, 0.33, 0.33);

		// create light
		this.light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(0, 1, 0), this.scene);
		this.light.diffuse = new BABYLON.Color3(1, 1, 1);
		this.light.specular = new BABYLON.Color3(0, 0, 0);
		this.light.groundColor = new BABYLON.Color3(0, 0, 0);
		this.light.intensity = 0.9;

		// create camera rotate
		this.cameraControl = new BABYLON.ArcRotateCamera("cameraControl", -1.57, Math.PI/2, 5, new BABYLON.Vector3(0, 1.7, 0), this.scene);
		this.cameraControl.wheelPrecision = 10;
		this.cameraControl.lowerRadiusLimit = 2;
		this.cameraControl.upperRadiusLimit = 11;
		this.cameraControl.lowerBetaLimit = 0.96;
		this.cameraControl.upperBetaLimit = 1.68;		
		this.cameraControl.maxZ = sizeSkyBox*1.5;
		this.cameraControl.attachControl(this.canvas, true);
		this.scene.activeCamera = this.cameraControl;

		// create skybox
		this.skybox = BABYLON.Mesh.CreateBox("skyBox", sizeSkyBox, this.scene);
		let skyboxMaterial = new BABYLON.StandardMaterial("skyBox", this.scene);
		skyboxMaterial.backFaceCulling = false;
		skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture(this.path+"characterSet/deserted", this.scene);
		skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
		skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
		skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
		skyboxMaterial.disableLighting = true;
		this.skybox.material = skyboxMaterial;

		// Create scene 3D
		let rootMesh = this.path+"characterSet/";
		let fileMesh =  "Temple.babylon";
		BABYLON.SceneLoader.Append(rootMesh, fileMesh, this.scene, (newScene) => {
			newScene.meshes[1].scaling = this.rescaleMesh(newScene.meshes[1]);
			newScene.meshes[1].scaling = new BABYLON.Vector3(0.2, 0.1, 0.2);
			newScene.meshes[1].position = new BABYLON.Vector3(0, -1.33, 0);
			newScene.activeCamera = this.cameraControl;
			this.scene = newScene;
		});

		//Render loop scene
		this.engine.runRenderLoop(() => {
			if(this.scene.isReady()) {
				this.scene.render();
				if(this.skybox) {
					this.skybox.rotation.y += 0.0003 * this.scene.getAnimationRatio();// rotate skybox
				}
			}
        });
		
		//BABYLON.SIMDHelper.DisableSIMD(); //Optimisation	SIMD desactiver
		BABYLON.SIMDHelper.EnableSIMD(); //Optimisation	SIMD activer

		// Resize engine if resize browser
		window.onresize = () => { this.engine.resize(); };
	}
	
	rescaleMesh(mesh) 
	{
		let scaling = new BABYLON.Vector3(1, 1, 1);
		let sizeMesh = mesh.getBoundingInfo().maximum.y;
		if(sizeMesh > 1000) {		
			scaling = new BABYLON.Vector3(mesh.scaling.x / 1000, mesh.scaling.y / 1000, mesh.scaling.z / 1000);
		} else if(sizeMesh > 100) {		
			scaling = new BABYLON.Vector3(mesh.scaling.x / 100, mesh.scaling.y / 100, mesh.scaling.z / 100);
		} else if(sizeMesh > 10) {		
			scaling = new BABYLON.Vector3(mesh.scaling.x / 10, mesh.scaling.y / 10, mesh.scaling.z / 10);
		}
		return scaling;		
	}

	createCharacter(actorSelected, genre)
	{
		this.engine.displayLoadingUI();
		
		let fileJson = null;
		if(actorSelected) {			
			fileJson = actorSelected.race+"."+actorSelected.classe+".json";
		} else {
			fileJson = this.currentNameRace+"."+this.currentNameClass+".json";
		}
		$.ajaxSetup({ async: false});
		$.getJSON(this.path+"game data/actors/"+fileJson+"", (data) => {
			this.dataActor = data;
		});
		$.ajaxSetup({ async: true});
		let fileMesh = this.dataActor["apparence"]["mesh_body"];
		let rootMesh = getPath(fileMesh);
		fileMesh = getFile(fileMesh);		
		let rootActor = rootMesh.split("actors/");
		rootMesh = rootMesh.replace(rootActor[0], "");		
		rootMesh = this.path+"meshes/actors/"+rootActor[1];	
		BABYLON.SceneLoader.ImportMesh("", rootMesh, fileMesh, this.scene, (newMeshes) => {
			this.actor = newMeshes[0];			
			this.actor.position = BABYLON.Vector3.Zero();
			this.actor.rotation = BABYLON.Vector3.Zero();
			if(this.actor.skeleton) this.actor.updatePoseMatrix(BABYLON.Matrix.Identity());	
			this.actor.metadata = {type: "player"};
			if(this.dataActor["apparence"]["armure_defaut"] != "" && this.dataActor["apparence"]["armure_defaut"] != "None") {
				this.addarmureCharacter(this.dataActor["apparence"]["armure_defaut"]);
			}
			this.engine.hideLoadingUI();
		});
	}

	addHairCharacter(mesh, color)
	{		
		let rootMesh = getPath(mesh);
		let fileMesh = getFile(mesh);
		let rootActor = rootMesh.split("actors/");
		rootMesh = rootMesh.replace(rootActor[0], "");		
		rootMesh = this.path+"meshes/actors/"+rootActor[1];
		BABYLON.SceneLoader.ImportMesh("", rootMesh, fileMesh, this.scene, (newMeshes) => {
			this.hair = newMeshes[0];
			this.hair.position = BABYLON.Vector3.Zero();
			this.hair.rotation = BABYLON.Vector3.Zero();
			this.hair.scaling = this.rescaleMesh(this.hair);
		});
	}

	addBeardCharacter(mesh, color)
	{		
		let rootMesh = getPath(mesh);
		let fileMesh = getFile(mesh);
		let rootActor = rootMesh.split("actors/");
		rootMesh = rootMesh.replace(rootActor[0], "");		
		rootMesh = this.path+"meshes/actors/"+rootActor[1];
		BABYLON.SceneLoader.ImportMesh("", rootMesh, fileMesh, this.scene, (newMeshes) => {
			this.beard = newMeshes[0];
			this.beard.position = BABYLON.Vector3.Zero();
			this.beard.rotation = BABYLON.Vector3.Zero();
			this.beard.scaling = this.rescaleMesh(this.beard);
		});
	}

	addarmureCharacter(armure)
	{		
		let rootMesh = getPath(armure);
		let fileMesh = getFile(armure);
		let rootActor = rootMesh.split("actors/");
		rootMesh = rootMesh.replace(rootActor[0], "");		
		rootMesh = this.path+"meshes/actors/"+rootActor[1];
		BABYLON.SceneLoader.ImportMesh("", rootMesh, fileMesh, this.scene, (newMeshes) => {
			this.armure = newMeshes[0];
			this.armure.position = BABYLON.Vector3.Zero();
			this.armure.rotation = BABYLON.Vector3.Zero();			
			this.armure.scaling = this.rescaleMesh(this.armure);
		});
	}
}

var getFile = function(fullPath) { // recuper le fichier avec l'extention sans le chemin
	if(fullPath) {
		let filename = fullPath.replace(/^.*[\\\/]/, '');
		return filename;
	}
};
var getPath = function(fullPath) { // recuper le chemin sans le fichier
	if(fullPath) {
		let filename = fullPath.replace(/^.*[\\\/]/, '');
		let root = fullPath.replace(filename, "");
		return root;
	}
};