//#####################################################################################################################################################################################
// Heroon Engine SDK V2
// Copyright (C) 2017 ActifGames, LLC. All rights reserved
// contact@heroonengine.actifgames.com
//
// Programmer: David Pellier
// Revision  : 22/06/2017
//#######################################################################################################################################################################################
class PlayerAttribute {

	constructor()
	{		
		this.path = SceneManager.getFolderPath();			
		this.currentNameRace = characterSet.currentNameRace;
		this.currentNameClass = characterSet.currentNameClass;		
		this.listeAttribute = [];
		this.listeValueAttribute = [];
		this.XP = 10;
		this.divAttributeText = [];
		this.divAttributeTextValue = [];
		this.divAtttributeButtonPrevious = [];
		this.divAtttributeButtonNext = [];
		this.countAttribute = 0;
		
		$.ajaxSetup({ async: false});
		$.getJSON(this.path+"game data/actors/"+this.currentNameRace+"."+this.currentNameClass+".json", (data) => {			
			this.XP = data.XP;
			this.countAttribute = Object.keys(data.aptitude).length;
			for(let i = 0; i < this.countAttribute; i++) {
				this.listeAttribute.push(data.aptitude[i].name);
				this.listeValueAttribute.push(data.aptitude[i].value);
			}			
		});
		$.ajaxSetup({ async: true});
	}

	createElementBox()
	{	
		this.divAttributeBox = UI.Window({width: "280px", height: (36 * (this.countAttribute + 1))+"px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP, top: 15, left: -15, radius: 1});		
		UI.Image("AttributeBox", {URLImage: this.path+"skins/Body-alert.png", width: "280px", height: (36 * (this.countAttribute + 1))+"px"}, this.divAttributeBox);		
	}
	
	createElementTextAttributePoint()
	{		
		UI.Text("Points : ", {left: 110, top: 10, width: "100px", height: "25px", zIndex: 1}, this.divAttributeBox);
		this.divAtttributeTextPointsToUse = UI.Text(this.XP, {left: 170, top: 10, width: "30px", height: "25px", zIndex: 1}, this.divAttributeBox);	
	}

	createElementTextAttribute()
	{
		let top = 40;
		for(let i = 0, count = this.listeAttribute.length; i < count; i++) {			
			this.divAttributeText[i] = UI.Text(this.listeAttribute[i], {left: 10, top: top, width: "30px", height: "25px",  zIndex: 2}, this.divAttributeBox);			
			top += 30;
		}		
	}
	
	createElementTextValue()
	{
		let top = 40;
		for(let i = 0, count = this.listeValueAttribute.length; i < count; i++) {			
			this.divAttributeTextValue[i] = UI.Text(this.listeValueAttribute[i], {left: 130, top: top, width: "30px", height: "25px",  zIndex: 3}, this.divAttributeBox);			
			top += 30;
		}			
	}
	
	createElementButton()
	{		
		//Previous
		let topPrevious = 34;
		for(let iPrevious = 0, count = this.listeAttribute.length; iPrevious < count; iPrevious++)
		{			
			this.divAtttributeButtonPrevious[iPrevious] = UI.Button("Previous"+count, null, {width: "25px", height: "25px", top: topPrevious, left: 210, align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP, zIndex: 4}, this.path+"skins/btnPrecedent.png", this.divAttributeBox, null, null, (e) => { this.previous(iPrevious); }, null, null, null);
			topPrevious += 32;
		}		
		//Next
		let topNext = 34;
		for(let iNext = 0, count = this.listeAttribute.length; iNext < count; iNext++)
		{			
			this.divAtttributeButtonPrevious[iNext] = UI.Button("Next"+count, null, {width: "25px", height: "25px", top: topNext, left: 240, align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP, zIndex: 4}, this.path+"skins/btnSuivant.png", this.divAttributeBox, null, null, (e) => { this.next(iNext); }, null, null, null);
			topNext += 32;
		}		
	}	
		
	next(i)
	{		
		let pointsToUseInt = parseInt(this.divAtttributeTextPointsToUse.text);
		let valueAtributteInt = parseInt(this.divAttributeTextValue[i].text);		
		if(pointsToUseInt > 0) {
			if(valueAtributteInt < 100) {				
				this.divAtttributeTextPointsToUse.text = ""+(pointsToUseInt + 1)+"";				
				let topText = this.divAttributeTextValue[i].top;
				this.divAttributeBox.removeControl(this.divAttributeTextValue[i]);				
				this.divAttributeTextValue[i] = UI.Text((valueAtributteInt - 1), {left: 130, top: topText, width: "30px"}, this.divAttributeBox);
			} else {
				alert("Maximum reached: 0");
			}
		}
	}
	
	previous(i)
	{
		let pointsToUseInt = parseInt(this.divAtttributeTextPointsToUse.text);
		let valueAtributteInt = parseInt(this.divAttributeTextValue[i].text);
		if(pointsToUseInt < this.XP) {
			if(valueAtributteInt > 0) {	
				this.divAtttributeTextPointsToUse.text = ""+(pointsToUseInt - 1)+"";				
				let topText = this.divAttributeTextValue[i].top;
				this.divAttributeBox.removeControl(this.divAttributeTextValue[i]);				
				this.divAttributeTextValue[i] = UI.Text((valueAtributteInt + 1), {left: 130, top: topText, width: "30px"}, this.divAttributeBox);
			} else {
				alert("Minimum reached: 100");
			}
		}		
	}
	
	createElements()
	{
		this.createElementBox();		
		this.createElementTextAttributePoint();
		this.createElementTextAttribute();
		this.createElementTextValue();
		this.createElementButton();			
	}

}