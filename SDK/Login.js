//#####################################################################################################################################################################################
// Heroon Engine SDK V2
// Copyright (C) 2017 ActifGames, LLC. All rights reserved
// contact@heroonengine.actifgames.com
//
// Programmer: David Pellier
// Revision  : 22/06/1017
//#######################################################################################################################################################################################
class Login {

	constructor(id, editor)
	{		
		this.content = document.body;
		this.id = id;
		this.path = SceneManager.getFolderPath();
		this.data = null;
		console.log("HeroonEngine version "+editor.type+" - "+editor.version);
	}	
	
	createElementBox()
	{	
		this.divConnexionBox = document.createElement("div");
		this.divConnexionBox.style.backgroundImage = "url('"+this.path+"skins/Body-alert.png')";
		this.divConnexionBox.style.backgroundSize = "100% 100%";
		this.divConnexionBox.style.width = "350px";
		this.divConnexionBox.style.height = "170px";
		this.divConnexionBox.style.top = "50%";
		this.divConnexionBox.style.left = "50%";
		this.divConnexionBox.style.marginTop = "-150px";
		this.divConnexionBox.style.marginLeft = "-175px";		
		this.divConnexionBox.style.position = "absolute";
		this.divConnexionBox.style.boxShadow = "8px 8px 12px #424242"; 
		this.divConnexionBox.id = this.id;
		this.divConnexionBox.className = this.id;
	}
	
	createElementText()
	{	
		this.divConnexionTextUser = document.createElement("span");
		this.divConnexionTextUser.innerHTML = "Utilisateur : ";
		this.divConnexionTextUser.style.top = "20px";
		this.divConnexionTextUser.style.left = "15px";
		this.divConnexionTextUser.style.position = "absolute";
		
		this.divConnexionTextMail = document.createElement("span");
		this.divConnexionTextMail.innerHTML = "E-Mail : ";
		this.divConnexionTextMail.style.top = "50px";
		this.divConnexionTextMail.style.left = "15px";
		this.divConnexionTextMail.style.position = "absolute";		
		
		this.divConnexionTextPassword = document.createElement("span");
		this.divConnexionTextPassword.innerHTML = "Mot de passe : ";
		this.divConnexionTextPassword.style.top = "80px";
		this.divConnexionTextPassword.style.left = "15px";
		this.divConnexionTextPassword.style.position = "absolute";
	}
	
	createElementInput()
	{	
		this.divFormConnexionButton = document.createElement("form");	
		this.divFormConnexionButton.method = "POST"; // Ne pas modifier | Not edit
		this.divFormConnexionButton.action = "index.php"; // Ne pas modifier | Not edit
		
		this.divConnexionInputUser = document.createElement("input");	
		this.divConnexionInputUser.style.background = "#1c1b1b";
		this.divConnexionInputUser.placeholder = "Pseudo";
		this.divConnexionInputUser.style.color = "white";
		this.divConnexionInputUser.style.top = "15px";
		this.divConnexionInputUser.style.left = "180px";
		this.divConnexionInputUser.style.width = "150px";
		this.divConnexionInputUser.style.height = "20px";		
		this.divConnexionInputUser.style.position = "absolute";
		this.divConnexionInputUser.type = "text";		
		this.divConnexionInputUser.name = "pseudo"; // Ne pas modifier | Not edit
		
		this.divConnexionInputMail = document.createElement("input");	
		this.divConnexionInputMail.style.background = "#1c1b1b";
		this.divConnexionInputMail.placeholder = "Mail";
		this.divConnexionInputMail.style.color = "white";
		this.divConnexionInputMail.style.top = "45px";
		this.divConnexionInputMail.style.left = "180px";
		this.divConnexionInputMail.style.width = "150px";
		this.divConnexionInputMail.style.height = "20px";		
		this.divConnexionInputMail.style.position = "absolute";
		this.divConnexionInputMail.type = "text";		
		this.divConnexionInputMail.name = "mail"; // Ne pas modifier | Not edit
		
		this.divConnexionInputPassword = document.createElement("input");	
		this.divConnexionInputPassword.style.background = "#1c1b1b";
		this.divConnexionInputPassword.placeholder = "Password";
		this.divConnexionInputPassword.style.color = "white";
		this.divConnexionInputPassword.style.top = "75px";
		this.divConnexionInputPassword.style.left = "180px";
		this.divConnexionInputPassword.style.width = "150px";
		this.divConnexionInputPassword.style.height = "20px";		
		this.divConnexionInputPassword.style.position = "absolute";
		this.divConnexionInputPassword.type = "password";		
		this.divConnexionInputPassword.name = "pass"; // Ne pas modifier | Not edit
	}
	
	createElementButton()
	{			
		this.divConnexionButtonCompte = document.createElement("input");	
		this.divConnexionButtonCompte.style.background = "red";
		this.divConnexionButtonCompte.style.color = "white";
		this.divConnexionButtonCompte.style.top = "120px";
		this.divConnexionButtonCompte.style.left = "16px";
		this.divConnexionButtonCompte.style.width = "155px";
		this.divConnexionButtonCompte.style.height = "30px";
		this.divConnexionButtonCompte.value = "Créer le compte";
		this.divConnexionButtonCompte.style.position = "absolute";
		this.divConnexionButtonCompte.style.cursor = "pointer";
		this.divConnexionButtonCompte.type = "submit";
		this.divConnexionButtonCompte.name = "btnCompte"; // Ne pas modifier | Not edit
		
		this.divConnexionButtonLogin = document.createElement("input");	
		this.divConnexionButtonLogin.style.background = "green";
		this.divConnexionButtonLogin.style.color = "white";
		this.divConnexionButtonLogin.style.top = "120px";
		this.divConnexionButtonLogin.style.left = "180px";
		this.divConnexionButtonLogin.style.width = "155px";
		this.divConnexionButtonLogin.style.height = "30px";
		this.divConnexionButtonLogin.value = "Connexion";
		this.divConnexionButtonLogin.style.position = "absolute";
		this.divConnexionButtonLogin.style.cursor = "pointer";
		this.divConnexionButtonLogin.type = "submit";
		this.divConnexionButtonLogin.name = "btnLogin"; // Ne pas modifier | Not edit
	}
	
	createElementCopyright()
	{	
		this.logo = document.createElement("img");
		this.logo.src = "http:\/\/www.heroonengine.actifgames.com\/images\/HEngine.png";
		this.logo.style.width = "64px";
		this.logo.style.height = "64px";
		this.logo.style.position = "absolute";	
		this.logo.style.bottom = "0px";
		this.logo.style.right = "330px";		
				
		this.textCopyright = document.createElement("span");
		this.textCopyright.innerHTML = "<a href='http:\/\/www.heroonengine.actifgames.com\/' target='_blank'>Propulsé par HeroonEngine</a><br /> L'éditeur de MMORPG sur navigateur";
		this.textCopyright.style.width = "320px";
		this.textCopyright.style.color = "blue";
		this.textCopyright.style.fontSize = "18px";		
		this.textCopyright.style.position = "absolute";	
		this.textCopyright.style.bottom = "10px";
		this.textCopyright.style.right = "0px";		
		
		this.content.appendChild(this.logo);
		this.content.appendChild(this.textCopyright);
	}
	
	createElements()
	{	
		this.createElementCopyright();
		this.createElementBox();
		this.createElementText();
		this.createElementInput();
		this.createElementButton();
		
		this.content.appendChild(this.divConnexionBox);
		
		this.divConnexionBox.appendChild(this.divConnexionTextUser);
		this.divConnexionBox.appendChild(this.divConnexionTextMail);
		this.divConnexionBox.appendChild(this.divConnexionTextPassword);
		
		this.divConnexionBox.appendChild(this.divFormConnexionButton);
		this.divFormConnexionButton.appendChild(this.divConnexionInputUser);
		this.divFormConnexionButton.appendChild(this.divConnexionInputMail);
		this.divFormConnexionButton.appendChild(this.divConnexionInputPassword);
		this.divFormConnexionButton.appendChild(this.divConnexionButtonCompte);
		this.divFormConnexionButton.appendChild(this.divConnexionButtonLogin);		
	}
	
}