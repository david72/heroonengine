//#####################################################################################################################################################################################
// Heroon Engine SDK V2
// Copyright (C) 2017 ActifGames, LLC. All rights reserved
// contact@heroonengine.actifgames.com
//
// Programmer: David Pellier
// Revision  : 22/06/1017
//#######################################################################################################################################################################################
class PlayerHelp {

	constructor()
	{
		this.path = SceneManager.getFolderPath();		
	}

	createElement()
	{		
		let text = this.contentHelp() || "";
		this.divHelpForm = UI.Window({width: "300px", height: "500px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER, radius: 1});		
		UI.Image("Help", {URLImage: this.path+"skins/Body-big.png", width: "300px", height: "500px"}, this.divHelpForm);		
		UI.Text(text, {left: 10, top: 15}, this.divHelpForm);
		this.divHelpForm.isVisible = false;
	}

	contentHelp()
	{
		let help = "";
		$.ajax({type: "GET",
				url: this.path+'help.txt',
				async:false,
				success: (msg) => {
					help = msg;
				}
		});
		return help || "";
	}

	switchVisibility() {
		if(this.divHelpForm.isVisible == false) {
			this.divHelpForm.isVisible = true;
		} else {
			this.divHelpForm.isVisible = false;
		}
	}

}