//#####################################################################################################################################################################################
// Heroon Engine SDK V2																								
// Copyright (C) 2017 ActifGames, LLC. All rights reserved																	
// contact@heroonengine.actifgames.com																																																		
// 
// Programmer: David Pellier	
// Revision  : 22/06/1017																									
//#######################################################################################################################################################################################
class ProgressBar {

	constructor(Container)
	{		
		this.path = SceneManager.getFolderPath();
		this.divMaskFond = document.createElement("div");
		this.divMaskFond.style.backgroundColor = "black";
		this.divMaskFond.style.backgroundImage = "url('"+this.path+"images/LoadingZone.jpg')";
		this.divMaskFond.style.backgroundSize = "100% 100%";
		this.divMaskFond.style.top = "0px";
		this.divMaskFond.style.left = "0px";
		this.divMaskFond.style.width = "100%";
		this.divMaskFond.style.height = "100%";	
		this.divMaskFond.style.position = "absolute";
		this.divMaskFond.style.zIndex = "1000";
		this.divMaskFond.style.display = "none";

		this.divTextFond = document.createElement("span");		
		this.divTextFond.style.width = "200px";
		this.divTextFond.style.height = "30px";	
		this.divTextFond.style.bottom = "10px";
		this.divTextFond.style.right = "10px";
		this.divTextFond.style.fontSize = "18px";
		this.divTextFond.style.color = "white";
		this.divTextFond.style.position = "absolute";
		this.divTextFond.style.zIndex = "1001";
		this.divTextFond.innerHTML = "Propulsé par HeroonEngine";
		this.divTextFond.style.display = "none";
		
		this.divProgressBar = document.createElement("progress");		
		this.divProgressBar.style.width = "600px";
		this.divProgressBar.style.height = "30px";	
		this.divProgressBar.style.top = "50%";		
		this.divProgressBar.style.left = "50%";
		this.divProgressBar.style.marginLeft = "-300px";
		this.divProgressBar.style.position = "absolute";
		this.divProgressBar.style.zIndex = "1002";
		this.divProgressBar.id = "progressBar";
		this.divProgressBar.className = "progressBar";
		this.divProgressBar.style.display = "none";	
		this.divProgressBar.max = 100;
		this.divProgressBar.value = 0;	
		
		document.body.appendChild(this.divMaskFond);
		this.divMaskFond.appendChild(this.divProgressBar);	
		this.divMaskFond.appendChild(this.divTextFond);
	}	
	
	onProgress(num)
	{			
		setTimeout(() => {			
			if(this.getValue() >= 100) {
				this.hideProgess();
			} else {
				this.divProgressBar.value += num;
				this.onProgress(num);
			}			
		}, 750);		
	}
	
	setValue(value)
	{
		this.divProgressBar.value = value;
	}
	
	getValue()
	{
		return this.divProgressBar.value;
	}
	
	showProgess()
	{
		this.divMaskFond.style.display = "block";
		this.divProgressBar.style.display = "block";	
		this.divTextFond.style.display = "block";
		this.divProgressBar.value = 0;
	}
	
	hideProgess()
	{		
		this.divMaskFond.style.display = "none";
		this.divProgressBar.style.display = "none";
		this.divTextFond.style.display = "none";		
	}	
}