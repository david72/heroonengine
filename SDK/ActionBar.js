//#####################################################################################################################################################################################
// Heroon Engine SDK V2
// Copyright (C) 2017 ActifGames, LLC. All rights reserved
// contact@heroonengine.actifgames.com
//
// Programmer: David Pellier
// Revision  : 19/06/2017
//#######################################################################################################################################################################################
class ActionBar {

	constructor()
	{
		this.nbrSlot = 12;
		this.nbrPage = 4;
		this.spaceLeft = 1;		
		this.divEmptySlot = [];
		this.divPage = [];
		this.currentPage = 0;
		this.path = SceneManager.getFolderPath();
	}

	createElement()
	{		
		// Bar action		
		this.divActionBar = UI.Window({width: "762px", height: "42px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_BOTTOM, top: -6, radius: 1});		
		UI.Image("actionBarre", {URLImage: this.path+"skins/actionBarre.png", width: "760px", height: "42px"}, this.divActionBar);		

		UI.Button("pageTop", "-", {width: "15px", height: "15px", left: "434px", top: "3px"}, null, this.divActionBar, null, null, () => { this.PageTop(); }, null, null);
		UI.Button("pageBottom", "+", {width: "15px", height: "15px", left: "434px", top: "21px"}, null, this.divActionBar, null, null, () => { this.PageBottom(); }, null, null);

		// Add Slot empty
		this.SlotEmpty();
		
		// Add Slot window
		this.SlotWindow();
	}

	SlotEmpty()
	{	
		// Empty slots		
		for(let page = 0; page < this.nbrPage; page++)
		{
			for(let index = 0; index < this.nbrSlot; index++)
			{				
				this.divEmptySlot[index] = UI.Button("slot"+index, "", {width: "34px", height: "34px", left: this.spaceLeft+"px", top: "3px", background: "transparent", radius: 1}, null, this.divActionBar, null, null, () => { this.Action(this); }, null, null);
				if(page > 0) {
					this.divEmptySlot[index].isVisible = false; // On masque les slots des pages superieur a 0, mais pas la premiere afficher par defaut
				} 
				this.spaceLeft += 36;				
			}
			this.spaceLeft = 1;			
			this.divPage.push(this.divEmptySlot);
			this.divEmptySlot = [];
		}
		/*
		let elements = document.querySelectorAll('.draggable');        
        for(let i = 0 ; i < elements.length ; i++) {
            ActionBar.applyDragEvents(elements[i]);// Paramètres élément déplaçables
        }       
        let droppers = document.querySelectorAll('.dropper');        
        for(let i = 0 ; i < droppers.length ; i++) {
            ActionBar.applyDropEvents(droppers[i]); // Evénements aux zones de drop
        }
		*/
	}
	
	Action(actionType)
	{		
		//TODO: Recuperer l'action dans le moteur de jeux et l'executer.
	}

	SlotWindow()
	{		
		let divSlotForm = [];
		let image = ["Chat.png", "Inventory.png", "Map.png", "Character.png", "Party.png", "Abilities.png", "Quests.png", "Help.png"];
		let openForm = ["chat", "inventory", "map", "character", "party", "abilities", "quests", "help"];
		let spaceLeft = 477;
		for(let index = 0; index < 8; index++)
		{	
			divSlotForm[index] = UI.Button(openForm[index], null, {width: "30px", height: "30px", left: spaceLeft+"px", top: "5px", background: "transparent", radius: 1}, this.path+"skins/"+image[index], this.divActionBar, null, null, () => { ActionBar.openDialog(openForm[index]); }, null, null);
			spaceLeft += 36;
		}		
	}

	PageTop()
	{
		// Si on est superieur de la page 0 on peut passer a la page precedente
		if(this.currentPage > 0 && this.currentPage <= this.nbrPage) {
			this.currentPage -= 1;
			for(let page = 0; page < this.divPage.length; page++) {
				if(page == this.currentPage) {						
					for(let index = 0; index < this.divPage[this.currentPage].length; index++) {	
						this.divPage[page][index].isVisible = true;
					}					
				} else {					
					for(let index = 0; index < this.nbrSlot; index++) {
						this.divPage[page][index].isVisible = false;
					}					
				}
			}
		} else {
			return;
		}
	}

	PageBottom()
	{
		// Si on est inferieur au nombre de page max on peut passer a la page suivante
		if(this.currentPage >= 0 && this.currentPage < this.nbrPage) {
			this.currentPage += 1;
			for(let page = 0; page < this.divPage.length; page++) {
				if(page == this.currentPage) {						
					for(let index = 0; index < this.divPage[this.currentPage].length; index++) {	
						this.divPage[page][index].isVisible = true;
					}					
				} else {					
					for(let index = 0; index < this.nbrSlot; index++) {
						this.divPage[page][index].isVisible = false;
					}					
				}
			}
		} else {
			return;
		}
	}

	static openDialog(type)
	{
		switch(type) {
			case "chat": interfaces.chat.switchVisibility(); break;
			case "inventory": interfaces.inventory.switchVisibility(); break;
			case "map": interfaces.map.switchVisibility(); break;
			case "character": interfaces.character.switchVisibility(); break;
			case "party": interfaces.party.switchVisibility(); break;
			case "abilities": interfaces.abilities.switchVisibility(); break;
			case "quests": interfaces.quests.switchVisibility(); break;
			case "help": interfaces.help.switchVisibility(); break;
		}
	}
	
	/*
	static applyDragEvents(element) {			
		element.draggable = true;
		let dndHandler = ActionBar; // Cette letiable est nécessaire pour que l'événement "dragstart" ci-dessous accède facilement au namespace "dndHandler"			
		element.addEventListener('dragstart', function(e) {
			dndHandler.draggedElement = e.target; // On sauvegarde l'élément en cours de déplacement
			e.target.parent.background = "transparent";
			e.dataTransfer.setData('text/plain', ''); // Nécessaire pour Firefox
		}, false);			
	}
	
	static applyDropEvents(dropper) {			
		dropper.addEventListener('dragover', function(e) {
			e.preventDefault(); // On autorise le drop d'éléments
			this.className = 'dropper drop_hover'; // Et on applique le design adéquat à notre zone de drop quand un élément la survole
		}, false);			
		dropper.addEventListener('dragleave', function() {
			this.className = 'dropper'; // On revient au design de base lorsque l'élément quitte la zone de drop
		});			
		let dndHandler = ActionBar; // Cette letiable est nécessaire pour que l'événement "drop" ci-dessous accède facilement au namespace "dndHandler"
		dropper.addEventListener('drop', (e) => {
			let target = e.target,
			draggedElement = dndHandler.draggedElement, // Récupération de l'élément concerné
			clonedElement = draggedElement.cloneNode(true); // On créé immédiatement le clone de cet élément
			target.className = 'dropper'; // Application du design par défaut				
			clonedElement = target.appendChild(clonedElement); // Ajout de l'élément cloné à la zone de drop actuelle
			this.applyDragEvents(clonedElement); // Nouvelle application des événements qui ont été perdus lors du cloneNode()
			target.background = "none";
			draggedElement.parentNode.removeChild(draggedElement); // Suppression de l'élément d'origine				
		});			
	}
	*/
}