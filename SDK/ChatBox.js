//#####################################################################################################################################################################################
// Heroon Engine SDK V2
// Copyright (C) 2017 ActifGames, LLC. All rights reserved
// contact@heroonengine.actifgames.com
//
// Programmer: David Pellier
// Revision  : 21/06/1017
//#######################################################################################################################################################################################
class ChatBox {

	constructor()
	{
		this.path = SceneManager.getFolderPath();
	}

	createElement()
	{
		this.divChatContent = UI.Window({width: "480px", height: "350px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_BOTTOM, radius: 1, left: 15, top: -75});		
		
		this.TextChat = UI.Text("", {left: 10, top: 32, width: "470px", height: "330px"}, this.divChatContent);		
		
		this.divChatBoxTab1 = UI.Button(menuGame.lang.general, menuGame.lang.general, {width: "89px", height: "25px", top: 1, left: 0, align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP}, null, this.divChatContent,null, null, null, null, null, null);
		this.divChatBoxTab2 = UI.Button(menuGame.lang.chat1, menuGame.lang.chat1, {width: "89px", height: "25px", top: 1, left: 91, align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP}, null, this.divChatContent,null, null, null, null, null, null);
		this.divChatBoxTab3 = UI.Button(menuGame.lang.chat2, menuGame.lang.chat2, {width: "89px", height: "25px", top: 1, left: 182, align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP}, null, this.divChatContent,null, null, null, null, null, null);
		
		this.divChatContent.isVisible = false;		
		
		$("body").append('<form id="chatmessage" name="chatmessage" method="POST" onKeyUp="chat.disableEnterKey(event);" onSubmit="chat.addMessage(game.character.actor); return false;" style="position:relative;margin-left:50%;left:-380px;bottom:80px;z-index:100;display:none;">'+
			'<input type="text" class="messageChat" name="message" id="messagetext" value="" placeholder="'+menuGame.lang.whatMean+'" style="width:760px;" />'+
		'</form>');
	}

	getMessages()
	{		
		$.ajax({url:"chat_messages.php", type:"POST",
			success: (msg) => {
				if(msg) {					
					this.TextChat.text = msg;	
					setTimeout(() => { this.getMessages(); }, 3000);
				}
			}
		});
	}

	addMessage(actor, mess)
	{
		let message;
		if(mess) { message = mess; }
		else { message = $("#messagetext").val(); }
		if(message) {
			$.ajax({url:"chat_add.php", type:"POST", data:"message=" + message,
				success: (msg) => {
					if(actor.name) {
						game.createBullChat(actor, message);
					}
					this.getMessages();
					$("#messagetext").val("");					
				}
			});
		}
	}

	disableEnterKey(e)
	{
		let key;
		if(window.event) {
			key = window.event.keyCode; //pour IE
		} else{
			key = e.which; //pour firefox
		}
	}

	switchVisibility()
	{
		if(this.divChatContent.isVisible == true) {
			this.divChatContent.isVisible = false;
			$("#chatmessage").hide();
		} else {
			this.divChatContent.isVisible = true;
			$("#chatmessage").show();
		}
	}
	
}