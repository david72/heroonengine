//#####################################################################################################################################################################################
// Heroon Engine SDK V2
// Copyright (C) 2017 ActifGames, LLC. All rights reserved
// contact@heroonengine.actifgames.com
//
// Programmer: David Pellier
// Revision  : 22/06/1017
//#######################################################################################################################################################################################
var map = [];
var frame_start = 0, frame_end = 0, frame_speed = 1.0;

class CameraController {

	constructor(cameraPlayer, cameraMiniMap, actor, user, scene) {
		
		this.path = SceneManager.getFolderPath();
		this.idUser = 0;
		this.pseudo = user;
		this.userActor = null;
		this.scene = scene;
		this.cameraPlayer = cameraPlayer;
		this.cameraMiniMap = cameraMiniMap;
		this.keyAnimation = {};
		this.animationName = {};
		let newObjectAnim = {};	
		this.actor = actor;
		this.radiusCamera = game.cameraPlayer.radius;
		this.speedMoveActor = 0;	
		this.radiusReset = false;
		this.gravity = 0.15;
		this.PI = Math.PI;
		this.rotationActor = (this.PI + (this.PI / 2));
		this.cameraHeight = 1.7;
		this.underWater = 6.6;
		this.keys = {
			walk: 0,
			backwards: 0,
			run: 0,
			jump: 0
		};
		
		$.ajaxSetup({ async: false});			
			$.getJSON(this.path+"game data/users.json?" + (new Date()).getTime(), (data) => {
				let countUsers = Object.keys(data["users"]).length;
				for(let i = 0; i < countUsers; i++) {
					if(data["users"][i]["actors"]["name"] == nameActor) {
						this.idUser = i;										
						break;
					}					
				}
				for(let i = 0; i < countUsers; i++) {
					if(data["users"][i]["pseudo"] == this.pseudo) {						
						this.userActor = data["users"][i]["actors"][this.idUser];						
						break;
					}
					
				}
			});				
			$.getJSON(this.path+"game data/actors/animations.json", (data) => {
				let countAnim = Object.keys(data["animations"]).length;
				for(let i = 0; i < countAnim; i++) {					
					if(data["animations"][i].name == this.userActor.animation) {
						this.keyAnimation = data["animations"][i].etat;
						break;
					}
				}
				let countEtat = Object.keys(this.keyAnimation).length;
				for(let i = 0; i < countEtat; i++) {									
					newObjectAnim[this.keyAnimation[i].frame_name] = {};
					newObjectAnim[this.keyAnimation[i].frame_name].frame_start = this.keyAnimation[i].frame_start;
					newObjectAnim[this.keyAnimation[i].frame_name].frame_end = this.keyAnimation[i].frame_end;
					newObjectAnim[this.keyAnimation[i].frame_name].frame_speed = this.keyAnimation[i].frame_speed;
					this.animationName = newObjectAnim;
				}
			});			
		$.ajaxSetup({ async: true});		
	}
		
	moveActor() {		
		if(this.cameraPlayer && this.actor) {						
			if(this.speedMoveActor > 0) {
				this.actor.rotation.y = (-this.rotationActor - this.cameraPlayer.alpha);				
				this.cameraPlayer.setTarget(new BABYLON.Vector3(this.actor.position.x, (this.actor.position.y + this.cameraHeight), this.actor.position.z));
				this.cameraPlayer.radius = this.radiusCamera;
			}
			if(this.cameraMiniMap) {
				this.cameraMiniMap.position.x = this.actor.position.x;
				this.cameraMiniMap.position.z = this.actor.position.z;
			}			
			if(game.collisionWalls.length > 0) {
				this.CollisionCamera();
			}			
			if(this.keys.walk == 1 || this.keys.run == 1 || this.keys.jump == 1) { // En avant				
				let forward = new BABYLON.Vector3(Math.sin(parseFloat(this.actor.rotation.y)) / this.speedMoveActor, this.gravity, Math.cos(parseFloat(this.actor.rotation.y)) / this.speedMoveActor);
				this.actor.moveWithCollisions(forward.negate());			
			} else if(this.keys.backwards == 1) { // En arriere
				this.cameraPlayer.radius = this.radiusCamera;
				let backwards = new BABYLON.Vector3(Math.sin(parseFloat(this.actor.rotation.y)) / this.speedMoveActor, -this.gravity, Math.cos(parseFloat(this.actor.rotation.y)) / this.speedMoveActor);
				this.actor.moveWithCollisions(backwards);
			} else {
				this.radiusCamera = this.cameraPlayer.radius;
			}	
		}
	}
	
	CollisionCamera() {		
		this.cameraPlayer._getViewMatrix();
		let cameraRay = new BABYLON.Ray(this.actor.position, this.cameraPlayer.position.subtract(this.actor.position).normalize());
		let smallestDistance = this.radiusCamera;				
		game.collisionWalls.forEach((wall) => {			
			if(!wall.isInFrustum(BABYLON.Frustum.GetPlanes(this.scene._transformMatrix))) {	return; }
			let pickInfo = this.scene.pickWithRay(cameraRay, function(mesh) { return wall == mesh;}, true);			
			if(pickInfo.hit && pickInfo.distance > 0) {				
				smallestDistance = pickInfo.distance;
			}
		});	
		if(smallestDistance <= this.cameraPlayer.radius) { 
			this.cameraPlayer.radius = smallestDistance;
		}		
	}	
	
	controlKeyDown(that, evt) {	
		/*
		Gestion clavier français et anglais
		16 = MAJ
		90 = Z (FR)
		81 = Q
		65 = S (FR)
		83 = A
		87 = W
		32 = space
		*/		
		if(map[90] && !map[16] && !map[32] && !map[83]) {
			if (that.keys.walk == 0) {
				that.keyAssign("walk");		
				that.speedMoveActor = 15;
				frame_start = parseInt(that.animationName["marcher"].frame_start);
				frame_end = parseInt(that.animationName["marcher"].frame_end);
				frame_speed = parseFloat(that.animationName["marcher"].frame_speed)/100;
				that.scene.beginAnimation(that.actor.skeleton, frame_start, frame_end, true, frame_speed);
			}
		} else if(map[65] || map[83]) {
			if (that.keys.backwards == 0) {
				that.keyAssign("backwards");
				that.speedMoveActor = 15;
				let frame_start = parseInt(that.animationName["marcher"].frame_start);
				let frame_end = parseInt(that.animationName["marcher"].frame_end);
				let frame_speed = parseFloat(that.animationName["marcher"].frame_speed)/100;				
				that.scene.beginAnimation(that.actor.skeleton, frame_start, frame_end, true, frame_speed);
			}
		} else if((map[16] && map[90]) || (map[16] && map[87])) {	
			if (that.keys.run == 0) {
				that.keyAssign("run");			
				that.speedMoveActor = 6;
				let frame_start = parseInt(that.animationName["courir"].frame_start);
				let frame_end = parseInt(that.animationName["courir"].frame_end);
				let frame_speed = parseFloat(that.animationName["courir"].frame_speed)/100;	
				that.scene.beginAnimation(that.actor.skeleton, frame_start, frame_end, true, frame_speed);
			}	
		}
		$.ajaxSetup({ async: true});
	}

	controlKeyUp(that, evt) {	
		/*
		Gestion clavier français et anglais
		16 = MAJ
		90 = Z (FR)
		87 = W
		*/		
		if(map[16] == false && (map[90] == true || map[87] == true)) {
			// Si on cours et que l'on veux marcher. On relache MAJ avec Z enfoncer pour marcher
			if(that.keys.run == 1) {
				that.keyAssign("walk");			
				that.speedMoveActor = 15;
				frame_start = parseInt(that.animationName["marcher"].frame_start);
				frame_end = parseInt(that.animationName["marcher"].frame_end);
				frame_speed = parseFloat(that.animationName["marcher"].frame_speed)/100;
				that.scene.beginAnimation(that.actor.skeleton, frame_start, frame_end, true, frame_speed);
			}
		} else {
			that.keyAssign("null");
			that.speedMoveActor = 0;
			frame_start = parseInt(that.animationName["repos"].frame_start);
			frame_end = parseInt(that.animationName["repos"].frame_end);
			frame_speed = parseFloat(that.animationName["repos"].frame_speed)/100;	
			that.scene.beginAnimation(that.actor.skeleton, frame_start, frame_end, true, frame_speed);
		}		
		$.ajaxSetup({ async: true});
	}	
	
	keyAssign(indexAssign) {		
		$.ajaxSetup({ async: false});
		$.each(this.keys, (key, dataKey) => {			
			if(indexAssign == key) {
				this.keys[key] = 1;
			} else {
				this.keys[key] = 0;
			}			
		});
	}

}

onkeydown = onkeyup = function(e){
	e = e || event; 
	map[e.keyCode] = e.type == 'keydown';
};