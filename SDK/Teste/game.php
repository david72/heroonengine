<?php
session_start();
error_reporting(0);
?>
<!DOCTYPE HTML>
<html xmlns="http://wwww.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<title>Game</title>	
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<link rel="shortcut icon" href="../../Editor/Heroon Engine/Data Project/Default/Game.ico">	
	<link rel="stylesheet" type="text/css" href="../../Editor/Heroon Engine/Data Project/Default/game.css?<?php echo time();?>">	
	<script src="../../Editor/Heroon Engine/Data Project/Default/Engines/jquery.min.js"></script>
	<script src="https://preview.babylonjs.com/cannon.js?<?php echo time();?>"></script>
	<script src="https://preview.babylonjs.com/babylon.js?<?php echo time();?>"></script>
	<script src="https://preview.babylonjs.com/materialsLibrary/babylon.waterMaterial.min.js?<?php echo time();?>"></script>
	<script src="https://preview.babylonjs.com/materialsLibrary/babylon.customMaterial.min.js?<?php echo time();?>"></script>
	<script src="https://preview.babylonjs.com/gui/babylon.gui.min.js?<?php echo time();?>"></script>
	<script src="http://cdn.rawgit.com/NasimiAsl/Extensions/master/ShaderBuilder/Babylonx.ShaderBuilder.js"></script>
	<script src="../../Editor/Heroon Engine/Data Project/Default/Engines/scripting.min.js?<?php echo time();?>"></script>	
	<script src="../ActionBar.js?<?php echo time();?>"></script>
	<script src="../CameraController.js?<?php echo time();?>"></script>
	<script src="../MapBox.js?<?php echo time();?>"></script>
	<script src="../ChatBox.js?<?php echo time();?>"></script>	
	<script src="../GameMenu.js?<?php echo time();?>"></script>	
	<script src="../Players.js?<?php echo time();?>"></script>
	<script src="../PlayerAttribute.js?<?php echo time();?>"></script>
	<script src="../PlayerHealtBars.js?<?php echo time();?>"></script>
	<script src="../PlayerCompass.js?<?php echo time();?>"></script>
	<script src="../PlayerHelp.js?<?php echo time();?>"></script>
	<script src="../PlayerInventory.js?<?php echo time();?>"></script>
	<script src="../PlayerMap.js?<?php echo time();?>"></script>
	<script src="../PlayerParty.js?<?php echo time();?>"></script>
	<script src="../PlayerQuest.js?<?php echo time();?>"></script>	
	<script src="../PlayerStats.js?<?php echo time();?>"></script>
	<script src="../ProgressBar.js?<?php echo time();?>"></script>
	<script src="../SceneManager.js?<?php echo time();?>"></script>
	<script>
	SceneManager.setFolderPath("../../Editor/Heroon Engine/Data Project/Default/");
	</script>
	<script src="../../Game/Utiles.js?<?php echo time();?>"></script>
	<script src="../../Game/SceneMain.js?<?php echo time();?>"></script>
	<script src="../../Game/Player.js?<?php echo time();?>"></script>
	<script src="../../Game/NPC.js?<?php echo time();?>"></script>
	<script src="../../Game/Teleportation.js?<?php echo time();?>"></script>
	<script src="../../Game/Environement.js?<?php echo time();?>"></script>
	<script src="../../Game/Reseau.js?<?php echo time();?>"></script>
	<script src="../../Game/Sounds.js?<?php echo time();?>"></script>
	<script src="../../Game/ExecScripts.js?<?php echo time();?>"></script>
	<script src="../../Game/IA.js?<?php echo time();?>"></script>
	<script src="../../Game/Items.js?<?php echo time();?>"></script>
	<script src="../../Game/Particles.js?<?php echo time();?>"></script>	
	<script src="../../Game/Competances.js?<?php echo time();?>"></script>
	<script src="../../Game/Options.js?<?php echo time();?>"></script>
	
	<?php
	if(@$_GET['zone']) $zoneName = @$_GET['zone'];
	else { $zoneName = "Delos";	}
	
	$dirnameScript = '../../Editor/Heroon Engine/_Projects/Teste/scripts/'.$zoneName.'/';
	$dirScript = @opendir($dirnameScript); 
	while($file = @readdir($dirScript)) {
		if($file != '.' && $file != '..' && !is_dir($dirnameScript.$file))
		{ 
			$extention = pathinfo($dirnameScript.$file, PATHINFO_EXTENSION); 
			if($extention == "js") echo '<script src="'.$dirnameScript.$file.'" ></script>';
		}
	}    
	@closedir($dirScript);		
	?>
</head>
<body>
	<div id="ContentScene" style="width:100%;height:100%;"></div>
	<script>
	var user = "<?php echo $_SESSION['pseudo'];?>";	
	var nameActor = "<?php echo $_GET['nameActor'];?>";	
	// Scene 3D
	var game = new SceneMain("<?php echo $zoneName;?>");	
	game.createScene();	
	
	var serveur = new Reseau(game.scene);
	serveur.connectToServeur();// Connexion au serveur
	serveur.newClient(user); // Nouveau client connecter
	serveur.recieveMessage(); // Ecoute les messages du serveur
	
	//GUI
	var interfaces = {};
	
	if(GameMenu.getCookie("HEGame-Lang") == null) {
		GameMenu.setCookie("HEGame-Lang", "French");
	}
	var menuGame = new GameMenu();
	
	var sceneManager = new SceneManager();

	var minimap = new PlayerMap(game.cameraMiniMap, game.scene, game.engine);
	minimap.createElement();

	var actionBar = new ActionBar();
	actionBar.createElement();

	var compass = new PlayerCompass();
	compass.createElement();
	compass.rotateCompass(game.cameraPlayer, game.scene);

	var statutBar = new PlayerHealtBars();
	statutBar.createElementForPlayer();
	statutBar.createElementForEnemy();
	statutBar.visibility("enemy", false);

	// For the window of actionbar
	interfaces.inventory = new PlayerInventory(null, game.scene);
	interfaces.inventory.createElement();

	interfaces.chat = new ChatBox();
	interfaces.chat.createElement();
	interfaces.chat.getMessages();

	interfaces.map = new Map();
	interfaces.map.createElement();

	interfaces.help = new PlayerHelp();
	interfaces.help.createElement();

	interfaces.character = new Players();
	interfaces.character.createElement();

	interfaces.party = new PlayerParty();
	interfaces.party.createElement();

	interfaces.abilities = new PlayerStats();
	interfaces.abilities.createElement();

	interfaces.quests = new PlayerQuest();
	interfaces.quests.createElement();
	</script>
</body>
</html>