//#####################################################################################################################################################################################
// Heroon Engine SDK V2
// Copyright (C) 2017 ActifGames, LLC. All rights reserved
// contact@heroonengine.actifgames.com
//
// Programmer: David Pellier
// Revision  : 22/06/1017
//#######################################################################################################################################################################################
class Map {

	constructor()
	{
		this.path = SceneManager.getFolderPath();		
	}

	createElement()
	{		
		// Event for drag
		let start = (coordinates) => {
			UI.dragStart(coordinates, this.divMapForm);
		};
		
		let stop = function() {
			UI.dragStop();
		};
		
		let drag = (coordinates) => {
			UI.dragMove(coordinates, this.divMapForm);
			UI.isDraggable(game.UI,
				(coordinates) => {
					UI.dragMove(coordinates, this.divMapForm);
				});
		};
		
		this.divMapForm = UI.Window({width: "512px", height: "512px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER, radius: 10},
		null, null, null, start, stop, drag);					
		UI.Image("borderMap", {URLImage: this.path+"skins/Body-big.png", width: "512px", height: "512px", zIndex: 1}, this.divMapForm);
		UI.Image("imageMap", {URLImage: this.path+"images/map.png", width: "500px", height: "500px", zIndex: 2, top: "3px", left: "4px"}, this.divMapForm);	
		this.divMapForm.isVisible = false;
	}

	switchVisibility() {
		if(this.divMapForm.isVisible == true) {
			this.divMapForm.isVisible = false;
		} else {
			this.divMapForm.isVisible = true;
		}
	}

}