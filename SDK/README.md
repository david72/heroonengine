# SDK #

Le SDK regroupe des fonctions qui créer toutes l'interface utilisateur du jeu. 
Elle peut être facillement personnalisable par les utilisateurs pour créer leur propre UI.

Le SDK utilise les fonctions de scripting disponnible, permetant à l'utilisateur de rester familiariser avec le code du SDK.
Il est donc possible via des scripts dans l'editeur d'ajouter des elements d'interface pour la rendre dynamique.
