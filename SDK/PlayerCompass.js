﻿//#####################################################################################################################################################################################
// Heroon Engine SDK V2
// Copyright (C) 2017 ActifGames, LLC. All rights reserved
// contact@heroonengine.actifgames.com
//
// Programmer: David Pellier
// Revision  : 21/06/1017
//#######################################################################################################################################################################################
class PlayerCompass {

	constructor()
	{
		this.path = SceneManager.getFolderPath();
	}

	createElement()
	{
		let sizeCompass = 104;
		// window compass
		this.divBorder = UI.Window({width: (sizeCompass+12)+"px", height: (sizeCompass+12)+"px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP, top: 9, radius: 1, background: "transparent", zIndex: 1});
			
		// Border of compass
		UI.Image("borderCompass", {URLImage: this.path+"skins/Compass-round.png", width: (sizeCompass+12)+"px", height: (sizeCompass+12)+"px", zIndex: 2, align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER}, this.divBorder);
		// Needle of compass
		this.divNeedle = UI.Image("needleCompass", {URLImage: this.path+"skins/compass.png", width: sizeCompass+"px", height: sizeCompass+"px", zIndex: 3, align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER}, this.divBorder);
		// Fond of compass
		this.divOverlay = UI.Image("overlayCompass", {URLImage: this.path+"skins/overlay.png", width: sizeCompass-2+"px", height: sizeCompass-2+"px", zIndex: 4, align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER}, this.divBorder);
	}

	rotateCompass(cameraPlayer, scene)
	{
		scene.registerBeforeRender(() => {
			this.divOverlay.rotation = -(cameraPlayer.alpha - 4.71);
		});
	}

}