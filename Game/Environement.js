class Environnement {

	constructor(scene)
	{
		this.path = SceneManager.getFolderPath();
		this.scene = scene;		
		this.CreateSun();
		this.CreateMoon();
		this.CreateAxisWorldWithSunAndMooon();
		this.isDay = true;
		this.particleEnv = new Particles(this.scene);
		this.soundEnv = new Sounds(this.scene);
	}

	CreateSun()
	{
		// Soleil toujour face camera
		this.sun = BABYLON.Mesh.CreatePlane("plane", 60.0, this.scene);
		this.sun.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;
		this.sun.position.y = (game.terrain.metadata.size/1.5);
		// Materiel du soleil
		let materialSun = new BABYLON.StandardMaterial("texture6", this.scene);
		materialSun.diffuseTexture = new BABYLON.Texture(this.path+"textures/Suns_Moons/Sun.png", this.scene);
		materialSun.diffuseTexture.hasAlpha = true;	
		materialSun.emissiveColor = new BABYLON.Color3(1, 1, 1);
		this.sun.material = materialSun;
		// lensFlare sur le soleil				
		this.lensFlareSunSystem = new BABYLON.LensFlareSystem("lensFlareSystem", this.sun, this.scene);		
		this.SunFlare = new BABYLON.LensFlare(0.5, 1, new BABYLON.Color3(1, 1, 1), this.path+"images/Flares/SunFlare.png", this.lensFlareSunSystem);
		this.lensFlareSunSystem.isEnabled = true;
		// Repositionner la lumiere	sur le soleil			
		game.light.position = this.sun.position;
	}
	
	CreateMoon()
	{
		// Lune toujour face camera
		this.moon = BABYLON.Mesh.CreatePlane("plane", 50.0, this.scene);
		this.moon.position.y = -(game.terrain.metadata.size/1.5);
		this.moon.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;
		// Materiel de la lune
		let materialMoon = new BABYLON.StandardMaterial("texture6", this.scene);
		materialMoon.diffuseTexture = new BABYLON.Texture(this.path+"textures/Suns_Moons/Moon.png", this.scene);
		materialMoon.diffuseTexture.hasAlpha = true;
		materialMoon.emissiveColor = new BABYLON.Color3(0.5, 0.5, 0.5);
		this.moon.material = materialMoon;
	}
	
	CreateAxisWorldWithSunAndMooon()
	{
		// Axe de rotation du centre du monde qui fait tourner le soleil avec la lumiere et la lune		
		this.axisWorld = new BABYLON.Mesh("axisWorld", this.scene);
		this.axisWorld.position = BABYLON.Vector3.Zero();
		this.sun.parent = this.axisWorld;
		this.moon.parent = this.axisWorld;
		this.particleChange = 0;
		this.scene.registerBeforeRender(() => {			
			this.DayOrNight();
		});	
	}
	
	DayOrNight()
	{
		if(game.skyboxMaterial) {
			// Changement de la position du soleil et de le lune
			this.axisWorld.rotation.z -= 0.003 * this.scene.getAnimationRatio();
			let positionSun = this.sun.getAbsolutePosition();
			game.light.position = positionSun;
			
			// Degrader du coucher ou lever du soleil
			if(positionSun.y > 0 && positionSun.y < 21) {			
				if(game.light.intensity < 1.5) game.light.intensity += 0.01;			
			} else if(positionSun.y > 21 && positionSun.y < 0) {				
				if(game.light.intensity > 0) game.light.intensity -= 0.01;				
			}
			
			// Changement du ciel lorsque l'on est de jour ou de nuit 
			if(positionSun.y > 21) { // Jour
				
				this.particleChange = 0;
				this.lensFlareSunSystem.isEnabled = true;
				game.light.intensity = 1.5;
				if(this.isDay == false) {
					this.particleChange += 1;
					game.skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture(this.path+"textures/skybox/TropicalSunnyDay", this.scene);
					game.skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
					this.isDay = true;
				}
				
			} else if(positionSun.y < 21) { // Nuit
				this.particleChange = 0;
				this.lensFlareSunSystem.isEnabled = false;
				game.light.intensity = 0;
				if(this.isDay == true) {
					this.particleChange += 1;
					game.skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture(this.path+"textures/skybox/TropicalSunnyNight", this.scene);
					game.skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
					this.isDay = false;
					
				}
			}
			// Load particle and sound environnement			
			if(this.particleChange == 1) {
				let random = Math.floor((Math.random() * 100) + 1);
				let chanceToRain = 70,
					chanceToSnow = 90,
					chanceToStorm = 80;
					
				// On supprimer dans un premier temps les particle et sont d'environnement créer
				if(this.particleEnv.globalParticle.selectionEmiter)	{
					this.particleEnv.deleteParticule();
					this.soundEnv.sound.stop();
					this.soundEnv.sound.dispose();
				}				
					
				// On charge les particules et les sons
				if(random > chanceToRain) {
					if(game.dataEnvironnement.rainParticle != "None") {
						this.particleEnv.getParticle(game.dataEnvironnement.rainParticle);
						if(game.dataEnvironnement.rainSound != "None") this.soundEnv.loadSimpleSound(game.dataEnvironnement.rainSound);
					}
				}
				else if(random > chanceToSnow) {
					if(game.dataEnvironnement.snowParticle != "None") {
						this.particleEnv.getParticle(game.dataEnvironnement.snowParticle);
						if(game.dataEnvironnement.snowSound != "None") this.soundEnv.loadSimpleSound(game.dataEnvironnement.snowSound);
					}
				}
				if(random > chanceToStorm) {
					if(game.dataEnvironnement.stormParticle != "None") {
						this.particleEnv.getParticle(game.dataEnvironnement.stormParticle);
						if(game.dataEnvironnement.stormSound != "None") this.soundEnv.loadSimpleSound(game.dataEnvironnement.stormSound);
					}
				}
			}			
		}
	}
}