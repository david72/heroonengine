class Reseau {

	constructor(scene)
	{
		this.scene = scene;
		this.socket = null;
		$("head").append('<script id="ioConnect" type="text/javascript" src="http:\/\/localhost:5840/socket.io/socket.io.js"></script>');
	}

	connectToServeur()
	{
		try {			
			this.socket = io.connect('http:\/\/localhost:5840');
		} catch(e) {
			$("#ioConnect").remove();
			console.log("Connexion serveur echec");
		}
	}
	
	newClient(user)
	{
		if(this.socket) this.socket.emit('nouveau_client', ''+user+'');	
	}
	
	sendMessage(type, value)
	{
		if(this.socket) this.socket.emit(type, value);
	}
	
	recieveMessage()
	{
		if(this.socket) {
			// Quand on reçoit un message, on l'insère dans la page
			this.socket.on('message', function(data) {						
				let messageSend = '' + data.pseudo + ': ' + data.message + '<br />';
				$.ajax({url:"chat_add.php", type:"POST", data:"message=" + messageSend});
			});
			
			// Quand un objet 3d change de position
			this.socket.on('moveObjet', function(data) {
				let mesh = this.scene.getMeshByName(data.name);
				mesh.position = new BABYLON.Vector3(data.pos.x, data.pos.y, data.pos.z);
				mesh.rotation = new BABYLON.Vector3(data.rot.x, data.rot.y, data.rot.z);
				//mesh.animation = data.anim; // play animation ici
			});
			
			this.socket.on('new_actor', function(data) {
				game.character.createCharacter(data.rootMesh, data.fileMesh);
			});
			
			// Quand un nouveau client se connecte, on affiche l'information
			this.socket.on('nouveau_client', function(pseudo) {
				let messageSend = '<em>Bienvenue à ' + pseudo + ' qui nous a rejoint !</em><br />';
				$.ajax({url:"chat_add.php", type:"POST", data:"message=" + messageSend});			
			});
		}
	}

	interestZoneEnter()
	{		
		if(this.socket) this.socket.emit('room_enter', 'room_'+user+'');
	}
	
	interestZoneExit()
	{		
		if(this.socket) this.socket.emit('room_exit', 'room_'+user+'');
	}

}