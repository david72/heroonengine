class NPC {

	constructor(scene)
	{
		this.path = SceneManager.getFolderPath();
		this.scene = scene;
		this.npc = null;

		game.getActorNPC();
		game.dataJson(this.path+"game data/actors/"+game.userPNJ.race+"."+game.userPNJ.classe+".json", "actor");
		let fileMeshActor = this.dataPNJ.mesh_body;
		fileMeshActor = fileMeshActor.replace("_Projects\/Teste\/meshes\/actors\/", "");
		this.root = getPath(fileMeshActor);
		this.sceneFilename = getFile(fileMeshActor);

		this.IA = new IA(this.scene);
	}

	createCharacter()
	{
		BABYLON.SceneLoader.ImportMesh("", this.path+"meshes/actors/"+this.root, this.sceneFilename, this.scene, (newMesh) => {
			let mesh = newMesh[0];
			mesh.rotationQuaternion = null;
			mesh.rotation.y = -3.14;
			if(mesh.skeleton) mesh.updatePoseMatrix(BABYLON.Matrix.Identity());
			mesh.position.y = 0.25;
			mesh.checkCollisions = true;
			mesh.ellipsoid = new BABYLON.Vector3(1.0, 1.0, 1.0);
			mesh.ellipsoidOffset = new BABYLON.Vector3(0, 1.995, 0);
			mesh.applyGravity = true;
			mesh.isPickable = true;
			mesh.material.emissiveColor = new BABYLON.Color3(1, 1, 1);

			game.createEtiqueteName(mesh, mesh.name);

			for(let i = 0; i < newMesh.length; i++) {
				newMesh[i].receiveShadows = false;
			}
			if(stringToBoolean(game.dataGeneral.collision_actor) == false){
				mesh.collisionMask = game.getNewMask();
			}
			if(stringToBoolean(game.data.shadow.enabledShadow) && game.shadowMap) {
				game.shadowMap.renderList.push(mesh);
			}
			if(stringToBoolean(game.data.Octree)) {
				mesh.useOctreeForCollisions = true;
				game.octree = this.scene.createOrUpdateSelectionOctree();
				game.octree.dynamicContent.push(mesh);
			}

			this.createHighlightLayer(mesh);

			// Action sur les PNJ
			mesh.actionManager = new BABYLON.ActionManager(this.scene);
			mesh.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickDownTrigger, (e) => { this.createSelectNPC(e.meshUnderPointer); }, null));

			this.npc = mesh;
		});

		this.scene.registerBeforeRender(function() {
			this.IA.updateRenderer();
		});

	}

	createHighlightLayer(mesh)
	{
		// Highlight avec couleur degrader suivant la faction allant du rouge (enemies), bleux (neutre), vers (amis)
		let color1 = 0, color2 = 0, color3 = 0;
		let numFaction = game.getFactionActor();
		if(numFaction < -50) { // Enemies
			color1 = 1.0;
			color2 = parseFloat(((numFaction - (numFaction * 2)) / 100) / 2);
			color3 = 0.0;
		} else if(numFaction > -50 && numFaction < 50) { // Neutre
			color1 = 0.0;
			color2 = 0.5;
			color3 = (0.5 + parseFloat(((numFaction - (numFaction * 2)) / 100) / 2));
		} else if(numFaction > 50) { // Amis
			color1 = 0.0;
			color2 = parseFloat(((numFaction - (numFaction * 2)) / 100) / 2);
			color3 = 0.0;
		}
		this.PNJHighlight = new BABYLON.HighlightLayer("hl_"+mesh.name, this.scene, {camera: game.cameraPlayer});
		this.PNJHighlight.addMesh(mesh, new BABYLON.Color3(color1, color2, color3));
		this.PNJHighlight.blurHorizontalSize = 0.2;
		this.PNJHighlight.blurVerticalSize = 0.2;
		this.PNJHighlight.isEnabled = false;
	}

	createSelectNPC(mesh) {
		if(!this.HaloOfSelected) {
			let y = game.terrain.getHeightAtCoordinates(mesh.position.x, mesh.position.z);
			this.HaloOfSelected = BABYLON.Mesh.CreateDecal("decalHaloNPC", game.terrain, new BABYLON.Vector3(mesh.position.x, y, mesh.position.z), null, new BABYLON.Vector3(0.75, 0.75, 0.75));
			this.HaloOfSelected.material = game.decalMaterialSelect;
			this.HaloOfSelected.position.y += 0.033;
			this.createDialogActorSelect(this.HaloOfSelected, mesh);
		}
	}

	createDialogNPCSelect(selector, NPCSelect) {
		game.ActorSelect = NPCSelect;
		this.contoleSelectActor = UI.Window({width: "300px", height: "155px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER, top: -100, radius: 1});
		UI.Image("contoleSelectActor", {URLImage: this.path+"skins/Body-alert.png", width: "300px", height: "155px"}, this.contoleSelectActor);
		UI.Button("pageTop", "ATTAQUER", {width: "280px", height: "50px", left: 10, top: 20}, null, this.contoleSelectActor, null, null, () => {
			let actorToAttack = NPCSelect;
		});
		UI.Button("pageTop", "QUITTER", {width: "280px", height: "50px", left: 10, top: 80}, null, this.contoleSelectActor, null, null, () => {
			selector.dispose();
			this.contoleSelectActor.isVisible = false;
		});
	}

}