class SceneMain {

	constructor(zoneName)
	{
		// Creation html du canvas pour afficher la scene 3D
		$("#ContentScene").append('<canvas id="scene" style="width:100%;height:100%;touch-action:none;background-color:#6394ed;z-index:1;"></canvas>');

		// Quelque parametre pour le loader de scene
		BABYLON.SceneLoader.ForceFullSceneLoadingForIncremental = true;
		BABYLON.SceneLoader.ShowLoadingScreen = false;
		// Quelque parametre pour le moteur

		BABYLON.Engine.ShadersRepository = "./";

		this.path = SceneManager.getFolderPath();

		this.canvas = document.getElementById("scene");// Recuperation de l'élement canvas par son ID
		this.engine = new BABYLON.Engine(this.canvas, true, {stencil: true, preserveDrawingBuffer: true});// Creation du moteur 3D pour le canvas
		BABYLONX.ShaderBuilder.InitializeEngine();
		this.engine.enableOfflineSupport = false;

		BABYLON.Engine.audioEngine.setGlobalVolume(0.5);

		this.dataEnvironnement = {};
		this.dataGeneral = {};
		this.dataGlobal = 0;
		this.dataFaction = {};
		this.dataActor = {};
		this.dataItems = {};

		this.scene = null;
		this.skybox = null;
		this.octree = null;
		this.shadowMap = null;
		this.zoneName = zoneName;
		this.script = null;

		this.userActor = {};
		this.infoActor = {};
		this.userPNJ = {};

		this.cheminImageGound = null;
		this.loadedTexture = null;
		this.textureGround = null;
		this.textureSize = null;
		this.textureContext = null;
		this.terrain = null;
		this.character = null;
		this.PNJ = [];
		this.spanpoint = [];
		this.ActorSelect = null;
		this.cameraPlayer = null;
		this.cameraMiniMap = null;
		this.collisionWalls = [];
		this.LightPoint = [];
		this.LightSpot = [];
		this.zoneInterestCurrent = null;
		this.currentPositionPlayer = null;
		this.currentRotationPlayer = null;
		this.currentAnimationPlayer = null;
		this.zoneIntest = [];
		this.triggers = [];
		this.portails = [];
		this.lightLoaded = null;
		this.decalMaterialSelect = null;
		this.decalMaterialBlood = null;
		this.sounds = new Sounds(this.scene);
		this.musicZone = new Sounds(this.scene);
	}

	createScene()
	{
		this.dataJson(this.path+"game data/environement.json", "environement");// Appel du fichier environement de la scene
		this.dataJson(this.path+"game data/actors/general.json", "general");// Appel du fichier actor
		this.dataJson(this.path+"game data/actors/faction.json", "faction");// Appel du fichier faction

		// SCENE
		this.scene = new BABYLON.Scene(this.engine);// Création du moteur de scene pour le moteur 3D
		this.scene.ambientColor = new BABYLON.Color3(0.33, 0.33, 0.33);
		this.scene.clearColor = new BABYLON.Color3(0.39, 0.58, 0.93);

		// PHYSIC COLLISIONS
		this.scene.collisionsEnabled = true;
		//this.scene.workerCollisions = false;
		this.scene.enablePhysics(new BABYLON.Vector3(0, parseFloat(this.dataEnvironnement.Gravity), 0), new BABYLON.CannonJSPlugin()); // Activation du moteur de physique avec la gravité

		// Material pour les acteurs selectionner.
		this.decalMaterialSelect = new BABYLON.StandardMaterial("decalMaterialSelect", this.scene);
		this.decalMaterialSelect.diffuseTexture = new BABYLON.Texture(this.path+"Select.png", this.scene);
		this.decalMaterialSelect.diffuseTexture.hasAlpha = true;

		if(this.dataGeneral.image_sang != "None") {
			this.decalMaterialBlood = new BABYLON.StandardMaterial("decalMaterialBlood", this.scene);
			this.decalMaterialBlood.diffuseTexture = new BABYLON.Texture(this.path+this.dataGeneral.image_sang, this.scene);
			this.decalMaterialBlood.diffuseTexture.hasAlpha = true;
		}

		// MUSIC
		this.musicZone.loadMusicOfZone(this.dataEnvironnement.Music);

		// CAMERA
		this.createCameraPlayer();
		this.createCameraMiniMap();

		// OPTIMIZER
		if(stringToBoolean(this.dataEnvironnement.Optimizer)) {
			BABYLON.SceneOptimizer.OptimizeAsync(this.scene, SceneManager.EngineOptimizer());
		}

		// LIGHT
		this.light = new BABYLON.PointLight("Sun", new BABYLON.Vector3(0, 100, 0), this.scene);
		this.light.diffuse = new BABYLON.Color3(1, 1, 1);
		this.light.specular = new BABYLON.Color3(0.3, 0.3, 0.3);
		this.light.intensity = 1.5;		

		// SHADOW
		if(stringToBoolean(this.dataEnvironnement.shadow.enabledShadow)) {
			this.shadow = new BABYLON.ShadowGenerator(parseInt(this.dataEnvironnement.shadow.resolutionShadow), this.light);
			this.shadow.bias = parseFloat(this.dataEnvironnement.shadow.biasShadow);
			this.shadow.blurScale = 2.0;
			if(this.dataEnvironnement.shadow.filterShadow == "0") {
				this.shadow.useBlurExponentialShadowMap = this.shadow.usePoissonSampling = this.shadow.useExponentialShadowMap = this.shadow.useBlurCloseExponentialShadowMap = false;
			}
			else if(this.dataEnvironnement.shadow.filterShadow == "useExponentialShadowMap") {
				this.shadow.useExponentialShadowMap = true;
				this.shadow.usePoissonSampling = this.shadow.useBlurExponentialShadowMap = this.shadow.useBlurCloseExponentialShadowMap = false;
			}
			else if(this.dataEnvironnement.shadow.filterShadow == "useBlurExponentialShadowMap") {
				this.shadow.useBlurExponentialShadowMap = true;
				this.shadow.blurBoxOffset = 2.0;
				this.shadow.blurKernel = 64;
				this.shadow.useKernelBlur = true;
				this.shadow.usePoissonSampling = this.shadow.useExponentialShadowMap = this.shadow.useBlurCloseExponentialShadowMap = false;
			}
			else if(this.dataEnvironnement.shadow.filterShadow == "useBlurCloseExponentialShadowMap") {
				this.shadow.useBlurCloseExponentialShadowMap = true;
				this.shadow.blurBoxOffset = 2.0;
				this.shadow.forceBackFacesOnly = true;
				this.shadow.blurKernel = 64;
				this.shadow.useKernelBlur = true;				
				this.shadow.usePoissonSampling = this.shadow.useExponentialShadowMap = this.shadow.useBlurExponentialShadowMap = false;
			}
			else if(this.dataEnvironnement.shadow.filterShadow == "usePoissonSampling") {
				this.shadow.usePoissonSampling = true;
				this.shadow.useBlurExponentialShadowMap = this.shadow.useBlurCloseExponentialShadowMap = this.shadow.useExponentialShadowMap = false;
			}

			this.light.shadowMinZ = parseInt(this.dataEnvironnement.shadow.distanceShadowMinZ) || 2;
			this.light.shadowMaxZ = parseInt(this.dataEnvironnement.shadow.distanceShadowMaxZ) || 2000;
			this.light.shadowAngle = Math.PI/2;

			this.shadowMap = this.shadow.getShadowMap();
		}

		// LOADING SCENE
		this.loadZone(this.zoneName);

		// FOG
		if(this.dataEnvironnement.Fog.ModeFog != "BABYLON.Scene.FOGMODE_NONE") {
			this.createFog();
		}

		this.scene.executeWhenReady(() => {
			// SIMD
			if(stringToBoolean(this.dataEnvironnement.SIMD)) {
				//BABYLON.SIMDHelper.DisableSIMD(); //Optimisation	SIMD desactiver
				BABYLON.SIMDHelper.EnableSIMD(); //Optimisation	SIMD activer
			}
			// OCTREE
			if(stringToBoolean(this.dataEnvironnement.Octree)) {
				this.octree = this.scene.createOrUpdateSelectionOctree();
			}

			this.scene.activeCameras.push(this.cameraPlayer);
			this.scene.activeCameras.push(this.cameraMiniMap);
		});

		this.listeZoneInterest = () => {
			for(let i = 0; i < this.zoneIntest.length; i++) {
				// Un joueur entre dans une zone d'interet pour le serveur
				if (this.character.actor.intersectsMesh(this.zoneIntest[i], false)) {
					if(this.zoneInterestCurrent != null) {
						serveur.interestZoneExit(this.zoneInterestCurrent);// on quitte une zone d'interet avant d'entrer dans une autres
					}
					serveur.interestZoneEnter(this.zoneIntest[i].name); // On entre dans une nouvelle zone d'interet
					this.zoneInterestCurrent = this.zoneIntest[i].name;// On enregistre la zone d'interet en cours
				}
			}
		};

		// Boucle du jeu à 60 FPS/seconde
		this.engine.runRenderLoop(() => {
			if(this.scene.isReady()) {
				this.scene.render();// Rendu de la scene dans la boucle.
				if(this.skybox) {
					this.skybox.rotation.y += 0.0003 * this.scene.getAnimationRatio();// rotation du ciel
				}
			}
        });

		// EVENT WINDOW
		window.onresize = () => { this.engine.resize(); };
	}

	createPostProcess()
	{
		let value = this.dataEnvironnement.postprocess;
		if(value == "FXAA") {
			let postProcess = new BABYLON.FxaaPostProcess("FXAA", 1.0, this.cameraPlayer, BABYLON.Texture.BILINEAR_SAMPLINGMODE, this.engine, true);
			this.cameraPlayer.attachPostProcess(postProcess);
		}
		else if(value == "HDR") {
			this.hdr = new BABYLON.HDRRenderingPipeline("HDR", this.scene, 1.0, null, [this.cameraPlayer], true);
			this.hdr.brightThreshold = 0.5;
			this.hdr.gaussCoeff = 0.4;
			this.hdr.gaussMean = 1.0;
			this.hdr.gaussStandDev = 10.0;
			this.hdr.minimumLuminance = 0.5;
			this.hdr.luminanceDecreaseRate = 0.5;
			this.hdr.luminanceIncreaserate = 0.5;
			this.hdr.exposure = 1.0;
			this.hdr.gaussMultiplier = 4;
		}
		else if(value == "SSAO") {
			this.ssao = new BABYLON.SSAORenderingPipeline("SSAO", this.scene, 0.5, true);
			this.ssao.fallOff = 0.000001;
			this.ssao.area = 1;
			this.ssao.radius = 0.0001;
			this.ssao.totalStrength = 1.0;
			this.ssao.base = 0.5;
			this.scene.postProcessRenderPipelineManager.attachCamerasToRenderPipeline("SSAO", this.cameraPlayer);
            this.scene.postProcessRenderPipelineManager.enableEffectInPipeline("SSAO", this.ssao.SSAOCombineRenderEffect, this.cameraPlayer);
		}
		else if(value == "BlackAndWhite") {
			let postProcess = new BABYLON.BlackAndWhitePostProcess("BlackAndWhite", 1.0, null, null, this.engine, true);
			this.cameraPlayer.attachPostProcess(postProcess);
		}
		else if(value == "Blur") {
			let postProcess0 = new BABYLON.BlurPostProcess("Horizontal blur", new BABYLON.Vector2(1.0, 0), 40.0, 1.0, this.cameraPlayer);
			let postProcess1 = new BABYLON.BlurPostProcess("Vertical blur", new BABYLON.Vector2(0, 1.0), 40.0, 1.0, this.cameraPlayer);
		}
		else if(value == "CellShading") { }
	}

	_materialGround()
	{
		let cheminImageGound = this.path+"scenes/"+this.terrain.name+".png?" + (new Date()).getTime();
		let rangeSt = -0.4;
		let rangePo = 0.1;

		this.textureGround = new BABYLON.DynamicTexture("DynamicTexture_"+this.terrain.name, 1024, this.scene);
		let textureSize = this.textureGround.getSize();
		let textureContext = this.textureGround.getContext();
		let loadedTexture = new Image();
		loadedTexture.crossOrigin = "Anonymous";
		loadedTexture.src = cheminImageGound;
		loadedTexture.onload = () => {
			textureContext.save();
			textureContext.clearRect(0, 0, textureSize.width, textureSize.height);
			textureContext.drawImage(loadedTexture, 0, 0);
			textureContext.restore();
			this.textureGround.update();
		};

		//Create material shader
		$.ajaxSetup({ async: false});
		$.getJSON(this.path+"scenes/"+this.terrain.name+".data?" + (new Date()).getTime(), (data) => {

			let sresult = BABYLONX.Shader.Join(["vec2 rotate_xy(vec2 pr1, vec2 pr2, float alpha) {vec2 pp2 = vec2( pr2.x - pr1.x, pr2.y - pr1.y ); return vec2( pr1.x + pp2.x * cos(alpha*3.14159265/180.) - pp2.y * sin(alpha*3.14159265/180.), pr1.y + pp2.x * sin(alpha*3.14159265/180.) + pp2.y * cos  (alpha*3.14159265/180.)); } \n vec3 r_y(vec3 n, float a, vec3 c) {vec3 c1 = vec3( c.x, c.y,  c.z ); c1.x = c1.x; c1.y = c1.z; vec2 p = rotate_xy(vec2(c1.x, c1.y), vec2( n.x, n.z ), a); n.x = p.x; n.z = p.y; return n;  } \n vec3 r_x(vec3 n, float a, vec3 c) {vec3 c1 = vec3( c.x, c.y,  c.z ); c1.x = c1.y; c1.y = c1.z; vec2 p = rotate_xy(vec2(c1.x, c1.y), vec2( n.y, n.z ), a); n.y = p.x; n.z = p.y; return n;  } \n vec3 r_z(vec3 n, float a, vec3 c) { vec3 c1 = vec3( c.x, c.y,  c.z ); vec2 p = rotate_xy(vec2(c1.x, c1.y), vec2( n.x, n.y ), a); n.x = p.x; n.y = p.y; return n;  }", "vec3 normalMap() { vec4 result = vec4(0.);  return result.xyz; }"]);
			let root = this.path+"/textures/terrain/";

			this.terrainMaterial = new BABYLON.CustomMaterial('terrainMaterial', this.scene);
			global.terrainMaterial.AddUniform('dyTexture', 'sampler2D');
			global.terrainMaterial.AddUniform('texture1', 'sampler2D');
			global.terrainMaterial.AddUniform('texture2', 'sampler2D');
			global.terrainMaterial.AddUniform('texture3', 'sampler2D');
			global.terrainMaterial.AddUniform('texture4', 'sampler2D');
			global.terrainMaterial.AddUniform('texture5', 'sampler2D');
			global.terrainMaterial.AddUniform('texture6', 'sampler2D');
			global.terrainMaterial.AddUniform('texture7', 'sampler2D'); 
			global.terrainMaterial.AddUniform('texture8', 'sampler2D');		
			
			global.terrainMaterial.AddUniform('scaleTxt1', 'vec2');
			global.terrainMaterial.AddUniform('scaleTxt2', 'vec2');
			global.terrainMaterial.AddUniform('scaleTxt3', 'vec2');
			global.terrainMaterial.AddUniform('scaleTxt4', 'vec2');
			global.terrainMaterial.AddUniform('scaleTxt5', 'vec2');
			global.terrainMaterial.AddUniform('scaleTxt6', 'vec2');
			global.terrainMaterial.AddUniform('scaleTxt7', 'vec2');
			global.terrainMaterial.AddUniform('scaleTxt8', 'vec2');
			
			global.terrainMaterial.onBindObservable.add(() => {        
                if (global.terrainMaterial.getEffect && global.terrainMaterial.getEffect()) {
					global.terrainMaterial.getEffect().setTexture('dyTexture', this.textureGround);
					global.terrainMaterial.getEffect().setTexture('texture1', new BABYLON.Texture(root+getFile(data.textures._1), global.scene));
					global.terrainMaterial.getEffect().setTexture('texture2', new BABYLON.Texture(root+getFile(data.textures._2), global.scene));
					global.terrainMaterial.getEffect().setTexture('texture3', new BABYLON.Texture(root+getFile(data.textures._3), global.scene));
					global.terrainMaterial.getEffect().setTexture('texture4', new BABYLON.Texture(root+getFile(data.textures._4), global.scene));
					global.terrainMaterial.getEffect().setTexture('texture5', new BABYLON.Texture(root+getFile(data.textures._5), global.scene));
					global.terrainMaterial.getEffect().setTexture('texture6', new BABYLON.Texture(root+getFile(data.textures._6), global.scene));
					global.terrainMaterial.getEffect().setTexture('texture7', new BABYLON.Texture(root+getFile(data.textures._7), global.scene));
					global.terrainMaterial.getEffect().setTexture('texture8', new BABYLON.Texture(root+getFile(data.textures._8), global.scene));					
					
					global.terrain.material.getEffect().setVector2('scaleTxt1', {x: parseFloat(data.uvScale._1), y: parseFloat(data.uvScale._1)});
					global.terrain.material.getEffect().setVector2('scaleTxt2', {x: parseFloat(data.uvScale._2), y: parseFloat(data.uvScale._2)});
					global.terrain.material.getEffect().setVector2('scaleTxt3', {x: parseFloat(data.uvScale._3), y: parseFloat(data.uvScale._3)});
					global.terrain.material.getEffect().setVector2('scaleTxt4', {x: parseFloat(data.uvScale._4), y: parseFloat(data.uvScale._4)});
					global.terrain.material.getEffect().setVector2('scaleTxt5', {x: parseFloat(data.uvScale._5), y: parseFloat(data.uvScale._5)});
					global.terrain.material.getEffect().setVector2('scaleTxt6', {x: parseFloat(data.uvScale._6), y: parseFloat(data.uvScale._6)});
					global.terrain.material.getEffect().setVector2('scaleTxt7', {x: parseFloat(data.uvScale._7), y: parseFloat(data.uvScale._7)});
					global.terrain.material.getEffect().setVector2('scaleTxt8', {x: parseFloat(data.uvScale._8), y: parseFloat(data.uvScale._8)});					
				}
			});

			this.terrainMaterial.Fragment_Definitions(sresult+' vec4 SB(vec3 pos, vec3 nrm, vec2 vuv) { vec4 result = vec4(0.); vec3 center = vec3(0.); '+
			new BABYLONX.ShaderBuilder().Map({alpha: true, index: 'dyTexture', bias:0.}).Reference(1)
			.SetUniform('dyTexture', 'sampler2D').SetUniform('texture1', 'sampler2D').SetUniform('texture2', 'sampler2D').SetUniform('texture3', 'sampler2D').SetUniform('texture4', 'sampler2D').SetUniform('texture5', 'sampler2D').SetUniform('texture6', 'sampler2D').SetUniform('texture7', 'sampler2D').SetUniform('texture8', 'sampler2D')
			.SetUniform('scaleTxt1', 'vec2').SetUniform('scaleTxt2', 'vec2').SetUniform('scaleTxt3', 'vec2').SetUniform('scaleTxt4', 'vec2').SetUniform('scaleTxt5', 'vec2').SetUniform('scaleTxt6', 'vec2').SetUniform('scaleTxt7', 'vec2').SetUniform('scaleTxt8', 'vec2')
			.Black(1, BABYLONX.Helper().Map({alpha: true, index: 'texture1', uv: 'vec2(vuv*'+String(data.uvScale._8)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
			.Red(1, BABYLONX.Helper().Map({alpha: true, index: 'texture2', uv:'vec2(vuv*'+String(data.uvScale._1)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
			.Green(1, BABYLONX.Helper().Map({alpha: true, index: 'texture3', uv: 'vec2(vuv*'+String(data.uvScale._2)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
			.Blue(1, BABYLONX.Helper().Map({alpha: true, index: 'texture4', uv: 'vec2(vuv*'+String(data.uvScale._3)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
			.Yellow(1, BABYLONX.Helper().Map({alpha: true, index: 'texture5', uv: 'vec2(vuv*'+String(data.uvScale._4)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
			.Cyan(1, BABYLONX.Helper().Map({alpha: true, index: 'texture6', uv: 'vec2(vuv*'+String(data.uvScale._5)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
			.Pink(1, BABYLONX.Helper().Map({alpha: true, index: 'texture7', uv: 'vec2(vuv*'+String(data.uvScale._6)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
			.White(1, BABYLONX.Helper().Map({alpha: true, index: 'texture8', uv: 'vec2(vuv*'+String(data.uvScale._7)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
			.DisableAlphaTesting().Body +' return result; }');

			this.terrainMaterial.diffuseTexture = new BABYLON.Texture(root+"blank.png", this.scene);
			this.terrainMaterial.Fragment_Custom_Diffuse('result = SB(vPositionW,vNormalW,vDiffuseUV).xyz;');
			this.terrain.material = this.terrainMaterial;
		});
		$.ajaxSetup({ async: true});
	}

	loadZone(nameTerrain)
	{
		this.progress = new ProgressBar();
		if(this.dataEnvironnement.LoadingZone != this.path+"images/LoadingZone.jpg" && this.dataEnvironnement.LoadingZone != "None") {
			this.progress.divMaskFond.style.backgroundImage = "url("+this.path+this.dataEnvironnement.LoadingZone+")"; // On change l'image du loading zone personnalisé
		}
		else if(this.dataEnvironnement.LoadingZone == "None") {
			this.progress.divMaskFond.style.backgroundImage = "url("+this.path+"images/LoadingZone.jpg)"; // On change l'image du loading zone par defaut
		}
		this.progress.showProgess();
		this.createGround(nameTerrain);
	}

	loadMeshOnGround(nameTerrain)
	{
		let rootScene = this.path+'scenes/'+nameTerrain+'/';
		this.item = new Items();
		let assetsManager = new BABYLON.AssetsManager(this.scene);
		assetsManager.useDefaultLoadingScreen = false;
		let countliste = 0;
		$.ajaxSetup({ async: false});
		$.ajax({type: "POST", url: "listeMeshOnGround.php", data: "root="+rootScene,
			success: (liste) => {
				liste = liste.split(";");
				countliste = liste.length - 1;
				this.progress.onProgress((100 / countliste));
				for(let i = 0; i < countliste; i++) {
					let resultJson = null;
					let listes = liste[i]+'?'+(new Date()).getTime();
					$.getJSON(rootScene+listes, (data) => { resultJson = data; });
					if(resultJson.type == "point" || resultJson.type == "spot") {
						this.createLight(resultJson); // BUG
					} else {
						let meshTask = assetsManager.addMeshTask("task"+i, "", rootScene, listes);
						meshTask.onSuccess = (task) => {
							let mesh = task.loadedMeshes[0];
							if(mesh.metadata.type != "zoneInteret" &&  mesh.metadata.type != "sound" && mesh.metadata.type != "portail" && mesh.metadata.type != "collisionbox" && mesh.metadata.type != "trigger" && mesh.metadata.type != "flagspanpoint") {
								this.collisionWalls.push(mesh);
							} else if(mesh.metadata.type == "zoneInteret" || mesh.metadata.type == "sound" || mesh.metadata.type == "portail" || mesh.metadata.type == "collisionbox" || mesh.metadata.type == "trigger" || mesh.metadata.type == "flagspanpoint") {
								mesh.checkCollision = false;
								mesh.isVisible = false;
								mesh.isPickable = false;
								mesh.freezeWorldMatrix();
							}
							switch(mesh.metadata.type) {
								case "water":
									this.water = mesh;
									this.createMaterialOcean();
								break;
								case "skybox":
									this.skybox = mesh;
								break;
								case "zoneInteret":
									this.zoneIntest[this.zoneIntest.length] = mesh;
								break;
								case "sound":
									this.sounds.loadSpacialSound(mesh.name, {mp3: mesh.metadata.mp3, ogg: mesh.metadata.ogg, wav: mesh.metadata.wav}, mesh.getBoundingInfo().boundingBox.maximumWorld.x, mesh.position);
									mesh.dispose();// La schere n'est plus utile maintenant, on la dispose.
								break;
								case "portail":
									this.portails[this.portails.length] = mesh;
								break;
								case "trigger":
									this.triggers[this.trigger.length] = mesh;
								break;
								case "flagspanpoint":
									this.spanpoint[this.spanpoint.length] = mesh;
								break;
								case "pnj":
									let countPNJ = this.PNJ.length;
									this.PNJ[countPNJ] = new NPC(this.scene);
									this.PNJ[countPNJ].createCharacter();
								break;
								case "item":
									if(this.item.exlusivityRace(mesh.metadata.data) == false) mesh.dispose();
									if(this.item.exlusivityClasse(mesh.metadata.data) == false) mesh.dispose();
								break;
							}
							if(mesh.metadata.castShadow == true) {
								if(stringToBoolean(this.dataEnvironnement.shadow.enabledShadow)) {
									this.shadow.push(mesh);
								}
							}
							if(stringToBoolean(this.dataEnvironnement.Octree)) { this.octree = this.scene.createOrUpdateSelectionOctree();}
							mesh.material.freeze();
						}
					}
				}
			}
		});
		$.ajaxSetup({ async: true});
		assetsManager.onFinish = (tasks) => {
			// On ajoute le personnage joueur controlable a la scene
			this.character = new Player(this.scene);
			this.character.createCharacter();
			// SKY par defaut si aucune n'est créer
			if(this.skybox == null) {
				this.createSkyBox("day");
			}
			// Si aucune zone d'interet n'est créer, on en ajoute une par defaut
			if(this.zoneIntest.length == 0) {
				this.zoneIntest[0] = BABYLON.Mesh.CreateBox("ZoneInteret0", this.terrain.metadata.size*1.5, this.scene, false);
				this.zoneIntest[0].metadata = {type: "zoneInteret"};
				this.zoneIntest[0].isVisible = false;
				this.zoneIntest[0].checkCollisions = false;
			}
			this.script = new ExecScripts();
			this.script.loadScript();
		};
		assetsManager.load();
	}

	createGround(nameTerrain)
	{
		BABYLON.SceneLoader.ImportMesh("", this.path+"scenes/", nameTerrain+"_ground.babylon?" + (new Date()).getTime(), this.scene, (newMeshes) => {
			this.terrain = newMeshes[0];
			this.terrain.position.y = 0.1;
			this.terrain.isPickable = false;
			this.terrain.checkCollisions = true;
			this._materialGround();
			this.collisionWalls.push(this.terrain);
			if(stringToBoolean(this.dataEnvironnement.Octree)) {
				this.terrain.useOctreeForCollisions = true;
				this.terrain.subdivide(36);
				this.terrain.createOrUpdateSubmeshesOctree();
			}

			/*
			if(stringToBoolean(this.dataEnvironnement.shadow.enabledShadow)) {
				this.terrain.receiveShadows = true;
			} else {
				this.terrain.receiveShadows = false;
			}
			*/
			//this.environement = new Environnement(this.scene);

			this.loadMeshOnGround(nameTerrain);
		});
	}

	createFog()
	{
		let modeFog = BABYLON.Scene.FOGMODE_NONE;
		if(this.dataEnvironnement.Fog.ModeFog == "BABYLON.Scene.FOGMODE_EXP") modeFog = BABYLON.Scene.FOGMODE_EXP;
		else if(this.dataEnvironnement.Fog.ModeFog == "BABYLON.Scene.FOGMODE_EXP2") modeFog = BABYLON.Scene.FOGMODE_EXP2;
		else if(this.dataEnvironnement.Fog.ModeFog == "BABYLON.Scene.FOGMODE_LINEAR") modeFog = BABYLON.Scene.FOGMODE_LINEAR;
		this.scene.fogMode = modeFog;
		this.scene.fogDensity = parseFloat(this.dataEnvironnement.Fog.DensityFog);
		this.scene.fogStart = parseFloat(this.dataEnvironnement.Fog.StartFog);
		this.scene.fogEnd = parseFloat(this.dataEnvironnement.Fog.EndFog);
		this.scene.fogColor = new BABYLON.Color3(parseFloat(hexToRgb(this.dataEnvironnement.Fog.ColorFog).r), parseFloat(hexToRgb(this.dataEnvironnement.Fog.ColorFog).g), parseFloat(hexToRgb(this.dataEnvironnement.Fog.ColorFog).b));
	}

	createSkyBox(dayOrNight)
	{
		let skyImage = null;
		if(dayOrNight == "day") {
			skyImage = this.dataEnvironnement.SkyDay.split("_");
		} else {
			skyImage = this.dataEnvironnement.SkyNight.split("_");
		}
		let envTexture = new BABYLON.CubeTexture(this.path+"textures/skybox/"+skyImage[0], this.scene);
		this.scene.environmentTexture = envTexture;
		this.skybox = this.scene.createDefaultSkybox(envTexture, true, 2048);
		this.skybox.layerMask = 1;
	}

	createCameraPlayer()
	{
		this.cameraPlayer = new BABYLON.ArcRotateCamera("CameraPlayer", 4.71, Math.PI/2, 4.5, new BABYLON.Vector3(0, 1.5, 0), this.scene);
		this.cameraPlayer.wheelPrecision = 10;
		this.cameraPlayer.lowerRadiusLimit = 0.0001;
		this.cameraPlayer.upperRadiusLimit = 5;
		this.cameraPlayer.layerMask = 1;
		this.cameraPlayer.minZ = 0;
		this.cameraPlayer.maxZ = parseInt(this.dataEnvironnement.CameraDistance);
		this.cameraPlayer.checkCollisions = false;
		this.cameraPlayer.attachControl(this.canvas, true);
		this.cameraPlayer.viewport = new BABYLON.Viewport(0, 0, 1.0, 1.0);
		this.scene.cameraToUseForPointers = this.cameraPlayer;
		// POST-PROCESS
		this.createPostProcess();
	}

	createCameraMiniMap()
	{
		this.cameraMiniMap = new BABYLON.FreeCamera("cameraMiniMap", BABYLON.Vector3.Zero(), this.scene);
		this.cameraMiniMap.position.y = 20;
		this.cameraMiniMap.rotation = new BABYLON.Vector3(1.57, 0, 0);
		this.cameraMiniMap.layerMask = 2;
		this.cameraMiniMap.checkCollisions = false;
	}

	createMaterialOcean()
	{
		this.waterMaterial = new BABYLON.WaterMaterial("water_material", this.scene);
		this.waterMaterial.bumpTexture = new BABYLON.Texture(this.path+"textures/Materials/waterbump.png", this.scene);
		this.waterMaterial.windForce = -6;
		this.waterMaterial.waveHeight = 0.3;
		this.waterMaterial.windDirection = new BABYLON.Vector2(1, 1);
		this.waterMaterial.waterColor = new BABYLON.Color3(0.1, 0.1, 0.6);
		this.waterMaterial.colorBlendFactor = 0.3;
		this.waterMaterial.bumpHeight = 0.1;
		this.waterMaterial.waveLength = 0.1;
		this.waterMaterial.refractionTexture.activeCamera = this.cameraPlayer;
		this.waterMaterial.reflectionTexture.activeCamera = this.cameraPlayer;
		if(this.skybox) this.waterMaterial.addToRenderList(this.skybox);
		if(this.terrain) this.waterMaterial.addToRenderList(this.terrain);

		this.water.material = this.waterMaterial;
	}

	createLight(result)
	{
		let options = result;
		let PositionLumiere = BABYLON.Vector3.Zero();
		switch(options.type) {
			case "point":
				let countLightPoint = this.LightPoint.length;
				this.LightPoint[countLightPoint] = new BABYLON.PointLight("LightPoint"+countLightPoint, new BABYLON.Vector3(0, -1, 0), this.scene);
				this.LightPoint[countLightPoint].diffuse = new BABYLON.Color3(0.8, 0.8, 0.8);
				this.LightPoint[countLightPoint].specular = BABYLON.Color3.Black();
				this.LightPoint[countLightPoint].intensity = 1.0;
				this.lightLoaded = this.LightPoint[countLightPoint];
			break;
			case "spot":
				let countLightSpot = this.LightSpot.length;
				this.LightSpot[countLightSpot] = new BABYLON.SpotLight("LightSpot"+countLightSpot, new BABYLON.Vector3(0, 0, 0), new BABYLON.Vector3(0, -1, 0), 1, 1, this.scene);
				this.LightSpot[countLightSpot].diffuse = new BABYLON.Color3(0.8, 0.8, 0.8);
				this.LightSpot[countLightSpot].specular = BABYLON.Color3.Black();
				this.LightSpot[countLightSpot].intensity = 1.0;
				this.LightSpot[countLightSpot].position = PositionLumiere;
				this.lightLoaded = this.LightSpot[countLightSpot];
			break;
		}
		if(options && this.lightLoaded) {
			this.lightLoaded.diffuse = new BABYLON.Color3(options.diffuse.r, options.diffuse.g, options.diffuse.b);
			this.lightLoaded.specular = new BABYLON.Color3(options.specular.r, options.specular.g, options.specular.b);
			this.lightLoaded.intensity = parseFloat(options.intensity);
			this.lightLoaded.includeOnlyWithLayerMask = options.includeOnlyWithLayerMask;
			this.lightLoaded.excludeWithLayerMask = options.excludeWithLayerMask;
			this.lightLoaded.includedOnlyMeshes = options.includedOnlyMeshes;
			this.lightLoaded.excludedMeshes = options.excludedMeshes;
			this.lightLoaded.direction = new BABYLON.Vector3(options.direction.x, options.direction.y, options.direction.z);
			this.lightLoaded.angleLight = options.angleLight;
			this.lightLoaded.exponent = parseFloat(options.exponent);
			let position = new BABYLON.Vector3(options.positionLight.x, options.positionLight.y, options.positionLight.z);
			this.lightLoaded.position = position;
			this.lightLoaded.position = position;
			let rotation = new BABYLON.Vector3(options.rotationLight.x, options.rotationLight.y, options.rotationLight.z);
			this.lightLoaded.rotation = rotation;
			this.lightLoaded.rotation = rotation;						
		}
	}

	setMeshTransparentFog(mesh, maxz)
	{
		let colors = [];
		let color = [1,1,1,1];
		let vtx = mesh.getVerticesData(BABYLON.VertexBuffer.PositionKind);
		let i = 0;
		while (i < vtx.length) {
			let x = vtx[i++];
			let y = vtx[i++];
			let z = vtx[i++];
			color[3] = 1.0 - Math.min(1, Math.max(0, z / maxz));
			colors.push(color[0],color[1],color[2],color[3]);
		}
		mesh.setVerticesData(BABYLON.VertexBuffer.ColorKind, colors);
		mesh.useVertexColors = true;
		mesh.hasVertexAlpha = true;
	}

	getNewMask(){
		this.dataJson(this.path+"game data/global.json", "global");
		let globalMask = this.dataGlobal.collisionMask *= 2;
		this.saveGlobal("collisionMask", globalMask);
		return globalMask;
	}

	dataJson(file, type)
	{
		$.ajaxSetup({ async: false});
		$.getJSON(file, (data) => {
			if(type == "environement") { this.dataEnvironnement = data[this.zoneName]; }
			else if(type == "general") { this.dataGeneral = data; }
			else if(type == "global") { this.dataGlobal = data; }
			else if(type == "faction") { this.dataFaction = data; }
			else if(type == "actor") { this.dataActor = data; }
			else if(type == "items") { this.dataItems = data; }
		});
		$.ajaxSetup({ async: true});
	}

	getActorPLayer(userName, actorName)
	{
		this.indexActor = 0;
		$.ajaxSetup({ async: false});
		$.getJSON(this.path+"game data/users.json", (data) => {
			let countUsers = Object.keys(data["users"]).length;
			for(let i = 0; i < countUsers; i++) {
				if(data["users"][i]["pseudo"] == userName) {
					let countActorForUsers = Object.keys(data["users"][i]["actors"]).length;
					for(let a = 0; a < countActorForUsers; a++) {
						if(data["users"][i]["actors"][a].name == actorName) {
							game.userActor = data["users"][i]["actors"][a];
							this.indexActor = a;
							return game.userActor;
						}
					}
				}
			}
		});
		this._getInfoPLayer(userName, actorName);
		$.ajaxSetup({ async: true});
	}

	_getInfoPLayer(userName, actorName)
	{
		$.ajaxSetup({ async: false});
		$.getJSON(this.path+"game data/users.json", (data) => {
			let countUsers = Object.keys(data["users"]).length;
			for(let i = 0; i < countUsers; i++) {
					if(data["users"][i]["pseudo"] == userName) {
					if(data["users"][i]["info"][this.indexActor]) {
						game.infoActor = data["users"][i]["info"][this.indexActor];
						return game.infoActor;
					}
				}
			}
		});
		$.ajaxSetup({ async: true});
	}

	getActorNPC(actorName)
	{
		$.ajaxSetup({ async: false});
		$.getJSON(this.path+"game data/users.json", (data) => {

			//this.userPNJ

		});
		$.ajaxSetup({ async: true});
	}

	getFactionActor(factionName)
	{
		return parseIn(this.dataFaction.factions[factionName][factionName]) || 0;
	}

	saveGlobal(type, value)
	{
		$.ajaxSetup({ async: false});
		$.ajax({type: "POST", url: this.path+"save_global.php", data: "type="+type+"&value="+value});
		$.ajaxSetup({ async: true});
	}

	castRay(mesh, distance, showRay)
	{
		let ray = new BABYLON.Ray();
		let rayHelper = new BABYLON.RayHelper(ray);
		let localMeshDirection = new BABYLON.Vector3(0, 0.1, 1);
		let localMeshOrigin = mesh.position;
		rayHelper.attachToMesh(this.cameraPlayer, localMeshDirection, localMeshOrigin, distance);
		if(showRay) { rayHelper.show(scene); }
        let hit = this.scene.pickWithRay(ray) || null;
        if (hit){ return hit.pickedMesh; }
		return false;
	}

	createEtiqueteName(mesh, name) {
		/*
		if(stringToBoolean(this.dataGeneral.label_pseudo_head) == true) {
			let etiqueteName = BABYLON.Mesh.CreatePlane("EtiqueteName", 2);
			etiqueteName.scalign.y = 0.20;
			etiqueteName.parent = mesh;
			etiqueteName.position = new BABYLON.Vector3(0, (mesh.getBoundingInfo().maximum.y + 15), 0);

			let etiquete = BABYLON.GUI.AdvancedDynamicTexture.CreateForMesh(etiqueteName);
			let textPseudo = new BABYLON.GUI.TextBlock();
			textPseudo.text = name;
			textPseudo.color = "white";
			textPseudo.fontSize = 24;
			etiquete.addControl(textPseudo);
		}
		*/
	}

	createBullChat(mesh, text)
	{
		if(this.dataGeneral.display_message == "ChatAndFlottant" || this.dataGeneral.display_message == "Flottant") {
			if(this.bullChat) this.bullChat.dispose();

			this.bullChat = BABYLON.Mesh.CreatePlane("BullChat", 3);
			plane.scalign.y = 0.20;
			plane.parent = mesh;
			plane.position = new BABYLON.Vector3(0, (mesh.getBoundingInfo().maximum.y + 25), 0);

			let bullDynamic = BABYLON.GUI.AdvancedDynamicTexture.CreateForMesh(this.bullChat);
			let rect1 = new BABYLON.GUI.Rectangle();
			rect1.width = 0.3;
			rect1.height = "40px";
			rect1.cornerRadius = 20;
			rect1.color = "transparent";
			rect1.background = this.dataGeneral.color_fond;
			bullDynamic.addControl(rect1);

			let textCHat = new BABYLON.GUI.TextBlock();
			textCHat.text = text;
			textCHat.color = this.dataGeneral.color_text;
			textCHat.fontSize = 24;
			rect1.addControl(textCHat);

			setTimeout(() => {
				if(this.bullChat) { bullDynamic.dispose(); this.bullChat.dispose(); }
			}, 5000);
		}
	}

	createFloatingNumber(mesh, num)
	{
		if(this.dataGeneral.display_dammage == "Chat") {
			interfaces.chat.addMessage(mesh, user+" -"+num);
		}
		else if(this.dataGeneral.display_dammage == "ChatAndFlottant" || this.dataGeneral.display_dammage == "Flottant") {
			if(this.dataGeneral.display_dammage == "ChatAndFlottant") {
				interfaces.chat.addMessage(mesh, user+" -"+num);
			}

			let bullNumber = BABYLON.Mesh.CreatePlane("bullNumber", 1);
			bullNumber.parent = mesh;
			bullNumber.position = new BABYLON.Vector3(0, (mesh.getBoundingInfo().maximum.y + 30), 0);

			let bullDynamicNumber = BABYLON.GUI.AdvancedDynamicTexture.CreateForMesh(bullNumber);
			let textNumber = new BABYLON.GUI.TextBlock();
			textNumber.text = text;
			textNumber.color = "red";
			textNumber.fontSize = 30;
			bullDynamicNumber.addControl(textNumber);

			SceneMain.animNumberFloating(bullDynamicNumber, textNumber, bullNumber);
		}
	}

	static animNumberFloating(systemGUI, control, mesh)
	{
		setTimeout(function() {
			if(mesh.position.y < 60) {
				mesh.position.y += 1;
				SceneMain.animNumberFloating(systemGUI, control, mesh);
			} else {
				if(mesh) { systemGUI.removeControl(control); mesh.dispose();  }
			}
		}, 33);
	}

	projectionBlood(mesh, impactPoint, matrix)
	{
		if(this.decalMaterialBlood) {
			this.impactBlood = BABYLON.Mesh.CreateDecal("decalHalo", mesh, impactPoint, matrix, new BABYLON.Vector3(0.5, 0.5, 0.5));
			this.impactBlood.material = this.decalMaterialBlood;
			this.impactBlood.position.z -= 0.02;
			setTimeout(() => {
				this.impactBlood.dispose();
			}, 10000);
		}
	}
}