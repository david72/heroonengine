class Teleportation {

	constructor(scene)
	{
		this.scene = scene;
	}

	GoTo(portailName)
	{
		let portail = this.scene.getMeshByName(portailName);
		if(portail) {
			if(portail.metadata.soundPortail) {
				game.sounds.loadSimpleSound({mp3: portail.metadata.mp3, ogg: portail.metadata.ogg, wav: portail.metadata.wav});
			}
			if(portail.metadata.goToZoneName != game.zoneName && portail.metadata.goToZoneName != undefined) {
				location.href="game.php?zone="+portail.metadata.goToZoneName+"&go="+portail.position+"&nameActor="+user;
			} else {					
				return new BABYLON.Vector3(portail.position.x, portail.position.y, portail.position.z);
			}
		}
	}
}