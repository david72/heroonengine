class Particles {

	constructor(scene)
	{		
		this.path = SceneManager.getFolderPath();
		this.scene = scene;
		this.globalParticle = {};
		
		BABYLON.Effect.ShadersStore["myEffectFragmentShader"] =
		"#ifdef GL_ES\n" +
		"precision highp float;\n" +
		"#endif\n" +

		"varying vec2 vUV;\n" +                     // Provided by babylon.js
		"varying vec4 vColor;\n" +                  // Provided by babylon.js

		"uniform sampler2D diffuseSampler;\n" +     // Provided by babylon.js
		"uniform float time;\n" +                   // This one is custom so we need to declare it to the effect

		"void main(void) {\n" +
			"vec2 position = vUV;\n" +
			"float color = 0.0;\n" +
			"vec2 center = vec2(0.5, 0.5);\n" +	
			"color = sin(distance(position, center) * 10.0+ time * vColor.g);\n" +
			"vec4 baseColor = texture2D(diffuseSampler, vUV);\n" +
			"gl_FragColor = baseColor * vColor * vec4( vec3(color, color, color), 1.0 );\n" +
		"}\n" +
		"";
		
		let time = 0, order = 0.1;
		if(this.effect) {
			this.effect.onBind = () => {
				this.effect.setFloat("time", time);
				time += order;
				if (time > 100 || time < 0) {
					order *= -1;
				}
			};
		}		
	}
	
	getParticle(selectionEmiter, emiterMesh)
	{					
		if(emiterMesh == undefined) {
			emiterMesh = BABYLON.Mesh.CreateBox("emiter", 1.0, this.scene);
			emiterMesh.isVisible = false;
		}		
		this.globalParticle.selectionEmiter = selectionEmiter;		
		$.getScript(this.path+'/particles/'+this.globalParticle.selectionEmiter+'.js', (data) => {			
			this.globalParticle.mode = _mode;
			this.globalParticle.depthWrite = _depthWrite;
			this.globalParticle.imageParticles = _imageParticles;
			this.globalParticle.sizeEmiter = 1;
			this.globalParticle.colorMask[0] = _colorMask[0];
			this.globalParticle.colorMask[1] = _colorMask[1];
			this.globalParticle.colorMask[2] = _colorMask[2];
			this.globalParticle.colorMask[3] = _colorMask[3];
			this.globalParticle.minEmitBox[0] = _minEmitBox[0];
			this.globalParticle.minEmitBox[1] = _minEmitBox[1];
			this.globalParticle.minEmitBox[2] = _minEmitBox[2];
			this.globalParticle.maxEmitBox[0] = _maxEmitBox[0];
			this.globalParticle.maxEmitBox[1] = _maxEmitBox[1];
			this.globalParticle.maxEmitBox[2] = _maxEmitBox[2];
			this.globalParticle.particleColor1[0] = _particleColor1[0];
			this.globalParticle.particleColor1[1] = _particleColor1[1];
			this.globalParticle.particleColor1[2] = _particleColor1[2];
			this.globalParticle.particleColor1[3] = _particleColor1[3];
			this.globalParticle.particleColor2[0] = _particleColor2[0];
			this.globalParticle.particleColor2[1] = _particleColor2[1];
			this.globalParticle.particleColor2[2] = _particleColor2[2];
			this.globalParticle.particleColor2[3] = _particleColor2[3];
			this.globalParticle.particleColorDead[0] = _particleColorDead[0];
			this.globalParticle.particleColorDead[1] = _particleColorDead[1];
			this.globalParticle.particleColorDead[2] = _particleColorDead[2];
			this.globalParticle.particleColorDead[3] = _particleColorDead[3];
			this.globalParticle.particleSizeMin = _particleSizeMin;
			this.globalParticle.particleSizeMax = _particleSizeMax;
			this.globalParticle.particleDurerVieMin = _particleDurerVieMin;
			this.globalParticle.particleDurerVieMax = _particleDurerVieMax;
			this.globalParticle.particleDentity = _particleDentity;
			this.globalParticle.particleGravity[0] = _particleGravity[0];
			this.globalParticle.particleGravity[1] = _particleGravity[1];
			this.globalParticle.particleGravity[2] = _particleGravity[2];
			this.globalParticle.particleDirection1[0] = _particleDirection1[0];
			this.globalParticle.particleDirection1[1] = _particleDirection1[1];
			this.globalParticle.particleDirection1[2] = _particleDirection1[2];
			this.globalParticle.particleDirection2[0] = _particleDirection2[0];
			this.globalParticle.particleDirection2[1] = _particleDirection2[1];
			this.globalParticle.particleDirection2[2] = _particleDirection2[2];
			this.globalParticle.particleRotation = _particleRotation;
			this.globalParticle.vieParticleStop = _vieParticleStop;
			this.globalParticle.manualEmitCount = _manualEmitCount;
			this.globalParticle.vieParticleDuration = _vieParticleDuration;
			this.globalParticle.particleEmitPowerMin = _particleEmitPowerMin;
			this.globalParticle.particleEmitPowerMax = _particleEmitPowerMax;
			this.globalParticle.particleUpdateSpeed = _particleUpdateSpeed;
			this.globalParticle.particleEffect = _particleEffect;
			
			this.createParticule(this.scene, emiterMesh);
		});	
	}

	createParticule(scene, emiterMesh)
	{
		this.effect = scene.getEngine().createEffectForParticles("myEffect", ["time"]);
		
		this.globalParticle.particleSystem = new BABYLON.ParticleSystem("systeme_particles_"+this.globalParticle.selectionEmiter, 4000, scene, this.effect);
		this.globalParticle.particleSystem.emitter = emiterMesh;
		
		this.globalParticle.particleSystem.particleTexture = new BABYLON.Texture(this.path+"images/Flares/"+this.globalParticle.imageParticles+"", scene);
		this.globalParticle.particleSystem.minEmitBox = new BABYLON.Vector3(this.globalParticle.minEmitBox[0], this.globalParticle.minEmitBox[1], this.globalParticle.minEmitBox[2]);
		this.globalParticle.particleSystem.maxEmitBox = new BABYLON.Vector3(this.globalParticle.maxEmitBox[0], this.globalParticle.maxEmitBox[1], this.globalParticle.maxEmitBox[2]);

		// Colors of all particles
		this.globalParticle.particleSystem.textureMask = new BABYLON.Color4(this.globalParticle.colorMask[0], this.globalParticle.colorMask[1], this.globalParticle.colorMask[2], this.globalParticle.colorMask[3]);
		this.globalParticle.particleSystem.color1 = new BABYLON.Color4(this.globalParticle.particleColor1[0], this.globalParticle.particleColor1[1], this.globalParticle.particleColor1[2], this.globalParticle.particleColor1[3]);
		this.globalParticle.particleSystem.color2 = new BABYLON.Color4(this.globalParticle.particleColor2[0], this.globalParticle.particleColor2[1], this.globalParticle.particleColor2[2], this.globalParticle.particleColor2[3]);
		this.globalParticle.particleSystem.colorDead = new BABYLON.Color4(this.globalParticle.particleColorDead[0], this.globalParticle.particleColorDead[1], this.globalParticle.particleColorDead[2], this.globalParticle.particleColorDead[3]);

		// Size of each particle (random between...
		this.globalParticle.particleSystem.minSize = this.globalParticle.particleSizeMin;
		this.globalParticle.particleSystem.maxSize = this.globalParticle.particleSizeMax;

		// Life time of each particle (random between...
		this.globalParticle.particleSystem.minLifeTime = this.globalParticle.particleDurerVieMin;
		this.globalParticle.particleSystem.maxLifeTime = this.globalParticle.particleDurerVieMax;

		if(this.globalParticle.depthWrite == "true") {
			this.globalParticle.particleSystem.forceDepthWrite = true;
		} else { 
			this.globalParticle.particleSystem.forceDepthWrite = false;
		}

		// Emission rate
		this.globalParticle.particleSystem.emitRate = this.globalParticle.particleDentity;
		
		// Blend mode : BLENDMODE_ONEONE, or BLENDMODE_STANDARD
		if(this.globalParticle.mode == "one") this.globalParticle.particleSystem.blendMode = BABYLON.ParticleSystem.BLENDMODE_ONEONE;
		else { this.globalParticle.particleSystem.blendMode = BABYLON.ParticleSystem.BLENDMODE_STANDARD; }

		// Set the gravity of all particles
		this.globalParticle.particleSystem.gravity = new BABYLON.Vector3(this.globalParticle.particleGravity[0], this.globalParticle.particleGravity[1], this.globalParticle.particleGravity[2]);

		// Direction of each particle after it has been emitted
		this.globalParticle.particleSystem.direction1 = new BABYLON.Vector3(this.globalParticle.particleDirection1[0], this.globalParticle.particleDirection1[1], this.globalParticle.particleDirection1[2]);
		this.globalParticle.particleSystem.direction2 = new BABYLON.Vector3(this.globalParticle.particleDirection2[0], this.globalParticle.particleDirection2[1], this.globalParticle.particleDirection2[2]);

		// Angular speed, in radians
		if(this.globalParticle.particleRotation == true) {
		   this.globalParticle.particleSystem.minAngularSpeed = 0;
		   this.globalParticle.particleSystem.maxAngularSpeed = Math.PI;
		}

		// Speed
		this.globalParticle.particleSystem.minEmitPower = this.globalParticle.particleEmitPowerMin;
		this.globalParticle.particleSystem.maxEmitPower = this.globalParticle.particleEmitPowerMax;
		this.globalParticle.particleSystem.updateSpeed = this.globalParticle.particleUpdateSpeed;

		// Start the particle system
		if(this.globalParticle.vieParticleStop == true) {
			this.globalParticle.particleSystem.targetStopDuration = this.globalParticle.vieParticleDuration; 
			this.globalParticle.particleSystem.disposeOnStop = true;
		}
		
		if(this.globalParticle.manualEmitCount > 0) {
			this.globalParticle.particleSystem.manualEmitCount = this.globalParticle.manualEmitCount;
		}
		
		this.globalParticle.particleSystem.start();	

		if(this.globalParticle.particleEffect == false) {
			this.globalParticle.particleSystem._customEffect = null;			
		}
	}
	
	deleteParticule()
	{		
		this.globalParticle.particleSystem.dispose();
	}

}