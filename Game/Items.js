class Items {

	constructor()
	{
		this.path = SceneManager.getFolderPath();
		this.itemClicked = null;
		this.typeItem = null;			
		game.dataJson(this.path+"game data/items.json", "items");
		this.items = [];
		this.countItems = Object.keys(game.dataItems.items).length;
		this.getListeItemsToSelect();
	}
	
	getIndexItemsByName(nameitem)
	{			
		for(let i = 0; i < this.countItems; i++) {
			if(game.dataItems.items[i]["name"] == nameitem) {					
				return i;
			}
		}		
	}
		
	getListeItemsToSelect()
	{			
		for(let i = 0; i < this.countItems; i++) {			
			this.items[i] = game.scene.getMeshByName(game.dataItems.items.name) || null;			
			if(this.items[i]) {			
				this.items[i].actionManager = new BABYLON.ActionManager(game.scene);
				this.items[i].actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickDownTrigger,
					(e) => {
						let mesh = e.meshUnderPointer;	
						let item = game.dataItems[this.getIndexItemsByName(game.dataItems.items.name)];					
						game.script.executeScript(no_extention(item.script));					
						if(item.range == "None" || item.range == "None" || item.type == "Potion" || item.type == "Image" || item.type == "Arme" || item.type == "Armure" && item.type != "Nouriture" && item.type != "Argent") {					
							if(this._verifyInventoryIsFull() == false) {
								if(this._verifyItemsExistOnInventory(item.name) == false || (this._verifyItemsExistOnInventory(item.name) == true && stringToBoolean(item.canBeStacked) == false)) {							
									this.addToInventory(false, mesh, item);
								} else if(this._verifyItemsExistOnInventory(item.name) == true && stringToBoolean(item.canBeStacked) == true) {
									this.addToInventory(true, mesh, item);
								}
							}				
						}
						else if(item.range != "None" && item.type != "Nouriture" && item.type != "Argent") {									
							this.addToBone(mesh, item.range);
						}
						else if(item.type == "Argent" && item.type != "Nouriture") {					
							this.addToBanque(item.value, mesh);
						}
						else if(item.type == "Nouriture" && item.type != "Argent") {
							this.addToPlayer(item.value, mesh);
						} else {
							return;
						}
					}
				));
			}
		}		
	}
	
	addToInventory(isCanBeStacked, mesh, itemData)
	{
		if(isCanBeStacked == true) { // on incremente l'objet selement, on ne l'ajoute pas en tant que nouvelle item
			
		} else {			
			let countSlot = interfaces.inventory.divEmptySlot.length;		
			for(let index = 0; index < countSlot; index++) {
				if(interfaces.inventory.divEmptySlot[countSlot].style.background == "transparent") {
					// add item here
					let item = document.createElement("img");
					item.src = itemData.icone;
					item.style.width = "34px";
					item.style.height = "34px";
					item.id = itemData.name;
					interfaces.inventory.divEmptySlot[countSlot].style.background = "none";
					interfaces.inventory.divEmptySlot[countSlot].appendChild(item);					
					break;
				}	
			}			
		}
		mesh.dispose();
	}
		
	addToBanque(sommes, mesh)
	{		
		ACTOR.SetMoney(mesh, sommes);
		mesh.dispose();
	}
	
	addToPlayer(value, mesh)
	{
		let health = ACTOR.GetAttribute(mesh, "Health") + value;
		if(health > 100) health = 100;
		ACTOR.SetAttribute(mesh, "Health", health);		
		mesh.dispose();
	}
	
	addToBone(mesh, boneName)
	{
		//meshToAtttach.detachFromBone();
		//game.character.skeleton.bones[game.character.skeleton.getBoneIndexByName(boneName)] = null;
		let meshToAtttach = mesh.clone(meshToAtttach.name);
		mesh.dispose(); // supprime l'objet trouver apres l'avoir cloner
		let actor = game.character.actor;
		let skeleton = actor.skeleton;
		// on deattache ce qui ce trouve sur l'os sur le quelle on veux attacher un nouvelle objet, si il y en a un.	
		if(skeleton.bones[skeleton.getBoneIndexByName(boneName)] != null || skeleton.bones[skeleton.getBoneIndexByName(boneName)] != undefined) {
			//meshToAtttach.detachFromBone();
			skeleton.bones[skeleton.getBoneIndexByName(boneName)] = null;
		}
		//Et enfin on attache l'objet cloner sur un os du personnage
		meshToAtttach.attachToBone(skeleton.bones[skeleton.getBoneIndexByName(boneName)], actor);		
	}
		
	exlusivityRace(item)
	{
		if(item.exclusivRace == game.userActor.race || item.exclusivRace == "all") { return true; }
		else { return false; }
	}
	
	exlusivityClasse(item)
	{
		if(item.exclusivClasse == game.userActor.classe || item.exclusivClasse == "all") { return true; }
		else { return false; }
	}
	
	//****** Function private
	
	_verifyItemsExistOnInventory(itemsName)
	{		
		let countSlot = interfaces.inventory.divEmptySlot.length;		
		for(let index = 0; index < countSlot; index++)
		{
			if(interfaces.inventory.divEmptySlot[countSlot].id == itemsName)
			{
				return true;// Si l'objet exit dans l'inventaire on retour true
			}	
		}		
		return false; 
	}
	
	_verifyInventoryIsFull()
	{
		let slotVide = 0;
		let countSlot = interfaces.inventory.divEmptySlot.length;		
		for(let index = 0; index < countSlot; index++)
		{
			if(interfaces.inventory.divEmptySlot[countSlot].style.background == "transparent")
			{
				slotVide++;
			}	
		}
		if(slotVide < countSlot) {
			return false;
		} else {
			return true;// Si l'inventaire est plein (voir le poids et le nombre de slot) return true;
		} 
	}
	
	_scriptItem()
	{
		
	}	

}