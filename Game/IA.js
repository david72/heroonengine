class IA {

	constructor(scene)
	{
		this.scene = scene;
		this.pathPoint = [];
		this.dataPNJ = [];
		this.currentPathPoint = 0;
		// recupere la liste de chaque chemin et de chaque points dans chaque chemin
		// recupere aussi la liste des PNJ pour chaque chemin
		for(let i = 0, count = game.spanpoint.length; i < count; i++) {
			this.pathPoint[i] = [];
			this.actorPNJ[i] = [];
			this.dataPNJ[i].push(game.spanpoint[i].metadata);
			for(let p = 0; p < game.spanpoint[i].metadata.arrayPoints.length; p++) {
				this.pathPoint[i].push(game.spanpoint[i].metadata.arrayPoints[p]);
			}
		}		
	}

	updateRenderer()
	{				
		this.scene.registerBeforeRender(() => {
			this.pathPNJ();
		});
	}

	//######## PLayer
	PlayerGoPNJ(positionPNJ)
	{
		// Le joueur suivras le PNJ si il demande a l'attaquer
		// Il jouera l'aniation de marche jusqu'a lui et l'attaque des qu'il est a 1 m de lui
		
	}

	PlayerGoAttaque()
	{
		//Executer l'animation d'attaque ici et retirer de la santer des qu'il y a hit au PNJ
		
	}

	positionRand(pnj, rayonWaypoint)
	{
		let random = Math.floor((Math.random() * rayonWaypoint));
		let randBool = parseInt(Math.random() * 2) ?  true : false;
		if(randBool == true) {
			let randBool = parseInt(Math.random() * 2) ?  true : false;
			if(randBool == true) {
				pnj.position.x = this.pathPoint[i][0].x + random;
				pnj.position.z = this.pathPoint[i][0].z - random;
			} else {
				pnj.position.x = this.pathPoint[i][0].x - random;
				pnj.position.z = this.pathPoint[i][0].z + random;
			}
		} else {
			let randBool = parseInt(Math.random() * 2) ?  true : false;
			if(randBool == true) {
				pnj.position.x = this.pathPoint[i][0].x - random;
				pnj.position.z = this.pathPoint[i][0].z + random;
			} else {
				pnj.position.x = this.pathPoint[i][0].x + random;
				pnj.position.z = this.pathPoint[i][0].z - random;
			}
		}
	}

	//######## PNJ
	startPNJ()
	{
		for(let i = 0, count = game.PNJ.length; i < count; i++) {
			let pnj = game.PNJ[i].clone(game.PNJ[i].name+game.PNJ[i].uniqueID);
			pnj.position = new BABYLON.Vector3(this.pathPoint[i][0].x, this.pathPoint[i][0].y, this.pathPoint[i][0].z);
			this.positionRand(pnj, this.actorPNJ[i].rayonWaypoint);
			game.script.executeScript(this.dataPNJ[i].waypointScript);
			this.updateRenderer();
		}
	}

	PNJGoAttaque(pnj)
	{
		//Executer le script de combat ici		
		setTimeout(() => {
			game.script.executeScript(game.dataGlobal.formule_combat);
			this.PNJGoAttaque();
		}, 3000);
	}

	deathPNJ(pnj, numPath)
	{
		// le PNJ meur, il disparait de la scene pour réaparaitre au bout de x seconde au drapeau
		// Il executera un script de resuration si il exist
		game.script.executeScript(this.dataPNJ[numPath].deathScript);
		pnj.isVisible = false;
		pnj.position = this.pathPoint[numPath][0];
		setTimeout(function() {
			pnj.isVisible = true;
			this.positionRand(pnj, this.actorPNJ[numPath].rayonWaypoint);
		}, parseInt(this.dataPNJ[numPath].waypointDelai) * 1000);
	}

	pathPNJ()
	{
		// Le PNJ ce deplace le long d'un chemin et verifie la distance avec le joueurs et suivant sa faction, il attaquera en courant vers lui, le fuiras dans le sens inversse de son chemin ou sera neutre et continuras sa route.
		for(let i = 0; i < game.PNJ.length; i++) {
			let positionPath = new BABYLON.Vector3(this.pathPoint[i][this.currentPathPoint].x, this.pathPoint[i][this.currentPathPoint].y, this.pathPoint[i][this.currentPathPoint].z);// position du point a atteindre
			let distanceOfPoint = BABYLON.Distance(game.PNJ[i].position, positionPath); // distance entre le PNJ et le point a atteindre
			let distanceOfPlayer = BABYLON.Distance(game.character.actor.position, game.PNJ[i].position); // distance entre le PNJ et le joueur
			if(distanceOfPoint > 0) { //Si la distance est superieur a 0
				let forward = null;
				//let numFaction = game.getFactionActor(game.PNJ[i].metadata.faction);
				if(distanceOfPlayer <= game.dataActor.range) { //Si le PNJ est a une distance x du joueur et que ca faction le permet il change sont chemin pour attaquer le joueurs
					game.PNJ[i].lookAt = game.character.actor.position; // Le PNJ regarde vers le joueur
					forward = new BABYLON.Vector3(Math.sin(parseFloat(game.PNJ[i].rotation.y)) / 7, 0.075, Math.cos(parseFloat(game.PNJ[i].rotation.y)) / 7);	// direction du deplacement du PNJ
					if(distanceOfPlayer) { // si le PNJ et a bonne distance du joeurs on lance l'arraque
						this.PNJGoAttaque();
						this.PlayerGoAttaque();
						this.MountGoAttaque();// seulement si on a une monture
					} else { // sinon on continue le deplacement
						game.PNJ[i].moveWithCollisions(forward); //forward.negate() // Le PNJ ce deplace endirection du point ou il regarde
					}
				} else {
					game.PNJ[i].lookAt = positionPath; // Le PNJ regarde vers le point
					forward = new BABYLON.Vector3(Math.sin(parseFloat(game.PNJ[i].rotation.y)) / 15, 0.075, Math.cos(parseFloat(game.PNJ[i].rotation.y)) / 15);	// direction du deplacement du PNJ
					game.PNJ[i].moveWithCollisions(forward); //forward.negate() // Le PNJ ce deplace endirection du point ou il regarde
				}

			} else { //SI la distance est égale a 0 on increment le point suivant a atteindre
				if(this.currentPathPoint == this.pathPoint[i].length) { // SI on a atteind le dernier point du chemin, on le remet a 0 pour crée rune boucle au chemin
					this.currentPathPoint = 0;
				} else { // Sinon on increment le point courent a atteindre.
					this.currentPathPoint += 1;
				}
			}
		}
	}

	//######## Monture
	MountGoPNJ()
	{

	}

	MountGoAttaque()
	{

	}

	PlayerCallMount()
	{

	}

	MountRejoinOrFollowPlayer()
	{

	}

}