class Sounds {

	constructor(scene)
	{
		this.scene = scene;
		this.music = null;
		if(getComptibleFormatAudio().ogg) {
			this.soundCompatible = "ogg";
		} else if(getComptibleFormatAudio().mp3) {
			this.soundCompatible = "mp3";
		} else if(getComptibleFormatAudio().wav) {
			this.soundCompatible = "wav";
		}
		this.soundSpacial = [];
		this.sound = null;
		this.withCallback = null;
	}

	loadMusicOfZone(music)
	{
		let musicZone = null;
		if(this.soundCompatible == "ogg") musicZone = music.ogg;
		else if(this.soundCompatible == "mp3") musicZone = music.mp3;
		else if(this.soundCompatible == "wav") musicZone = music.wav;	
		if(musicZone != "None" && musicZone != undefined) {
			this.music = new BABYLON.Sound("musicZone", musicZone, this.scene, null, { volume: 0.5, loop: true, autoplay: true, streaming: true, spatialSound: false });
		}
	}
	
	loadSimpleSound(sound)
	{
		let mySound = null;
		if(this.soundCompatible == "ogg") mySound = sound.ogg;
		else if(this.soundCompatible == "mp3") mySound = sound.mp3;
		else if(this.soundCompatible == "wav") mySound = sound.wav;
		if(mySound && mySound != undefined) {
			this.sound = new BABYLON.Sound("sound", mySound, this.scene, null, { volume: 1.0, loop: false, autoplay: true, streaming: true, spatialSound: false });	
		}
	}
	
	loadSpacialSound(name, sound, distance, position)
	{
		let countSoundSpacial = this.soundSpacial.length;
		let mySound = null;
		if(this.soundCompatible == "ogg") mySound = sound.ogg;
		else if(this.soundCompatible == "mp3") mySound = sound.mp3;
		else if(this.soundCompatible == "wav") mySound = sound.wav;
		if(mySound && mySound != undefined) {
			this.soundSpacial[countSoundSpacial] = new BABYLON.Sound("sound_"+name, mySound, this.scene, null, { volume: 1.0, loop: true, autoplay: true, streaming: true, spatialSound: true, maxDistance: distance });
			this.soundSpacial[countSoundSpacial].setPosition(new BABYLON.Vector3(parseFloat(position.x), parseFloat(position.y), parseFloat(position.z)));
		}
	}
	
	loadSoundAfterClick(sound, attachToMesh, key)
	{		
		let mySound = null;
		if(this.soundCompatible == "ogg") mySound = sound.ogg;
		else if(this.soundCompatible == "mp3") mySound = sound.mp3;
		else if(this.soundCompatible == "wav") mySound = sound.wav;
		if(mySound && mySound != undefined) {
			if(attachToMesh == undefined) attachToMesh = null;	
			this.shot = new BABYLON.Sound("shot", mySound, this.scene, null, { volume: 0.8, loop: false, autoplay: false, streaming: true, spatialSound: true });
			if(attachToMesh) {
				this.shot.attachToMesh(attachToMesh);
			}
			window.addEventListener("mousedown", function (evt) {
				if (evt.button === 0) {
					gunshot.play();
				}
			});
			if(key) {
				window.addEventListener("keydown", function (evt) {
					if (evt.keyCode === key) {
						gunshot.play();
					}
				});
			}
		}
	}
	
	loadSoundWithCallback(sound, volume, loop, attachToMesh, callback)
	{		
		let mySound = null;
		if(this.soundCompatible == "ogg") mySound = sound.ogg;
		else if(this.soundCompatible == "mp3") mySound = sound.mp3;
		else if(this.soundCompatible == "wav") mySound = sound.wav;
		if(mySound && mySound != undefined) {
			if(callback == undefined) callback = null;
			if(volume == undefined) volume = 0.9;
			if(loop == undefined) loop = true;
			if(attachToMesh == undefined) attachToMesh = null;
			this.withCallback = new BABYLON.Sound("sound", mySound, this.scene, callback, {volume: volume, loop: loop, streaming: true, autoplay: false, spatialSound: true});
			if(attachToMesh) {
				this.withCallback.attachToMesh(attachToMesh);
			}
			this.withCallback.play();	
		}
	}	
}