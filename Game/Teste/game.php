<?php
session_start();
error_reporting(0);
?>
<!DOCTYPE HTML>
<html xmlns="http://wwww.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<title>Game</title>
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<link rel="shortcut icon" href="../../Editor/Heroon Engine/Data Project/Default/Game.ico">	
	<link rel="stylesheet" type="text/css" href="../../Editor/Heroon Engine/Data Project/Default/game.css?<?php echo time();?>">	
	<script src="../../Editor/Heroon Engine/Data Project/Default/Engines/jquery.min.js"></script>
	<script src="https://preview.babylonjs.com/cannon.js?<?php echo time();?>"></script>
	<script src="https://preview.babylonjs.com/babylon.js?<?php echo time();?>"></script>	
	<script src="https://preview.babylonjs.com/materialsLibrary/babylon.waterMaterial.min.js?<?php echo time();?>"></script>
	<script src="https://preview.babylonjs.com/materialsLibrary/babylon.customMaterial.min.js?<?php echo time();?>"></script>
	<script src="https://preview.babylonjs.com/gui/babylon.gui.min.js?<?php echo time();?>"></script>
	<script src="http://cdn.rawgit.com/NasimiAsl/Extensions/master/ShaderBuilder/Babylonx.ShaderBuilder.js"></script>
	<script src="../../Editor/Heroon Engine/Data Project/Default/Engines/scripting.min.js?<?php echo time();?>"></script>
	<script src="../../Editor/Heroon Engine/Data Project/Default/Engines/SDK.min.js?<?php echo time();?>"></script>	
	<script>
	SceneManager.setFolderPath("../../Editor/Heroon Engine/Data Project/Default/");
	</script>
	<script src="../Utiles.js?<?php echo time();?>"></script>
	<script src="../SceneMain.js?<?php echo time();?>"></script>
	<script src="../Player.js?<?php echo time();?>"></script>
	<script src="../NPC.js?<?php echo time();?>"></script>
	<script src="../Teleportation.js?<?php echo time();?>"></script>
	<script src="../Environement.js?<?php echo time();?>"></script>
	<script src="../Reseau.js?<?php echo time();?>"></script>
	<script src="../Sounds.js?<?php echo time();?>"></script>
	<script src="../ExecScripts.js?<?php echo time();?>"></script>
	<script src="../IA.js?<?php echo time();?>"></script>
	<script src="../Items.js?<?php echo time();?>"></script>
	<script src="../Particles.js?<?php echo time();?>"></script>	
	<script src="../Competances.js?<?php echo time();?>"></script>
	<script src="../Options.js?<?php echo time();?>"></script>
	<?php
	if(@$_GET['zone']) $zoneName = @$_GET['zone'];
	else { $zoneName = "Delos";	}
	
	$dirnameScript = '../../Editor/Heroon Engine/_Projects/Teste/scripts/'.$zoneName.'/';
	$dirScript = @opendir($dirnameScript); 
	while($file = @readdir($dirScript)) {
		if($file != '.' && $file != '..' && !is_dir($dirnameScript.$file))
		{ 
			$extention = pathinfo($dirnameScript.$file, PATHINFO_EXTENSION); 
			if($extention == "js") echo '<script src="'.$dirnameScript.$file.'" ></script>';
		}
	}    
	@closedir($dirScript);		
	?>
</head>
<body>
	<div id="ContentScene" style="width:100%;height:100%;"></div>
	<script>
	var user = "<?php echo $_SESSION['pseudo'];?>";	
	var nameActor = "<?php echo $_GET['nameActor'];?>";	
	// Scene 3D
	var game = new SceneMain("<?php echo $zoneName;?>");	
	game.createScene();	
	
	var serveur = new Reseau(game.scene);
	serveur.connectToServeur();// Connexion au serveur
	serveur.newClient(user); // Nouveau client connecter
	serveur.recieveMessage(); // Ecoute les messages du serveur
	
	//GUI
	var interfaces = {};
	
	if(GameMenu.getCookie("HEGame-Lang") == null) {
		GameMenu.setCookie("HEGame-Lang", "French");
	}
	var menuGame = new GameMenu();
	
	var sceneManager = new SceneManager(game.scene);

	var minimap = new PlayerMap(game.cameraMiniMap, game.scene, game.engine);
	minimap.createElement();

	var actionBar = new ActionBar();
	actionBar.createElement();

	var compass = new PlayerCompass();
	compass.createElement();
	compass.rotateCompass(game.cameraPlayer, game.scene);

	var statutBar = new PlayerHealtBars();
	statutBar.createElementForPlayer();
	statutBar.createElementForEnemy();
	statutBar.visibility("enemy", false);

	// For the window of actionbar
	interfaces.inventory = new PlayerInventory(null, game.scene);
	interfaces.inventory.createElement();

	interfaces.chat = new ChatBox();
	interfaces.chat.createElement();
	interfaces.chat.getMessages();

	interfaces.map = new Map();
	interfaces.map.createElement();

	interfaces.help = new PlayerHelp();
	interfaces.help.createElement();

	interfaces.character = new Player();
	interfaces.character.createElement();

	interfaces.party = new PlayerParty();
	interfaces.party.createElement();

	interfaces.abilities = new PlayerStats();
	interfaces.abilities.createElement();

	interfaces.quests = new PlayerQuest();
	interfaces.quests.createElement();
	</script>
</body>
</html>