<?php
session_start();
if($_SESSION['pseudo'] == false) {
	header("location: index.php");
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>CharacterSet</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<link rel="shortcut icon" href="../../Editor/Heroon Engine/Data Project/Default/Game.ico">	
	<link rel="stylesheet" type="text/css" href="../../Editor/Heroon Engine/Data Project/Default/game.css?<?php echo time();?>">	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js" type="text/javascript"></script>
	<script src="https://preview.babylonjs.com/babylon.js?<?php echo time();?>" type="text/javascript"></script>
	<script src="../../Editor/Heroon Engine/Data Project/Default/Engines/SDK.min.js?<?php echo time();?>" type="text/javascript"></script>
	<script>
	SceneManager.setFolderPath("../../Editor/Heroon Engine/_Projects/Teste/");
	</script>	
	<script src="../utiles.js?<?php echo time();?>" type="text/javascript"></script>	
</head>
<body>
	<div id="ContentCharacterSet" style="width:100%;height:100%;"></div>
	<script>
	var user = "<?php echo $_SESSION['pseudo'];?>";
	if(GameMenu.getCookie("HEGame-Lang") == null) {
		GameMenu.setCookie("HEGame-Lang", "French");
	}	
	
	var characterSet = new PlayerSelector("characterSet", user);
	characterSet.createScene();
	characterSet.createElements();
	</script>
</body>
</html>