<?php
$idPseudo = $_POST['id'];
$pseudo = $_POST['user'];
$animType = $_POST['animType'];
$nameActeur = $_POST['name'];
$race = $_POST['race'];
$genre = $_POST['genre'];
$class = $_POST['class'];
$head = $_POST['head'];
$hair = $_POST['hair'];
$hairColor = $_POST['hairColor'];
$barbe = $_POST['beard'];
$beardColor = $_POST['beardColor'];
$vetement = $_POST['clothes'];
$attributeListe = $_POST['attributeListe'];
$valueAttributeListe = $_POST['valueAttributeListe'];
$root = $_POST['root'];
$file = $root."game data/users.json";
$json = json_decode(file_get_contents($file), true);
$countActors = count($json["users"][$idPseudo]["actors"]);
$json["users"][$idPseudo]["actors"][$countActors]["animation"] = $animType;
$json["users"][$idPseudo]["actors"][$countActors]["name"] = $nameActeur;
$json["users"][$idPseudo]["actors"][$countActors]["race"] = $race;
$json["users"][$idPseudo]["actors"][$countActors]["genre"] = $genre;
$json["users"][$idPseudo]["actors"][$countActors]["classe"] = $class;
$json["users"][$idPseudo]["actors"][$countActors]["hair"]["mesh"] = $hair;
$json["users"][$idPseudo]["actors"][$countActors]["hair"]["color"] = $hairColor;
$json["users"][$idPseudo]["actors"][$countActors]["beard"]["mesh"] = $barbe;
$json["users"][$idPseudo]["actors"][$countActors]["beard"]["color"] = $beardColor;
$json["users"][$idPseudo]["actors"][$countActors]["head"] = $head;
$json["users"][$idPseudo]["actors"][$countActors]["clothes"] = $vetement;
$liste = explode(";", $attributeListe);
$value = explode(";", $valueAttributeListe);
$i = 0;
for($i = 0; $i < count($liste); $i++) {
	$json["users"][$idPseudo]["actors"][$countActors]["attribute"][$liste[$i]] = $value[$i];	
}
$json["users"][$idPseudo]["info"][$countActors]["bank"]["money1"] = 0;
$json["users"][$idPseudo]["info"][$countActors]["bank"]["money2"] = 0;
@chmod($file, 0777);
$save = json_encode($json, JSON_PRETTY_PRINT | JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE);
@file_put_contents($file, $save);
?>