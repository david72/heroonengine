class Player {

	constructor(scene)
	{
		this.path = SceneManager.getFolderPath();
		this.scene = scene;
		this.actor = null;
		this.cameraControler = null;
		this.HighlightHitCurrent = null;
		this.teleportation = null;
		this.ActorHighlight = null;
		this.content = document.body;

		game.getActorPLayer(user, nameActor);
		if(game.userActor.race == null || game.userActor.classe == null) {
			game.userActor.race = "race";
			game.userActor.classe = "classe";
		}
		game.dataJson(this.path+"game data/actors/"+game.userActor.race+"."+game.userActor.classe+".json?" + (new Date()).getTime(), "actor");
		let fileMeshActor = game.dataActor.apparence.mesh_body;
		fileMeshActor = fileMeshActor.replace("_Projects/Teste/meshes/actors/", "");
		this.root = getPath(fileMeshActor);
		this.sceneFilename = getFile(fileMeshActor)+"?" + (new Date()).getTime();
		this.portail = new Teleportation(scene);
	}

	createCharacter(rootFile, meshFile)
	{
		let root = null, file = null;
		if(rootFile && meshFile) {
			root = rootFile;
			file = meshFile;
		} else {
			root = this.path+"meshes/actors/"+this.root;
			file = this.sceneFilename;
		}
		BABYLON.SceneLoader.ImportMesh("", root, file, this.scene, (newMesh) => {
			let mesh = newMesh[0];
			mesh.rotationQuaternion = null;
			mesh.updatePoseMatrix(BABYLON.Matrix.Identity());
			mesh.rotation.y = -3.14;
			mesh.name = user;
			if(GET("go")) {
				mesh.position = new BABYLON.Vector3(_GET("go").x, _GET("go").y, _GET("go").z);
			} else {
				mesh.position = this.portail.GoTo("Start") || new BABYLON.Vector3(0, 1.0, 0);
			}
			mesh.position.y -= 2;
			mesh.material.emissiveColor = new BABYLON.Color3(1, 1, 1);
			mesh.checkCollisions = true;
			mesh.ellipsoid = new BABYLON.Vector3(1.0, 1.0, 1.0);
			mesh.ellipsoidOffset = new BABYLON.Vector3(0, 1.995, 0);
			mesh.applyGravity = true;
			mesh.isPickable = true;
			mesh.metadata = {"type": "player"};

			if(mesh.metadata.bank == null) {
				mesh.metadata.bank = {};
				mesh.metadata.bank.monaie1 = game.dataGeneral.monaie_start;
				mesh.metadata.bank.monaie2 = 0;
				game.infoActor.bank.money1 = game.dataGeneral.monaie_start;
				game.infoActor.bank.money2 = 0;
			}
			for(let i = 0; i < newMesh.length; i++) {
				newMesh[i].receiveShadows = false;
			}

			if(stringToBoolean(game.dataEnvironnement.shadow.enabledShadow) && game.shadowMap) {
				game.shadowMap.renderList.push(mesh);
			}

			if(stringToBoolean(game.dataEnvironnement.Octree)) {
				mesh.useOctreeForCollisions = true;
				game.octree = this.scene.createOrUpdateSelectionOctree();
				game.octree.dynamicContent.push(mesh);
			}

			// Action sur les Joueurs
			mesh.actionManager = new BABYLON.ActionManager(this.scene);
			mesh.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickDownTrigger, (e) => { this.createSelectActor(e.meshUnderPointer); }, null));

			game.createEtiqueteName(mesh, user);
			this.createHighlightLayer(mesh);

			/*
			if(stringToBoolean(game.dataGeneral.collision_actor) == false){
				mesh.collisionMask = game.getNewMask();
			}
			*/

			game.cameraPlayer.setPosition(new BABYLON.Vector3(mesh.position.x, mesh.position.y+1.7,mesh.position.z -4));
			game.cameraPlayer.setTarget(new BABYLON.Vector3(mesh.position.x, mesh.position.y+1.7, mesh.position.z));
			game.cameraPlayer.alpha = 4.71;
			game.cameraPlayer.beta =  Math.PI/2;
			game.cameraPlayer.radius = 4.5;

			this.cameraControler = new CameraController(game.cameraPlayer, game.cameraMiniMap, mesh, user, this.scene);

			window.addEventListener("keydown", this.cameraControler.controlKeyDown.bind(null, this.cameraControler), false);
			window.addEventListener("keyup", this.cameraControler.controlKeyUp.bind(null, this.cameraControler), false);

			if(serveur.socket) {
				serveur.sendMessage('nouveau_client', nameActor);
				serveur.sendMessage('new_actor', {rootMesh: this.path+"meshes/actors/"+this.root, fileMesh: this.sceneFilename});
			}

			this.actor = mesh;
		});

		this.countTriggers = game.triggers.length;
		this.countPortails = game.portails.length;
		this.scene.registerBeforeRender(() => {
			this.BeforeRender();
		});

	}

	BeforeRender() {
		// Camera
		if(this.actor && this.cameraControler) {
			let meshHit = game.castRay(this.actor, 20, false);
			this.cameraControler.moveActor();
			if(this.HaloOfSelected) {
				let y = game.terrain.getHeightAtCoordinates(this.actor.position.x, this.actor.position.z);
				this.HaloOfSelected.position.x = this.actor.position.x;
				this.HaloOfSelected.position.y = (y+0.033);
				this.HaloOfSelected.position.z = this.actor.position.z;
			}
			//Highlight
			if(meshHit) {
				this.HighlightHitCurrent = this.scene.getHighlightLayerByName("hl_"+meshHit.name) || null;
				if(this.HighlightHitCurrent) {
					this.HighlightHitCurrent.isEnabled = true;
				}
			} else {
				if(this.HighlightHitCurrent) {
					this.HighlightHitCurrent.isEnabled = false;
				}
			}
			// Triggers enter
			for(let i=0; i < this.countTriggers; i++) {
				let triggerBox = game.triggers[i];
				if (this.actor.intersectsMesh(triggerBox, false)) {
					game.script.executeScript(no_extention(triggerBox.metadata.scriptName));
				}
			}
			// Portails
			for(let i=0; i < this.countPortails; i++) {
				let portailBox = game.portails[i];
				if (this.actor.intersectsMesh(portailBox, false)) {
					this.portail.GoTo(portailBox.metadata.goToPortailName);
				}
			}
			// Reseau
			if(serveur.socket) {
				game.listeZoneInterest();
				// Si le personnage est en mouvement en position ou rotation ou sont animation jouer on l'envoie au serveur (sinon on envoie rien pour économiser les envoies)
				if(this.actor.position != game.currentPositionPlayer || this.actor.rotation != game.currentRotationPlayer) { // || this.actor.animation != game.currentAnimationPlayer
					game.currentPositionPlayer = this.actor.position;
					game.currentRotationPlayer = this.actor.rotation.y;
					game.currentAnimationPlayer = "walk";
					serveur.sendMessage('moveObjet', {name: this.actor.name, pos: game.currentPositionPlayer, rotY: game.currentRotationPlayer.y, anim: game.currentAnimationPlayer});
				}
			}
		}
	}

	createHighlightLayer(mesh) {
		// Highlight avec couleur rouge ou vert suivant si la zone est PVP ou pas
		let color1 = 0, color2 = 0, color3 = 0;
		if(stringToBoolean(game.dataEnvironnement.PVP)) { // PVP, donc tous enemis
			color1 = 1.0;
			color2 = 0.0;
			color3 = 0.0;
		} else { // Non PVP, donc amis
			color1 = 0.0;
			color2 = 1.0;
			color3 = 0.0;
		}
		this.ActorHighlight = new BABYLON.HighlightLayer("hl_"+mesh.name, this.scene, {camera: game.cameraPlayer});
		this.ActorHighlight.addMesh(mesh, new BABYLON.Color3(parseFloat(color1), parseFloat(color2), parseFloat(color3)));
		this.ActorHighlight.blurHorizontalSize = 0.2;
		this.ActorHighlight.blurVerticalSize = 0.2;
		this.ActorHighlight.isEnabled = false;
	}

	createSelectActor(mesh) {
		if(!this.HaloOfSelected) {
			let y = game.terrain.getHeightAtCoordinates(mesh.position.x, mesh.position.z);
			this.HaloOfSelected = BABYLON.Mesh.CreateDecal("decalHaloPLayer", game.terrain, new BABYLON.Vector3(mesh.position.x, y, mesh.position.z), null, new BABYLON.Vector3(0.75, 0.75, 0.75));
			this.HaloOfSelected.material = game.decalMaterialSelect;
			this.HaloOfSelected.position.y += 0.033;
			this.createDialogActorSelect(this.HaloOfSelected, mesh);
		}
	}

	createDialogActorSelect(selector, actorSelect) {

		let marginTop = 20;
		this.contoleSelectActor = UI.Window({width: "300px", height: "155px", align: BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER, verticalAlign: BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER, top: -100, radius: 1});
		UI.Image("contoleSelectActor", {URLImage: this.path+"skins/Body-alert.png", width: "300px", height: "155px"}, this.contoleSelectActor);
		if(stringToBoolean(game.dataEnvironnement.PVP)) {
			this.contoleSelectActor.height = "205px";
			marginTop = 80;
		} else {
			this.contoleSelectActor.height = "155px";
			marginTop = 20;
		}
		if(stringToBoolean(game.dataEnvironnement.PVP)) {
			UI.Button("pageTop", "ATTAQUER", {width: "280px", height: "50px", left: 10, top: 20}, null, this.contoleSelectActor, null, null, () => {
				let actorToAttack = actorSelect;
				let IA = new IA();
				IA.PlayerGoPNJ();
				this.contoleSelectActor.isVisible = false;
			});
		}
		UI.Button("pageTop", "PROFILE", {width: "280px", height: "50px", left: 10, top: marginTop}, null, this.contoleSelectActor, null, null, () => {
			this.contoleSelectActor.isVisible = false;
		});
		UI.Button("pageTop", "QUITTER", {width: "280px", height: "50px", left: 10, top: (marginTop+60)}, null, this.contoleSelectActor, null, null, () => {
			selector.dispose();
			this.contoleSelectActor.isVisible = false;
		});
	}
}