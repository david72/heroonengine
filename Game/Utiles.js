var getCookie = function(name) {
	if(document.cookie.length == 0) return null;
	let value = "; " + document.cookie;
	let parts = value.split("; " + name + "=");
	if (parts.length == 2) return parts.pop().split(";").shift();
};
var setCookie = function(name, value, days) {
	let expires = "";
	if (days) {
		let date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		let expires = "; expires="+date.toGMTString();
	}
	document.cookie = name+"="+value+expires+"; path=/";
};
var remove_item = function(arr, value){
	for(let b in arr ){ if(arr[b] == value) { arr.splice(b,1); break;} }
	return arr;
};
var basename = function(path, suffix) { // Recupere le nom d'un fichier
	if(path !== '' || path !== null) var b = path.replace(/^.*[\/\\]/g, '');
	if(typeof suffix === 'string' && b.substr(b.length - suffix.length) == suffix) {
		b = b.substr(0, b.length - suffix.length);
	}
	return b;
};
var getFile = function(fullPath) { // recuper le fichier avec l'extention d'un chemin complet
	if(fullPath) {
		let filename = fullPath.replace(/^.*[\\\/]/, '');
		return filename;
	}
};
var getPath = function(fullPath) { // recuper le chemin du fichier
	if(fullPath) {
		let filename = fullPath.replace(/^.*[\\\/]/, '');
		let root = fullPath.replace(filename, "");
		return root;
	}
};
var no_extention = function(path) { // Recupere le fichier sans l'extention
	if(path) {
		let ext = path.split('.').pop();
		let pathNoExtention = path.replace("."+ext, "");
		return pathNoExtention;
	}
};
var extention = function(path) { // Recupere l'extention d'un fichier
	if(path) return path.split('.').pop();
};
var removeArray = function(array, indice)
{
	let index = array.indexOf(indice);
	if(index > -1) {
		array.splice(index, 1);
	}
	return array;
};
var stringToBoolean = function(myString) {
	switch(myString.toLowerCase()) {
		case "true": case "yes": case "1": return true;
		case "false": case "no": case "0": case null: return false;
		default: return Boolean(myString);
	}
};
var file_exists = function(file) { // Verifie l'existance d'un fichier
	$.ajaxSetup({ async: false});
	let retour;
	$.ajax({ url: file, success: function(data){ retour = true;}, error: function(data){ retour = false; }});
	$.ajaxSetup({ async: true});
	return retour;
};
var hexToR = function(h) {return parseInt((cutHex(h)).substring(0,2),16)};
var hexToG = function(h) {return parseInt((cutHex(h)).substring(2,4),16)};
var hexToB = function(h) {return parseInt((cutHex(h)).substring(4,6),16)};
var cutHex = function(h) {return (h.charAt(0)=="#") ? h.substring(1,7):h};
var toHex = function(n) { n = parseInt(n,10); if (isNaN(n)) return "00"; n = Math.max(0,Math.min(n,255)); return "0123456789abcdef".charAt((n-n%16)/16) + "0123456789abcdef".charAt(n%16); };
// Gestion des couleurs (convertion hexa et rgb)
var hexToRgb = function(hex) {
	let c_r = hexToR(hex) / 255,
		c_g = hexToG(hex) / 255,
		c_b = hexToB(hex) / 255;
	return {r: c_r, g: c_g, b: c_b};
};
var rgbToHex = function(R,G,B) {
	return "#" + toHex(R*255)+toHex(G*255)+toHex(B*255);
};
var getComptibleFormatAudio = function() {
	let audio = new Audio();
	let _format = false, _wav = false, _mp3 = false;
	if(audio.canPlayType("audio/ogg") != "") { // ogg
		_ogg = true;
	}
	if(audio.canPlayType("audio/wav") != "") { // wav
		_wav = true;
	}
	if(audio.canPlayType("audio/mpeg") != "") { // mp3
		_mp3 = true;
	}
	return {ogg: _ogg, wav: _wav, mp3: _mp3};
};
var CustomLoadingScreen = function(engine, data) {	
	//value default
	let loadingText = data.loadingText || "Loading...";
	let colorText = data.colorText || "#990000";
	let sizeText = data.sizeText || "16px";	
	let imageLoadingProgress = data.imageLoadingProgress || "images/loading.png";
	let positionTopLoadingProgress = data.imageLoadingProgress || "30%";
	let marginTopLoadingProgress = data.marginTopLoadingProgress || "250px";	
	let imageLogo = data.imageLogo || "images/logo.png";
	let positionTopImageLogo = data.positionTopImageLogo || "100px";
	let marginTopImageLogo = data.marginTopImageLogo || "0px";
	let marginLeftImageLogo = data.marginLeftImageLogo || "-175px";
	
	let loadingText = engine._loadingScreen._loadingTextDiv.parentElement.childNodes[0];
	loadingText.innerHTML = loadingText;
	loadingText.style.color = colorText;
	loadingText.style.fontSize = sizeText;
	loadingText.style.fontWeight = "bold";	
	
	let loadingImage = engine._loadingScreen._loadingTextDiv.parentElement.childNodes[1];
	loadingImage.src = imageLoadingProgress;
	loadingImage.style.top = positionTopLoadingProgress;
	loadingImage.style.marginTop = marginTopLoadingProgress;	
	
	let loadingLogo = engine._loadingScreen._loadingTextDiv.parentElement.childNodes[2];
	loadingLogo.src = imageLogo;
	loadingLogo.style.top = positionTopImageLogo;
	loadingLogo.style.marginTop = marginTopImageLogo;
	loadingLogo.style.marginLeft = marginLeftImageLogo;
};
var GET = function(param) {
	let lets = {};
	window.location.href.replace(location.hash, '').replace(/[?&]+([^=&]+)=?([^&]*)?/gi, function( m, key, value ) { lets[key] = value !== undefined ? value : ''; });
	if (param) {
		return lets[param] ? lets[param] : null;	
	}
	return lets;
};