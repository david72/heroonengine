class VISUAL {
	
	static CreateParticle(fileNameParticleConfig, meshToAttach)
	{
		this.particle = new HEROONGAME.Particles(game.scene);
		this.particle.getParticle(fileNameParticleConfig, meshToAttach);
	}
	static DisposeParticles(disposeEmiter, meshToDispose)
	{
		if(disposeEmiter == undefined) disposeEmiter = false;
		this.particle.deleteParticule();
		if(disposeEmiter == true) {
			if(meshToDispose) { meshToDispose.dispose(); }
			else { console.log("Not mesh emiter to disposed"); }
		}
	}	
	static CreateFloatingNumber()
	{
		
	}
	static ScreenFlash()
	{
		
	}
	static DistanceSquared(mesh)
	{		
		let distance = BABYLON.Vector3.DistanceSquared(game.cameraPlayer, mesh.position);
		return distance;
	}
	static CreateMesh(rootMesh, fileMesh, positionVector3, rotationVector3, scalingVector3)
	{
		let mesh = null;
		if(!positionVector3) { positionVector3 = BABYLON.Vector3.Zero(); }
		else { positionVector3 = new BABYLON.Vector3(positionVector3); }
		if(!rotationVector3) { rotationVector3 = BABYLON.Vector3.Zero(); }
		else { rotationVector3 = new BABYLON.Vector3(rotationVector3); }
		if(!scalingVector3) { scalingVector3 = BABYLON.Vector3.Zero(); }
		else { scalingVector3 = new BABYLON.Vector3(scalingVector3); }
		BABYLON.SceneLoader.ImportMesh("", rootMesh, fileMesh, game.scene, function (newMeshes) {
			mesh = newMeshes[0];
			mesh.position = positionVector3;
			mesh.rotation = rotationVector3;
			mesh.scaling = scalingVector3;
			if(mesh.skeleton) mesh.updatePoseMatrix(BABYLON.Matrix.Identity());
			return mesh;
		});		
	}
	static MeshVisible(mesh, isVisible)
	{
		if(!isVisible) isVisible = false;
		mesh.isEnabled(isVisible);
	}
	static DisposeMesh(mesh)
	{
		mesh.dispose();
	}	
}