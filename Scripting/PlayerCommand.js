class PLAYER {
	static GetUser(actor)
	{
		return actor.user;
	}	
	static PlayerAccountEmail(user)
	{
		return user.mail;
	}
	static PlayerAccountName(user)
	{
		return user.pseudo;
	}
	static PlayerInGame(user)
	{
		return user.inGame || false;
	}
	static PlayerIsBanned(user)
	{
		return user.isBanned || false;
	}
	static PlayerIsGM(user)
	{
		return user.IsGM || false;
	}
}