class UI 
{
	constructor()
	{
		this.drag = false;
		this.startingPoint = null;
		this.startElement = null;
	}	
	
	static createUIManager(nameUI, addPanel = true, layerMask = 3)
	{		
		this.UIManager = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI(nameUI);
		this.UIManager.layer.layerMask = layerMask;	
		if(addPanel) {
			this.panel = new BABYLON.GUI.Container();			
			this.UIManager.addControl(this.panel);			
			this.UIManager = this.panel;
		} 		
		return this.UIManager;		
	}
	
	static createUIManagerForMesh(mesh, layerMask = 3, size = 1024)
	{
		if(this.UIManager) this.UIManager.dispose();
		this.UIManager = BABYLON.GUI.AdvancedDynamicTexture.CreateForMesh(mesh, size, size);
		this.UIManager.layer.layerMask = layerMask;	
		return this.UIManager;
	}
	
	static isDraggable(container, callback)
	{
		container.onPointerMoveObservable.add(callback); 
	}
	
	static dragStart(coordinates, element)
	{
		this.startingPoint = new BABYLON.Vector2(coordinates.x, coordinates.y);
		this.startElement = new BABYLON.Vector2(parseFloat(element.left), parseFloat(element.top));
		this.drag = true;
	}
	
	static dragStop()
	{
		this.drag = false;        
        this.startingPoint = null;
	}
	
	static dragMove(coordinates, element)
	{
		if (!this.startingPoint) return;
        if (this.drag == true) {
            let diff = this.startingPoint.subtract(new BABYLON.Vector2(coordinates.x, coordinates.y));
            element.left = -diff.x + this.startElement.x;
            element.top = -diff.y + this.startElement.y;
        }
	}
	
	static getSizeBrowser()
	{
		let largeur = 0, hauteur = 0;
		if(document.body) {
			largeur = document.body.clientWidth;
			hauteur = document.body.clientHeight;
		} else {
			largeur = window.innerWidth;
			hauteur = window.innerHeight;
		}
		return {width: largeur, height: hauteur};
	}	

	static Window(optionsContent = {}, optionsTitle = null, containeur = null, group = null, onPointerDownCallback = null, onPointerUpCallback = null, onPointerMoveCallback = null)
	{
		let window = new BABYLON.GUI.Rectangle();
		window.width = (optionsContent.width || 200)+"px";
		window.height = (optionsContent.height || 350)+"px";
		window.cornerRadius = optionsContent.radius || 0;
		window.color = optionsContent.color || "white";
		window.thickness = optionsContent.border || 0;
		window.zIndex = optionsContent.zIndex || 1;
		window.background = optionsContent.background || "#1e1e1e";
		window.horizontalAlignment = optionsContent.align || BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
		window.verticalAlignment = optionsContent.verticalAlign || BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
		window.top = (optionsContent.top || 0)+"px";
		window.left = (optionsContent.left || 0)+"px";
		window.paddingTop = (optionsContent.paddingTop || 0)+"px";
		window.paddingBottom = (optionsContent.paddingBottom || 0)+"px";
		window.paddingLeft = (optionsContent.paddingLeft || 0)+"px";
		window.paddingRight = (optionsContent.paddingRight || 0)+"px";		
		if(onPointerDownCallback) window.onPointerDownObservable.add(onPointerDownCallback);
		if(onPointerUpCallback) window.onPointerUpObservable.add(onPointerUpCallback);		
		if(onPointerMoveCallback) window.onPointerMoveObservable.add(onPointerMoveCallback);
		this.UIManager.addControl(window);
		if(optionsTitle) {
			let titleWindow = new BABYLON.GUI.Rectangle();
			titleWindow.width = "100%";
			titleWindow.zIndex = 2;
			titleWindow.isHitTestVisible = false;
			titleWindow.fontSize = optionsTitle.size || 15;
			titleWindow.thickness = optionsTitle.border || 0;
			titleWindow.height = (optionsTitle.height || 25)+"px";
			titleWindow.cornerRadius = optionsTitle.radius || 20;
			titleWindow.color = optionsTitle.color || "white";
			titleWindow.top = (optionsTitle.top || 0)+"px";
			titleWindow.left = (optionsTitle.left || 0)+"px";
			titleWindow.background = optionsTitle.background || "transparent";
			titleWindow.horizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER;
			titleWindow.verticalAlignment = optionsTitle.verticalAlign || BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
			window.addControl(titleWindow);
			this.Text(optionsTitle.title, optionsTitle, titleWindow, null);
		}
		return window;
	}

	static Button(nameUI, nameButton, options = {}, UrlIcone = null, containeur = null, group = null, onPointerDownCallback = null, onPointerUpCallback = null, onPointerEnterCallback = null, onPointerOutCallback = null, onPointerMoveCallback = null)
	{
		let button = null;
		if(UrlIcone && nameButton) {
			button = BABYLON.GUI.Button.CreateImageButton(nameUI, nameButton, UrlIcone);
		} else if(UrlIcone && !nameButton) {
			button = BABYLON.GUI.Button.CreateImageOnlyButton(nameUI, UrlIcone);
		} else {
			button = BABYLON.GUI.Button.CreateSimpleButton(nameUI, nameButton);
		}
		button.width = (options.width || 160)+"px";
		button.height = (options.height || 30)+"px";
		button.cornerRadius = options.radius || 15;
		button.thickness = options.border || 0;
		button.color = options.color || "white";
		button.background = options.background || "#104358";
		button.horizontalAlignment = options.align || BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
		button.verticalAlignment = options.verticalAlign || BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
		button.top = (options.top || 0)+"px";
		button.left = (options.left || 0)+"px";
		button.paddingTop = (options.paddingTop || 0)+"px";
		button.paddingBottom = (options.paddingBottom || 0)+"px";
		button.paddingLeft = (options.paddingLeft || 0)+"px";
		button.paddingRight = (options.paddingRight || 0)+"px";
		if(onPointerDownCallback) button.onPointerDownObservable.add(onPointerDownCallback);
		if(onPointerUpCallback) button.onPointerUpObservable.add(onPointerUpCallback);
		if(onPointerEnterCallback) button.onPointerEnterObservable.add(onPointerEnterCallback);
		if(onPointerOutCallback) button.onPointerOutObservable.add(onPointerOutCallback);
		if(onPointerMoveCallback) button.onPointerMoveObservable.add(onPointerMoveCallback);
		if(containeur) {
			containeur.addControl(button);
		} else if(group) {
			group.addControl(button);
		} else {
			this.UIManager.addControl(button);
		}
		return button;
	}

	static Text(texte, options = {}, containeur = null, group = null)
	{
		let text = new BABYLON.GUI.TextBlock();
		text.text = texte;
		text.color = options.color || "white";
		text.fontSize = options.size || 15;
		text.top = (options.top || 0)+"px";
		text.zIndex = options.zIndex || 3;
		text.left = (options.left || 0)+"px";
		text.paddingTop = (options.paddingTop || 0)+"px";
		text.paddingBottom = (options.paddingBottom || 0)+"px";
		text.paddingLeft = (options.paddingLeft || 0)+"px";
		text.paddingRight = (options.paddingRight || 0)+"px";
		text.fontFamily = options.fontFamily || "arial";
		text.isHitTestVisible = false;
		text.textWrapping = true;
		text.textHorizontalAlignment = options.align || BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
		text.textVerticalAlignment = options.verticalAlign || BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
		if(containeur) {
			containeur.addControl(text);
		} else if(group) {
			group.addControl(text);
		} else {
			this.UIManager.addControl(text);
		}
		return text;
	}

	static Line(meshParent, MeshConnected, options = {})
	{
		let line = new BABYLON.GUI.Line();
		line.alpha = 0.5;
		line.lineWidth = 2;
		line.dash = [2, 8];
		line.color = "white";
		this.UIManager.addControl(line);
		line.linkWithMesh(meshParent);
		line.connectedControl = MeshConnected;
		return line;
	}

	static Image(nameImage, options = {}, containeur = null, group = null, onPointerDownCallback = null, onPointerUpCallback = null, onPointerEnterCallback = null, onPointerOutCallback = null, onPointerMoveCallback = null)
	{
		if(options.radius) {
			var rect = new BABYLON.GUI.Rectangle();
			rect.top = (options.top || 0)+"px";
			rect.left = (options.left || 0)+"px";
			rect.paddingTop = (options.paddingTop || 0)+"px";
			rect.paddingBottom = (options.paddingBottom || 0)+"px";
			rect.paddingLeft = (options.paddingLeft || 0)+"px";
			rect.paddingRight = (options.paddingRight || 0)+"px";
			rect.width = (options.width || 100)+"px";
			rect.isHitTestVisible = false;
			rect.height = (options.height || 100)+"px";
			rect.horizontalAlignment = options.align || BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
			rect.verticalAlignment = options.verticalAlign || BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
			rect.cornerRadius = options.radius;
			rect.thickness = 0;
			rect.background = "transparent";			
			if(containeur) {
				containeur.addControl(rect);
			} else if(group) {
				group.addControl(rect);
			} else {
				this.UIManager.addControl(rect);
			}
		}
		let image = new BABYLON.GUI.Image(nameImage, options.URLImage);
		image.width = (options.width || 100)+"px";
		image.height = (options.height || 100)+"px";
		if(!options.radius) {
			image.top = (options.top || 0)+"px";
			image.left = (options.left || 0)+"px";
			image.paddingTop = (options.paddingTop || 0)+"px";
			image.paddingBottom = (options.paddingBottom || 0)+"px";
			image.paddingLeft = (options.paddingLeft || 0)+"px";
			image.paddingRight = (options.paddingRight || 0)+"px";
		}
		image.horizontalAlignment = options.align || BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
		image.verticalAlignment = options.verticalAlign || BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER;
		image.stretch = options.stretch || BABYLON.GUI.Image.STRETCH_FILL;
		image.isHitTestVisible = false;
		if(onPointerDownCallback) image.onPointerDownObservable.add(onPointerDownCallback);
		if(onPointerUpCallback) image.onPointerUpObservable.add(onPointerUpCallback);
		if(onPointerEnterCallback) image.onPointerEnterObservable.add(onPointerEnterCallback);
		if(onPointerOutCallback) image.onPointerOutObservable.add(onPointerOutCallback);
		if(onPointerMoveCallback) image.onPointerMoveObservable.add(onPointerMoveCallback);
		if(options.radius) {
			rect.addControl(image);
		} else {
			if(containeur) {
				containeur.addControl(image);
			} else if(group) {
				group.addControl(image);
			} else {
				this.UIManager.addControl(image);
			}
		}
		return image;
	}

	static Link(text, url, options = {}, containeur = null, group = null)
	{
		let textLink = new BABYLON.GUI.TextBlock();
		textLink.text = text;
		textLink.color = options.color || "white";
		textLink.fontSize = options.size || 15;
		textLink.horizontalAlignment = options.align || BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
		textLink.verticalAlignment = options.verticalAlign || BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER;
		textLink.top = (options.top || 0)+"px";
		textLink.left = (options.left || 0)+"px";
		textLink.paddingTop = (options.paddingTop || 0)+"px";
		textLink.paddingBottom = (options.paddingBottom || 0)+"px";
		textLink.paddingLeft = (options.paddingLeft || 0)+"px";
		textLink.paddingRight = (options.paddingRight || 0)+"px";
		textLink.fontFamily = options.fontFamily || "italic arial";
		textLink.horizontalAlignment = options.align || BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
		textLink.verticalAlignment = options.verticalAlign || BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER;		
		if(onPointerUpCallback) image.onPointerDownObservable.add(() => { location.href = url; });
		if(onPointerEnterCallback) image.onPointerEnterObservable.add(() => { textLink.color = options.colorHover || "blue"; });
		if(onPointerOutCallback) image.onPointerOutObservable.add(() => { textLink.color = options.color || "white"; });
		if(containeur) {
			containeur.addControl(textLink);
		} else if(group) {
			group.addControl(textLink);
		} else {
			this.UIManager.addControl(textLink);
		}
		return textLink;
	}

	static Checkbox(labelName, optionsCheckbox = {}, containeur = null, group = null, onIsCheckedChangedCallback = null)
	{
		let checkbox = new BABYLON.GUI.Checkbox();
		checkbox.width = "20px";
		checkbox.height = "20px";
		checkbox.checkSizeRatio = 0.7; 
		checkbox.isChecked = optionsCheckbox.isChecked || false;
		checkbox.color = optionsCheckbox.colorBox || "#104358";
		checkbox.background = optionsCheckbox.backgroundBox || "white";
		checkbox.onIsCheckedChangedObservable.add(onIsCheckedChangedCallback);
		let panel = BABYLON.GUI.AddHeader(checkbox, labelName, optionsCheckbox.width+"px", { isHorizontal: true, controlFirst: true});
		panel.color = optionsCheckbox.labelColor || "white";
		panel.height = "20px";
		panel.top = (optionsCheckbox.top || 0)+"px";
		panel.left = (optionsCheckbox.left || 0)+"px";
		panel.paddingTop = (optionsCheckbox.paddingTop || 0)+"px";
		panel.paddingBottom = (optionsCheckbox.paddingBottom || 0)+"px";
		panel.paddingLeft = (optionsCheckbox.paddingLeft || 0)+"px";
		panel.paddingRight = (optionsCheckbox.paddingRight || 0)+"px";
		panel.horizontalAlignment = optionsCheckbox.align || BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
		panel.verticalAlignment = optionsCheckbox.verticalAlign || BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;		
		panel.children[1].onPointerDownObservable.add(function() { checkbox.isChecked = true; });
		if(containeur) {
			containeur.addControl(panel);
		} else if(group) {
			group.addControl(panel);
		} else {
			this.UIManager.addControl(panel);
		}
		return panel;
	}
	
	static Radio(labelName, options = {}, containeur = null, group = null, onIsCheckedChangedCallback = null)
	{		
		let panel = new BABYLON.GUI.StackPanel();
		panel.width = (options.width + 20 || 220)+"px";		
		panel.top = (options.top || 0)+"px";
		panel.left = (options.left || 0)+"px";
		panel.zIndex = 1;
		panel.paddingTop = (options.paddingTop || 0)+"px";
		panel.paddingBottom = (options.paddingBottom || 0)+"px";
		panel.paddingLeft = (options.paddingLeft || 0)+"px";
		panel.paddingRight = (options.paddingRight || 0)+"px";
		panel.horizontalAlignment = options.align || BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
		panel.verticalAlignment = options.verticalAlign || BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
		if(containeur) {
			containeur.addControl(panel);
		} else if(group) {
			group.addControl(panel);
		} else {
			this.UIManager.addControl(panel);
		}		
		let radio = new BABYLON.GUI.RadioButton();
		radio.width = "20px";
		radio.height = "20px";
		radio.color = options.color || "#104358";
		radio.background = options.background || "white";
		radio.checkSizeRatio = 0.7; 
		radio.onIsCheckedChangedObservable.add(onIsCheckedChangedCallback);
		let header = BABYLON.GUI.AddHeader(radio, labelName, options.width+"px", { isHorizontal: true, controlFirst: true });
		header.height = "30px";
		header.children[1].onPointerDownObservable.add(function() { radio.isChecked = true; });
		panel.addControl(header); 
		return panel;
	}

	static Slider(options = {}, containeur = null, group = null, onValueChangedCallback = null)
	{		
		let panel = new BABYLON.GUI.StackPanel();
		panel.width = (options.width + 20 || 220)+"px";		
		panel.top = (options.top || 0)+"px";
		panel.left = (options.left || 0)+"px";
		panel.zIndex = 1;
		panel.paddingTop = (options.paddingTop || 0)+"px";
		panel.paddingBottom = (options.paddingBottom || 0)+"px";
		panel.paddingLeft = (options.paddingLeft || 0)+"px";
		panel.paddingRight = (options.paddingRight || 0)+"px";
		panel.horizontalAlignment = options.align || BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
		panel.verticalAlignment = options.verticalAlign || BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
		if(containeur) {
			containeur.addControl(panel);
		} else if(group) {
			group.addControl(panel);
		} else {
			this.UIManager.addControl(panel);
		}		
		let header = new BABYLON.GUI.TextBlock();
		header.text = options.text || "";
		header.height = "30px";
		header.color = options.color || "white";
		panel.addControl(header);		
		let slider = new BABYLON.GUI.Slider();
		slider.minimum = options.min || 0;
		slider.maximum = options.max || 100;
		slider.value = options.value || 0;
		slider.height = (options.height || 20)+"px";
		slider.width = (options.width || 200)+"px";
		slider.borderColor = options.borderColor || "#104358";
		slider.color = options.color || "#104358";
		slider.background = options.background || "white";		
		slider.onValueChangedObservable.add(onValueChangedCallback);
		panel.addControl(slider);		
		return panel;
	}
	
	static Input(options = {}, containeur = null, group = null, onValueChangedCallback = null)
	{
		let input = new BABYLON.GUI.InputText();
        input.width = options.width || 100+"px";
		input.height = (options.height || 40)+"px";
        input.maxWidth = (options.maxWidth || 100)+"%";       
        input.text = options.text;
        input.autoStretchWidth = options.stretch || true;        
        input.color = options.color || "black";
        input.background = options.background || "white"; 
		input.focusedBackground = options.backgroundFocus || "#f7f6e7"; 
		input.thickness = options.border || 1;       
		if(containeur) {
			containeur.addControl(input);
		} else if(group) {
			group.addControl(input);
		} else {
			this.UIManager.addControl(input);
		}
		return input;
	}
	
	static ColorPicker(options = {}, containeur = null, group = null, onValueChangedCallback = null, onPointerDownCallback = null, onPointerUpCallback = null, onPointerEnterCallback = null, onPointerOutCallback = null, onPointerMoveCallback = null)
	{		
		let panel = new BABYLON.GUI.StackPanel();
		panel.width = (options.width + 50 || 200)+"px";	
		panel.isVertical = true;
		panel.top = (options.top || 0)+"px";
		panel.left = (options.left || 0)+"px";
		panel.paddingTop = (options.paddingTop || 0)+"px";
		panel.paddingBottom = (options.paddingBottom || 0)+"px";
		panel.paddingLeft = (options.paddingLeft || 0)+"px";
		panel.paddingRight = (options.paddingRight || 0)+"px";
		panel.horizontalAlignment = options.align || BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
		panel.verticalAlignment = options.verticalAlign || BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;		
		if(containeur) {
			containeur.addControl(panel);
		} else if(group) {
			group.addControl(panel);
		} else {
			this.UIManager.addControl(panel);
		}		
		let color = new BABYLON.GUI.ColorPicker();	
		color.value = options.valueColor || new BABYLON.Color3(0.5, 0.5, 0.5);
		color.height = (options.width || 150)+"px";		
		color.width = (options.width || 150)+"px";		
		color.onValueChangedObservable.add(onValueChangedCallback);
		if(onPointerDownCallback) color.onPointerDownObservable.add(onPointerDownCallback);
		if(onPointerUpCallback) color.onPointerUpObservable.add(onPointerUpCallback);
		if(onPointerEnterCallback) color.onPointerEnterObservable.add(onPointerEnterCallback);
		if(onPointerOutCallback) color.onPointerOutObservable.add(onPointerOutCallback);
		if(onPointerMoveCallback) color.onPointerMoveObservable.add(onPointerMoveCallback);		
		panel.addControl(color);
		return panel;
	}

	static PanelGroup(container = null, options = {})
	{
		let group = new BABYLON.GUI.StackPanel();
		group.zIndex = options.zIndex || 0;
		group.fontSize = (options.size || 15)+"px";
		group.top = (options.top || 0)+"px";
		group.left = (options.left || 0)+"px";
		group.paddingTop = (options.paddingTop || 0)+"px";
		group.paddingBottom = (options.paddingBottom || 0)+"px";
		group.paddingLeft = (options.paddingLeft || 0)+"px";
		group.paddingRight = (options.paddingRight || 0)+"px";
		group.horizontalAlignment = options.align || BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
		group.verticalAlignment = options.verticalAlign || BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
		if(container) {
			container.addControl(group);
		} else {
			this.UIManager.addControl(group);
		}
		return group;
	}
	
}