class ACTION {
	
	constructor(mesh) {
		this.actions = [];
		this.trigger = null;
		this.operator = null;	
		this.mesh = mesh;		
		let scene = this.mesh.getScene();
		this.mesh.actionManager = new BABYLON.ActionManager(scene);
	}

	_trigger(trigger) {
		switch(trigger) {
			case "OnPickTrigger": this.trigger = BABYLON.ActionManager.OnPickTrigger; break;
			case "OnPickDownTrigger": this.trigger = BABYLON.ActionManager.OnPickDownTrigger; break;
			case "OnPickUpTrigger": this.trigger = BABYLON.ActionManager.OnPickUpTrigger; break;
			case "OnPickOutTrigger": this.trigger = BABYLON.ActionManager.OnPickOutTrigger; break;
			case "OnLeftPickTrigger": this.trigger = BABYLON.ActionManager.OnLeftPickTrigger; break;
			case "OnRightPickTrigger": this.trigger = BABYLON.ActionManager.OnRightPickTrigger; break;
			case "OnCenterPickTrigger": this.trigger = BABYLON.ActionManager.OnCenterPickTrigger; break;
			case "OnLongPressTrigger": this.trigger = BABYLON.ActionManager.OnLongPressTrigger; break;
			case "OnPointerOverTrigger": this.trigger = BABYLON.ActionManager.OnPointerOverTrigger; break;
			case "OnPointerOutTrigger": this.trigger = BABYLON.ActionManager.OnPointerOutTrigger; break;
			case "OnIntersectionEnterTrigger": this.trigger = BABYLON.ActionManager.OnIntersectionEnterTrigger; break;
			case "OnIntersectionExitTrigger": this.trigger = BABYLON.ActionManager.OnIntersectionExitTrigger; break;
			case "OnKeyDownTrigger": this.trigger = BABYLON.ActionManager.OnKeyDownTrigger; break;
			case "OnKeyUpTrigger": this.trigger = BABYLON.ActionManager.OnKeyUpTrigger; break;			
		}
		return this.trigger;
	}

	CreateCondition(actionManager, meshEdit, property, value, operator) {
		switch(operator) {
			case "IsEqual": this.operator = BABYLON.ValueCondition.IsEqual; break;
			case "IsDifferent": this.operator = BABYLON.ValueCondition.IsDifferent; break;
			case "IsGreater": this.operator = BABYLON.ValueCondition.IsGreater; break;
			case "IsLesser": this.operator = BABYLON.ValueCondition.IsLesser; break;
		}
		this.condition = BABYLON.ValueCondition(actionManager, data.condition.target, data.condition.property, data.condition.value, this.operator);
		return this.condition;
	}

	AddActionSwitchBoolean(trigger, meshEdit, property, value, options, condition) {
		if(!condition) condition = null;
		if(!options) options = null;
		let nbrActions = this.actions.length;
		this.actions[nbrActions] = new BABYLON.SwitchBooleanAction({trigger: this._trigger(trigger), parameter: options}, meshEdit, property, condition);
	}

	AddActionSetValue(trigger, meshEdit, property, value, options, condition) {
		if(!condition) condition = null;
		if(!options) options = null;
		let nbrActions = this.actions.length;
		this.actions[nbrActions] = new BABYLON.SetValueAction({trigger: this._trigger(trigger), parameter: options}, meshEdit, property, condition);
	}

	AddActionIncrementValue(trigger, meshEdit, property, value, options, condition) {
		if(!condition) condition = null;
		if(!options) options = null;
		let nbrActions = this.actions.length;
		this.actions[nbrActions] = new BABYLON.IncrementValueAction({trigger: this._trigger(trigger), parameter: options}, meshEdit, property, condition);
	}

	AddActionPlayAnimation(trigger, meshEdit, from, to, loop, options, condition) {
		if(!condition) condition = null;
		if(!options) options = null;
		let nbrActions = this.actions.length;
		this.actions[nbrActions] = new BABYLON.PlayAnimationAction({trigger: this._trigger(trigger), parameter: options}, meshEdit, from, to, loop, condition);
	}

	AddActionStopAnimation(trigger, meshEdit, options, condition) {
		if(!condition) condition = null;
		if(!options) options = null;
		let nbrActions = this.actions.length;
		this.actions[nbrActions] = new BABYLON.StopAnimationAction({trigger: this._trigger(trigger), parameter: options}, meshEdit, condition);
	}

	AddActionCombine(trigger, children, options, condition) {
		if(!condition) condition = null;
		if(!options) options = null;
		let nbrActions = this.actions.length;
		this.actions[nbrActions] = new BABYLON.CombineAction({trigger: this._trigger(trigger), parameter: options}, children, condition);
	}

	AddActionExecuteCode(trigger, code, options, condition) {
		if(!condition) condition = null;
		if(!options) options = null;
		let nbrActions = this.actions.length;
		this.actions[nbrActions] = new BABYLON.ExecuteCodeAction({trigger: this._trigger(trigger), parameter: options}, code, condition);
	}

	AddActionSetParent(trigger, meshParent, property, options, condition) {
		if(!condition) condition = null;
		if(!options) options = null;
		let nbrActions = this.actions.length;
		this.actions[nbrActions] = new BABYLON.SetParentAction({trigger: this._trigger(trigger), parameter: options}, meshParent, property, condition);
	}

	AddActionInterpolateValue(trigger, meshEdit, property, value, duration, options, condition, stopOtherAnimations) {
		if(!condition) condition = null;
		if(!options) options = null;
		let nbrActions = this.actions.length;
		this.actions[nbrActions] = new BABYLON.InterpolateValueAction({trigger: this._trigger(trigger), parameter: options}, meshEdit, property, value, duration, condition, stopOtherAnimations);
	}

	RegisterActions() {
		switch(this.actions.length) {
			case 1: this.mesh.actionManager.registerAction(this.actions[0]); break;
			case 2: this.mesh.actionManager.registerAction(this.actions[0]).then(this.actions[1]); break;
			case 3: this.mesh.actionManager.registerAction(this.actions[0]).then(this.actions[1]).then(this.actions[2]); break;
			case 4: this.mesh.actionManager.registerAction(this.actions[0]).then(this.actions[1]).then(this.actions[2]).then(this.actions[3]); break;
			case 5: this.mesh.actionManager.registerAction(this.actions[0]).then(this.actions[1]).then(this.actions[2]).then(this.actions[3]).then(this.actions[4]); break;
			case 6: this.mesh.actionManager.registerAction(this.actions[0]).then(this.actions[1]).then(this.actions[2]).then(this.actions[3]).then(this.actions[4]).then(this.actions[5]); break;
			case 7: this.mesh.actionManager.registerAction(this.actions[0]).then(this.actions[1]).then(this.actions[2]).then(this.actions[3]).then(this.actions[4]).then(this.actions[5]).then(this.actions[6]); break;
			case 8: this.mesh.actionManager.registerAction(this.actions[0]).then(this.actions[1]).then(this.actions[2]).then(this.actions[3]).then(this.actions[4]).then(this.actions[5]).then(this.actions[6]).then(this.actions[7]); break;
			case 9: this.mesh.actionManager.registerAction(this.actions[0]).then(this.actions[1]).then(this.actions[2]).then(this.actions[3]).then(this.actions[4]).then(this.actions[5]).then(this.actions[6]).then(this.actions[7]).then(this.actions[8]); break;
			case 10: this.mesh.actionManager.registerAction(this.actions[0]).then(this.actions[1]).then(this.actions[2]).then(this.actions[3]).then(this.actions[4]).then(this.actions[5]).then(this.actions[6]).then(this.actions[7]).then(this.actions[8]).then(this.actions[9]); break;
		}
	}
}