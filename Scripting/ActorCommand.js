class ACTOR {
	
	//Getter	
	static GetActor()
	{
		return game.character.actor;
	}
	static GetTarget()
	{
		return game.ActorSelect;
	}
	static GetBehavior(actor)
	{
		return actor.behavior;
	}
	static GetAIState(actor, state)
	{
		/*
		DeACTORion:
		The ActorAIState command returns the current AI mode of an actor. There are letious possible states:
		0 - Wait in current position, attack any valid targets who come into range
		1 - Patrol - walk to destination, then set destination to new waypoint if available
		2 - Run - same as Patrol but running instead of walking
		3 - Chase and attack target
		4 - For actors who are paused at a waypoint while on patrol
		5 - Pet mode - actor will follow leader and attack leader's target (actor MUST have a leader to use this)
		6 - Pet attack mode - actor is chasing and attacking the leader's target
		7 - Pet wait mode - actor waits in current position and does absolutely nothing
		*/		
		
	}	
	static CallActorForHelp(actorHelp, actorCall)
	{		
		//La fonction CallActorForHelp permet d'appeller un autres acteur de sa guilde ou son animal pour l'aider l'ors de combat		
		
	}
	static GetClothes(actor)
	{
		return actor.clothes;
	}
	static GetBeard(actor)
	{
		return actor.beard;
	}	
	static GetFace(actor)
	{
		return actor.head;
	}
	static GetGender(actor)
	{
		return actor.sex;
	}
	static GetClass(actor)
	{
		return actor.classe;
	}
	static GetRace(actor)
	{
		return actor.race;
	}
	static GetGlobal()
	{
		
	}
	static GetActorsInGroup(actor, groupName)
	{
		
	}
	static GetHair(actor)
	{
		return actor.hair;
	}
	static ActorHasEffect()
	{
		
	}
	static GeID(actor)
	{
		return actor.actorId;
	}	
	static GetLevel(actor)
	{
		return actor.level;
	}
	static GetMount(actor)
	{
		return game.mount;
	}	
	static GetActorPets(actor)
	{
		
	}
	static GetRider(actor)
	{
		
	}		
	static GetPosition(actor)
	{
		return actor.position;
	}
	static GetDistance(actor, target)
	{
		let distance = BABYLON.Distance(actor.position, target.position);		
		return distance;
	}	
	static GetRotation(actor)
	{
		return actor.rotation;
	}
	static GetScaling(actor)
	{
		return actor.scaling;
	}	
	static GetXP(actor)
	{
		return actor.XP;
	}	
	static GetTag(actor)
	{
		return actor.tag;
	}
	static GetActorInZone(actor)
	{
		
	}			
	static GetAttribute(actor, attributeName)
	{
		return actor.attribute[attributeName];
	}	
	static GetMoney(actor)
	{
		return {monaie1: actor.metadata.bank.monaie1, monaie2: actor.metadata.bank.monaie2}
	}	
	static GetName(actor)
	{
		return actor.name;
	}	
	
	// Setter	
	static SetAnimate(actor, animationName, loop, speed)
	{
		
	}		
	static SetPosition(actor, X, Y, Z)
	{
		actor.position = new BABYLON.Vector3(X, Y, Z);
	}
	static SetDestination(actor, target)
	{
		actor.position(new BABYLON.Vector3(target.position));
	}	
	static SetRotate(actor, X, Y, Z)
	{
		actor.rotation = new BABYLON.Vector3(X, Y, Z);
	}	
	static SetScaling(actor, X, Y, Z)
	{
		actor.scaling = new BABYLON.Vector3(X, Y, Z);
	}
	static SetAIState()
	{
		
	}
	static SetBeard()
	{
		
	}
	static SetClothes()
	{
		
	}	
	static SetFace()
	{
		
	}
	static SetGender()
	{
		
	}
	static SetGlobal()
	{
		
	}	
	static SetHair()
	{
		
	}
	static SetLevel(actor, level)
	{
		actor.level = level;
	}
	static SetTarget(actor, target)
	{
		actor.target = target;
	}
	static SetAttribute(actor, attributeName, value)
	{
		actor.attribute[attributeName] = value;
	}
	static SetLeader(actor, leaderName)
	{
		actor.leader = leaderName;
	}	
	static SetMoney(actor, value, moneyType)
	{
		let moneycurrent = ACTOR.GetMoney(actor);
		if(moneyType == undefined || moneyType == false) {
			actor.metadata.bank.monaie1 = parseInt(moneycurrent.monaie1 + value);
			game.infoActor.bank.money1 = actor.metadata.bank.monaie1;
			actor.metadata.bank.monaie2 = parseInt(moneycurrent.monaie2);
			game.infoActor.bank.money2 = actor.metadata.bank.monaie2;
		}
		if(moneyType == true) {
			actor.metadata.bank.monaie1 = parseInt(moneycurrent.monaie1);
			game.infoActor.bank.money1 = actor.metadata.bank.monaie1;
			actor.metadata.bank.monaie2 = parseInt(moneycurrent.monaie2 + value);
			game.infoActor.bank.money2 = actor.metadata.bank.monaie2;
		}

		// enregistrer avec ajax : game.infoActor.bank.money1 et game.infoActor.bank.money2
		
	}
	static SetName(actor, name)
	{
		actor.name = name;
	}
	static SetReputation(actor, value)
	{
		actor.reputation = value;
	}
	static SetResistance(actor, resistanceName, value)
	{
		actor.resistance[resistanceName] = value;
	}
	static SetFloatNumber(actor, value)
	{
		game.createFloatingNumber(actor, value);
	}
	static SetBood(actor, impactPoint, matrix)
	{
		game.projectionBlood(actor, impactPoint, matrix);
	}
	static SetTag(actor, value)
	{
		actor.tag = value;
	}

	// Is and In
	static InTrigger()
	{
		
	}
	static IsPlayer(actor)
	{
		if(actor.actor_type == "player") {
			return true;
		}
		return false;		
	}
	static IsLeader(actor)
	{
		return actor.leader;
	}
	static IsOutdoors(actor)
	{
		return true;
	}
	static IsUnderWater(actor)
	{
		return false;
	}
	
	
	// Autre
	static OpenTrading()
	{
		
	}
	static PartyMember()
	{
		
	}
	static PlaySound(sound)
	{
		sound.play();
	}	
	static StopSound(sound)
	{
		sound.stop();
	}	
	static GetReputation(actor)
	{
		return actor.reputation;
	}
	static GetResistance(actor, resistanceName)
	{
		return actor.resistance[resistanceName];
	}
	static Spawn()
	{
		
	}
	static UpdateHealtBar()
	{
		
	}
	static UpdateXPBar()
	{
		
	}
	static Warp()
	{
		
	}
	static AddGroup()
	{
		
	}
	static Change(actor, newActorName)
	{
		
	}	
	static Context()
	{
		
	}
	static CountPartyMembers()
	{
		
	}
	static DeleteEffect()
	{
		
	}
	static FindActorByName(nameActor)
	{
		let actor = game.scene.getMeshByName(nameActor);
		return actor;
	}
	static FindActorById(idActor)
	{
		let actor = game.scene.getMeshByID(idActor);
		return actor;
	}
	static FireProjectile()
	{
		
	}	
	static GiveItem()
	{
		
	}
	static HasItem()
	{
		
	}
	static GiveXP()
	{
		
	}
	static GiveKillXP()
	{
		
	}	
	static Delete(actor)
	{
		actor.dispose();
	}	
	static Move(actor, destination)
	{
		
	}
	static AddEffect(actor, effect)
	{
		
	}
	
}