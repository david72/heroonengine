class GENERAL {
	static getScene()
	{
		return game.scene;
	}	
	static getEngine()
	{
		return game.engine;
	}
	static getMeshByName(name)
	{
		return game.scene.getMeshByName(name);
	}
	static Timestamp()
	{
		return new Date();
	}	
	static RealDate()
	{
		let date = GENERAL.Timestamp();
		return date.getDay()+"/"+date.getMonth()+"/"+date.getFullYear();
	}
	static RealTime(year, mouth, day)
	{
		let time = GENERAL.Timestamp();		
		return time.getHours()+"/"+time.getMinutes()+"/"+time.getSeconds();
	}	
	static SetSuperGlobal(data, value)
	{
		global[data] = value;
	}
	static GetSuperGlobal(data)
	{
		return global[data];
	}
	static ExecuteFilePHP(file, data)
	{
		$.ajax({
			type: "POST",
			url: 'GENERALs/'+file,
			data: data
		});
	}
	static ReadFileJSON(file, data)
	{
		let _data = null;
		$.ajaxSetup({ async: false});	
		$.getJSON('GENERALs/'+file+'', function(data) {
			_data = data;
		});		
		$.ajaxSetup({ async: true});
		return _data;
	}
}