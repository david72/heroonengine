# HeroonEngine #

Heroon engine est un éditeur tout en un pour créer un jeu de type MMORPG entièrement sur le navigateur avec webgl2 et le moteur 3D BabylonJS. L’éditeur et le jeu sont écrit en Javascript ES6.

[![891.png](https://bitbucket.org/repo/Krngy6x/images/1531790679-891.png)
](http://www.indiedb.com/engines/heroonengine)

## Téléchargement et mise à jour ##

Pour l'utiliser, télécharger l’éditeur et le serveur a partir du site (vous n'avez besoin que de cela en tant qu'utilisateur). Vous pourrez par la suite le mettre a jour en temps réelle avec [tortoiseSVN](https://tortoisesvn.net/downloads.html) ou [sourceTree](https://www.sourcetreeapp.com/), pour recevoir les mise à jour très facilement et rapidement en un clic. Ou vous pouvez passer par le site, mais c’est plus long et les mise à jour ne sont pas en temps réelle.

- Lien de téléchargement de l’éditeur : [http://www.heroonengine.actifgames.com/download.php](http://www.heroonengine.actifgames.com/download.php)
- Utiliser SourceTree pour recuperer les mise à jour en temps réele ou/et contribuer. Nous avons un [tutoriel ici](https://bitbucket.org/JSbabylon/heroonengine/wiki/Utiliser%20sourceTree%20pour%20mettre%20%C3%A0%20jour%20l'editeur). 

Voici comment paramétrer tortoiseSVN avec ce lien https :

Utiliser ce lien en https pour tortoiseSVN afin de recuperer les mise à jour en temps réele :
[https://Dad72@bitbucket.org/JSbabylon/heroonengine.git](https://Dad72@bitbucket.org/JSbabylon/heroonengine.git)

![tortoiseSVN_seting.jpg](https://bitbucket.org/repo/Krngy6x/images/3702708130-tortoiseSVN_seting.jpg)

Vous aurez plus qu'à faire un clic droit sur votre dossier local et cliquer sur "SVN Checkout" pour mettre a jour l’éditeur. C’est rapide et vous obtenez les mises à jour en avant-première.

## Exécuter HeroonEngine ##

Exécuter le fichier HeroonEngine.exe qui se trouve dans le dossier HeroonEngine/. Cela ouvrira le serveur web. Il ne vous reste plus qu'à cliquer sur "Start Apache" qui exécutera le serveur et lancera HeroonEngine sur votre navigateur favori.

![serveur_web.jpg](https://bitbucket.org/repo/Krngy6x/images/2826030116-serveur_web.jpg)

Pour toutes questions n’hésitez pas à aller dans "[Signalement de bug](https://bitbucket.org/JSbabylon/heroonengine/issues?status=new&status=open)". Lien du menu à gauche.
Nous n'avons pour le moment pas de forum, mais si vous êtes suffisamment de personne a pensez que cela serait utiles, dites-le-moi et j'en ajouterons un. Mais pour le moment, il n'est pas nécessaire, HeroonEngine est tout jeune.

## Contributions ##

Le dépôt vous permet de participer au développement de ce projet. Toutes vos contributions son bien sûr créditer et les bienvenues. Si vous avez envie de passer un peu de temps sur un projet sérieux qui vous plais. Ce projet est aussi le vôtre, il vie grâce à vous, grâce à vos testes, rapport de bug, suggestions. N’hésitez pas a participer de la maniéré que vous pouvez, nous ne refuserons jamais d'aide. Vous pouvez créer des tutoriels, corriger des bugs, améliorer l'éditeur, ajouter des fonctionnalités, compléter la démo, l'améliorer...

Dans les sources de ce dépôt vous trouverez :

1. **Editor** qui est l’éditeur (le GE)
1. **GUI_PSD** qui sont tous les PSD utiliser pour créer l'interface graphique
1. **Game** est toutes la partie jeu générer par l’éditeur, le moteur de jeu.
1. **SDK** c’est toutes la partie interface graphique de l’éditeur
1. **Scripting** C’est toutes les fonctions de script pour le Scripting dans l’éditeur et pour le jeu
1. **Serveur** C’est le serveur de jeu NodeJS avec cluster de serveur et zone d’intérêt.

Vous voulez plus de détail ? lisez ceci :

[Caractéristique et contribution de l'éditeur](https://bitbucket.org/JSbabylon/heroonengine/src/91c727cc7abef92d192d8d8528b7320bc3df764c/Editor/README.md?at=master&fileviewer=file-view-default)

![CharacterSet.jpg](https://bitbucket.org/repo/Krngy6x/images/560093959-CharacterSet.jpg)

![Game.1.jpg](https://bitbucket.org/repo/Krngy6x/images/4046615168-Game.1.jpg)

![HeroonEngine.jpg](https://bitbucket.org/repo/Krngy6x/images/2983051436-HeroonEngine.jpg)
