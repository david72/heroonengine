# SERVEUR #

Des questions ? Voici [notre forum](http://www.heroonengine.actifgames.com/forum/) officiel.

Le serveur utilise NodeJS. Tout ce passe dans le fichier serveur.js. L'interface utilisateur ce trouve dans le serveur.hta.
Le dossier node_modules/ permet de faire fonctionner les fonctionnaliter utiliser par le serveur, notament: cluster, http, socket.io, socket.io-redis, os, express, ent, fs.
Vous devez pour tester en local avoir installer NodeJS.


### Vous souhaitez contribuer, que faut-il faire ? ###


Avant toutes chose, n'ayez pas peur, vous n'avez pas besoin d’être un gourou de la programmation ou expert pour participer. Tout le monde peut participer à sa façon et avec ses moyens, connaissance. Ce projet a besoin de participant comme vous, expert ou non. Alors lancer vous. Soyez juste soigner, rigoureux et tout iras bien.

Dans un premier temps, vous devez télécharger et installer un logiciel fournit sur ce site "bitbucket" qui permet de communiquer entre vos fichiers en local (sur votre ordinateur) et ce dépôt distant. Ce logiciel se nomme "[SourceTree](https://www.sourcetreeapp.com/?utm_source=internal&utm_medium=link&utm_campaign=clone_repo_win)".

Ensuite, vous devez créer une branche (Forks) pour créer votre dépôt à partir du dépôt principal pour ensuite le cloner sur votre ordinateur. Consulter [notre tutoriel complet ici](https://bitbucket.org/HeroonEngine/editor/wiki/Cloner%20le%20d%C3%A9p%C3%B4t) pour voir comment faire.

Puis il vous suffira de modifier les fichiers sur votre ordinateur pour faire ce que vous souhaitez comme un correctif, comme fonctionnalités... Vous devrez ensuite créer un "Pull requests" pour soumettre vos modifications locales. Votre "Pull requests" (PR) sera étudier, puis fusionner.


### Quelque recommandation pour les Pull Request. ###


* vous ne devez jamais supprimer de fonction dans le code. Si vous expédiez une fonction supprimer, elle ne sera pas validée. Toutefois, vous pouvez modifier une fonction si vous pensez qu'elle sera mieux ou plus complète.
* Vous devrez vérifier que ce que vous avez fait fonctionne normalement avant de soumettre votre PR.
* N’hésitez pas à venir poser vos questions sur le forum pour proposer votre idée de modification, vous pourriez recueillir des avis divers qui pourrait faire évoluer votre idée première.


### Nos directives de codage ###


Nous ne sommes pas trop exigent, mais nous avons quelque recommandation pour l’écriture de code de façons à garder toujours le même style d’écriture du code pour tout le monde. Voici nos quelques directives pour que nous validions vos contributions :

* Les variables et fonctions privées commençant par un underscore ( _ ) : *_myVariable*, *_myFunction()*
* Les noms de variables, fonctions, classes commence par une Majuscule. Dans le cas de mot composer, Une majuscule à chaque mot : *myVariableComposed*, *myFunctionComposed()*
* Les accolades de fonctions, conditions "*{ }*" doivent être utilisées pour chaque bloc, même s'il n'y a qu'une seule ligne. L’accolade d'ouverture est sur la même ligne après la fermeture de parenthèse.
* Des commentaires peuvent être utiles pour relire votre code, dans le cas de nouvelle fonctions (En français de préférence, mais nous acceptons en anglais aussi)
* Le code doit être indenter et aérer (sans trop d'exagération) pour une relecture plus facile.

