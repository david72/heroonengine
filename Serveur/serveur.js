var info = {
	serveur: {
		host: "localhost",
		port: 5840		
	}
};

var cluster = require('cluster'),
	_portSocket = parseInt(info.serveur.port),
	_portRedis = 6379,
	_HostRedis = info.serveur.host;

if(cluster.isMaster) {	
	var server = require('http').createServer(),
	socketIO = require('socket.io').listen(server),
	redis = require('socket.io-redis');	
	
	socketIO.adapter(redis({ host: _HostRedis, port: _portRedis }));
	
	var numberOfCPUs = require('os').cpus().length;
	for (var i = 0; i < numberOfCPUs; i++) {
		cluster.fork();		
	}
	
	cluster.on('fork', function(worker) {
        console.log('Travailleur %s créer', worker.id);
    });
    cluster.on('online', function(worker) {
        console.log('Travailleur %s en ligne', worker.id);
    });
    cluster.on('listening', function(worker, addr) {
        console.log('Travailleur %s écoute sur %s:%d', worker.id, addr.address, addr.port);
    });
    cluster.on('disconnect', function(worker) {
        console.log('Travailleur %s déconnecter', worker.id);
    });
    cluster.on('exit', function(worker, code, signal) {
        console.log('Travailleur %s mort (%s)', worker.id, signal || code);
        if (!worker.suicide) {
            console.log('Nouveau travailleur %s créer', worker.id);
            cluster.fork();
        }
    });
}

if(cluster.isWorker) {	

	var http = require('http');	
	http.globalAgent.maxSockets = Infinity;	
	
	var app = require('express')(), ent = require('ent'), fs  = require('fs'), server = http.createServer(app).listen(_portSocket), socketIO = require('socket.io').listen(server), redis = require('socket.io-redis');
	
	socketIO.adapter(redis({ host: _HostRedis, port: _portRedis }));
	
	//app.get('/', function (req, res) { res.emitfile(__dirname + '/game.php');});
	
	socketIO.sockets.on('connection', function(socket, pseudo) {

		socket.setNoDelay(true);
		
		socket.on('nouveau_client', function(pseudo) {
			pseudo = ent.encode(pseudo);			
			socket.pseudo = pseudo;
			try {
				socket.broadcast.to(socket.room).emit('nouveau_client', pseudo);
			} catch(e) {
				socket.to(socket.room).emit('nouveau_client', pseudo);
			}
			console.log('Le joueur : '+socket.pseudo+' s\'est connecter');
		});	
		
		socket.on('new_actor', function(data) {	
			socket.to(socket.room).emit('new_actor', {rootMesh: data.rootMesh, fileMesh: data.fileMesh});			
			console.log('Actor créer');
		});

		socket.on('message', function(data) {
			socket.broadcast.to(socket.room).emit('dispatch', data);
		});	

		socket.on('exit', function(data) { socket.close();});
		
		socket.on('room_enter', function(room) {
			socket.room = room;
			socket.id = room;
			socket.join(room);	
			console.log('Le joueur : '+socket.pseudo+' est entrer dans une zone d\'interet : '+socket.room);
		});		
		
		socket.on('room_exit', function(room) {	
			if(room == socket.room) {
				socket.leave(socket.room);			
				console.log('Le joueur : '+socket.pseudo+' a quitter la zone d\'interet : '+socket.room);
				socket.room = null;
				socket.id = null;
			}
		});
	});	
}