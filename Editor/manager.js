/*##################################################
 *                                manager.js
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 01/10/2016
 ###################################################*/

 $(function() {	
	
	var projetSelected = null, idSelected = null, nbNoUpdate = 0;
	
	$("#HE-manager-onglet").tabs();
	$("#HE-manager-menu-select").selectmenu();
	$( "#HE-manager" ).draggable();
	
    var setCookie = function(name, value, days) {
		if(name && value) {
			var expires = "";
			if (days) {
				var date = new Date();
				date.setTime(date.getTime()+(days*24*60*60*1000));
				var expires = "; expires="+date.toGMTString();
			}
			document.cookie = name+"="+value+expires+"; path=/";
		}
	};

	//Ouvrir un projet
	$("#HE-manager-control-open").click(function() {
		if(projetSelected) {					
			setCookie("projet_name", projetSelected, 1);
			setCookie("lic_HE", 1, 1);		
			window.location.href="Heroon Engine/index.php";	
		} else {
			jAlert("Vous n'avez pas selectionner de projet à ouvrir \n\n You did not select project to open", "Erreur");			
		}		
	});	

	//Créer un nouveau projet
	var newProject = function(projetname, msg)
	{
		var msg = msg.split(";"),
		version = msg[0],
		date = msg[1],						 
		poids = msg[2],
		nbrprojet = $("tbody").length + 1;		
		$("#throbber").hide();
		$("#listeproject").prepend('<tbody id="projet-'+nbrprojet+'" class="projet">'+					
			'<tr>'+					
				'<td style="text-align:left;height:10px;" name="'+projetname+'" id="'+projetname+'"><img id="deleteproject_'+projetname+'" src="Heroon Engine/Ressources/throbber.gif" style="display:none;margin-left:10px;margin-right:5px;" />'+projetname+'</td>'+
				'<td style="text-align:center;">'+version+'</td>'+
				'<td style="text-align:center;">'+date+'</td>'+
				'<td style="text-align:center;">'+poids+'</td>'+						
			'</tr>'+
		'</tbody>');		
		document.location.reload(true);
	};	
	$("#HE-manager-control-new").on('click', function() {
		jPrompt("Nom du nouveau projet:", "new projet", "Nommer le projet", function(projetname) {	
			if(projetname != null) {
				$("#throbber").show();
				$.ajax({ type: "POST",url: 'Heroon Engine/PHP/copy.php', data: "type=newproject&nameProject="+projetname,
					success: function (msg) {						
						newProject(projetname, msg);						
					}
				});				
			}
		});
	});
	
	//Copy un projet	
	$("#HE-manager-control-copy").on('click', function() {
		$("#throbber").show();	
		$.ajax({ type: "POST",url: 'Heroon Engine/PHP/copy.php', data: "type=copyproject&projetSelected="+projetSelected,
			success: function (msg) {
				newProject(projetname, msg);
			}
		});		
	});

	//Renomer un projet	
	var renameProject = function(newname)
	{
		$("#"+idSelected).find("td").attr("name", newname);
		$("#"+idSelected).find("td:first").html(newname)
		projetSelected = newname;
	};
	$("#HE-manager-control-rename").on('click', function() {
		if(projetSelected) {
			jPrompt("Nouveau nom du projet:", projetSelected, "Renommer le projet", function(newname) {	
				if(newname != null) {					
					$.ajax({ type: "POST",url: 'Heroon Engine/PHP/rename.php', data: "projetSelected="+projetSelected+"&newName="+newname+"&renameProjet=1",
						success: function() {
							renameProject(newname);
						}
					});					
				}
			});
		} else {
			jAlert("Vous n'avez pas selectionner le projet à renommer \n\n You do not select the project to be renamed", "Erreur");			
		}
	});

	//Supprimer un projet	
	$("#HE-manager-control-delete").on('click', function() {
		if(projetSelected) {
			$("deleteproject_"+projetSelected).show();
			jConfirm("Etes vous sûr de vouloir supprimer ce projet : "+projetSelected, "Confirmation", function() {					
				$.ajax({ type: "POST",url: 'Heroon Engine/PHP/delete.php', data: "projetSelected="+projetSelected+"&deleteProjet=1",
					success: function() {
						$("deleteproject_"+projetSelected).hide();
						$("#"+idSelected).remove();
					}
				});				
			});
		} else {
			jAlert("Vous n'avez pas selectionner le projet à supprimer \n\n You do not select the project to delete", "Erreur");			
		}
	});		
	
	//Selectionner un projet
	$("tbody").click(function() {
		$(".projet").css({"background": "white"});
		idSelected = $(this).attr("id");
		$("#"+idSelected).css({"background": "#bcff8e"});
		projetSelected = $("#"+idSelected).find("td").attr("name");
		
	});
	$("tbody").dblclick(function() {
		$(".projet").css({"background": "white"});
		idSelected = $(this).attr("id");
		$("#"+idSelected).css({"background": "#bcff8e"});
		projetSelected = $("#"+idSelected).find("td").attr("name");		
		if(projetSelected) {						
			setCookie("projet_name", projetSelected, 1);
			setCookie("lic_HE", 1, 1);			
			window.location.href="Heroon Engine/index.php";
		}		
	});	
	
	// Search update editor
	$.ajax({ type: "POST",url: 'Heroon Engine/PHP/searchUpdate.php',
		success: function(msg) {
			if(msg == 1) {				
				$("#HENewUpdate").css({"display":"inline-block"}).attr('onclick', "window.open('http://www.heroonengine.actifgames.com/HEROONENGINE/HeroonEngine.zip');");
			} else {
				$("#HENewUpdate").css({"display":"none"}).removeAttr('onclick');
			}
		}
	});
	
	$("#HE-manager-menu-select").selectmenu({ change: function( event, ui ) 
	{	
		var result = $(this).val();
		switch(result) {			
			case "convertproject":			
				$("#dialog-converter").dialog({
					modal: true,
					autoOpen: true,
					width: 600,
					height: 750,
					draggable: true,					
					close: function( event, ui ) {						
						$( this ).dialog("close");					
					},
					buttons: [{
						text: "OK", click: function() {
							$(this).dialog("close");
						}
					}]
				});
			break;
		}
	}});	
	
	console.log("HeroonEngine version "+ editorVersion+" - "+numVersion);
});