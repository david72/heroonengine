<?php
/*##################################################
 *                                actors.php
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 01/01/2017
 ###################################################

Créer des acteurs
 */
?>
<script> var apercuActor = null; </script>
<div id="actor_apparance_dialog"></div>
<div id="dialog-article_defaut" title="<?php echo $lang["actor"]["titleListeArticles"];?>">
	<div id="listeArticles"></div>
</div>
<div id="dialog-texture_mesh" title="<?php echo $lang["actor"]["titleListeTexturesObjet"];?>" style="display:none">
	<div id="selectImageFile" style="width:340px;height:200px;border:1px solid black;overflow-y:auto;"></div>
	<div id="listeMediasTextures" style="width:340px;height:400px;border:1px solid black;overflow-y:auto;"></div>
</div>
<div id="dialog-mesh_mesh" title="<?php echo $lang["actor"]["titleListeObjet"];?>" style="display:none">
	<div id="selectMeshFile" style="width:340px;height:200px;border:1px solid black;overflow-y:auto;"></div>
	<div id="listeMediasMesh" style="width:340px;height:400px;border:1px solid black;overflow-y:auto;"></div>
</div>
<table style="width:100%;" class="height-content-actor">
	<tr>
		<td style="width:15%">
			<div id="HE-accordion-actors">
				<h3><?php echo $lang["actor"]["PJ"];?></h3>
				<div id="player"></div>
				<h3><?php echo $lang["actor"]["PNJ"];?></h3>
				<div id="pnj"></div>
				<h3><?php echo $lang["actor"]["animaux"];?></h3>
				<div id="animaux"></div>
			</div>
		</td>
		<td style="padding-top:4px;padding-left:10px;width:48%;">
			<button id="HE-newActeur" style="margin-bottom:6px;"><?php echo $lang["button"]["newPJ"];?></button>
			<button id="HE-newActeurPNJ" style="margin-bottom:6px;"><?php echo $lang["button"]["newPNJ"];?></button>
			<button id="HE-newActeurAnimal" style="margin-bottom:6px;"><?php echo $lang["button"]["newAnimal"];?></button>
			<button id="HE-copyActeur" style="margin-bottom:6px;"><?php echo $lang["button"]["copy"];?></button>
			<button id="HE-deleteActeur" style="margin-bottom:6px;"><?php echo $lang["button"]["delete"];?></button><br />
			<br />
			<div style="display:inline-block;vertical-align:top;">
				<label class="actor"><?php echo $lang["actor"]["race"];?> :</label><input type="text" id="raceActeur" value="" placeholder="peuple humain" /><br />
				<label class="actor"><?php echo $lang["actor"]["classe"];?> :</label><input type="text" id="classActeur" value="" placeholder="sorcier" /><br />
				<label class="actor"><?php echo $lang["actor"]["sex"];?> :</label><select id="sexeActeur">
														<option value="Homme"><?php echo $lang["actor"]["optionHomme"];?></option>
														<option value="Femme"><?php echo $lang["actor"]["optionFemme"];?></option>
														<option value="HommeAndFemme"><?php echo $lang["actor"]["optionHommeAndFemme"];?></option>
													</select>
				<br />
				<label class="actor"><?php echo $lang["actor"]["zoneStart"];?> :</label><input type="text" id="ZoneStart" value="" placeholder="Islande" size="10" /><br />				
				<label class="actor"><?php echo $lang["actor"]["portailName"];?> :</label><input type="text" id="PortailName" value="" placeholder="Start" size="10" /><br />
				<label class="actor"><?php echo $lang["actor"]["comportement"];?> :</label><select id="AgressiviterType">
																<option value="passive">Passive</option>
																<option value="neutral">Neutral</option>
																<option value="defensive">Defensive</option>
																<option value="attacker">Attacker</option>
															</select>
															<?php echo $lang["actor"]["distanceRange"];?> : <input type="text" id="AgressiviterRange" value="" placeholder="5" size="2" /><br />
				<label class="actor">Faction :</label><select id="factionActor">
															<option value="none">None</option>
														</select><br />
				<label class="actor">XP <?php echo $lang["actor"]["multiplieur"];?>:</label><input type="text" id="XPActeur" value="10" size="2" /><br />
				<label class="actor"><?php echo $lang["actor"]["armeDefaut"];?> :</label><select id="armeDefautActor">
																	<option value="none">None</option>
																</select><br />
				<label class="actor"><?php echo $lang["actor"]["commerceMode"];?> :</label><select id="PNJMode">
																<option value="non-comercant"><?php echo $lang["actor"]["optionNoCommercant"];?></option>
																<option value="commercant"><?php echo $lang["actor"]["optionCommercant"];?></option>
															</select><br />
				<label class="actor"><?php echo $lang["actor"]["animationType"];?> :</label><select id="AnnimationType">
																<option value="none">None</option>
															</select><br />
				<label class="actor"><?php echo $lang["actor"]["sound"];?> :</label><input type="button" value="None" style="width:140px;" id="ActorSound" onClick="$('#dialog-add-sounds').data('opener', this).dialog('open');" /><br />											
				<label class="actor"><?php echo $lang["actor"]["actorApparence"];?>:</label><fieldset style="display:inline-block;width:310px;"><legend></legend>
																		<label class="actor"><?php echo $lang["actor"]["actorApparence_meshBody"];?> : </label><button class="apparence" id="apparence-corp" onClick="$('#dialog-add-meshes').data('opener', this).dialog('open');" style="width:140px;white-space:nowrap;direction:rtl;"><?php echo $lang["button"]["choisir"];?></button><br />
																		<label class="actor"><?php echo $lang["actor"]["actorApparence_meshHair"];?> : </label><button class="apparence" id="apparence-hair" onClick="$('#dialog-mesh-hair').data('opener', this).dialog('open');" style="width:140px;white-space:nowrap;direction:rtl;"><?php echo $lang["button"]["editer"];?></button><br />
																		<div id="BeardForMan"><label class="actor"><?php echo $lang["actor"]["actorApparence_meshBeard"];?> : </label><button class="apparence" id="apparence-beard" onClick="$('#dialog-mesh-beard').data('opener', this).dialog('open');" style="width:140px;white-space:nowrap;direction:rtl;"><?php echo $lang["button"]["editer"];?></button><div>
																		<label class="actor"><?php echo $lang["actor"]["actorApparence_textureHead"];?> : </label><button class="apparence" id="apparence-visage" onClick="$('#dialog-textures-head').data('opener', this).dialog('open');" style="width:140px;white-space:nowrap;direction:rtl;"><?php echo $lang["button"]["editer"];?></button><br />
																		<label class="actor"><?php echo $lang["actor"]["actorApparence_textureTenu"];?> : </label><button class="apparence" id="apparence-tenu" onClick="$('#dialog-textures-wear').data('opener', this).dialog('open');" style="width:140px;white-space:nowrap;direction:rtl;"><?php echo $lang["button"]["editer"];?></button><br />
																		<label class="actor"><?php echo $lang["actor"]["actorApparence_armureDefaut"];?> : </label><button class="apparence" id="apparence-armure" onClick="$('#dialog-article_defaut').data('opener', this).dialog('open');" style="width:140px;white-space:nowrap;direction:rtl;"><?php echo $lang["button"]["choisir"];?></button><br />
																	</fieldset><br /><br />
				<label class="actor"><?php echo $lang["actor"]["descriptionActor"];?> :</label><textarea id="descriptionActeur" cols="44" rows="4"></textarea>
			</div>
			<div style="display:inline-block;text-align:center;padding-left:10px;">
				<?php echo $lang["actor"]["aptitudeActor"];?> :<br /><fieldset style="display:inline-block;width:300px;">
																		<center>
																			<button style="margin-bottom:5px;" id="addAptitudeActor"><?php echo $lang["button"]["addAptitude"];?></button>
																			<button style="margin-bottom:5px;" id="removeAptitudeActor"><?php echo $lang["button"]["deleteAptitude"];?></button>
																		</center>
																		<ol class="selectable" id="listeAptitude">
																			<li class="ui-widget-content" id="index" value="Santé">Santé</li>
																			<li class="ui-widget-content" id="index" value="Nager">Nager</li>
																		</ol>
																		<label class="base" style="margin-top:5px;"><?php echo $lang["actor"]["name"];?> :</label>&nbsp;<input type="text" id="attrDefautName" value="" size="12" /><br />
																		<label class="base"><?php echo $lang["actor"]["value"];?> :</label>&nbsp;<input type="text" id="attrDefautValue" value="" size="12" />
																	</fieldset><br />
				<br />
				<?php echo $lang["actor"]["resistanceActor"];?> :<br /><fieldset style="display:inline-block;width:300px;">
																		<center>
																			<button style="margin-bottom:5px;" id="addResistanceActor"><?php echo $lang["button"]["addResistance"];?></button>
																			<button style="margin-bottom:5px;" id="removeResistanceActor"><?php echo $lang["button"]["deleteResistance"];?></button>
																		</center>
																		<ol class="selectable" id="listeResistance">
																			<li class="ui-widget-content" id="index" value="Eau">Eau</li>
																		</ol>
																		<label class="base" style="margin-top:5px;"><?php echo $lang["actor"]["name"];?> :</label>&nbsp;<input type="text" id="resistDefautName" value="" size="12" /><br />
																		<label class="base"><?php echo $lang["actor"]["value"];?> :</label>&nbsp;<input type="text" id="resistDefautValue" value="" size="12" />
																	</fieldset>
			</div>
		</td>
		<td style="padding-top:35px;width:37%;">
			<?php echo $lang["actor"]["apercuActor"];?> :<br /><br />
			<canvas id="HE-canvas-appercu-actor" style="z-index:1;"></canvas>
			<div id="control_material" style="width:150px;height:50px;position:absolute;top:128px;margin-left:545px;z-index:10000;color:white;"></div>
		</td>
	</tr>
</table>
<script>
$(function() {
	$("#HE-canvas-appercu-actor").css({"height": $("body").height() - 220 +"px"});
	$("#HE-accordion-actors").accordion({heightStyle: "fill"});
	setTimeout(function() {
		$("table.height-content-actor").css({"height": $("body").height() - 200 +"px"});
	}, 3000);
});
</script>
<script src="JS/actors/actors.js?<?php echo time();?>" type="text/javascript"></script>
<script>
apercuActor = new actor();
apercuActor.scene();
</script>