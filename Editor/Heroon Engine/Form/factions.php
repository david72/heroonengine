<?php
/*##################################################
 *                                factions.php
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 01/01/2017
 ###################################################

Créer les factions des acteurs
 */
?>
<button onClick="newFaction();"><?php echo $lang["button"]["create"];?></button>
<button onClick="deleteFaction();"><?php echo $lang["button"]["delete"];?></button>
&nbsp; <?php echo $lang["faction"]["factionName"];?> :<input type="text" id="factionName" value="" /><br />
<br />
<div id="refrechFaction"></div>
<script src="JS/actors/factions.js?<?php echo time();?>" type="text/javascript"></script>