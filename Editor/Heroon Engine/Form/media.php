<?php session_start();
/*##################################################
 *                media.php
 *              -------------------
 *  copyright      : (C) 2017 Pellier David (dad72)
 *  email          : dad72@heroonengine.com
 *  Revision       : 23/07/2017
 ###################################################

Ajouter des medias pour le jeu
 */
?>
<script>
var mediaGlobal = {
		media: null, objetSelected: null
	};
</script>
<div class="HE-liste-media">
	<!-- Liste des medias -->
	<button style="width:111px;margin-top:5px;margin-bottom:6px;font-size:12px;" id="importMedia3d" onClick="$('#dialog-import-mesh').dialog('open');" ><?php echo $lang["button"]["import_mesh"];?></button>
	<button style="width:111px;margin-top:5px;margin-bottom:6px;font-size:12px;" id="importMediaImage" onClick="$('#dialog-import-image').dialog('open');" ><?php echo $lang["button"]["import_images"];?></button>
	<button style="width:111px;margin-top:5px;margin-bottom:6px;font-size:12px;" id="importMediaMusic" onClick="$('#dialog-import-music').dialog('open');" ><?php echo $lang["button"]["import_music"];?></button>
	<br />
	<button style="width:111px;margin-top:5px;margin-bottom:6px;font-size:12px;" id="importMediaSound" onClick="$('#dialog-import-sound').dialog('open');" ><?php echo $lang["button"]["import_son"];?></button>
	<button style="width:111px;margin-top:5px;margin-bottom:6px;font-size:12px;" id="importMediaVideo" onClick="$('#dialog-import-video').dialog('open');" ><?php echo $lang["button"]["import_video"];?></button><br />
	<div id="CategorieMedia" style="width:100%;height:325px;border:1px solid black;overflow-y:auto;"></div>
	<button style="width:99px;margin-top:5px;" id="newCategorieMedia"><?php echo $lang["button"]["add"];?></button>
	<button style="width:99px;margin-top:5px;" id="deleteCategorieMedia"><?php echo $lang["button"]["delete"];?></button><br /><br />
	<div id="listeMedias" style="width:365px;height:440px;border:1px solid black;overflow-y:auto;"></div>
</div>
<div class="HE-display-media">
	<!-- Affichage des medias -->
	<canvas id="HE-canvas-media"></canvas>
	<canvas id="HE-canvas-media-image" style="display:none;"></canvas>
	<div style="position:absolute;top:60px;right:50px;">Auto rotation : <input type="checkbox" id="autoRotation" value="1" checked="true" /></div>
</div>
<script>
mediaGlobal.media = new assets();
mediaGlobal.media.assetMedia();
mediaGlobal.media.jquery();
</script>