<?php
/*##################################################
 *                                attributs.php
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 01/01/2017
 ###################################################

Créer des aptitudes/competances au acteurs
 */
?>
<table>
	<tr>
		<td valign="top" width="220px">
			<div id="accordion-competances" style="overflow:auto">
				<h3><?php echo $lang["competance"]["listeCompetance"];?></h3>
				<div id="listeAbiliter"></div>
			</div>
		</td>
		<td valign="top">
			<div style="margin-left:10px;margin-top:4px;">
				<button id="newAbility"><?php echo $lang["button"]["create"];?></button>
				<button id="deleteAbility"><?php echo $lang["button"]["delete"];?></button><br />
				<br />			
				<label class="base"><?php echo $lang["generality"]["name"];?> :</label> <input type="text" id="abilityName" value="" placeholder="Ghost rider" /> <?php echo $lang["competance"]["infoName"];?><br />
				<label class="base"><?php echo $lang["competance"]["description"];?> :</label> <input type="text" id="abilityDescription" size="100" value="" placeholder="<?php echo $lang["competance"]["placeholder"];?>" /><br />
				<label class="base"><?php echo $lang["generality"]["icone"];?> :</label> <input type="button" onClick="$('#dialog-add-images').data('opener', this).dialog('open');" id="icone" value="None" style="white-space:nowrap;direction:rtl;" /><br />
				<label class="base"><?php echo $lang["competance"]["durer"];?> :</label> <input id="spinnerTMPDurer" class="spinner" name="spinner" value="0" size="4" /> <?php echo $lang["competance"]["timeBySeconde"];?><br />
				<label class="base"><?php echo $lang["competance"]["rechargement"];?> :</label> <input id="spinnerTMPRecharge" class="spinner" name="spinner" value="0" size="4" /> <?php echo $lang["competance"]["timeBySeconde"];?><br />
				<fieldset><legend><?php echo $lang["generality"]["legendExlusivity"];?></legend>
					<label class="base"><?php echo $lang["actor"]["race"];?> :</label><select id="exclusivRace_competance">
											<option value="0">None</option>
										</select><br />
					<label class="base"><?php echo $lang["actor"]["classe"];?> :</label><select id="exclusivClass_competance">
											<option value="0">None</option>	
										 </select><br />
				</fieldset>
				<br />
				<fieldset><legend><?php echo $lang["generality"]["legendScriptPero"];?></legend>
					<label class="base"><?php echo $lang["generality"]["scriptToUse"];?> :</label><select id="script_competance">
															<option value="0">None</option>
															<?php echo listeScripts($projet_name);?>
														</select><br />
				</fieldset>
			</div>
		</td>
	</tr>
</table>
<script>
$(function() {
	$("#accordion-competances").accordion({heightStyle: "fill"});
	$("#accordion-competances").accordion("option", "icons", null);
	$("#listeAbiliter").css("height", $("body").height() - 200+"px");
});
</script>
<script src="JS/actors/competances.js?<?php echo time();?>" type="text/javascript"></script>