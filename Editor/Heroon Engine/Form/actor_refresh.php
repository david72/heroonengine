<?php
$projet_name = @$_COOKIE["projet_name"];
$json_get = json_decode(file_get_contents("../Data Project/editor.json"), true);
$getlang = $json_get["lang"];	
$lang = json_decode(file_get_contents("../Lang/".$getlang.".lng.json"), true);
if(@$_SESSION['actor'] == false) $_SESSION['actor'] = "race.classe";
$json = json_decode(file_get_contents("../_Projects/".$projet_name."/game data/actors/".$_SESSION['actor'].".json"), true);
$nbrSlots = 10;
?>
<div id="dialog-textures-head" title="<?php echo $lang["actor"]["titleAddVisage"];?>" style="display:none;">
	<table border style="width:600px;height:auto">
		<tr style="background-color:#d0d0d0;">
			<th>n°</th>
			<th>Diffuse</th>
			<th>Bump<br /><?php echo $lang["actor"]["facultatif"];?></th>
			<th>Specular<br /><?php echo $lang["actor"]["facultatif"];?></th>
		</tr>
		<?php		
		for($i = 0; $i < $nbrSlots; $i++) {
			$headD = @$json["apparence"]["texture_face"][$i]['diffuse'];
			$headB = @$json["apparence"]["texture_face"][$i]['bump'];
			$headS = @$json["apparence"]["texture_face"][$i]['specular'];
			?>
			<tr>
				<td align="center" style="width:20px;background-color:#d0d0d0;"><?php echo $i+1;?></td>
				<td align="center"><button style="width:140px;height:25px;white-space:nowrap;direction:rtl;" onClick="apercuActor.assignTexture(<?php echo $i;?>, 'diffuse', 'head');" id="canal_diffuse-slot_<?php echo $i;?>-head"><?php echo $headD ? $headD : "None";?></button></td>
				<td align="center"><button style="width:140px;height:25px;white-space:nowrap;direction:rtl;" onClick="apercuActor.assignTexture(<?php echo $i;?>, 'normal', 'head');" id="canal_normal-slot_<?php echo $i;?>-head"><?php echo $headB ? $headB : "None";?></button></td>
				<td align="center"><button style="width:140px;height:25px;white-space:nowrap;direction:rtl;" onClick="apercuActor.assignTexture(<?php echo $i;?>, 'specular', 'head');" id="canal_specular-slot_<?php echo $i;?>-head"><?php echo $headS ? $headS : "None";?></button></td>
			</tr>
			<?php
		}
		?>
	</table>
</div>
<div id="dialog-textures-wear" title="<?php echo $lang["actor"]["titleAddTenu"];?>" style="display:none;">
	<table border style="width:600px;height:auto">
		<tr style="background-color:#d0d0d0;">
			<th>n°</th>
			<th>Diffuse</th>
			<th>Bump<br /><?php echo $lang["actor"]["facultatif"];?></th>
			<th>Speculaire<br /><?php echo $lang["actor"]["facultatif"];?></th>
		</tr>
		<?php
		for($i = 0; $i < $nbrSlots; $i++) {
			$tenuD = @$json["apparence"]["texture_tenu"][$i]['diffuse'];
			$tenuB = @$json["apparence"]["texture_tenu"][$i]['bump'];
			$tenuS = @$json["apparence"]["texture_tenu"][$i]['specular'];
			?>
			<tr>
				<td align="center" style="width:20px;background-color:#d0d0d0;"><?php echo $i+1;?></td>
				<td align="center"><button style="width:140px;height:25px;white-space:nowrap;direction:rtl;" onClick="apercuActor.assignTexture(<?php echo $i;?>, 'diffuse', 'wear');" id="canal_diffuse-slot_<?php echo $i;?>-wear"><?php echo $tenuD ? $tenuD : "None";?></button></td>
				<td align="center"><button style="width:140px;height:25px;white-space:nowrap;direction:rtl;" onClick="apercuActor.assignTexture(<?php echo $i;?>, 'normal', 'wear');" id="canal_normal-slot_<?php echo $i;?>-wear"><?php echo $tenuB ? $tenuB : "None";?></button></td>
				<td align="center"><button style="width:140px;height:25px;white-space:nowrap;direction:rtl;" onClick="apercuActor.assignTexture(<?php echo $i;?>, 'specular', 'wear');" id="canal_specular-slot_<?php echo $i;?>-wear"><?php echo $tenuS ? $tenuB : "None";?></button></td>
			</tr>
			<?php
		}
		?>
	</table>
</div>
<div id="dialog-mesh-hair" title="<?php echo $lang["actor"]["titleAddHair"];?>" style="display:none;">
	<table border style="width:200px;height:auto">
		<tr style="background-color:#d0d0d0;">
			<th>n°</th>
			<th>Mesh</th>			
		</tr>
		<?php
		for($i = 0; $i < $nbrSlots; $i++) {
			$mesh = @$json["apparence"]["hair"][$i]['mesh'];
			?>
			<tr>
				<td align="center" style="width:20px;background-color:#d0d0d0;"><?php echo $i+1;?></td>
				<td align="center"><button style="width:140px;height:25px;white-space:nowrap;direction:rtl;" onClick="apercuActor.assignMesh(<?php echo $i;?>, 'hair');" id="canal_mesh-slot_<?php echo $i;?>-hair"><?php echo $mesh ? $mesh : "None";?></button></td>				
			</tr>
			<?php
		}
		?>
	</table>
</div>
<div id="dialog-mesh-beard" title="<?php echo $lang["actor"]["titleAddBeard"];?>" style="display:none;">
	<table border style="width:200px;height:auto">
		<tr style="background-color:#d0d0d0;">
			<th>n°</th>
			<th>Mesh</th>			
		</tr>
		<?php
		for($i = 0; $i < $nbrSlots; $i++) {
			$mesh = @$json["apparence"]["beard"][$i]['mesh'];
			?>
			<tr>
				<td align="center" style="width:20px;background-color:#d0d0d0;"><?php echo $i+1;?></td>
				<td align="center"><button style="width:140px;height:25px;white-space:nowrap;direction:rtl;" onClick="apercuActor.assignMesh(<?php echo $i;?>, 'beard');" id="canal_mesh-slot_<?php echo $i;?>-beard"><?php echo $mesh ? $mesh : "None";?></button></td>				
			</tr>
			<?php
		}
		?>
	</table>
</div>
<script>
$("#dialog-mesh-hair").dialog({
	modal: false,
	autoOpen: false,
	width: 230,	
	closeText: "",
	buttons: [{
		text: lang.button.js.ok, click: function() {
			$(this).dialog( "close" );
		}
	}]
});

$("#dialog-mesh-beard").dialog({
	modal: false,
	autoOpen: false,
	closeText: "",
	width: 230,			
	buttons: [{
		text: lang.button.js.ok, click: function() {
			$(this).dialog( "close" );
		}
	}]
});

$("#dialog-textures-wear").dialog({
	modal: false,
	autoOpen: false,
	closeText: "",
	width: 630,
	buttons: [{
		text: lang.button.js.ok, click: function() {
			$(this).dialog( "close" );
		}
	}]
});

$("#dialog-textures-head").dialog({
	modal: false,
	autoOpen: false,
	closeText: "",
	width: 630,
	buttons: [{
		text: lang.button.js.ok, click: function() {
			$(this).dialog( "close" );
		}
	}]
});
</script>