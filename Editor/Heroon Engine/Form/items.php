<?php
/*##################################################
 *                                items.php
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 01/01/2017
 ###################################################

Créer des Items a ramasser ou a trouver sur le jeu
 */
$json = json_decode(file_get_contents("_Projects/".$projet_name."/game data/items.json"), true);
$nbrSlots = 20;
?>
<div id="dialog-add-damage" title="<?php echo $lang["items"]["titleDialogDamage"];?>">
	<table border style="width:200px;height:auto">
		<tr style="background-color:#d0d0d0;">
			<th>n°</th>
			<th>type</th>			
		</tr>
		<?php		
		for($i = 0; $i < $nbrSlots; $i++) {
			$type = @$json["apparence"]["listeDamage"][$i]['value'];	
			?>
			<tr>
				<td align="center" style="width:20px;background-color:#d0d0d0;"><?php echo $i+1;?></td>
				<td align="center"><input type="text" style="width:160px;height:20px;" onChange="saveDamage(<?php echo $i;?>);" class="slot_damage" onClick="assignDamage(<?php echo $i;?>);" id="canal_damage-slot_<?php echo $i;?>" value="<?php echo $type ? $type : "";?>" /></td>
			</tr>
			<?php
		}
		?>
	</table>
</div>
<table width="100%" class="height-content-items">
	<tr>
		<td width="220px">
			<div id="accordion-items">
				<h3><?php echo $lang["items"]["myItems"];?></h3><div id="listeItems"></div>
			</div>
		</td>
		<td width="40%" style="padding-top:5px;padding-left:10px">
			<button id="newItems"><?php echo $lang["button"]["create"];?></button>
			<button id="copyItems"><?php echo $lang["button"]["copy"];?></button>
			<button id="deleteItems"><?php echo $lang["button"]["delete"];?></button><br />
			<br />
			<label class="base"><?php echo $lang["generality"]["name"];?> :</label><input type="text" id="itemName" value="" placeholder="épée" />  <?php echo $lang["items"]["infoNameItems"];?>
			<br />
			<label class="base"><?php echo $lang["generality"]["icone"];?> :</label><input type="button" onClick="$('#dialog-add-images').data('opener', this).dialog('open');" style="width:140px;white-space:nowrap;direction:rtl;" id="icone_item" value="None" />
			<br />
			<label class="base"><?php echo $lang["generality"]["mesh"];?></label><input type="button" onClick="$('#dialog-add-meshes').data('opener', this).dialog('open');" style="width:140px;white-space:nowrap;direction:rtl;" id="mesh_item" value="None" />
			<br />
			<label class="base"><?php echo $lang["items"]["labelType"];?> :</label><select id="ItemType">
																					<option value="0">None</option>
																					<option value="Arme"><?php echo $lang["article"]["optionArme"];?></option>
																					<option value="Armure"><?php echo $lang["article"]["optionArmure"];?></option>
																					<option value="Potion"><?php echo $lang["items"]["optionPotion"];?></option>
																					<option value="Nouriture"><?php echo $lang["items"]["optionNouriture"];?></option>
																					<option value="Image"><?php echo $lang["items"]["optionImages"];?></option>
																					<option value="Argent"><?php echo $lang["items"]["optionArgent"];?></option>
																				</select>
			<br />
			<label class="base">Intentaire :</label><select id="rangeSlotIntentaire">
														<option value="0">None</option>
														<option value="head"><?php echo $lang["article"]["optionAttacheArticleOn_head"];?></option>
														<option value="chest"><?php echo $lang["article"]["optionAttacheArticleOn_chest"];?></option>
														<option value="r_shoulder"><?php echo $lang["article"]["optionAttacheArticleOn_r_shoulder"];?></option>
														<option value="l_shoulder"><?php echo $lang["article"]["optionAttacheArticleOn_l_shoulder"];?></option>
														<option value="pelvis"><?php echo $lang["article"]["optionAttacheArticleOn_pelvis"];?></option>
														<option value="r_forearm"><?php echo $lang["article"]["optionAttacheArticleOn_r_forearm"];?></option>
														<option value="l_forearm"><?php echo $lang["article"]["optionAttacheArticleOn_l_forearm"];?></option>
														<option value="r_hand"><?php echo $lang["article"]["optionAttacheArticleOn_r_hand"];?></option>
														<option value="l_hand"><?php echo $lang["article"]["optionAttacheArticleOn_l_hand"];?></option>
														<option value="r_shin"><?php echo $lang["article"]["optionAttacheArticleOn_r_shin"];?></option>
														<option value="l_shin"><?php echo $lang["article"]["optionAttacheArticleOn_l_shin"];?></option>
														<option value="r_footer"><?php echo $lang["article"]["optionAttacheArticleOn_r_footer"];?></option>
														<option value="l_footer"><?php echo $lang["article"]["optionAttacheArticleOn_l_footer"];?></option>
													</select>
			<br />
			<label class="base"><?php echo $lang["actor"]["value"];?> :</label><input type="text" class="spinner" id="valueItem" value="" size="4" placeholder="50" />
			<br />
			<label class="base"><?php echo $lang["items"]["poids"];?> :</label><input type="text" class="spinner" id="poidsItem" value="" size="4" placeholder="600" /> G
			<br />
			<label class="item"><?php echo $lang["items"]["itemCanBeStacked"];?> :</label><input type="checkbox" id="itemCanBeStacked" value="1" /> <?php echo $lang["items"]["infoItemCanBeStacked"];?>
			<br />
			<label class="item"><?php echo $lang["items"]["itemCanBeDamaged"];?> :</label><input type="checkbox" id="itemCanBeDamaged" value="1" /> &nbsp; <?php echo $lang["items"]["infoItemCanBeDamaged"];?> : <input type="text" class="spinner" id="itemQuantityDamage" value="" size="4" placeholder="5" />/100%
			<br />
			<label class="item"><?php echo $lang["items"]["itemCanBeRepair"];?> :</label><input type="checkbox" id="itemCanBeRepair" value="1" /> &nbsp; <?php echo $lang["items"]["infoItemCanBeRepair"];?> : <input type="text" class="spinner" id="itemCoutRepair" value="" size="4" placeholder="100" />
			<br />
			<fieldset><legend><?php echo $lang["generality"]["legendExlusivity"];?></legend>
				<label class="base"><?php echo $lang["actor"]["race"];?> :</label><select id="exclusivRace_items">
																					<option value="0">None</option>
																				</select>
				<br />
				<label class="base"><?php echo $lang["actor"]["classe"];?> :</label><select id="exclusivClass_items">
																					<option value="0">None</option>
																				</select>
				<br />
			</fieldset>
			<br />
			<fieldset><legend><?php echo $lang["generality"]["legendScriptPero"];?></legend>
				<label class="base"><?php echo $lang["generality"]["scriptToUse"];?> :</label><select id="script_items">
																								<option value="0">None</option>
																								<?php echo listeScripts($projet_name);?>
																							</select><br />
			</fieldset>
		</td>
		<td style="padding-top:35px;padding-left:30px;">
			<div id="ParamArme">
				<fieldset style="margin-left:15px;width:340px"><legend><?php echo $lang["items"]["legendParametreArme"];?></legend>
					<label class="base"><?php echo $lang["items"]["labelNbrDegat"];?> :</label><input type="text" class="spinner" id="armeDamaged" value="1" size="4" />
					<br />
					<label class="base"><?php echo $lang["items"]["labelTypeDegat"];?> :</label><input type="button" id="typeDamage" onClick="$('#dialog-add-damage').data('opener', this).dialog('open');" style="width:140px" value="None" />
					<br />
					<label class="base"><?php echo $lang["items"]["labelProjectileMesh"];?> :</label><input type="button" id="projectileMesh" onClick="$('#dialog-add-meshes').data('opener', this).dialog('open');" style="width:140px" value="None" />
					<br />
					<label class="base"><?php echo $lang["items"]["labelParticleStart"];?> :</label><select id="startParticuleProjectiles">
																										<option value="0">None</option>
																										<?php echo listeParticles($projet_name); ?>
																									</select>
					<br />
					<label class="base"><?php echo $lang["items"]["labelParticleCible"];?> :</label><select id="cibleParticuleProjectile">
																										<option value="0">None</option>
																										<?php echo listeParticles($projet_name); ?>
																									</select>
					<br />
					<label class="base"><?php echo $lang["items"]["labelSpeed"];?> :</label><input type="text" class="spinner" id="speedProjectile" value="65" size="4" /> %
					<br />
					<label class="base"><?php echo $lang["items"]["labelChangeReussite"];?> :</label><input type="text" class="spinner" id="chanceGoCible" value="80" size="4" /> %
				</fieldset>
			</div>
			<div id="ParamArmure">
				<fieldset style="margin-left:15px;width:340px"><legend><?php echo $lang["items"]["legendParametreArmure"];?></legend>
					<label class="base"><?php echo $lang["items"]["labelNiveau"];?> :</label><input type="text" class="spinner" id="niveauArmureParam" value="" size="4" />
				</fieldset>
			</div>
			<div id="ParamPotion">
				<fieldset style="margin-left:15px;width:340px"><legend><?php echo $lang["items"]["legendParametrePotion"];?></legend>
					<label class="base"><?php echo $lang["items"]["labelDurer"];?> :</label><input type="text" class="spinner" id="effectDurerParam" value="" size="4" />
				</fieldset>
			</div>
			<div id="ParamImage">
				<fieldset style="margin-left:15px;width:340px"><legend><?php echo $lang["items"]["legendParametreImage"];?></legend>
					<label class="base"><?php echo $lang["items"]["labelImage"];?> :</label><input type="button" onClick="$('#dialog-add-images').dialog('open');" style="width:140px" id="image_param_item" value="None" />
				</fieldset>
			</div>
		</td>
	</tr>
</table>
<script src="JS/items.js?<?php echo time();?>" type="text/javascript"></script>
<script>
$(function() {
	$(".height-content-items").css({"height": $("body").height() - 135 +"px"});
	$("#listeItems").css("height", $("body").height() - 135 +"px");
});
</script>