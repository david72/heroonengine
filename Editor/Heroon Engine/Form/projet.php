<?php
/*##################################################
 *        projets.php
 *       -------------------
 * copyright   : (C) 2017 Pellier David (dad72)
 * email    : dad72@heroonengine.com
 * Revision    : 01/01/2017
 ###################################################

Recapitulatifs et gestions du projet
*/
?>
<div id="dialog-addon" title="<?php echo $lang["projet"]["labelAddon"];?>">	
	<table id="liste_addon" border="1" width="100%">
		<tr>
			<th width="170px">Nom</th>			
			<th>Description</th>
			<th width="100px">Installer</th>
			<th width="100px">Desinstaller</th>
		</tr>		
	</table>
</div>
<div id="dialog-add-collaborateur" title="<?php echo $lang["projet"]["titleAddCollaborateur"];?>">
	<label class="base"><?php echo $lang["projet"]["userCollaborateur"];?> :</label><input type="text" id="collaborateur_name" />
	<label class="base"><?php echo $lang["projet"]["passwordCollaborateur"];?> :</label><input type="text" id="collaborateur_pass" />
</div>
<div id="dialog-config-collaborateur" title="<?php echo $lang["projet"]["titleEditeCollaborateur"];?>">
	<p><?php echo $lang["projet"]["whatEditCollaborateur"];?> ?</p>
	<table id="liste_collaborateur" border="1" width="100%">
		<tr>
			<th><?php echo $lang["projet"]["collaborateurs"];?></th>
			<th><?php echo $lang["GE"]["tabProject"];?></th>
			<th><?php echo $lang["GE"]["tabActors"];?></th>
			<th><?php echo $lang["GE"]["tabItems"];?></th>
			<th><?php echo $lang["GE"]["tabZones"];?></th>
			<th><?php echo $lang["GE"]["tabParticle"];?></th>			
			<th><?php echo $lang["GE"]["tabScripts"];?></th>
		</tr>
		<tbody id="config-collaborateur_0" class="conf">
			<?php
			$file_editor = json_decode(file_get_contents("Data Project/editor.json"), true);
			for($i = 0; $i < count($file_editor["collaborateurs"]["users"]); $i++) {				
				if($file_editor["collaborateurs"]["users"][$i]["pseudo"] != "admin") {
					$canEditeProject = "";
					$canEditeActeurs = "";
					$canEditeItems = "";
					$canEditeZones = "";
					$canEditeParticules = "";
					$canEditeInterface = "";
					$canEditeScripts = "";
					if($file_editor["collaborateurs"]["users"][$i]["canEditeProject"] == "true") $canEditeProject = "checked";
					if($file_editor["collaborateurs"]["users"][$i]["canEditeActeurs"] == "true") $canEditeActeurs = "checked";
					if($file_editor["collaborateurs"]["users"][$i]["canEditeItems"] == "true") $canEditeItems = "checked";
					if($file_editor["collaborateurs"]["users"][$i]["canEditeZones"] == "true") $canEditeZones = "checked";
					if($file_editor["collaborateurs"]["users"][$i]["canEditeParticules"] == "true") $canEditeParticules = "checked";
					if($file_editor["collaborateurs"]["users"][$i]["canEditeScripts"] == "true") $canEditeScripts = "checked";
					?>
					<tr>
						<td align="center"><?php echo $file_editor["collaborateurs"]["users"][$i]["pseudo"];?></td>
						<td align="center"><input type="checkbox" id="canEditeProject" value="1" <?php echo $canEditeProject;?> /></td>
						<td align="center"><input type="checkbox" id="canEditeActeurs" value="1" <?php echo $canEditeActeurs;?> /></td>
						<td align="center"><input type="checkbox" id="canEditeItems" value="1" <?php echo $canEditeItems;?> /></td>
						<td align="center"><input type="checkbox" id="canEditeZones" value="1" disabled <?php echo $canEditeZones;?> /></td>
						<td align="center"><input type="checkbox" id="canEditeParticules" value="1" <?php echo $canEditeParticules;?> /></td>
						<td align="center"><input type="checkbox" id="canEditeScripts" value="1" <?php echo $canEditeScripts;?> /></td>
					</tr>
					<?php
				}				
			}
			?>
		</tbody>
	</table>
</div>
<table style="width:100%;" cellspacing="0" cellpadding="0">
	<tr>
		<td style="width:450px;" valign="top">
			<fieldset style="width:440px;display:block;height:80px;"><legend><?php echo $lang["projet"]["legendTesteGame"];?></legend>				
				<p style="margin-top:1px;"><?php echo $lang["projet"]["labelTesteGame"];?> :</p>
				<center><button id="HE-start-game" style="width:95%;"><?php echo $lang["button"]["startGame"];?></button></center>			
			</fieldset>
			<br />			
			<fieldset style="width:440px;display:block;"><legend><?php echo $lang["projet"]["legendUpdateGame"];?></legend>
				<label for="update" class="projet"><?php echo $lang["projet"]["labelEnvoyerUpdatePublic"];?> :</label><button id="HE-update-public"><?php echo $lang["button"]["envoyerRelease"];?></button>
				<label for="update" class="projet"><?php echo $lang["projet"]["labelFolderPublic"];?> :</label><input type="text" id="HE-folder-public" value="Game/" />
				<label for="update" class="projet"><?php echo $lang["projet"]["labelNbrActorParCompte"];?> :</label><input type="text" id="HE-nbrActor-public" value="5" />
				<hr /><br />
				<label for="update" class="projet"><?php echo $lang["projet"]["labelEnvoyerUpdateBeta"];?> :</label><button id="HE-update-beta"><?php echo $lang["button"]["envoyerBeta"];?></button>
				<label for="update" class="projet"><?php echo $lang["projet"]["labelFolderBeta"];?> :</label><input type="text" id="HE-folder-beta" value="Beta/" />
				<label for="update" class="projet"><?php echo $lang["projet"]["labelNbrActorParCompte"];?> :</label><input type="text" id="HE-nbrActor-beta" value="1" />
			</fieldset>
			<br />	
			<fieldset style="width:440px;display:block;"><legend><?php echo $lang["projet"]["legendAddon"];?></legend>			
				<label for="update" class="projet"><?php echo $lang["projet"]["labelAddon"];?> :</label><button id="HE-update-beta" onCLick="$('#dialog-addon').dialog('open');"><?php echo $lang["projet"]["buttonAddon"];?></button>		
			</fieldset>
		</td>
		<td valign="top">
			<table>
				<tr>
					<td valign="top">
						<fieldset style="width:440px;display:block;height:80px;"><legend><?php echo $lang["projet"]["legendEditor"];?></legend>
							<label for="change-langue" class="projet"><?php echo $lang["projet"]["labelChangeLanguage"];?> :</label><select id="HE-LanguageDefaut" name="HE-LanguageDefaut" style="width:200px;">
								<?php
								$dirname = 'Lang/';
								$dir = opendir($dirname);
								while($file = readdir($dir)) {
									if($file != '.' && $file != '..' && $file != 'images') {
										$file = str_replace('.lng.json', '', $file);
										$extension=pathinfo($dirname.$file, PATHINFO_EXTENSION);
										if($extension != "json") {
											echo '<option value="'.$file.'" data-image="'.$dirname.'images/'.$file.'.png">'.$file.'</option>';
										}
									}
								} closedir($dir);
								?>
							</select>
							<br />
							<label for="change-theme" class="projet"><?php echo $lang["projet"]["labelChangeTheme"];?> :</label><select id="HE-theme" style="width:200px;">
								<option value="White" data-color="white">White</option>
								<option value="Blue" data-color="#c3d4e6">Blue</option>
								<option value="Black" data-color="#383838">Black</option>
							</select>
						</fieldset>
						<br />
						<fieldset style="width:440px;display:block;height:80px;"><legend><?php echo $lang["projet"]["legendCollaboration"];?></legend>
							<?php
							$disabled = "";
							if(@$_SESSION['user'] != "admin") {
								$disabled = "disabled";
							}
							?>
							<label for="add-collaborateurs" class="projet"><?php echo $lang["projet"]["labelAddCollaborateur"];?> :</label><button id="HE-add-collaborateur" <?php echo $disabled;?>><?php echo $lang["button"]["ajouter"];?></button><br />
							<label for="config-collaboration" class="projet"><?php echo $lang["projet"]["labelconfigureCollaborateur"];?> :</label><button id="HE-config-collaborateur" <?php echo $disabled;?>><?php echo $lang["button"]["configurer"];?></button>
						</fieldset>
					</td>
					<td valign="top">
						<?php
						include_once("PHP/foldersize.php");
						?>
						<fieldset style="width:360px;display:block;height:196px;"><legend><?php echo $lang["projet"]["legendStatistiques"];?></legend>
							<label class="projet">- Textures :</label><span id="HE-nbr-textures" style="font-weight: bold;"><?php echo count_files("_Projects/".$projet_name."/textures/");?></span> - <span id="HE-poids-textures"><?php echo format_size(foldersize("_Projects/".$projet_name."/textures/"), 2)?></span><br />
							<label class="projet">- Images :</label><span id="HE-nbr-images" style="font-weight: bold;"><?php echo count_files("_Projects/".$projet_name."/images/");?></span> - <span id="HE-poids-images"><?php echo format_size(foldersize("_Projects/".$projet_name."/images/"), 2)?></span><br />
							<label class="projet">- Meshes :</label><span id="HE-nbr-meshes" style="font-weight: bold;"><?php echo count_files("_Projects/".$projet_name."/meshes/", "babylon");?></span> - <span id="HE-poids-meshes"><?php echo format_size(foldersize("_Projects/".$projet_name."/meshes/", "babylon"), 2)?></span><br />
							<label class="projet">- Scenes :</label><span id="HE-nbr-scenes" style="font-weight: bold;"><?php echo count_files("_Projects/".$projet_name."/scenes/");?></span> - <span id="HE-poids-scenes"><?php echo format_size(foldersize("_Projects/".$projet_name."/scenes/"), 2)?></span><br />
							<label class="projet">- Musics :</label><span id="HE-nbr-musics" style="font-weight: bold;"><?php echo count_files("_Projects/".$projet_name."/musics/");?></span> - <span id="HE-poids-musics"><?php echo format_size(foldersize("_Projects/".$projet_name."/musics/"), 2)?></span><br />
							<label class="projet">- Sounds :</label><span id="HE-nbr-sounds" style="font-weight: bold;"><?php echo count_files("_Projects/".$projet_name."/sounds/");?></span> - <span id="HE-poids-sounds"><?php echo format_size(foldersize("_Projects/".$projet_name."/sounds/"), 2)?></span><br />
							<label class="projet">- Video :</label><span id="HE-nbr-videos" style="font-weight: bold;"><?php echo count_files("_Projects/".$projet_name."/videos/");?></span> - <span id="HE-poids-videos"><?php echo format_size(foldersize("_Projects/".$projet_name."/videos/"), 2)?></span><br />
						</fieldset>
				</td>
			</tr>
			<tr>
				<td valign="top" colspan="2">
					<br />
					<fieldset style="width:832px;display:block;height:173px;margin-top:-2px"><legend><?php echo $lang["projet"]["legendPersonalNote"];?></legend>
						<textarea style="width:822px;height:148px;" id="HE-personnal-note"></textarea>						
					</fieldset>
				</td>
			</tr>
		</table>
		</td>		
	</tr>
</table>
<script src="JS/projet.js?<?php echo time();?>" type="text/javascript"></script>