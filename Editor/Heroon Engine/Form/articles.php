<?php
/*##################################################
 *                                articles.php
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 01/01/2017
 ###################################################

Créer des articles pour les acteurs
*/
?>
<script> var articles = null, engine = null, scene = null, canvas = null, globalParticle = {}; </script>
<div id="dialog-form-add-acteur" title="<?php echo $lang["article"]["titleAddActor"];?>">
	<select multiple="multiple" id="AjouterActorListe" size="16" style="width:250px;">
		<?php
		$chemin = "./_Projects/".$projet_name."/game data/actors/";
		$listeActor = "";
		$MyDirectory = opendir($chemin);
		while($Entry = @readdir($MyDirectory)) {
			if($Entry != "animations.json" && $Entry != "articles.json" && $Entry != "competances.json" && $Entry != "faction.json" && $Entry != "general.json" && $Entry != '.' && $Entry != '..') {
				$actorType = explode(".", $Entry);
				$listeActor .= '<option value="'.$actorType[0].'.'.$actorType[1].'">'.$actorType[0].'.'.$actorType[1].'</option>';
			}
		}
		closedir($MyDirectory);
		echo $listeActor;
		?>
	</select>
</div>
<table width="100%" height="100%" id="tableArticle">
	<tr>
		<td width="16%" valign="top">
			<fieldset><legend><?php echo $lang["article"]["listeArticle"];?></legend>
				<select id="templateArticles">
					<option value="0">None</option>
				</select>
				<br /><br />
				<button id="addArticle"><?php echo $lang["button"]["new"];?></button>
				<button id="editeArticle" style="margin-left:2px;"><?php echo $lang["button"]["rename"];?></button>
				<button id="delArticle" style="margin-left:2px;"><?php echo $lang["button"]["delete"];?></button>
			</fieldset>
			<br />
			<fieldset><legend><?php echo $lang["article"]["legendActorType"];?></legend>
				<?php echo $lang["article"]["infoActorUseArticle"];?><br />
				<select id="actorListeArticle" align="top" size="20" multiple="multiple" style="width:99%;" onChange="articles.createActor(this);"></select>
				<button id="addActor" style="width:49%"><?php echo $lang["button"]["add"];?></button>
				<button id="delActor" style="width:49%"><?php echo $lang["button"]["delete"];?></button>
			</fieldset>
		</td>
		<td id="containeurCanvas" style="width:67%;height:100%;" align="center">
			<canvas id="HE-canvas-scene-article"></canvas>
		</td>
		<td width="17%" valign="top">
			<fieldset><legend><?php echo $lang["article"]["legendParametre"];?></legend>
			<div style="margin-left:5px;">
				<label class="base"><?php echo $lang["generality"]["mesh"];?> :</label><button id="addArticle3D" onChange="articles.createArticle(this);" onClick="$('#dialog-add-meshes').data('opener', this).dialog('open');" style="width:140px;white-space:nowrap;direction:rtl;"><?php echo $lang["button"]["choisir"];?></button>
				<br />
				<label class="base"><?php echo $lang["article"]["articleType"];?> :</label><select id="typeArticle">
																<option value="0">None</option>
																<option value="Arme"><?php echo $lang["article"]["optionArme"];?></option>
																<option value="Armure"><?php echo $lang["article"]["optionArmure"];?></option>
																<option value="Outils"><?php echo $lang["article"]["optionOutils"];?></option>
																<option value="Autres"><?php echo $lang["article"]["optionAutres"];?></option>
															</select>
				<br />
				<label class="base"><?php echo $lang["article"]["attacheArticleOn"];?> :</label><select id="AttacheOs">
																<option value="0">None</option>
																<option value="head"><?php echo $lang["article"]["optionAttacheArticleOn_head"];?></option>
																<option value="chest"><?php echo $lang["article"]["optionAttacheArticleOn_chest"];?></option>
																<option value="r_shoulder"><?php echo $lang["article"]["optionAttacheArticleOn_r_shoulder"];?></option>
																<option value="l_shoulder"><?php echo $lang["article"]["optionAttacheArticleOn_l_shoulder"];?></option>
																<option value="pelvis"><?php echo $lang["article"]["optionAttacheArticleOn_pelvis"];?></option>
																<option value="r_forearm"><?php echo $lang["article"]["optionAttacheArticleOn_r_forearm"];?></option>
																<option value="l_forearm"><?php echo $lang["article"]["optionAttacheArticleOn_l_forearm"];?></option>
																<option value="r_hand"><?php echo $lang["article"]["optionAttacheArticleOn_r_hand"];?></option>
																<option value="l_hand"><?php echo $lang["article"]["optionAttacheArticleOn_l_hand"];?></option>
																<option value="r_shin"><?php echo $lang["article"]["optionAttacheArticleOn_r_shin"];?></option>
																<option value="l_shin"><?php echo $lang["article"]["optionAttacheArticleOn_l_shin"];?></option>
																<option value="r_footer"><?php echo $lang["article"]["optionAttacheArticleOn_r_footer"];?></option>
																<option value="l_footer"><?php echo $lang["article"]["optionAttacheArticleOn_l_footer"];?></option>
															</select>
				<br />
				<label class="base"><?php echo $lang["article"]["positionArticle"];?> :</label><input type="text" id="position" value="" placeholder="x,y,z" size="14" />
				<br />
				<label class="base"><?php echo $lang["article"]["rotationArticle"];?> :</label><input type="text" id="rotation" value="" placeholder="x,y,z" size="14" />
				<br />
				<label class="base"><?php echo $lang["article"]["echelleArticle"];?> :</label><input type="text" id="echelle" value="" placeholder="x,y,z" size="14" />
				<br />
				<label class="base"><?php echo $lang["article"]["particle"];?> :</label><select id="emiter_article">
															<option value="0">None</option>	
															<?php echo listeParticles($projet_name); ?>
														</select>
				<br />
				<div id="panelParticule" style="display:none">
					<label class="base"><?php echo $lang["article"]["positionParticle"];?> :</label><input type="text" id="position_particule" value="" placeholder="x,y,z" size="14" />
					<br />
				</div>
				<label class="base"><?php echo $lang["article"]["material"];?> :</label><select id="material">
															<option value="0">None</option>
															<option value="fire"><?php echo $lang["article"]["optionAmerial_fire"];?></option>
														</select>
				<br />
				<div id="panelMaterial" style="display:none">
					<label class="base"><?php echo $lang["article"]["positionMaterial"];?> :</label><input type="text" id="position_material" value="" placeholder="x,y,z" size="14" />
					<br />
					<label class="base"><?php echo $lang["article"]["echelleMaterial"];?> :</label><input type="text" id="echelle_material" value=""  placeholder="x,y,z" size="14" />
					<br />
				</div>
				<label class="base"><?php echo $lang["article"]["light"];?> :</label><select id="activerLumiere">
														<option value="0" selected><?php echo $lang["generality"]["no"];?></option>
														<option value="1"><?php echo $lang["generality"]["yes"];?></option>
													</select>
				<br />
				<div id="panelLumiere" style="display:none">
					<label class="base"><?php echo $lang["article"]["colorDiffuse"];?> :</label><input type="text" id="color-article-diffuse" class="colorwell" value="#FFFFFF" />
					<br />
					<div id="picker1" style="margin-left:0px"></div>
					<label class="base"><?php echo $lang["article"]["colorSpecular"];?> :</label><input type="text" id="color-article-spec" class="colorwell" value="#FFFFFF" />
					<br />
					<div id="picker2" style="margin-left:0px"></div>
					<label class="base"><?php echo $lang["article"]["intensity"];?> :</label><input type="text" class="spinner" id="intensityLumiere" value="0.6" size="11" />
					<br />
					<label class="base"><?php echo $lang["article"]["radius"];?> :</label><input type="text" class="spinner" id="rayonLumiere" value="20" size="11" />
					<br />
					<label class="base"><?php echo $lang["article"]["positionLight"];?> :</label><input type="text" id="position_light" value="" placeholder="x,y,z" size="14" />
				</div>
			</div>
			</fieldset>
		</td>
	</tr>
</table>
<script src="JS/actors/articles.js?<?php echo time();?>" type="text/javascript"></script>
<script>
$(function() {
	$(".height-content-actor").css({"height": $("body").height() - 100 +"px"});
	$("#containeurCanvas").css({"height": $("body").height() - 140 +"px"});
});
canvas = document.getElementById('HE-canvas-scene-article');
engine = new BABYLON.Engine(canvas, true);
articles = new articlesEditor();
articles.createScene();
</script>