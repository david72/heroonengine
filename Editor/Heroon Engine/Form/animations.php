<?php
/*##################################################
 *                                annimations.php
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 01/01/2017
 ###################################################

Créer des animations
 */
?>
<script>
var animation = null, engineAnim = null, sceneAnim = null, canvasAnim = null;
</script>
<table width="100%">
	<tr>
		<td align="left" width="220px" class="liste_animation">
			<div id="accordion-animation">
				<h3><?php echo $lang["animation"]["listeModeleAnimation"];?></h3>
				<div id="ListeAnimation"></div>
			</div>
		</td>
		<td class="liste_animation">
			&nbsp; &nbsp;<button id="newAnmation" style="margin-top:4px;"><?php echo $lang["button"]["create"];?></button>
						 <button id="copyAnmation" style="margin-top:4px;"><?php echo $lang["button"]["copy"];?></button>
						 <button id="deleteAnmation" style="margin-top:4px;"><?php echo $lang["button"]["delete"];?></button>
						 <span style="margin-top:5px;margin-left:110px;"><?php echo $lang["animation"]["animationName"];?> :</span><input type="text" id="animName" value="" placeholder="player man" /><?php echo $lang["animation"]["infoAnimationName"];?><br />
			<br />
			<table width="100%" height="95%">
				<tr>
					<td width="160px" height="250px">
						<?php echo $lang["animation"]["listeEtat"];?> :<select id="frameAnimation" multiple="multiple" size="20" style="width:160px;"></select>
					</td>
					<td width="160px" height="200px"><br />
						<div style="margin-left:4px;">
							<button style="width:145px;margin-top:5px;" id="newFrame" ><?php echo $lang["button"]["addEtat"];?></button><br />
							<button style="width:145px;margin-top:10px;margin-bottom:10px;" id="deleteFrame"><?php echo $lang["button"]["deleteEtat"];?></button><br />
							<?php echo $lang["animation"]["nameEtat"];?> :<br />
							<input type="text" id="frameName" size="16" value="" placeholder="walk" /><br />
							<br />
							<?php echo $lang["animation"]["keyStart"];?> :<br />
							<input id="frameStart" class="spinner" name="value" size="5" value="" placeholder="0" /><br />
							<br />
							<?php echo $lang["animation"]["keyEnd"];?> :<br />
							<input id="frameEnd" class="spinner" name="value" size="5" value="" placeholder="30" /><br />
							<br />
							<?php echo $lang["animation"]["speed"];?> :<br />
							<input id="frameSpeed" class="spinner" name="value" size="5" value="" placeholder="100" />%
						</div>
					</td>
					<td width="80%" id="apercueAnim" rowspan="3">
						<canvas id="HE-canvas-apercu-animation"></canvas>
						<div id="fps" style="top:110px;right:40px;position:absolute;color:white;"></div>
						<div id="stats" style="top:130px;right:40px;position:absolute;color:white;"></div>
					</td>
				</tr>
				<tr>
					<td align="left" rowspan="2"><br />
						<div style="margin-left:4px;">
							<?php echo $lang["animation"]["appercuActor"];?> :<br />
							<select id="listeActeurForAnimation" class="panel">
								<option value="0" selected>None</option>
							</select>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<script src="JS/actors/animation.js?<?php echo time();?>" type="text/javascript"></script>
<script>
$(function() {
	var heightFrame = screen.availHeight;
	$("#accordion-animation").accordion({heightStyle: "fill"});
	$("#accordion-animation").accordion("option", "icons", null);
	$("#ListeAnimation").css("height", $("body").height() - 200 +"px");
	$(".liste_animation").css({"height": $("body").height() - 140 +"px"});
});

canvasAnim = document.getElementById('HE-canvas-apercu-animation');
engineAnim = new BABYLON.Engine(canvasAnim, true);
animation = new animationEditor();
animation.createScene();
</script>