<div id="HE-accordion-script">
	<h3>Scripts</h3>
	<div id="listeScripts">
		<?php
		$link = "";
		$dirname = '../Scripts/'.file_get_contents('../Data Project/Selected.dat')."/";
		$dir = opendir($dirname);
		while($file = readdir($dir)) {
			if($file != '.' && $file != '..' && !is_dir($dirname.$file))
			{
				if(pathinfo($dirname.$file, PATHINFO_EXTENSION) == "js")
					$link .= '<img src="Ressources/js_mini.png" /> <a href="javascript:void(0)" class="scriptListe" id="'.basename($file, '.js').'_js" onDblClick="script.openScript(\''.$dirname.$file.'\')">'.$file.'</a><br />';
				if(pathinfo($dirname.$file, PATHINFO_EXTENSION) == "php")
					$link .= '<img src="Ressources/php_mini.png" /> <a href="javascript:void(0)" class="scriptListe" id="'.basename($file, '.php').'_php" onDblClick="script.openScript(\''.$dirname.$file.'\')">'.$file.'</a><br />';
			}
		}
		closedir($dir);
		echo $link;
		?>
	</div>
</div>
<script>
$(function() {
	$("#listeScripts").css({"height": $("body").height() - 163 +"px"});
	$("#HE-accordion-script").accordion({heightStyle: "content"});
	$("#HE-accordion-script").accordion("option", "icons", null);
});
</script>
