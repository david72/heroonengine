<?php
/*##################################################
 *                                particules2d.php
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 01/01/2017
 ###################################################

Créer des sytemes de particules pour les maillages
 */
?>
<script>
var globalParticle = {
	scene: null, engine: null, selectionEmiter: null, particleSystem: null, depthWrite: null, mode: "one", imageParticles: null, particleRotation: null, myEmiterParticule: null, particle: null,
	particleSizeMin: null, particleSizeMax: null, particleDurerVieMin: null, particleDentity: null, particleEmitPowerMin: null, particleEmitPowerMax: null, particleUpdateSpeed: null, vieParticleStop: null,
	vieParticleDuration: null, colorMask: [], maxEmitBox: [], minEmitBox: [], particleColor1: [], particleColor2: [], particleColorDead: [], particleGravity: [], particleDirection1: [], particleDirection2: []
};
</script>
<div id="ContoleHeightParticle" style="display:none;">
	<input type="image" onClick="globalParticle.particle.up()" style="position:absolute;top:80px;margin-left:45px;z-index:10000;cursor:pointer;" src="Ressources/up.png" />
	<input type="image" onClick="globalParticle.particle.left()" style="position:absolute;top:110px;margin-left:10px;z-index:10000;cursor:pointer;" src="Ressources/left.png" />
	<input type="image" onClick="globalParticle.particle.centreTarget()" style="position:absolute;top:110px;margin-left:45px;z-index:10000;cursor:pointer;" src="Ressources/moins.png" />
	<input type="image" onClick="globalParticle.particle.right()" style="position:absolute;top:110px;margin-left:80px;z-index:10000;cursor:pointer;" src="Ressources/right.png" />
	<input type="image" onClick="globalParticle.particle.down()" style="position:absolute;top:140px;margin-left:45px;z-index:10000;cursor:pointer;" src="Ressources/down.png" />
</div>
<table width="100%">
	<tr>
		<td style="width:600px;height:600px" align="center">
			<div style="width:600px;height:600px;border:1px solid black;margin-top:10px;">
				<canvas id="HE-canvas-particle"></canvas>
				<div id="fps_particle" style="top:75px;margin-left:540px;"></div>
				<div id="stats_particle" style="top:100px;margin-left:460px;"></div>
			</div>
		</td>
		<td>
			<table>
				<tr>
					<td colspan="2">
						<fieldset><legend><?php echo $lang["particle"]["administration"];?></legend>
							<label class="item"><?php echo $lang["particle"]["emiter"];?> :</label><select id="listeEmeteur"><option value="0">None</option>
																										<?php
																										$option = null;
																										$dirname = '_Projects/'.$projet_name.'/particles/';
																										$dir = opendir($dirname);
																										while($file = readdir($dir)) {
																											if($file != '.' && $file != '..' && !is_dir($dirname.$file))
																											{
																												$files3d = substr($file, 0, strpos($file, 'd_'));
																												if(!$files3d) {
																													$option .= '<option value="'.basename(''.$file.'','.js').'">'.basename(''.$file.'','.js').'</option>';
																												}
																											}
																										}
																										closedir($dir);
																										echo $option;
																										?> &nbsp;
																									</select>
							<button id="addParticle"><?php echo $lang["button"]["new"];?></button>
							<button id="copyParticle"><?php echo $lang["button"]["copy"];?></button>
							<button id="delParticle"><?php echo $lang["button"]["delete"];?></button>
							<button id="saveParticle"><?php echo $lang["button"]["enregistrer"];?></button>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td>
						<fieldset><legend><?php echo $lang["particle"]["legendGeneral"];?></legend>
							<label class="item"><?php echo $lang["particle"]["sizeParticleMin"];?> :</label><input type="text" class="spinner" id="tailleParticleMin" value="1" size="5" /><br />
							<label class="item"><?php echo $lang["particle"]["sizeParticleMax"];?> :</label><input type="text" class="spinner" id="tailleParticleMax" value="1" size="5" /><br />
							<label class="item"><?php echo $lang["particle"]["directionEmeteurMin"];?> :</label><input type="text" id="tailleEmeteurMin" value="0,1,0" placeholder="0,1,0" size="5" /> x,y,z<br />
							<label class="item"><?php echo $lang["particle"]["directionEmeteurMax"];?> :</label><input type="text" id="tailleEmeteurMax" value="0,2,0" placeholder="0,2,0" size="5" /> x,y,z
						</fieldset>
						<fieldset style="height:278px"><legend><?php echo $lang["particle"]["legendColorAndTexture"];?></legend>
							<label class="item"><?php echo $lang["particle"]["image"];?> :</label><input type="button" onClick="$('#dialog-add-images').data('opener', this).dialog('open');" style="width:215px;height:22px;white-space:nowrap;direction:rtl;" id="particleImage" value="None" /><br />
							<label class="item"><?php echo $lang["particle"]["mode"];?> :</label><select id="mode">
																									<option value="one">BLENDMODE_ONEONE</option>
																									<option value="standard">BLENDMODE_STANDARD</option>
																								</select>
							<br />
							<label class="item"><?php echo $lang["particle"]["profondeur"];?> :</label><select id="depthWrite">
																										<option value="true"><?php echo $lang["generality"]["yes"];?></option>
																										<option value="false"><?php echo $lang["generality"]["no"];?></option>
																									</select>
							<br />
							<label class="item"><?php echo $lang["particle"]["colorFond"];?> :</label><input id="masqueFond" class="colorwell" value="#000" size="7"/><br />
							<label class="item"><?php echo $lang["particle"]["color1"];?> :</label><input id="color-particle1" class="colorwell" value="#000" size="7"/><br />
							<label class="item"><?php echo $lang["particle"]["color2"];?> :</label><input id="color-particle2" class="colorwell" value="#000" size="7"/><br />
							<label class="item"><?php echo $lang["particle"]["colorMort"];?> :</label><input id="color-particle3" class="colorwell" value="#000" size="7"/><br />
						</fieldset>
					</td>
					<td>
						<fieldset style="height:417px"><legend><?php echo $lang["particle"]["legendMove"];?></legend>
							<label class="item"><?php echo $lang["particle"]["rotationParticle"];?> :</label><select id="rotationParticles">
																												<option value="true"><?php echo $lang["generality"]["yes"];?></option>
																												<option value="false"><?php echo $lang["generality"]["no"];?></option>
																											</select>
							<br />
							<label class="item"><?php echo $lang["particle"]["density"];?> :</label><input type="text" class="spinner" id="densityParticle" value="700" size="5" /> (max 6000)<br />
							<label class="item"><?php echo $lang["particle"]["durerMin"];?> :</label><input type="text" class="spinner" id="durrerVieParticleMin" value="0.1" size="5" /><br />
							<label class="item"><?php echo $lang["particle"]["durerMax"];?> :</label><input type="text" class="spinner" id="durrerVieParticleMax" value="2" size="5" /><br />
							<label class="item"><?php echo $lang["particle"]["particleVivante"];?> :</label><select id="vieParticleStop">
																												<option value="false"><?php echo $lang["generality"]["no"];?></option>
																												<option value="true"><?php echo $lang["generality"]["yes"];?></option>
																											</select>
							<br />
							<div id="durerVide" style="display:none">
								<label class="item"><?php echo $lang["particle"]["durerVie"];?> :</label><input type="text" class="spinner" id="vieParticleDurer" value="5" size="5" /><br />
								<label class="item"><?php echo $lang["particle"]["manualEmitCount"];?> :</label><input type="text" class="spinner" id="manualEmitCount" value="0" size="5" /><br />
							</div>
							<label class="item"><?php echo $lang["animation"]["speed"];?> :</label><input type="text" class="spinner" id="vitesseUpdate" value="0.01" size="5" /><br />
							<label class="item"><?php echo $lang["particle"]["powerMin"];?> :</label><input type="text" class="spinner" id="vitessePowerMin" value="1" size="5" /><br />
							<label class="item"><?php echo $lang["particle"]["powerMax"];?> :</label><input type="text" class="spinner" id="vitessePowerMax" value="4" size="5" /><br />
							<label class="item"><?php echo $lang["particle"]["gravity"];?> :</label><input type="text" id="graviter" value="0,-22,0" size="5" placeholder="0,-22,0" /> x,y,z<br />
							<label class="item"><?php echo $lang["particle"]["direction1"];?> :</label><input type="text" id="direction1" value="-10,15,10" size="5" placeholder="-10,15,10" /> x,y,z<br />
							<label class="item"><?php echo $lang["particle"]["direction2"];?> :</label><input type="text" id="direction2" value="10,15,-10" size="5" placeholder="10,15,-10" /> x,y,z<br />
							<label class="item"><?php echo $lang["particle"]["effect"];?> :</label><select id="effect">
																									<option value="true"><?php echo $lang["generality"]["yes"];?></option>
																									<option value="false"><?php echo $lang["generality"]["no"];?></option>
																								</select>
						</fieldset>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<script src="JS/particles.js?<?php echo time();?>" type="text/javascript"></script>
<script>
var divFps = document.getElementById("fps_particle");
var stats = document.getElementById("stats_particle");
var canvas = document.getElementById('HE-canvas-particle');

globalParticle.engine = new BABYLON.Engine(canvas, true);
globalParticle.particle = new particle();
</script>