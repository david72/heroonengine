<?php
/*##################################################
 *                             particules3d.php
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 23/10/2017
 ###################################################

Créer des sytemes de particules solid pour les maillages
 */
?>
<script>
var globalParticleSolide = {
	scene: null, engine: null, selectionEmiter: null, selectionShape: "plane", particleSystem: null, imageParticles: null, particleRotation: false, emiterRotation: false, myEmiterParticule: null, particle: null,
	particleSize: null, particleDurerVie: null, particleDentity: null, particleUpdateSpeed: null, vieParticleStop: null, isPickable : false, particleIntersection: false,
	vieParticleDuration: null, particleGravity: -0.01, velocity: 0, computeParticleRotation: true, computeParticleColor: true, computeParticleTexture: true, computeParticleVertex: false, particleColor: []
};
</script>
<table width="100%">
	<tr>
		<td style="width:600px;height:600px" align="center">
			<div style="width:600px;height:600px;border:1px solid black;margin-top:10px;">
				<canvas id="HE-canvas-particleSolide"></canvas>
				<div id="fps_particleSolide" style="top:75px;margin-left:530px;"></div>
			</div>
		</td>
		<td>
			<table>
				<tr>
					<td colspan="2">
						<fieldset><legend><?php echo $lang["particle"]["administration"];?></legend>
							<label class="item"><?php echo $lang["particle"]["emiter"];?> :</label><select id="listeEmeteurSolid"><option value="0">None</option>
																										<?php
																										$option = null;
																										$dirname = '_Projects/'.$projet_name.'/particles/';
																										$dir = opendir($dirname);
																										while($file = readdir($dir)) {
																											if($file != '.' && $file != '..' && !is_dir($dirname.$file))
																											{
																												$files3d = substr($file, 0, strpos($file, 'd_'));
																												if($files3d) {
																													$file = str_replace("3d_", "", $file);
																													$option .= '<option value="'.basename(''.$file.'','.js').'">'.basename(''.$file.'','.js').'</option>';
																												}
																											}
																										}
																										closedir($dir);
																										echo $option;
																										?> &nbsp;
																									</select>
							<button id="addParticleSolid"><?php echo $lang["button"]["new"];?></button>
							<button id="copyParticleSolid"><?php echo $lang["button"]["copy"];?></button>
							<button id="delParticleSolid"><?php echo $lang["button"]["delete"];?></button>
							<button id="saveParticleSolid"><?php echo $lang["button"]["enregistrer"];?></button>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td>
						<fieldset><legend><?php echo $lang["particle"]["legendGeneral"];?></legend>

							<label class="item"><?php echo $lang["particle"]["shape"];?> :</label><select id="shapeEmeteurSolid">
																										<option value="plane">Plan</option>
																										<option value="disc">Disque</option>
																										<option value="triangle">Triangle</option>
																										<option value="box">Boite</option>
																										<option value="sphere">Sphere</option>
																										<option value="cylindre">Cylindre</option>
																										<option value="torus">Torus</option>
																										<!--<option value="custom">Personnalisé</option>-->
																									</select><br />
							<label class="item"><?php echo $lang["particle"]["sizeParticle"];?> :</label><input type="text" class="spinner" id="tailleParticleSolid" value="1" size="5" /><br />
							<label class="item"><?php echo $lang["particle"]["selectableParticle"];?> :</label><input type="checkbox" id="selectableParticleSolid" value="1" /><br />
							<label class="item"><?php echo $lang["particle"]["collisionnableParticle"];?> :</label><input type="checkbox" id="collisionnableParticleSolid" value="1" /><br />
							<label class="item"><?php echo $lang["particle"]["computeParticleRotation"];?> :</label><input type="checkbox" id="computeParticleRotationSolid" value="1" checked /><br />
							<label class="item"><?php echo $lang["particle"]["computeParticleColor"];?> :</label><input type="checkbox" id="computeParticleColorSolid" value="1" checked /><br />
							<label class="item"><?php echo $lang["particle"]["computeParticleTexture"];?> :</label><input type="checkbox" id="computeParticleTextureSolid" value="1" checked /><br />
							<label class="item"><?php echo $lang["particle"]["computeParticleVertex"];?> :</label><input type="checkbox" id="computeParticleVertexSolid" value="1" /><br />
						</fieldset>
						<fieldset style="height:120px"><legend><?php echo $lang["particle"]["legendColorAndTexture"];?></legend>
							<label class="item"><?php echo $lang["particle"]["image"];?> :</label><input type="button" onClick="$('#dialog-add-images').data('opener', this).dialog('open');" style="width:215px;height:22px;white-space:nowrap;direction:rtl;" id="particleImageSolid" value="None" /><br />
							<label class="item"><?php echo $lang["particle"]["color"];?> :</label><input id="color-particleSolid" class="colorwell" value="#000000" size="7"/><br />
						</fieldset>
					</td>
					<td>
						<fieldset style="height:360px"><legend><?php echo $lang["particle"]["legendMove"];?></legend>
							<label class="item"><?php echo $lang["particle"]["rotationParticle"];?> :</label><select id="rotationParticlesSolid">
																												<option value="true"><?php echo $lang["generality"]["yes"];?></option>
																												<option value="false"><?php echo $lang["generality"]["no"];?></option>
																											</select>
							<br />
							<label class="item"><?php echo $lang["particle"]["emiterRotationParticle"];?> :</label><select id="rotationEmiterParticlesSolid">
																												<option value="true"><?php echo $lang["generality"]["yes"];?></option>
																												<option value="false"><?php echo $lang["generality"]["no"];?></option>
																											</select>
							<br />
							<label class="item"><?php echo $lang["particle"]["density"];?> :</label><input type="text" class="spinner" id="densityParticleSolid" value="1000" size="5" /><br />
							<label class="item"><?php echo $lang["particle"]["particleVivante"];?> :</label><select id="vieParticleStopSolid">
																												<option value="false"><?php echo $lang["generality"]["no"];?></option>
																												<option value="true"><?php echo $lang["generality"]["yes"];?></option>
																											</select>
							<br />
							<div id="durerVie" style="display:none">
								<label class="item"><?php echo $lang["particle"]["durerVie"];?> :</label><input type="text" class="spinner" id="vieParticleDurerSolid" value="5" size="5" /><br />
							</div>
							<label class="item"><?php echo $lang["particle"]["force"];?> :</label><input type="text" class="spinner" id="vitesseUpdateSolid" value="0.7" size="5" /><br />
							<label class="item"><?php echo $lang["particle"]["gravity"];?> :</label><input type="text" id="graviterSolid" value="0.005" size="5" placeholder="0.005" /><br />
							<label class="item"><?php echo $lang["particle"]["direction"];?> :</label><input type="text" id="velocitySolid" value="0,0.6,0" size="5" placeholder="0,0.6,0" /> x,y,z<br />
							<label class="item"><?php echo $lang["particle"]["isBlocker"];?> :</label><select id="isBlockerParticleSolid">
																												<option value="false"><?php echo $lang["generality"]["no"];?></option>
																												<option value="true"><?php echo $lang["generality"]["yes"];?></option>
																											</select><br />
							<label class="item"><?php echo $lang["particle"]["renderingGroupId"];?> :</label><input type="text" class="spinner" id="renderingGroupIdSolid" value="1" size="5" /><br />
							<label class="item"><?php echo $lang["particle"]["layerMask"];?> :</label><input type="text" class="spinner" id="layerMaskSolid" value="1" size="5"  /><br />
							<label class="item"><?php echo $lang["particle"]["collisionMask"];?> :</label><input type="text" class="spinner" id="collisionMaskSolid" value="1" size="5" /><br />
							<label class="item"><?php echo $lang["particle"]["collisionGroup"];?> :</label><input type="text" class="spinner" id="collisionGroupSolid" value="1" size="5" /><br />
						</fieldset>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<script src="JS/particlesSolide.js?<?php echo time();?>" type="text/javascript"></script>
<script>
var divFpsParticleSolide = document.getElementById("fps_particleSolide");
var statsParticleSolide = document.getElementById("stats_particleSolide");
var canvasParticleSolide = document.getElementById('HE-canvas-particleSolide');

globalParticleSolide.engine = new BABYLON.Engine(canvasParticleSolide, true);
globalParticleSolide.particle = new particleSolide();
</script>