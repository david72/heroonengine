<?php session_start();
/*##################################################
 *                zones.php
 *              -------------------
 *  copyright      : (C) 2017 Pellier David (dad72)
 *  email          : dad72@heroonengine.com
 *  Revision       : 01/01/2017
 ###################################################

Créer des zones/scene pour le jeu
 */
?>
<script>
var global = {
		editor: null, scene: null, canvas: null, canvas_for_screenshot: null, canvas_for_material: null, engine: null, engine_for_screenshot: null, engine_for_material: null, terrainIsSelected: false, objetSelected: null, spawnpointSelected: null, countMatObject: 0, fire: null, lava: null, chat: null,
		lightSelected: null, lightSpotSelected: null, lightPointSelected: null, selectMaterial: null, terrain: null, water: null, skybox: null, innerLayout: null, assets: null, environement: null, musicZone: null, grassControl: null, saved: true,
		objets: [], zoneInterest: [], sound: [], soundMesh: null, portail: [], collisionBox: [], trigger: [], waypoint: [], spawnpoint: [], parentLightPoint: [], parentLightSpot: [], LightSpot: [], LightPoint: [], matObject: [], objectsFreeze: [], ZoneForest: [], fur: [], cellShading: [], objetMultiSelected: [], meshInShadow: []
	};
</script>
<!-- Dialog import mesh -->
<div id="dialog-import-mesh" title="<?php echo $lang["zones"]["titleImportMesh"];?>">
	<?php echo $lang["zones"]["rangeIn"];?> : <br />
	<div id="importMeshIn" style="width:200px;height:200px;border:1px solid black;overflow-y:auto;display:inline-block;"></div>
	<canvas id="HE-appercu-meshes-for-screenshot" style="display:inline-block;"></canvas>
	<div id="block-position-generation-image" style="display:none;">
		<input type="image" class="buttonControleTop" onClick="global.editor.upTarget()" style="position:absolute;top:30px;margin-left:455px;z-index:10000;cursor:pointer;" src="Ressources/up.png" />
		<input type="image" class="buttonControleTop" onClick="global.editor.leftTarget()" style="position:absolute;top:60px;margin-left:420px;z-index:10000;cursor:pointer;" src="Ressources/left.png" />
		<input type="image" class="buttonControleTop" onClick="global.editor.centreTarget()" style="position:absolute;top:60px;margin-left:455px;z-index:10000;cursor:pointer;" src="Ressources/moins.png" />
		<input type="image" class="buttonControleTop" onClick="global.editor.rightTarget()" style="position:absolute;top:60px;margin-left:490px;z-index:10000;cursor:pointer;" src="Ressources/right.png" />
		<input type="image" class="buttonControleTop" onClick="global.editor.downTarget()" style="position:absolute;top:90px;margin-left:455px;z-index:10000;cursor:pointer;" src="Ressources/down.png" />
		<div style="position:absolute;top:120px;margin-left:420px;">
			Scaling : <input type="text" size="2" id="changeScaleImportMesh" value="1.0" onInput="global.editor.addObject.changeScaleImportMesh();" onChange="global.editor.addObject.saveScaleImportMesh();" /><br />
			<br />
			box unit 1m : <input type="checkbox" id="displayBoxUnitOfMeseur" value="1" checked="true" />
		</div>
	</div>
	<div id="block-generation-image" style="display:none;">
	<?php echo $lang["zones"]["infoPositionImportMesh"];?><button style="margin-top:7px;float:right;background:green;color:white;" onClick="global.editor.addObject.createMiniatureScreenshot();"><?php echo $lang["button"]["createImage"];?></button>
	</div>
	<p><span style="color:red;"><?php echo $lang["zones"]["extention"];?> :</span><b>Mesh:</b> .babylon, .glb, .gltf, .obj, .stl - <b>Textures:</b> .png, .jpg, .jpeg, .tga, .dds</p>
	<p><?php echo $lang["zones"]["importTextureAttention"];?></p>
	<form id="my_form_meshes" method="post" action="PHP/upload.php" enctype="multipart/form-data">
		<input type="text" name="range_importMeshFile" id="range_importMeshFile" value="" style="display:none;" />
		<input type="text" name="importMesh" value="1" style="display:none;" />
		<input type="text" name="nameProject" id="nameProject" value="" style="display:none;" />
		<input type="file" name="importMeshFile[]" id="importMeshFile" accept=".png, .jpg, .jpeg, .tga, .dds, .babylon, .glb, .gltf, .obj, .stl" multiple="true" /><br />
		<button type="submit" style="margin-top:7px;display:inline-block;"><?php echo $lang["button"]["envoyer"];?></button> <div id="progress_import" style="display:none;"><img src="Ressources/throbber.gif" /> Patienter un instant...</div>
	</form>
</div>
<!-- Dialog import scene -->
<div id="dialog-import-scene" title="<?php echo $lang["zones"]["titleImportZone"];?>">	
	<p><span style="color:red;"><?php echo $lang["zones"]["extention"];?> :</span><b>Scene:</b> .babylon, .glb, .gltf, .obj, .stl - <b>Textures:</b> .png, .jpg, .jpeg, .tga, .dds</p>
	<form id="my_formscene" method="post" action="PHP/upload.php" enctype="multipart/form-data">
		<input type="text" name="range_importSceneFile" id="range_importSceneFile" value="" style="display:none;" />
		<input type="text" name="importScene" value="1" style="display:none;" />
		<input type="text" name="nameProject" id="nameProject" value="" style="display:none;" />
		<?php echo $lang["zones"]["titleImportMesh"];?> : <input type="text" name="nameTerrain" id="nameTerrain" value="" /><br /><br />
		<?php echo $lang["zones"]["fileZone"];?> : <input type="file" name="importSceneFile[]" id="importSceneFile" accept=".png, .jpg, .jpeg, .tga, .dds, .babylon, .glb, .gltf, .obj, .stl" multiple="true" /><br />
		<button type="submit" style="margin-top:7px;display:inline-block;"><?php echo $lang["button"]["envoyer"];?></button> <div id="progress_import_Scene" style="display:none;"><img src="Ressources/throbber.gif" /> Patienter un instant...</div>
	</form>
</div>
<!-- Dialog parametre grass -->
<div id="dialog-grass-editor" title="<?php echo $lang["zones"]["titleParametreGrass"];?>">
	<label class="base"><?php echo $lang["zones"]["textureGrass"];?> :</label><input type="button" value="None" style="width:120px;" id="textureGrass" /><br />
	<label class="base"><?php echo $lang["zones"]["grassDensity"];?> :</label><input type="checkbox" id="grassDense" value="1" /> <?php echo $lang["generality"]["yes"];?><br />
	<label class="base"><?php echo $lang["zones"]["hauteurGrassMax"];?> :</label><input type="text" id="heightgrassMax" placeholder="1.0" size="10" /><br />
</div>
<!-- Dialog import image -->
<div id="dialog-import-image" title="<?php echo $lang["zones"]["titleImportImage"];?>">
	<label class="dialog"><?php echo $lang["zones"]["rangeIn"];?> :</label><div id="importImageIn" style="width:255px;height:220px;border:1px solid black;overflow-y:auto;"></div>
	<p><span style="color:red;"><?php echo $lang["zones"]["extention"];?> :</span> .png, .jpg, .jpeg, .gif</p>
	<form id="my_form_image" method="post" action="PHP/upload.php" enctype="multipart/form-data">
		<input type="text" name="range_importImageFile" id="range_importImageFile" value="" style="display:none;" />
		<input type="text" name="importImage" value="1" style="display:none;" />
		<input type="text" name="nameProject" id="nameProject" value="" style="display:none;" />
		<input type="file" name="importImageFile[]" id="importImageFile" accept=".png, .jpg, .jpeg, .gif" multiple="true" /><br />
		<button type="submit" style="margin-top:7px;"><?php echo $lang["button"]["envoyer"];?></button>
	</form>
</div>
<!-- Dialog import music -->
<div id="dialog-import-music" title="<?php echo $lang["zones"]["titleImportMusic"];?>">
	<label class="dialog"><?php echo $lang["zones"]["rangeIn"];?> :</label><div id="importMusicIn" style="width:255px;height:220px;border:1px solid black;overflow-y:auto;"></div>
	<p><span style="color:red;"><?php echo $lang["zones"]["extention"];?> :</span> .mp3, .ogg</p>
	<form id="my_form_music" method="post" action="PHP/upload.php" enctype="multipart/form-data">
		<input type="text" name="range_importMusicFile" id="range_importMusicFile" value="" style="display:none;" />
		<input type="text" name="importMusic" value="1" style="display:none;" />
		<input type="text" name="nameProject" id="nameProject" value="" style="display:none;" />
		<input type="file" name="importMusicFile[]" id="importMusicFile" accept=".mp3, .ogg" multiple="true" /><br />
		<button type="submit" style="margin-top:7px;"><?php echo $lang["button"]["envoyer"];?></button>
	</form>
</div>
<!-- Dialog import son -->
<div id="dialog-import-sound" title="<?php echo $lang["zones"]["titleImportSound"];?>">
	<label class="dialog"><?php echo $lang["zones"]["rangeIn"];?> :</label><div id="importSoundIn" style="width:255px;height:220px;border:1px solid black;overflow-y:auto;"></div>
	<p><span style="color:red;"><?php echo $lang["zones"]["extention"];?> :</span> .mp3, .ogg</p>
	<form id="my_form_sound" method="post" action="PHP/upload.php" enctype="multipart/form-data">
		<input type="text" name="range_importSoundFile" id="range_importSoundFile" value="" style="display:none;" />
		<input type="text" name="importSound" value="1" style="display:none;" />
		<input type="text" name="nameProject" id="nameProject" value="" style="display:none;" />
		<input type="file" name="importSoundFile[]" id="importSoundFile" accept=".mp3, .ogg" multiple="true" /><br />
		<button type="submit" style="margin-top:7px;"><?php echo $lang["button"]["envoyer"];?></button>
	</form>
</div>
<!-- Dialog import video -->
<div id="dialog-import-video" title="<?php echo $lang["zones"]["titleImportVideo"];?>">
	<label class="dialog"><?php echo $lang["zones"]["rangeIn"];?> :</label><div id="importVideoIn" style="width:255px;height:220px;border:1px solid black;overflow-y:auto;"></div>
	<p><span style="color:red;"><?php echo $lang["zones"]["extention"];?> :</span> .mp4, .webm, .ogv</p>
	<form id="my_form_video" method="post" action="PHP/upload.php" enctype="multipart/form-data">
		<input type="text" name="range_importVideoFile" id="range_importVideoFile" value="" style="display:none;" />
		<input type="text" name="importVideo" value="1" style="display:none;" />
		<input type="text" name="nameProject" id="nameProject" value="" style="display:none;" />
		<input type="file" name="importVideoFile[]" id="importVideoFile" accept=".mp4, .webm, .ogv" multiple="true" /><br />
		<button type="submit" style="margin-top:7px;"><?php echo $lang["button"]["envoyer"];?></button>
	</form>
</div>
<!-- Dialog générer une foret -->
<div id="dialog-generate-forest" title="<?php echo $lang["zones"]["titleGeneratForest"];?>">
	<label class="dialog"><?php echo $lang["zones"]["meshTree"];?> :</label><input type="button" value="None" style="width:140px;" id="meshTree" onClick="$('#dialog-add-meshes').data('opener', this).dialog('open');" /><br />
	<label class="dialog"><?php echo $lang["zones"]["rotationRand"];?> :</label><input type="checkbox" id="rotateTreeRand" value="1" /> <?php echo $lang["generality"]["yes"];?><br />
	<label class="dialog"><?php echo $lang["zones"]["heightRand"];?> :</label><input type="checkbox" id="heightTreeRand" value="1" /> <?php echo $lang["generality"]["yes"];?><br />
	<label class="dialog"><?php echo $lang["zones"]["forestDentity"];?> :</label><input type="checkbox" id="densityTree" value="1" /> <?php echo $lang["generality"]["yes"];?><br />
	<label class="dialog"><?php echo $lang["zones"]["heightMin"];?> :</label><input type="text" id="heightTreeMin" placeholder="0.3" size="13" /><br />
	<label class="dialog"><?php echo $lang["zones"]["heightMax"];?> :</label><input type="text" id="heightTreeMax" placeholder="0.45" size="13" /><br />
	<label class="dialog"><?php echo $lang["zones"]["defineZoneForest"];?> :</label><input type="button" value="<?php echo $lang["button"]["definir"];?>" style="width:140px;" onCLick="global.editor.addObject.createZoneForest();" /><br />
	<label class="dialog"><?php echo $lang["zones"]["generateForest"];?> :</label><input type="button" value="<?php echo $lang["button"]["generer"];?>" style="width:140px;" onCLick="global.editor.addObject.generateForestInZone();" /><br />
</div>
<!-- Dialog editeur d'options du jeu et de l'editeur -->
<div id="dialog-options" title="<?php echo $lang["zones"]["titleOption"];?>">
	<fieldset><legend><?php echo $lang["zones"]["legendEditeurAndGame"];?></legend>
		<label class="dialog-big"><?php echo $lang["zones"]["labelDistanceCamera"];?> :</label><input type="text" id="viewCamera" value="" size="3" /><br />
	</fieldset>
	<fieldset><legend><?php echo $lang["zones"]["legendEditeur"];?></legend>
		<label class="dialog-big"><?php echo $lang["zones"]["labelSpeedCamera"];?> :</label><input type="text" id="speedCamera" size="3" value="" /><br />
		<label class="dialog-big"><?php echo $lang["zones"]["labelActifInfoBull"];?> :</label><input type="checkbox" id="hideInfoBull" value="1" /><br />
	</fieldset>
</div>
<!-- Dialog liste de materiel-->
<div id="dialog-materiel" title="<?php echo $lang["zones"]["titleMaterialManager"];?>">
	<canvas id="HE-canvas-apercu-material" style="display:inline-block;vertical-align:top;"></canvas>
	<div id="HE-liste-Material" style="display:inline-block;width:200px;height:350px;overflow:auto;border:1px solid black;"></div>
	<button onCLick="global.editor.addObject.createMaterial();" style="position:absolute;top:220px;left:50px;"><?php echo $lang["button"]["newMaterial"];?></button>
</div>
<!-- Dialog modifier l'environement du jeu -->
<div id="dialog-yearSetup" title="<?php echo $lang["zones"]["titleEcoulementTime"];?>">
	<label class="dialog"><?php echo $lang["zones"]["dayInAMonth"];?> :</label><input type="text" id="DayInAMonth" value="" size="9" onChange="global.environement.changeDayInAMonth();" />J<br />
	<label class="dialog"><?php echo $lang["zones"]["dayInAYear"];?> :</label><input type="text" id="DayInAYear" value="" size="9" onChange="global.environement.changeDayInAYear();" />J<br />
	<label class="dialog"><?php echo $lang["zones"]["dayCurrent"];?> :</label><input type="text" id="DayCurrent" value="" size="9" onChange="global.environement.changeDayCurrent();" /><br />
	<label class="dialog"><?php echo $lang["zones"]["yearCurrent"];?> :</label><select id="YearCurrent" onChange="global.environement.changeYearCurrent();">
												<optgroup label="<?php echo $lang["zones"]["optgroupLabel_Hiver"];?>">
													<option value="Janvier"><?php echo $lang["zones"]["optionJanvier"];?></option>
													<option value="Fevrier"><?php echo $lang["zones"]["optionFevrier"];?></option>
													<option value="Mars"><?php echo $lang["zones"]["optionMars"];?></option>
												</optgroup>
												<optgroup label="<?php echo $lang["zones"]["optgroupLabel_Primtemps"];?>">
													<option value="Avril"><?php echo $lang["zones"]["optionAvril"];?></option>
													<option value="Mai"><?php echo $lang["zones"]["optionMai"];?></option>
													<option value="Juin"><?php echo $lang["zones"]["optionJuin"];?></option>
												</optgroup>
												<optgroup label="<?php echo $lang["zones"]["optgroupLabel_Ete"];?>">
													<option value="Juillet" selected><?php echo $lang["zones"]["optionJuillet"];?></option>
													<option value="Aout"><?php echo $lang["zones"]["optionAout"];?></option>
													<option value="Septembre"><?php echo $lang["zones"]["optionSeptembre"];?></option>
												</optgroup>
												<optgroup label="<?php echo $lang["zones"]["optgroupLabel_Automne"];?>">
													<option value="Octobre"><?php echo $lang["zones"]["optionOctobre"];?></option>
													<option value="Novembre"><?php echo $lang["zones"]["optionNovembre"];?></option>
													<option value="Decembre"><?php echo $lang["zones"]["optionDecembre"];?></option>
												</optgroup>
											</select><br />
	<label class="dialog"><?php echo $lang["zones"]["secondPerMinute"];?> :</label><input type="text" id="SecondPerMinute" value="" size="9" onChange="global.environement.changeSecondPerMinute();" />S<br />
	<label class="dialog"><?php echo $lang["zones"]["sunrise"];?> :</label><input type="text" id="Sunrise" value="" size="9" onChange="global.environement.changeSunrise();" />H<br />
	<label class="dialog"><?php echo $lang["zones"]["sunset"];?> :</label><input type="text" id="Sunset" value="" size="9" onChange="global.environement.changeSunset();" />H<br />
</div>
<!-- Dialog modifier l'environement du jeu -->
<div id="dialog-environement" title="<?php echo $lang["zones"]["titleEditEnvironement"];?>" style="display:none;">
	<div style="display:inline-block">
		<fieldset><legend><?php echo $lang["zones"]["legendZone"];?></legend>
			<label class="dialog"><?php echo $lang["zones"]["changeZoneImage"];?> :</label><input type="button" value="" style="width:140px;" id="LoadingZone" onClick="$('#dialog-add-images').data('opener', this).dialog('open');" onChange="global.environement.changeLoadingZone();" /><br />
			<label class="dialog"><?php echo $lang["zones"]["musicZone"];?> :</label><input type="button" value="" style="width:140px;" id="MusicZone" onClick="$('#dialog-add-sounds').data('opener', this).dialog('open');" onChange="global.environement.changeMusic();" /><br />
			<label class="dialog"><?php echo $lang["zones"]["GravityScene"];?> :</label><input type="text" id="GravityZone" value="-0.75" size="13" onChange="global.environement.changeGravity();" /><br />
			<label class="dialog"><?php echo $lang["zones"]["ecoulementTime"];?> :</label><input type="button" id="yearSetup" value="Modifier" onCLick="$('#dialog-yearSetup').dialog('open');" style="width:140px;" /><br />
			<label class="dialog"><?php echo $lang["zones"]["PVPZone"];?> :</label><input type="checkbox" id="PVPZone" value="1" onChange="global.environement.changePVP();" /><br />
			<label class="dialog"><?php echo $lang["generality"]["legendScriptPero"];?> :</label><select id="ScriptZone" onChange="global.environement.changeScript();">
																									<option value="None">None</option>
																									<?php echo listeScripts($projet_name);?>
																								</select>
		</fieldset>
		<fieldset><legend><?php echo $lang["zones"]["titleOption"];?></legend>
			<label class="dialog-big"><?php echo $lang["zones"]["activOctree"];?> :</label><input type="checkbox" id="OptionOctreeZone" value="1" onChange="global.environement.changeOptionOctree();" /><br />
			<label class="dialog-big"><?php echo $lang["zones"]["activOptimizer"];?> :</label><input type="checkbox" id="OptionOptimizerZone" value="1" onChange="global.environement.changeOptionOptimizer();" /><br />
			<label class="dialog-big"><?php echo $lang["zones"]["activeSIMD"];?> :</label><input type="checkbox" id="OptionSIMD" value="1" onChange="global.environement.changeOptionSIMD();" /><br />
		</fieldset>
		<fieldset><legend><?php echo $lang["zones"]["legendCiel"];?></legend>
			<label class="dialog"><?php echo $lang["zones"]["cielDay"];?> :</label><input type="button" value="None" style="width:140px;" id="TextureSkyDay" onClick="$('#dialog-add-images').data('opener', this).dialog('open');" onChange="global.environement.changeSkyDay();" /><br />
			<label class="dialog"><?php echo $lang["zones"]["cielNight"];?> :</label><input type="button" value="None" style="width:140px;" id="TextureSkyNight" onClick="$('#dialog-add-images').data('opener', this).dialog('open');" onChange="global.environement.changeSkyNight();" />
		</fieldset>
	</div>
	<div style="display:inline-block;vertical-align:top;">
		<fieldset style="height:188px"><legend><?php echo $lang["zones"]["legendFog"];?></legend>
			<label class="dialog"><?php echo $lang["zones"]["activeFog"];?> :</label><input type="checkbox" id="OptionFogEnabled" value="1" onChange="global.environement.changeFog();" /><br />
			<label class="dialog">Mode :</label><select id="ModeFog" onChange="global.environement.changeFog();">
													<option value="FOGMODE_NONE">NONE</option>
													<option value="FOGMODE_EXP">EXP</option>
													<option value="FOGMODE_EXP2">EXP2</option>
													<option value="FOGMODE_LINEAR">LINEAR</option>
												</select><br />
			<label class="dialog"><?php echo $lang["zones"]["debugFog"];?> :</label><input type="text" id="StartFog" placeholder="30.0" size="13" onChange="global.environement.changeFog();" /><br />
			<label class="dialog"><?php echo $lang["zones"]["finFog"];?> :</label><input type="text" id="EndFog" placeholder="80.0" size="13" onChange="global.environement.changeFog();" /><br />
			<label class="dialog"><?php echo $lang["zones"]["colorFog"];?> :</label><input id="ColorFog" value="#d8d8d8" size="13" onChange="global.environement.changeFog();" /><br />
			<label class="dialog"><?php echo $lang["zones"]["densityFog"];?> :</label><input type="text" id="DensityFog" placeholder="0.02" size="13" onChange="global.environement.changeFog();" />
		</fieldset>
		<fieldset><legend><?php echo $lang["zones"]["legendMeteo"];?></legend>
			<label class="dialog"><?php echo $lang["zones"]["rainParticle"];?> :</label><select style="width:140px;" id="rainParticle" onChange="global.environement.changeMeteoParticle('rain');">
																	<option value="0">None</option>
																	<?php echo listeParticles($projet_name); ?>
																</select><br />
			<label class="dialog"><?php echo $lang["zones"]["rainSound"];?> :</label><input type="button" value="None" style="width:140px;" id="rainSound" onClick="$('#dialog-add-sounds').data('opener', this).dialog('open');" onChange="global.environement.changeMeteoSound('rain');" /><br />
			<hr />
			<label class="dialog"><?php echo $lang["zones"]["snowParticle"];?> :</label><select style="width:140px;" id="snowParticle" onChange="global.environement.changeMeteoParticle('snow');">
																	<option value="0">None</option>
																	<?php echo listeParticles($projet_name); ?>
																</select><br />
			<label class="dialog"><?php echo $lang["zones"]["snowSound"];?> :</label><input type="button" value="None" style="width:140px;" id="snowSound" onClick="$('#dialog-add-sounds').data('opener', this).dialog('open');" onChange="global.environement.changeMeteoSound('snow');" /><br />
			<hr />
			<label class="dialog"><?php echo $lang["zones"]["stormParticle"];?> :</label><select style="width:140px;" id="stormParticle" onChange="global.environement.changeMeteoParticle('storm');">
																	<option value="0">None</option>
																	<?php echo listeParticles($projet_name); ?>
																</select><br />
			<label class="dialog"><?php echo $lang["zones"]["stormSound"];?> :</label><input type="button" value="None" style="width:140px;" id="stormSound" onClick="$('#dialog-add-sounds').data('opener', this).dialog('open');" onChange="global.environement.changeMeteoSound('storm');" /><br />
			<br />
		</fieldset>
	</div>
	<div style="display:inline-block;vertical-align:top;">
		<fieldset><legend><?php echo $lang["zones"]["legentShadow"];?></legend>
			<label class="dialog"><?php echo $lang["zones"]["activeShadow"];?> :</label><input type="checkbox" id="optionShadowEnabled" value="1" onChange="global.environement.changeShadow('enabledShadow');" /><br />
			<label class="dialog"><?php echo $lang["zones"]["shadowLevel"];?> :</label><input type="checkbox" id="shadowLevel" value="0.6" onChange="global.environement.changeShadow('shadowLevel');" /><br />
			<label class="dialog"><?php echo $lang["zones"]["filterShadow"];?> :</label><select id="filterShadow" onChange="global.environement.changeShadow('filterShadow');">
													<option value="0">NONE</option>													
													<option value="usePoissonSampling">Poisson Sampling</option>
													<option value="useExponentialShadowMap">Exponential</option>
													<option value="useBlurExponentialShadowMap">Blur Exponential</option>
													<option value="useBlurCloseExponentialShadowMap">Blur Close Exponential</option>
												</select><br />
			<label class="dialog"><?php echo $lang["zones"]["resolutionShadow"];?> :</label><select id="resolutionShadow" onChange="global.environement.changeShadow('resolutionShadow');">
													<option value="512">512*512</option>
													<option value="1024">1024*1024</option>
													<option value="2048">2048*2048</option>
													<option value="2560">2560*2560</option>
													<option value="3072">3072*3072</option>
												</select><br />
			<label class="dialog"><?php echo $lang["zones"]["biasShadow"];?> :</label><input type="text" id="biasShadow" placeholder="0.00005" size="13" onChange="global.environement.changeShadow('biasShadow');" /><br />
			<label class="dialog"><?php echo $lang["zones"]["distanceMinZ"];?> :</label><input type="text" id="distanceMinZ" placeholder="5" size="13" onChange="global.environement.changeShadow('distanceShadowMinZ');" /><br />
			<label class="dialog"><?php echo $lang["zones"]["distanceMaxZ"];?> :</label><input type="text" id="distanceMaxZ" placeholder="200" size="13" onChange="global.environement.changeShadow('distanceShadowMaxZ');" /><br />			
		</fieldset>
		<fieldset><legend>Post-process</legend>
			<label class="dialog"><?php echo $lang["zones"]["effectProstProcess"];?> :</label><select id="effectPostprocess" onChange="global.environement.changePostProcess();">
													<option value="0">NONE</option>
													<option value="BlackAndWhite">Black And White</option>
													<option value="Blur">Blur</option>
													<option value="FXAA">FXAA</option>
													<option value="HDR">HDR</option>
													<option value="SSAO">SSAO</option>
													<option value="CellShading">Cell Shading</option>
												</select><br />
		</fieldset>
	</div>
</div>
<!-- Dialog ajouter des objets primitif -->
<div id="dialog-add-primitif" title="<?php echo $lang["zones"]["titleAddObjetPrimitif"];?>">
	<div style="display:inline-block;">
		<label class="dialog"><?php echo $lang["zones"]["addCube"];?> :</label><img src="Ressources/primitifs/box.png" style="margin-top:5px;margin-bottom:6px;" onClick="global.editor.addObject.createPrimitif('box');" />
		<label class="dialog"><?php echo $lang["zones"]["addSphere"];?> :</label><img src="Ressources/primitifs/sphere.png" style="margin-top:5px;margin-bottom:6px;" onClick="global.editor.addObject.createPrimitif('sphere');" />
		<label class="dialog"><?php echo $lang["zones"]["addCylindre"];?> :</label><img src="Ressources/primitifs/cylindre.png" style="margin-top:5px;margin-bottom:6px;" onClick="global.editor.addObject.createPrimitif('cylindre');" />
		<label class="dialog"><?php echo $lang["zones"]["addCone"];?> :</label><img src="Ressources/primitifs/cone.png" style="margin-top:5px;margin-bottom:6px;" onClick="global.editor.addObject.createPrimitif('cone');" />
	</div>
	<div style="display:inline-block;">
		<label class="dialog"><?php echo $lang["zones"]["addPyramide"];?> :</label><img src="Ressources/primitifs/pyramide.png" style="margin-top:5px;margin-bottom:6px;" onClick="global.editor.addObject.createPrimitif('pyramide');" />
		<label class="dialog"><?php echo $lang["zones"]["addTorus"];?> :</label><img src="Ressources/primitifs/torus.png" style="margin-top:5px;margin-bottom:6px;" onClick="global.editor.addObject.createPrimitif('torus');" />
		<label class="dialog"><?php echo $lang["zones"]["addPlan"];?> :</label><img src="Ressources/primitifs/plane.png" style="margin-top:5px;margin-bottom:6px;" onClick="global.editor.addObject.createPrimitif('plan');" />
		<label class="dialog"><?php echo $lang["zones"]["addDisque"];?> :</label><img src="Ressources/primitifs/disc.png" style="margin-top:5px;margin-bottom:6px;" onClick="global.editor.addObject.createPrimitif('disque');" />
	</div>
	<br /><hr />
	<label class="dialog"><?php echo $lang["zones"]["addVers"];?> :</label><select id="addObjetPrimitifTo" class="panel">
														<option value="faceCam"><?php echo $lang["zones"]["onFaceCameraOption"];?></option>
														<option value="centreMap"><?php echo $lang["zones"]["onCenterMapOption"];?></option>
														<option value="onObjectSelected"><?php echo $lang["zones"]["onMeshSelectedOption"];?></option>
													</select>
</div>
<!-- Dialog ajouter des medias -->
<div id="dialog-add-medias" title="Médias manager">
	<table>
		<tr>
			<td width="200px">
				<div id="CategorieMediaZone" style="width:100%;height:325px;border:1px solid black;overflow-y:auto;"></div>
				<button style="width:99px;margin-top:5px;" id="newCategorieMedia"><?php echo $lang["button"]["add"];?></button>
				<button style="width:99px;margin-top:5px;" id="deleteCategorieMedia"><?php echo $lang["button"]["delete"];?></button><br />
				<canvas id="HE-canvas-apercu-media"></canvas>
				<canvas id="HE-canvas-apercu-media-image" style="display:none;"></canvas>
			</td>
			<td width="416px">
				<div id="listeMediasZone" style="width:365px;height:560px;border:1px solid black;overflow-y:auto;"></div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<button style="width:111px;margin-top:5px;margin-bottom:6px;font-size:12px;" id="importMedia3d" onClick="$('#dialog-import-mesh').dialog('open');" ><?php echo $lang["button"]["import_mesh"];?></button>
				<button style="width:111px;margin-top:5px;margin-bottom:6px;font-size:12px;" id="importMediaImage" onClick="$('#dialog-import-image').dialog('open');" ><?php echo $lang["button"]["import_images"];?></button>
				<button style="width:111px;margin-top:5px;margin-bottom:6px;font-size:12px;" id="importMediaMusic" onClick="$('#dialog-import-music').dialog('open');" ><?php echo $lang["button"]["import_music"];?></button>
				<button style="width:111px;margin-top:5px;margin-bottom:6px;font-size:12px;" id="importMediaSound" onClick="$('#dialog-import-sound').dialog('open');" ><?php echo $lang["button"]["import_son"];?></button>
				<button style="width:111px;margin-top:5px;margin-bottom:6px;font-size:12px;" id="importMediaVideo" onClick="$('#dialog-import-video').dialog('open');" ><?php echo $lang["button"]["import_video"];?></button><br />
				<div id="optionAddObjet">
				<small><?php echo $lang["zones"]["addVers"];?> :</small><select id="addObjetComplexTo" class="panel">
														<option value="faceCam"><?php echo $lang["zones"]["onFaceCameraOption"];?></option>
														<option value="centreMap"><?php echo $lang["zones"]["onCenterMapOption"];?></option>
														<option value="onObjectSelected"><?php echo $lang["zones"]["onMeshSelectedOption"];?></option>
													</select>
				<small><?php echo $lang["zones"]["closeLibAfterAdd"];?> :</small><input type="checkbox" id="closeLibsAfterAdd" value="1" />
				</div>
				<button id="goBoutique" style="float:right;"><?php echo $lang["button"]["boutique"];?></button><br />
			</td>
		</tr>
	</table>
</div>
<!-- Dialog ajouter des lumière -->
<div id="dialog-add-light" title="<?php echo $lang["zones"]["titleAddLight"];?>">
	<label class="dialog"><?php echo $lang["zones"]["addSpotLight"];?> :</label><button style="width:136px;margin-top:5px;margin-bottom:6px;" onClick="global.editor.addObject.createLight('spot');" ><?php echo $lang["button"]["add"];?></button><br />
	<label class="dialog"><?php echo $lang["zones"]["addPointLight"];?> :</label><button style="width:136px;margin-top:5px;margin-bottom:6px;" onClick="global.editor.addObject.createLight('point');" ><?php echo $lang["button"]["add"];?></button><br />
	<label class="dialog"><?php echo $lang["zones"]["addVers"];?> :</label><select id="addObjetLightTo" class="panel">
														<option value="faceCam"><?php echo $lang["zones"]["onFaceCameraOption"];?></option>
														<option value="centreMap"><?php echo $lang["zones"]["onCenterMapOption"];?></option>
														<option value="onObjectSelected"><?php echo $lang["zones"]["onMeshSelectedOption"];?></option>
													</select>
</div>
<!-- Barre d'outils top -->
<div class="header" id="header">
	<ul id="toolbarsTop" style="list-style-type:none;margin:0px;padding:0px;">
		<li class="ui-state-default" style="display:inline-block;" id="positionControl">
			<fieldset style="min-width:0px;margin:0px;padding:0px;display:inline-block;padding-left:8px;padding-right:8px;"><legend class="toolbar-legend"><?php echo $lang["zones"]["toolBarLegend_Control"];?></legend>
				<img src="Ressources/3dmove.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpControlMove"];?>" onCLick='global.editor.manipulatorControl("deplacement");' />
				<img src="Ressources/3drotate.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpControlRotate"];?>" onCLick='global.editor.manipulatorControl("rotation");' />
				<img src="Ressources/3dscale.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpControlScale"];?>" onCLick='global.editor.manipulatorControl("scale");' />
				<img src="Ressources/pointer.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpControlMain"];?>" onCLick='global.editor.manipulatorControl("pointer");' />
				<sup>
					<label for="local">Local <input type="checkbox" id="local" title="<?php echo $lang["zones"]["bullhelpControlLocalOrWorld"];?>" onCLick='global.editor.manipulatorControl("local");' checked="true"></label>
					<label for="snap">Snap <input type="checkbox" id="snap" title="<?php echo $lang["zones"]["bullhelpControlSnap"];?>" onCLick='global.editor.manipulatorControl("snap");'></label>
				</sup>
				<img src="Ressources/undo.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpControlUndo"];?>" onCLick='global.editor.manipulatorControl("undo");' />
				<img src="Ressources/redo.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpControlRedo"];?>" onCLick='global.editor.manipulatorControl("redo");' />
				<img src="Ressources/target.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpControlFocus"];?>" onCLick='global.editor.manipulatorControl("focus");' />
				<sup><select id="select-cameraMode" title="Camera mode">
					<option value="fly">Camera fly</option>
					<option value="walk">Camera walk</option>
					<option value="orbital">Camera orbital</option>
				</select></sup>
			</fieldset>
		</li>
		<li class="ui-state-default" style="display:inline-block;" id="positionAdd">
			<fieldset style="min-width:0px;margin:0px;padding:0px;display:inline-block;padding-left:8px;padding-right:8px;"><legend class="toolbar-legend"><?php echo $lang["zones"]["toolBarLegend_Ajouter"];?></legend>
				<img src="Ressources/primitifs.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpAddPritif"];?>" onClick="$('#dialog-add-primitif').dialog('open');" />
				<img src="Ressources/hut.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpAddMeshComplex"];?>" onClick="$('#dialog-add-medias').dialog('open');" />
				<img src="Ressources/skybox.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpParametreSky"];?>" onClick="global.editor.addObject.createSkyBox();" />
				<img src="Ressources/lighting_32.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpAddLight"];?>" onClick="$('#dialog-add-light').dialog('open');" />
				<img src="Ressources/material.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpAddMaterial"];?>" onClick="$('#dialog-materiel').dialog('open');" />
				<img src="Ressources/music.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpAddSound"];?>" onCLick="global.editor.addObject.createZoneSound();" />
				<img src="Ressources/portal.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpAddPortail"];?>" onCLick="global.editor.addObject.createZonePortail();" />
				<img src="Ressources/water.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpAddWater"];?>" onCLick="global.editor.addObject.createOcean(512);" />
				<img src="Ressources/cadena.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpAddMurCollision"];?>" onCLick="global.editor.addObject.createZoneCollisionBox();" />
				<img src="Ressources/triggers.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpAddTrigger"];?>" onCLick="global.editor.addObject.createZoneTriggerScript();" />
				<img src="Ressources/triangle-blue.png" id="waypointsStart" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpAddWaypointAndSpanpoints"];?>" onCLick="global.editor.addObject.createFlagWaypoint(false);" />
				<img src="Ressources/triangle-green.png" id="waypointsEnd" style="display:none;width:22px;height:22px;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpAddFinishWaypointAndSpanpoints"];?>" onCLick="global.editor.addObject.createFlagWaypoint(true);" />
				<img src="Ressources/zoneInteret.png" style="width:22px;height:22px;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpAddZoneInteretBox"];?>" onCLick="global.editor.addObject.createZoneInteret();" />
			</fieldset>
		</li>		
		<li class="ui-state-default" style="display:inline-block;" id="positionTerrain">
			<fieldset style="min-width:0px;margin:0px;padding:0px;display:inline-block;padding-left:8px;padding-right:8px;"><legend class="toolbar-legend"><?php echo $lang["zones"]["toolBarLegend_Terrain"];?></legend>
				<img src="Ressources/Tree.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpGenererForest"];?>" onClick="$('#dialog-generate-forest').dialog('open');" />
				<img src="Ressources/Grass.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="Peindre de l'herbe" onClick="$('#dialog-grass-editor').dialog('open');" />
			</fieldset>
		</li>
		<li class="ui-state-default" style="display:inline-block;" id="positionZone">
			<fieldset style="min-width:0px;margin:0px;padding:0px;display:inline-block;padding-left:8px;padding-right:8px;"><legend class="toolbar-legend"><?php echo $lang["zones"]["toolBarLegend_Zones"];?></legend>
				<sup>Terrain : <select id="select-zone" title="<?php echo $lang["zones"]["bullhelpSelectedGround"];?>" onChange="global.editor.addObject.loadGround(this.value);">
					<option value="0">None</option>
					<?php
					$dir = "_Projects/".$projet_name."/scenes/";
					if (is_dir($dir)) {
						if ($dh = opendir($dir)) {
							while (($file = readdir($dh)) !== false) {
								if($file != '..' && $file != '.') {
									if(is_dir($dir.$file) && !is_file($dir.$file) && $file != "completes") {
										echo "<option id=\"".$file."\" value=\"".$file."\">".$file."</option>";
									}
								}
							}
							closedir($dh);
						}
					}
					?>
				</select></sup>				
				<sup>Scene : <select id="select-scene" title="<?php echo $lang["zones"]["bullhelpSelectedScene"];?>" onChange="global.editor.addObject.loadScene(this.value);">
					<option value="0">None</option>
					<?php
					$dir = "_Projects/".$projet_name."/scenes/completes/";
					if (is_dir($dir)) {
						if ($dh = opendir($dir)) {
							while (($file = readdir($dh)) !== false) {
								if($file != '..' && $file != '.') {
									if(is_dir($dir.$file) && !is_file($dir.$file)) {
										echo "<option id=\"".$file."\" value=\"".$file."\">".$file."</option>";
									}
								}
							}
							closedir($dh);
						}
					}
					?>
				</select></sup>
				<img src="Ressources/scene.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpImportScene"];?>" onClick="$('#dialog-import-scene').dialog('open');" />
			</fieldset>
		</li>		
	</ul>
</div>
<!-- Panel de l'interface -->
<div id="DockingManager">
	<div class="middle-left">
		<div class="content-panel" id="explorer_window" style="height:50%;overflow:auto;">
			<div class="title-panel"> &nbsp; <?php echo $lang["zones"]["dockingTitleExplorer"];?></div>
			<img src='Ressources/close.png' style='position:relative;float:right;margin-right:8px;top:2px;cursor:pointer;' onCLick='global.editor.hidePanel("explorer_window");' />
			<div id="explorer" style="margin-top:20px;width:100%;"></div>
		</div>
	</div>
	<div class="middle-center content-panel" id="scene_window" style="overflow:hidden;">
		<div class="title-panel" id="title-panel-scene"> &nbsp; <?php echo $lang["zones"]["dockingTitleScene"];?> &nbsp; <img src="Ressources/throbber.gif" id="saveSceneEnCour" style="display:none" /></div>
		<canvas id="HE-canvas-scene"></canvas>
		<div id="show_stats_fps" class="statScene"></div>
		<div id="show_stats_scene" class="statScene"></div>
		<div id="saveScene" style="width:100%;height:100%;"></div>
	</div>
	<div class="middle-east content-panel" id="properties_window" style="overflow:auto;">
		<div class="title-panel" style="width:99.2%;"> &nbsp; <?php echo $lang["zones"]["dockingTitleProperty"];?></div>
		<div id='propGrid' class="content-panel" style="margin-top:20px;"></div>
	</div>
	<div class="middle-bottom">
		<div class="content-panel" id="chat_window" style="width:49.76%;height:97.5%;float:left;border:2px solid #cccccc;position:relative;overflow:auto;">
			<div class="title-panel" style="width:99.75%;"> &nbsp; <?php echo $lang["zones"]["dockingTitleChat"];?></div>
			<img src='Ressources/close.png' style='position:absolute;right:8px;top:2px;cursor:pointer;' onCLick='global.editor.hidePanel("chat_window");' />
			<div id="chat" style="margin-top:20px;width:100%;overflow:auto;font-size:14px;text-align:left;">
				<form id="chatmessage" name="chatmessage" method="POST" onKeyUp="global.chat.disableEnterKey(event);" onSubmit="global.chat.addmessage(); return false;">
					<input type="text" class="messageChat" name="message" id="messagetext" value="" placeholder="<?php echo $lang["zones"]["chatPlaceholder"];?> ?" style="width:92%;background:#d4ffd3;" />
					<input id="button" class="buttonParler" type="button" onClick="global.chat.addmessage();" value="<?php echo $lang["button"]["envoyer"];?>" />
				</form>
				<div id="chat-message" style="margin-top:5px;width:100%;overflow:auto;font-size:14px;text-align:left;font-weight:bold;"></div>
			</div>
		</div>
		<div class="content-panel" id="problems_window" style="width:49.76%;height:97.5%;float:left;border:2px solid #cccccc;position:relative;overflow:auto;background-color:#e6dfdf;">
			<div class="title-panel" style="width:99.75%;"> &nbsp; <?php echo $lang["zones"]["dockingTitleConsole"];?></div>
			<img src='Ressources/close.png' style='position:absolute;right:8px;top:2px;cursor:pointer;' onCLick='global.editor.hidePanel("problems_window");' />
			<div id="errors" style="margin-top:20px;width:100%;"></div>
		</div>
	</div>
</div>
<!-- Barre d'outils bottom -->
<div class="footer" id="footer">
	<ul id="toolbarsBottom" style="list-style-type:none;margin:0px;padding:0px;">
		<li class="ui-state-default" style="display:inline-block;" id="positionOptions">
			<fieldset style="min-width:0px;margin:0px;padding:0px;display:inline-block;padding-left:8px;padding-right:8px;"><legend class="toolbar-legend"><?php echo $lang["zones"]["toolBarLegend_Option"];?></legend>
				<img src="Ressources/options.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpOptionsInterface"];?>" onClick="$('#dialog-options').dialog('open');" />
				<img src="Ressources/saisons.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpParametreEnvironement"];?>" onClick="sceneEditor.openDialogEnvironement();" />
			</fieldset>
		</li>		
		<li class="ui-state-default" style="display:inline-block;" id="positionPanel">
			<fieldset style="min-width:0px;margin:0px;padding:0px;display:inline-block;padding-left:8px;padding-right:8px;"><legend class="toolbar-legend"><?php echo $lang["zones"]["toolBarLegend_Panneaux"];?></legend>
				<img src="Ressources/explorer.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpDisplayExplorer"];?>" onClick='global.editor.showPanel("explorer_window");' />
				<img src="Ressources/error.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpDisplayConsole"];?>" onClick='global.editor.showPanel("problems_window");' />
				<img src="Ressources/grid.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpDisplayGridScene"];?>" onClick='global.editor.switchGrid();' />
				<img src="Ressources/zoneInteret.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpDisplayZoneInterest"];?>" onClick='global.editor.showZoneInterest();' />
				<img src="Ressources/debug.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpDisplayDebuger"];?>" onClick='global.editor.switchDebugLayer();' />
				<img src="Ressources/fps.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpDisplayFPS"];?>" onClick='global.editor.switchFPS();' />
				<img src="Ressources/statistics.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpDisplayStatistiques"];?>" onClick='global.editor.switchStats();' />
			</fieldset>
		</li>		
		<li class="ui-state-default" style="display:inline-block;" id="positionCollaboration">
			<fieldset style="min-width:0px;margin:0px;padding:0px;display:inline-block;padding-left:8px;padding-right:8px;"><legend class="toolbar-legend"><?php echo $lang["zones"]["toolBarLegend_Collaboration"];?></legend>
				<img src="Ressources/chat.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpDisplayChat"];?>" onClick='global.editor.showPanel("chat_window");' />
				<img src="Ressources/network.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpEnvoyerUpdate"];?>" onCLick='equipe.send();' />
				<img src="Ressources/update.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="<?php echo $lang["zones"]["bullhelpRecevoirUpdate"];?>" onCLick='equipe.receive();' />
				<img src="Ressources/bullet_green.png" id="avertirCollaborateurOfUpdate" style="width:22px;height:22px;display:inline;cursor:auto;" title="<?php echo $lang["zones"]["bullhelpInfoUpdate"];?>" />
			</fieldset>
		</li>
	</ul>
</div>
<!-- Fichier Javascript de l'onglet Zone -->
<script src="https://preview.babylonjs.com/inspector/babylon.inspector.bundle.js?<?php echo time();?>"></script>
<script src="https://preview.babylonjs.com/materialsLibrary/babylon.fireMaterial.min.js?<?php echo time();?>"></script>
<script src="https://preview.babylonjs.com/materialsLibrary/babylon.waterMaterial.min.js?<?php echo time();?>"></script>
<script src="https://preview.babylonjs.com/materialsLibrary/babylon.lavaMaterial.min.js?<?php echo time();?>"></script>
<script src="https://preview.babylonjs.com/materialsLibrary/babylon.furMaterial.min.js?<?php echo time();?>"></script>
<script src="https://preview.babylonjs.com/materialsLibrary/babylon.cellMaterial.min.js?<?php echo time();?>"></script>
<script src="https://preview.babylonjs.com/materialsLibrary/babylon.customMaterial.min.js?<?php echo time();?>"></script>
<script src="https://preview.babylonjs.com/gui/babylon.gui.min.js?<?php echo time();?>"></script>
<script src="http://cdn.rawgit.com/NasimiAsl/Extensions/master/ShaderBuilder/Babylonx.ShaderBuilder.js"></script>

<script src="JS/zones/chat.js?<?php echo time();?>"></script>
<script src="JS/zones/grassControl.js?<?php echo time();?>"></script>
<script src="JS/zones/addObject.js?<?php echo time();?>"></script>
<script src="JS/zones/environement.js?<?php echo time();?>"></script>
<script src="JS/zones/editor.js?<?php echo time();?>"></script>
<script src="JS/zones/property.js?<?php echo time();?>"></script>
<script src="JS/zones/manipulator.js?<?php echo time();?>"></script>
<!-- point d'entrer de la creation de la scène et des evenements de l'editeur -->
<script>
global.chat = new Chat();
global.assets = new assets();
global.assets.assetZone();
global.assets.jquery();
global.canvas_for_screenshot = document.getElementById("HE-appercu-meshes-for-screenshot");
global.engine_for_screenshot = new BABYLON.Engine(global.canvas_for_screenshot, true);
global.engine_for_screenshot.enableOfflineSupport = false;
global.canvas_for_material = document.getElementById("HE-canvas-apercu-material");
global.engine_for_material = new BABYLON.Engine(global.canvas_for_material, true);
global.engine_for_material.enableOfflineSupport = false;
global.canvas = document.getElementById("HE-canvas-scene");
global.engine = new BABYLON.Engine(global.canvas, true, {stencil: true, preserveDrawingBuffer: true});
global.environement = new environement();
global.editor = new sceneEditor();
global.editor.createScene();
</script>