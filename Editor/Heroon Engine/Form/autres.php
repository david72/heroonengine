<?php
/*##################################################
 *                                autres.php
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 01/01/2017
 ###################################################

Modification de certaine donner du jeu inclassable (pour le combat, la monnaie et le chat)
 */
?>
<table width="55%">
	<tr>
		<td>
			<br />
			<fieldset style="width:85%"><legend><?php echo $lang["autres"]["legendCombat"];?></legend>
				<label class="big"><?php echo $lang["autres"]["scriptFormulCOmbat"];?> :</label><select id="formuleCombat" style="width:140px">
																								<option value="default_fight">default_fight.js</option>
																								<option value="personalized_fight">personalized_fight.js</option>
																							</select><br />
				<label class="big"><?php echo $lang["autres"]["displayDamage"];?> :</label><select id="afficheDomage" style="width:140px">
																						<option value="Flottant"><?php echo $lang["autres"]["optionFloatant"];?></option>
																						<option value="Chat"><?php echo $lang["autres"]["optionChat"];?></option>
																						<option value="ChatAndFlottant"><?php echo $lang["autres"]["optionChatandFloatant"];?></option>
																					</select><br />				
				<label class="big"><?php echo $lang["autres"]["collisionActor"];?> :</label><select id="collisionActeur" style="width:140px">
																<option value="false"><?php echo $lang["generality"]["no"];?></option>
																<option value="true"><?php echo $lang["generality"]["yes"]." ".$lang["autres"]["notAviable"];?></option>
															</select> <?php echo $lang["autres"]["infoCollisionActor"];?><br />												
				<label class="big"><?php echo $lang["autres"]["projectionSang"];?> :</label><input type="button" onClick="$('#dialog-add-images').data('opener', this).dialog('open');" style="width:140px;white-space:nowrap;direction:rtl;" id="sang" value="None" /> <?php echo $lang["autres"]["infoProjectionSang"];?>
			</fieldset>
			<br />
			<fieldset style="width:85%;"><legend><?php echo $lang["autres"]["legendArgent"];?></legend>
				<label class="big"><?php echo $lang["autres"]["monaies1"];?> :</label><input type="text" id="argentNameprimaire" value="" placeholder="Euro" style="width:140px" /><br />
				<label class="big"><?php echo $lang["autres"]["monaies2"];?> :</label><input type="text" id="argentNameSecondaire" value=""  placeholder="Centime" style="width:140px" /><br />
				<label class="big"><?php echo $lang["autres"]["argentdefaut"];?> :</label><input type="text" id="argentdefaut" value="150" placeholder="150" style="width:140px" />
			</fieldset>
			<br />
			<fieldset style="width:85%"><legend><?php echo $lang["autres"]["legendChat"];?></legend>
				<label class="big"><?php echo $lang["autres"]["displayPseudo"];?> :</label><select id="affichePseudo" style="width:140px">
																		<option value="true"><?php echo $lang["generality"]["yes"];?></option>
																		<option value="false"><?php echo $lang["generality"]["no"];?></option>
																	</select><br />
				<label class="big"><?php echo $lang["autres"]["displayMessage"];?> :</label><select id="afficheMessage" style="width:140px">
																		<option value="Flottant"><?php echo $lang["autres"]["optionFloatant"];?></option>
																		<option value="Chat"><?php echo $lang["autres"]["optionChat"];?></option>
																		<option value="ChatAndFlottant"><?php echo $lang["autres"]["optionChatandFloatant"];?></option>
																	</select><br />
				<label class="big"><?php echo $lang["autres"]["colorText"];?> :</label><input type="text" id="color-text" class="colorwell" value="black" size="9" /><br />
				<label class="big"><?php echo $lang["autres"]["colorFond"];?> :</label><input type="text" id="color-fond" class="colorwell" value="white" size="9" />
			</fieldset>
		</td>
	</tr>
</table>
<script src="JS/actors/autres.js?<?php echo time();?>" type="text/javascript"></script>