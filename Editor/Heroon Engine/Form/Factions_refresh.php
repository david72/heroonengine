<?php session_start();
error_reporting(0);
$projet_Selected = file_get_contents('../Data Project/selected.dat');
$cheminFichierJson = '../_Projects/'.$projet_Selected.'/game data/actors/faction.json';
$file_json = file_get_contents($cheminFichierJson);
$json = json_decode($file_json, true);
$arrayColone = array();
$lign = 0;
echo "<table align='left' valign='top' border cellspacing='2' cellpadding='5'><tr><th style='background-color:#b3b3b3;'></th>";
foreach($json["factions"] as $key => $val) {
	if($key) {
		echo "<th>".$key."</th>";
		$arrayColone[] = $key;
	}
}
echo '</tr>';
foreach($json["factions"] as $key => $val)
{
	$data = $json["factions"][$key];
	$nb_col = count($json["factions"]);
	echo "<tr id='select".$lign."' onClick='selectLigne(\"".$arrayColone[$lign]."\", \"".$lign."\", $nb_col);'><th>".$arrayColone[$lign]."</th>";
	for($i = 0; $i < $nb_col; $i++) {		
		if($arrayColone[$i] == $key) {
			$disabled = 'disabled';
			$color='#b3b3b3';
			echo '<script>$("#factionActor").append(\'<option id="'.$arrayColone[$i].'" value="'.$arrayColone[$i].'">'.$arrayColone[$i].'</option>\');</script>';
		} else {
			$disabled = null;
			$color='white';
		}
		echo "<td align='center'>";
				if($disabled) {
					echo "<input style='text-align:center;background-color:".$color.";' id='lign".$key."_champs".$arrayColone[$i]."' $disabled value='0' size=5 />";	
				} else {					
					echo "<select style='background-color:".$color.";' onClick='selectColone(\"".$arrayColone[$i]."\");' id='lign".$key."_champs".$arrayColone[$i]."' onChange='changeFaction(\"".$key."\", \"".$arrayColone[$i]."\");'>
							<option value='".$data[$arrayColone[$i]]."' selected>".$data[$arrayColone[$i]]."</option>
							<option value='Friend'>Friend</option>
							<option value='Neutral'>Neutral</option>
							<option value='Enemy'>Enemy</option>
						</select>";
				}
			echo "</td>";
	}
	echo "</tr>";
	$lign++;
}
echo "</table>";
?>