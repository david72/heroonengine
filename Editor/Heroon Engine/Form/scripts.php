<?php
/*##################################################
 *                                scripts.php
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 01/01/2017
 ###################################################

Créer des scripts pour le jeu et le serveur
 */
?>
<table width="100%">
	<tr>
		<td class="margeLeft">
			<button id="newScriptjs" style="cursor:pointer;margin-left:3px;" title="<?php echo $lang["script"]["titleCreateScriptJS"];?>"><img src="Ressources/js.png" style="width:22px;height:22px;padding:0;margin:0;" /></button>
			<button id="newScriptphp" style="cursor:pointer;margin-left:1px;" title="<?php echo $lang["script"]["titleCreateScriptPHP"];?>"><img src="Ressources/php.png" style="width:22px;height:22px;padding:0;margin:0;" /></button>
			<button id="saveScript" style="cursor:pointer;margin-left:1px;" title="<?php echo $lang["script"]["saveScript"];?>"><img src="Ressources/32_save.png" style="width:22px;height:22px;padding:0;margin:0;" /></button>
			<button id="renameScript" style="cursor:pointer;margin-left:1px;" title="<?php echo $lang["script"]["renameScript"];?>"><img src="Ressources/rename.png" style="width:22px;height:22px;padding:0;margin:0;" /></button>
			<button id="deleteScript" style="cursor:pointer;margin-left:1px;" title="<?php echo $lang["script"]["deleteScript"];?>"><img src="Ressources/iconRemove.png" style="width:22px;height:22px;padding:0;margin:0;" /></button>
		</td>
		<td style="border:1px solid black;" rowspan="2" valign="top">
			<form>
				<textarea id="code" name="code"></textarea>
			</form>
		</td>
	</tr>
	<tr>
		<td class="margeLeft"><div id="ListeScript"></div></td>
	</tr>
</table>
<script src="Library/codemirror.js" type="text/javascript"></script>
<script src="Library/match-highlighter.js" type="text/javascript"></script>
<script src="Library/show-hint.js" type="text/javascript"></script>
<script src="Library/javascript-hint.js" type="text/javascript"></script>
<script src="Library/jshint.js" type="text/javascript"></script>
<script src="Data Project/Default/Engines/scripting.min.js?<?php echo time();?>" type="text/javascript"></script>
<script src="JS/scripts.js?<?php echo time();?>" type="text/javascript"></script>
<script>
var script = null, scriptActive, changing, fileWritten, widgets = [], waiting, colorActif = "blue", colorBase = "black";
$(function() {
	// Fixe un bug de codemirror
	$("div.CodeMirror-linenumber.CodeMirror-gutter-elt").css({"left":"-30px"});
});
script = new scriptEditor();
script.jquery();
</script>