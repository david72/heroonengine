<?php
/*##################################################
 *                                index.php
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 01/01/2017
 ###################################################*/
ini_set("memory_limit","1024M");
ini_set("post_max_size","1024M");
ini_set("upload_max_filesize","1024M");
ini_set("max_execution_time","300");
session_start();
define('PATH_TO_ROOT', '.');
require_once PATH_TO_ROOT.'/CheckProject.php';
require_once PATH_TO_ROOT.'/GE.php';
?>