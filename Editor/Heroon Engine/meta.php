<!-- Style -->
<?php
if($theme == "Black") { echo '<link rel="stylesheet" type="text/css" href="Styles/Black/monokai.css">'; }
elseif($theme == "White") { echo '<link rel="stylesheet" type="text/css" href="Styles/White/eclipse.css">'; }
elseif($theme == "Blue") { echo '<link rel="stylesheet" type="text/css" href="Styles/Blue/cobalt.css">'; }
?>
<link rel="stylesheet" type="text/css" href="Styles/<?php echo $theme;?>/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="Styles/<?php echo $theme;?>/editor.css?<?php echo time();?>">
<link rel="stylesheet" type="text/css" href="Styles/codemirror.css">
<link rel="stylesheet" type="text/css" href="Styles/show-hint.css">
<link rel="stylesheet" type="text/css" href="Styles/select2.css">
<link rel="stylesheet" type="text/css" href="Styles/jquery.contextMenu.css">
<link rel="stylesheet" type="text/css" href="Styles/spectrum.css">
<link rel="stylesheet" type="text/css" href="Styles/jquery.alerts.css">
<link rel="stylesheet" type="text/css" href="Styles/layout-default-latest.css">
<link rel="stylesheet" type="text/css" href="Styles/jstree.css">
<!-- libs -->
<script src="Library/jquery.min.js"></script>
<script src="Library/jquery-ui.min.js"></script>
<script src="Library/jquery.contextMenu.js"></script>
<script src="Library/jquery.editable.js"></script>
<script src="Library/jquery.alerts.js"></script>
<script src="Library/jquery.contextMenu.js"></script>
<script src="Library/select2.full.min.js"></script>
<script src="Library/jquery.layout.js"></script>
<script src="Library/jstree.min.js"></script>
<script src="Library/jquery.knob.js"></script>
<script src="Library/pep.js"></script>
<script src="Library/spectrum.js"></script>
<script src="Library/propertyGrid.js?<?php echo time();?>"></script>
<!-- Engine Babylon -->
<script src="https://preview.babylonjs.com/cannon.js?<?php echo time();?>"></script>
<script src="https://preview.babylonjs.com/babylon.js?<?php echo time();?>"></script>
<script src="https://preview.babylonjs.com/loaders/babylon.stlFileLoader.min.js"></script>
<script src="https://preview.babylonjs.com/loaders/babylon.objFileLoader.min.js"></script>
<script src="https://preview.babylonjs.com/loaders/babylon.glTF1FileLoader.min.js"></script>
<!-- Editor -->
<script src="JS/collaborations.js?<?php echo time();?>"></script>
<script src="JS/addons.js?<?php echo time();?>"></script>
<script src="JS/assets.js?<?php echo time();?>"></script>