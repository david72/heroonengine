/*jshint esversion: 6 */
class death {
	
	constructor() {
		
	}
	
	Start() {	 
        
        let actor = ACTOR.GetActor();        
        let random = Math.floor((Math.random() * 3));
        let killer = ACTOR.GetTarget();
        
        // Inform player
        if (killer.name !== null) {            
        	console.log("You were killed by: " + killer.name);            
        } else {
            console.log("You have died...");
    	}
        
        // Play death animation
        if (random === 0) {
            ACTOR.SetAnimate(actor, "Death 1", false, 1.0);        
        } else if (random == 1) {
            ACTOR.SetAnimate(actor, "Death 2", false, 1.0);
        } else {
            ACTOR.SetAnimate(actor, "Death 3", false, 1.0);
        }       
        ACTOR.SetAttribute(Player, "Health", 0);
        
        SCRIPT.RespawnTimer(actor);
	}
    
    RespawnTimer(Player) 
    {
        setTimeout(function() {
            // Restore la santé du joueur et le retire de l'argent pour la restauration
            ACTOR.SetAttribute(Player, "Health", ACTOR.GetAttribute(Player, "Health"));
            ACTOR.SetMoney(Player, -10, false);
            // Rediriger l'acteur au portail de d'arriver
            ACTOR.Warp(Player, "Start");
            
        }, 5000);
    }
 
}