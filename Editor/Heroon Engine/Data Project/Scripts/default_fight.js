/*jshint esversion: 6 */
class default_fight {
	
	constructor() {
		
	}
	
	Start()
    {		
		let actor = ACTOR.GetActor();
        let target = ACTOR.GetTarget();
        
        let chanceToHit = 10; // 90% chance to hit
        let random = Math.floor((Math.random() * 100) + 1); // random return 1 to 100
        let ToHit = random; // value random for hit

        if(ToHit > chanceToHit) {            
            let Strength = actor.GetAttribute(StrengthAttribute) || 1; // force
            let weapon = actor.Backpack(ItemSlot.Weapon) || {};
            let StrengthAttribute = null;
       		let ToughnessAttribute = null;
        	let HealthAttribute = null;
        	let ItemSlot = {}; 
            let damageType = 0;
            let damage = 0;
            
            if(weapon !== null) {
                // Actor has weapon
                if(weapon.Health > 0) {
                    // Not broken weapon
                    damage = weapon.Damage;
                    if(Strength < damage) {
                        damage -= random.Next(5, 8);
                    } else if(Strength > damage) {
                        damage += Math.floor(Math.random() * 8) + 5;
                    } else {
                        damage += Math.floor(Math.random() - 5) + 5;
                    }
                    damageType = weapon.DamageType;
                } else {
                    // Broken weapon
                    damage = Math.floor(Math.random() - 2) + 6;
                    damageType = actor.DefaultDamageType;
                }
            } else {
                // Unarmed / Chuck Norrissing
                damage = Math.floor(Math.random() - 5) + 4;
                damageType = actor.DefaultDamageType;
            }

            // Critical damage (4 in 10 chance to critical damage)
            let criticalDamageChance = (Math.floor(Math.random() * 10) + 4);
            if(criticalDamageChance >= 7) {
                damage = criticalDamageChance;
                console.log("You hit a critical damage!");
            } 

            // Armour
            let ArmourPoints = SCRIPT.GetArmourLevel(target, ItemSlot) + (target.GetResistance(damageType) - 100);
            ArmourPoints += (target.GetAttribute(ToughnessAttribute) / 8);
            damage -= ap;

            // Minimum of 1
            if(damage === 0) {
                damage = 1;
            }

            // Animation
            if(weapon) {               
                ACTOR.SetAnimate(actor, "Attack Weapon", true, 1.0);                
            } else {
                ACTOR.SetAnimate(actor, "Attack Hand", true, 1.0);
            }

            ACTOR.SetFloatNumber(actor, damage);
            
            let vectorPoint = new BABYLON.Vector3(actor.position.x, actor.getBoundingInfo().maximum.y/2, actor.z);
            ACTOR.SetBood(actor, vectorPoint, null);
            
            target.SetAttribute(HealthAttribute, target.GetAttribute(HealthAttribute) - damage);

            SCRIPT.DamageWeapon(weapon);
            SCRIPT.DamageArmour(target, ItemSlot);

        } else { // Miss!       
            return null;
        }
    }

    DamageWeapon(weapon)
    {
        // 1 in 5 chance to damage item
        if(weapon !== null && Math.floor(Math.random() * 5) + 1 == 3) {
            if(weapon.Health !== null) {
                weapon.Health = weapon.Health - 1;
                if(weapon.Health < 0) {
                    weapon.Health = 0;
                }
            } else {
                return false;
            }
        }
    }

    DamageArmour(actor, ItemSlot)
    {
        // All wearable items
        for(let i = 0; i <= ItemSlot.Feet; i++) {
            if(Math.floor(Math.random() * 5) + 1 == 3) {
                let item = actor.Backpack((ItemSlot[i]));
                if(item !== null) {
                    item.Health -= 1;                
                    if(item.Healt < 0) {
                        item.Health = 0;
                    }
                } else {
                    return false;
                }
            }
        }
    }
    
    GetArmourLevel(actor, ItemSlot) // Gets total level of armour points from items an actor is sporting. 
    {
        let ArmourPoints = 0;
        // Pro tip - ItemSlot max value is 49.
        for(let i = 0; i <= 49; i++) {           
            let item = actor.Backpack((ItemSlot[i]));
            if(item !== null) {
                ArmourPoints += item.Armor;
            }
        }
        return ArmourPoints;
    }
 
}