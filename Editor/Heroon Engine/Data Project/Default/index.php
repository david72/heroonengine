<?php
session_start();
error_reporting(0);
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Connexion</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<link rel="shortcut icon" href="Game.ico">	
	<link rel="stylesheet" type="text/css" href="game.css?<?php echo time();?>">	
	<link rel="stylesheet" type="text/css" href="jquery.alerts.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js" type="text/javascript"></script>
	<script src="Engines/jquery.alerts.js" type="text/javascript"></script>
	<script src="Engines/SDK.min.js?<?php echo time();?>" type="text/javascript"></script>
	<script>
	SceneManager.setFolderPath("./");
	
	var getCookie = function(name) {
		if(document.cookie.length == 0) return null;
		var value = "; " + document.cookie;
		var parts = value.split("; " + name + "=");
		if (parts.length == 2) return parts.pop().split(";").shift();
	};
	var setCookie = function(name, value, days) {
		if(name && value) {
			var expires = "";
			if (days) {
				var date = new Date();
				date.setTime(date.getTime()+(days*24*60*60*1000));
				var expires = "; expires="+date.toGMTString();
			}
			document.cookie = name+"="+value+expires+"; path=/";
		}
	};
	</script>			
</head>
<body>
	<?php	
	// Connexion du joueur
	if(@$_POST['btnLogin'] && @$_POST['pseudo'] && @$_POST['pass'] || @$_POST['btnLogin'] && @$_POST['mail'] && @$_POST['pass']) {		
		$userExist = false;		
		$pseudo = $_POST['pseudo'];
		$mail = $_POST['mail'];
		$pass = $_POST['pass'];		
		$file = "game data/users.json";
		$json = json_decode(file_get_contents($file), true);		
		$nbrUsers = @count($json["users"]);
		for($i = 0; $i < $nbrUsers; $i++) {
			if($pseudo) {
				if($json["users"][$i]["pseudo"] == $pseudo && md5($pass) == $json["users"][$i]["pass"]) {
					$userExist = true;
					break;
				}
			} else if($mail) {
				if($json["users"][$i]["mail"] == $mail && md5($pass) == $json["users"][$i]["pass"]) {
					$userExist = true;
					break;
				}
			}
		}		
		if($userExist) {
			$_SESSION['pseudo'] = $pseudo;
			header("location: characterSet.php");
		} else {
			echo "<script id='connexionNotValide'>jAlert('Ce pseudo ou cette adresse mail ne corespond pas au mot de passe!', 'Erreur', function(e) { if(e) $('#connexionNotValide').remove(); });</script>";
		} 
	}	
	// Inscription du joueur
	else if(@$_POST['btnCompte'] && isset($_POST['pseudo']) && isset($_POST['mail']) && isset($_POST['pass'])) {
		if(filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL)) {
			$pseudoExist = false;
			$mailExist = false;		
			$pseudo = $_POST['pseudo'];
			$mail = $_POST['mail'];
			$pass = $_POST['pass'];		
			// Enregistrer dans un fichier
			$file = "game data/users.json";
			$json = json_decode(file_get_contents($file), true);		
			$nbrUsers = @count($json["users"]);
			for($i = 0; $i < $nbrUsers; $i++) {
				if($json["users"][$i]["pseudo"] == $pseudo) {
					$pseudoExist = true;
					break;
				}
				if($json["users"][$i]["mail"] == $mail) {
					$mailExist = true;
					break;
				}
			}
			if($pseudoExist == false) {
				if($mailExist == false) {
					$json["users"][$nbrUsers]["pseudo"] = $pseudo;
					$json["users"][$nbrUsers]["mail"] = $mail;
					$json["users"][$nbrUsers]["pass"] = md5($pass);	
					$json["users"][$nbrUsers]["actors"] = array();
					$json["users"][$nbrUsers]["info"] = array();
					@chmod($file, 0777);
					$save = json_encode($json, JSON_PRETTY_PRINT | JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE);
					@file_put_contents($file, $save);
				} else {
					echo "<script id='mailExist'>jAlert('Cette adresse mail existe déjà!', 'Attention', function(e) { if(e) $('#mailExist').remove(); });</script>";
				}
			} else {
				echo "<script id='pseudoExist'>jAlert('Ce pseudo existe déjà!', 'Attention', function(e) { if(e) $('#pseudoExist').remove(); });</script>";
			}
		} else {
			echo "<script id='mailNotValide'>jAlert('Cette adresse mail n\'est pas valide!', 'Erreur', function(e) { if(e) $('#mailNotValide').remove(); });</script>";
		}
	} else if(@$_POST['btnCompte']) {
		echo "<script>jAlert('Tous les champs ne sont pas remplie!', 'Erreur'});</script>";
	}
	$version = file_get_contents("Version.dat");
	$json = json_decode(file_get_contents("game data/projet.json"), true);
	?>
	<script>
	if(GameMenu.getCookie("HEGame-Lang") == null) {
		GameMenu.setCookie("HEGame-Lang", "French");
	}
	var login = new Login("login", {version: "<?php echo $version;?>", type: "<?php echo $json["update"]["licence"];?>"});
	login.createElements();	
	</script>
</body>
</html>