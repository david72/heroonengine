<?php
session_start();
error_reporting(0);
if($_SESSION['pseudo'] == false) {
	header("location: index.php");
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>CharacterSet</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<link rel="shortcut icon" href="Game.ico">	
	<link rel="stylesheet" type="text/css" href="game.css?<?php echo time();?>">
	<link rel="stylesheet" type="text/css" href="jquery.alerts.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js" type="text/javascript"></script>
	<script src="Engines/jquery.alerts.js" type="text/javascript"></script>
	<script src="https://preview.babylonjs.com/babylon.js?<?php echo time();?>" type="text/javascript"></script>
	<script src="https://preview.babylonjs.com/gui/babylon.gui.min.js?<?php echo time();?>"></script>
	<script src="Engines/scripting.min.js?<?php echo time();?>"></script>
	<script src="Engines/SDK.min.js?<?php echo time();?>" type="text/javascript"></script>
	<script>
	SceneManager.setFolderPath("./");
	</script>
</head>
<body>
	<div id="ContentCharacterSet" style="width:100%;height:100%;"></div>
	<script>
	var user = "<?php echo $_SESSION['pseudo'];?>";
	if(GameMenu.getCookie("HEGame-Lang") == null) {
		GameMenu.setCookie("HEGame-Lang", "French");
	}	
	
	var characterSet = new PlayerSelector(user);
	characterSet.createScene();
	
	var sceneManager = new SceneManager();
	
	characterSet.createElements();
	
	var attribute = new PlayerAttribute();
	attribute.createElements();
	</script>
</body>
</html>