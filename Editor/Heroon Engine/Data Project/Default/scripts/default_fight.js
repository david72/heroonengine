
class default_fight{constructor(){}
Start()
{let actor=ACTOR.GetActor();let target=ACTOR.GetTarget();let chanceToHit=10;let random=Math.floor((Math.random()*100)+1);let ToHit=random;if(ToHit>chanceToHit){let Strength=actor.GetAttribute(StrengthAttribute)||1;let weapon=actor.Backpack(ItemSlot.Weapon)||{};let ToughnessAttribute=null;let HealthAttribute=null;let damageType=0;let damage=0;if(weapon!==null){if(weapon.Health>0){damage=weapon.Damage;if(Strength<damage){damage-=random.Next(5,8);}else if(Strength>damage){damage+=Math.floor(Math.random()*8)+5;}else{damage+=Math.floor(Math.random()-5)+5;}
damageType=weapon.DamageType;}else{damage=Math.floor(Math.random()-2)+6;damageType=actor.DefaultDamageType;}}else{damage=Math.floor(Math.random()-5)+4;damageType=actor.DefaultDamageType;}
let criticalDamageChance=(Math.floor(Math.random()*10)+4);if(criticalDamageChance>=7){damage=criticalDamageChance;console.log("You hit a critical damage!");}
let ArmourPoints=SCRIPT.GetArmourLevel(target,ItemSlot)+(target.GetResistance(damageType)-100);ArmourPoints+=(target.GetAttribute(ToughnessAttribute)/8);damage-=ap;if(damage===0){damage=1;}
if(weapon){ACTOR.SetAnimate(actor,"Attack Weapon",true,1.0);}else{ACTOR.SetAnimate(actor,"Attack Hand",true,1.0);}
ACTOR.SetFloatNumber(actor,damage);let vectorPoint=new BABYLON.Vector3(actor.position.x,actor.getBoundingInfo().maximum.y/2,actor.z);ACTOR.SetBood(actor,vectorPoint,null);target.SetAttribute(HealthAttribute,target.GetAttribute(HealthAttribute)-damage);SCRIPT.DamageWeapon(weapon);SCRIPT.DamageArmour(target,ItemSlot);}else{return null;}}
DamageWeapon(weapon)
{if(weapon!==null&&Math.floor(Math.random()*5)+1==3){if(weapon.Health!==null){weapon.Health=weapon.Health-1;if(weapon.Health<0){weapon.Health=0;}}else{return false;}}}
DamageArmour(actor,ItemSlot)
{for(let i=0;i<=ItemSlot.Feet;i++){if(Math.floor(Math.random()*5)+1==3){let item=actor.Backpack((ItemSlot[i]));if(item!==null){item.Health-=1;if(item.Healt<0){item.Health=0;}}else{return false;}}}}
GetArmourLevel(actor,ItemSlot)
{let ArmourPoints=0;for(let i=0;i<=49;i++){let item=actor.Backpack((ItemSlot[i]));if(item!==null){ArmourPoints+=item.Armor;}}
return ArmourPoints;}}