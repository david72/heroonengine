<?php @error_reporting(0); @session_start();
if(file_get_contents("../mode.dat") == "dev") { $JSON_OPTION = JSON_PRETTY_PRINT; }
else { $JSON_OPTION = null; }
$rootFile = "../Data Project/editor.json";
$file = file_get_contents($rootFile);
$json = json_decode($file, true);
if(@$_POST['deconnexion']) {
	for($i=0;$i < count($json["collaborateurs"]["users"]); $i++) {
		if($json["collaborateurs"]["users"][$i]["pseudo"] == $_POST['user']) {
			$json["collaborateurs"]["users"][$i]["connecter"] = 0;
			session_destroy();
			break;
		}
	}
}
else if(@$_POST['updateTools']) {
	$lis = explode(';', $_POST['positionsToolbar']);
	foreach ($lis as $key => $val ) {
		$pos = explode('=', $val);
		$json["interface"]["tools"][$pos[0]] = $pos[1];
	}
}
else if(@$_POST['updateLayout']) {
	$json = json_decode($file, true);
	$json["interface"]["layout_panel"][$_POST['paneLayout']] = $_POST['etatLayout'];
}
else if(@$_POST['updateForm']) {
	$json = json_decode($file, true);
	$json["interface"]["layout_form"][$_POST['layout_form']] = $_POST['etat'];
}
else if(@$_POST['updateCollaborateur']) {
	$json = json_decode($file, true);
	$pass = base64_encode($_POST['pass']);
	$nbr_collaborateur = count($json["collaborateurs"]["users"]);
	$json["collaborateurs"]["users"][$nbr_collaborateur]["pseudo"] = $_POST['user'];
	$json["collaborateurs"]["users"][$nbr_collaborateur]["password"] = md5($pass);
	$json["collaborateurs"]["users"][$nbr_collaborateur]["mail"] = $_POST['mail'];
	$json["collaborateurs"]["users"][$nbr_collaborateur]["connecter"] = 0;
	$json["collaborateurs"]["users"][$nbr_collaborateur]["canEditeProject"] = "false";
	$json["collaborateurs"]["users"][$nbr_collaborateur]["canEditeMedia"] = "false";
	$json["collaborateurs"]["users"][$nbr_collaborateur]["canEditeActeurs"] = "false";
	$json["collaborateurs"]["users"][$nbr_collaborateur]["canEditeItems"] = "false";
	$json["collaborateurs"]["users"][$nbr_collaborateur]["canEditeZones"] = "true";
	$json["collaborateurs"]["users"][$nbr_collaborateur]["canEditeParticules"] = "false";
	$json["collaborateurs"]["users"][$nbr_collaborateur]["canEditeScripts"] = "false";
}
else if(@$_POST['configCollaborateur']) {
	$json = json_decode($file, true);
	$collaborateur = $_POST['userInt'];
	$json["collaborateurs"]["users"][$collaborateur]["canEditeProject"] = $_POST['canEditeProject'];
	$json["collaborateurs"]["users"][$collaborateur]["canEditeMedia"] = @$_POST['canEditeMedia'];
	$json["collaborateurs"]["users"][$collaborateur]["canEditeActeurs"] = $_POST['canEditeActeurs'];
	$json["collaborateurs"]["users"][$collaborateur]["canEditeItems"] = $_POST['canEditeItems'];
	$json["collaborateurs"]["users"][$collaborateur]["canEditeZones"] = $_POST['canEditeZones'];
	$json["collaborateurs"]["users"][$collaborateur]["canEditeParticules"] = $_POST['canEditeParticules'];
	$json["collaborateurs"]["users"][$collaborateur]["canEditeScripts"] = $_POST['canEditeScripts'];
}
if($_POST) {
	$save = json_encode($json, $JSON_OPTIONT);
	@chmod($rootFile, 0777);
	file_put_contents($rootFile, $save);
}
?>