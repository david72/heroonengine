<?php
@error_reporting(0);
@session_start();
ini_set("memory_limit","1024M");
ini_set("post_max_size","1024M");
ini_set("upload_max_filesize","1024M");
ini_set("max_execution_time","300");
if($_POST['terrainSelected']) {
	$extention = "_ground.babylon";
	$root = "../".$_POST['root']."/";
} else {
	$extention = "_data.babylon";
	$root = "../".$_POST['root']."/".$_POST['terrainName']."/";
	@mkdir($root, 0777);
}
$fichier = $_POST['file'].$extention;
$value = stripslashes($_POST['value']);
$value = str_replace("Eplus", "e+", $value);
$value = str_replace($_POST['cheminProject'], "../../", $value);
if(!file_exists($root.$fichier)) {
	file_put_contents($root.$fichier, "");
}
$save = $value;
@chmod($root.$fichier, 0777);
file_put_contents($root.$fichier, $save);
?>