<?php error_reporting(0);
session_start();
if(file_get_contents("../mode.dat") == "dev") { $JSON_OPTION = JSON_PRETTY_PRINT; }
else { $JSON_OPTION = null; }
$projet_Selected = file_get_contents('../Data Project/selected.dat');
$cheminFichierJson = '../_Projects/'.$projet_Selected.'/game data/actors/general.json';
$file_json = file_get_contents($cheminFichierJson);
$json = json_decode($file_json, true);
$value = $_POST['value'];
$path_parts = pathinfo($value);
$fichier = $path_parts['basename'];
if($fichier == "_None.png") {
	$value = "None";
}
$json[$_POST['type']] = $value;
$save = json_encode($json, $JSON_OPTION);
chmod($cheminFichierJson, 0777);
file_put_contents($cheminFichierJson, $save);
?>