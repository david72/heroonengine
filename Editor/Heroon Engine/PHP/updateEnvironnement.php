<?php @error_reporting(0); @session_start();
if(file_get_contents("../mode.dat") == "dev") { $JSON_OPTION = JSON_PRETTY_PRINT; }
else { $JSON_OPTION = null; }
$projet = $_POST['project'];
$element = @$_POST['element'];
$value = $_POST['value'];
$rootFile = "../_Projects/".$projet."/game data/environement.json";
$file = file_get_contents($rootFile);
$json = json_decode($file, true);
if(@$_POST['type'] == "create") {
	$newJson = array(
			"".$value."" => array(
				"Gravity"=> "-0.175",
				"Octree"=> "false",
				"Optimizer"=> "false",
				"EnabledPhysic"=> "false",
				"CheckCollision"=> "true",
				"playerMap"=> "true",
				"DayInAMonth"=> "30",
				"DayInAYear"=> "365",
				"DayCurrent"=> "8",
				"YearCurrent"=> "Juillet",
				"SecondPerMinute"=> "25",
				"Sunrise"=> "7",
				"Sunset"=> "22",
				"LoadingZone"=> "None",
				"Music"=> "None",
				"PVP"=> "false",
				"Script"=> "None",
				"SkyDay"=> "TropicalSunnyDay_nx.jpg",
				"SkyNight"=> "TropicalSunnyNight_nx.jpg",
				"Fog" => array(
					"OptionFogEnabled"=> "true",
					"ModeFog"=> "FOGMODE_EXP",
					"DensityFog"=> "",
					"StartFog"=> "",
					"EndFog"=> "",
					"ColorFog"=> "#d8d8d8"
				),
				"rainParticle"=> "None",
				"snowParticle"=> "None",
				"stormParticle"=> "None",
				"rainSound"=> "None",
				"snowSound"=> "None",
				"stormSound"=> "None",
				"CameraDistance"=> "6000",
				"shadow" => array(
					"enabledShadow"=> "true",
					"filterShadow"=> "0",
					"resolutionShadow"=> "1024",
					"dynamiqueShadow"=> "450.0",
					"staticShadow"=> "30.0",
					"biasShadow"=> "0.00005"
				),
				"postprocess"=> "FXAA"
			));
	array_push($json, $newjson);
} else if(@$_POST['type'] == "delete") {
	unset($json[$value]);
	unlink("../_Projects/".$projet."/scenes/".$_POST["oldValue"]."_ground.babylon");
	unlink("../_Projects/".$projet."/scenes/".$_POST["oldValue"].".png");
	unlink("../_Projects/".$projet."/scenes/".$_POST["oldValue"].".data");
} else if(@$_POST['type'] == "copy") {
	$groundCopy = $json[$_POST["oldNameZone"]];
	$json[$_POST["newNameZone"]] = $groundCopy;
} else if(@$_POST['type'] == "rename") {
	$groundOld = $json[$_POST["oldValue"]];
	$json[$_POST["newValue"]] = $groundOld;
	unset($json[$_POST["oldValue"]]);
	unlink("../_Projects/".$projet."/scenes/".$_POST["oldValue"]."_ground.babylon");
	unlink("../_Projects/".$projet."/scenes/".$_POST["oldValue"].".png");
	unlink("../_Projects/".$projet."/scenes/".$_POST["oldValue"].".data");
} else {
	if(@$_POST['type'] == "Fog") {
		$json[$_POST['zone']][$_POST['type']]["OptionFogEnabled"] = $_POST['OptionFogEnabled'];
		$json[$_POST['zone']][$_POST['type']]["ModeFog"] = $_POST['ModeFog'];
		$json[$_POST['zone']][$_POST['type']]["StartFog"] = $_POST['StartFog'];
		$json[$_POST['zone']][$_POST['type']]["EndFog"] = $_POST['EndFog'];
		$json[$_POST['zone']][$_POST['type']]["ColorFog"] = $_POST['ColorFog'];
		$json[$_POST['zone']][$_POST['type']]["DensityFog"] = $_POST['DensityFog'];
	} else if($element) {
		$json[$_POST['zone']][$_POST['type']][$element] = $value;
	} else {
		$json[$_POST['zone']][$_POST['type']] = $value;
	}
}
$save = json_encode($json, $JSON_OPTION);
@chmod($rootFile, 0777);
file_put_contents($rootFile, $save);
?>