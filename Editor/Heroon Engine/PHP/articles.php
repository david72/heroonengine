<?php error_reporting(0);
session_start();
if(file_get_contents("../mode.dat") == "dev") { $JSON_OPTION = JSON_PRETTY_PRINT; }
else { $JSON_OPTION = null; }
$projet = file_get_contents('../Data Project/selected.dat');
$file = "../_Projects/".$projet."/game data/actors/articles.json";
@chmod($file, 0777);
$json = json_decode(file_get_contents($file), true);
if(@$_POST['articleSelect']) {
	$articleSelect = $_POST['articleSelect'];
	$idArticle = 0;
	$idActor = 0;
	for($i = 0; $i < count($json["articles"]); $i++) {
		if($json["articles"][$i]["name_article"] == $articleSelect) {
			$idArticle = $i;
			break;
		}
	}
}
switch(@$_POST['type']) {
	case "listeArticle":
		$liste = "";
		for($i = 0; $i < count($json["articles"]); $i++) {
			$liste .= $json["articles"][$idArticle]["used_actors"][$i]["actor_name"].";";
		}
		echo $liste;
	break;
	case "addArticle":
		$jsonAdd = array("name_article" => "",
						"meshes" => "",
						"attach_bone" => "",
						"position" => "",
						"rotation" => "",
						"echelle" => "",
						"particles" => "",
						"particles_position" => "",
						"material" => "",
						"material_position" => "",
						"material_echelle" => "",
						"lumiere" => "",
						"lumiere_intensity" => "",
						"lumiere_position" => "",
						"lumiere_colorDiffuse" => "",
						"lumiere_colorSpecular" => "",
						"lumiere_rayon" => "",
						"used_actors" => array("actor_name"=> ""));
		$json = array_push($json, $jsonAdd);
	break;
	case "editeArticle":
		$json["articles"][$idArticle]["meshes"] = $_POST['value'];
	break;
	case "delArticle":
		unset($json["articles"][$idArticle]);
	break;
	case "addActor":
		$json["articles"][$idArticle]['used_actors'][count($json[$idArticle]['used_actors'])] = array("actor_name"=> $_POST['value']);
	break;
	case "addMesh":
		$json["articles"][$idArticle]['meshes'] = $_POST['value'];
	break;
	case "delActor":
		unset($json["articles"][$idArticle]['used_actors']);
		$liste = explode(";", $_POST['value']);
		foreach($liste as $value) {
			if($value != "") {
				$json["articles"][$idArticle]['used_actors'] = $value;
			}
		}
	break;
	case "position":
		$json["articles"][$idArticle]["position"] = $_POST['value'];
	break;
	case "rotation":
		$json["articles"][$idArticle]["rotation"] = $_POST['value'];
	break;
	case "echelle":
		$json["articles"][$idArticle]["echelle"] = $_POST['value'];
	break;
	case "AttacheOs":
		$json["articles"][$idArticle]["attach_bone"] = $_POST['value'];
	break;
	case "emiter":
		$json["articles"][$idArticle]["particles"] = $_POST['value'];
	break;
	case "position_particule":
		$json["articles"][$idArticle]["particles_position"] = $_POST['value'];
	break;
	case "material":
		$json["articles"][$idArticle]["material"] = $_POST['value'];
	break;
	case "position_material":
		$json["articles"][$idArticle]["material_position"] = $_POST['value'];
	break;
	case "echelle_material":
		$json["articles"][$idArticle]["material_echelle"] = $_POST['value'];
	break;
	case "activerLumiere":
		$json["articles"][$idArticle]["lumiere"] = $_POST['value'];
	break;
	case "intensityLumiere":
		$json["articles"][$idArticle]["lumiere_intensity"] = $_POST['value'];
	break;
	case "rayonLumiere":
		$json["articles"][$idArticle]["lumiere_rayon"] = $_POST['value'];
	break;
	case "colorDif":
		$json["articles"][$idArticle]["lumiere_colorDiffuse"] = $_POST['value'];
	break;
	case "colorSpec":
		$json["articles"][$idArticle]["lumiere_colorSpecular"] = $_POST['value'];
	break;
	case "position_light":
		$json["articles"][$idArticle]["lumiere_position"] = $_POST['value'];
	break;
}
if($_POST['type'] != 'listeArticle') {
	$save = json_encode($json, $JSON_OPTION);
	@file_put_contents($file, $save);
}
?>