<?php error_reporting(0);
function foldersize($directory, $pExtension = false) {
    $size = 0;
    foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory)) as $file)
	{
        $extension = pathinfo($file, PATHINFO_EXTENSION);
		if($extension == $pExtension || $pExtension == false)
			$size += $file->getSize();
    }
    return $size;
}
function format_size($bytes, $round)
{
    $result = "0B";
	$bytes = floatval($bytes);
	if($bytes > 40964) {
		$arBytes = array(
			0 => array(
				"UNIT" => "TB",
				"VALUE" => pow(1024, 4)
			),
			1 => array(
				"UNIT" => "GB",
				"VALUE" => pow(1024, 3)
			),
			2 => array(
				"UNIT" => "MB",
				"VALUE" => pow(1024, 2)
			),
			3 => array(
				"UNIT" => "KB",
				"VALUE" => 1024
			),
			4 => array(
				"UNIT" => "B",
				"VALUE" => 1
			),
		);
		foreach($arBytes as $arItem)
		{
			if($bytes >= $arItem["VALUE"])
			{
				$result = $bytes / $arItem["VALUE"];
				if($result == true) {
					$result = str_replace(".", "," , strval(round($result, $round)))." ".$arItem["UNIT"];
				}
				break;
			}
		}
	}
    return $result;
}
function count_files($dir, $pExtension = false) {
   $num = 0;
   $dir_handle = opendir($dir);
   while($entry = readdir($dir_handle)) {
		$path = $dir.$entry;
		$extension = pathinfo($path, PATHINFO_EXTENSION);
		if($entry == '.' || $entry == '..') continue;
		if(is_dir($dir.$entry)) {
			$num += count_files($dir.$entry, $pExtension);
		} else {
			if($extension == $pExtension || $pExtension == false)
				$num++;
		}
   }
   closedir($dir_handle);
   return $num;
}
?>