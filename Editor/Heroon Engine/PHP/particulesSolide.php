<?php session_start();
error_reporting(0);
$projet_Selected = file_get_contents('../Data Project/selected.dat');
$dirname = '../_Projects/'.$projet_Selected.'/particles/';
$fichier = "3d_".$_POST['name'];
if($_POST['type'] == 'creerParticules') {
	$option = null;
	$dir = opendir($dirname);
	while($file = readdir($dir)) {
		if($file != '.' && $file != '..' && !is_dir($dirname.$file)) {
			if($fichier == $file) { $option = "failled"; break; }
			$option = "ok";
		}
	}
	closedir($dir);
	if($option == "ok") {
		chmod($dirname.$fichier.".js", 0777);
		file_put_contents($dirname.$fichier.".js", 'var _shape = "plane";
var _image = "_Projects/'.$projet_Selected.'/images/blood.png";
var _color = "#000000";
var _size = 1;
var _selectable = false;
var _collisionnable = false;
var _computeParticleRotation = false;
var _computeParticleColor = false;
var _computeParticleTexture = false;
var _computeParticleVertex = false;
var _particleRotation = true;
var _emiterRotation = true;
var _dentity = 1000;
var _durerVie = false;
var _vieParticleDuration = 5;
var _updateSpeed = 1.5;
var _gravity = -0.01;
var _directionX = 0;
var _directionY = -0.01;
var _directionZ = 0;
var _isBlocker = 0;
var _renderingGroupId = 0;
var _layerMask = 0;
var _collisionMask = 0;
var _collisionGroup = 0;
');
		echo "1";
	}
	else { echo "Un emeteur existe déjà sous ce nom!"; }
}
else if($_POST['type'] == 'copyParticules') {
	copy($dirname."3d_".$_POST['oldName'].".js", $dirname.$fichier.".js");
	@chmod($dirname.$fichier.".js", 0777);
	echo "1";
}
else if($_POST['type'] == 'saveParticules') {
	file_put_contents($dirname.$fichier.".js", 'var _shape = "'.$_POST['shape'].'";
var _image = "'.$_POST['imageParticles'].'";
var _color = "'.$_POST['color'].'";
var _size = '.$_POST['particleSize'].';
var _selectable = '.$_POST['selectableParticle'].';
var _collisionnable = '.$_POST['collisionnableParticle'].';
var _computeParticleRotation = '.$_POST['computeParticleRotation'].';
var _computeParticleColor = '.$_POST['computeParticleColor'].';
var _computeParticleTexture = '.$_POST['computeParticleTexture'].';
var _computeParticleVertex = '.$_POST['computeParticleVertex'].';
var _particleRotation = '.$_POST['particleRotation'].';
var _emiterRotation = '.$_POST['emiterRotation'].';
var _dentity = '.$_POST['particleDentity'].';
var _durerVie = '.$_POST['particleDurerVie'].';
var _vieParticleDuration = '.$_POST['vieParticleDuration'].';
var _updateSpeed = '.$_POST['particleUpdateSpeed'].';
var _gravity = '.$_POST['particleGravity'].';
var _directionX = '.$_POST['particleDirectionX'].';
var _directionY = '.$_POST['particleDirectionY'].';
var _directionZ = '.$_POST['particleDirectionZ'].';
var _isBlocker = '.$_POST['particleIsBlocker'].';
var _renderingGroupId = '.$_POST['particleRenderingGroupId'].';
var _layerMask = '.$_POST['particleLayerMask'].';
var _collisionMask = '.$_POST['particleCollisionMask'].';
var _collisionGroup = '.$_POST['particleCollisionGroup'].';
	');
}
else if($_POST['type'] == 'deleteParticules') {
	chmod($dirname.$fichier.".js", 0777);
	unlink($dirname.$fichier.".js");
}
?>