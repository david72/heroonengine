<?php session_start();
error_reporting(0);
include_once('jsmin.class.php');
$rootProject = './../_Projects/'.file_get_contents('./../Data Project/Selected.dat').'/';
$dirname = './../Scripts/'.file_get_contents('./../Data Project/Selected.dat').'/';
$dirname_game = $rootProject.'scripts/';
if($_POST['getScript']) {

	echo file_get_contents($dirname.$_POST['getScript']);

} elseif($_POST['setScript']) {
	$scriptName = $_POST['name'];
	chmod($dirname.$_POST['setScript'], 0777);
	chmod($rootProject.$_POST['setScript'], 0777);

	$code = $_POST['code'];
	$code = str_replace('.Start', '.Start_'.$scriptName.'', $code);
	$code_game = $code;

	file_put_contents($dirname.$_POST['setScript'], $_POST['code']);

	$path_parts = pathinfo($dirname_game.$_POST['setScript']);
	if($path_parts['extension'] == 'js') {
		file_put_contents($dirname_game.$_POST['setScript'], JSMin::minify($code_game));
	} else {
		file_put_contents($dirname_game.$_POST['setScript'], $code_game);
	}
}
elseif($_POST['newScript'])
{
	$scriptNew = null;
	$scriptNew_game = null;
	$mode = $_POST['mode'];
	if($mode == "javascript") {
		$extention = ".js";
		$scriptNew = "/*jshint esversion: 6 */
class nameClasse {
	
	constructor()
	{
		
	}

	Start() 
	{
		
	}

}";

$scriptNew_game = "/*jshint esversion: 6 */
class ".$_POST['name']." {
	
	constructor()
	{
		
	}

	Start_".$_POST['name']."() 
	{
		
	}

}";
	} elseif($mode == "php") {

		$extention = ".php";
		$scriptNew = "<?php
	// Code php ici
?>";
		$scriptNew_game = "<?php

?>";

	}
	chmod($dirname, 0777);
	chmod($dirname_game, 0777);
	file_put_contents($dirname.$_POST['name'].$extention, $scriptNew);
	file_put_contents($dirname_game.$_POST['name'].$extention, $scriptNew_game);
	echo $scriptNew;

} elseif($_POST['deleteScript']) {

	chmod($dirname.$_POST['deleteScript'], 0777);
	chmod($dirname_game.$_POST['deleteScript'], 0777);
	unlink($dirname.$_POST['deleteScript']);
	unlink($dirname_game.$_POST['deleteScript']);

} elseif($_POST['newNameScript']) {
	$mode = $_POST['mode'];
	if($mode == "javascript") $extention = ".js";
	elseif($mode == "php") $extention = ".php";
	chmod($dirname.$_POST['oldName'].$extention, 0777);
	chmod($dirname_game.$_POST['oldName'].$extention, 0777);
	rename($dirname.$_POST['oldName'].$extention, $dirname.$_POST['name'].$extention);
	rename($dirname_game.$_POST['oldName'].$extention, $dirname_game.$_POST['name'].$extention);
	echo file_get_contents($dirname.$_POST['name'].$extention);
}
?>