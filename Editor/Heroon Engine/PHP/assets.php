<?php error_reporting(0);
session_start();
if(file_get_contents("../mode.dat") == "dev") { $JSON_OPTION = JSON_PRETTY_PRINT; }
else { $JSON_OPTION = null; }
include_once("contenueFolder.php");
$fileJson = "../_Projects/".$_POST['nameProject']."/game data/assets.json";
if(@$_POST['addFolderCategories']) {
	$pathname = "../_Projects/".$_POST['nameProject']."/".$_POST['categoriesRange']."/".$_POST['addFolderCategories'];
	$json = json_decode(file_get_contents($fileJson), true);
	$range = explode("/", $_POST['categoriesRange']);
	if($range[0] == "meshes") {
		$json[0]["children"][count($json["children"])] = array("id"=>"child_node_".$_POST['addFolderCategories']."", "text"=>$_POST['addFolderCategories']);
	}
	if($range[0] == "textures") {
		$json[1]["children"][count($json["children"])] = array("id"=>"child_node_".$_POST['addFolderCategories']."", "text"=>$_POST['addFolderCategories']);
	}
	if($range[0] == "images") {
		$json[2]["children"][count($json["children"])] = array("id"=>"child_node_".$_POST['addFolderCategories']."", "text"=>$_POST['addFolderCategories']);
	}
	if($range[0] == "musics") {
		$json[3]["children"][count($json["children"])] = array("id"=>"child_node_".$_POST['addFolderCategories']."", "text"=>$_POST['addFolderCategories']);
	}
	if($range[0] == "sounds") {
		$json[4]["children"][count($json["children"])] = array("id"=>"child_node_".$_POST['addFolderCategories']."", "text"=>$_POST['addFolderCategories']);
	}
	if($range[0] == "videos") {
		$json[5]["children"][count($json["children"])] = array("id"=>"child_node_".$_POST['addFolderCategories']."", "text"=>$_POST['addFolderCategories']);
	}
	$save = json_encode($json, $JSON_OPTION);
	@chmod($fileJson, 0777);
	file_put_contents($fileJson, $save);
	@mkdir($pathname, 0777);
} else if(@$_POST['deleteFolderCategories']) {
	$json = json_decode(file_get_contents($fileJson), true);
	$range = explode("/", $_POST['categoriesRange']);
	if($range[0] == "meshes") {
		$key = array_search($_POST['deleteFolderCategories'], $json[0]["children"]);
		unset($json[0]["children"][$key]);
	}
	if($range[0] == "textures") {
		$key = array_search($_POST['deleteFolderCategories'], $json[1]["children"]);
		unset($json[1]["children"][$key]);
	}
	if($range[0] == "images") {
		$key = array_search($_POST['deleteFolderCategories'], $json[1]["children"]);
		unset($json[2]["children"][$key]);
	}
	if($range[0] == "musics") {
		$key = array_search($_POST['deleteFolderCategories'], $json[2]["children"]);
		unset($json[3]["children"][$key]);
	}
	if($range[0] == "sounds") {
		$key = array_search($_POST['deleteFolderCategories'], $json[3]["children"]);
		unset($json[4]["children"][$key]);
	}
	if($range[0] == "videos") {
		$key = array_search($_POST['deleteFolderCategories'], $json[4]["children"]);
		unset($json[5]["children"][$key]);
	}
	$save = json_encode($json, $JSON_OPTION);
	@chmod($fileJson, 0777);
	file_put_contents($fileJson, $save);
	$pathname = "../_Projects/".$_POST['nameProject']."/".$_POST['categoriesRange'];
	@rmdir($pathname);
} else if(@$_POST['categoriesSelect']) {
	$type = explode("/", $_POST['categoriesSelect']);
	$texture = false;
	if(@$_POST['texture']) $texture = $_POST['texture'];
	echo contenueFolder("../_Projects/".$_POST['nameProject']."/".$_POST['categoriesSelect'], $type[0], $texture);
}
?>