<?php  error_reporting(0); session_start();
header('Content-Type: image/png');
function saveImg($img, $finalDir, $nameImage) {
	$_img = str_replace('data:image/png;base64,', '', $img);
	$_img = str_replace(' ', '+', $_img);
	$data = base64_decode($_img);
	$file = $finalDir.$nameImage.'.png';
	file_put_contents($file, $data);
	$image = imagecreatefrompng($file);
	imagepng($image, $file, 9);
}
saveImg($_POST['imgbase64'], $_POST['root'], $_POST['nameImage']);
?>