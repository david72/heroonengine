<?php session_start();
error_reporting(0);
$projet_Selected = file_get_contents('../Data Project/selected.dat');
$dirname = '../_Projects/'.$projet_Selected.'/particles/';
$fichier = $_POST['name'];
if($_POST['type'] == 'creerParticules') {
	$option = null;
	$dir = opendir($dirname);
	while($file = readdir($dir)) {
		if($file != '.' && $file != '..' && !is_dir($dirname.$file)) {
			if($fichier == $file) { $option = "failled"; break; }
			$option = "ok";
		}
	}
	closedir($dir);
	if($option == "ok") {
		chmod($dirname.$fichier.".js", 0777);
		file_put_contents($dirname.$fichier.".js", 'var _mode = "one";
var _depthWrite = "true";
var _imageParticles = "";
var _colorMask = [];
	_colorMask[0] = 0.0;
	_colorMask[1] = 0.0;
	_colorMask[2] = 0.0;
	_colorMask[3] = 0.0;
var _minEmitBox = [];
	_minEmitBox[0] = -1;
	_minEmitBox[1] = 0;
	_minEmitBox[2] = 0;
var _maxEmitBox = [];
	_maxEmitBox[0] = 1;
	_maxEmitBox[1] = 0;
	_maxEmitBox[2] = 0;
var _particleColor1 = [];
	_particleColor1[0] = 0.7;
	_particleColor1[1] = 0.8;
	_particleColor1[2] = 1.0;
	_particleColor1[3] = 1.0;
var _particleColor2 = [];
	_particleColor2[0] = 0.2;
	_particleColor2[1] = 0.5;
	_particleColor2[2] = 1.0;
	_particleColor2[3] = 1.0;
var _particleColorDead = [];
	_particleColorDead[0] = 0.0;
	_particleColorDead[1] = 0.0;
	_particleColorDead[2] = 0.2;
	_particleColorDead[3] = 0.0;
var _particleSizeMin = 0.2;
var _particleSizeMax = 0.5;
var _particleDurerVieMin = 0.3;
var _particleDurerVieMax = 1.5;
var _particleDentity = 1000;
var _particleGravity = [];
	_particleGravity[0] = 0;
	_particleGravity[1] = -9.81;
	_particleGravity[2] = 0;
var _particleDirection1 = [];
	_particleDirection1[0] = -7;
	_particleDirection1[1] = 8;
	_particleDirection1[2] = 3;
var _particleDirection2 = [];
	_particleDirection2[0] = 7;
	_particleDirection2[1] = 8;
	_particleDirection2[2] = -3;
var _particleRotation = true;
var _vieParticleStop = false;
var _manualEmitCount = 0;
var _vieParticleDuration = 5;
var _particleEmitPowerMin = 1;
var _particleEmitPowerMax = 3;
var _particleUpdateSpeed = 0.005;
var _particleEffect = true;');
		echo "1";
	}
	else { echo "Un emeteur existe déjà sous ce nom!"; }
}
else if($_POST['type'] == 'copyParticules') {
	copy($dirname.$_POST['oldName'].".js", $dirname.$fichier.".js");
	@chmod($dirname.$_POST['name'].".js", 0777);
	echo "1";
}
else if($_POST['type'] == 'saveParticules') {
file_put_contents($dirname.$fichier.".js", 'var _mode = "'.$_POST['mode'].'";
var _depthWrite = "'.$_POST['depthWrite'].'";
var _imageParticles = "'.$_POST['imageParticles'].'";
var _colorMask = [];
	_colorMask[0] = '.$_POST['colorMask0'].';
	_colorMask[1] = '.$_POST['colorMask1'].';
	_colorMask[2] = '.$_POST['colorMask2'].';
	_colorMask[3] = '.$_POST['colorMask3'].';
var _minEmitBox = [];
	_minEmitBox[0] = '.$_POST['minEmitBox0'].';
	_minEmitBox[1] = '.$_POST['minEmitBox1'].';
	_minEmitBox[2] = '.$_POST['minEmitBox2'].';
var _maxEmitBox = [];
	_maxEmitBox[0] = '.$_POST['maxEmitBox0'].';
	_maxEmitBox[1] = '.$_POST['maxEmitBox1'].';
	_maxEmitBox[2] = '.$_POST['maxEmitBox2'].';
var _particleColor1 = [];
	_particleColor1[0] = '.$_POST['particleColor10'].';
	_particleColor1[1] = '.$_POST['particleColor11'].';
	_particleColor1[2] = '.$_POST['particleColor12'].';
	_particleColor1[3] = '.$_POST['particleColor13'].';
var _particleColor2 = [];
	_particleColor2[0] = '.$_POST['particleColor20'].';
	_particleColor2[1] = '.$_POST['particleColor21'].';
	_particleColor2[2] = '.$_POST['particleColor22'].';
	_particleColor2[3] = '.$_POST['particleColor23'].';
var _particleColorDead = [];
	_particleColorDead[0] = '.$_POST['particleColorDead0'].';
	_particleColorDead[1] = '.$_POST['particleColorDead1'].';
	_particleColorDead[2] = '.$_POST['particleColorDead2'].';
	_particleColorDead[3] = '.$_POST['particleColorDead3'].';
var _particleSizeMin = '.$_POST['particleSizeMin'].';
var _particleSizeMax = '.$_POST['particleSizeMax'].';
var _particleDurerVieMin = '.$_POST['particleDurerVieMin'].';
var _particleDurerVieMax = '.$_POST['particleDurerVieMax'].';
var _particleDentity = '.$_POST['particleDentity'].';
var _particleGravity = [];
	_particleGravity[0] = '.$_POST['particleGravity0'].';
	_particleGravity[1] = '.$_POST['particleGravity1'].';
	_particleGravity[2] = '.$_POST['particleGravity2'].';
var _particleDirection1 = [];
	_particleDirection1[0] = '.$_POST['particleDirection10'].';
	_particleDirection1[1] = '.$_POST['particleDirection11'].';
	_particleDirection1[2] = '.$_POST['particleDirection12'].';
var _particleDirection2 = [];
	_particleDirection2[0] = '.$_POST['particleDirection20'].';
	_particleDirection2[1] = '.$_POST['particleDirection21'].';
	_particleDirection2[2] = '.$_POST['particleDirection22'].';
var _particleRotation = '.$_POST['particleRotation'].';
var _vieParticleStop = '.$_POST['vieParticleStop'].';
var _manualEmitCount = '.$_POST['manualEmitCount'].';
var _vieParticleDuration = '.$_POST['vieParticleDuration'].';
var _particleEmitPowerMin = '.$_POST['particleEmitPowerMin'].';
var _particleEmitPowerMax = '.$_POST['particleEmitPowerMax'].';
var _particleUpdateSpeed = '.$_POST['particleUpdateSpeed'].';
var _particleEffect = '.$_POST['particleEffect'].';');
}
else if($_POST['type'] == 'deleteParticules') {	
	chmod($dirname.$fichier.".js", 0777);
	unlink($dirname.$fichier.".js");
}
?>