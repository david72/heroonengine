<?php error_reporting(0);
session_start();
if(file_get_contents("../mode.dat") == "dev") { $JSON_OPTION = JSON_PRETTY_PRINT; }
else { $JSON_OPTION = null; }
$projet_Selected = file_get_contents('../Data Project/selected.dat');
$cheminFichierJson = '../_Projects/'.$projet_Selected.'/game data/actors/competances.json';
$frame_json = file_get_contents($cheminFichierJson);
$json = json_decode($frame_json, true);
$IDAbility = 0;
if($_POST["abilitySelect"]) {
	for($i = 0; $i < count($json["competances"]); $i++) {
		if($_POST["abilitySelect"] == $json["competances"][$i]["name"]) {
			$IDAbility = $i;
			break;
		}
	}
}
else if($_POST["type"] == "new") {
	$newJson = array(
			"name" => "".$_POST["value"]."",
			"description" => "",
			"icon" => "none",
			"durer" => "5",
			"rechargement" => "10",
			"exclusivité_race" => "",
			"exclusivité_classe" => "",
			"script" => "");
	array_push($json["competances"], $newjson);
}
else if($_POST["type"] == "delete") {
	unset($json["competances"][$IDAbility]);
}
else if($_POST["type"] == "deleteActor") {
	foreach($json["competances"] as $key => $value) {
		$json["competances"][$key]["exclusivité_race"] = "";
		$json["competances"][$key]["exclusivité_classe"] = "";
	}
}
else if($_POST["type"] == "deleteScript") {
	foreach($json["competances"] as $key => $value) {
		$json["competances"][$key]["script"] = "";
	}
}
else {
	$json["competances"][$IDAbility][$_POST["type"]] = $_POST["value"];
}
$save = json_encode($json, $JSON_OPTION);
chmod($cheminFichierJson, 0777);
file_put_contents($cheminFichierJson, $save);
?>