<?php error_reporting(0);
session_start();
if(file_get_contents("../mode.dat") == "dev") { $JSON_OPTION = JSON_PRETTY_PRINT; }
else { $JSON_OPTION = null; }
$projet_Selected = file_get_contents('../Data Project/selected.dat');
$cheminFichierJson = '../_Projects/'.$projet_Selected.'/game data/items.json';
$file_json = file_get_contents($cheminFichierJson);
$json = json_decode($file_json, true);
$IDItem = 0;
if($_POST["name"]) {
	for($i = 0; $i < count($json["items"]); $i++) {
		if($_POST["name"] == $json["items"][$i]["name"]) {
			$IDItem = $i;
			break;
		}
	}
}
if($_POST['type'] == "deleteScript") {
	$script = $_POST['script'];
	for($i = 0; $i < count($json["items"]); $i++) {
		if($script == $json["items"][$i]["script"]) {
			unset($json["items"][$i]["script"]);
		}
	}
}
else if($_POST['type'] == "deleteActor") {
	$race = $_POST['race'];
	$classe = $_POST['classe'];
	for($i = 0; $i < count($json["items"]); $i++) {
		if($race == $json["items"][$i]["exclusivRace"]) {
			unset($json["items"][$i]["exclusivRace"]);
		}
		if($classe == $json["items"][$i]["exclusivClasse"]) {
			unset($json["items"][$i]["exclusivClasse"]);
		}
	}
}
else if($_POST['type'] == "new") {
	$newjson = array(
		"name" => "",
		"icone" => "None",
		"mesh" => "None",
		"type" => "None",
		"range" => "None",
		"value" => "",
		"poids" => "",
		"canBeStacked" => "false",
		"canBeDamaged" => "false",
		"quantityDamage" => "5",
		"canBeRepair" => "false",
		"coutRepair" => "100",
		"exclusivRace" => "all",
		"exclusivClasse" => "all",
		"script" => "None",
		"damageArme" => "10",
		"typeDamage" => "None",
		"meshProjectile" => "None",
		"startParticle" => "",
		"cibleParticle" => "",
		"speedProgectile" => "80",
		"chanceGoCible" => "50",
		"niveauArmure" => "4",
		"durerEffet" => "10",
		"imageItemParam" => "None",
		"listeDamage" => array()
	);
	array_push($json["items"], $newjson);
}
else if($_POST['type'] == "listeDamage") {
	if(isset($_POST['value']) == "") $value = "";
	else $value = $_POST['value'];
	$json["items"]["listeDamage"][count($json["items"]["listeDamage"])]["value"] = $value;
}
else if($_POST['type'] == "copy") {
	array_push($json["items"], $json["items"][$IDItem]);
	$json["items"][count($json["items"])]["name"] = $_POST['value'];
}
else if($_POST['type'] == "delete") {
	unset($json["items"][$IDItem]);
}
else {
	$json["items"][$IDItem][$_POST['champ']] = $_POST['value'];
}
$save = json_encode($json, $JSON_OPTION);
chmod($cheminFichierJson, 0777);
file_put_contents($cheminFichierJson, $save);
?>