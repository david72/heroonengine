<?php session_start();
error_reporting(0);
if(file_get_contents("../mode.dat") == "dev") { $JSON_OPTION = JSON_PRETTY_PRINT; }
else { $JSON_OPTION = null; }
$projet_Selected = file_get_contents('../Data Project/selected.dat');
$cheminFichierJson = '../_Projects/'.$projet_Selected.'/game data/actors/faction.json';
$file_json = file_get_contents($cheminFichierJson);
$json = json_decode($file_json, true);
$typePost = $_POST['type'];
if($typePost == 'new') {
	$keyValue = "";
	foreach($json["factions"] as $key => $val) {
		$keyValue = $key;
		break;
	}
	$json["factions"]["Nouvelle faction"] = $json["factions"][$keyValue];
	foreach($json["factions"] as $key => $val) {
		$json["factions"][$key]["Nouvelle faction"] = "0";
	}
}
else if($typePost == 'rename') {
	$json["factions"][$_POST['newNom']] = $json["factions"][$_POST['champsFaction']];
	unset($json["factions"][$_POST['champsFaction']]);
	foreach($json["factions"] as $key => $val) {
		$json["factions"][$key][$_POST['newNom']] = "0";
		unset($json["factions"][$key][$_POST['champsFaction']]);
	}
}
else if($typePost == 'delete') {
	unset($json["factions"][$_POST["champsFaction"]]);
	foreach($json["factions"] as $key => $val) {
		unset($json["factions"][$key][$_POST["champsFaction"]]);
	}
}
else if($typePost == 'change') {
	$json["factions"][$_POST['colone']][$_POST['champ']] = $_POST['value'];
}
$save = json_encode($json, $JSON_OPTION);
chmod($cheminFichierJson, 0777);
file_put_contents($cheminFichierJson, $save);
?>