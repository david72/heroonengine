<?php @error_reporting(0); @session_start();
function listeParticles($projet_name) {
	$liste = "";
	$dir = "_Projects/".$projet_name."/particles/";
	if (is_dir($dir)) {
		if ($dh = opendir($dir)) {
			while (($file = readdir($dh)) !== false) {
				if($file != '..' && $file != '.') {
					$fileOption = pathinfo($file, PATHINFO_FILENAME);
					$liste .= "<option value=\"".$fileOption."\">".$fileOption."</option>";
				}
			}
			closedir($dh);
		}
	}
	return $liste;
}
if(@$_POST['listeForProperty'])
{
	$projet_Selected = file_get_contents('../Data Project/selected.dat');
	$listes = Array();
	$dir = "../_Projects/".$projet_Selected."/particles/";
	if (is_dir($dir)) {
		if ($dh = opendir($dir)) {
			while (($file = readdir($dh)) !== false) {
				if($file != '..' && $file != '.') {
					$fileOption = pathinfo($file, PATHINFO_FILENAME);
					$listes[] = $fileOption;
				}
			}
			closedir($dh);
		}
	}
	echo json_encode($listes);
}
?>