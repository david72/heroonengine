<?php @error_reporting(0); @session_start();
if(file_get_contents("../mode.dat") == "dev") { $JSON_OPTION = JSON_PRETTY_PRINT; }
else { $JSON_OPTION = null; }
$root = $_POST['root'];
$objetName = $_POST['objetName'];
if($_POST['particules']) {
	$fichier = "particles.json";
	if(!file_exists($root.$fichier)) file_put_contents($root.$fichier, "{}");
	$json = json_decode(file_get_contents($root.$fichier), true);
	$json[$objetName]["particle_config"] = $_POST['particle_config'];
}
if($_POST['balisage']) {
	$fichier = "balisages.json";
	if(!file_exists($root.$fichier)) file_put_contents($root.$fichier, "{}");
	$json = json_decode(file_get_contents($root.$fichier), true);
	$json[$objetName]["waypointPNJ"] = $_POST['waypointPNJ'];
	$json[$objetName]["startFlag"] = $_POST['startFlag'];
	$json[$objetName]["waypointScript"] = $_POST['waypointScript'];
	$json[$objetName]["deathScript"] = $_POST['deathScript'];
	$json[$objetName]["pauseHereFor"] = $_POST['pauseHereFor'];
	$json[$objetName]["waypointDelai"] = $_POST['waypointDelai'];
	$json[$objetName]["numberToWaypoint"] = $_POST['numberToWaypoint'];
	$json[$objetName]["rayonWaypoint"] = $_POST['rayonWaypoint'];
}
if($_POST['portail']) {
	$fichier = "portails.json";
	if(!file_exists($root.$fichier)) file_put_contents($root.$fichier, "{}");
	$json = json_decode(file_get_contents($root.$fichier), true);
	$json[$objetName]["portailName"] = $_POST['portailName'];
	$json[$objetName]["portailStartToPortailEnd"] = $_POST['portailStartToPortailEnd'];
	$json[$objetName]["zoneOfPortail"] = $_POST['zoneOfPortail'];
	$json[$objetName]["soundPortail"] = $_POST['soundPortail'];
}
if($_POST['trigger']) {
	$fichier = "triggers.json";
	if(!file_exists($root.$fichier)) file_put_contents($root.$fichier, "{}");
	$json = json_decode(file_get_contents($root.$fichier), true);
	$json[$objetName]["script"] = $_POST['script'];
}
$save = json_encode($json, $JSON_OPTION);
@chmod($root.$fichier, 0777);
file_put_contents($root.$fichier, $save);
?>