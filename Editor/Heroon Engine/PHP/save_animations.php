<?php error_reporting(0);
session_start();
if(file_get_contents("../mode.dat") == "dev") { $JSON_OPTION = JSON_PRETTY_PRINT; }
else { $JSON_OPTION = null; }
$projet_Selected = file_get_contents('../Data Project/selected.dat');
$cheminFichierJson = '../_Projects/'.$projet_Selected.'/game data/actors/animations.json';
$file_json = file_get_contents($cheminFichierJson);
$json = json_decode($file_json, true);
$IDAnimation = 0;
$IDEtat = 0;
if($_POST["AnimName"]) {
	for($i = 0; $i < count($json["animations"]); $i++) {
		if($_POST["AnimName"] == $json["animations"][$i]["name"]) { $IDAnimation = $i; break; }
	}
}
if($_POST["FrameName"]) {
	for($i = 0; $i < count($json["animations"][$IDAnimation]["etat"]); $i++) {
		if($_POST["FrameName"] == $json["animations"][$IDAnimation]["etat"][$i]["frame_name"]) { $IDEtat = $i; break; }
	}
}
switch($_POST['type']) {
	case "new":
		$newjson = array(
			array("frame_name" => "repos",
				"frame_start" => "0",
				"frame_end" => "0",
				"frame_speed" => "100"
			),
			array("frame_name" => "marcher",
				"frame_start" => "0",
				"frame_end" => "100",
				"frame_speed" => "100"
			),
			array("frame_name" => "courir",
				"frame_start" => "0",
				"frame_end" => "0",
				"frame_speed" => "100"
			),
			array("frame_name" => "sauter",
				"frame_start" => "0",
				"frame_end" => "0",
				"frame_speed" => "100"
			),
			array("frame_name" => "nager",
				"frame_start" => "0",
				"frame_end" => "0",
				"frame_speed" => "100"
			),
			array("frame_name" => "ramasser",
				"frame_start" => "0",
				"frame_end" => "0",
				"frame_speed" => "100"
			),
			array("frame_name" => "attaque",
				"frame_start" => "0",
				"frame_end" => "0",
				"frame_speed" => "100"
			),
			array("frame_name" => "mourir",
				"frame_start" => "0",
				"frame_end" => "0",
				"frame_speed" => "100"
			),
			array("frame_name" => "dancer",
				"frame_start" => "0",
				"frame_end" => "0",
				"frame_speed" => "100"
			),
			array("frame_name" => "saluer",
				"frame_start" => "0",
				"frame_end" => "0",
				"frame_speed" => "100"
			),
			array("frame_name" => "applaudire",
				"frame_start" => "0",
				"frame_end" => "0",
				"frame_speed" => "100"
			)
		);
		$json["animations"][$IDAnimation]["name"] = "newAnimation";
		array_push($json["animations"][$IDAnimation]["etat"], $newjson);
	break;
	case "copy":
		array_push($json["animations"], $json["animations"][$IDAnimation]);
		$json["animations"][count($json["animations"])]["name"] = $_POST['animName'];
	break;
	case "delete":
		unset($json["animations"][$IDAnimation]);
	break;
	case "rename":
		$json["animations"][$IDAnimation]["frame_name"] = $_POST['newName'];
	break;
	case "newFrame":
		$newjson = array("frame_name" => "newFrame",
					"frame_start" => "0",
					"frame_end" => "0",
					"frame_speed" => "100"
				);
		array_push($json["animations"][$IDAnimation]["etat"], $newjson);
	break;
	case "deleteFrame":
		unset($json["animations"][$IDAnimation][$_POST['animName']][$_POST["FrameName"]]);
	break;
	case "listeFrame":
		$option = "";
		for($i = 0; $i < count($json["animations"][$IDAnimation]["etat"]); $i++) {
			$option .= $json["animations"][$IDAnimation]["etat"][$i]["frame_name"].";";
		}
		echo $option;
	break;
	case "start": case "end": case "speed":
		if($_POST['type'] == "start") $json["animations"][$IDAnimation]["etat"][$IDEtat]["frame_start"]["start"] = $_POST['keyValue'];
		else if($_POST['type'] == "end") $json["animations"][$IDAnimation]["etat"][$IDEtat]["frame_end"] = $_POST['keyValue'];
		else if($_POST['type'] == "speed") $json["animations"][$IDAnimation]["etat"][$IDEtat]["frame_speed"] = $_POST['keyValue'];
	break;
	case "Load":
		$start =  $json["animations"][$IDAnimation]["etat"][$IDEtat]["frame_start"];
		$end =  $json["animations"][$IDAnimation]["etat"][$IDEtat]["frame_end"];
		$speed = $json["animations"][$IDAnimation]["etat"][$IDEtat]["frame_speed"];
		echo $start.";".$end.";".$speed;
	break;
}
if($_POST['type'] != "Load" && $_POST['type'] != "listeFrame") {
	$save = json_encode($json, $JSON_OPTION);
	chmod($cheminFichierJson, 0777);
	file_put_contents($cheminFichierJson, $save);
}
?>