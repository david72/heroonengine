<?php @error_reporting(0); @session_start();
if(@$_POST['importMesh']) {
	$retour = array();
	$uploaddir = "../_Projects/".$_POST['nameProject']."/".$_POST['range_importMeshFile'];
	foreach ($_FILES["importMeshFile"]["error"] as $key => $error) {
		if ($error == UPLOAD_ERR_OK) {
			$tmp_name = $_FILES["importMeshFile"]["tmp_name"][$key];
			$name = $_FILES["importMeshFile"]["name"][$key];
			move_uploaded_file($tmp_name, "$uploaddir/$name");
			$retour[] = $uploaddir."/".$name;
		}
	}
	echo json_encode($retour);
}
if(@$_POST['importScene']) {
	$retour = array();
	$uploaddir = "../_Projects/".$_POST['nameProject']."/scenes/completes/";
	foreach ($_FILES["importSceneFile"]["error"] as $key => $error) {
		if ($error == UPLOAD_ERR_OK) {
			$tmp_name = $_FILES["importSceneFile"]["tmp_name"][$key];
			$name = $_FILES["importSceneFile"]["name"][$key];
			move_uploaded_file($tmp_name, "$uploaddir/$name");
			$retour[] = $uploaddir."/".$name;
		}
	}
	echo json_encode($retour);
}
if(@$_POST['importImage']) {
	$retour = array();
	$uploaddir = "../_Projects/".$_POST['nameProject']."/".$_POST['range_importImageFile'];
	foreach ($_FILES["importImageFile"]["error"] as $key => $error) {
		if ($error == UPLOAD_ERR_OK) {
			$tmp_name = $_FILES["importImageFile"]["tmp_name"][$key];
			$name = $_FILES["importImageFile"]["name"][$key];
			move_uploaded_file($tmp_name, "$uploaddir/$name");
			$retour[] = $uploaddir."/".$name;
		}
	}
	echo json_encode($retour);
}
if(@$_POST['importMusic']) {
	$retour = array();
	$uploaddir = "../_Projects/".$_POST['nameProject']."/".$_POST['range_importMusicFile'];
	foreach ($_FILES["importMusicFile"]["error"] as $key => $error) {
		if ($error == UPLOAD_ERR_OK) {
			$tmp_name = $_FILES["importMusicFile"]["tmp_name"][$key];
			$name = $_FILES["importMusicFile"]["name"][$key];
			move_uploaded_file($tmp_name, "$uploaddir/$name");
			$retour[] = $uploaddir."/".$name;
		}
	}
	echo json_encode($retour);
}
if(@$_POST['importSound']) {
	$retour = array();
	$uploaddir = "../_Projects/".$_POST['nameProject']."/".$_POST['range_importSoundFile'];
	foreach ($_FILES["importSoundFile"]["error"] as $key => $error) {
		if ($error == UPLOAD_ERR_OK) {
			$tmp_name = $_FILES["importSoundFile"]["tmp_name"][$key];
			$name = $_FILES["importSoundFile"]["name"][$key];
			move_uploaded_file($tmp_name, "$uploaddir/$name");
			$retour[] = $uploaddir."/".$name;
		}
	}
	echo json_encode($retour);
}
if(@$_POST['importVideo']) {
	$retour = array();
	$uploaddir = "../_Projects/".$_POST['nameProject']."/".$_POST['range_importVideoFile'];
	foreach ($_FILES["importVideoFile"]["error"] as $key => $error) {
		if ($error == UPLOAD_ERR_OK) {
			$tmp_name = $_FILES["importVideoFile"]["tmp_name"][$key];
			$name = $_FILES["importVideoFile"]["name"][$key];
			move_uploaded_file($tmp_name, "$uploaddir/$name");
			$retour[] = $uploaddir."/".$name;
		}
	}
	echo json_encode($retour);
}
if(@$_POST['importZone']) {
	$retour = array();
	$uploaddir = "../_Projects/".$_POST['nameProject']."/scenes/";
	foreach ($_FILES["importZoneFile"]["error"] as $key => $error) {
		if ($error == UPLOAD_ERR_OK) {
			$tmp_name = $_FILES["importZoneFile"]["tmp_name"][$key];
			$name = $_FILES["importZoneFile"]["name"][$key];
			move_uploaded_file($tmp_name, "$uploaddir/$name");
			$retour[] = $uploaddir."/".$name;
		}
	}
	echo json_encode($retour);
}
?>