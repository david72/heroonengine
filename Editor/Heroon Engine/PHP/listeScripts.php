<?php @error_reporting(0); @session_start();
function listeScripts($projet_name) {
	$liste = "";
	$dir = "_Projects/".$projet_name."/scripts/";
	if (is_dir($dir)) {
		if ($dh = opendir($dir)) {
			while (($file = readdir($dh)) !== false) {
				if($file != '..' && $file != '.') {
					$fileOption = pathinfo($file, PATHINFO_EXTENSION);
					if($fileOption == "js") {
						$liste .= "<option value=\"".$file."\">".$file."</option>";
					}
				}
			}
			closedir($dh);
		}
	}
	return $liste;
}
?>