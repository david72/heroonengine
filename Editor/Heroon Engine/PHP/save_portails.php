<?php error_reporting(0);
session_start();
if(file_get_contents("../mode.dat") == "dev") { $JSON_OPTION = JSON_PRETTY_PRINT; }
else { $JSON_OPTION = null; }
$cheminFichierJson = '../Data Project/materials.json';
$file_json = file_get_contents($cheminFichierJson);
$json = json_decode($file_json, true);
$nameMaterial = $_POST['nameMaterial'];
$valueMaterial = $_POST['valueMaterial'];
$json[$nameMaterial] = $valueMaterial;
$save = json_encode($json, $JSON_OPTION);
chmod($cheminFichierJson, 0777);
file_put_contents($cheminFichierJson, $save);
?>