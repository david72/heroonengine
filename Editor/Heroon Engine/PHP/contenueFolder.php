<?php error_reporting(0);
session_start();
function contenueFolder($dir, $type, $type2) {
	$fichier = '';
	if($type == "meshes" && $type2 == false){
		$preg_match = '#\.(babylon|incremental|binary)$#i';
	}
	if($type == "meshes" && $type2 == true){
		$preg_match = '#\.(jpe?g|gif|png|tga|bmp|dds)$#i';
	}
	if($type == "images" || $type == "textures") {
		$preg_match = '#\.(jpe?g|gif|png)$#i';
	}
	if($type == "musics") {
		$preg_match = '#\.(mp3|ogg)$#i';
	}
	if($type == "sounds") {
		$preg_match = '#\.(mp3|ogg)$#i';
	}
	if($type == "videos") {
		$preg_match = '#\.(ogv|mp4|webm)$#i';
	}
	if (is_dir($dir)) {
		if ($dh = opendir($dir)) {
		    while (($file = readdir($dh)) !== false)
			{
				if($file != "." && $file != ".." && preg_match($preg_match, $file)) {
					$fichier .= $file.";";
				}
			}
			closedir($dh);
		}
		echo $fichier;
	}
}
?>