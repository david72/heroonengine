<?php error_reporting(0);
session_start();
if(file_get_contents("../mode.dat") == "dev") { $JSON_OPTION = JSON_PRETTY_PRINT; }
else { $JSON_OPTION = null; }
$projet_Selected = file_get_contents('../Data Project/selected.dat');
$chemin = '../_Projects/'.$projet_Selected.'/game data/actors/';
if($_POST['actor']) {
	$data = explode(".", $_POST['actor']);
	$file = "../".$_POST['root'].$data[0].".".$data[1].".json";
}
else if($_POST['race'] && $_POST['classe']) {
	$file = "../".$_POST['root'].$_POST['race'].".".$_POST['classe'].".json";
}
@chmod($file, 0777);
$json = json_decode(file_get_contents($file), true);
if($_POST['sessionActor']) {
	$_SESSION['actor'] = $_POST['value'];
}
else if($_POST['type'] == "new") {
	file_put_contents("../".$_POST['root'].$_POST['race'].".".$_POST['classe'].".json", '{
    "actor_type": "",
    "sex": "",
    "zone_start": "",
    "portail_start": "",
    "comportement": "",
	"range": "5",
    "faction": "",
    "XP": "",
    "arme_defaut": "",
    "commerce_mode": "",
    "animation_type": "",
	"sound": "None",
    "apparence": {
        "mesh_body": "",
		"armure_defaut": "",
        "hair": {

		},
		"beard": {

		},
        "texture_face": {

        },
        "texture_tenu": {

		}
    },
    "description": "",
    "aptitude": {
        "0": {
            "name": "Health",
            "value": "100"
        },
        "1": {
            "name": "Nager",
            "value": "100"
        }
    },
    "resistance": {
        "0": {
            "name": "Eau",
            "value": "100"
        }
    }
}');
}
else if($_POST['type'] == "mesh_body") {
	$json['apparence']["mesh_body"] = $_POST['value'];

	print_r($json);

}
else if($_POST['type'] == "head") {
	$nbr = count($json['apparence']["texture_face"]);
	$json['apparence']["texture_face"][$nbr][$_POST['materialType']] = $_POST['value'];
}
else if($_POST['type'] == "wear") {
	$nbr = count($json['apparence']["texture_tenu"]);
	$json['apparence']["texture_tenu"][$nbr][$_POST['materialType']] = $_POST['value'];
}
else if($_POST['type'] == "hair") {
	$nbr = count($json['apparence']["hair"]);
	$json['apparence']["hair"][$nbr]["mesh"] = $_POST['value'];
}
else if($_POST['type'] == "beard") {
	$nbr = count($json['apparence']["beard"]);
	$json['apparence']["beard"][$nbr]["mesh"] = $_POST['value'];
}
else if($_POST['type'] == "copy") {
	@copy("../".$_POST['root'].$_POST['race'].".".$_POST['classe'].".json", "../".$_POST['root'].$_POST['race']."-copy.".$_POST['classe']."-copy.json");
}
else if($_POST['type'] == "delete") {
	@unlink("../".$_POST['root'].$_POST['race'].".".$_POST['classe'].".json");
}
else if($_POST['type'] == "deleteFaction") {
	$MyDirectory = opendir($chemin);
	while($Entry = @readdir($MyDirectory)) {
		if($Entry != "animations.json" && $Entry != "articles.json" && $Entry != "competances.json" && $Entry != "faction.json" && $Entry != "general.json" && $Entry != '.' && $Entry != '..') {
			$json = json_decode(file_get_contents($chemin.$Entry), true);
			if($json['faction'] == $_POST['value']) {
				$json['faction'] = "";
			}
		}
	}
	closedir($MyDirectory);
	$file = $chemin.$Entry;
}
else if($_POST['type'] == "deleteAnimation") {
	$MyDirectory = opendir($chemin);
	while($Entry = @readdir($MyDirectory)) {
		if($Entry != "animations.json" && $Entry != "articles.json" && $Entry != "competances.json" && $Entry != "faction.json" && $Entry != "general.json" && $Entry != '.' && $Entry != '..') {
			$json = json_decode(file_get_contents($chemin.$Entry), true);
			if($json['animation_type'] == $_POST['value']) {
				$json['animation_type'] = "";
			}
		}
	}
	closedir($MyDirectory);
	$file = $chemin.$Entry;
}
else if($_POST['type'] == "deleteArme") {
	$MyDirectory = opendir($chemin);
	while($Entry = @readdir($MyDirectory)) {
		if($Entry != "animations.json" && $Entry != "articles.json" && $Entry != "competances.json" && $Entry != "faction.json" && $Entry != "general.json" && $Entry != '.' && $Entry != '..') {
			$json = json_decode(file_get_contents($chemin.$Entry), true);
			if($json['arme_defaut'] == $_POST['value']) {
				$json['arme_defaut'] = "";
			}
		}
	}
	closedir($MyDirectory);
	$file = $chemin.$Entry;
}
else if($_POST['type'] == "deleteArticle") {
	$i=0;
	$MyDirectory = opendir($chemin);
	while($Entry = @readdir($MyDirectory)) {
		if($Entry != "animations.json" && $Entry != "articles.json" && $Entry != "competances.json" && $Entry != "faction.json" && $Entry != "general.json" && $Entry != '.' && $Entry != '..') {
			$json = json_decode(file_get_contents($chemin.$Entry), true);
			if($json['apparence'][$i]["armure_defaut"] == $_POST['value']) {
				$json['apparence'][$i]["armure_defaut"] = "";
			}
			$i++;
		}
	}
	closedir($MyDirectory);
	$file = $chemin.$Entry;
}
else if($_POST['type'] == "addAptitude") {
	$nbr = count($json['aptitude']);
	$json['aptitude'][$nbr]["name"] = $_POST['value'];
	$json['aptitude'][$nbr]["value"] = "100";
}
else if($_POST['type'] == "addResistance") {
	$nbr = count($json['resistance']);
	$json['resistance'][$nbr]["name"] = $_POST['value'];
	$json['resistance'][$nbr]["value"] = "100";
}
else if($_POST['type'] == "removeAptitude") {
	unset($json['aptitude'][$_POST['value']]);
}
else if($_POST['type'] == "removeResistance") {
	unset($json['resistance'][$_POST['value']]);
}
else if($_POST['type'] == "attrName") {
	$nbr = $_POST['id'];
	$json['aptitude'][$nbr]["name"] = $_POST['value'];
}
else if($_POST['type'] == "attrValue") {
	$nbr = $_POST['id'];
	$json['aptitude'][$nbr]["value"] = $_POST['value'];
}
else if($_POST['type'] == "resistName") {
	$nbr = $_POST['id'];
	$json['resistance'][$nbr]["name"] = $_POST['value'];
}
else if($_POST['type'] == "resistValue") {
	$nbr = $_POST['id'];
	$json['resistance'][$nbr]["value"] = $_POST['value'];
}
else {
	if($_POST['type'] == "classe") {
		if($_POST['race'].".".$_POST['classe'] != $_POST['root'].$_POST['race'].".".$_POST['newclasse']) {
			@rename("../".$_POST['root'].$_POST['race'].".".$_POST['classe'].".json", "../".$_POST['root'].$_POST['race'].".".$_POST['newclasse'].".json");
		}
	}
	else if($_POST['type'] == "race") {
		if($_POST['race'].".".$_POST['classe'] != $_POST['root'].$_POST['newrace'].".".$_POST['classe']) {
			@rename("../".$_POST['root'].$_POST['race'].".".$_POST['classe'].".json", "../".$_POST['root'].$_POST['newrace'].".".$_POST['classe'].".json");
		}
	}
	else {
		if($_POST['type'] == "armure_defaut") {
			$json['apparence']["armure_defaut"] = $_POST['value'];
		} else {
			$json[$_POST['type']] = $_POST['value'];
		}
	}
}
if($_POST['type'] != "new" && $_POST['type'] != "copy" && $_POST['type'] != "delete") {
	$save = json_encode($json, $JSON_OPTION);
	@file_put_contents($file, $save);
}
?>