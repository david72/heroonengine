<?php @error_reporting(0); @session_start();
include_once("../../LicenceInstall/class.code.lib.php");
if(file_get_contents("../mode.dat") == "dev") { $JSON_OPTION = JSON_PRETTY_PRINT; }
else { $JSON_OPTION = null; }
$projet_Selected = file_get_contents('../Data Project/selected.dat');
$type = $_POST['type'];
$element = $_POST['element'];
$root = "../".$_POST['root'];
$fichier = $_POST['file'].".json";
$value = stripslashes($_POST['value']);
if(!file_exists($root.$fichier)) {
	file_put_contents($root.$fichier, "");
}
$json = @json_decode(file_get_contents($root.$fichier), true);
if($type != "null" && $element != "null") {
	if($type == "serveur" && $element == "port") {
		$fichier = "../_Projects/".$projet_Selected."/Serveur/serveur.js";
		if(file_exists($fichier)) {
			$contenu = file_get_contents($fichier);
			$contenuMod = str_replace($_POST['oldValue'], $value, $contenu);
			file_put_contents($fichier, $contenuMod);
		}
	}
	if($element == "lang") {
		$_SESSION['HE-Lang'] = $value;
	}	
	$json[$type][$element] = $value;
}
else if($element != "null") {
	$json[$element] = $value;
}
/*
$crypt = new encrypt();		
$crypt->init('HOMEKEY');
if($crypt->is_encoded("../Data Project/GenerateSDK/SDK.min.js")) {
	$editor = "free";
} else {
	$editor = "comlete";
}
$json['update']['licence'] = $editor;
*/
$save = json_encode($json, $JSON_OPTION);
@chmod($root.$fichier, 0777);
file_put_contents($root.$fichier, $save);
?>