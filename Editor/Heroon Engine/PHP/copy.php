<?php session_start();
error_reporting(0);
require_once("foldersize.php");
function copy_dir($dir2copy, $dir_paste) {
	if (is_dir($dir2copy)) {
		if ($dh = opendir($dir2copy)) {
			while (($file = readdir($dh)) !== false) {
				if (!is_dir($dir_paste)) mkdir ($dir_paste, 0777);
				if(is_dir($dir2copy.$file) && $file != '..'  && $file != '.') copy_dir ( $dir2copy.$file.'/' , $dir_paste.$file.'/' );
				elseif($file != '..'  && $file != '.') copy ( $dir2copy.$file , $dir_paste.$file );
			}
			closedir($dh);
		}
	}
}
if($_POST['type'] && $_POST['projetSelected']) {
	$root = "../_Projects/".$_POST['projetSelected']."/";
	$nameProject = $_POST['projetSelected']."-copy";
}
else if($_POST['type'] && $_POST['nameProject']) { /* on créer un nouveau projet, on copy le projet par defaut */
	$root = "../Data Project/Default/";
	$rootSDK = "../Data Project/SDK/";
	$nameProject = $_POST['nameProject'];
}
/* on copy un projet de l'utilisateur*/
$date = date('d/m/Y', time());
$version = file_get_contents("../Version.dat");
$total_size = foldersize($root);
$poids = format_size ($total_size, 2);
/* Copy du projet venant des projet ou du projet par defaut */
mkdir("../_Projects/Default/", 0777);
copy_dir($root, "../_Projects/Default/");
mkdir("../_Projects/Default/SDK/", 0777);
copy_dir($rootSDK, "../_Projects/Default/SDK/");
mkdir("../Scripts/".$_POST['nameProject'], 0777);
copy_dir("../Data Project/Scripts/", "../Scripts/".$_POST['nameProject']."/");
/* On renomme le projet copier. */
@rename("../_Projects/Default", "../_Projects/".$nameProject);
@rename("../_Projects/".$_POST['projetSelected'], "../_Projects/".$nameProject);
file_put_contents("../_Projects/".$nameProject."/date.dat", date('d/m/Y', time()));
echo $version.";".$date.";".$poids;
?>