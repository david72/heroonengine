<?php @error_reporting(0); @session_start();
$projet_Selected = $_POST['project'];
$chemin = '../_Projects/'.$projet_Selected.'/game data/';
$file = $chemin."projet.json";
$json = json_decode(file_get_contents($file), true);
$rootUpdatePublic = $json['update']['public_folder'];
$rootUpdateBeta = $json['update']['beta_folder'];
function copy_all($source, $dest){
    if(is_dir($source)) {
        $dir_handle=opendir($source);
        while($file=readdir($dir_handle)){
            if($file!="." && $file!=".."){
                if(is_dir($source."/".$file)){
                    if(!is_dir($dest."/".$file)){
                        mkdir($dest."/".$file);
                    }
                    copy_all($source."/".$file, $dest."/".$file);
                } else {
					$dateFileDest = 0;
					$dateFileSouce = filemtime($source."/".$file);
					if(file_exists($dest."/".$file)) {
						$dateFileDest = filemtime($dest."/".$file);
					}
					if($dateFileSouce > $dateFileDest) {
						copy($source."/".$file, $dest."/".$file);
					}
                }
            }
        }
        closedir($dir_handle);
    } else {
		$dateFileDest = 0;
		$dateFileSouce = filemtime($source."/".$file);
		if(file_exists($dest."/".$file)) {
			$dateFileDest = filemtime($dest."/".$file);
		}
		if($dateFileSouce > $dateFileDest) {
			copy($source, $dest);
		}
    }
}
if(@$_POST['type'] == "updatePublic") {
	if(file_exists("../../".$rootUpdatePublic) == false) {
		@mkdir("../../".$rootUpdatePublic);
	}
	if(file_exists("../../".$rootUpdatePublic.$projet_Selected) == false) {
		@mkdir("../../".$rootUpdatePublic.$projet_Selected);
	}
	copy_all('../_Projects/'.$projet_Selected, "../../".$rootUpdatePublic.$projet_Selected);
}
else if(@$_POST['type'] == "updateBeta") {
	if(file_exists("../../".$rootUpdateBeta) == false) {
		@mkdir("../../".$rootUpdateBeta);
	}
	if(file_exists("../../".$rootUpdateBeta.$projet_Selected) == false) {
		@mkdir("../../".$rootUpdateBeta.$projet_Selected);
	}
	copy_all('../_Projects/'.$projet_Selected, "../../".$rootUpdateBeta.$projet_Selected);
}
?>