<?php error_reporting(0);
session_start();
$projet_Selected = file_get_contents('../Data Project/selected.dat');
$listes = Array();
$dir = "../_Projects/".$projet_Selected."/videos/";
if (is_dir($dir)) {
	if ($dh = opendir($dir)) {
		while (($file = readdir($dh)) !== false) {
			if($file != '..' && $file != '.') {
				$fileOption = pathinfo($file, PATHINFO_FILENAME);
				$extentionOption = pathinfo($file, PATHINFO_EXTENSION);
				if($extentionOption == $_POST['format']) {
					$listes[] = $fileOption;
				}
			}
		}
		closedir($dh);
	}
}
echo json_encode($listes);
?>