/*##################################################
 *                                items.php
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 01/01/2017
 ###################################################
 */

var itemSelected = null;
 
var selectItem = function(item) {
	itemSelected = item;
	$.getJSON("_Projects/"+projet_name+"/game data/items.json?" + (new Date()).getTime(), function(data) {		
		for(let i = 0, count = Object.keys(data.items).length; i < count; i++) {
			if(data.items[i].name == itemSelected) {
				$("#itemName").val(data.items[i].name);
				$("#icone_item").val(data.items[i].icone);		
				$("#mesh_item").val(data.items[i].mesh);				
				$("#ItemType option").filter('[value='+String(data.items[i].type)+']').attr('selected', true);
				$("#rangeSlotIntentaire option").filter('[value='+String(data.items[i].range)+']').attr('selected', true);
				$('#valueItem').spinner('value', data.items[i].value);		
				$('#poidsItem').spinner('value', data.items[i].poids);
				if(data.items[i].canBeStacked) {
					$("#itemCanBeStacked").attr('checked', true);
				}
				if(data.items[i].canBeDamaged) {
					$("#itemCanBeDamaged").attr('checked', true);
				}		
				$('#itemQuantityDamage').spinner('value', data.items[i].quantityDamage);	
				if(data.items[i].canBeRepair) {
					$("#itemCanBeRepair").attr('checked', true);
				}
				$('#itemCoutRepair').spinner('value', data.items[i].coutRepair);		
				$("#exclusivRace_items option").filter('[value='+String(data.items[i].exclusivRace)+']').attr('selected', true);		
				$("#exclusivClass_items option").filter('[value='+String(data.items[i].exclusivClasse)+']').attr('selected', true);		
				$("#script_items option").filter('[value='+String(data.items[i].script)+']').attr('selected', true);
				//Param arme		
				$('#armeDamaged').spinner('value', data.items[i].damageArme);		
				$("#typeDamage").val(data.items[i].typeDamage);		
				$("#projectileMesh").val(data.items[i].meshProjectile);	
				$("#startParticuleProjectiles option").filter('[value='+String(data.items[i].startParticle)+']').attr('selected', true);
				$("#cibleParticuleProjectile option").filter('[value='+String(data.items[i].cibleParticle)+']').attr('selected', true);	
				$('#speedProjectile').spinner('value', data.items[i].speedProgectile);		
				$('#chanceGoCible').spinner('value', data.items[i].chanceGoCible);		
				//Param armure		
				$('#niveauArmureParam').spinner('value', data.items[i].niveauArmure);		
				//Param potion		
				$('#effectDurerParam').spinner('value', data.items[i].durerEffet);		
				//Param image		
				$("#image_param_item").val(data.items[i].imageItemParam);
				switch(String(data.items[i].type)) {
					case "Arme":
						$("#ParamArme").show();
					break;
					case "Armure":
						$("#ParamArmure").show();
					break;
					case "Potion":
						$("#ParamPotion").show();
					break;			
					case "Image":
						$("#ParamImage").show();
					break;			
				}
				break;
			}			
		}			
	});	
}; 

var saveDamage = function(idSlot) {
	let valueSlot = $("#canal_damage-slot_"+idSlot).val();
	let typeDamage = $("#typeDamage").val(valueSlot);
	$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=typeDamage&value="+valueSlot});
};

var assignDamage = function(idSlot) {
	let valueSlot = $("#canal_damage-slot_"+idSlot).val();
	$(".slot_damage").css({"background": "white"});
	$("#canal_damage-slot_"+idSlot).css({"background": "#bcff8e"});
	$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=listeDamage&value="+valueSlot});
};
 
$(function() {
	$("#accordion-items").accordion( {heightStyle: "fill"});
	$("#accordion-items").accordion("option", "icons", null);
	$("#ParamArme, #ParamImage, #ParamPotion, #ParamArmure").hide();
		
	$.getJSON("_Projects/"+projet_name+"/game data/items.json?" + (new Date()).getTime(), function(data) {		
		for(let i = 0, count = Object.keys(data.items).length; i < count; i++) {
			$("#listeItems").append('<a href="javascript:void(0)" id="'+data.items[i].name+'" onClick="selectItem(\''+data.items[i].name+'\')">- '+data.items[i].name+'</a>');
		}				
		$.ajax({type: "POST", url: 'PHP/listeActor.php',
			success: function(msg) {						
				let actor = $(msg).attr("value");		
				msg = actor.split(".");
				let race = msg[0];
				let classe = msg[1];
				$("#exclusivRace_items").append('<option value="'+race+'">'+race+'</option>');	
				$("#exclusivClass_items").append('<option value="'+classe+'">'+classe+'</option>');
			}
		});	
	});	
	
	$("#newItems").click(function() {	
		jPrompt(lang.items.js.nameItem, "", lang.items.js.titleNewItems, function(result) {
			if(result) {
				$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "type=new&champ=name&value="+result,
					success: function() {
						$("#listeItems").append('<a href="javascript:void(0)" id="'+result+'" onClick="selectItem(\''+result+'\')">- '+result+'</a>');
					}
				});
			}
		});
	});
	
	$("#copyItems").click(function() {	
		jPrompt(lang.items.js.nameItem, "", lang.items.js.titleCopyItems, function(result) {
			if(result) {
				$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "type=copy&champ=name&value="+result,
					success: function() {
						$("#listeItems").append('<a href="javascript:void(0)" id="'+result+'" onClick="selectItem(\''+result+'\')">- '+result+'</a>');
					}
				});
			}
		});
	});
	
	$("#deleteItems").click(function() {		
		jComfirm(lang.items.js.comfirmDeleteItem, "Comfirmer", function(isOk) {
			if(isOk) {
				$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "type=delete&value="+itemSelected,
					success: function() {
						$("a#"+itemSelected).remove();
						$("option#"+itemSelected).remove();
						$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=deleteArme&value="+itemSelected});
						itemSelected = null;
					}
				});
			}
		});
	});
	
	$("#itemName").change(function() {	
		let nameItem = $("#itemName").val();
		$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=name&value="+nameItem});
	});
	
	$("#icone_item").change(function() {	
		let iconItem = 	$("#icone_item").val();
		$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=icone&value="+iconItem});
	});
	
	$("#mesh_item").change(function() {	
		let meshItem = $("#mesh_item").val();
		$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=mesh&value="+meshItem});
	});
	
	$("#ItemType").change(function() {	
		let selectedItemType = $("#ItemType :selected").val();
		$("#ParamArme, #ParamImage, #ParamPotion, #ParamArmure").hide();
		switch(selectedItemType) {
			case "Arme":
				$("#ParamArme").show();
			break;
			case "Armure":
				$("#ParamArmure").show();
			break;
			case "Potion":
				$("#ParamPotion").show();
			break;			
			case "Image":
				$("#ParamImage").show();
			break;			
		}
		$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=type&value="+selectedItemType});		
	});
	
	$("#rangeSlotIntentaire").change(function() {	
		let rangeInSlot = $("#rangeSlotIntentaire :selected").val();
		$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=range&value="+rangeInSlot});	
	});
	
	$("#valueItem").spinner({
		step: 1, change : function(event, ui) {
			let valueItem = $('#valueItem').spinner('value');
			$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=value&value="+valueItem});	
		}
	});
	
	$("#poidsItem").spinner({
		step: 1, change : function(event, ui) {
			let poidsItem = $('#poidsItem').spinner('value');
			$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=poids&value="+poidsItem});
		}
	});	
	
	$("#itemCanBeStacked").click(function() {	
		if($("#itemCanBeStacked").is(":checked")) {
			$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=canBeStacked&value=true"});
		} else {
			$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=canBeStacked&value=false"});
		}
	});
		
	$("#itemCanBeDamaged").click(function() {	
		if($("#itemCanBeDamaged").is(":checked")) {
			$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=canBeDamaged&value=true"});
		} else {
			$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=canBeDamaged&value=true"});
		}
	});
	
	$("#itemQuantityDamage").spinner({
		step: 1, change : function(event, ui) {
			let quantityDamage = $('#itemQuantityDamage').spinner('value');
			$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=quantityDamage&value="+quantityDamage});
		}
	});
	
	$("#itemCanBeRepair").click(function() {	
		if($("#itemCanBeRepair").is(":checked")) {
			$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=canBeRepair&value=true"});
		} else {
			$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=canBeRepair&value=true"});
		}
	});
	
	$("#itemCoutRepair").spinner({
		step: 1, change : function(event, ui) {
			let coutReparationItem = $('#itemCoutRepair').spinner('value');
			$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=coutRepair&value="+coutReparationItem});
		}
	});
	
	$("#exclusivRace_items").change(function() {	
		let exlusiveRace = $("#exclusivRace_items :selected").val();
		$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=exclusivRace&value="+exlusiveRace});
	});
	
	$("#exclusivClass_items").change(function() {	
		let exlusiveClass = $("#exclusivRace_items :selected").val();
		$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=exclusivClasse&value="+exlusiveClass});
	});
	
	$("#script_items").change(function() {	
		let scriptUsed = $("#exclusivRace_items :selected").val();
		$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=script&value="+scriptUsed});
	});
	
	//Param arme
	
	$("#armeDamaged").spinner({
		step: 1, change : function(event, ui) {
			let damageArme = $('#armeDamaged').spinner('value');
			if(damageArme) {
				$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=damageArme&value="+damageArme});
			}
		}
	});		
	
	$("#dialog-add-damage").dialog({
		modal: false,
		autoOpen: false,
		closeText: "",
		width: 230,			
		buttons: [{
			text: lang.button.js.ok, click: function() {
				$(this).dialog( "close" );
			}
		}]
	});
	
	$("#projectileMesh").change(function() {	
		let meshProjectile = $("#projectileMesh").val();
		$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=meshProjectile&value="+meshProjectile});
	});
	
	$("#startParticuleProjectiles").change(function() {	
		let startParticle = $("#startParticuleProjectiles :selected").val();
		$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=startParticle&value="+startParticle});
	});
	
	$("#cibleParticuleProjectile").change(function() {	
		let cibleParticle = $("#cibleParticuleProjectile :selected").val();
		$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=cibleParticle&value="+cibleParticle});
	});
	
	$("#speedProjectile").spinner({
		step: 0.1, change : function(event, ui) {
			let speedProgectile = $('#speedProjectile').spinner('value');
			if(speedProgectile) {
				$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=speedProgectile&value="+speedProgectile});
			}
		}
	});
	
	$("#chanceGoCible").spinner({
		step: 1, change : function(event, ui) {
			let chanceAtteindreCible = $('#chanceGoCible').spinner('value');
			if(chanceAtteindreCible) {
				$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=chanceGoCible&value="+chanceAtteindreCible});
			}
		}
	});
	
	//Param armure	
	$("#niveauArmureParam").spinner({
		step: 1, change : function(event, ui) {
			let niveauArmure = $('#niveauArmureParam').spinner('value');
			$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=niveauArmure&value="+niveauArmure});
		}
	});
	
	//Param potion	
	$("#effectDurerParam").spinner({
		step: 1, change : function(event, ui) {
			let durerEffet = $('#effectDurerParam').spinner('value');
			$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=durerEffet&value="+durerEffet});
		}
	});
	
	//Param image	
	$("#image_param_item").change(function() {	
		let imageItemParam = $("#image_param_item").val();
		$.ajax({ type: "POST", url: 'PHP/save_items.php', data: "champ=imageItemParam&value="+imageItemParam});
	});	
	
});