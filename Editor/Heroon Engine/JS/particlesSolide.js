/// <reference path="../Engines/babylon.d.ts" />
/*##################################################
 *                             particlesSolide.js
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 23/10/2017
 ###################################################
 */
class particleSolide {

	constructor()
	{
		this.camera = null;
		this.light = null;
		this.mesh = null;
		this.SPS = null;
		this.isBlocker = false;
		this.renderingGroupId = 0;
		this.layerMask = 0;
		this.collisionMask = 0;
		this.collisionGroup = 0;
		this.position = {};
		this.rotation = {};
		this.scale = {};
		this.color = {};
		this.jquery();
	}

	init()
	{
		if(globalParticleSolide.scene) {
			globalParticleSolide.engine.stopRenderLoop();
			if(this.camera) { this.camera.dispose(); }
			if(this.light) { this.light.dispose(); }
			if(this.pointLight) { this.pointLight.dispose(); }
			globalParticleSolide.scene.dispose();
		}

		globalParticleSolide.scene = new BABYLON.Scene(globalParticleSolide.engine);
		globalParticleSolide.scene.ambientColor = new BABYLON.Color3(0.33, 0.33, 0.33);
		globalParticleSolide.scene.clearColor = new BABYLON.Color3(0.39, 0.58, 0.93);

		this.camera = new BABYLON.ArcRotateCamera("CameraParticleSolid", 1.52, 1.52, 200, new BABYLON.Vector3.Zero(), globalParticleSolide.scene);
		this.camera.wheelPrecision = 2;
		globalParticleSolide.scene.activeCamera.attachControl(canvasParticleSolide);

		this.light = new BABYLON.HemisphericLight("HemiForParticleSolid", new BABYLON.Vector3(0, 1, 0), globalParticleSolide.scene);
		this.light.diffuse = new BABYLON.Color3(1, 1, 1);
		this.light.specular = new BABYLON.Color3(0, 0, 0);
		this.light.groundColor = new BABYLON.Color3(0, 0, 0);

		this.pointLight = new BABYLON.PointLight("PointLiForParticleSolid", new BABYLON.Vector3(0, 0, 0), globalParticleSolide.scene);
		this.pointLight.diffuse = new BABYLON.Color3(1, 1, 1);
		this.pointLight.specular = new BABYLON.Color3(0, 0, 0);
		this.pointLight.intensity = 0.75;

		this.setParticle();

		globalParticleSolide.engine.runRenderLoop(() =>
		{
			if(globalParticleSolide.selectionEmiter != "0") {
				divFpsParticleSolide.innerHTML = BABYLON.Tools.Format(globalParticleSolide.engine.getFps(), 0)+" FPS";
			} else {
				divFpsParticleSolide.innerHTML = "";
			}
			globalParticleSolide.scene.render();
		});

		window.addEventListener("resize", function() {
			globalParticleSolide.engine.resize();
		});
	}

	setParticle()
	{
		if(globalParticleSolide.selectionEmiter != 0) {
			$.getScript('_Projects/'+projet_name+'/particles/3d_'+globalParticleSolide.selectionEmiter +'.js?' + (new Date()).getTime(), (data) => {
				globalParticleSolide.selectionShape = _shape;
				globalParticleSolide.imageParticles = _image;
				let couleurRGB = hexToRgb(_color);
				globalParticleSolide.particleColor[0] = couleurRGB.r;
				globalParticleSolide.particleColor[1] = couleurRGB.g;
				globalParticleSolide.particleColor[2] = couleurRGB.b;
				globalParticleSolide.particleColor[3] = 1.0;
				globalParticleSolide.particleSize = _size;
				globalParticleSolide.selectableParticle = _selectable;
				globalParticleSolide.collisionnableParticle = _collisionnable;
				globalParticleSolide.computeParticleRotation = _computeParticleRotation;
				globalParticleSolide.computeParticleColor = _computeParticleColor;
				globalParticleSolide.computeParticleTexture = _computeParticleTexture;
				globalParticleSolide.computeParticleVertex = _computeParticleVertex;
				globalParticleSolide.particleRotation = _particleRotation;
				globalParticleSolide.emiterRotation = _emiterRotation;
				globalParticleSolide.particleDentity = _dentity;
				globalParticleSolide.particleDurerVie = _durerVie;
				globalParticleSolide.vieParticleDuration = _vieParticleDuration;
				globalParticleSolide.particleUpdateSpeed = _updateSpeed;
				globalParticleSolide.particleGravity = _gravity;
				globalParticleSolide.velocity = _directionX+","+_directionY+","+_directionZ;
				globalParticleSolide.isBlocker = _isBlocker;
				globalParticleSolide.renderingGroupId = _renderingGroupId;
				globalParticleSolide.layerMask = _layerMask;
				globalParticleSolide.collisionMask = _collisionMask;
				globalParticleSolide.collisionGroup = _collisionGroup;

				this.directionX = parseFloat(_directionX);
				this.directionY = parseFloat(_directionY);
				this.directionZ = parseFloat(_directionZ);
				this.image = _image;
				this.nbParticles = parseFloat(_dentity);
				this.speed = parseFloat(_updateSpeed);
				this.gravity = parseFloat(_gravity);
				this.isPickable = _selectable;
				this.particleIntersection = _collisionnable;
				this.position.x = 0;
				this.position.y = 0;
				this.position.z = 0;
				this.rotation.x = 3.14;
				this.rotation.y = 3.14;
				this.rotation.z = 3.14;
				this.scale.x = 1.0;
				this.scale.y = 1.0;
				this.scale.z = 1.0;
				this.color.r = globalParticleSolide.particleColor[0];
				this.color.g = globalParticleSolide.particleColor[1];
				this.color.b = globalParticleSolide.particleColor[2];
				this.color.a = globalParticleSolide.particleColor[3];

				$("#particleImageSolid").val(globalParticleSolide.imageParticles);
				$("#color-particleSolid").spectrum("set", "rgba("+parseFloat(globalParticleSolide.particleColor[0]*255)+", "+parseFloat(globalParticleSolide.particleColor[1]*255)+", "+parseFloat(globalParticleSolide.particleColor[2]*255)+", "+parseFloat(globalParticleSolide.particleColor[3])+")");
				$("select#rotationParticlesSolid option").filter('[value='+String(globalParticleSolide.particleRotation)+']').attr('selected', true);
				$("select#rotationEmiterParticlesSolid option").filter('[value='+String(globalParticleSolide.emiterRotation)+']').attr('selected', true);
				$('#velocitySolid').val(_directionX+","+_directionY+","+_directionZ);
				$('#graviterSolid').val(globalParticleSolide.particleGravity);
				$('#vitesseUpdateSolid').spinner('value', globalParticleSolide.particleUpdateSpeed);
				$("select#vieParticleStopSolid option").filter('[value='+String(globalParticleSolide.particleDurerVie)+']').attr('selected', true);
				$('#vieParticleDurerSolid').spinner('value', globalParticleSolide.vieParticleDuration);
				$('#densityParticleSolid').spinner('value', globalParticleSolide.particleDentity);
				$('#tailleParticleSolid').spinner('value', globalParticleSolide.particleSize);
				$("select#isBlockerParticleSolid option").filter('[value='+String(globalParticleSolide.isBlocker)+']').attr('selected', true);
				$('#renderingGroupIdSolid').spinner('value', globalParticleSolide.renderingGroupId);
				$('#layerMaskSolid').spinner('value', globalParticleSolide.layerMask);
				$('#collisionMaskSolid').spinner('value', globalParticleSolide.collisionMask);
				$('#collisionGroupSolid').spinner('value', globalParticleSolide.collisionGroup);

				if(globalParticleSolide.selectableParticle == true || globalParticleSolide.selectableParticle == "true") {
					$("#selectableParticleSolid").attr('checked', true);
				} else {
					$("#selectableParticleSolid").removeAttr('checked');
				}
				if(globalParticleSolide.collisionnableParticle == true || globalParticleSolide.collisionnableParticle == "true") {
					$("#collisionnableParticleSolid").attr('checked', true);
				} else {
					$("#collisionnableParticleSolid").removeAttr('checked');
				}
				if(globalParticleSolide.computeParticleRotation == true || globalParticleSolide.computeParticleRotation == "true") {
					$("#computeParticleRotationSolid").attr('checked', true);
				} else {
					$("#computeParticleRotationSolid").removeAttr('checked');
				}
				if(globalParticleSolide.computeParticleColor == true || globalParticleSolide.computeParticleColor == "true") {
					$("#computeParticleColorSolid").attr('checked', true);
				} else {
					$("#computeParticleColorSolid").removeAttr('checked');
				}
				if(globalParticleSolide.computeParticleTexture == true || globalParticleSolide.computeParticleTexture == "true") {
					$("#computeParticleTextureSolid").attr('checked', true);
				} else {
					$("#computeParticleTextureSolid").removeAttr('checked');
				}
				if(globalParticleSolide.computeParticleVertex == true || globalParticleSolide.computeParticleVertex == "true") {
					$("#computeParticleVertexSolid").attr('checked', true);
				} else {
					$("#computeParticleVertexSolid").removeAttr('checked');
				}

				this.createEmiteur(globalParticleSolide.scene, globalParticleSolide.selectionShape, globalParticleSolide.imageParticles);
			});
		}
	}

	createEmiteur(scene, type, image)
	{
		this.mat = new BABYLON.StandardMaterial("materialParticleSolid", scene);
		this.backFaceCulling = false;
		this.mat.diffuseTexture = new BABYLON.Texture(image, scene);

		let primitif = null;
		switch(type) {
			case "plane":
				primitif = BABYLON.Mesh.CreatePlane("plane", 1.0, scene);
			break;
			case "disc":
				primitif = BABYLON.Mesh.CreateDisc("disc", 1, 16, scene);
			break;
			case "triangle":
				primitif = BABYLON.MeshBuilder.CreateDisc("triangle", {tessellation: 3}, scene);
			break;
			case "box":
				primitif = BABYLON.Mesh.CreateBox("box", 1.0, scene);
			break;
			case "sphere":
				primitif = BABYLON.Mesh.CreateSphere("sphere", 10.0, 1.0, scene);
			break;
			case "cylindre":
				primitif = BABYLON.Mesh.CreateCylinder("cylinder", 1, 0.2, 0.2, 6, 1, scene);
			break;
			case "torus":
				primitif = BABYLON.Mesh.CreateTorus("torus", 1, 1, 10, scene);
			break;
			case "custom":
				//primitif =
			break;
		}

		this.SPS = new BABYLON.SolidParticleSystem('SolidParticleSystem', scene, {isPickable: this.isPickable, particleIntersection: this.particleIntersection, updatable: true});
		var addShape = () => {
			setTimeout(() => {
				if(primitif) {
					this.SPS.addShape(primitif, this.nbParticles);
					this.mesh = this.SPS.buildMesh();
					if(type != "custom") {
						this.mesh.material = this.mat;
					}
					this.mesh.position.y = -50;
					primitif.dispose(); // free memory
					this.createParticule(scene);
				} else {
					addShape();
				}
			}, 400);
		};
		addShape();
	}

	createParticule(scene)
	{
		let that = this;

		this.SPS.billboard = true;
		this.SPS.mesh.hasVertexAlpha = true;
		this.SPS.mesh.material.diffuseTexture.hasAlpha = true;
		this.SPS.mesh.material.useAlphaFromDiffuseTexture = true;
		this.SPS.mesh.isBlocker = globalParticleSolide.isBlocker;
		this.SPS.mesh.renderingGroupId = globalParticleSolide.renderingGroupId;
		this.SPS.mesh.layerMask = globalParticleSolide.layerMask;
		this.SPS.mesh.collisionMask = globalParticleSolide.collisionMask;
		this.SPS.mesh.collisionGroup = globalParticleSolide.collisionGroup;
		this.SPS.computeParticleRotation = globalParticleSolide.computeParticleRotation;
		this.SPS.computeParticleColor = globalParticleSolide.computeParticleColor;
		this.SPS.computeParticleTexture = globalParticleSolide.computeParticleTexture;
		this.SPS.computeParticleVertex = globalParticleSolide.computeParticleVertex;  // prevents from calling the custom updateParticleVertex() function

		// initialise les particules
		this.SPS.initParticles = function() {
			for (let p = 0; p < that.nbParticles; p++) {
				this.recycleParticle(this.particles[p]);
			}
		};

		// recycle les particules
		this.SPS.recycleParticle = function(particle) {
			particle.position.x = that.position.x;
			particle.position.y = that.position.y;
			particle.position.z = that.position.y;
			particle.scale.x = that.scale.x;
			particle.scale.y = that.scale.y;
			particle.scale.z = that.scale.z;
			particle.velocity.x = (Math.random() - 0.5) * parseFloat(that.speed);
			particle.velocity.y = Math.random() * parseFloat(that.directionY);
			particle.velocity.z = (Math.random() - 0.5) * parseFloat(that.speed);
			particle.color.r = that.color.r;
			particle.color.g = that.color.g;
			particle.color.b = that.color.b;
			particle.color.a = that.color.a;
		};

		// Animation des particules
		this.SPS.updateParticle = function(particle) {
			if (particle.position.y < 0) {
			  this.recycleParticle(particle);
			}

			particle.velocity.x += parseFloat(that.directionX / 100);
			particle.velocity.y += parseFloat(-that.gravity);
			particle.velocity.z += parseFloat(that.directionZ / 100);
			particle.position.addInPlace(particle.velocity);// update particle new position

			//particle.position.x += that.directionX;
			//particle.position.y += that.directionY;
			//particle.position.z += that.directionZ;

			if(globalParticleSolide.particleRotation == true) {
				particle.pivot.y = 0.02;				
			}
		};

		/*
		// Met a jour les vertes de chaque particules
		this.SPS.updateParticleVertex = function(particle, vertex, v) {

		};
		*/

		// Boucle de rendu des particules
		scene.registerBeforeRender(() => {
			if(this.SPS) {
				this.SPS.setParticles();
				this.pointLight.position = this.camera.position;
				if(globalParticleSolide.emiterRotation == true) {
					this.SPS.mesh.rotation.y += 0.02;
				}
			}
		});

		this.SPS.initParticles();
		this.SPS.setParticles();
	}

	jquery()
	{
		$("select#listeEmeteurSolid").change(() => {
			globalParticleSolide.selectionEmiter = $("#listeEmeteurSolid option:selected").val();
			if(globalParticleSolide.selectionEmiter !== "0") {
				this.init();
			} else {
				globalParticleSolide.selectionEmiter = "0";
				if(this.SPS) {
					this.SPS.dispose();
					this.SPS = null;
				}
			}
		});

		$("#saveParticleSolid").click(() => {
			if(globalParticleSolide.selectionEmiter !== "0") {
				let velocity = globalParticleSolide.velocity.split(",");
				$.ajax({ type: "POST", url: 'PHP/particulesSolide.php',
					data: "type=saveParticules&name="+globalParticleSolide.selectionEmiter+
					"&shape="+globalParticleSolide.selectionShape+
					"&imageParticles="+globalParticleSolide.imageParticles+
					"&color="+rgbToHex(globalParticleSolide.particleColor.r, globalParticleSolide.particleColor.g, globalParticleSolide.particleColor.b)+
					"&particleSize="+globalParticleSolide.particleSize+
					"&selectableParticle="+globalParticleSolide.selectableParticle+
					"&collisionnableParticle="+globalParticleSolide.collisionnableParticle+
					"&computeParticleRotation="+globalParticleSolide.computeParticleRotation+
					"&computeParticleColor="+globalParticleSolide.computeParticleColor+
					"&computeParticleTexture="+globalParticleSolide.computeParticleTexture+
					"&computeParticleVertex="+globalParticleSolide.computeParticleVertex+
					"&particleRotation="+globalParticleSolide.particleRotation+
					"&emiterRotation="+globalParticleSolide.emiterRotation+
					"&particleDentity="+globalParticleSolide.particleDentity+
					"&particleDurerVie="+globalParticleSolide.particleDurerVie+
					"&vieParticleDuration="+globalParticleSolide.vieParticleDuration+
					"&particleUpdateSpeed="+globalParticleSolide.particleUpdateSpeed+
					"&particleGravity="+globalParticleSolide.particleGravity+
					"&particleDirectionX="+velocity[0]+
					"&particleDirectionY="+velocity[1]+
					"&particleDirectionZ="+velocity[2]+
					"&particleIsBlocker="+globalParticleSolide.isBlocker+
					"&particleRenderingGroupId="+globalParticleSolide.renderingGroupId+
					"&particleLayerMask="+globalParticleSolide.layerMask+
					"&particleCollisionMask="+globalParticleSolide.collisionMask+
					"&particleCollisionGroup="+globalParticleSolide.collisionGroup,
					success: function(msg) {
						jAlert(lang.particle.js.saveOk, "Attention");
					}
				});
			} else {
				jAlert(lang.particle.js.alertEmeterNoSelectedForSave, "Attention");
			}
		});

		$("#delParticleSolid").click(function() {
			if(globalParticleSolide.selectionEmiter !== "0") {
				jConfirm(lang.particle.js.comfirmDeleteEmeter, "Confirmation", function(conf) {
					if(conf) {
						$.ajax({ type: "POST", url: 'PHP/particulesSolide.php', data: "type=deleteParticules&name=" +globalParticleSolide.selectionEmiter,
							success: function(msg) {
								$("#listeEmeteurSolid option[value='"+ globalParticleSolide.selectionEmiter +"']").remove();
								$("#"+ globalParticleSolide.selectionEmiter).remove();
							}
						});
					}
				});
			} else {
				jAlert(lang.particle.js.alertEmeterNoSelectedForDelete, "Attention");
			}
		});

		$("#addParticleSolid").click(() => {
			jPrompt(lang.particle.js.nameEmeteur, "", lang.particle.js.titleCreateEmeter, (result) => {
				if(result) {
					$.ajax({ type: "POST", url: 'PHP/particulesSolide.php', data: "type=creerParticules&name=" +result,
						success: (msg) => {
							if(msg == "1") {
								$("#listeEmeteur, #rainParticle, #snowParticle, #stormParticle, #emiter_article, #startParticuleProjectiles, #cibleParticuleProjectile").append('<option id="'+ result +'" value="'+ result +'" selected>'+result+'</option>');
								globalParticleSolide.selectionEmiter = result;
								if(globalParticleSolide.myEmiterParticule) {
									globalParticleSolide.myEmiterParticule.dispose();
									globalParticleSolide.selectionEmiter = "0";
									if(this.SPS) {
										this.SPS.dispose();
										this.SPS = null;
									}
								}
								this.init();
							}
						}
					});
				}
			});
		});

		$("#copyParticleSolid").click(() => {
			if(globalParticleSolide.selectionEmiter) {
				jPrompt(lang.particle.js.nameEmeteur, globalParticleSolide.selectionEmiter, lang.particle.js.titleCopyEmeter, (result) => {
					if(result) {
						$.ajax({ type: "POST", url: 'PHP/particulesSolide.php', data: "type=copyParticules&name=" +result+"&oldName"+globalParticleSolide.selectionEmiter,
							success: (msg) => {
								if(msg == "1") {
									$("#"+globalParticleSolide.selectionEmiter).text(result);
									$("#"+globalParticleSolide.selectionEmiter).val(result);
									globalParticleSolide.selectionEmiter = result;
									if(globalParticleSolide.myEmiterParticule) {
										globalParticleSolide.myEmiterParticule.dispose();
										globalParticleSolide.selectionEmiter = "0";
										if(this.SPS) {
											this.SPS.dispose();
											this.SPS = null;
										}
									}
									this.init();
								}
							}
						});
					}
				});
			} else {
				jAlert(lang.particle.js.alertEmeterNoSelectedForCopy, "Attention");
			}
		});

		$("#color-particleSolid").spectrum({
			showAlpha: true,
			hideAfterPaletteSelect: true,
			hide: (color) => {
				globalParticleSolide.particleColor[0] = color._r/255;
				globalParticleSolide.particleColor[1] = color._g/255;
				globalParticleSolide.particleColor[2] = color._b/255;
				globalParticleSolide.particleColor[3] = color._a;
				this.color.r = globalParticleSolide.particleColor[0];
				this.color.g = globalParticleSolide.particleColor[1];
				this.color.b = globalParticleSolide.particleColor[2];
				this.color.a = globalParticleSolide.particleColor[3];
			},
			move: (color) => {
				globalParticleSolide.particleColor[0] = color._r/255;
				globalParticleSolide.particleColor[1] = color._g/255;
				globalParticleSolide.particleColor[2] = color._b/255;
				globalParticleSolide.particleColor[3] = color._a;
				this.color.r = globalParticleSolide.particleColor[0];
				this.color.g = globalParticleSolide.particleColor[1];
				this.color.b = globalParticleSolide.particleColor[2];
				this.color.a = globalParticleSolide.particleColor[3];
			},
		});

		$("#shapeEmeteurSolid").change(() => {
			globalParticleSolide.selectionShape = $("#listeEmeteurSolid option:selected").val();
			this.SPS.dispose();
			this.SPS = null;
			setTimeout(() => {
				this.createEmiteur(globalParticleSolide.scene, globalParticleSolide.selectionShape, null);
			}, 100);
		});

		$("#particleImageSolid").change(() => {
			globalParticleSolide.imageParticles = $("#particleImageSolid").val();
			this.mat.diffuseTexture = new BABYLON.Texture(globalParticleSolide.imageParticles, scene);
		});

		$("select#rotationParticlesSolid").change(function() {
			let select = $("#rotationParticlesSolid option:selected").val();
			if(select == "false") {
				globalParticleSolide.particleRotation = false;
			} else {
				globalParticleSolide.particleRotation = true;
			}
		});

		$("select#rotationEmiterParticlesSolid").change(function() {
			let select = $("#rotationEmiterParticlesSolid option:selected").val();
			if(select == "false") {
				globalParticleSolide.emiterRotation = false;
			} else {
				globalParticleSolide.emiterRotation = true;
			}
		});

		$("#tailleParticleSolid").spinner({
			step: 0.1, spin: (event, ui) => {
				globalParticleSolide.particleSize = $('#tailleParticleSolid').spinner('value');
				this.scale.x = globalParticleSolide.particleSize;
				this.scale.y = globalParticleSolide.particleSize;
				this.scale.z = globalParticleSolide.particleSize;
			}, change : (event, ui) => {
				if(globalParticleSolide.particleSize) {
					globalParticleSolide.particleSize = $('#tailleParticleSolid').spinner('value');
					this.scale.x = globalParticleSolide.particleSize;
					this.scale.y = globalParticleSolide.particleSize;
					this.scale.z = globalParticleSolide.particleSize;
				}
			}
		});

		$("#selectableParticleSolid").change(() => {
			let select = $("#selectableParticleSolid").is(":checked");
			if(select == false) {
				globalParticleSolide.isPickable = false;
				this.isPickable = false;
			} else {
				globalParticleSolide.isPickable = true;
				this.isPickable = true;
			}
		});

		$("#collisionnableParticleSolid").change(() => {
			let select = $("#collisionnableParticleSolid").is(":checked");
			if(select == false) {
				globalParticleSolide.particleIntersection = false;
				this.particleIntersection = false;
			} else {
				globalParticleSolide.particleIntersection = true;
				this.particleIntersection = true;
			}
		});

		$("#computeParticleRotationSolid").change(() => {
			let select = $("#computeParticleRotationSolid").is(":checked");
			if(select == false) {
				globalParticleSolide.computeParticleRotation = false;
				this.SPS.computeParticleRotation = globalParticleSolide.computeParticleRotation;
			} else {
				globalParticleSolide.computeParticleRotation = true;
				this.SPS.computeParticleRotation = globalParticleSolide.computeParticleRotation;
			}
		});

		$("#computeParticleColorSolid").change(() => {
			let select = $("#computeParticleColorSolid").is(":checked");
			if(select == false) {
				globalParticleSolide.computeParticleColor = false;
				this.SPS.computeParticleColor = globalParticleSolide.computeParticleColor;
			} else {
				globalParticleSolide.computeParticleColor = true;
				this.SPS.computeParticleColor = globalParticleSolide.computeParticleColor;
			}
		});

		$("#computeParticleTextureSolid").change(() => {
			let select = $("#computeParticleTextureSolid").is(":checked");
			if(select == false) {
				globalParticleSolide.computeParticleTexture = false;
				this.SPS.computeParticleTexture = globalParticleSolide.computeParticleTexture;
			} else {
				globalParticleSolide.computeParticleTexture = true;
				this.SPS.computeParticleTexture = globalParticleSolide.computeParticleTexture;
			}
		});

		$("#computeParticleVertexSolid").change(() => {
			let select = $("#computeParticleVertexSolid").is(":checked");
			if(select == false) {
				globalParticleSolide.computeParticleVertex = false;
				this.SPS.computeParticleVertex = globalParticleSolide.computeParticleVertex;
			} else {
				globalParticleSolide.computeParticleVertex = true;
				this.SPS.computeParticleVertex = globalParticleSolide.computeParticleVertex;
			}
		});

		$("#densityParticleSolid").spinner({
			step: 1, spin: (event, ui) => {
				globalParticleSolide.particleDentity = $('#densityParticleSolid').spinner('value');
				this.nbParticles = globalParticleSolide.particleDentity;
			}, change : (event, ui) => {
				if(globalParticleSolide.particleDentity) {
					globalParticleSolide.particleDentity = $('#densityParticleSolid').spinner('value');
					this.nbParticles = globalParticleSolide.particleDentity;
				}
			}
		});

		$("select#vieParticleStopSolid").change(function() {
			globalParticleSolide.particleDurerVie = $("#vieParticleStopSolid option:selected").val();
			if(globalParticleSolide.particleDurerVie == "true") {
				$("#durerVie").show();
			} else {
				$("#durerVie").hide();
			}
		});

		$("#vieParticleDurerSolid").spinner({
			step: 1, spin: (event, ui) => {
				globalParticleSolide.vieParticleDuration = $('#vieParticleDurerSolid').spinner('value');

				// TODO: Ajouter une durrer de vie au particule


			}, change : (event, ui) => {
				if(globalParticleSolide.vieParticleDuration) {
					globalParticleSolide.vieParticleDuration = $('#vieParticleDurerSolid').spinner('value');

				}
			}
		});

		$("#vitesseUpdateSolid").spinner({
			step: 0.01, spin: (event, ui) => {
				globalParticleSolide.particleUpdateSpeed = parseFloat($('#vitesseUpdateSolid').spinner('value'));
				this.speed = parseFloat(globalParticleSolide.particleUpdateSpeed);
			}, change : (event, ui) => {
				if(globalParticleSolide.particleUpdateSpeed) {
					globalParticleSolide.particleUpdateSpeed = parseFloat($('#vitesseUpdateSolid').spinner('value'));
					this.speed = parseFloat(globalParticleSolide.particleUpdateSpeed);
				}
			}
		});

		$("#graviterSolid").spinner({
			step: 0.01, spin: (event, ui) => {
				globalParticleSolide.particleGravity = parseFloat($('#graviterSolid').spinner('value'));
				this.gravity = parseFloat(globalParticleSolide.particleGravity);
			}, change : (event, ui) => {
				if(globalParticleSolide.particleGravity) {
					globalParticleSolide.particleGravity = parseFloat($('#graviterSolid').spinner('value'));
					this.gravity = parseFloat(globalParticleSolide.particleGravity);
				}
			}
		});

		$("#velocitySolid").change(() => {
			globalParticleSolide.velocity = $('#velocitySolid').val();
			let directions = globalParticleSolide.velocity.split(",");
			this.directionX = parseFloat(directions[0]);
			this.directionY = parseFloat(directions[1]);
			this.directionZ = parseFloat(directions[2]);
		});

		$("#isBlockerParticleSolid").change(() => {
			globalParticleSolide.isBlocker = $('#velocitySolid').val();
			this.isBlocker = globalParticleSolide.isBlocker;
		});

		$("#renderingGroupIdSolid").spinner({
			step: 1, spin: (event, ui) => {
				globalParticleSolide.renderingGroupId = $('#renderingGroupIdSolid').spinner('value');
				this.renderingGroupId = globalParticleSolide.renderingGroupId;
			}, change : (event, ui) => {
				if(globalParticleSolide.renderingGroupId) {
					globalParticleSolide.renderingGroupId = $('#renderingGroupIdSolid').spinner('value');
					this.renderingGroupId = globalParticleSolide.renderingGroupId;
				}
			}
		});

		$("#layerMaskSolid").spinner({
			step: 1, spin: (event, ui) => {
				globalParticleSolide.layerMask = $('#layerMaskSolid').spinner('value');
				this.layerMask = globalParticleSolide.layerMask;
			}, change : (event, ui) => {
				if(globalParticleSolide.layerMask) {
					globalParticleSolide.layerMask = $('#layerMaskSolid').spinner('value');
					this.layerMask = globalParticleSolide.layerMask;
				}
			}
		});

		$("#collisionMaskSolid").spinner({
			step: 1, spin: (event, ui) => {
				globalParticleSolide.collisionMask = $('#collisionMaskSolid').spinner('value');
				this.collisionMask = globalParticleSolide.collisionMask;
			}, change : (event, ui) => {
				if(globalParticleSolide.collisionMask) {
					globalParticleSolide.collisionMask = $('#collisionMaskSolid').spinner('value');
					this.collisionMask = globalParticleSolide.collisionMask;
				}
			}
		});

		$("#collisionGroupSolid").spinner({
			step: 1, spin: (event, ui) => {
				globalParticleSolide.collisionGroup = $('#collisionGroupSolid').spinner('value');
				this.collisionGroup = globalParticleSolide.collisionGroup;
			}, change : (event, ui) => {
				if(globalParticleSolide.collisionGroup) {
					globalParticleSolide.collisionGroup = $('#collisionGroupSolid').spinner('value');
					this.collisionGroup = globalParticleSolide.collisionGroup;
				}
			}
		});
	}

}