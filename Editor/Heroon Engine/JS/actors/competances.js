/*##################################################
 *                                competances.php
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 01/01/2017
 ###################################################
 */
 
var abilitySelect = null;
var selectCompetance = function(competanceName, id) {	
	abilitySelect = competanceName;
	$.getJSON("_Projects/"+projet_name+"/game data/actors/competances.json?" + (new Date()).getTime(), function(data) {		
		let item = data.competances[id];
		$("#abilityName").val(item.name);		
		$("#abilityDescription").val(item.description);
		$("#icone").val(item.icon);
		$('#spinnerTMPDurer').spinner('value', item.durer);
		$('#spinnerTMPRecharge').spinner('value', item.rechargement);		
		$("#exclusivRace_competance").filter('[value='+String(globalParticle.exclusiviter_race)+']').attr('selected', true);
		$("#exclusivClass_competance").filter('[value='+String(globalParticle.exclusiviter_classe)+']').attr('selected', true);
		$("#script_competance").filter('[value='+String(globalParticle.script)+']').attr('selected', true);		
	});	
}; 
 
$(function() { 	

	$.getJSON("_Projects/"+projet_name+"/game data/actors/competances.json?" + (new Date()).getTime(), function(data) {			
		for(let i = 0, count = Object.keys(data.competances).length; i < count; i++) {				
			$("#listeAbiliter").append('<a href="javascript:void(0);" id="'+data.competances[i].name+'" onClick="selectCompetance(\''+data.competances[i].name+'\', '+i+');">- '+data.competances[i].name+'</a><br />');
		}
	});	
	
	$.ajax({type: "POST", url: 'PHP/listeActor.php',
		success: function(msg) {
			let actor = $(msg).attr("value");
			if(actor) {
				msg = actor.split(".");			
				let race = msg[0];
				let classe = msg[1];
				$("#exclusivRace_competance").append('<option value="'+race+'">'+race+'</option>');	
				$("#exclusivClass_competance").append('<option value="'+classe+'">'+classe+'</option>');
			}
		}
	});

	$("#newAbility").click(function() {			
		jPrompt(lang.competance.js.name, "", lang.competance.js.titleCreateCompetance, function(result) {
			if(result) {
				let i = ($("#listeAbiliter a").length - 1);	
				abilitySelect = result;
				$("#listeAbiliter").append('<a href="javascript:void(0);" id="'+result+'" onClick="selectCompetance(\''+result+'\', '+i+');">- '+result+'</a><br />');
				$('#abilityName').val("newCompetance");
				$.ajax({ type: "POST", url: 'PHP/save_competance.php', data: "type=new&value="+result});
			}
		});
	});
	
	$("#deleteAbility").click(function() {
		jComfirm(lang.competance.js.comfirmDeleteCompetance, "Comfirmer", function(isOk) {
			if(isOk) {
				let value = $('#deleteAbility').val();
				$('#'+abilitySelect).remove();
				abilitySelect = null;
				$.ajax({ type: "POST", url: 'PHP/save_competance.php', data: "type=delete&value="+value+"&abilitySelect="+abilitySelect});
			}
		});
	});
	
	$("#abilityName").change(function() {
		let value = $('#abilityName').val();
		$('#'+abilitySelect).val(value);
		abilitySelect = value;
		$.ajax({ type: "POST", url: 'PHP/save_competance.php', data: "type=name&value="+value+"&abilitySelect="+abilitySelect});
	});
	
	$("#icone").change(function() {
		let value = $('#icone').val();
		$.ajax({ type: "POST", url: 'PHP/save_competance.php', data: "type=icon&value="+value+"&abilitySelect="+abilitySelect});
	});
	
	$("#abilityDescription").change(function() {
		let value = $('#abilityDescription').val();
		$.ajax({ type: "POST", url: 'PHP/save_competance.php', data: "type=description&value="+value+"&abilitySelect="+abilitySelect});
	});
	
	$("#spinnerTMPDurer").spinner({
		min: 0, step: 1,
		spin: function( event, ui ) {
			let value = $('#spinnerTMPDurer').spinner('value');			
		},
		change: function( event, ui ) {
			let value = $('#spinnerTMPDurer').spinner('value');
			$.ajax({ type: "POST", url: 'PHP/save_competance.php', data: "type=durer&value="+value+"&abilitySelect="+abilitySelect});			
		}
	});	

	$("#spinnerTMPRecharge").spinner({
		min: 0, step: 1,
		spin: function( event, ui ) {
			let value = $('#spinnerTMPRecharge').spinner('value');			
		},
		change: function( event, ui ) {
			let value = $('#spinnerTMPRecharge').spinner('value');
			$.ajax({ type: "POST", url: 'PHP/save_competance.php', data: "type=rechargement&value="+value+"&abilitySelect="+abilitySelect});			
		}
	});	
	
	$("#exclusivRace_competance").change(function() {
		let value = $("#exclusivRace_competance :selected").val();
		$.ajax({ type: "POST", url: 'PHP/save_competance.php', data: "type=name&exclusiviter_race="+value+"&abilitySelect="+abilitySelect});
	});
	
	$("#exclusivClass_competance").change(function() {
		let value = $("#exclusivClass_competance :selected").val();
		$.ajax({ type: "POST", url: 'PHP/save_competance.php', data: "type=exclusiviter_classe&value="+value+"&abilitySelect="+abilitySelect});
	});
	
	$("#script_competance").change(function() {
		let value = $("#script_competance :selected").val();
		$.ajax({ type: "POST", url: 'PHP/save_competance.php', data: "type=script&value="+value+"&abilitySelect="+abilitySelect});
	});
	
});