/*##################################################
 *                                fastions.js
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 01/01/2017
 ###################################################
 */
var champFaction = null, ligneFaction = null, coloneFaction = null;

var newFaction = function() {
	$.ajax({ type: "POST", url: 'PHP/factions.php', data: "type=new",
		success: function (msg) {
			$("#factionActor").append('<option id="Nouvelle faction" value="Nouvelle faction">New faction</option>');
			refreshDiv("refrechFaction", "Form/Factions_refresh.php");
		}
	});
};

var deleteFaction = function() {
	jComfirm(lang.faction.js.comfirmDeleteFaction, "Comfirmer", function(isOk) {
		if(isOk) {
			$.ajax({ type: "POST", url: 'PHP/factions.php', data: "type=delete&LigneFaction="+ligneFaction+"&champsFaction="+champFaction,
				success: function (msg) {
					$('#factionName').val("");			
					$("#factionActor option[value='"+champFaction+"']").remove();			
					$.ajax({ type: "POST", url: 'PHP/save_actor.php', data: "type=deleteFaction&value="+champFaction});			
					champFaction = null;
					ligneFaction = null;
					refreshDiv("refrechFaction", "Form/Factions_refresh.php");
				}
			});
		}
	});
};

var changeFaction = function(lign, champ) {
	if(champFaction) {		
		$.ajax({ type: "POST", url: 'PHP/factions.php', data: "value="+ $('#lign'+lign+'_champs'+champ+'').val()+"&type=change&champ="+champ+"&colone="+lign});
	} else {
		$('#lign'+lign+'_champs'+champ+'').val("0");
		jAlert("Vous devez selectionner une faction!");
	}
};

var selectLigne = function(champs, ligne, nbligneTotal) {
	champFaction = champs;
	$('#factionName').val(champs);
	ligneFaction = ligne;
	for(let f= 0 ; f <= nbligneTotal; f++) {
		$("#select"+f).css("background-color", 'white');
	}
	$("#select"+ligne).css("background-color", '#bcff8e');
};

var selectColone = function(coloneName) {	
	coloneFaction = coloneName;
};

$(function() {
	$('#factionName').change(function() {
		$.ajax({ type: "POST", url: 'PHP/factions.php', data: "type=rename&champsFaction="+champFaction+"&newNom="+$('#factionName').val(),
			success: function (msg) {				
				$("#factionActor option[value='"+champFaction+"']").val($("#factionName").val()).text($("#factionName").val()).attr("id", $("#factionName").val());				
				refreshDiv("refrechFaction", "Form/Factions_refresh.php");
			}
		});
	});
});

refreshDiv("refrechFaction", "Form/Factions_refresh.php");