/*##################################################
 *                                articles.js
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 13/05/2017
 ###################################################
 */
class articlesEditor {

	constructor()
	{
		this.articleSelect = null;
		this.modeleArticle = null;
		this.planMaterial = null;
		this.lightArticle = null;
		this.actor = null;
		this.skeleton = null;

		/*
		BABYLON.Effect.ShadersStore["myEffectFragmentShader"] =
		"#ifdef GL_ES\n" +
		"precision highp float;\n" +
		"#endif\n" +

		"letying vec2 vUV;\n" +                     // Provided by babylon.js
		"letying vec4 vColor;\n" +                  // Provided by babylon.js
		"uniform sampler2D diffuseSampler;\n" +     // Provided by babylon.js
		"uniform float time;\n" +                   // This one is custom so we need to declare it to the effect

		"void main(void) {\n" +
			"vec2 position = vUV;\n" +
			"float color = 0.0;\n" +
			"vec2 center = vec2(0.5, 0.5);\n" +
			"color = sin(distance(position, center) * 10.0+ time * vColor.g);\n" +
			"vec4 baseColor = texture2D(diffuseSampler, vUV);\n" +
			"gl_FragColor = baseColor * vColor * vec4( vec3(color, color, color), 1.0 );\n" +
		"}\n" +
		"";
		*/
		this.jquery();
	}

	createScene() 
	{
		if(scene) {
			engine.stopRenderLoop();
			scene.dispose();
		}

		scene = new BABYLON.Scene(engine);
		scene.ambientColor = new BABYLON.Color3(0.33, 0.33, 0.33);
		scene.clearColor = new BABYLON.Color3(0.39, 0.58, 0.93);
		BABYLON.SceneLoader.ShowLoadingScreen = false;

		this.camera = new BABYLON.ArcRotateCamera("Camera", -1.57, 1.57, 3, BABYLON.Vector3.Zero(), scene);
		this.camera.wheelPrecision = 50;		
		this.camera.doNotSerialize = true;
		scene.activeCamera.attachControl(canvas, true);
		
		let light = new BABYLON.HemisphericLight("Hemi0", new BABYLON.Vector3(0, 1, 0), scene);
		light.diffuse = new BABYLON.Color3(1, 1, 1);
		light.specular = new BABYLON.Color3(0, 0, 0);
		light.groundColor = new BABYLON.Color3(0, 0, 0);

		engine.runRenderLoop(function() {
			scene.render();
		});
		
		//BABYLON.SIMDHelper.DisableSIMD(); //Optimisation	SIMD desactiver
		BABYLON.SIMDHelper.EnableSIMD(); //Optimisation	SIMD activer
		
		engine.resize();
		window.addEventListener("resize", function() {
			engine.resize();
		});
	}
	
	createActor(actor)
	{		
		engine.displayLoadingUI();		
		if(this.actor) this.actor.dispose();		
		$.getJSON("_Projects/"+projet_name+"/game data/actors/"+actor.value+".json?" + (new Date()).getTime(), (data) => {			
			let actorPathAll = data.apparence.mesh_body;
			let actorRoot = getPath(actorPathAll);
			let actorFile = getFile(actorPathAll);
			setTimeout(() => {
				BABYLON.SceneLoader.ImportMesh("", actorRoot, actorFile+"?" + (new Date()).getTime(), scene, (newMeshes, particle, skeletons) => {
					this.actor = newMeshes[0];				
					this.actor.position.y = -1;
					if(this.actor.skeleton) this.actor.updatePoseMatrix(BABYLON.Matrix.Identity());
					this.skeleton = skeletons[0];				
					engine.resize();
					engine.hideLoadingUI();
				});
			}, 100);
		});
	}

	createArticle(article)
	{		
		let articleRoot = getPath(article.innerHTML);
		let articleFile = getFile(article.innerHTML);		
		setTimeout(() => {
			BABYLON.SceneLoader.ImportMesh("", articleRoot, articleFile+"?" + (new Date()).getTime(), scene, (newMeshes, particle, skeletons) => {
				this.articleSelect = newMeshes[0];
				this.modeleArticle = newMeshes[0];	
				//$("#echelle").val(this.modeleArticle.scaling.x+","+this.modeleArticle.scaling.y+","+this.modeleArticle.scaling.z);
				engine.resize();
				$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=addMesh&articleSelect="+this.articleSelect+"&value="+article.innerHTML});					
			});
		}, 200);
	}

	createEmiteurParticle(selectionEmiter)
	{
		$.getScript('_Projects/' + projet_name + '/particles/' + selectionEmiter +'.js?' + (new Date()).getTime(), (data) => {
			globalParticle.mode = _mode;
			globalParticle.depthWrite = _depthWrite;
			globalParticle.imageParticles = _imageParticles;
			globalParticle.sizeEmiter = 1;
			globalParticle.colorMask[0] = _colorMask[0];
			globalParticle.colorMask[1] = _colorMask[1];
			globalParticle.colorMask[2] = _colorMask[2];
			globalParticle.colorMask[3] = _colorMask[3];
			globalParticle.minEmitBox[0] = _minEmitBox[0];
			globalParticle.minEmitBox[1] = _minEmitBox[1];
			globalParticle.minEmitBox[2] = _minEmitBox[2];
			globalParticle.maxEmitBox[0] = _maxEmitBox[0];
			globalParticle.maxEmitBox[1] = _maxEmitBox[1];
			globalParticle.maxEmitBox[2] = _maxEmitBox[2];
			globalParticle.particleColor1[0] = _particleColor1[0];
			globalParticle.particleColor1[1] = _particleColor1[1];
			globalParticle.particleColor1[2] = _particleColor1[2];
			globalParticle.particleColor1[3] = _particleColor1[3];
			globalParticle.particleColor2[0] = _particleColor2[0];
			globalParticle.particleColor2[1] = _particleColor2[1];
			globalParticle.particleColor2[2] = _particleColor2[2];
			globalParticle.particleColor2[3] = _particleColor2[3];
			globalParticle.particleColorDead[0] = _particleColorDead[0];
			globalParticle.particleColorDead[1] = _particleColorDead[1];
			globalParticle.particleColorDead[2] = _particleColorDead[2];
			globalParticle.particleColorDead[3] = _particleColorDead[3];
			globalParticle.particleSizeMin = _particleSizeMin;
			globalParticle.particleSizeMax = _particleSizeMax;
			globalParticle.particleDurerVieMin = _particleDurerVieMin;
			globalParticle.particleDurerVieMax = _particleDurerVieMax;
			globalParticle.particleDentity = _particleDentity;
			globalParticle.particleGravity[0] = _particleGravity[0];
			globalParticle.particleGravity[1] = _particleGravity[1];
			globalParticle.particleGravity[2] = _particleGravity[2];
			globalParticle.particleDirection1[0] = _particleDirection1[0];
			globalParticle.particleDirection1[1] = _particleDirection1[1];
			globalParticle.particleDirection1[2] = _particleDirection1[2];
			globalParticle.particleDirection2[0] = _particleDirection2[0];
			globalParticle.particleDirection2[1] = _particleDirection2[1];
			globalParticle.particleDirection2[2] = _particleDirection2[2];
			globalParticle.particleRotation = _particleRotation;
			globalParticle.vieParticleStop = _vieParticleStop;
			globalParticle.vieParticleDuration = _vieParticleDuration;
			globalParticle.particleEmitPowerMin = _particleEmitPowerMin;
			globalParticle.particleEmitPowerMax = _particleEmitPowerMax;
			globalParticle.particleUpdateSpeed = _particleUpdateSpeed;
			globalParticle.particleEffect = _particleEffect;

			globalParticle.myEmiterParticule = BABYLON.Mesh.CreateBox("systeme_"+selectionEmiter+"_emeteur", globalParticle.sizeEmiter, scene);
			globalParticle.myEmiterParticule.parent = this.modeleArticle;
			globalParticle.myEmiterParticule.visibility = false;
			let effect = engine.createEffectForParticles("myEffect", ["time"]);
			globalParticle.particleSystem = new BABYLON.ParticleSystem("systeme_particles_"+selectionEmiter, 4000, scene, effect);
			globalParticle.particleSystem.emitter = globalParticle.myEmiterParticule;

			globalParticle.particleSystem.particleTexture = new BABYLON.Texture("_Projects/" + projet_name + "/images/Flares/"+globalParticle.imageParticles+"", scene);
			globalParticle.particleSystem.minEmitBox = new BABYLON.Vector3(globalParticle.minEmitBox[0], globalParticle.minEmitBox[1], globalParticle.minEmitBox[2]);
			globalParticle.particleSystem.maxEmitBox = new BABYLON.Vector3(globalParticle.maxEmitBox[0], globalParticle.maxEmitBox[1], globalParticle.maxEmitBox[2]);

			// Colors of all particles
			//globalParticle.particleSystem.textureMask = new BABYLON.Color4(globalParticle.colorMask[0], globalParticle.colorMask[1], globalParticle.colorMask[2], globalParticle.colorMask[3]);
			globalParticle.particleSystem.color1 = new BABYLON.Color4(globalParticle.particleColor1[0], globalParticle.particleColor1[1], globalParticle.particleColor1[2], globalParticle.particleColor1[3]);
			globalParticle.particleSystem.color2 = new BABYLON.Color4(globalParticle.particleColor2[0], globalParticle.particleColor2[1], globalParticle.particleColor2[2], globalParticle.particleColor2[3]);
			globalParticle.particleSystem.colorDead = new BABYLON.Color4(globalParticle.particleColorDead[0], globalParticle.particleColorDead[1], globalParticle.particleColorDead[2], globalParticle.particleColorDead[3]);

			// Size of each particle (random between...
			globalParticle.particleSystem.minSize = globalParticle.particleSizeMin;
			globalParticle.particleSystem.maxSize = globalParticle.particleSizeMax;

			// Life time of each particle (random between...
			globalParticle.particleSystem.minLifeTime = globalParticle.particleDurerVieMin;
			globalParticle.particleSystem.maxLifeTime = globalParticle.particleDurerVieMax;

			if(globalParticle.depthWrite == "true") {
				globalParticle.particleSystem.forceDepthWrite = true;
			} else {
				globalParticle.particleSystem.forceDepthWrite = false;
			}

			// Emission rate
			globalParticle.particleSystem.emitRate = globalParticle.particleDentity;

			// Blend mode : BLENDMODE_ONEONE, or BLENDMODE_STANDARD
			if(globalParticle.mode == "one") globalParticle.particleSystem.blendMode = BABYLON.ParticleSystem.BLENDMODE_ONEONE;
			else { globalParticle.particleSystem.blendMode = BABYLON.ParticleSystem.BLENDMODE_STANDARD; }

			// Set the gravity of all particles
			globalParticle.particleSystem.gravity = new BABYLON.Vector3(globalParticle.particleGravity[0], globalParticle.particleGravity[1], globalParticle.particleGravity[2]);

			// Direction of each particle after it has been emitted
			globalParticle.particleSystem.direction1 = new BABYLON.Vector3(globalParticle.particleDirection1[0], globalParticle.particleDirection1[1], globalParticle.particleDirection1[2]);
			globalParticle.particleSystem.direction2 = new BABYLON.Vector3(globalParticle.particleDirection2[0], globalParticle.particleDirection2[1], globalParticle.particleDirection2[2]);

			// Angular speed, in radians
			if(globalParticle.particleRotation === true) {
			   globalParticle.particleSystem.minAngularSpeed = 0;
			   globalParticle.particleSystem.maxAngularSpeed = Math.PI;
			}

			// Speed
			globalParticle.particleSystem.minEmitPower = globalParticle.particleEmitPowerMin;
			globalParticle.particleSystem.maxEmitPower = globalParticle.particleEmitPowerMax;
			globalParticle.particleSystem.updateSpeed = globalParticle.particleUpdateSpeed;

			// Start the particle system
			if(globalParticle.vieParticleStop === true) {
				globalParticle.particleSystem.targetStopDuration = globalParticle.vieParticleDuration;
				globalParticle.particleSystem.disposeOnStop = true;
			}
			globalParticle.particleSystem.start();

			/*
			if(globalParticle.particleEffect === false) {
				globalParticle.particleSystem._customEffect = null;
				globalParticle.particleEffect = "false";
			}
			*/
			
		});
	}

	createMaterialArticle(mat)
	{
		this.planMaterial = BABYLON.Mesh.CreatePlane("plane", 1, scene);
		this.planMaterial.parent = this.modeleArticle;
		if(mat == "fire") {
			let root = "_Projects/" + projet_name + "/textures/Materials/";
			let fireMaterial = new BABYLON.FireMaterial("fireMaterial", scene);
			fireMaterial.diffuseTexture = new BABYLON.Texture(root+"diffuse.png", scene);
			fireMaterial.distortionTexture = new BABYLON.Texture(root+"distortion.png", scene);
			fireMaterial.opacityTexture = new BABYLON.Texture(root+"opacity.png", scene);
		}
		this.planMaterial.material = fireMaterial;
	}

	createLightArticle() 
	{
		this.lightArticle = new BABYLON.PointLight("Omni", BABYLON.Vector3.Zero(), scene);
		this.lightArticle.diffuse = new BABYLON.Color3(1, 0, 0);
		this.lightArticle.specular = new BABYLON.Color3(1, 1, 1);
		this.lightArticle.parent = this.modeleArticle;
	}

	jquery()
	{
		//## Liste d'article		
		$.getJSON("_Projects/"+projet_name+"/game data/actors/articles.json?" + (new Date()).getTime(), function(data) {			
			let count = Object.keys(data.articles).length;
			for(let i = 0; i < count; i++) {
				let article = data.articles[i];
				$("#templateArticles").append('<option value="'+article.name_article+'">'+article.name_article+'</option>');
			}
		});		
		
		//## Créeation d'article
		$("#templateArticles").change(() => {
			this.articleSelect = $("#templateArticles option:selected").val();
			$("#actorListeArticle").html("");
			$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=listeArticle&articleSelect="+this.articleSelect,
				success: (msg) => {
					if(msg) {
						msg = msg.split(';');
						for(let i = 0; i < msg.length - 1; i++) {
							$("#actorListeArticle").append('<option value="'+msg[i]+'">'+msg[i]+'</option>');
						}			
						$.getJSON("_Projects/"+projet_name+"/game data/actors/articles.json?" + (new Date()).getTime(), (data) => {
							let idArticle = 0;
							let count = Object.keys(data.articles).length;
							for(let i = 0; i < count; i++) {
								let article = data.articles[i];
								if(article.name_article == this.articleSelect) {
									idArticle = i;
									break;
								}
							}
							let actorListe = $("#actorListeArticle option:eq(0)").prop('selected', true).val();	
							this.createActor({value: actorListe});					
							data = data.articles[idArticle];							
							$("#addArticle3D").html(data.meshes).val(data.meshes).change();
							$("#AttacheOs").filter('[value='+String(globalParticle.attach_bone)+']').attr('selected', true);
							$("#position").val(data.position);
							$("#rotation").val(data.rotation);
							$("#echelle").val(data.echelle);
							$("#emiter_article").filter('[value='+String(globalParticle.particles)+']').attr('selected', true);
							$("#position_particule").val(data.particles_position);
							$("#material").filter('[value='+String(globalParticle.material)+']').attr('selected', true);
							$("#position_material").val(data.material_position);
							$("#echelle_material").val(data.material_echelle);
							$("#activerLumiere").filter('[value='+String(globalParticle.lumiere)+']').attr('selected', true);
							$("#color-article-diffuse").spectrum("set", data.lumiere_colorDiffuse);
							$("#color-article-spec").spectrum("set", data.lumiere_colorSpecular);
							$("#color-article-diffuse").val(data.lumiere_colorDiffuse);
							$("#color-article-spec").val(data.lumiere_colorSpecular);
							$("#intensityLumiere").spinner('value', data.lumiere_intensity);
							$("#rayonLumiere").spinner('value', data.lumiere_rayon);
							$("#position_light").val(data.lumiere_position);
							setTimeout(() => {								
								let scale = data.echelle.split(",");
								this.modeleArticle.scaling = new BABYLON.Vector3(scale[0], scale[1], scale[2]);
								let rotation = data.rotation.split(",");
								this.modeleArticle.rotation = new BABYLON.Vector3(rotation[0], rotation[1], rotation[2]);
								let position = data.position.split(",");
								this.modeleArticle.position = new BABYLON.Vector3(position[0], position[1], position[2]);
							}, 333);
						});
					}
				}
			});
		});

		$("#addArticle").click(function() {
			jPrompt(lang.article.js.addArticle, "", lang.article.js.titleAddArticle, function(addArticle) {
				if(addArticle) {
					$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=addArticle&value="+addArticle,
						success: function (msg) {
							$("#templateArticles").append('<option value="'+addArticle+'">'+addArticle+'</option>');
						}
					});
				}
			});
		});

		$("#editeArticle").click(function() {
			let oldArticleName = $("#templateArticles option:selected").val();
			jPrompt(lang.article.js.addArticle, oldArticleName, lang.article.js.titleEditeArticle, function(editeArticle) {
				if(editeArticle) {
					$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=editeArticle&value="+editeArticle+"&oldArticleName="+oldArticleName,
						success: function (msg) {
							$("#templateArticles option:selected").val(editeArticle).text(editeArticle);
						}
					});
				}
			});
		});

		$("#delArticle").click(function() {
			jComfirm(lang.article.js.comfirmDeleteArticle, "Comfirmer", function(isOk) {
				if(isOk) {
					let articleSelect = $("#templateArticles option:selected").val();
					$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=delArticle&value="+$("#templateArticles option:selected").val(),
						success: function (msg) {
							$("#templateArticles option:selected").remove("option");
							$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=deleteArticle&value="+articleSelect});
						}
					});
				}
			});
		});

		$("#dialog-form-add-acteur").dialog({ 
			autoOpen: false,
			closeText: "",
			height: 450, 
			width: 280,
			draggable: true,
			resizeable: false,
			modal: true,
			buttons: {
				"Ajouter" : function() {
					$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=addActor&value="+$("#AjouterActorListe").val() +"&articleSelect="+this.articleSelect,
						success: function (msg) {
							let selectionOption = $("#AjouterActorListe").val();
							for(let i = 0; i < selectionOption.length; i++) {
								$("#actorListeArticle").append('<option value="'+selectionOption[i]+'">'+selectionOption[i]+'</option>');
							}
							$("#dialog-form-add-acteur").dialog( "close" );
						}
					});
				},
				Cancel: function() { $("#dialog-form-add-acteur").dialog( "close" ); }
			}, close: function() { $("#dialog-form-add-acteur").dialog( "close" ); }
		});

		$("#addActor").click(function() {
			$("#dialog-form-add-acteur").dialog("open");
		});
		
		$("#typeArticle").change(() => {
			let selectionType = $("#typeArticle option:selected").val();
			if(this.modeleArticle) {				
				$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=typeArticle&articleSelect="+this.articleSelect+"&value="+selectionType});
			}
		});

		$("#delActor").click(() => {
			$("#actorListeArticle option:selected").remove();
			let listerestante = '';
			$("#actorListeArticle > option").each(function (){
				listerestante += $(this).val()+";";
			});
			$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=delActor&value="+listerestante+"&articleSelect="+this.articleSelect});
		});

		$( "#dialog-form-add-article" ).dialog({
			autoOpen: false,
			closeText: "",
			height: 380, 
			width: 550,
			draggable: true, 
			resizeable: false,
			modal: true,
			buttons: {
				Cancel: function() { $("#dialog-form-add-article").dialog( "close" ); }
			}, close: function() { $("#dialog-form-add-article").dialog( "close" ); }
		});

		//## Controle d'ajustement des articles

		$("#AttacheOs").change(() => {
			let selectionOs = $("#AttacheOs option:selected").val();
			if(this.modeleArticle) {
				if(selectionOs == "0")	 {
					this.modeleArticle.detachFromBone();
				} else {
					let boneIndex = this.skeletons.getBoneIndexByName(selectionOs);
					this.modeleArticle.attachToBone(this.skeletons.bones[boneIndex], this.actor);
				}
				$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=AttacheOs&articleSelect="+this.articleSelect+"&value="+selectionOs});
			}
		});

		$("#position").change(() => {
			let position = $('#position').val();
			position = position.split(",");
			if(this.modeleArticle) {
				this.modeleArticle.position.x = position[0];
				this.modeleArticle.position.y = position[1];
				this.modeleArticle.position.z = position[2];
				if(this.articleSelect) {
					$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=position&value="+$('#position').val()+"&articleSelect="+this.articleSelect});
				}
			}
		});

		$("#rotation").change(() => {
			let rotation = $('#rotation').val();
			rotation = rotation.split(",");
			if(this.modeleArticle) {
				this.modeleArticle.rotation.x = rotation[0];
				this.modeleArticle.rotation.y = rotation[1];
				this.modeleArticle.rotation.z = rotation[2];
				if(this.articleSelect) {
					$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=rotation&value="+$('#rotation').val()+"&articleSelect="+this.articleSelect});
				}
			}
		});

		$("#echelle").change(() => {
			let scaling = $('#echelle').val('value');
			scaling =  scalign.split(",");
			if(this.modeleArticle) {
				this.modeleArticle.scaling.x = scaling[0];
				this.modeleArticle.scaling.y = scaling[1];
				this.modeleArticle.scaling.z = scaling[2];
				if(this.articleSelect) {
					$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=echelle&value="+$('#echelle').val()+"&articleSelect="+this.articleSelect});
				}
			}
		});

		$("#emiter_article").change(() => {
			let selectionEmiter = $("#emiter_article option:selected").val();
			if(selectionEmiter != "0") { $("#panelParticule").show(); }
			else { $("#panelParticule").hide(); }
			if(this.articleSelect) {
				if(selectionEmiter != "0") {
					this.createEmiteurParticle(selectionEmiter);
				} else {
					if(globalParticle.myEmiterParticule) {
						globalParticle.myEmiterParticule.dispose();
					}
				}
				$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=emiter&articleSelect="+this.articleSelect+"&value="+selectionEmiter,
					success: function (msg) {
						isParticle = false;
					}
				});
			}
		});

		$("#position_particule").change(() => {
			let position = $('#position_particule').val();
			position = position.split(",");
			globalParticle.myEmiterParticule.position.x = position[0];
			globalParticle.myEmiterParticule.position.y = position[1];
			globalParticle.myEmiterParticule.position.z = position[2];
			if(this.articleSelect) {
				$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=position_particule&value="+$('#position_particule').val()+"&articleSelect="+this.articleSelect});
			}
		});

		$("#material").change(() => {
			let selectionMaterial = $("#material option:selected").val();
			if(selectionMaterial != "0") { $("#panelMaterial").show(); }
			else { $("#panelMaterial").hide(); }
			if(this.articleSelect) {
				if(selectionMaterial != "0") {
					this.createMaterialArticle();
				} else{
					if(this.planMaterial) {
						this.planMaterial.dispose();
					}
				}
				$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=material&articleSelect="+this.articleSelect+"&value="+selectionMaterial});
			}
		});

		$("#position_material").change(() => {
			let position = $('#position_material').val();
			position = position.split(",");
			this.planMaterial.position.x = position[0];
			this.planMaterial.position.y = position[1];
			this.planMaterial.position.z = position[2];
			if(this.articleSelect) {
				$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=position_material&value="+$('#position_material').val()+"&articleSelect="+this.articleSelect});
			}

		});

		$("#echelle_material").change(() => {
			let scaling = $('#echelle_material').val();
			scaling =  scalign.split(",");
			this.planMaterial.scaling.x = scaling[0];
			this.planMaterial.scaling.y = scaling[1];
			this.planMaterial.scaling.z = scaling[2];
			if(this.articleSelect) {
				$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=echelle_material&value="+$('#echelle_material').val()+"&articleSelect="+this.articleSelect});
			}
		});

		$("#activerLumiere").change(() => {
			let selectionLumiere = $("#activerLumiere option:selected").val();
			if(selectionLumiere == '1') $("#panelLumiere").show();
			else { $("#panelLumiere").hide(); }
			if(this.articleSelect) {
				if(selectionLumiere ==  '1') {
					this.createLightArticle();
				} else {
					if(this.lightArticle) {
						this.lightArticle.dispose();
					}
				}
				$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=activerLumiere&articleSelect="+this.articleSelect+"&value="+selectionLumiere});
			}
		});

		$("#intensityLumiere").spinner({
			step: 0.1, spin: ( event, ui ) => {
				let intensityLumiere = $('#intensityLumiere').spinner('value');
				if(this.lightArticle) this.lightArticle.intensity = intensityLumiere;
			}, change : ( event, ui ) => {
				let intensityLumiere = $('#intensityLumiere').spinner('value');
				if(this.articleSelect && this.lightArticle) {
					$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=intensityLumiere&articleSelect="+this.articleSelect+"&value="+intensityLumiere,
						success: (msg) => {
							this.lightArticle.intensity = intensityLumiere;
						}
					});
				}
			}
		});

		$("#rayonLumiere").spinner({
			step: 5, min: 15, max: 100, spin: ( event, ui ) => {
				let rayonLumiere = $('#rayonLumiere').spinner('value');
				if(this.lightArticle) this.lightArticle.range = rayonLumiere;
			}, change : ( event, ui ) => {
				let rayonLumiere = $('#rayonLumiere').spinner('value');
				if(this.articleSelect && this.lightArticle) {
					$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=rayonLumiere&articleSelect="+this.articleSelect+"&value="+rayonLumiere,
						success: (msg) => {
							this.lightArticle.range = rayonLumiere;
						}
					});
				}
			}
		});

		$("#position_light").change(() => {
			let position = $('#position_light').val();
			position = position.split(",");
			this.lightArticle.position.x = position[0];
			this.lightArticle.position.y = position[1];
			this.lightArticle.position.z = position[2];
			if(this.articleSelect) {
				$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=position_light&value="+$('#position_light').val()+"&articleSelect="+this.articleSelect});
			}
		});

		$('#color-article-diffuse').spectrum({
			showAlpha: false,
			hideAfterPaletteSelect: true,
			showInput: true,
			hide: (color) => {
				let r = color._r/100;
				let g = color._g/100;
				let b = color._b/100;
				this.lightArticle.diffuse = new BABYLON.Color3(r, g, b);
				$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=colorDif&articleName="+addArticle+"&value="+color});
			},
			move : (color) => {
				let r = color._r/100;
				let g = color._g/100;
				let b = color._b/100;
				this.lightArticle.diffuse = new BABYLON.Color3(r, g, b);
			}
		});

		$('#color-article-spec').spectrum({
			showAlpha: false,
			hideAfterPaletteSelect: true,
			showInput: true,
			hide: (color) => {
				let r = color._r/100;
				let g = color._g/100;
				let b = color._b/100;
				this.lightArticle.specular = new BABYLON.Color3(r, g, b);
				$.ajax({ type: "POST", url: 'PHP/articles.php', data: "type=colorSpec&articleName="+addArticle+"&value="+color});
			},
			move : (color) => {
				let r = color._r/100;
				let g = color._g/100;
				let b = color._b/100;
				this.lightArticle.specular = new BABYLON.Color3(r, g, b);
			}
		});
	}
}