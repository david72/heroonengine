/*##################################################
 *                                actors.js
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 13/05/2017
 ###################################################

Créer des acteurs
 */
var lang = null;
$.ajaxSetup({ async: false});	
$.getJSON('Lang/'+getLang+'.lng.json', function(data) { lang = data; });
$.ajaxSetup({ async: true});

var indexCurrentAptitude = 0,
indexCurrentResistance = 0,
root = "_Projects/"+projet_name+"/game data/actors/",
currentRaceActeur = "race",
currentClasseActeur = "classe";

class actor {

	constructor()
	{
		this.jquery();

		this.canvas = document.getElementById("HE-canvas-appercu-actor");
		this.engine = new BABYLON.Engine(this.canvas, true);
		this.engine.enableOfflineSupport = false;
		BABYLON.SceneLoader.ForceFullSceneLoadingForIncremental = true;
		BABYLON.SceneLoader.ShowLoadingScreen = false;

		this.actor = null;
		this.skeletons = null;
		this.hair = null;
		this.armure = null;
		this.materialsHead = [];
		this.materialsTenu = [];
		this.materialsHeadCurent = 0;
		this.materialsTenuCurent = 0;
	}

	selectActor(actor)
	{		
		let result = actor.split(".");
		currentRaceActeur = result[0];
		currentClasseActeur = result[1];
		$.getJSON(root+currentRaceActeur+"."+currentClasseActeur+".json?" + (new Date()).getTime(), function(data) {
			$("#raceActeur").val(currentRaceActeur);
			$("#classActeur").val(currentClasseActeur);
			$("#sexeActeur").filter('[value='+String(data.sex)+']').attr('selected', true);
			$("#ZoneStart").val(data.zone_start);
			$("#PortailName").val(data.portail_start);
			$("#AgressiviterType").filter('[value='+String(data.comportement)+']').attr('selected', true);
			$("#AgressiviterRange").val(String(data.range));
			$("#ActorSound").val(String(data.sound));
			$("#factionActor").filter('[value='+String(data.faction)+']').attr('selected', true);
			$("#XPActeur").val(data.XP);
			$("#armeDefautActor").filter('[value='+String(data.arme_defaut)+']').attr('selected', true);
			$("#PNJMode").filter('[value='+String(data.commerce_mode)+']').attr('selected', true);
			$("#AnnimationType").filter('[value='+String(data.animation_type)+']').attr('selected', true);
			$("#descriptionActeur").val(data.description);
			$("#listeAptitude, #listeResistance").html("");
			if(data.apparence.mesh_body != "") { $("#apparence-corp").html(data.apparence.mesh_body).val(data.apparence.mesh_body).change(); }
			if(data.apparence.hair[0].mesh != "") { $("#apparence-hair").html(data.apparence.hair[0].mesh).val(data.apparence.hair[0].mesh).change(); }			
			if(data.apparence.beard[0].mesh != "") { $("#apparence-beard").html(data.apparence.beard[0].mesh).val(data.apparence.beard[0].mesh).change(); }			
			if(data.apparence.texture_face[0].diffuse != "") { $("#apparence-visage").html(data.apparence.texture_face[0].diffuse).val(data.apparence.texture_face[0].diffuse).change(); }			
			if(data.apparence.texture_tenu[0].diffuse != "") { $("#apparence-tenu").html(data.apparence.texture_tenu[0].diffuse).val(data.apparence.texture_tenu[0].diffuse).change(); }			
			if(data.apparence.armure_defaut != "") { $("#apparence-armure").html(data.apparence.armure_defaut).val(data.apparence.armure_defaut).change(); }	
			for(let i = 0, countAptitude = Object.keys(data.aptitude).length; i < countAptitude; i++) {
				$("#listeAptitude").append('<li class="ui-widget-content" id="index" value="'+data.aptitude[i].name+'">'+data.aptitude[i].name+'</li>');
			}
			for(let i = 0, countResistance = Object.keys(data.resistance).length; i < countResistance; i++) {
				$("#listeResistance").append('<li class="ui-widget-content" id="index" value="'+data.resistance[i].name+'">'+data.resistance[i].name+'</li>');
			}			
			refreshDiv("actor_apparance_dialog", "Form/actor_refresh.php");			
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=sessionActor&value="+(currentRaceActeur+"."+currentClasseActeur)});
		});

		$.getJSON(root+"articles.json?" + (new Date()).getTime(), function(data) {
			for(let i = 0, count = Object.keys(data.articles).length; i < count; i++) {	
				if(data.articles[i].type == "Armure") {
					$("#listeArticles").append('<a href="javascript:void(0);" onClick="apercuActor.assigneArticle(\''+data.articles[i].name_article+'\')">- '+data.articles[i].name_article+'</a><br />');
				}
			}
		});
		
		$.getJSON("_Projects/"+projet_name+"/game data/items.json?" + (new Date()).getTime(), function(data) {
			for(let i = 0, count = Object.keys(data.items).length; i < count; i++) {
				if(data.items[i].type == "Arme") {
					$("#armeDefautActor").append('<option id="'+data.items[i].name+'" value="'+data.items[i].name+'">'+data.items[i].name+'</option>');
				}
			}
		});
	}

	scene()
	{
		this.scene = new BABYLON.Scene(this.engine);		
		this.scene.ambientColor = new BABYLON.Color3(0.33, 0.33, 0.33);
		this.scene.clearColor = new BABYLON.Color3(0.39, 0.58, 0.93);
		BABYLON.SceneLoader.ShowLoadingScreen = false;

		let light = new BABYLON.HemisphericLight("Hemi", new BABYLON.Vector3(0, 1, 0), this.scene);
		light.diffuse = new BABYLON.Color3(1, 1, 1);
		light.specular = new BABYLON.Color3(1, 1, 1);
		light.groundColor = new BABYLON.Color3(0, 0, 0);

		this.camera = new BABYLON.ArcRotateCamera("ArcRotateCamera", -1.57, 1.57, 3, new BABYLON.Vector3.Zero(), this.scene);
		this.camera.wheelPrecision = 50;		
		this.camera.doNotSerialize = true;
		this.scene.activeCamera.attachControl(this.canvas, true);

		this.engine.runRenderLoop(() => {
			if(this.scene.isReady()) {
				this.scene.render();
			}
        });	
		
		window.addEventListener("resize", () => {
			this.engine.resize();
		});
		
		//BABYLON.SIMDHelper.DisableSIMD(); //Optimisation	SIMD desactiver
		BABYLON.SIMDHelper.EnableSIMD(); //Optimisation	SIMD activer
	}

	createMeshBody(rootMesh)
	{
		this.engine.displayLoadingUI();		
		let root = getPath(rootMesh);
		let file = getFile(rootMesh);
		setTimeout(() => {
			BABYLON.SceneLoader.ImportMesh("", root, file+"?" + (new Date()).getTime(), this.scene, (newMeshes, particle, skeletons) => {
				this.actor = newMeshes[0];			
				this.actor.position.y = -1;
				if(this.actor.skeleton) this.actor.updatePoseMatrix(BABYLON.Matrix.Identity());
				this.skeletons = skeletons[0];			
				if(this.hair) {
					this.hair.dispose();
					this.hair = null;
				}
				if(this.beard) {
					this.beard.dispose();
					this.beard = null;
				}
				if(this.armure) {
					this.armure.dispose();
					this.armure = null;
				}
				this.controleAppercu();
				this.engine.resize();
				this.engine.hideLoadingUI();
			});
		}, 100);
	}

	remove() 
	{
		if(this.actor) {
			this.actor.dispose();
			this.actor = null;
		}
		if(this.hair) {
			this.hair.dispose();
			this.hair = null;
		}
		if(this.beard) {
			this.beard.dispose();
			this.beard = null;
		}
		if(this.armure) {
			this.armure.dispose();
			this.armure = null;
		}
		this.removeControleAppercu();
	}

	createMeshHair(rootMesh)
	{
		let root = getPath(rootMesh);
		let file = getFile(rootMesh);
		BABYLON.SceneLoader.ImportMesh("", root, file+"?" + (new Date()).getTime(), this.scene, (newMeshes) => {
			this.hair = newMeshes[0];			
			// Attache les cheveux sur l'os de la têtes
			let boneIndex = this.skeletons.getBoneIndexByName("head");
			this.hair.attachToBone(this.skeletons.bones[boneIndex], this.actor);
		});
	}
	
	createMeshBeard(rootMesh)
	{ 
		let root = getPath(rootMesh);
		let file = getFile(rootMesh);
		BABYLON.SceneLoader.ImportMesh("", root, file+"?" + (new Date()).getTime(), this.scene, (newMeshes) => {
			this.beard = newMeshes[0];			
			// Attache les cheveux sur l'os de la têtes
			let boneIndex = this.skeletons.getBoneIndexByName("head");
			this.beard.attachToBone(this.skeletons.bones[boneIndex], this.actor);
		});
	}

	createMeshArmure(rootMesh)
	{
		let root = getPath(rootMesh);
		let file = getFile(rootMesh);
		BABYLON.SceneLoader.ImportMesh("", root, file+"?" + (new Date()).getTime(), this.scene, (newMeshes) => {
			this.armure = newMeshes[0];			
			// Attache l'armure sur l'os du corp
			let boneIndex = this.skeletons.getBoneIndexByName("body");
			this.armure.attachToBone(this.skeletons.bones[boneIndex], this.actor);
		});
	}

	changeTextureHead(diffuseTexture, bumpTexture, specularTexture)
	{
		if(diffuseTexture) {
			this.actor.material.subMaterials[0].diffuseTexture = new BABYLON.Texture(diffuseTexture, this.scene);
		} else if(bumpTexture) {
			this.actor.material.subMaterials[0].bumpTexture = new BABYLON.Texture(bumpTexture, this.scene);
		} else if(specularTexture) {
			this.actor.material.subMaterials[0].specularTexture = new BABYLON.Texture(specularTexture, this.scene);
		}
		this.materialsHead.push(new BABYLON.Texture(diffuseTexture, this.scene));
		this.materialsHead.push(this.actor.material);
		this.controleAppercu();
	}

	changeTextureTenu(diffuseTexture, bumpTexture, specularTexture)
	{
		if(diffuseTexture) {
			this.actor.material.subMaterials[1].diffuseTexture = new BABYLON.Texture(diffuseTexture, this.scene);
		} else if(bumpTexture) {
			this.actor.material.subMaterials[1].bumpTexture = new BABYLON.Texture(bumpTexture, this.scene);
		} else if(specularTexture) {
			this.actor.material.subMaterials[1].specularTexture = new BABYLON.Texture(specularTexture, this.scene);
		}
		this.materialsTenu.push(new BABYLON.Texture(diffuseTexture, this.scene));
		this.materialsTenu.push(this.actor.material);
		this.controleAppercu();
	}

	assigneArticle(elementArticle)
	{
		this.createMeshArmure(elementArticle);
		$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=armure_defaut&value="+elementArticle+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
	}

	setTexture(texture)
	{
		this.imageSelected = texture;
	}
	
	assignTexture(idSlot, materialType, textureType)
	{
		$("#dialog-texture_mesh").show();
		$("#listeMediasTextures").html("");
		$.getJSON("_Projects/"+projet_name+"/game data/assets.json?" + (new Date()).getTime(), function(data) {
			$('#selectImageFile').jstree({
				"core": {
					"check_callback": true,
					'data' : function (obj, cb) {
						cb.call(this, data[0]);
					}
				}
			}).on('select_node.jstree', function (e, data) {
				$("#listeMediasTextures").html("");
				let path = data.instance.get_path(data.node, '/'), liste = '';
				$.ajaxSetup({ async: false});
				$.ajax({ type: "POST",url: 'php/assets.php', data: "categoriesSelect="+path+"&texture=1&nameProject="+projet_name,
					success: function (msg) {
						msg = msg.split(";");
						let myImage = null;
						for(let i = 0, count = msg.length -1; i < count; i++) {
							myImage = "_Projects/"+projet_name+"/"+path+"/"+msg[i];
							let action = "<a href='javascript:void(0);' onClick='apercuActor.setTexture(\""+myImage+"\");'>";
							liste += "<div style='display:inline-block;width:90px;height:90px;margin:10px;text-align:center;'>"+
										action+
											"<img src='"+myImage+"' style='width:90px;height:90px;' />"+
										"</a><br />"+
										"<small>"+no_extention(msg[i])+"</small>"+
									"</div>";
						}
						$("#listeMediasTextures").append(liste);
					}
				});
				$.ajaxSetup({ async: true});
			}).bind("loaded.jstree", function (e, data) { $(this).jstree("open_all"); });
		});
		$("#dialog-texture_mesh").dialog({
			modal: false,
			autoOpen: true,
			closeText: "",
			width: 380,
			buttons: [{
				text: lang.actor.js.assigne, click: () =>{
					let texture = this.imageSelected;
					if(textureType == "head") {
						if(materialType == "diffuse") {
							$("#canal_diffuse-slot_"+idSlot+"-head").html(getFile(texture));
							$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&textureType=head&materialType=diffuse&actor="+currentRaceActeur+"."+currentClasseActeur+"&value="+texture});
							if(this.actor) this.changeTextureHead(texture, null, null);							
						}
						else if(materialType == "normal") {
							$("#canal_normal-slot_"+idSlot+"-head").html(getFile(texture));
							$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&textureType=head&materialType=bump&actor="+currentRaceActeur+"."+currentClasseActeur+"&value="+texture});
							if(this.actor) this.changeTextureHead(null, texture, null);
						}
						else if(materialType == "specular") {
							$("#canal_specular-slot_"+idSlot+"-head").html(getFile(texture));
							$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&textureType=head&materialType=specular&actor="+currentRaceActeur+"."+currentClasseActeur+"&value="+texture});
							if(this.actor) this.changeTextureHead(null, null, texture);
						}
					}
					else if(textureType == "wear") {
						if(materialType == "diffuse") {
							$("#canal_diffuse-slot_"+idSlot+"-wear").html(getFile(texture));
							$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&textureType=wear&materialType=diffuse&actor="+currentRaceActeur+"."+currentClasseActeur+"&value="+texture});
							if(this.actor) this.changeTextureTenu(texture, null, null);
						}
						else if(materialType == "normal") {
							$("#canal_normal-slot_"+idSlot+"-wear").html(getFile(texture));
							$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&textureType=wear&materialType=bump&actor="+currentRaceActeur+"."+currentClasseActeur+"&value="+texture});
							if(this.actor) this.changeTextureTenu(null, texture, null);
						}
						else if(materialType == "specular") {
							$("#canal_specular-slot_"+idSlot+"-wear").html(getFile(texture));
							$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&textureType=wear&materialType=specular&actor="+currentRaceActeur+"."+currentClasseActeur+"&value="+texture});
							if(this.actor) this.changeTextureTenu(null, null, texture);
						}
					}
					$(this).dialog("close");
					$("#dialog-texture_mesh").hide();
				}
			}]
		});
	}
	
	setMesh(mesh)
	{
		this.meshSelected = mesh;
	}
	
	assignMesh(idSlot, maillageType)
	{		
		$("#dialog-mesh_mesh").show();
		$("#listeMediasMesh").html("");
		$.getJSON("_Projects/"+projet_name+"/game data/assets.json?" + (new Date()).getTime(), function(data) {
			$('#selectMeshFile').jstree({
				"core": {
					"check_callback": true,
					'data' : function (obj, cb) {
						cb.call(this, data[0]);
					}
				}
			}).on('select_node.jstree', function (e, data) {
				$("#listeMediasMesh").html("");
				let path = data.instance.get_path(data.node, '/'), liste = '';
				$.ajaxSetup({ async: false});
				$.ajax({ type: "POST",url: 'php/assets.php', data: "categoriesSelect="+path+"&nameProject="+projet_name,
					success: function (msg) {
						msg = msg.split(";");
						let myMesh = "_Projects/"+projet_name+"/"+path+"/"+msg[i];
						for(let i = 0, count = msg.length -1; i < count; i++) {							
							let myImage = "_Projects/"+projet_name+"/"+path+"/"+no_extention(msg[i])+".png";
							let action = "<a href='javascript:void(0);' onClick='apercuActor.setMesh(\""+myMesh+"\");'>";
							liste += "<div style='display:inline-block;width:90px;height:90px;margin:10px;text-align:center;'>"+
										action+
											"<img src='"+myImage+"' style='width:90px;height:90px;' />"+
										"</a><br />"+
										"<small>"+no_extention(msg[i])+"</small>"+
									"</div>";
						}
						$("#listeMediasMesh").append(liste);
					}
				});
				$.ajaxSetup({ async: true});
			}).bind("loaded.jstree", function (e, data) { $(this).jstree("open_all"); });
		});
		$("#dialog-mesh_mesh").dialog({
			modal: false,
			autoOpen: true,
			closeText: "",
			width: 380,
			buttons: [{
				text: lang.actor.js.assigne, click: () => {
					let mesh = this.meshSelected;										
					if(maillageType == "hair") {
						$("#canal_mesh-slot_"+idSlot+"-hair").html(getFile(mesh));
						$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=hair&actor="+currentRaceActeur+"."+currentClasseActeur+"&value="+mesh});
						if(this.actor) this.createMeshHair(mesh);							
					}
					else if(maillageType == "beard") {
						$("#canal_mesh-slot_"+idSlot+"-beard").html(getFile(mesh));
						$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=beard&actor="+currentRaceActeur+"."+currentClasseActeur+"&value="+mesh});
						if(this.actor) this.createMeshBeard(mesh);
					}
					$(this).dialog("close");
					$("#dialog-texture_mesh").hide();
				}
			}]
		});		
	}

	controleAppercu()
	{
		$("#control_material").html("");
		let controlHeadRight = "", controlTenuRight = "";
		if(this.materialsHead > 1) {
			controlHeadRight = '<input type="image" onClick="apercuActor.controleClick(\'head\', \'right\');" style="margin-left:10px;cursor:pointer;" src="Ressources/right.png" /><br />';
		}
		if(this.materialsTenu > 1) {
			controlTenuRight = '<input type="image" onClick="apercuActor.controleClick(\'tenu\', \'right\');" style="margin-left:10px;cursor:pointer;" src="Ressources/right.png" />';
		}
		$("#control_material").append('Têtes: <input type="image" onClick="apercuActor.controleClick(\'head\', \'left\');" style="cursor:pointer;margin-left:26px;" src="Ressources/left.png" />'+controlHeadRight+
									  'Vêtement: <input type="image" onClick="apercuActor.controleClick(\'tenu\', \'left\');" style="cursor:pointer;" src="Ressources/left.png" />'+controlTenuRight);
	}

	controleClick(type, direction) 
	{
		if(type == "head") {
			if(direction == "left") {
				if(this.materialsHeadCurent > 0) {
					this.materialsHeadCurent -= 1;
					this.actor.material = this.materialsHead[this.materialsHeadCurent];
				}
			} else if(direction == "right") {
				if(this.materialsHeadCurent < this.materialsHead.length) {
					this.materialsHeadCurent += 1;
					this.actor.material = this.materialsHead[this.materialsHead];
				}
			}
		} else if(type == "tenu") {
			if(direction == "left") {
				if(this.materialsTenuCurent > 0) {
					this.materialsTenuCurent -= 1;
					this.actor.material = this.materialsTenu[this.materialsTenuCurent];
				}
			} else if(direction == "right") {
				if(this.materialsTenuCurent < this.materialsTenu.length) {
					this.materialsTenuCurent += 1;
					this.actor.material = this.materialsTenu[this.materialsTenuCurent];
				}
			}
		}
	}

	jquery() 
	{
		setTimeout(function() {
			$.ajax({type: "POST", url: 'PHP/listeActor.php',
				success: function(msg) {
					let actor = $(msg).attr("value");
					$.getJSON(root+actor+".json?" + (new Date()).getTime(), function(data) {
						if(data.actor_type == "player") {
							$("#player").append("<a href='javascript:void(0);' onClick='apercuActor.selectActor(\""+actor+"\");'>- "+actor+"</a><br />");
						} else if(data.actor_type == "pnj") {
							$("#pnj").append("<a href='javascript:void(0);' onClick='apercuActor.selectActor(\""+actor+"\");'>- "+actor+"</a><br />");
						} else if(data.actor_type == "animaux") {
							$("#animaux").append("<a href='javascript:void(0);' onClick='apercuActor.selectActor(\""+actor+"\");'>- "+actor+"</a><br />");
						}
					});
				}
			});			
		}, 1000);		
		
		$("#apparence-corp").change(() => {
			if(this.actor == null) {
				let mesh = $("#apparence-corp").val();			
				$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=mesh_body&actor="+currentRaceActeur+"."+currentClasseActeur+"&value="+mesh});
				this.createMeshBody(mesh);
			}
		});

		$("#listeAptitude").selectable({
			stop: function() {
				let result = $("#select-result").empty();
				$(".ui-selected", this).each(function() {
					let index = $("#listeAptitude li").index(this);
					indexCurrentAptitude = index;
					$("#attrDefautName").val($(this).attr("value"));
					$.getJSON(root+currentRaceActeur+"."+currentClasseActeur+".json?" + (new Date()).getTime(), function(data) {
						$("#attrDefautValue").val(data.aptitude[index].value);
					});
				});
			}
		});

		$("#listeResistance").selectable({
			stop: function() {
				let result = $("#select-result").empty();
				$(".ui-selected", this).each(function() {
					let index = $("#listeResistance li").index(this);
					indexCurrentResistance = index;
					$("#resistDefautName").val($(this).attr("value"));
					$.getJSON(root+currentRaceActeur+"."+currentClasseActeur+".json?" + (new Date()).getTime(), function(data) {
						$("#resistDefautValue").val(data.resistance[index].value);
					});
				});
			}
		});		

		$("#dialog-article_defaut").dialog({
			modal: false,
			autoOpen: false,
			closeText: "",
			width: 350,
			buttons: [{
				text: lang.button.js.ok, click: function() {
					$(this).dialog("close");
				}
			}]
		});

		$("#HE-newActeur").click(function() {
			jPrompt(lang.actor.js.classeAndRaceName, "race.class", lang.actor.js.createPJ, function(result) {
				if(result) {
					result = result.split(".");
					$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=new&race="+result[0]+"&classe="+result[1]+"&rangeActeur=player",
						success: function() {
							$("#raceActeur").val(result[0]);
							$("#classActeur").val(result[1]);
							$("#HE-accordion-actors > #player").append("<a href='javascript:void(0);' id='"+result+"' onClick='apercuActor.selectActor(\""+result+"\");'>- "+result+"</a><br />");
							$("#exclusivRace_competance").append('<option id="'+result[0]+'" value="'+result[0]+'">'+result[0]+'</option>');
							$("#exclusivRace_items").append('<option id="'+result[0]+'" value="'+result[0]+'">'+result[0]+'</option>');
							$("#exclusivClass_competance").append('<option id="'+result[1]+'" value="'+result[1]+'">'+result[1]+'</option>');
							$("#exclusivClass_items").append('<option id="'+result[1]+'" value="'+result[1]+'">'+result[1]+'</option>');
							$("#listeActeurForAnimation").append('<option id="'+result+'" value="'+result+'">'+result+'</option>');
						}
					});
				}
			});
		});

		$("#HE-newActeurPNJ").click(function() {
			jPrompt(lang.actor.js.classeAndRaceName, "race.class", lang.actor.js.createPNJ, function(result) {
				if(result) {
					result = result.split(".");
					$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=new&race="+result[0]+"&classe="+result[1]+"&rangeActeur=pnj",
						success: function() {
							$("#raceActeur").val(result[0]);
							$("#classActeur").val(result[1]);
							$("#HE-accordion-actors > #pnj").append("<a href='javascript:void(0);' id='"+result+"' onClick='apercuActor.selectActor(\""+result+"\");'>- "+result+"</a><br />");
							$("#exclusivRace_competance").append('<option value="'+result[0]+'">'+result[0]+'</option>');
							$("#exclusivRace_items").append('<option value="'+result[0]+'">'+result[0]+'</option>');
							$("#exclusivClass_competance").append('<option value="'+result[1]+'">'+result[1]+'</option>');
							$("#exclusivClass_items").append('<option value="'+result[1]+'">'+result[1]+'</option>');
							$("#listeActeurForAnimation").append('<option value="'+result+'">'+result+'</option>');
						}
					});
				}
			});
		});

		$("#HE-newActeurAnimal").click(function() {
			jPrompt(lang.actor.js.classeAndRaceName, "race.class", lang.actor.js.createAnimal, function(result) {
				if(result) {
					result = result.split(".");
					$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=new&race="+result[0]+"&classe="+result[1]+"&rangeActeur=animaux",
						success: function() {
							$("#raceActeur").val(result[0]);
							$("#classActeur").val(result[1]);
							$("#HE-accordion-actors > #animaux").append("<a href='javascript:void(0);' id='"+result+"' onClick='apercuActor.selectActor(\""+result+"\");'>- "+result+"</a><br />");
							$("#exclusivRace_competance").append('<option id="'+result[0]+'" value="'+result[0]+'">'+result[0]+'</option>');
							$("#exclusivRace_items").append('<option id="'+result[0]+'" value="'+result[0]+'">'+result[0]+'</option>');
							$("#exclusivClass_competance").append('<option id="'+result[1]+'" value="'+result[1]+'">'+result[1]+'</option>');
							$("#exclusivClass_items").append('<option id="'+result[1]+'" value="'+result[1]+'">'+result[1]+'</option>');
							$("#listeActeurForAnimation").append('<option id="'+result+'" value="'+result+'">'+result+'</option>');
						}
					});
				}
			});
		});

		$("#HE-copyActeur").click(function() {
			if(currentRaceActeur && currentClasseActeur) {
				$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=copy&race="+currentRaceActeur+"&classe="+currentClasseActeur,
					success: function(msg) {
						$("#HE-accordion-actors > #"+msg).append("<a href='javascript:void(0);' onClick='apercuActor.selectActor(\""+currentRaceActeur+"-copy."+currentClasseActeur+"-copy\");'>- "+currentRaceActeur+"-copy."+currentClasseActeur+"-copy</a><br />");
						$("#exclusivRace_competance").append('<option value="'+result[0]+'-copy">'+result[0]+'-copy</option>');
						$("#exclusivRace_items").append('<option value="'+result[0]+'-copy">'+result[0]+'-copy</option>');
						$("#exclusivClass_competance").append('<option value="'+result[1]+'-copy">'+result[1]+'-copy</option>');
						$("#exclusivClass_items").append('<option value="'+result[1]+'-copy">'+result[1]+'-copy</option>');
						$("#listeActeurForAnimation").append('<option value="'+result+'-copy">'+result+'-copy</option>');
					}
				});
			} else {
				jAlert(lang.actor.js.alertActorNoSelected, "Attention");
			}
		});

		$("#HE-deleteActeur").click(function() {
			if(currentRaceActeur && currentClasseActeur) {
				jComfirm(lang.actor.js.confirmDeleteActor, lang.actor.js.confirm, function(isOk) {
					if(isOk) {
						$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=delete&race="+currentRaceActeur+"&classe="+currentClasseActeur,
							success: function() {
								$("input").val("");
								$(".listeAptitude").html('<li class="ui-widget-content" id="index" value="Santé">Santé</li><li class="ui-widget-content" id="index" value="Nager">Nager</li>');
								$(".listeResistance").html('<li class="ui-widget-content" id="index" value="Eau">Eau</li>');
								$("#exclusivRace_competance option[value='"+currentRaceActeur+"']").remove();
								$("#exclusivRace_items option[value='"+currentRaceActeur+"']").remove();
								$("#exclusivClass_competance option[value='"+currentClasseActeur+"']").remove();
								$("#exclusivClass_items option[value='"+currentClasseActeur+"']").remove();
								$("button.apparence").html("Choisir");
								let actor = currentRaceActeur+"."+currentClasseActeur;
								$("#listeActeurForAnimation option[value='"+actor+"']").remove();
								$("#HE-accordion-actors > #"+actor).remove();
								apercuActor.remove();
								$.ajax({type: "POST", url: 'PHP/save_competance.php', data: "type=deleteActor&race="+currentRaceActeur+"&classe="+currentClasseActeur});
								$.ajax({type: "POST", url: 'PHP/save_items.php', data: "type=deleteActor&race="+currentRaceActeur+"&classe="+currentClasseActeur});
							}
						});
					}
				});
			} else {
				jAlert(lang.actor.js.alertActorNoSelected, "Attention");
			}
		});

		$("#raceActeur").change(function() {
			let race = $("#raceActeur").val();
			let actor = currentRaceActeur+"."+currentClasseActeur;
			$("#exclusivRace_competance option[value='"+currentRaceActeur+"']").val(race).text(race).attr("id", race);
			$("#exclusivRace_items option[value='"+currentRaceActeur+"']").val(race).text(race).attr("id", race);
			$("#listeActeurForAnimation option[value='"+actor+"']").val(race+"."+currentClasseActeur).text(race+"."+currentClasseActeur).attr("id", race+"."+currentClasseActeur);
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=race&race="+currentRaceActeur+"&newrace="+race});
		});

		$("#classActeur").change(function() {
			let classe = $("#classActeur").val();
			let actor = currentRaceActeur+"."+currentClasseActeur;
			$("#exclusivClass_competance option[value='"+currentClasseActeur+"']").val(classe).text(classe).attr("id", classe);
			$("#exclusivClass_items option[value='"+currentClasseActeur+"']").val(classe).text(classe).attr("id", classe);
			$("#listeActeurForAnimation option[value='"+actor+"']").val(currentRaceActeur+"."+classe).text(currentRaceActeur+"."+classe).attr("id", currentRaceActeur+"."+classe);
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=classe&classe="+currentClasseActeur+"&newclasse="+classe});
		});

		$("#sexeActeur").change(function() {
			let sexe = $("#sexeActeur :selected").val();
			if(sexe == "Femme") { $("#BeardForMan").hide(); }
			else { $("#BeardForMan").show(); }
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=sex&value="+sexe+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
		});

		$("#ZoneStart").change(function() {
			let zoneStart = $("#ZoneStart").val();
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=zone_start&value="+zoneStart+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
		});

		$("#PortailName").change(function() {
			let portailName = $("#PortailName").val();
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=portail_start&value="+portailName+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
		});

		$("#AgressiviterType").change(function() {
			let agressivityType = $("#AgressiviterType :selected").val();
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=comportement&value="+agressivityType+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
		});
		
		$("#AgressiviterRange").change(function() {
			let agressivityRange = $("#AgressiviterRange").val();
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=range&value="+agressivityRange+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
		});
		
		$("#ActorSound").change(function() {
			let sound = $("#ActorSound").val();
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=sound&value="+sound+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
		});

		$("#factionActor").change(function() {
			let faction = $("#factionActor :selected").val();
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=faction&value="+faction+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
		});

		$("#XPActeur").change(function() {
			let XP = $("#XPActeur").val();
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=XP&value="+XP+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
		});

		$("#armeDefautActor").change(function() {
			let armeDefaut = $("#armeDefautActor :selected").val();
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=arme_defaut&value="+armeDefaut+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
		});

		$("#PNJMode").change(function() {
			let PNJMode = $("#PNJMode :selected").val();
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=commerce_mode&value="+PNJMode+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
		});

		$("#AnnimationType").change(function() {
			let animationModele = $("#AnnimationType :selected").val();
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=animation_type&value="+animationModele+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
		});

		$("#descriptionActeur").change(function() {
			let description = $("#descriptionActeur").val();
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=description&value="+description+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
		});

		$("#addAptitudeActor").click(function() {
			jPrompt(lang.actor.js.createAptitude, "", lang.actor.js.titleCreateAptitude, function(result) {
				if(result) {
					let nbElement = $('ol#addAptitudeActor li').length;
					$("#listeAptitude").append('<li class="ui-widget-content" id="index" value="'+result+'">'+result+'</li>');
					$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=addAptitude&value="+result+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
				}
			});
		});

		$("#removeAptitudeActor").click(function() {
			$('ol#listeAptitude li:eq('+indexCurrentAptitude+')').remove();
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=removeAptitude&value="+indexCurrentAptitude+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
		});

		$("#addResistanceActor").click(function() {
			jPrompt(lang.actor.js.createResistance, "", lang.actor.js.titleCreateResistance, function(result) {
				if(result) {
					let nbElement = $('ol#addAptitudeActor li').length;
					$("#listeResistance").append('<li class="ui-widget-content" id="index" value="'+result+'">'+result+'</li>');
					$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=addResistance&value="+result+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
				}
			});
		});

		$("#removeResistanceActor").click(function() {
			$('ol#listeResistance li:eq('+indexCurrentResistance+')').remove();
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=removeResistance&value="+indexCurrentResistance+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
		});

		$("#attrDefautName").change(function() {
			let attrName = $("#attrDefautName").val();
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=attrName&value="+attrName+"&id="+indexCurrentAptitude+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
		});

		$("#attrDefautValue").change(function() {
			let attrValue = $("#attrDefautValue").val();
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=attrValue&value="+attrValue+"&id="+indexCurrentAptitude+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
		});

		$("#resistDefautName").change(function() {
			let resistName = $("#resistDefautName").val();
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=resistName&value="+resistName+"&id="+indexCurrentResistance+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
		});

		$("#resistDefautValue").change(function() {
			let resistValue = $("#resistDefautValue").val();
			$.ajax({type: "POST", url: './PHP/save_actor.php', data: "root="+root+"&type=resistValue&value="+resistValue+"&id="+indexCurrentResistance+"&race="+currentRaceActeur+"&classe="+currentClasseActeur});
		});
		
		refreshDiv("actor_apparance_dialog", "Form/actor_refresh.php");
	}

}