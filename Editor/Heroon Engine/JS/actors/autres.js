/*##################################################
 *                                autres.js
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 01/01/2017
 ###################################################
 */
$(function() {	
	
	$.getJSON("_Projects/"+projet_name+"/game data/actors/general.json?" + (new Date()).getTime(), function(data) {		
		$("#formuleCombat option").filter('[value='+String(data.formule_combat)+']').attr('selected', true);		
		$("#afficheDomage option").filter('[value='+String(data.display_dammage)+']').attr('selected', true);		
		$("#collisionActeur option").filter('[value='+String(data.collision_actor)+']').attr('selected', true);
		$("#sang").val(data.image_sang);		
		$("#argentNameprimaire").val(data.monaie1);		
		$("#argentNameSecondaire").val(data.monaie2);		
		$("#argentdefaut").val(data.monaie_start);		
		$("#affichePseudo option").filter('[value='+String(data.label_pseudo_head)+']').attr('selected', true);		
		$("#afficheMessage option").filter('[value='+String(data.display_message)+']').attr('selected', true);	
		$('#color-text').spectrum("set", data.color_text); 
		$('#color-fond').spectrum("set", data.color_fond); 
	});
	
	$("#formuleCombat").change(function() {
		let formule = $("#formuleCombat :selected").val();
		$.ajax({ type: "POST", url: 'PHP/save_general.php', data: "type=formule_combat&value="+formule});	
	});
	
	$("#afficheDomage").change(function() {
		let damage = $("#afficheDomage :selected").val();
		$.ajax({ type: "POST", url: 'PHP/save_general.php', data: "type=display_dammage&value="+damage});
	});	
	
	$("#collisionActeur").change(function() {
		let collision = $("#collisionActeur :selected").val();
		$.ajax({ type: "POST", url: 'PHP/save_general.php', data: "type=collision_actor&value="+collision});
	});	
	
	$("#sang").change(function() {
		let sang = $("#sang").val();		
		$.ajax({ type: "POST", url: 'PHP/save_general.php', data: "type=image_sang&value="+sang});
	});
	
	$("#argentNameprimaire").change(function() {
		let argent1 = $("#argentNameprimaire").val();
		$.ajax({ type: "POST", url: 'PHP/save_general.php', data: "type=monaie1&value="+argent1});
	});
	
	$("#argentNameSecondaire").change(function() {
		let argent2 = $("#argentNameSecondaire").val();
		$.ajax({ type: "POST", url: 'PHP/save_general.php', data: "type=monaie2&value="+argent2});
	});
	
	$("#argentdefaut").change(function() {
		let argentDefaut = $("#argentdefaut").val();
		$.ajax({ type: "POST", url: 'PHP/save_general.php', data: "type=monaie_start&value="+argentDefaut});
	});
	
	$("#affichePseudo").change(function() {
		let displayPseudoOn = $("#affichePseudo :selected").val();
		$.ajax({ type: "POST", url: 'PHP/save_general.php', data: "type=label_pseudo_head&value="+displayPseudoOn});
	});
	
	$("#afficheMessage").change(function() {
		let displaymessage = $("#afficheMessage :selected").val();
		$.ajax({ type: "POST", url: 'PHP/save_general.php', data: "type=display_message&value="+displaymessage});
	});
	
	$('#color-text').spectrum({
		showAlpha: false,			
		hideAfterPaletteSelect: true,
		showInput: true,
		hide: function(color) {					
			$.ajax({ type: "POST", url: 'PHP/save_general.php', data: "type=color_text&value="+color});	
		}
	});
	$('#color-fond').spectrum({
		showAlpha: false,			
		hideAfterPaletteSelect: true,
		showInput: true,
		hide: function(color) {					
			$.ajax({ type: "POST", url: 'PHP/save_general.php', data: "type=color_fond&value="+color});	
		}
	});
	
});