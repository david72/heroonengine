/*##################################################
 *                                animations.js
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 13/05/2017
 ###################################################
 */
class animationEditor {

	constructor()
	{
		this.jquery();
		this.actor = null;
		this.skeleton = null;
		this.modeleSelectionner = null;
		this.animationSelected = null;
	}

	createScene()
	{		
		let divFps = document.getElementById("fps");
		let stats = document.getElementById("stats");

		if(sceneAnim) {
			engineAnim.stopRenderLoop();
			sceneAnim.dispose();
		}

		sceneAnim = new BABYLON.Scene(engineAnim);
		sceneAnim.ambientColor = new BABYLON.Color3(0.33, 0.33, 0.33);
		sceneAnim.clearColor = new BABYLON.Color3(0.39, 0.58, 0.93);
		BABYLON.SceneLoader.ShowLoadingScreen = false;

		this.camera = new BABYLON.ArcRotateCamera("Camera", -1.57, 1.57, 3, BABYLON.Vector3.Zero(), sceneAnim);
		this.camera.wheelPrecision = 50;		
		this.camera.doNotSerialize = true;
		sceneAnim.activeCamera.attachControl(canvasAnim, true);

		let light = new BABYLON.HemisphericLight("Hemi0", new BABYLON.Vector3(0, 1, 0), sceneAnim);
		light.diffuse = new BABYLON.Color3(1, 1, 1);
		light.specular = new BABYLON.Color3(0, 0, 0);
		light.groundColor = new BABYLON.Color3(0, 0, 0);

		engineAnim.runRenderLoop(() => {
			if(sceneAnim.isReady()) {
				if(this.modeleSelectionner) {
					divFps.innerHTML = BABYLON.Tools.Format(engineAnim.getFps(), 0)+" FPS<br />";
					stats.innerHTML = sceneAnim.getLastFrameDuration()+" MS";
				}
				sceneAnim.render();
			}
		});
		
		//BABYLON.SIMDHelper.DisableSIMD(); //Optimisation	SIMD desactiver
		BABYLON.SIMDHelper.EnableSIMD(); //Optimisation	SIMD activer

		engineAnim.resize();
		window.addEventListener("resize", function() {
			engineAnim.resize();
		});
	}

	createActor(actorPath, start, end, speed)
	{
		engineAnim.displayLoadingUI();		
		$.getJSON("_Projects/"+projet_name+"/game data/actors/"+actorPath+".json?" + (new Date()).getTime(), (data) => {
			let meshBody = data.apparence.mesh_body;
			let actorRoot = getPath(meshBody);
			let actorFile = getFile(meshBody);
			if(this.actor) {
				this.actor.dispose();
			}
			setTimeout(() => {
				BABYLON.SceneLoader.ImportMesh("", actorRoot, actorFile+"?" + (new Date()).getTime(), sceneAnim, (newMeshes, particle, skeletons) => {
					this.actor = newMeshes[0];				
					this.actor.position.y = -1;
					if(this.actor.skeleton) this.actor.updatePoseMatrix(BABYLON.Matrix.Identity());
					this.skeleton = skeletons[0];			
					this.modeleSelectionner = true;				
					if(end) {					
						sceneAnim.beginAnimation(this.skeleton, parseInt(start), parseInt(end), true, parseFloat(speed/100));					
					}
					engineAnim.resize();
					engineAnim.hideLoadingUI();
				});
			}, 100);
		});
	}

	animationActor(start, end, speed) 
	{
		if(this.skeleton) sceneAnim.beginAnimation(this.skeleton, parseInt(start), parseInt(end), true, parseFloat(speed/100));
	}

	selectedActor(fichierSelectionner, start, end, speed)
	{
		this.createActor(fichierSelectionner, start, end, speed);		
	}

	selectedAnimation(animation)
	{
		this.animationSelected = animation;
		$("#animName").val(this.animationSelected);
		$.ajax({ type: "POST", url: 'PHP/save_animations.php', data: "type=listeFrame&animName=" + this.animationSelected,
			success: function (msg) {
				$("#frameAnimation").html("");
				msg = msg.split(";");
				for(let i = 0, count = msg.length - 1; i < count; i++) {
					if(msg[i] != "") {
						$("#frameAnimation").append('<option value=\"'+msg[i]+'\">'+msg[i]+'</option>');
					}
				}
			}
		});
	}

	jquery(actor)
	{
		$.ajax({type: "POST", url: 'PHP/listeActor.php',
			success: function(msg) {
				$("#listeActeurForAnimation").append(msg);
			}
		});

		$.getJSON("_Projects/"+projet_name+"/game data/actors/animations.json?" + (new Date()).getTime(), function(data) {
			for(let i = 0, count = Object.keys(data.animations).length; i < count; i++) {
				let animationModele = data.animations[i].name;
				$("#ListeAnimation").append('<a href="javascript:void(0);" id="'+animationModele+'" onClick="animation.selectedAnimation(\''+animationModele+'\');">- '+animationModele+'</a><br />');
				$("#AnnimationType").append('<option id="'+animationModele+'" value="'+animationModele+'">'+animationModele+'</option>');
			}
		});

		$("#frameStart").spinner({
			min: 0, step: 1,
			spin: (event, ui) => {
				this.animationActor(parseInt($('#frameStart').spinner('value')), parseInt($('#frameEnd').spinner('value')), parseInt($('#frameSpeed').spinner('value')));
			},
			change: (event, ui) => {
				let frameSelectionner = $("#frameAnimation option:selected").val();
				if(frameSelectionner && this.animationSelected) {
					let start = $('#frameStart').spinner('value');
					$.ajax({ type: "POST", url: 'PHP/save_animations.php', data: "FrameName="+frameSelectionner+"&type=start&AnimName="+this.animationSelected+"&keyValue="+start,
						success: (msg) => {
							if(this.modeleSelectionner != null) this.animationActor(parseInt(start), parseInt($('#frameEnd').spinner('value')), parseInt($('#frameSpeed').spinner('value')));
						}
					});
				}
			}
		});

		$("#frameEnd").spinner({
			min: 0, step: 1,
			spin: (event, ui) => {
				this.animationActor(parseInt($('#frameStart').spinner('value')), parseInt($('#frameEnd').spinner('value')), parseInt($('#frameSpeed').spinner('value')));
			},
			change: (event, ui) => {
				let frameSelectionner = $("#frameAnimation option:selected").val();
				if(frameSelectionner && this.animationSelected) {
					let end = $('#frameEnd').spinner('value');
					$.ajax({ type: "POST", url: 'PHP/save_animations.php', data: "FrameName="+frameSelectionner+"&type=end&AnimName="+this.animationSelected+"&keyValue="+end,
						success: (msg) => {
							if(this.modeleSelectionner != null) this.animationActor(parseInt($('#frameStart').spinner('value')), parseInt(end), parseInt($('#frameSpeed').spinner('value')));
						}
					});
				}
			}
		});

		$("#frameSpeed").spinner({
			min: 0, step: 1,
			spin: (event, ui) => {
				this.animationActor(parseInt($('#frameStart').spinner('value')), parseInt($('#frameEnd').spinner('value')), parseInt($('#frameSpeed').spinner('value')));
			},
			change: (event, ui) => {
				let frameSelectionner = $("#frameAnimation option:selected").val();
				if(frameSelectionner && this.animationSelected) {
					let vitesse = $('#frameSpeed').spinner('value');
					$.ajax({ type: "POST", url: 'PHP/save_animations.php', data: "FrameName="+frameSelectionner+"&type=speed&AnimName="+this.animationSelected+"&keyValue="+vitesse,
						success: (msg) => {
							if(this.modeleSelectionner != null) this.animationActor(parseInt($('#frameStart').spinner('value')), parseInt($('#frameEnd').spinner('value')), parseInt(vitesse));
						}
					});
				}
			}
		});

		$("#newAnmation").on('click', function() {
			$.ajax({ type: "POST", url: 'PHP/save_animations.php', data: "type=new&animName=newAnimation",
				success: function (msg) {
					$("#ListeAnimation").append('<a href="javascript:void(0);" id="newAnimation" onClick="animation.selectedAnimation(\'newAnimation\');">- newAnimation</a><br />');
					$("#AnnimationType").append('<option id="newAnimation" value="newAnimation">newAnimation</option>');
				}
			});
		});

		$("#copyAnmation").on('click', function() {
			$.ajax({ type: "POST", url: 'PHP/save_animations.php', data: "type=copy&animName=Copie "+$("#animName").val()+"&oldAnimName="+$("#animName").val(),
				success: function (msg) {
					$("#ListeAnimation").append('<a href="javascript:void(0);" id="Copie '+$("#animName").val()+'" onClick="animation.selectedAnimation(\'Copie '+$("#animName").val()+'\');">- Copie '+$("#animName").val()+'</a><br />');
					$("#AnnimationType").append('<option id="Copie '+$("#animName").val()+'" value="Copie '+$("#animName").val()+'">Copie '+$("#animName").val()+'</option>');
				}
			});
		});

		$("#deleteAnmation").on('click', function() {
			jComfirm(lang.animation.js.confirmDeleteAnimation, "Comfirmer", function(isOk) {
				if(isOk) {
					let animation = $("#animName").val();
					$.ajax({ type: "POST", url: 'PHP/save_animations.php', data: "type=delete&animName="+animation,
						success: function (msg) {
							$("#"+animation).remove();
							$.ajax({ type: "POST", url: 'PHP/save_actor.php', data: "type=deleteAnimation&value="+animation});
						}
					});
				}
			});
		});

		$("#animName").change(() => {
			$.ajax({ type: "POST", url: 'PHP/save_animations.php', data: "type=rename&oldAnimName="+this.animationSelected+"&newName="+$("#animName").val(),
				success: (msg) => {
					$("#AnnimationType option[value='"+this.animationSelected+"']").val($("#animName").val()).text($("#animName").val()).attr("id", $("#animName").val());
					$("#"+this.animationSelected).attr("onclick", 'animation.selectedAnimation(\''+$("#animName").val()+'\')');
					$("#"+this.animationSelected).val($("#animName").val()).text($("#animName").val()).attr("id", $("#animName").val());
				}
			});
		});

		$("#frameAnimation").on("click", () => {
			let frameSelectionner = $("#frameAnimation option:selected").val();
			$.ajax({ type: "POST", url: 'PHP/save_animations.php', data: "FrameName="+frameSelectionner+"&type=Load&AnimName="+this.animationSelected,
				success: (msg) => {
					msg = msg.split(";");
					$("#frameName").val(frameSelectionner);
					$('#frameStart').spinner('value', msg[0]);
					$('#frameEnd').spinner('value', msg[1]);
					$('#frameSpeed').spinner('value', msg[2]);					
					if(this.modeleSelectionner == null) {
						fichierSelectionner = $("#listeActeurForAnimation option:eq(1)").prop('selected', true).val();
						this.selectedActor(fichierSelectionner, parseInt(msg[0]), parseInt(msg[1]), parseInt(msg[2]));
					}					
				}
			});
		});

		$("#listeActeurForAnimation").on("change", () => {
			let ActorSelectionner = $("#listeActeurForAnimation option:selected").val();			
			if(ActorSelectionner) {
				this.selectedActor(ActorSelectionner, parseInt($('#frameStart').spinner('value')), parseInt($('#frameEnd').spinner('value')), parseInt($('#frameSpeed').spinner('value')));
			}
		});

		$("#frameName").change(() => {
			let frameSelectionner = $("#frameAnimation option:selected").val();
			let oldFrameName = frameSelectionner;
			let newFrameName = $("#frameName").val();
			$.ajax({ type: "POST", url: 'PHP/save_animations.php', data: "oldFrameName="+oldFrameName+"&newFrameName="+newFrameName+"&oldAnimName="+this.animationSelected,
				success: function (msg) {
					$("#frameAnimation :selected").val(newFrameName);
					$("#frameAnimation :selected").text(newFrameName);
				}
			});
		});

		$("#newFrame").on('click', function() {
			$.ajax({ type: "POST", url: 'PHP/save_animations.php', data: "type=newFrame&animName="+$("#animName").val(),
				success: function (msg) {
					$("#frameAnimation").apppend('<option value="newFrame">newFrame</option>');
				}
			});
		});

		$("#deleteFrame").on('click', function() {
			let frameSelectionner = $("#frameAnimation option:selected").val();
			$.ajax({ type: "POST", url: 'PHP/save_animations.php', data: "type=deleteFrame&animName="+$("#animName").val()+"&FrameName="+frameSelectionner,
				success: function (msg) { $("#frameAnimation option:contains(\""+frameSelectionner+"\")").remove(); }
			});
		});
	}
}