/// <reference path="../Engines/babylon.d.ts" />
/*##################################################
 *                                particles.js
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 13/05/2017
 ###################################################
 */
class particle {

	constructor()
	{
		this.camera = null;
		this.light = null;
		this.jquery();

		BABYLON.Effect.ShadersStore["myEffectFragmentShader"] =
		"#ifdef GL_ES\n" +
		"precision highp float;\n" +
		"#endif\n" +

		"varying vec2 vUV;\n" +                     // Provided by babylon.js
		"varying vec4 vColor;\n" +                  // Provided by babylon.js

		"uniform sampler2D diffuseSampler;\n" +     // Provided by babylon.js
		"uniform float time;\n" +                   // This one is custom so we need to declare it to the effect

		"void main(void) {\n" +
			"vec2 position = vUV;\n" +
			"float color = 0.0;\n" +
			"vec2 center = vec2(0.5, 0.5);\n" +
			"color = sin(distance(position, center) * 10.0+ time * vColor.g);\n" +
			"vec4 baseColor = texture2D(diffuseSampler, vUV);\n" +
			"gl_FragColor = baseColor * vColor * vec4( vec3(color, color, color), 1.0 );\n" +
		"}\n" +
		"";
	}

	init()
	{
		$("#ContoleHeightParticle").show();
		if(globalParticle.scene) {
			globalParticle.engine.stopRenderLoop();
			if(this.camera) { this.camera.dispose(); }
			if(this.light) { this.light.dispose(); }
			globalParticle.scene.dispose();
		}

		globalParticle.scene = new BABYLON.Scene(globalParticle.engine);
		globalParticle.scene.ambientColor = new BABYLON.Color3(0.33, 0.33, 0.33);
		globalParticle.scene.clearColor = new BABYLON.Color3(0.39, 0.58, 0.93);

		this.effect = globalParticle.engine.createEffectForParticles("myEffect", ["time"]);
		//this.effect = null;

		this.camera = new BABYLON.ArcRotateCamera("Camera", 1.52, 1.52, 65, new BABYLON.Vector3.Zero(), globalParticle.scene);
		this.camera.wheelPrecision = 2;
		globalParticle.scene.activeCamera.attachControl(canvas);

		this.light = new BABYLON.HemisphericLight("Hemi0", new BABYLON.Vector3(0, 1, 0), globalParticle.scene);
		this.light.diffuse = new BABYLON.Color3(1, 1, 1);
		this.light.specular = new BABYLON.Color3(0, 0, 0);
		this.light.groundColor = new BABYLON.Color3(0, 0, 0);

		this.setParticle();

		setTimeout(() => {
			this.createEmiteur(globalParticle.scene);
			this.createParticule(globalParticle.scene);
		}, 400);

		globalParticle.engine.runRenderLoop(() =>
		{
			if(globalParticle.selectionEmiter != "0") {
				divFps.innerHTML = BABYLON.Tools.Format(globalParticle.engine.getFps(), 0)+" FPS";
				stats.innerHTML = lang.particle.js.particleActif+globalParticle.scene.getActiveParticles()+"<br />";
			} else {
				divFps.innerHTML = "";
				stats.innerHTML = "";
				$("#ContoleHeightParticle").hide();
			}
			globalParticle.scene.render();
		});

		let time = 0, order = 0.1;
		if(this.effect) {
			this.effect.onBind = () => {
				this.effect.setFloat("time", time);
				time += order;
				if (time > 100 || time < 0) {
					order *= -1;
				}
			};
		}

		window.addEventListener("resize", function() {
			globalParticle.engine.resize();
		});
	}

	setParticle()
	{
		$.getScript('_Projects/'+projet_name+'/particles/'+globalParticle.selectionEmiter +'.js?' + (new Date()).getTime(), function(data) {
			globalParticle.mode = _mode;
			globalParticle.depthWrite = _depthWrite;
			globalParticle.imageParticles = _imageParticles;
			globalParticle.sizeEmiter = 1;
			globalParticle.colorMask[0] = _colorMask[0];
			globalParticle.colorMask[1] = _colorMask[1];
			globalParticle.colorMask[2] = _colorMask[2];
			globalParticle.colorMask[3] = _colorMask[3];
			globalParticle.minEmitBox[0] = _minEmitBox[0];
			globalParticle.minEmitBox[1] = _minEmitBox[1];
			globalParticle.minEmitBox[2] = _minEmitBox[2];
			globalParticle.maxEmitBox[0] = _maxEmitBox[0];
			globalParticle.maxEmitBox[1] = _maxEmitBox[1];
			globalParticle.maxEmitBox[2] = _maxEmitBox[2];
			globalParticle.particleColor1[0] = _particleColor1[0];
			globalParticle.particleColor1[1] = _particleColor1[1];
			globalParticle.particleColor1[2] = _particleColor1[2];
			globalParticle.particleColor1[3] = _particleColor1[3];
			globalParticle.particleColor2[0] = _particleColor2[0];
			globalParticle.particleColor2[1] = _particleColor2[1];
			globalParticle.particleColor2[2] = _particleColor2[2];
			globalParticle.particleColor2[3] = _particleColor2[3];
			globalParticle.particleColorDead[0] = _particleColorDead[0];
			globalParticle.particleColorDead[1] = _particleColorDead[1];
			globalParticle.particleColorDead[2] = _particleColorDead[2];
			globalParticle.particleColorDead[3] = _particleColorDead[3];
			globalParticle.particleSizeMin = _particleSizeMin;
			globalParticle.particleSizeMax = _particleSizeMax;
			globalParticle.particleDurerVieMin = _particleDurerVieMin;
			globalParticle.particleDurerVieMax = _particleDurerVieMax;
			globalParticle.particleDentity = _particleDentity;
			globalParticle.particleGravity[0] = _particleGravity[0];
			globalParticle.particleGravity[1] = _particleGravity[1];
			globalParticle.particleGravity[2] = _particleGravity[2];
			globalParticle.particleDirection1[0] = _particleDirection1[0];
			globalParticle.particleDirection1[1] = _particleDirection1[1];
			globalParticle.particleDirection1[2] = _particleDirection1[2];
			globalParticle.particleDirection2[0] = _particleDirection2[0];
			globalParticle.particleDirection2[1] = _particleDirection2[1];
			globalParticle.particleDirection2[2] = _particleDirection2[2];
			globalParticle.particleRotation = _particleRotation;
			globalParticle.vieParticleStop = _vieParticleStop;
			globalParticle.manualEmitCount = _manualEmitCount || 0;
			globalParticle.vieParticleDuration = _vieParticleDuration;
			globalParticle.particleEmitPowerMin = _particleEmitPowerMin;
			globalParticle.particleEmitPowerMax = _particleEmitPowerMax;
			globalParticle.particleUpdateSpeed = _particleUpdateSpeed;
			globalParticle.particleEffect = _particleEffect;

			$("select#mode option").filter('[value='+String(globalParticle.mode)+']').attr('selected', true);
			$("select#depthWrite option").filter('[value='+String(globalParticle.depthWrite)+']').attr('selected', true);
			$("#particleImage").val(globalParticle.imageParticles);

			$("#masqueFond").spectrum("set", "rgba("+parseFloat(globalParticle.colorMask[0]*255)+", "+parseFloat(globalParticle.colorMask[1]*255)+", "+parseFloat(globalParticle.colorMask[2]*255)+", "+parseFloat(globalParticle.colorMask[3])+")");
			$("#color-particle1").spectrum("set", "rgba("+parseFloat(globalParticle.particleColor1[0]*255)+", "+parseFloat(globalParticle.particleColor1[1]*255)+", "+parseFloat(globalParticle.particleColor1[2]*255)+", "+parseFloat(globalParticle.particleColor1[3])+")");
			$("#color-particle2").spectrum("set", "rgba("+parseFloat(globalParticle.particleColor2[0]*255)+", "+parseFloat(globalParticle.particleColor2[1]*255)+", "+parseFloat(globalParticle.particleColor2[2]*255)+", "+parseFloat(globalParticle.particleColor2[3])+")");
			$("#color-particle3").spectrum("set", "rgba("+parseFloat(globalParticle.particleColorDead[0]*255)+", "+parseFloat(globalParticle.particleColorDead[1]*255)+", "+parseFloat(globalParticle.particleColorDead[2]*255)+", "+parseFloat(globalParticle.particleColorDead[3])+")");

			$("#masqueFond").val(rgbToHex(parseFloat(globalParticle.colorMask[0]*255), parseFloat(globalParticle.colorMask[1]*255), parseFloat(globalParticle.colorMask[2]*255)));
			$("#color-particle1").val(rgbToHex(parseFloat(globalParticle.particleColor1[0]*255), parseFloat(globalParticle.particleColor1[1]*255), parseFloat(globalParticle.particleColorDead[0]*255)));
			$("#color-particle2").val(rgbToHex(parseFloat(globalParticle.particleColor2[0]*255), parseFloat(globalParticle.particleColor2[1]*255), parseFloat(globalParticle.particleColorDead[1]*255)));
			$("#color-particle3").val(rgbToHex(parseFloat(globalParticle.particleColorDead[0]*255), parseFloat(globalParticle.particleColorDead[1]*255), parseFloat(globalParticle.particleColorDead[2]*255)));

			$("select#rotationParticles option").filter('[value='+String(globalParticle.particleRotation)+']').attr('selected', true);
			$('#direction1').val(globalParticle.particleDirection1[0]+","+globalParticle.particleDirection1[1]+","+globalParticle.particleDirection1[2]);
			$('#direction2').val(globalParticle.particleDirection2[0]+","+globalParticle.particleDirection2[1]+","+globalParticle.particleDirection2[2]);
			$('#graviter').val(globalParticle.particleGravity[0]+","+ globalParticle.particleGravity[1]+","+globalParticle.particleGravity[2]);
			$('#vitessePowerMin').spinner('value', globalParticle.particleEmitPowerMin);
			$('#vitessePowerMax').spinner('value', globalParticle.particleEmitPowerMax);
			$('#vitesseUpdate').spinner('value', globalParticle.particleUpdateSpeed);
			$('#vieParticleDurer').spinner('value', globalParticle.vieParticleDuration);
			$('#manualEmitCount').spinner('value', globalParticle.manualEmitCount);
			$('#durrerVieParticleMin').spinner('value', globalParticle.particleDurerVieMin);
			$('#durrerVieParticleMax').spinner('value', globalParticle.particleDurerVieMax);
			$("select#vieParticleStop option").filter('[value='+String(globalParticle.vieParticleStop)+']').attr('selected', true);
			$('#densityParticle').spinner('value', globalParticle.particleDentity);
			$('#tailleEmeteurMin').val(globalParticle.minEmitBox[0]+","+globalParticle.minEmitBox[1]+","+globalParticle.minEmitBox[2]);
			$('#tailleEmeteurMax').val(globalParticle.maxEmitBox[0]+","+globalParticle.maxEmitBox[1]+","+globalParticle.maxEmitBox[2]);
			$('#tailleParticleMax').spinner('value', globalParticle.particleSizeMax);
			$('#tailleParticleMin').spinner('value', globalParticle.particleSizeMin);
			$('select#effect option').filter('[value='+String(globalParticle.particleEffect)+']').attr('selected', true);
		});
	}

	createEmiteur(scene)
	{
		globalParticle.myEmiterParticule = BABYLON.Mesh.CreateBox("systeme_"+globalParticle.selectionEmiter+"_emeteur", globalParticle.sizeEmiter, scene);
		globalParticle.myEmiterParticule.position.y = 0;
		globalParticle.myEmiterParticule.visibility = false;
		this.effect = globalParticle.engine.createEffectForParticles("myEffect", ["time"]);
		globalParticle.particleSystem = new BABYLON.ParticleSystem("systeme_particles_"+globalParticle.selectionEmiter, 6000, scene, this.effect);
		globalParticle.particleSystem.emitter = globalParticle.myEmiterParticule;
	}

	createParticule(scene)
	{
		globalParticle.particleSystem.particleTexture = new BABYLON.Texture("_Projects/"+projet_name+"/images/Flares/"+globalParticle.imageParticles+"", scene);
		globalParticle.particleSystem.minEmitBox = new BABYLON.Vector3(globalParticle.minEmitBox[0], globalParticle.minEmitBox[1], globalParticle.minEmitBox[2]);
		globalParticle.particleSystem.maxEmitBox = new BABYLON.Vector3(globalParticle.maxEmitBox[0], globalParticle.maxEmitBox[1], globalParticle.maxEmitBox[2]);

		// Colors of all particles
		globalParticle.particleSystem.textureMask = new BABYLON.Color4(globalParticle.colorMask[0], globalParticle.colorMask[1], globalParticle.colorMask[2], globalParticle.colorMask[3]);
		globalParticle.particleSystem.color1 = new BABYLON.Color4(globalParticle.particleColor1[0], globalParticle.particleColor1[1], globalParticle.particleColor1[2], globalParticle.particleColor1[3]);
		globalParticle.particleSystem.color2 = new BABYLON.Color4(globalParticle.particleColor2[0], globalParticle.particleColor2[1], globalParticle.particleColor2[2], globalParticle.particleColor2[3]);
		globalParticle.particleSystem.colorDead = new BABYLON.Color4(globalParticle.particleColorDead[0], globalParticle.particleColorDead[1], globalParticle.particleColorDead[2], globalParticle.particleColorDead[3]);

		// Size of each particle (random between...
		globalParticle.particleSystem.minSize = globalParticle.particleSizeMin;
		globalParticle.particleSystem.maxSize = globalParticle.particleSizeMax;

		// Life time of each particle (random between...
		globalParticle.particleSystem.minLifeTime = globalParticle.particleDurerVieMin;
		globalParticle.particleSystem.maxLifeTime = globalParticle.particleDurerVieMax;

		if(globalParticle.depthWrite == "true") {
			globalParticle.particleSystem.forceDepthWrite = true;
		} else {
			globalParticle.particleSystem.forceDepthWrite = false;
		}

		// Emission rate
		globalParticle.particleSystem.emitRate = globalParticle.particleDentity;

		// Blend mode : BLENDMODE_ONEONE, or BLENDMODE_STANDARD
		if(globalParticle.mode == "one") globalParticle.particleSystem.blendMode = BABYLON.ParticleSystem.BLENDMODE_ONEONE;
		else { globalParticle.particleSystem.blendMode = BABYLON.ParticleSystem.BLENDMODE_STANDARD; }

		// Set the gravity of all particles
		globalParticle.particleSystem.gravity = new BABYLON.Vector3(globalParticle.particleGravity[0], globalParticle.particleGravity[1], globalParticle.particleGravity[2]);

		// Direction of each particle after it has been emitted
		globalParticle.particleSystem.direction1 = new BABYLON.Vector3(globalParticle.particleDirection1[0], globalParticle.particleDirection1[1], globalParticle.particleDirection1[2]);
		globalParticle.particleSystem.direction2 = new BABYLON.Vector3(globalParticle.particleDirection2[0], globalParticle.particleDirection2[1], globalParticle.particleDirection2[2]);

		// Angular speed, in radians
		if(globalParticle.particleRotation === true) {
		   globalParticle.particleSystem.minAngularSpeed = 0;
		   globalParticle.particleSystem.maxAngularSpeed = Math.PI;
		}

		if(globalParticle.manualEmitCount > 0) {
			globalParticle.particleSystem.manualEmitCount = globalParticle.manualEmitCount;
		}

		// Speed
		globalParticle.particleSystem.minEmitPower = globalParticle.particleEmitPowerMin;
		globalParticle.particleSystem.maxEmitPower = globalParticle.particleEmitPowerMax;
		globalParticle.particleSystem.updateSpeed = globalParticle.particleUpdateSpeed;

		// Start the particle system
		if(globalParticle.vieParticleStop === true) {
			globalParticle.particleSystem.targetStopDuration = globalParticle.vieParticleDuration;
			globalParticle.particleSystem.disposeOnStop = true;
		}
		globalParticle.particleSystem.start();

		if(globalParticle.particleEffect === false) {
			globalParticle.particleSystem._customEffect = null;
			globalParticle.particleEffect = "false";
		}
	}

	up() { if(this.camera) this.camera.target.y -= 2; }
	left() { if(this.camera) this.camera.target.x -= 2; }
	centreTarget() { if(this.camera) { this.camera.target.y = 0; this.camera.target.x = 0; } }
	right() { if(this.camera) this.camera.target.x += 2; }
	down() { if(this.camera) this.camera.target.y += 2; }

	jquery()
	{
		$("select#listeEmeteur").change(() => {
			globalParticle.selectionEmiter = $("#listeEmeteur option:selected").val();
			if(globalParticle.selectionEmiter !== "0") {
				this.init();
			} else {
				globalParticle.selectionEmiter = "0";
				globalParticle.particleSystem.dispose();
			}
		});

		$("#saveParticle").click(function() {
			if(globalParticle.selectionEmiter !== "0") {
				$.ajax({ type: "POST", url: 'PHP/particules.php',
					data: "type=saveParticules&name=" +globalParticle.selectionEmiter+"&depthWrite="+globalParticle.depthWrite+"&mode="+ globalParticle.mode +"&imageParticles="+globalParticle.imageParticles +"&colorMask0="+globalParticle.colorMask[0]+"&colorMask1="+globalParticle.colorMask[1]+"&colorMask2="+globalParticle.colorMask[2]+"&colorMask3="+globalParticle.colorMask[3]+"&minEmitBox0="+globalParticle.minEmitBox[0]+"&minEmitBox1="+globalParticle.minEmitBox[1]+"&minEmitBox2="+globalParticle.minEmitBox[2]+"&maxEmitBox0="+globalParticle.maxEmitBox[0]+"&maxEmitBox1="+globalParticle.maxEmitBox[1]+"&maxEmitBox2="+globalParticle.maxEmitBox[2]+"&particleColor10="+globalParticle.particleColor1[0]+"&particleColor11="+globalParticle.particleColor1[1]+"&particleColor12="+globalParticle.particleColor1[2]+"&particleColor13="+globalParticle.particleColor1[3]+"&particleColor20="+globalParticle.particleColor2[0]+"&particleColor21="+globalParticle.particleColor2[1]+"&particleColor22="+globalParticle.particleColor2[2]+"&particleColor23="+globalParticle.particleColor2[3]+"&particleColorDead0="+globalParticle.particleColorDead[0]+"&particleColorDead1="+globalParticle.particleColorDead[1]+"&particleColorDead2="+globalParticle.particleColorDead[2]+"&particleColorDead3="+globalParticle.particleColorDead[3]+"&particleSizeMin="+globalParticle.particleSizeMin+"&particleSizeMax="+globalParticle.particleSizeMax+"&particleDurerVieMin="+globalParticle.particleDurerVieMin+"&manualEmitCount="+globalParticle.manualEmitCount+"&particleDurerVieMax="+globalParticle.particleDurerVieMax+"&particleDentity="+globalParticle.particleDentity+"&particleGravity0="+globalParticle.particleGravity[0]+"&particleGravity1="+globalParticle.particleGravity[1]+"&particleGravity2="+globalParticle.particleGravity[2]+"&particleDirection10="+globalParticle.particleDirection1[0]+"&particleDirection11="+globalParticle.particleDirection1[1]+"&particleDirection12="+globalParticle.particleDirection1[2]+"&particleDirection20="+globalParticle.particleDirection2[0]+"&particleDirection21="+globalParticle.particleDirection2[1]+"&particleDirection22="+globalParticle.particleDirection2[2]+"&particleRotation="+globalParticle.particleRotation+"&vieParticleStop="+globalParticle.vieParticleStop+"&vieParticleDuration="+globalParticle.vieParticleDuration+"&particleEmitPowerMin="+globalParticle.particleEmitPowerMin+"&particleEmitPowerMax="+globalParticle.particleEmitPowerMax+"&particleUpdateSpeed="+globalParticle.particleUpdateSpeed+"&particleEffect="+globalParticle.particleEffect,
					success: function(msg) {
						jAlert(lang.particle.js.saveOk, "Attention");
					}
				});
			} else {
				jAlert(lang.particle.js.alertEmeterNoSelectedForSave, "Attention");
			}
		});

		$("#delParticle").click(function() {
			if(globalParticle.selectionEmiter !== "0") {
				jConfirm(lang.particle.js.comfirmDeleteEmeter, "Confirmation", function(conf) {
					if(conf) {
						$.ajax({ type: "POST", url: 'PHP/particules.php', data: "type=deleteParticules&name=" +globalParticle.selectionEmiter,
							success: function(msg) {
								$("#listeEmeteur option[value='"+ globalParticle.selectionEmiter +"']").remove();
								$("#"+ globalParticle.selectionEmiter).remove();
							}
						});
					}
				});
			} else {
				jAlert(lang.particle.js.alertEmeterNoSelectedForDelete, "Attention");
			}
		});

		$("#addParticle").click(() => {
			jPrompt(lang.particle.js.nameEmeteur, "", lang.particle.js.titleCreateEmeter, function(result) {
				if(result) {
					$.ajax({ type: "POST", url: 'PHP/particules.php', data: "type=creerParticules&name=" +result,
						success: (msg) => {
							if(msg == "1") {
								$("#listeEmeteur, #rainParticle, #snowParticle, #stormParticle, #emiter_article, #startParticuleProjectiles, #cibleParticuleProjectile").append('<option id="'+ result +'" value="'+ result +'" selected>'+result+'</option>');
								globalParticle.selectionEmiter = result;
								if(globalParticle.myEmiterParticule) {
									globalParticle.myEmiterParticule.dispose();
									globalParticle.selectionEmiter = "0";
									globalParticle.particleSystem.dispose();
								}
								this.init();
							}
						}
					});
				}
			});
		});

		$("#copyParticle").click(() => {
			if(globalParticle.selectionEmiter) {
				jPrompt(lang.particle.js.nameEmeteur, globalParticle.selectionEmiter, lang.particle.js.titleCopyEmeter, function(result) {
					if(result) {
						$.ajax({ type: "POST", url: 'PHP/particules.php', data: "type=copyParticules&name=" +result+"&oldName"+globalParticle.selectionEmiter,
							success: (msg) => {
								if(msg == "1") {
									$("#"+globalParticle.selectionEmiter).text(result);
									$("#"+globalParticle.selectionEmiter).val(result);
									globalParticle.selectionEmiter = result;
									if(globalParticle.myEmiterParticule) {
										globalParticle.myEmiterParticule.dispose();
										globalParticle.selectionEmiter = "0";
										globalParticle.particleSystem.dispose();
									}
									this.init();
								}
							}
						});
					}
				});
			} else {
				jAlert(lang.particle.js.alertEmeterNoSelectedForCopy, "Attention");
			}
		});

		$("select#mode").change(function() {
			mode = $("select#mode :selected").val();
			if(mode == "one") {
				globalParticle.particleSystem.blendMode = BABYLON.ParticleSystem.BLENDMODE_ONEONE;
				globalParticle.mode == "one";
			} else {
				globalParticle.particleSystem.blendMode = BABYLON.ParticleSystem.BLENDMODE_STANDARD;
				globalParticle.mode == "standard";
			}
		});

		$("select#depthWrite").change(function() {
			if($("select#depthWrite :selected").val() == "true") {
				globalParticle.depthWrite = "true";
				globalParticle.particleSystem.forceDepthWrite = true;
			}
			else {
				globalParticle.depthWrite = "false";
				globalParticle.particleSystem.forceDepthWrite = false;
			}
		});

		$("#masqueFond").spectrum({
			showAlpha: true,
			hideAfterPaletteSelect: true,
			change : function(color) {
				globalParticle.colorMask[0] = color._r/255;
				globalParticle.colorMask[1] = color._g/255;
				globalParticle.colorMask[2] = color._b/255;
				globalParticle.colorMask[3] = color._a;
				globalParticle.particleSystem.textureMask = new BABYLON.Color4(globalParticle.colorMask[0], globalParticle.colorMask[1], globalParticle.colorMask[2], globalParticle.colorMask[3]);
			},
			move : function (color) {
				globalParticle.colorMask[0] = color._r/255;
				globalParticle.colorMask[1] = color._g/255;
				globalParticle.colorMask[2] = color._b/255;
				globalParticle.colorMask[3] = color._a;
				globalParticle.particleSystem.textureMask = new BABYLON.Color4(globalParticle.colorMask[0], globalParticle.colorMask[1], globalParticle.colorMask[2], globalParticle.colorMask[3]);
			},
		});
		$("#color-particle1").spectrum({
			showAlpha: true,
			hideAfterPaletteSelect: true,
			hide: function(color) {
				globalParticle.particleColor1[0] = color._r/255;
				globalParticle.particleColor1[1] = color._g/255;
				globalParticle.particleColor1[2] = color._b/255;
				globalParticle.particleColor1[3] = color._a;
				globalParticle.particleSystem.color1 = new BABYLON.Color4(globalParticle.particleColor1[0], globalParticle.particleColor1[1], globalParticle.particleColor1[2], globalParticle.particleColor1[3]);
			},
			move : function (color) {
				globalParticle.particleColor1[0] = color._r/255;
				globalParticle.particleColor1[1] = color._g/255;
				globalParticle.particleColor1[2] = color._b/255;
				globalParticle.particleColor1[3] = color._a;
				globalParticle.particleSystem.color1 = new BABYLON.Color4(globalParticle.particleColor1[0], globalParticle.particleColor1[1], globalParticle.particleColor1[2], globalParticle.particleColor1[3]);
			},
		});
		$("#color-particle2").spectrum({
			showAlpha: true,
			hideAfterPaletteSelect: true,
			hide: function(color) {
				globalParticle.particleColor2[0] = color._r/255;
				globalParticle.particleColor2[1] = color._g/255;
				globalParticle.particleColor2[2] = color._b/255;
				globalParticle.particleColor2[3] = color._a;
				globalParticle.particleSystem.color2 = new BABYLON.Color4(globalParticle.particleColor2[0], globalParticle.particleColor2[1], globalParticle.particleColor2[2], globalParticle.particleColor2[3]);
			},
			move : function (color) {
				globalParticle.particleColor2[0] = color._r/255;
				globalParticle.particleColor2[1] = color._g/255;
				globalParticle.particleColor2[2] = color._b/255;
				globalParticle.particleColor2[3] = color._a;
				globalParticle.particleSystem.color2 = new BABYLON.Color4(globalParticle.particleColor2[0], globalParticle.particleColor2[1], globalParticle.particleColor2[2], globalParticle.particleColor2[3]);
			},
		});
		$("#color-particle3").spectrum({
			showAlpha: true,
			hideAfterPaletteSelect: true,
			hide: function(color) {
				globalParticle.particleColorDead[0] = color._r/255;
				globalParticle.particleColorDead[1] = color._g/255;
				globalParticle.particleColorDead[2] = color._b/255;
				globalParticle.particleColorDead[3] = color._a;
				globalParticle.particleSystem.colorDead = new BABYLON.Color4(globalParticle.particleColorDead[0], globalParticle.particleColorDead[1], globalParticle.particleColorDead[2], globalParticle.particleColorDead[3]);
			},
			move : function (color) {
				globalParticle.particleColorDead[0] = color._r/255;
				globalParticle.particleColorDead[1] = color._g/255;
				globalParticle.particleColorDead[2] = color._b/255;
				globalParticle.particleColorDead[3] = color._a;
				globalParticle.particleSystem.colorDead = new BABYLON.Color4(globalParticle.particleColorDead[0], globalParticle.particleColorDead[1], globalParticle.particleColorDead[2], globalParticle.particleColorDead[3]);
			},
		});

		$("#particleImage").change(function() {
			globalParticle.imageParticles = $("#particleImage").val();
			globalParticle.particleSystem.particleTexture = globalParticle.imageParticles;
		});

		$("select#vieParticleStop").change(() => {
			let select = $("select#vieParticleStop option:selected").val();
			if(select == "false") {
				$("#durerVide").hide();
				globalParticle.vieParticleStop = false;
				globalParticle.particleSystem.targetStopDuration = null;
				if(globalParticle.particleSystem.disposeOnStop == true) {
					globalParticle.particleSystem.disposeOnStop = false;
					this.init();
				}
			} else {
				$("#durerVide").show();
				globalParticle.vieParticleStop = true;
				globalParticle.particleSystem.targetStopDuration = parseFloat(globalParticle.vieParticleDuration);
				globalParticle.particleSystem.disposeOnStop = true;
			}
		});

		$("#manualEmitCount").spinner({
			step:  1, spin: (event, ui) => {
				globalParticle.manualEmitCount = $('#manualEmitCount').spinner('value');
				globalParticle.particleSystem.manualEmitCount = globalParticle.manualEmitCount;
			}, change : (event, ui) => {
				globalParticle.manualEmitCount = $('#manualEmitCount').spinner('value');
				if(globalParticle.manualEmitCount > 0 && globalParticle.particleSystem) {
					globalParticle.particleSystem.manualEmitCount = globalParticle.manualEmitCount;
					this.init();
				}
			}
		});

		$("select#rotationParticles").change(function() {
			let select = $("#rotationParticles option:selected").val();
			if(select == "false") {
				globalParticle.particleRotation = false;
				globalParticle.particleSystem.minAngularSpeed = 0;
				globalParticle.particleSystem.maxAngularSpeed = 0;
			} else {
				globalParticle.particleRotation = true;
				globalParticle.particleSystem.minAngularSpeed = 0;
				globalParticle.particleSystem.maxAngularSpeed = Math.PI;
			}
		});

		$("#tailleParticleMin").spinner({
			step: 0.1, spin: function(event, ui) {
				globalParticle.particleSizeMin = $('#tailleParticleMin').spinner('value');
				globalParticle.particleSystem.minSize = parseFloat(globalParticle.particleSizeMin);

			}, change : function(event, ui) {
				if(globalParticle.particleSizeMin && globalParticle.particleSystem) {
					globalParticle.particleSizeMin = $('#tailleParticleMin').spinner('value');
					globalParticle.particleSystem.minSize = parseFloat(globalParticle.particleSizeMin);
				}
			}
		});

		$("#tailleParticleMax").spinner({
			step: 0.1, spin: function(event, ui) {
				globalParticle.particleSizeMax = $('#tailleParticleMax').spinner('value');
				globalParticle.particleSystem.maxSize = parseFloat(globalParticle.particleSizeMax);
			}, change : function(event, ui) {
				if(globalParticle.particleSizeMax && globalParticle.particleSystem) {
					globalParticle.particleSizeMax = $('#tailleParticleMax').spinner('value');
					globalParticle.particleSystem.maxSize = parseFloat(globalParticle.particleSizeMax);
				}
			}
		});

		$("#tailleEmeteurMin").change(function(){
			let tailleEmeteurMin = $("#tailleEmeteurMin").val();
			tailleEmeteurMin = tailleEmeteurMin.split(",");
			globalParticle.minEmitBox[0] = tailleEmeteurMin[0];
			globalParticle.minEmitBox[1] = tailleEmeteurMin[1];
			globalParticle.minEmitBox[2] = tailleEmeteurMin[2];
			globalParticle.particleSystem.minEmitBox = new BABYLON.Vector3(parseFloat(globalParticle.minEmitBox[0]), parseFloat(globalParticle.minEmitBox[1]), parseFloat(globalParticle.minEmitBox[2]));
		});

		$("#tailleEmeteurMax").change(function(){
			let tailleEmeteurMax = $("#tailleEmeteurMax").val();
			tailleEmeteurMax = tailleEmeteurMax.split(",");
			globalParticle.maxEmitBox[0] = tailleEmeteurMax[0];
			globalParticle.maxEmitBox[1] = tailleEmeteurMax[1];
			globalParticle.maxEmitBox[2] = tailleEmeteurMax[2];
			globalParticle.particleSystem.maxEmitBox = new BABYLON.Vector3(parseFloat(globalParticle.maxEmitBox[0]), parseFloat(globalParticle.maxEmitBox[1]), parseFloat(globalParticle.maxEmitBox[2]));
		});

		$("#densityParticle").spinner({
			step: 1, spin: function(event, ui) {
				globalParticle.particleDentity = $('#densityParticle').spinner('value');
				globalParticle.particleSystem.emitRate = parseInt(globalParticle.particleDentity);
			}, change : function(event, ui) {
				if(globalParticle.particleDentity && globalParticle.particleSystem) {
					globalParticle.particleDentity = $('#densityParticle').spinner('value');
					globalParticle.particleSystem.emitRate = parseInt(globalParticle.particleDentity);
				}
			}
		});

		$("#durrerVieParticleMin").spinner({
			step:  0.1, spin: function(event, ui) {
				globalParticle.particleDurerVieMin = $('#durrerVieParticleMin').spinner('value');
				globalParticle.particleSystem.minLifeTime = parseFloat(globalParticle.particleDurerVieMin);
			}, change : function(event, ui) {
				globalParticle.particleDurerVieMin = $('#durrerVieParticleMin').spinner('value');
				if(globalParticle.particleDurerVieMin && globalParticle.particleSystem) {
					globalParticle.particleSystem.minLifeTime = parseFloat(globalParticle.particleDurerVieMin);
				}
			}
		});

		$("#durrerVieParticleMax").spinner({
			step: 0.1, spin: function(event, ui) {
				globalParticle.particleDurerVieMax = $('#durrerVieParticleMax').spinner('value');
				globalParticle.particleSystem.maxLifeTime = parseFloat(globalParticle.particleDurerVieMax);
			}, change : function(event, ui) {
				if(globalParticle.particleDurerVieMax && globalParticle.particleSystem) {
					globalParticle.particleDurerVieMax = $('#durrerVieParticleMax').spinner('value');
					globalParticle.particleSystem.maxLifeTime = parseFloat(globalParticle.particleDurerVieMax);
				}
			}
		});

		$("#vieParticleDurer").spinner({
			step: 1, spin: function(event, ui) {
				globalParticle.vieParticleDuration = $('#vieParticleDurer').spinner('value');
				globalParticle.particleSystem.targetStopDuration = parseFloat(globalParticle.vieParticleDuration);
				globalParticle.particleSystem.disposeOnStop = true;
			}, change : function(event, ui) {
				if(globalParticle.vieParticleDuration && globalParticle.particleSystem) {
					globalParticle.vieParticleDuration = $('#vieParticleDurer').spinner('value');
					globalParticle.particleSystem.targetStopDuration = parseFloat(globalParticle.vieParticleDuration);
					globalParticle.particleSystem.disposeOnStop = true;
				}
			}
		});

		$("#vitesseUpdate").spinner({
			step:  0.001, spin: function(event, ui) {
				globalParticle.particleUpdateSpeed = $('#vitesseUpdate').spinner('value');
				globalParticle.particleSystem.updateSpeed = parseFloat(globalParticle.particleUpdateSpeed);
			}, change : function(event, ui) {
				if(globalParticle.particleUpdateSpeed && globalParticle.particleSystem) {
					globalParticle.particleUpdateSpeed = $('#vitesseUpdate').spinner('value');
					globalParticle.particleSystem.updateSpeed = parseFloat(globalParticle.particleUpdateSpeed);
				}
			}
		});

		$("#vitessePowerMin").spinner({
			step: 0.1, spin: function(event, ui) {
				globalParticle.particleEmitPowerMin = $('#vitessePowerMin').spinner('value');
				globalParticle.particleSystem.minEmitPower = parseFloat(globalParticle.particleEmitPowerMin);

			}, change : function(event, ui) {
				if(globalParticle.particleEmitPowerMin && globalParticle.particleSystem) {
					globalParticle.particleEmitPowerMin = $('#vitessePowerMin').spinner('value');
					globalParticle.particleSystem.minEmitPower = parseFloat(globalParticle.particleEmitPowerMin);
				}
			}
		});

		$("#vitessePowerMax").spinner({
			step: 0.1, spin: function(event, ui) {
				globalParticle.particleEmitPowerMax = $('#vitessePowerMax').spinner('value');
				globalParticle.particleSystem.maxEmitPower = parseFloat(globalParticle.particleEmitPowerMax);
			}, change : function(event, ui) {
				if(globalParticle.particleEmitPowerMax && globalParticle.particleSystem) {
					globalParticle.particleEmitPowerMax = $('#vitessePowerMax').spinner('value');
					globalParticle.particleSystem.maxEmitPower = parseFloat(globalParticle.particleEmitPowerMax);
				}
			}
		});

		$("#graviter").change(function(){
			let gravity = $("#graviter").val();
			gravity = gravity.split(",");
			globalParticle.particleGravity[0] = gravity[0];
			globalParticle.particleGravity[1] = gravity[1];
			globalParticle.particleGravity[2] = gravity[2];
			globalParticle.particleSystem.gravity = new BABYLON.Vector3(parseFloat(globalParticle.particleGravity[0]), parseFloat(globalParticle.particleGravity[1]), parseFloat(globalParticle.particleGravity[2]));
		});

		$("#direction1").change(function(){
			let direction1 = $("#direction1").val();
			direction1 = direction1.split(",");
			globalParticle.particleDirection1[0] = direction1[0];
			globalParticle.particleDirection1[1] = direction1[1];
			globalParticle.particleDirection1[2] = direction1[2];
			globalParticle.particleSystem.direction1 = new BABYLON.Vector3(parseFloat(globalParticle.particleDirection1[0]), parseFloat(globalParticle.particleDirection1[1]), parseFloat(globalParticle.particleDirection1[2]));
		});

		$("#direction2").change(function(){
			let direction2 = $("#direction2").val();
			direction2 = direction2.split(",");
			globalParticle.particleDirection2[0] = direction2[0];
			globalParticle.particleDirection2[1] = direction2[1];
			globalParticle.particleDirection2[2] = direction2[2];
			globalParticle.particleSystem.direction2 = new BABYLON.Vector3(parseFloat(globalParticle.particleDirection2[0]), parseFloat(globalParticle.particleDirection2[1]), parseFloat(globalParticle.particleDirection2[2]));
		});

		$("select#effect").change(() =>{
			let effect = $("#effect :selected").val();
			if(effect == "true") {
				globalParticle.particleSystem._customEffect = this.effect;
				globalParticle.particleEffect = "true";
			} else {
				globalParticle.particleSystem._customEffect = null;
				globalParticle.particleEffect = "false";
			}
		});
	}

}