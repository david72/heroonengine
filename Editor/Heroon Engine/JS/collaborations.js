/*##################################################
 *                             collaboration.js
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 13/05/2017
 ###################################################
 */
class serveur {

	constructor(){
		
	}

	send(data) 
	{
		$.ajax({url:"PHP/collaborateur.php", type:"post", data: "data="+data+"&send=1&user="+user});
	}
	
	searchUpdate()
	{
		setTimeout(function() {			
			$.ajax({url: "PHP/collaborateur.php", type: "post", data: "searchUpdate=1",
				success: (msg) => {
					if(parseInt(msg) > 0) {
						$.ajax({url:"Form/chat_add.php", type:"post", data:"message=<b>Une nouvelle mise à jour de la scène envoyez par un collaborateur est disponible.</b>"});
						$("#avertirCollaborateurOfUpdate").attr('src', 'Ressources/bullet_red.png');
					}
					this.searchUpdate();
				}
			});
		}, 10000);	
	}

	receive()
	{
		$.ajax({url: "PHP/collaborateur.php", type: "post", data: "receive=1",
			success: (msg) => {				
				this.refreshScene(msg);
				$("#avertirCollaborateurOfUpdate").attr('src', 'Ressources/bullet_green.png');
			}
		});		
	}

	refreshScene(msg)
	{
		// ici on recoie les donnée json et on modifie la scene avec les données		
		let objEvent = jQuery.parseJSON(msg);		
		let objScene = jQuery.parseJSON(global.scene);
		for(let index = 0; i <= objScene.length; index++)
		{			
			//TODO: Pas sûr des données recue ici, a revoir apres teste concret.
			
			if(objEvent[objScene[index]]) {
				objScene[index] = objEvent[objScene[index]]
			}
		}
	}

}