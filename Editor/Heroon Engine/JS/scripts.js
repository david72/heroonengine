/*##################################################
 *                                scripts.js
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 13/05/2017
 ###################################################
 */
class scriptEditor {

	constructor()
	{
		let that = this;		
		this.editor = CodeMirror.fromTextArea(document.getElementById("code"), {
			lineNumbers: true,
			styleActiveLine: true,
			mode: {
				name: "javascript",
				globalVars: true
			},
			indentUnit: 4,	
			lineWrapping: true,
			extraKeys: {"Ctrl-Space": "autocomplete"},
			highlightSelectionMatches: {showToken: /\w/, annotateScrollbar: true}
		});
		this.editor.getWrapperElement().style["font-size"] = "15px";
		this.editor.refresh();
		this.editor.setSize(screen.width - 280, $("body").height() - 80);
		this.editor.scrollTo(screen.width - 280, $("body").height() - 80);
		
		if(theme == "White") {
			this.editor.setOption("theme", "eclipse");
		} else if(theme == "Blue") {
			this.editor.setOption("theme", "cobalt");
		} else if(theme == "Black") {
			this.editor.setOption("theme", "monokai");
		}
		
		this.editor.on("change", function(instance, changeObj) {
			if(fileWritten) {
				if(extention(fileWritten) == "js") {
					setTimeout(function() {
						that.updateHints(that);
					}, 3000);					
				}
			}
		});
		
		this.editor.on("changes", (editor, obj) => {
			if(obj[0].origin != "setValue") {
				this.goToSave(editor, obj);
			}
		});
	}

	goToSave(editeur, obj)
	{
		if(obj[0]) {
			if(obj[0].text[0] == ".") {
				CodeMirror.showHint(editeur);
			}
			changing = true;
			$("#saveScript").html('<img src="Ressources/files_mini_red.gif" style="width:22px;height:22px;padding:0;margin:0;" />');
		}
	}

	save()
	{
		let fileName = no_extention(fileWritten);
		$.ajax({ type: "POST",url: 'PHP/script.php', data: "setScript="+ scriptActive + "&code="+ encodeURIComponent(this.editor.getValue()) + "&name=" + fileName,
		success: function (msg) {
			$("#saveScript").html('<img src="Ressources/32_save.png" style="width:22px;height:22px;padding:0;margin:0;" />');
			changing = false;
		}});
	}

	openScript(rootScript)
	{
		clearTimeout(waiting);
		if(changing === true) {
			jConfirm(lang.script.js.comfirmSaveScript, "Attention", (msg) => {
				if(msg === true) {
					this.save();
				}
				$("#saveScript").html('<img src="Ressources/32_save.png" style="width:22px;height:22px;padding:0;margin:0;" />');
				changing = false;
				fileWritten = basename(rootScript);
				this.chargeScript(rootScript);
			});
		} else {
			fileWritten = basename(rootScript);
			this.chargeScript(rootScript);
		}
	}

	chargeScript(rootScript)
	{
		rootScript = basename(rootScript);
		let that = this;
		$.ajax({ type: "POST", url: 'PHP/script.php', data: "getScript="+ rootScript,
			success: function (msg) {
				scriptActive = rootScript;
				that.editor.setValue(msg);
				$(".scriptListe").each(function() { $(this).css({"color": colorBase}); });
				let ext = extention(rootScript);
				$("#"+no_extention(rootScript)+"_"+ext).css({"color":colorActif});
			}
		});
	}

	updateHints(that)
	{
		that.editor.operation(function() {			
			for (let i = 0; i < widgets.length; ++i) { 
				that.editor.removeLineWidget(widgets[i]);
			}
			widgets.length = 0;
			JSHINT(that.editor.getValue());			
			for (let i = 0; i < JSHINT.errors.length; i++) {
				let err = JSHINT.errors[i];
				if (!err) {
					continue;
				}
				let msg = document.createElement("div"), icon = msg.appendChild(document.createElement("span"));
				icon.innerHTML = "!!";
				icon.className = "lint-error-icon";
				msg.appendChild(document.createTextNode(err.reason));
				msg.className = "lint-error";
				widgets.push(that.editor.addLineWidget(err.line - 1, msg, { coverGutter: false, noHScroll: true }));
			}
		});
		let info = that.editor.getScrollInfo(), after = that.editor.charCoords({line: that.editor.getCursor().line + 1, ch: 0}, "local").top;
		if (info.top + info.clientHeight < after) {
			that.editor.scrollTo(null, after - info.clientHeight + 3);
		}
	}
	
	jquery()
	{
		let that = this;
		$("#newScriptjs").on('click', function() {
			jPrompt(lang.script.js.nameScript, "", lang.script.js.titleNewScriptJS, function(name_script) {
				if (name_script) {
					$.ajax({ type: "POST",url: 'PHP/script.php', data: "newScript=1&name=" + name_script+ "&mode=javascript", success: function (msg) {
						that.editor.setOption("mode", "javascript");
						refreshDiv("ListeScript", "Form/ListeScript.php");
						that.editor.setValue(msg);
						scriptActive = name_script + ".js";
						$(".scriptListe").each(function() { $(this).css({"color":colorBase}); });
						$("#script_competance").append('<option value="'+name_script+'">'+name_script+'</option>');
						$("#script_items").append('<option value="'+name_script+'">'+name_script+'</option>');						
						$("#"+name_script+"_js").css({"color":colorActif});
						fileWritten = name_script+".js";
					}});
				}
			});
		});
		$("#newScriptphp").on('click', function() {
			jPrompt(lang.script.js.nameScript, "", lang.script.js.titleNewScriptPHP, function(name_script) {
				if (name_script) {
					$.ajax({ type: "POST",url: 'PHP/script.php', data: "newScript=1&name=" + name_script+ "&mode=php", success: function (msg) {
						that.editor.setOption("mode", "text/x-php");
						refreshDiv("ListeScript", "Form/ListeScript.php");
						that.editor.setValue(msg);
						scriptActive = name_script + ".php";
						$(".scriptListe").each(function() { $(this).css({"color":colorBase}); });
						$("#"+name_script+"_php").css({"color":colorActif});
						fileWritten = name_script+".php";
					}});
				}
			});
		});
		$("#renameScript").on('click', function() {
			let fileWithExtension = basename(scriptActive);
			let file = null, ext = null, mode = null;
			if(extention(fileWithExtension) == "js") {
				file = fileWithExtension.replace(".js", "");
				ext = "js";
				mode = "javascript";
			} else {
				file = fileWithExtension.replace(".php", "");
				ext = "php";
				mode = "php";
			}
			jPrompt(lang.script.js.nameScript, "", lang.script.js.titleRenameScript, function(name_script) {
				if(name_script && name_script !== file) {
					$.ajax({ type: "POST",url: 'PHP/script.php', data: "newNameScript=1&name=" + name_script + "&oldName=" + file+ "&mode="+mode, success: function (msg) {
						refreshDiv("ListeScript", "Form/ListeScript.php");						
						$("#script_competance option[value='"+file+"']").val(name_script).text(name_script).attr("id", name_script);
						$("#script_items option[value='"+file+"']").val(name_script).text(name_script).attr("id", name_script);
						setTimeout(function() {
							that.editor.setValue(msg);
							scriptActive = name_script+"."+ext;
							fileWritten = name_script+"."+ext;
							$("#"+name_script+"_"+ext).css({"color":colorActif});
						}, 500);

					}});
				}
			});
		});

		$("#saveScript").on('click', () => {
			this.save();
		});

		$("#deleteScript").on('click', function() {
			jConfirm(lang.script.js.comfirmDeleteScript, "Comfirmation", function() {
				$.ajax({ type: "POST",url: 'PHP/script.php', data: "deleteScript="+ scriptActive, success: function (msg) {
					that.editor.setValue("");
					$("#script_competance option[value='"+scriptActive+"']").remove();
					$("#script_items option[value='"+scriptActive+"']").remove();				
					$.ajax({type: "POST", url: './PHP/save_competance.php', data: "type=deleteScript&script="+scriptActive});
					$.ajax({type: "POST", url: './PHP/save_items.php', data: "type=deleteScript&script="+scriptActive});					
					scriptActive = null;
					refreshDiv("ListeScript", "Form/ListeScript.php");					
				}});
			});
		});

		refreshDiv("ListeScript", "Form/ListeScript.php");
	}

}