class Chat {

	constructor() {
		this.chatTimeout = null;
	}
	
	getMessages() {
		$.ajax({url:"Form/chat_messages.php?" + (new Date()).getTime(), type:"post",
			success: (msg) => {
				$("#chat-message").html(msg);
				this.chatTimeout = setTimeout(() => { 
					this.getMessages();
				}, 3000);
			}
		});
	}

	addmessage(mess) {
		let message;
		if(mess != undefined) { message = mess; }
		else { message = $("#messagetext").val(); }
		$.ajax({url:"Form/chat_add.php", type:"post", data:"message=" + message,
			success: (msg) => {
				this.getMessages();
				if(mess == undefined) {
					$("#messagetext").val("");
				}
			}
		});
	}
	
	disableEnterKey(e){ //fonction permettant d'appeler la fonction d'ajout du texte a l'appuis sur la touche entr�e
		let key;
		if(window.event) {
			key = window.event.keyCode; //pour IE
		} else{
			key = e.which; //pour firefox
		}
	}
	
	disabledChat()
	{
		clearTimeout(this.chatTimeout);
	}
	
	enabledChat()
	{
		this.chatTimeout = setTimeout(() => {
			this.getMessages();
		}, 3000);
	}
}