/*##################################################
 *                                environement.js
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 13/05/2017
 ###################################################
 */
class environement {
	
	constructor() {
		this.hdr = null;
		this.ssao = null;		
	}

	changeDayInAMonth() {
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=DayInAMonth&value="+$("#DayInAMonth").val()});
	}

	changeDayInAYear() {
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=DayInAYear&value="+$("#DayInAYear").val()});
	}

	changeDayCurrent() {
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=DayCurrent&value="+$("#DayCurrent").val()});
	}

	changeYearCurrent() {
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=YearCurrent&value="+$("#YearCurrent :selected").val()});
	}

	changeSecondPerMinute() {
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=SecondPerMinute&value="+$("#SecondPerMinute").val()});
	}

	changeSunrise() {
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=Sunrise&value="+$("#Sunrise").val()});
	}

	changeSunset() {
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=Sunset&value="+$("#Sunset").val()});
	}

	changeLoadingZone() {
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=LoadingZone&value="+$("#LoadingZone").val()});
	}

	changeMusic() {
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=Music&value="+$("#MusicZone").val()});
		global.musicZone.updateOptions("mp3", $("#MusicZone").val());
	}

	changeGravity() {
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=Gravity&value="+$("#GravityZone").val()});
	}

	changePVP() {
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=PVP&value="+$("#PVPZone").is(":checked")});
	}

	changeScript() {
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=Script&value="+$("#ScriptZone :selected").val()});
	}

	changeOptionOctree() {
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=Octree&value="+$("#OptionOctreeZone").is(":checked")});
	}

	changeOptionOptimizer() {
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=Optimizer&value="+$("#OptionOptimizerZone").is(":checked")});
	}

	changeOptionSIMD() {
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=SIMD&value="+$("#OptionSIMD").is(":checked")});
	}

	changeSkyDay() {
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=SkyDay&value="+$("#TextureSkyDay").val()});
		let texture = $("#TextureSkyDay").val();
		texture = texture.split("_");
		global.editor.addObject.skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("_Projects/"+projet_name+"/textures/skybox/"+texture[0], global.scene);
		global.editor.addObject.skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
	}

	changeSkyNight() {
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=SkyNight&value="+$("#TextureSkyNight").val()});
	}

	changeFog()
	{		
		let color3 = $("#ColorFog").val();			
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=Fog&OptionFogEnabled="+$("#OptionFogEnabled").is(":checked")+"&ModeFog="+$("#ModeFog").val()+"&StartFog="+$("#StartFog").val()+"&EndFog="+$("#EndFog").val()+"&ColorFog="+color3+"&DensityFog="+$("#DensityFog").val()});
		let fogMode = $("#ModeFog :selected").val();
		let MODE = null;
		if(fogMode == "FOGMODE_NONE") MODE = BABYLON.Scene.FOGMODE_NONE;
		if(fogMode == "FOGMODE_EXP") MODE = BABYLON.Scene.FOGMODE_EXP;
		if(fogMode == "FOGMODE_EXP2") MODE = BABYLON.Scene.FOGMODE_EXP2;
		if(fogMode == "FOGMODE_LINEAR") MODE = BABYLON.Scene.FOGMODE_LINEAR;
		global.scene.fogMode = MODE;
		global.scene.fogStart = parseFloat($("#StartFog").val());
		global.scene.fogEnd = parseFloat($("#EndFog").val());
		global.scene.fogDensity = parseFloat($("#DensityFog").val());		
		color3 = hexToRgb(color3);
		global.scene.fogColor = new BABYLON.Color3(+parseFloat(color3.r), +parseFloat(color3.g), +parseFloat(color3.b));
	}

	changeMeteoParticle(type) {
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=MeteoParticle&element="+type+"Particle&value="+$("#"+type+"Particle :selected").val()});
	}

	changeMeteoSound(type) {
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=MeteoSound&element="+type+"Sound&value="+$("#"+type+"Sound").val()});
	}
	
	changeShadow(type)
	{
		let value = $("#"+type).val();		
		if(type == "enabledShadow") {			
			let renderList = global.editor.shadowGenerator.getShadowMap().renderList;
			if($("#optionShadowEnabled").is(":checked")) {	
				global.scene.shadowsEnabled = true;
				for(let i = 0; i < global.meshInShadow.length; i++) {					
					renderList.push(global.meshInShadow[i]);
				}				
			} else {
				global.scene.shadowsEnabled = false;		
				for(let i = 0; i < global.meshInShadow.length; i++) {
					remove_item(renderList, global.meshInShadow[i]);
				}
			}			
		} else if(type == "resolutionShadow") {
			value = $("#"+type+" :selected").val();
			global.editor.shadowGenerator._mapSize = parseInt(value);
		} else if(type == "filterShadow") {
			value = $("#"+type+" :selected").val();
			if(value == "0") {
				global.editor.shadowGenerator.useExponentialShadowMap = global.editor.shadowGenerator.useBlurExponentialShadowMap = global.editor.shadowGenerator.usePoissonSampling = global.editor.shadowGenerator.useBlurCloseExponentialShadowMap = false;
			} else if(value == "usePoissonSampling") {
				global.editor.shadowGenerator.usePoissonSampling = true;
				global.editor.shadowGenerator.useExponentialShadowMap = global.editor.shadowGenerator.useBlurExponentialShadowMap = global.editor.shadowGenerator.useBlurCloseExponentialShadowMap = false;
			} else if(value == "useExponentialShadowMap") {				
				global.editor.shadowGenerator.useExponentialShadowMap = true;
				global.editor.shadowGenerator.useBlurExponentialShadowMap = global.editor.shadowGenerator.usePoissonSampling = global.editor.shadowGenerator.useBlurCloseExponentialShadowMap = false;
			} else if(value == "useBlurExponentialShadowMap") {
				global.editor.shadowGenerator.useBlurExponentialShadowMap = true;
				global.editor.shadowGenerator.blurBoxOffset = 2.0;
				global.editor.shadowGenerator.blurKernel = 64;
				global.editor.shadowGenerator.useKernelBlur = true;
				global.editor.shadowGenerator.useExponentialShadowMap = global.editor.shadowGenerator.usePoissonSampling = global.editor.shadowGenerator.useBlurCloseExponentialShadowMap = false;
			} else if(value == "useBlurCloseExponentialShadowMap") {				
				global.editor.shadowGenerator.useBlurCloseExponentialShadowMap = true;
				global.editor.shadowGenerator.blurBoxOffset = 2.0;
				global.editor.shadowGenerator.blurKernel = 64;
				global.editor.shadowGenerator.useKernelBlur = true;
				global.editor.shadowGenerator.useBlurExponentialShadowMap = global.editor.shadowGenerator.useExponentialShadowMap = global.editor.shadowGenerator.usePoissonSampling = false;				
			}			
		} else if(type == "distanceShadowMinZ") {
			this.light.shadowMinZ = 5;				
		} else if(type == "distanceShadowMaxZ") {
			this.light.shadowMaxZ = 200;
		} else if(type == "shadowLevel") {			
			//global.editor.helper.groundShadowLevel = parseFloat($("#shadowLevel").val());
		}		
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type=shadow&element="+type+"&value="+value});
	}
	
	changePostProcess() 
	{
		let value = $("#"+type+" :selected").val();
		let postProcess = null;
		if(value == "0") {
			global.scene.activeCamera.detachPostProcess(postProcess);
			try { global.scene.postProcessRenderPipelineManager.detachCamerasFromRenderPipeline("ssao", global.camera); } catch(e) {}
			if(this.hdr) this.hdr.dispose();
			if(this.ssao) this.ssao.dispose();
		} else if(value == "FXAA") { 
			try { global.scene.postProcessRenderPipelineManager.detachCamerasFromRenderPipeline("ssao", global.camera); } catch(e) {}
			if(this.hdr) this.hdr.dispose();
			if(this.ssao) this.ssao.dispose();
			postProcess = new BABYLON.FxaaPostProcess("FXAA", 1.0, global.scene.activeCamera, BABYLON.Texture.BILINEAR_SAMPLINGMODE, global.engine, true);	
			global.scene.activeCamera.attachPostProcess(postProcess);
		} else if(value == "BlackAndWhite") {
			try { global.scene.postProcessRenderPipelineManager.detachCamerasFromRenderPipeline("ssao", global.camera); } catch(e) {}
			if(this.hdr) this.hdr.dispose();
			if(this.ssao) this.ssao.dispose();
			postProcess = new BABYLON.BlackAndWhitePostProcess("BlackAndWhite", 1.0, null, null, global.engine, true);
			global.scene.activeCamera.attachPostProcess(postProcess);
		} else if(value == "Blur") {
			try { global.scene.postProcessRenderPipelineManager.detachCamerasFromRenderPipeline("ssao", global.camera); } catch(e) {}
			if(this.hdr) this.hdr.dispose();
			if(this.ssao) this.ssao.dispose();			
			let postProcess0 = new BABYLON.BlurPostProcess("Horizontal blur", new BABYLON.Vector2(1.0, 0), 40.0, 1.0, global.camera);
			let postProcess1 = new BABYLON.BlurPostProcess("Vertical blur", new BABYLON.Vector2(0, 1.0), 40.0, 1.0, global.camera);	
		} else if(value == "HDR") {			
			this.hdr = new BABYLON.HDRRenderingPipeline("HDR", global.scene, 1.0, null, [global.scene.activeCamera]);
			if(this.ssao) this.ssao.dispose();
			this.hdr.brightThreshold = 0.5;
			this.hdr.gaussCoeff = 0.4;
			this.hdr.gaussMean = 1.0;
			this.hdr.gaussStandDev = 10.0;
			this.hdr.minimumLuminance = 0.5;
			this.hdr.luminanceDecreaseRate = 0.5;
			this.hdr.luminanceIncreaserate = 0.5;
			this.hdr.exposure = 1.0;
			this.hdr.gaussMultiplier = 4;			
		} else if(value == "SSAO") { 
			try { global.scene.postProcessRenderPipelineManager.detachCamerasFromRenderPipeline("ssao", global.camera); } catch(e) {}
			if(this.hdr) this.hdr.dispose();
			this.ssao = new BABYLON.SSAORenderingPipeline("SSAO", global.scene, 0.5);
			this.ssao.fallOff = 0.000001;
			this.ssao.area = 1;
			this.ssao.radius = 0.0001;
			this.ssao.totalStrength = 1.0;
			this.ssao.base = 0.5;
			global.scene.postProcessRenderPipelineManager.attachCamerasToRenderPipeline("SSAO", global.scene.activeCamera);
            global.scene.postProcessRenderPipelineManager.enableEffectInPipeline("SSAO", this.ssao.SSAOCombineRenderEffect, global.scene.activeCamera);
		} else if(value == "CellShading") {
			try { global.scene.postProcessRenderPipelineManager.detachCamerasFromRenderPipeline("ssao", global.camera); } catch(e) {}
			if(this.hdr) this.hdr.dispose();
			if(this.ssao) this.ssao.dispose();
		}					
		$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "zone="+global.editor.addObject.nameTerrain+"&project="+projet_name+"&type="+type+"&value="+value});
	}

	loadEnvironnementZone(nameZone)
	{
		$.getJSON("_Projects/"+projet_name+"/game data/environement.json?" + (new Date()).getTime(), function(data) {
			data = data[nameZone];
			$("#DayInAMonth").val(data.DayInAMonth);
			$("#DayInAYear").val(data.DayInAYear);
			$("#DayCurrent").val(data.DayCurrent);
			$("#YearCurrent option").filter('[value='+String(data.YearCurrent)+']').attr('selected', true);
			$("#SecondPerMinute").val(data.SecondPerMinute);
			$("#Sunrise").val(data.Sunrise);
			$("#Sunset").val(data.Sunset);
			$("#LoadingZone").val(data.LoadingZone);
			$("#MusicZone").val(data.Music)
			$("#GravityZone").val(data.Gravity);
			if(data.PVP == "true") { $("#PVPZone").attr('checked', true); }
			$("#ScriptZone option").filter('[value='+String(data.script)+']').attr('selected', true);
			if(data.Octree == "true") { $("#OptionOctreeZone").attr('checked', true); }
			if(data.Optimizer == "true") { $("#OptionOptimizerZone").attr('checked', true); }
			if(data.CheckCollision == "true") { $("#OptionCheckCollision").attr('checked', true); }
			if(data.EnabledPhysic == "true") { $("#OptionEnabledPhysic").attr('checked', true); }
			if(data.playerMap == "true") { $("#OptionPositionPlayerMap").attr('checked', true); }
			$("#TextureSkyDay").val(data.SkyDay);
			$("#TextureSkyNight").val(data.SkyNight);			
			if(data.Fog.OptionFogEnabled == "true") $("#OptionFogEnabled").attr('checked', true);
			$("#ModeFog option").filter('[value='+String(data.Fog.ModeFog)+']').attr('selected', true);
			$("#DensityFog").val(data.Fog.DensityFog);
			$("#StartFog").val(data.Fog.StartFog);
			$("#EndFog").val(data.Fog.EndFog);			
			let color3 = hexToRgb(data.Fog.ColorFog);					
			$("#ColorFog").spectrum("set", "rgb("+parseFloat(color3.r*255)+", "+parseFloat(color3.g*255)+", "+parseFloat(color3.b*255)+")");
			$("#ColorFog").val(data.Fog.ColorFog);
			$("#rainParticle option").filter('[value='+String(data.rainParticle)+']').attr('selected', true);
			$("#snowParticle option").filter('[value='+String(data.snowParticle)+']').attr('selected', true);
			$("#stormParticle option").filter('[value='+String(data.stormParticle)+']').attr('selected', true);
			$("#rainSound").val(data.rainSound);
			$("#snowSound").val(data.snowSound);
			$("#stormSound").val(data.stormSound);			
			if(data.shadow.enabledShadow == "true") $("#optionShadowEnabled").attr('checked', true); 	
			$("#filterShadow option").filter('[value='+String(data.shadow.filterShadow)+']').attr('selected', true);			
			$("#resolutionShadow option").filter('[value='+String(data.shadow.resolutionShadow)+']').attr('selected', true);
			$("#EndShadowDynamique").val(data.shadow.dynamiqueShadow);
			$("#EndShadowStatic").val(data.shadow.staticShadow);
			$("#biasShadow").val(data.shadow.biasShadow);
			$("#effectPostprocess option").filter('[value='+String(data.postprocess)+']').attr('selected', true);
		});
	}
}

$("#ColorFog").spectrum({		
	hideAfterPaletteSelect: true,
	change: function(color) {			
		let ColorFog = rgbToHex(color._r/255, color._g/255, color._b/255);
		$("#ColorFog").val(ColorFog);
	},
	move: function (color) {				
		let ColorFog = rgbToHex(color._r/255, color._g/255, color._b/255);
		$("#ColorFog").val(ColorFog);
	}
});