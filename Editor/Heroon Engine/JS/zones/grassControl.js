﻿/*##################################################
 *              grassControl.php
 *              -------------------
 *  copyright      : (C) 2017 Pellier David (dad72)
 *  email          : dad72@heroonengine.com
 *  Revision       : 13/05/2017
 ###################################################
 */
var direction = 0, MODE_CONTROLE = 0, MODE_CONTROLE_GRASS = 0;

class GrassControl {
	
	constructor(ground)
	{
		this.ground = ground;				
		this.scene = ground.getScene();		
		this.ctrl = false;
		this.down = false;			
		this.grassHeight = 0.8;
		this.invertDirection = 1.0;		
		this.radius = 20;
		this.Grass = [];
		this.gizmo = null;
		this.instanceGrass = 0;
		this.grassTexture = "_Projects/"+projet_name+"/textures/Grass/drygrass.png";
		this.spriteManagerGrass = new BABYLON.SpriteManager("grassManager", this.grassTexture, 20000, 128, this.scene);
		this.spriteManagerGrass.isPickable = true;
	}
	
	static sleep(milliseconds) {
		let start = new Date().getTime();
		for(let i = 0; i < 1e7; i++) {
			if ((new Date().getTime() - start) > milliseconds){
				break;
			}
		}
	}

	attachControl(canvas)
	{
		let currentPosition = null,	pickInfo = null, pickInfoSprite = null;
		if(this.gizmo) {
			this.gizmo.dispose();
			this.gizmo = null;
		}
		
		if(this.gizmo == null && MODE_CONTROLE == 1) {
			this.createGizmo(this.radius);			
		}

		this.onBeforeRender = () => {
			pickInfo = this.scene.pick(this.scene.pointerX, this.scene.pointerY);
			if(!this.gizmo) { return; }
			if(this.gizmo.name && pickInfo) {				
				this.gizmo.angle = (this.radius / 79); 				
				if(pickInfo.pickedPoint) { this.gizmo.position = new BABYLON.Vector3(pickInfo.pickedPoint.x, 50, pickInfo.pickedPoint.z); }
			}
			if(this.down && pickInfo.hit) {
				if (!pickInfo.hit || pickInfo.pickedMesh != this.ground) { return; }
				if(MODE_CONTROLE_GRASS > 0) { // On créer de l'herbes sur le terrain
					this.radius = $("#slider-radius").slider("value") * this.multiplicateur;
					global.saved = false;
					if(this.ctrl === false) {
						this.createGrass(pickInfo, this.radius);
					} else if(this.ctrl === true) {
						pickInfoSprite = this.scene.pickSprite(this.scene.pointerX, this.scene.pointerY);
						this.removeGrass(pickInfoSprite);
					}
				}
			}
		};

		// Clavier
		this.onKeyDown = (evt) => {
			if(evt.keyCode == 17) {
				$("#elevationButtonUp").hide();
				$("#elevationButtonDown").show();
				this.ctrl = true;				
				direction = -1;
			}
		};
		this.onKeyUp = (evt) => {
			if(evt.keyCode == 17) {
				$("#elevationButtonUp").show();
				$("#elevationButtonDown").hide();
				this.ctrl = false;				
				direction = 1;
			}
		};		
		this.scene.onPointerUp = (evt, pickResult) => {
			this.ctrl = false;
			this.down = false;
		};
		this.scene.onPointerMove = (evt, pickResult) => {
			evt.preventDefault();
            if (!currentPosition) return;
			this.invertDirection = evt.button == 2 ? -1 : 1;
			currentPosition = { x: evt.clientX, y: evt.clientY };
		};

		window.addEventListener("keydown", this.onKeyDown, false);
		window.addEventListener("keyup", this.onKeyUp, false);

		this.scene.registerBeforeRender(this.onBeforeRender);
	}

	detachControl(canvas)
	{
		window.removeEventListener("keydown", this.onKeyDown);
		window.removeEventListener("keyup", this.onKeyUp);
		this.scene.unregisterBeforeRender(this.onBeforeRender);
	}

	createGizmo(taille)
	{
		let size = parseFloat(taille/100);
		this.gizmo = new BABYLON.SpotLight("SpotGizmo", new BABYLON.Vector3(0, 10, 0), new BABYLON.Vector3(0, -1, 0), size, 0, this.scene);
		this.gizmo.diffuse = new BABYLON.Color3(0.76, 0, 0);
		this.gizmo.specular = new BABYLON.Color3(0.76, 0, 0);
		this.gizmo.doNotSerialize = true;
	}
	
	createGrass(pickInfo, radius)
	{
		let name = pickInfo.pickedMesh.name.indexOf("ass");
		if(name > -1 && this.ground) {
			let count = Math.round((radius*radius)/100);
			for(let i = 0; i < count; i++) {
				let x = (pickInfo.pickedPoint.x - (radius/10)) + Math.random() * (radius/5);
				let z = (pickInfo.pickedPoint.z - (radius/10)) + Math.random() * (radius/5);
				let y = (this.ground.getHeightAtCoordinates(x, z) + 0.5);
				this.Grass[this.instanceGrass] = new BABYLON.Sprite("grass"+this.instanceGrass, this.spriteManagerGrass);
				this.Grass[this.instanceGrass].position = new BABYLON.Vector3(x, y, z);
				this.Grass[this.instanceGrass].width = 1;
				this.Grass[this.instanceGrass].height = this.grassHeight;
				this.Grass[this.instanceGrass].isPickable = true;
				this.instanceGrass++;
			}
			GrassControl.sleep(180);
		}
	}

	removeGrass(pickInfoSprite, radius)
	{
		if(pickInfoSprite.hit) {
			let name = pickInfoSprite.pickedSprite.name.indexOf("ass");
			if(name > -1) {
				pickInfoSprite.pickedSprite.dispose();
				removeArray(this.Grass, pickInfoSprite.pickedSprite);
			}
		}
	}
}