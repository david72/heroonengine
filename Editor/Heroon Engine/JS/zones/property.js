var createProperty = function()
{
	let theObj = {
		position: '0,0,0',
		rotation: '0,0,0',
		scaling: '0,0,0',

		verrouiller: false,
		autolod: false,
		lod1: '',
		lod2: '',
		lod3: '',
		visibility: true,
		isBlocker: false,
		particules: 'none',
		checkCollisions: false,
		castShadow: false,
		recieveShadows: false,
		reflectWater: false,
		renderingGroupId: '0',
		layerMask: '1',
		material: 'none',

		mp3: 'none',
		ogg: 'none',
		wav: 'none',
		loopSound: true,
		autoplaySound: true,
		streaming: true,
		volumeSound: '1',

		mp4: 'none',
		ogv: 'none',
		webm: 'none',
		loopVideo: true,
		autoplayVideo: true,
		volumeVideo: '1',

		diffuse: '#a3ac03',
		speculare: '#a3ac03',
		intensity: 1.0,
		includeOnlyWithLayerMask: 0,
		excludeWithLayerMask: 0,
		includedOnlyMeshes: '',
		excludedMeshes: '',
		direction: '0,-1,0',
		angleLight: 0,
		exponent: 0.1,

		windForce: '-6',
		waveHeight: '0.3',
		windDirection: '1,1',
		waterColor: '#b3e8ff',
		colorBlendFactor: '0.3',
		bumpHeight: '0.1',
		waveLength: '0.1',

		waypointPNJ: 'none',
		startFlag: 'start',
		waypointScript: 'none',
		deathScript: 'none',
		pauseHereFor: 1,
		waypointDelai: 1,
		numberToWaypoint: 5,
		rayonWaypoint: 3,

		materialDefault: 'none',
		materialName: 'New material',
		diffuseTexture: 'none',
		specularTexture: 'none',
		bumpTexture: 'none',
		refractionTexture: 'none',
		reflexionTexture: 'none',
		diffuseColor: '#fff',
		uOffset: 0,
		vOffset: 0,
		uScale: 0,
		vScale: 0,
		hasAlpha: false,
		backFaceCulling: false,
		wireframe: false,

		portailNameStart: '',
		portailStartToPortailEnd: '',
		zoneOfPortail: '',
		soundPortailMp3: 'none',
		soundPortailOgg: 'none',
		soundPortailWav: 'none',

		scriptTrigger: 'none'
	};
	let theMeta = {
		position: { group: 'Transformation', name: 'Position x,y,z: ', type: "text", callback: "onChange='changePropertyMaillage(\"position\");'"},
		rotation: { group: 'Transformation', name: 'Rotation x,y,z: ', type: "text", callback: "onChange='changePropertyMaillage(\"rotation\");'"},
		scaling: { group: 'Transformation', name: 'Scaling x,y,z: ', type: "text", callback: "onChange='changePropertyMaillage(\"scaling\");'"},

		verrouiller: { group: 'Maillage', type: 'boolean', name: lang.zones.property.verrouiller, callback: "onClick='changePropertyMaillage(\"verrouiller\");'"},
		autolod: { group: 'Maillage', name: 'Auto-LOD: ', type: 'boolean', callback: "onClick='changePropertyMaillage(\"autolod\");'", description: ''},
		lod1: { group: 'Maillage', name: 'LOD height: ', type: 'liste', callback: "onChange='changePropertyMaillage(\"lod1\");'", description: ''},
		lod2: { group: 'Maillage', name: 'LOD medium: ', type: 'liste', callback: "onChange='changePropertyMaillage(\"lod2\");'", description: ''},
		lod3: { group: 'Maillage', name: 'LOD low: ', type: 'liste', callback: "onChange='changePropertyMaillage(\"lod3\");'", description: ''},
		visibility: { group: 'Maillage', name: 'Visibility: ', type: 'boolean', callback: "onClick='changePropertyMaillage(\"visibility\");'", description: ''},
		isBlocker: { group: 'Maillage', name: 'Is blocker: ', type: 'boolean', callback: "onClick='changePropertyMaillage(\"isBlocker\");'", description: ''},
		particules: { group: 'Maillage', name: lang.zones.property.particule, type: 'options', options: [{text: 'None', value: 'none'}], callback: "onChange='changePropertyMaillage(\"particules\");'", description: ''},
		checkCollisions: { group: 'Maillage', name: 'Check  collisions: ', type: 'boolean', callback: "onClick='changePropertyMaillage(\"checkCollisions\");'", description: ''},
		castShadow: { group: 'Maillage', name: lang.zones.property.castShadow, type: 'boolean', callback: "onClick='changePropertyMaillage(\"castShadow\");'", description: ''},
		recieveShadows: { group: 'Maillage', name: lang.zones.property.recieveShadows, type: 'boolean', callback: "onClick='changePropertyMaillage(\"recieveShadows\");'", description: ''},
		reflectWater: { group: 'Maillage', name: lang.zones.property.reflectWater, type: 'boolean', callback: "onClick='changePropertyMaillage(\"reflectWater\");'", description: ''},
		renderingGroupId: { group: 'Maillage', name: 'Rendu groupe id: ', type: 'number', options: { min: 0, max: 100, step: 1 }, callback: "onChange='changePropertyMaillage(\"renderingGroupId\");'"},
		layerMask: { group: 'Maillage', name: 'Layer Mask: ', type: 'number', options: { min: 0, max: 100, step: 1 }, callback: "onChange='changePropertyMaillage(\"layerMask\");'"},
		material: { group: 'Maillage', name: 'Material: ', type: 'options',  options: [{text: 'None', value: 'none'}], callback: "onChange='changePropertyMaillage(\"materiel\");'", description: ''},

		mp3: { group: 'Sound', name: 'MP3: ', type: 'options', options: [{text: 'None', value: 'none'}], callback: "onChange='changePropertySound(\"mp3\");'", description: ''},
		ogg: { group: 'Sound', name: 'OGG: ', type: 'options', options: [{text: 'None', value: 'none'}], callback: "onChange='changePropertySound(\"ogg\");'", description: ''},
		wav: { group: 'Sound', name: 'WAV: ', type: 'options', options: [{text: 'None', value: 'none'}], callback: "onChange='changePropertySound(\"wav\");'", description: ''},
		loopSound: { group: 'Sound', name: 'Loop: ', type: 'boolean', callback: "onChange='changePropertySound(\"loopSound\");'", description: ''},
		autoplaySound: { group: 'Sound', name: 'autoplay: ', type: 'boolean', callback: "onChange='changePropertySound(\"autoplaySound\");'", description: ''},
		streaming: { group: 'Sound', name: 'Streaming: ', type: 'boolean', callback: "onChange='changePropertySound(\"streaming\");'", description: ''},
		volumeSound: { group: 'Sound', name: 'Volume: ', type: 'number', options: { min: 0, max: 1, step: 0.1 }, callback: "onChange='changePropertySound(\"volumeSound\");'"},

		mp4: { group: 'Video', name: 'MP4: ', type: 'options', options: [{text: 'None', value: 'none'}], callback: "onChange='changePropertyVideo(\"mp4\");'", description: ''},
		ogv: { group: 'Video', name: 'OGC: ', type: 'options', options: [{text: 'None', value: 'none'}], callback: "onChange='changePropertyVideo(\"ogv\");'", description: ''},
		webm: { group: 'Video', name: 'WEBM: ', type: 'options', options: [{text: 'None', value: 'none'}], callback: "onChange='changePropertyVideo(\"webm\");'", description: ''},
		loopVideo: { group: 'Video', name: 'Loop: ', type: 'boolean', callback: "onChange='changePropertyVideo(\"loopVideo\");'", description: ''},
		autoplayVideo: { group: 'Video', name: 'autoplay: ', type: 'boolean', callback: "onChange='changePropertyVideo(\"autoplayVideo\");'", description: ''},
		volumeVideo: { group: 'Video', name: 'Volume: ', type: 'number', options: { min: 0, max: 1, step: 0.1 }, callback: "onChange='changePropertyVideo(\"volumeVideo\");'"},

		diffuse: { group: 'Light', name: 'Color diffuse: ', type: 'color', options: { preferredFormat: 'hex' }, callback: "onChange='changePropertyLight(\"diffuse\");'"},
		speculare: { group: 'Light', name: 'Color speculare: ', type: 'color', options: { preferredFormat: 'hex'}, callback: "onChange='changePropertyLight(\"speculare\");'"},
		intensity: { group: 'Light', name: 'Intensity: ', type: 'number', options: { min: 0, max: 100, step: 1}, callback: "onChange='changePropertyLight(\"intensity\");'"},
		includeOnlyWithLayerMask: { group: 'Light', name: 'Include With LayerMask: ', type: 'number', options: { min: 0, max: 100, step: 1}, callback: "onChange='changePropertyLight(\"includeOnlyWithLayerMask\");'"},
		excludeWithLayerMask: { group: 'Light', name: 'Exclude With LayerMask: ', type: 'number', options: { min: 0, max: 100, step: 1}, callback: "onChange='changePropertyLight(\"excludeWithLayerMask\");'"},
		includedOnlyMeshes: { group: 'Light', name: 'Included Meshes: ', type: 'liste', callback: "onChange='changePropertyLight(\"includedOnlyMeshes\");'", description: ''},
		excludedMeshes: { group: 'Light', name: 'Excluded Meshes: ', type: 'liste', callback: "onChange='changePropertyLight(\"excludedMeshes\");'", description: ''},
		direction: { group: 'Light', name: 'Direction x,y,z: ', callback: "onChange='changePropertyLight(\"direction\");'"},
		angleLight: { group: 'Light', name: 'Angle: ', type: 'number', options: { min: 0, max: 1, step: 0.1 }, callback: "onChange='changePropertyLight(\"angleLight\");'"},
		exponent: { group: 'Light', name: 'Exponent: ', type: 'number', options: { min: 0, max: 5, step: 1 }, callback: "onChange='changePropertyLight(\"exponent\");'"},

		windForce: { group: 'Water', name: 'Wind Force: ', type: 'number', options: { min: 0, max: 100, step: 0.1}, callback: "onChange='changePropertyWater(\"windForce\");'"},
		waveHeight: { group: 'Water', name: 'Wave Height: ', type: 'number', options: { min: 0, max: 5, step: 0.1}, callback: "onChange='changePropertyWater(\"waveHeight\");'"},
		windDirection: { group: 'Water', name: 'Wind Direction: ', callback: "onChange='changePropertyWater(\"windDirection\");'"},
		waterColor: { group: 'Water', name: 'Water Color: ', type: 'color', callback: "onChange='changePropertyWater(\"waterColor\");'"},
		colorBlendFactor: { group: 'Water', name: 'Color Blend Factor: ', type: 'number', options: { min: 0, max: 5, step: 0.1}, callback: "onChange='changePropertyWater(\"colorBlendFactor\");'"},
		bumpHeight: { group: 'Water', name: 'Bump Height: ', type: 'number', options: { min: 0, max: 1, step: 0.1}, callback: "onChange='changePropertyWater(\"bumpHeight\");'"},
		waveLength: { group: 'Water', name: 'Wave Length: ', type: 'number', options: { min: 0, max: 1, step: 0.1}, callback: "onChange='changePropertyWater(\"waveLength\");'"},

		waypointPNJ: { group: 'Balisage', name: lang.zones.property.waypointPNJ, type: 'liste', callback: "onChange='changePropertyBalisage(\"waypointPNJ\");", description: ''},
		startFlag: { group: 'Balisage', name: lang.zones.property.startFlag, type: 'options', options: [{text: 'Start', value: 'start'}, {text: 'End', value: 'end'}], callback: "onChange='changePropertyBalisage(\"startFlag\");'", description: ''},
		waypointScript: { group: 'Balisage', name: lang.zones.property.waypointScript, type: 'options', options: [{text: 'None', value: 'none'}], callback: "onChange='changePropertyBalisage(\"waypointScript\");'", description: ''},
		deathScript: { group: 'Balisage', name: lang.zones.property.deathScript, type: 'options', options: [{text: 'None', value: 'none'}], callback: "onChange='changePropertyBalisage(\"deathScript\");'", description: ''},
		pauseHereFor: { group: 'Balisage', name: lang.zones.property.pauseHereFor, type: 'number', options: { min: 0, max: 10, step: 1}, callback: "onChange='changePropertyBalisage(\"pauseHereFor\");'"},
		waypointDelai: { group: 'Balisage', name: lang.zones.property.waypointDelai, type: 'number', options: { min: 0, max: 10, step: 1}, callback: "onChange='changePropertyBalisage(\"waypointDelai\");'"},
		numberToWaypoint: { group: 'Balisage', name: lang.zones.property.numberToWaypoint, type: 'number', options: { min: 0, max: 20, step: 1}, callback: "onChange='changePropertyBalisage(\"numberToWaypoint\");'"},
		rayonWaypoint: { group: 'Balisage', name: lang.zones.property.rayonWaypoint, type: 'number', options: { min: 0, max: 15, step: 1}, callback: "onChange='changePropertyBalisage(\"rayonWaypoint\");'"},

		materialDefault: { group: 'Materiel', name: lang.zones.property.materialDefault, type: 'options', options: [{text: 'None', value: 'none'},
																									{text: lang.zones.property.optionFire, value: 'FeuxMaterial'},
																									{text: lang.zones.property.optionLava, value: 'LaveMaterial'},
																									{text: lang.zones.property.optionFur, value: 'FourureMaterial'},
																									{text: 'cellShading', value: 'cellShadingMaterial'}], callback: "onChange='changePropertyMateriaux(\"materialDefault\");'", description: lang.zones.property.descriptionMat},
		materialName: { group: 'Materiel', name: 'Name material: ', type: "text", callback: "onChange='changePropertyMateriaux(\"materialName\");'"},
		diffuseTexture: { group: 'Materiel', name: 'Texture diffuse: ', type: 'liste', callback: "onChange='changePropertyMateriaux(\"diffuseTexture\");'"},
		specularTexture: { group: 'Materiel', name: 'Texture speculare: ', type: 'liste', callback: "onChange='changePropertyMateriaux(\"specularTexture\");'"},
		bumpTexture: { group: 'Materiel', name: 'Texture bump: ', type: 'liste', callback: "onChange='changePropertyMateriaux(\"bumpTexture\");'"},
		refractionTexture: { group: 'Materiel', name: 'Texture Refraction: ', type: 'liste', callback: "onChange='changePropertyMateriaux(\"refractionTexture\");'"},
		reflexionTexture: { group: 'Materiel', name: 'Texture Reflexion: ', type: 'liste', callback: "onChange='changePropertyMateriaux(\"reflexionTexture\");'"},
		diffuseColor: { group: 'Materiel', name: 'Color diffuse: ', type: 'color', callback: "onChange='changePropertyMateriaux(\"diffuseColor\");'"},
		uOffset: { group: 'Materiel', name: 'uOffset: ', type: 'number', options: { min: 0.1, max: 100, step: 0.1 }, callback: "onChange='changePropertyMateriaux(\"uOffset\");'"},
		vOffset: { group: 'Materiel', name: 'vOffset: ', type: 'number', options: { min: 0.1, max: 100, step: 0.1 }, callback: "onChange='changePropertyMateriaux(\"vOffset\");'"},
		uScale: { group: 'Materiel', name: 'uScale: ', type: 'number', options: { min: 0.1, max: 100, step: 0.1 }, callback: "onChange='changePropertyMateriaux(\"uScale\");'"},
		vScale: { group: 'Materiel', name: 'vScale: ', type: 'number', options: { min: 0.1, max: 100, step: 0.1 }, callback: "onChange='changePropertyMateriaux(\"vScale\");'"},
		hasAlpha: { group: 'Materiel', name: 'Alpha: ', type: 'boolean', callback: "onChange='changePropertyMateriaux(\"hasAlpha\");'"},
		backFaceCulling: { group: 'Materiel', name: lang.zones.property.backFaceCulling, type: 'boolean', callback: "onChange='changePropertyMateriaux(\"backFaceCulling\");'"},
		wireframe: { group: 'Materiel', name: 'Wireframe: ', type: 'boolean', callback: "onChange='changePropertyMateriaux(\"wireframe\");'"},

		portailNameStart: { group: 'Portail', name: lang.zones.property.portailName, type: "text", callback: "onChange='changePropertyPortail(\"portailNameStart\");'", description: 'Nom du portail de depart'},
		portailStartToPortailEnd: { group: 'Portail', name: lang.zones.property.portailStartToPortailEnd, type: "text", callback: "onChange='changePropertyPortail(\"portailStartToPortailEnd\");'", description: 'Nom du portail de destination existant'},
		zoneOfPortail: { group: 'Portail', name: lang.zones.property.zoneOfPortail, type: "text", callback: "onChange='changePropertyPortail(\"zoneOfPortail\");'", description: 'Zone de destination du portail existant'},
		soundPortailMp3: { group: 'Portail', name: lang.zones.property.soundPortail+"MP3", type: 'options', options: [{text: 'None', value: 'none'}], callback: "onChange='changePropertyPortail(\"soundPortailMp3\");'", description: ''},
		soundPortailOgg: { group: 'Portail', name: lang.zones.property.soundPortail+"OGG", type: 'options', options: [{text: 'None', value: 'none'}], callback: "onChange='changePropertyPortail(\"soundPortailOgg\");'", description: ''},
		soundPortailWav: { group: 'Portail', name: lang.zones.property.soundPortail+"WAV", type: 'options', options: [{text: 'None', value: 'none'}], callback: "onChange='changePropertyPortail(\"soundPortailWav\");'", description: ''},

		scriptTrigger: { group: 'Declancheur', name: 'Script: ', type: 'options', options: [{text: 'None', value: 'none'}], callback: "onChange='changePropertyDeclancheur(\"scriptTrigger\");'", description: ''}
	};
	return {"Obj": theObj, "Meta": theMeta};
};

var changePropertyMaillage = function(propType)
{
	let inputValue = $("#HE_"+propType).val();
	let simplificationIsFinish = false;
	switch(propType) {
		case "position":
			let pos = inputValue.split(",");
			global.objetSelected.position = new BABYLON.Vector3(parseFloat(pos[0]), parseFloat(pos[1]), parseFloat(pos[2]));
		break;
		case "rotation":
			let rot = inputValue.split(",");
			global.objetSelected.rotation = new BABYLON.Vector3(parseFloat(rot[0]), parseFloat(rot[1]), parseFloat(rot[2]));
		break;
		case "scaling":
			let scal = inputValue.split(",");
			global.objetSelected.scaling = new BABYLON.Vector3(parseFloat(scal[0]), parseFloat(scal[1]), parseFloat(scal[2]));
		break;
		case "verrouiller":
			if($("#HE_"+propType+"").is(": checked") === true) {
				global.objectsFreeze[global.objetSelected.name] = global.objetSelected.clone(global.objetSelected.name);
				global.objectsFreeze[global.objetSelected.name].visibility = false;
				Object.freeze(global.objetSelected);
			} else {
				for(let i = 0, countObjet = global.objets.length; i < countObjet; i++) {
					if(global.objectsFreeze[global.objets[i].name]) {
						global.objectsFreeze[global.objets[i].name].visibility = true;
						global.objets[i].dispose();
						global.objets[i] = global.objectsFreeze[global.objets[i].name];
						global.objetSelected = global.objets[i];
					}
				}
			}
		break;
		case "autolod":
			if($("#HE_"+propType+"").is(": checked") === true) {
				$("#pgCell-left-lod1, #pgCell-left-lod2, #pgCell-left-lod3, #pgCell-right-lod1, #pgCell-right-lod2, #pgCell-right-lod3").hide();
				global.objetSelected.optimizeIndices(function() {
					global.objetSelected.simplify([{distance: 300, quality: 0.8}, {distance: 500, quality: 0.5}, {distance: 700, quality: 0.3}], false, BABYLON.SimplificationType.QUADRATIC, function() {
						simplificationIsFinish = true;
					});
				});
			} else {
				$("#pgCell-left-lod1, #pgCell-left-lod2, #pgCell-left-lod3, #pgCell-right-lod1, #pgCell-right-lod2, #pgCell-right-lod3").show();
				let cancelLOD = function() {
					let timeOut = setTimeout(function() {
						if(global.objetSelected._LODLevels.length > 0 && simplificationIsFinish === true) {
							for(var i = 0; i < global.objetSelected._LODLevels.length; i++) { global.objetSelected._LODLevels[i].mesh.dispose(); }
							global.objetSelected._LODLevels = [];
							clearTimeout(timeOut);
							return;
						} else { cancelLOD(); }
					}, 1000);
				};
				cancelLOD();
			}
		break;
		case "lod1":
			BABYLON.SceneLoader.ImportMesh("", getPath(inputValue), getFile(inputValue), global.scene, function (newMeshes) {
				global.objetSelected.addLODLevel(100, newMeshes[0]);
			});
		break;
		case "lod2":
			BABYLON.SceneLoader.ImportMesh("", getPath(inputValue), getFile(inputValue), global.scene, function (newMeshes) {
				global.objetSelected.addLODLevel(250, newMeshes[0]);
			});
		break;
		case "lod3":
			BABYLON.SceneLoader.ImportMesh("", getPath(inputValue), getFile(inputValue), global.scene, function (newMeshes) {
				global.objetSelected.addLODLevel(380, newMeshes[0]);
				global.objetSelected.addLODLevel(480, null);
			});
		break;
		case "visibility":
			if($("#HE_visibility").is(": checked") === true) {
				global.objetSelected.visibility = true;
			} else {
				global.objetSelected.visibility = false;
			}
		break;
		case "isBlocker":
			if($("#HE_isBlocker").is(": checked") === true) {
				global.objetSelected.isBlocker = true;
			} else {
				global.objetSelected.isBlocker = false;
			}
		break;
		case "particules":
			global.objetSelected.metadata.particleConfig = inputValue;
			$.ajax({type: "POST", url: './PHP/save_property.php', data: "property=particules&root=_Projects/"+projet_name+"/game data/&particle_config="+inputValue+"&objetName="+global.objetSelected.name});
		break;
		case "checkCollisions":
			if($("#HE_checkCollisions").is(": checked") === true) {
				global.objetSelected.checkCollisions = true;
			} else {
				global.objetSelected.checkCollisions = false;
			}
		break;
		case "castShadow":
			if($("#HE_castShadow").is(": checked") === true) {
				$("#pgCell-left-recieveShadows, #pgCell-right-recieveShadows").hide();
				global.meshInShadow.push(global.objetSelected);
				global.editor.shadowGenerator.getShadowMap().renderList.push(global.objetSelected);
				global.objetSelected.metadata.castShadow = true;
			} else {
				$("#pgCell-left-recieveShadows, #pgCell-right-recieveShadows").show();
				remove_item(global.meshInShadow, global.objetSelected);
				remove_item(global.editor.shadowGenerator.getShadowMap().renderList, global.objetSelected);
				global.objetSelected.metadata.castShadow = false;
			}
		break;
		case "recieveShadows":
			if($("#HE_recieveShadows").is(": checked") === true) {
				$("#pgCell-left-castShadow, #pgCell-right-castShadow").hide();
				global.objetSelected.recieveShadows = true;
			} else {
				$("#pgCell-left-castShadow, #pgCell-right-castShadow").show();
				global.objetSelected.recieveShadows = false;
			}
		break;
		case "reflectWater":
			if(global.editor.addObject.water) {
				if($("#HE_reflectWater").is(": checked") === true) {
					global.editor.addObject.waterMaterial.addToRenderList(global.objetSelected);
				} else {
					removeArray(global.editor.addObject.waterMaterial._refractionRTT.renderList, global.objetSelected);
					removeArray(global.editor.addObject.waterMaterial._reflectionRTT.renderList, global.objetSelected);
				}
			} else { jAlert("Il n'y a pas d'ocèan de créer sur la scène!", "Attention"); }
		break;
		case "renderingGroupId":
			global.objetSelected.renderingGroupId = parseInt(inputValue);
		break;
		case "layerMask":
			global.objetSelected.layerMask = parseInt(inputValue);
		break;
		case "material":
			let optionSelected = $("#HE_"+propType+" option: selected").val();
			let materialSelected = global.scene.getMaterialByName($("#HE_"+propType+" option: selected").val()) || global.editor.addObject.scene_for_material.getMaterialByName($("#HE_"+propType+" option: selected").val());
			if(!materialSelected) {
				$.ajax({type: "POST", url: './PHP/listeMaterials.php', data: "nameMaterial="+optionSelected,
					success: function(mess) {
						global.objetSelected.material = JSON.parse(mess);
					}
				});
			} else {
				global.objetSelected.material = materialSelected;
			}
			if(global.fur.length > -1) {
				for(let i = 0, count = global.fur.length; i < count; i++) {
					if(optionSelected == global.fur[i].name) {
						BABYLON.FurMaterial.FurifyMesh(global.objetSelected, 30);
						break;
					}
				}
			}
		break;
	}
};

var changePropertySound = function(propType)
{
	let inputValue = $("#HE_"+propType).val();
	switch(propType) {
		case "position":
			let pos = inputValue.split(",");
			global.objetSelected.position = new BABYLON.Vector3(parseFloat(pos[0]), parseFloat(pos[1]), parseFloat(pos[2]));
		break;
		case "rotation":
			let rot = inputValue.split(",");
			global.objetSelected.rotation = new BABYLON.Vector3(parseFloat(rot[0]), parseFloat(rot[1]), parseFloat(rot[2]));
		break;
		case "scaling":
			let scal = inputValue.split(",");
			global.objetSelected.scaling = new BABYLON.Vector3(parseFloat(scal[0]), parseFloat(scal[1]), parseFloat(scal[2]));
		break;
		case "mp3":
			let mp3 = inputValue;
			if(getComptibleFormatAudio().mp3) {
				global.objetSelected.metadata.mp3 = mp3;
				global.sound[global.objetSelected.name+"mp3"].updateOptions("mp3", mp3);
			} else {
				heroon.error("Le format mp3 n'est pas compatible pour ce navigateur et ne sera pas charger");
			}
		break;
		case "ogg":
			let ogg = inputValue;
			if(getComptibleFormatAudio().ogg) {
				global.objetSelected.metadata.ogg = ogg;
				global.sound[global.objetSelected.name+"ogg"].updateOptions("ogg", ogg);
			} else {
				heroon.error("Le format ogg n'est pas compatible pour ce navigateur et ne sera pas charger");
			}
		break;
		case "wav":
			let wav = inputValue;
			if(getComptibleFormatAudio().wav) {
				global.objetSelected.metadata.wav = wav;
				global.sound[global.objetSelected.name+"wav"].updateOptions("wav", wav);
			} else {
				heroon.error("Le format wav n'est pas compatible pour ce navigateur et ne sera pas charger");
			}
		break;
		case "loopSound":
			let loop = stringToBoolean(inputValue);
			global.objetSelected.metadata.loop = loop;
			global.sound[global.objetSelected.name+"mp3"].updateOptions("loop", loop);
		break;
		case "autoplaySound":
			let autoplay = stringToBoolean(inputValue);
			global.objetSelected.metadata.autoplay = autoplay;
			global.sound[global.objetSelected.name+"mp3"].updateOptions("autoplay", autoplay);
		break;
		case "streaming":
			let streaming = stringToBoolean(inputValue);
			global.objetSelected.metadata.streaming = streaming;
			global.sound[global.objetSelected.name+"mp3"].updateOptions("streaming", streaming);
		break;
		case "volumeSound":
			let volume = parseFloat(inputValue);
			global.objetSelected.metadata.volume = volume;
			global.sound[global.objetSelected.name+"mp3"].updateOptions("volume", volume);
		break;
	}
};

var changePropertyVideo = function(propType)
{
	let inputValue = $("#HE_"+propType).val();
	switch(propType) {
		case "mp4":
			let mp4 = inputValue;
			global.sound[global.objetSelected.name+"mp4"].updateOptions("mp4", mp4);
		break;
		case "ogv":
			let ogv = inputValue;
			global.sound[global.objetSelected.name+"ogv"].updateOptions("ogv", ogv);
		break;
		case "webm":
			let wav = inputValue;
			global.sound[global.objetSelected.name+"wav"].updateOptions("wav", wav);
		break;
		case "loopVideo":
			let loop = stringToBoolean(inputValue);
			global.sound[global.objetSelected.name+"mp3"].updateOptions("loop", loop);
		break;
		case "autoplayVideo":
			let autoplay = stringToBoolean(inputValue);
			global.sound[global.objetSelected.name+"mp3"].updateOptions("autoplay", autoplay);
		break;
		case "volumeVideo":
			let volume = parseFloat(inputValue);
			global.sound[global.objetSelected.name+"mp3"].updateOptions("volume", volume);
		break;
	}
};

var changePropertyLight = function(propType)
{
	let inputValue = $("#HE_"+propType).val();
	let lightSelected = null;
	if(global.lightSpotSelected) lightSelected = global.lightSpotSelected; // Le gizmo de la lumiere
	else if(global.lightPointSelected) lightSelected = global.lightPointSelected;// Le gizmo de la lumiere
	// On retire le gizmo du nom pour recuperer la lumiere
	let light_mesh = lightSelected.name.replace("Gizmo", "");
	light_mesh = scene.getLightByName(light_mesh); // La lumiere
	switch(propType) {
		case "position":
			let pos = inputValue.split(",");
			light_mesh.position = new BABYLON.Vector3(parseFloat(pos[0]), parseFloat(pos[1]), parseFloat(pos[2]));
		break;
		case "rotation":
			let rot = inputValue.split(",");
			light_mesh.rotation = new BABYLON.Vector3(parseFloat(rot[0]), parseFloat(rot[1]), parseFloat(rot[2]));
		break;
		case "scaling":
			let scal = inputValue.split(",");
			light_mesh.scaling = new BABYLON.Vector3(parseFloat(scal[0]), parseFloat(scal[1]), parseFloat(scal[2]));
		break;
		case "diffuse":
			let rgbDiffuse = hexToRgb(inputValue);
			light_mesh.diffuse = new BABYLON.Color3(parseFloat(rgbDiffuse.r), parseFloat(rgbDiffuse.g), parseFloat(rgbDiffuse.b));
		break;
		case "speculare":
			let rgbSpecular = hexToRgb(inputValue);
			light_mesh.specular = new BABYLON.Color3(parseFloat(rgbSpecular.r), parseFloat(rgbSpecular.g), parseFloat(rgbSpecular.b));
		break;
		case "intensity":
			light_mesh.intensity = parseFloat(inputValue);
		break;
		case "includeOnlyWithLayerMask":
			light_mesh.includeOnlyWithLayerMask = parseInt(inputValue);
		break;
		case "excludeWithLayerMask":
			light_mesh.excludeWithLayerMask = parseInt(inputValue);
		break;
		case "includedOnlyMeshes":
			let listeMeshesIncluded = inputValue.split(",");
			light_mesh.includedOnlyMeshes = listeMeshesIncluded;
		break;
		case "excludedMeshes":
			let listeMeshesExcluded = inputValue.split(",");
			light_mesh.excludedMeshes = listeMeshesExcluded;
		break;
		case "direction":
			let dir = inputValue.split(",");
			light_mesh.direction = new BABYLON.Vector3(parseFloat(dir[0]), parseFloat(dir[1]), parseFloat(dir[2]));
		break;
		case "angleLight":
			light_mesh.angleLight = parseFloat(inputValue);
		break;
		case "exponent":
			light_mesh.exponent = parseFloat(inputValue);
		break;
	}
};

var changePropertyWater = function(propType)
{
	let inputValue = $("#HE_"+propType).val();
	switch(propType) {
		case "windForce":
			global.water.windForce = parseFloat(inputValue);
		break;
		case "waveHeight":
			global.water.windForce = parseFloat(inputValue);
		break;
		case "windDirection":
			let dir = inputValue.split(",");
			global.water.windDirection = new BABYLON.Vector3(parseFloat(dir[0]), parseFloat(dir[1]), parseFloat(dir[2]));
		break;
		case "waterColor":
			let rgbWater = hexToRgb(inputValue);
			global.water.waterColor = new BABYLON.Color3(parseFloat(rgbWater.r), parseFloat(rgbWater.g), parseFloat(rgbWater.b));
		break;
		case "colorBlendFactor":
			let rgbBlend = hexToRgb(inputValue);
			global.water.colorBlendFactor = new BABYLON.Color3(parseFloat(rgbBlend.r), parseFloat(rgbBlend.g), parseFloat(rgbBlend.b));
		break;
		case "bumpHeight":
			global.water.bumpHeight = parseFloat(inputValue);
		break;
		case "waveLength":
			global.water.waveLength = parseFloat(inputValue);
		break;
	}
};

var changePropertyMateriaux = function(propType)
{
	let inputValue = $("#HE_"+propType).val();
	let rootMaterial = "_Projects/"+projet_name+"/textures/Materials";
	switch(propType) {
		case "materialDefault":
			let materialSelected = $("#HE_"+propType+" option: selected").val();
			if(materialSelected != "none") {
				$("#pgCell-left-materialName, #pgCell-left-materialName,"+
				"#pgCell-left-diffuseTexture, #pgCell-right-diffuseTexture,"+
				"#pgCell-right-specularTexture, #pgCell-right-specularTexture,"+
				"#pgCell-left-bumpTexture, #pgCell-left-bumpTexture,"+
				"#pgCell-left-colorMaterial, #pgCell-right-colorMaterial,"+
				"#pgCell-left-uOffset, #pgCell-left-uOffset,"+
				"#pgCell-left-vOffset, #pgCell-right-vOffset,"+
				"#pgCell-right-uScale, #pgCell-right-uScale,"+
				"#pgCell-left-vScale, #pgCell-left-vScale,"+
				"#pgCell-left-hasAlpha, #pgCell-right-hasAlpha,"+
				"#pgCell-right-backFaceCulling, #pgCell-right-backFaceCulling,"+
				"#pgCell-left-refractionTexture, #pgCell-left-refractionTexture,"+
				"#pgCell-left-reflectionTexture, #pgCell-right-reflectionTexture,"+
				"#pgCell-right-wireframe, #pgCell-right-wireframe").hide();
			} else {
				$("#pgCell-left-materialName, #pgCell-left-materialName,"+
				"#pgCell-left-diffuseTexture, #pgCell-right-diffuseTexture,"+
				"#pgCell-right-specularTexture, #pgCell-right-specularTexture,"+
				"#pgCell-left-bumpTexture, #pgCell-left-bumpTexture,"+
				"#pgCell-left-colorMaterial, #pgCell-right-colorMaterial,"+
				"#pgCell-left-uOffset, #pgCell-left-uOffset,"+
				"#pgCell-left-vOffset, #pgCell-right-vOffset,"+
				"#pgCell-right-uScale, #pgCell-right-uScale,"+
				"#pgCell-left-vScale, #pgCell-left-vScale,"+
				"#pgCell-left-hasAlpha, #pgCell-right-hasAlpha,"+
				"#pgCell-right-backFaceCulling, #pgCell-right-backFaceCulling,"+
				"#pgCell-left-refractionTexture, #pgCell-left-refractionTexture,"+
				"#pgCell-left-reflectionTexture, #pgCell-right-reflectionTexture,"+
				"#pgCell-right-wireframe, #pgCell-right-wireframe").show();
			}
			switch(materialSelected) {
				case 'FeuxMaterial':
					if(global.fire == null) {
						global.fire = new BABYLON.FireMaterial("fire", global.scene);
						global.fire.diffuseTexture = new BABYLON.Texture(rootMaterial+"/fire.png", global.scene);
						global.fire.distortionTexture = new BABYLON.Texture(rootMaterial+"/distortion.png", global.scene);
						global.fire.opacityTexture = new BABYLON.Texture(rootMaterial+"/candleOpacity.png", global.scene);
						global.fire.speed = 5.0;
						$("#HE_material").append("<option value='fire'>Fire</option>");
						saveMaterial("fire", global.fire);
					} else {
						jAlert("Vous avez déja créer un materiel de feux! Vous pouvez la réutiliser sur plusieurs objet");
					}
				break;
				case 'LaveMaterial':
					if(global.lava == null) {
						global.lava = new BABYLON.LavaMaterial("lava", global.scene);
						global.lava.noiseTexture = new BABYLON.Texture(rootMaterial+"/cloud.png",  global.scene);
						global.lava.diffuseTexture = new BABYLON.Texture(rootMaterial+"/lavatile.jpg",  global.scene);
						global.lava.speed = 0.15;
						global.lava.fogColor = new BABYLON.Color3(1, 0, 0);
						$("#HE_material").append("<option value='lava'>Lava</option>");
						saveMaterial("lava", global.lava);
					} else {
						jAlert("Vous avez déja créer un materiel de lave! Vous pouvez la réutiliser sur plusieurs objet");
					}
				break;
				case 'FourureMaterial':
					let countFur = global.fur.length;
					global.fur[countFur] = new BABYLON.FurMaterial("fur"+countFur, global.scene);
					global.fur[countFur].furLength = 1;
					global.fur[countFur].furAngle = 0;
					global.fur[countFur].furColor = BABYLON.Color3.White();
					global.fur[countFur].diffuseTexture = new BABYLON.Texture(rootMaterial+"/leopard_fur.jpg", global.scene);
					global.fur[countFur].furTexture = BABYLON.FurMaterial.GenerateTexture("furTexture", global.scene);
					global.fur[countFur].furSpacing = 10;
					global.fur[countFur].furDensity = 10;
					global.fur[countFur].furSpeed = 500;
					global.fur[countFur].furGravity = new BABYLON.Vector3(0, -1, 0);
					$("#HE_material").append("<option value='fur"+countFur+"'>Fur"+countFur+"</option>");
					saveMaterial("fur"+countFur+"", global.fur[countFur]);
				break;
				case 'cellShadingMaterial':
					if(global.objetSelected) {
						let countCellShading = global.cellShading.length;
						let TextureUsed = global.objetSelected.material.diffuseTexture;
						let cell = new BABYLON.CellMaterial("CellShading"+countCellShading, global.scene);
						cell.diffuseTexture = new BABYLON.Texture(TextureUsed, global.scene);
						cell.diffuseTexture.uScale = 1;
						cell.diffuseTexture.vScale = 1;
						cell.diffuseColor = new BABYLON.Color3(0.5, 0.5, 0.5);
						cell.computeHighLevel = true;
						global.cellShading[countCellShading] = cell;
						$("#HE_material").append("<option value='cellShading"+countCellShading+"'>Cell Shading"+countCellShading+"</option>");
						saveMaterial("cellShading"+countCellShading+"", cell);
					} else {
						heroon.error("Vous devez selectionner le maillage qui recevras le materiel cell Shading afin d'en recuperer la texture!");
					}
				break;
			}
		break;
		case "materialName":
			global.matObject["Material"+global.countMatObject].name = inputValue;
			$("#HE_material").append("<option value='"+inputValue+"'>"+inputValue+"</option>");
		break;
		case "diffuseTexture":
			global.matObject["Material"+global.countMatObject].diffuseTexture = new BABYLON.Texture(inputValue, global.scene);
		break;
		case "specularTexture":
			global.matObject["Material"+global.countMatObject].specularTexture  = new BABYLON.Texture(inputValue, global.scene);
		break;
		case "bumpTexture":
			global.matObject["Material"+global.countMatObject].bumpTexture  = new BABYLON.Texture(inputValue, global.scene);
		break;
		case "colorMaterial":
			var rgb = hexToRgb(inputValue);
			global.matObject["Material"+global.countMatObject].diffuseColor = new BABYLON.Color3(parseFloat(rgb.r), parseFloat(rgb.g), parseFloat(rgb.b));
		break;
		case "uOffset":
			global.matObject["Material"+global.countMatObject].diffuseTexture.uOffset = parseFloat(inputValue);
		break;
		case "vOffset":
			global.matObject["Material"+global.countMatObject].diffuseTexture.vOffset = parseFloat(inputValue);
		break;
		case "uScale":
			global.matObject["Material"+global.countMatObject].diffuseTexture.uScale  = parseFloat(inputValue);
		break;
		case "vScale":
			global.matObject["Material"+global.countMatObject].diffuseTexture.vScale = parseFloat(inputValue);
		break;
		case "hasAlpha":
			global.matObject["Material"+global.countMatObject].diffuseTexture.hasAlpha = stringToBoolean(inputValue);
		break;
		case "backFaceCulling":
			global.matObject["Material"+global.countMatObject].backFaceCulling = stringToBoolean(inputValue);
		break;
		case "refractionTexture":
			global.matObject["Material"+global.countMatObject].refractionTexture = new BABYLON.CubeTexture(inputValue, global.scene);
		break;
		case "reflectionTexture":
			global.matObject["Material"+global.countMatObject].reflectionTexture = new BABYLON.CubeTexture(inputValue, global.scene);
		break;
		case 'wireframe':
			global.matObject["Material"+global.countMatObject].wireframe = true;
		break;
	}
};

var changePropertyBalisage = function(propType)
{
	let id = parseInt(global.spawnpointSelected.name.replace("Spawnpoint", ""));
	let inputValue = $("#HE_"+propType).val();
	switch(propType) {
		case "waypointPNJ":
			global.spawnpointSelected.metadata = {waypointPNJ: inputValue, idPath: id};
			$.ajax({type: "POST", url: './PHP/save_property.php', data: "property=balisage&root=_Projects/"+projet_name+"/game data/&waypointPNJ="+inputValue+"&objetName="+global.spawnpointSelected.name });
		break;
		case "startFlag":
			global.spawnpointSelected.metadata = {startFlag: inputValue};
			$.ajax({type: "POST", url: './PHP/save_property.php', data: "property=balisage&root=_Projects/"+projet_name+"/game data/&startFlag="+inputValue+"&objetName="+global.spawnpointSelected.name});
		break;
		case "waypointScript":
			global.spawnpointSelected.metadata = {waypointScript: inputValue};
			$.ajax({type: "POST", url: './PHP/save_property.php', data: "property=balisage&root=_Projects/"+projet_name+"/game data/&waypointScript="+inputValue+"&objetName="+global.spawnpointSelected.name});
		break;
		case "deathScript":
			global.spawnpointSelected.metadata = {deathScript: inputValue};
			$.ajax({type: "POST", url: './PHP/save_property.php', data: "property=balisage&root=_Projects/"+projet_name+"/game data/&deathScript="+inputValue+"&objetName="+global.spawnpointSelected.name});
		break;
		case "pauseHereFor":
			global.spawnpointSelected.metadata = {pauseHereFor: parseInt(inputValue)};
			$.ajax({type: "POST", url: './PHP/save_property.php', data: "property=balisage&root=_Projects/"+projet_name+"/game data/&pauseHereFor="+inputValue+"&objetName="+global.spawnpointSelected.name});
		break;
		case "waypointDelai":
			global.spawnpointSelected.metadata = {waypointDelai: parseInt(inputValue)};
			$.ajax({type: "POST", url: './PHP/save_property.php', data: "property=balisage&root=_Projects/"+projet_name+"/game data/&waypointDelai="+inputValue+"&objetName="+global.spawnpointSelected.name});
		break;
		case "numberToWaypoint":
			global.spawnpointSelected.metadata = {numberToWaypoint: parseInt(inputValue)};
			$.ajax({type: "POST", url: './PHP/save_property.php', data: "property=balisage&root=_Projects/"+projet_name+"/game data/&numberToWaypoint="+inputValue+"&objetName="+global.spawnpointSelected.name});
		break;
		case "rayonWaypoint":
			global.spawnpointSelected.metadata = {rayonWaypoint: parseInt(inputValue)};
			$.ajax({type: "POST", url: './PHP/save_property.php', data: "property=balisage&root=_Projects/"+projet_name+"/game data/&rayonWaypoint="+inputValue+"&objetName="+global.spawnpointSelected.name});
		break;
	}
};

var changePropertyPortail = function(propType)
{
	let inputValue = $("#HE_"+propType).val();
	switch(propType) {
		case "position":
			let pos = inputValue.split(",");
			global.objetSelected.position = new BABYLON.Vector3(parseFloat(pos[0]), parseFloat(pos[1]), parseFloat(pos[2]));
		break;
		case "rotation":
			let rot = inputValue.split(",");
			global.objetSelected.rotation = new BABYLON.Vector3(parseFloat(rot[0]), parseFloat(rot[1]), parseFloat(rot[2]));
		break;
		case "scaling":
			let scal = inputValue.split(",");
			global.objetSelected.scaling = new BABYLON.Vector3(parseFloat(scal[0]), parseFloat(scal[1]), parseFloat(scal[2]));
		break;
		case "portailNameStart":
			global.objetSelected.name = inputValue;
			$.ajax({type: "POST", url: './PHP/save_property.php', data: "property=portail&root=_Projects/"+projet_name+"/game data/&portailName="+inputValue+"&objetName="+global.objetSelected.name});
		break;
		case "portailStartToPortailEnd":
			global.objetSelected.metadata.goToPortailName = inputValue;
			$.ajax({type: "POST", url: './PHP/save_property.php', data: "property=portail&root=_Projects/"+projet_name+"/game data/&portailStartToPortailEnd="+inputValue+"&objetName="+global.objetSelected.name});
		break;
		case "zoneOfPortail":
			global.objetSelected.metadata.goToZoneName = inputValue;
			$.ajax({type: "POST", url: './PHP/save_property.php', data: "property=portail&root=_Projects/"+projet_name+"/game data/&zoneOfPortail="+inputValue+"&objetName="+global.objetSelected.name});
		break;
		case "soundPortailMP3":
			global.objetSelected.metadata.mp3 = inputValue;
			$.ajax({type: "POST", url: './PHP/save_property.php', data: "property=portail&root=_Projects/"+projet_name+"/game data/&soundPortailMp3="+inputValue+"&objetName="+global.objetSelected.name});
		break;
		case "soundPortailOGG":
			global.objetSelected.metadata.ogg = inputValue;
			$.ajax({type: "POST", url: './PHP/save_property.php', data: "property=portail&root=_Projects/"+projet_name+"/game data/&soundPortailOgg="+inputValue+"&objetName="+global.objetSelected.name});
		break;
		case "soundPortailWAV":
			global.objetSelected.metadata.wav = inputValue;
			$.ajax({type: "POST", url: './PHP/save_property.php', data: "property=portail&root=_Projects/"+projet_name+"/game data/&soundPortailWav="+inputValue+"&objetName="+global.objetSelected.name});
		break;
	}
};

var changePropertyDeclancheur = function(propType)
{
	let inputValue = $("#HE_"+propType).val();
	switch(propType) {
		case "position":
			let pos = inputValue.split(",");
			global.objetSelected.position = new BABYLON.Vector3(parseFloat(pos[0]), parseFloat(pos[1]), parseFloat(pos[2]));
		break;
		case "rotation":
			let rot = inputValue.split(",");
			global.objetSelected.rotation = new BABYLON.Vector3(parseFloat(rot[0]), parseFloat(rot[1]), parseFloat(rot[2]));
		break;
		case "scaling":
			let scal = inputValue.split(",");
			global.objetSelected.scaling = new BABYLON.Vector3(parseFloat(scal[0]), parseFloat(scal[1]), parseFloat(scal[2]));
		break;
		case "scriptTrigger":
			global.objetSelected.metadata.scriptName = inputValue;
			$.ajax({type: "POST", url: './PHP/save_property.php', data: "property=trigger&root=_Projects/"+projet_name+"/game data/&script="+inputValue+"&objetName="+global.objetSelected.name});
		break;
	}
};

var loadDataProperty = function(groupProperty)
{
	let dataPropertyGrid = $('#propGrid').PropertyGrid('get'), entity = null;
	if(global.objetSelected) entity = global.objets[global.objetSelected.name];
	switch(groupProperty) {
		case "Portail", "CollisionBox", "Declancheur", "Sound", "Video", "Maillage":
			entity = global.objetSelected;
		break;
		case "Materiel":
			entity = global.selectMaterial;
			$(".pgGroupRow_Transformation,.pgRow_Transformation").hide();
		break;
		case "Balisage":
			entity = global.objetSelected;
			$("#pgCell-left-rotation, #pgCell-right-rotation, #pgCell-left-scaling, #pgCell-right-scaling").hide();
		break;
		case "Water":
			entity = global.water;
			$("#pgCell-left-rotation, #pgCell-right-rotation").hide();
		break;
		case "Light":
			if(global.lightSpotSelected.name) entity = global.LightSpot[global.lightSpotSelected.name];
			else if(global.lightPointSelected.name) entity = global.LightPoint[global.lightPointSelected.name];
		break;
	}

	$.each(dataPropertyGrid, function(index, value) {
		if(index != "type" && entity) {
			if($('#HE_'+index).attr("type") == "number" || $('#HE_'+index).attr("type") == "text" || $('#HE_'+index).attr("type") == "button") {
				// Peut être un bug de confusion entre autoload et des maillages perso: a verifier
				if(index == "lod1") {
					if(global.objetSelected.getLOD()._LODLevels.length > -1) {
						$('#HE_'+index).val(global.objetSelected.name+"_lod1");
					}
				} else if(index == "lod2") {
					if(global.objetSelected.getLOD()._LODLevels.length > -1) {
						$('#HE_'+index).val(global.objetSelected.name+"_lod2");
					}
				} else if(index == "lod3") {
					if(global.objetSelected.getLOD()._LODLevels.length > -1) {
						$('#HE_'+index).val(global.objetSelected.name+"_lod3");
					}
				}
				if(index == "position" || index == "rotation" || index == "scaling" || index == "direction") {
					if(entity[index]) {
						let valIndex = entity[index].x+","+entity[index].y+","+entity[index].z;
						$('#HE_'+index).val(valIndex);
					}
				} else{
					$('#HE_'+index).val(entity[index]);
				}
			}
			if($('#HE_'+index).attr("type") == "file") {
				$('#HE_'+index).val(entity[index]);
				$('#HE_'+index).find("input").val(entity[index]);
			}
			if($('#HE_'+index).attr("type") == "checkbox") {
				if(index == "verrouiller") {
					if(global.objectsFreeze[global.objetSelected.name]) {
						$('#HE_'+index+ '').attr('selected', true);
					}
				}
				if(index == "autolod") {
					if(global.objetSelected.simplify.length >= 3) {
						$('#HE_'+index).attr('selected', true);
					} else {
						$('#HE_'+index).removeAttr('checked');
					}
				} else {
					// Peut être un bug ici: A verifier
					$('#HE_'+index).filter('[value='+String(entity[index])+']').attr('selected', true);
				}
			}
			if($('#HE_'+index).attr("class") == "he-color") {
				$('#HE_'+index).val(rgbToHex(entity[index]));
			}
		}
	});
};

var saveMaterial = function(name, material) {
	$.ajax({type: "POST", url: './PHP/save_materials.php', data: "nameMaterial="+name+"&valueMaterial="+material});
};

var loadAllMaterialCreated = function() {
	for(let i = 0; i < getListeMaterials().length; i++) {
		if(i > -1) {
			$('#HE_material').append("<option value='"+getListeMaterials()[i]+"'>"+getListeMaterials()[i]+"</option>"); // liste des materiels
		}
	}
	for(let i = 0; i < getListeParticules().length; i++) {
		if(i > -1) {
			$('#HE_particules').append("<option value='"+getListeParticules()[i]+"'>"+getListeParticules()[i]+"</option>"); // liste des particules
		}
	}
	for(let i = 0; i < getListeSounds("mp3").length; i++) {
		if(i > -1) {
			$('#HE_mp3').append("<option value='"+getListeSounds("mp3")[i]+"'>"+getListeSounds("mp3")[i]+"</option>"); // liste des musique mp3
		}
	}
	for(let i = 0; i < getListeSounds("ogg").length; i++) {
		if(i > -1) {
			$('#HE_ogg').append("<option value='"+getListeSounds("ogg")[i]+"'>"+getListeSounds("ogg")[i]+"</option>"); // liste des musique ogv
		}
	}
	for(let i = 0; i < getListeSounds("wav").length; i++) {
		if(i > -1) {
			$('#HE_wav').append("<option value='"+getListeSounds("wav")[i]+"'>"+getListeSounds("wav")[i]+"</option>"); // liste des musique wav
		}
	}
	for(let i = 0; i < getListeSounds("wav").length; i++) {
		if(i > -1) {
			$('#HE_soundPortailWav').append("<option value='"+getListeSounds("wav")[i]+"'>"+getListeSounds("wav")[i]+"</option>"); // liste des musique wav
		}
	}
	for(let i = 0; i < getListeSounds("ogg").length; i++) {
		if(i > -1) {
			$('#HE_soundPortailOgg').append("<option value='"+getListeSounds("ogg")[i]+"'>"+getListeSounds("ogg")[i]+"</option>"); // liste des musique ogv
		}
	}
	for(let i = 0; i < getListeSounds("mp3").length; i++) {
		if(i > -1) {
			$('#HE_soundPortailMp3').append("<option value='"+getListeSounds("mp3")[i]+"'>"+getListeSounds("mp3")[i]+"</option>"); // liste des musique mp3
		}
	}
	for(let i = 0; i < getListeVideos("mp4").length; i++) {
		if(i > -1) {
			$('#HE_mp4').append("<option value='"+getListeVideos("mp4")[i]+"'>"+getListeVideos("mp4")[i]+"</option>"); // liste des video mp4
		}
	}
	for(let i = 0; i < getListeVideos("ogv").length; i++) {
		if(i > -1) {
			$('#HE_ogv').append("<option value='"+getListeVideos("ogv")[i]+"'>"+getListeVideos("ogv")[i]+"</option>"); // liste des video ogv
		}
	}
	for(let i = 0; i < getListeVideos("webm").length; i++) {
		if(i > -1) {
			$('#HE_webm').append("<option value='"+getListeVideos("webm")[i]+"'>"+getListeVideos("webm")[i]+"</option>"); // liste des video webm
		}
	}
};

var getListeSounds = function(format) {
	$.ajaxSetup({ async: false});
	let listes = [];
	$.ajax({type: "POST", url: './PHP/listeSounds.php', data: "format="+format,
		success: function(mess) {
			for(let i = 0; i < mess.length; i++){
				listes.push(mess[i]);
			}
		}
	});
	$.ajaxSetup({ async: true});
	return listes;
};

var getListeVideos = function(format) {
	$.ajaxSetup({ async: false});
	let listes = [];
	$.ajax({type: "POST", url: './PHP/listeVideos.php', data: "format="+format,
		success: function(mess) {
			for(let i = 0; i < mess.length; i++){
				listes.push(mess[i]);
			}
		}
	});
	$.ajaxSetup({ async: true});
	return listes;
};

var getListeParticules = function() {
	$.ajaxSetup({ async: false});
	let listes = [];
	$.ajax({type: "POST", url: './PHP/listeParticles.php', data: "listeForProperty=1",
		success: function(mess) {
			for(let i = 0; i < mess.length; i++){
				listes.push(mess[i]);
			}
		}
	});
	$.ajaxSetup({ async: true});
	return listes;
};

var getListeMaterials = function() {
	$.ajaxSetup({ async: false});
	let listes = [];
	$.ajax({type: "POST", url: './PHP/listeMaterials.php',
		success: function(mess) {
			for(let i = 0; i < mess.length; i++){
				listes.push(mess[i]);
			}
		}
	});
	$.ajaxSetup({ async: true});
	return listes;
};

$(function(){
	let property = createProperty();
	$('#propGrid').PropertyGrid(property.Obj, property.Meta);
	hideAllProperty();
	loadAllMaterialCreated();
});

var hideAllProperty = function() { $(".PGGrid").hide(); };