/*##################################################
 *                              manipulator.php
 *                            -------------------
 *   Revision                 : 28/07/2017
 ###################################################
 */
class Manipulator {
	
	constructor(mesh, camera, canvas, scale = 1.0)
	{			
		this.local = true;
		this.snapT = false;
		this.snapR = false;
		this.transSnap = 1;
		this.rotSnap = Math.PI / 18;
		this.axesLen = 0.4;		
		this.visibility = 0.7;
		this.pDown = false;
		this.pointerIsOver = false;
		this.editing = false;
		this.snapRX = 0;
		this.snapRY = 0;
		this.snapRZ = 0;
		this.snapTV = BABYLON.Vector3.Zero();
		this.transBy = BABYLON.Vector3.Zero();
		this.snapS = false;
		this.snapSX = 0;
		this.snapSY = 0;
		this.snapSZ = 0;
		this.snapSA = 0;
		this.snapSV = BABYLON.Vector3.Zero();
		this.scaleSnap = 0.25;
		this.scale = BABYLON.Vector3.Zero();
		this.eulerian = false;
		this.snapRA = 0;
		this.transEnabled = false;
		this.rotEnabled = false;
		this.scaleEnabled = false;
		this.localX = BABYLON.Vector3.Zero();
		this.localY = BABYLON.Vector3.Zero();
		this.localZ = BABYLON.Vector3.Zero();
		this.distFromCamera = 2;
		this.toParent = BABYLON.Vector3.Zero();
		this.cameraNormal = BABYLON.Vector3.Zero();
		this.mesh = mesh;
		this.canvas = canvas;
		this.axesScale = scale;
		this.scene = mesh.getScene();
		this.mainCamera = camera;
		this.actHist = new ActHist(mesh, 10);
		this.mesh.computeWorldMatrix(true);
		this.theParent = new BABYLON.Mesh("Manipulator", this.scene);
		this.mesh.getAbsolutePivotPointToRef(this.theParent.position);
		this.theParent.rotationQuaternion = mesh.rotationQuaternion;
		this.theParent.visibility = 0;
		this.theParent.isPickable = false;
		this.createMaterials(this.scene);
		this.createGuideAxes();
		this.guideCtl.parent = this.theParent;
		this.createPickPlane();
		this.pickPlanes.parent = this.theParent;
		this.pointerdown = (evt) => { this.onPointerDown(evt); };
		this.pointerup = (evt) => { this.onPointerUp(evt); };
		this.pointermove = (evt) => { this.onPointerMove(evt); };
		this.canvas.addEventListener("mousedown", this.pointerdown, false);
		this.canvas.addEventListener("mouseup", this.pointerup, false);
		this.canvas.addEventListener("mousemove", this.pointermove, false);
		this.setLocalAxes(mesh);
		this.renderer = () => { this.renderLoopProcess(); };
		this.scene.registerBeforeRender(this.renderer);
	};

	renderLoopProcess()
	{
		this.setAxesScale();
		this.mesh.getAbsolutePivotPointToRef(this.theParent.position);
		this.onPointerOver();
	}

	switchTo(mesh) 
	{
		this.mesh = mesh;
		this.mesh.computeWorldMatrix(true);		
		this.theParent.rotationQuaternion = this.mesh.rotationQuaternion;
		this.setLocalAxes(this.mesh);
		this.actHist = new ActHist(this.mesh, 10);
	}

	setUndoCount(c)
	{
		this.actHist.setCapacity(c);
	}

	undo() 
	{
		this.actHist.undo();
		this.meshPicked.computeWorldMatrix(true);
		this.setLocalAxes(this.meshPicked);
	}

	redo()
	{
		this.actHist.redo();
		this.meshPicked.computeWorldMatrix(true);
		this.setLocalAxes(this.meshPicked);
	}

	detach() 
	{
		this.canvas.removeEventListener("pointerdown", this.onPointerDown);
		this.canvas.removeEventListener("pointerup", this.onPointerUp);
		this.canvas.removeEventListener("pointermove", this.onPointerMove);
		this.scene.unregisterBeforeRender(this.renderer);
        this.disposeAll();
	}

	disposeAll()
	{
		this.theParent.dispose();
		this.disposeMaterials();
		this.actHist = null;
	}

	isEditing()
	{
		return this.editing;
	}
	
	isChanging()
	{
        return this.changing;
    }

	isPointerOver()
	{
		return this.pointerIsOver;
	}
	
	detachControl(camera, canvas) 
	{		
		camera.detachControl(canvas);
	}

	onPointerOver()
	{
		if(this.pDown) { return; }
		let pickResult = this.scene.pick(this.scene.pointerX, this.scene.pointerY, (mesh) => {
			if(this.transEnabled) {
				if((mesh == this.tX) || (mesh == this.tY) || (mesh == this.tZ) || (mesh == this.tXZ) || (mesh == this.tZY) || (mesh == this.tYX) || (mesh == this.tAll)) {
					return true;
				}
			} else if(this.rotEnabled) {
				if((mesh == this.rX) || (mesh == this.rY) || (mesh == this.rZ) || (mesh == this.rAll)) {
					return true;
				}
			} else if(this.scaleEnabled) {
				if((mesh == this.sX) || (mesh == this.sY) || (mesh == this.sZ) || (mesh == this.sXZ) || (mesh == this.sZY) || (mesh == this.sYX) || (mesh == this.sAll)) {
					return true;
				}
			}
			return false;
		}, null, this.mainCamera);
		if(pickResult.hit) {
			if(pickResult.pickedMesh != this.prevOverMesh) {
				this.pointerIsOver = true;
				if(this.prevOverMesh != null) {
					this.prevOverMesh.visibility = 0;
					this.restoreColor(this.prevOverMesh);
				}
				this.prevOverMesh = pickResult.pickedMesh;
				if(this.rotEnabled) {
					this.savedCol = this.prevOverMesh.getChildren()[0].color;
					this.prevOverMesh.getChildren()[0].color = BABYLON.Color3.White();
				} else {
					let childs = this.prevOverMesh.getChildren();
					if(childs.length > 0) {
						this.savedMat = childs[0].material;
						childs[0].material = this.whiteMat;
					} else {
						this.savedMat = this.prevOverMesh.material;
						this.prevOverMesh.material = this.whiteMat;
					}
				}
				if(this.prevOverMesh.name == "X") {
					this.xaxis.color = BABYLON.Color3.White();
				} else if(this.prevOverMesh.name == "Y") {
					this.yaxis.color = BABYLON.Color3.White();
				} else if(this.prevOverMesh.name == "Z") {
					this.zaxis.color = BABYLON.Color3.White();
				}
			}
		} else {
			this.pointerIsOver = false;
			if(this.prevOverMesh != null) {
				this.restoreColor(this.prevOverMesh);
				this.prevOverMesh = null;
			}
		}
	}

	onPointerDown(evt)
	{
		this.pDown = true;
		if(evt.button != 0) return;
		let pickResult = this.scene.pick(this.scene.pointerX, this.scene.pointerY, (mesh) => {
			if(this.transEnabled) {
				if((mesh == this.tX) || (mesh == this.tY) || (mesh == this.tZ) || (mesh == this.tXZ) || (mesh == this.tZY) || (mesh == this.tYX) || (mesh == this.tAll)) {
					return true;
				}
			} else if(this.rotEnabled) {
				if((mesh == this.rX) || (mesh == this.rY) || (mesh == this.rZ) || (mesh == this.rAll)) {
					return true;
				}
			} else if((this.scaleEnabled)) {
				if((mesh == this.sX) || (mesh == this.sY) || (mesh == this.sZ) || (mesh == this.sXZ) || (mesh == this.sZY) || (mesh == this.sYX) || (mesh == this.sAll)) {
					return true;
				}
			}
			return false;
		}, null, this.mainCamera);
		if(pickResult.hit) {
			this.axisPicked = pickResult.pickedMesh;
			let childs = this.axisPicked.getChildren();
			if(childs.length > 0) {
				childs[0].visibility = this.visibility;
			} else {
				this.axisPicked.visibility = this.visibility;
			}
			let name = this.axisPicked.name;
			if((name == "X")) {
				this.bXaxis.visibility = 1;
			} else if((name == "Y")) {
				this.bYaxis.visibility = 1;
			} else if((name == "Z")) {
				this.bZaxis.visibility = 1;
			} else if((name == "XZ")) {
				this.bXaxis.visibility = 1;
				this.bZaxis.visibility = 1;
			} else if((name == "ZY")) {
				this.bZaxis.visibility = 1;
				this.bYaxis.visibility = 1;
			} else if((name == "YX")) {
				this.bYaxis.visibility = 1;
				this.bXaxis.visibility = 1;
			} else if((name == "ALL")) {
				this.bXaxis.visibility = 1;
				this.bYaxis.visibility = 1;
				this.bZaxis.visibility = 1;
			}
			this.editing = true;
			this.pickPlane = this.getPickPlane(this.axisPicked);
			this.prevPos = this.getPosOnPickPlane();
			window.setTimeout(((cam, can) => { this.detachControl(cam, can); }), 1, this.mainCamera, this.canvas);
		}
	}

	onPointerMove(evt) 
	{
		if(!this.pDown || !this.editing) { return; }
		this.pickPlane = this.getPickPlane(this.axisPicked);
		let newPos = this.getPosOnPickPlane();
		if(newPos == null) { return; }
		let diff = newPos.subtract(this.prevPos);
		if(diff.x == 0 && diff.y == 0 && diff.z == 0) { return; }
		if(this.transEnabled) { this.doTranslation(diff); }
		if(this.scaleEnabled && this.local) { this.doScaling(diff); }
		if(this.rotEnabled) { this.doRotation(this.mesh, this.axisPicked, newPos); }
		this.prevPos = newPos;
	}

	onPointerUp(evt) 
	{
		this.pDown = false;
		if((this.editing)) {
			this.mainCamera.attachControl(this.canvas);
			this.editing = false;
			this.hideBaxis();
			this.restoreColor(this.prevOverMesh);
			this.prevOverMesh = null;
			this.actHist.add();
		}
	}

	restoreColor(mesh)
	{
		switch(mesh.name) {
			case "X":
				this.xaxis.color = BABYLON.Color3.Red();
			break;
			case "Y":
				this.yaxis.color = BABYLON.Color3.Green();
			break;
			case "Z":
				this.zaxis.color = BABYLON.Color3.Blue();
			break;
		}
		if(this.rotEnabled) {
			mesh.getChildren()[0].color = this.savedCol;
		} else {
			let childs = mesh.getChildren();
			if(childs.length > 0) {
				childs[0].material = this.savedMat;
			} else {
				mesh.material = this.savedMat;
			}
		}
	}

	doTranslation(diff)
	{
		this.transBy.x = 0;
		this.transBy.y = 0;
		this.transBy.z = 0;
		let n = this.axisPicked.name;
		if((n == "X") || (n == "XZ") || (n == "YX") || (n == "ALL")) {
			if(this.local) {
				this.transBy.x = BABYLON.Vector3.Dot(diff, this.localX) / (this.localX.length() * this.mesh.scaling.x);
			} else {
				this.transBy.x = diff.x;
			}
		}
		if((n == "Y") || (n == "ZY") || (n == "YX") || (n == "ALL")) {
			if(this.local) {
				this.transBy.y = BABYLON.Vector3.Dot(diff, this.localY) / (this.localY.length() * this.mesh.scaling.y);
			} else {
				this.transBy.y = diff.y;
			}
		}
		if((n == "Z") || (n == "XZ") || (n == "ZY") || (n == "ALL")) {
			if(this.local) {
				this.transBy.z = BABYLON.Vector3.Dot(diff, this.localZ) / (this.localZ.length() * this.mesh.scaling.z);
			} else {
				this.transBy.z = diff.z;
			}
		}
		this.transWithSnap(this.mesh, this.transBy, this.local);
		this.mesh.computeWorldMatrix(true);
	}
	
	transWithSnap(mesh, trans, local) {
		if(this.snapT) {
			let snapit = false;
			this.snapTV.addInPlace(trans);
			if(Math.abs(this.snapTV.x) > (this.tSnap.x / mesh.scaling.x)) {
				if(this.snapTV.x > 0) {
					trans.x = this.tSnap.x;
				} else {
					trans.x = -this.tSnap.x;
				}
				trans.x = trans.x / mesh.scaling.x;
				snapit = true;
			}
			if(Math.abs(this.snapTV.y) > (this.tSnap.y / mesh.scaling.y)) {
				if(this.snapTV.y > 0) {
					trans.y = this.tSnap.y;
				} else {
					trans.y = -this.tSnap.y;
				}
				trans.y = trans.y / mesh.scaling.y;
				snapit = true;
			}
			if(Math.abs(this.snapTV.z) > (this.tSnap.z / mesh.scaling.z)) {
				if(this.snapTV.z > 0) {
					trans.z = this.tSnap.z;
				} else {
					trans.z = -this.tSnap.z;
				}
				trans.z = trans.z / mesh.scaling.z;
				snapit = true;
			}
			if(!snapit) { return; }
			if(Math.abs(trans.x) !== this.tSnap.x / mesh.scaling.x) {
				trans.x = 0;
			}
			if(Math.abs(trans.y) !== this.tSnap.y / mesh.scaling.y) {
				trans.y = 0;
			}
			if(Math.abs(trans.z) !== this.tSnap.z / mesh.scaling.z) {
				trans.z = 0;
			}
			BABYLON.Vector3.FromFloatsToRef(0, 0, 0, this.snapTV);
			snapit = false;
		}
		if(local) {
			this.mesh.locallyTranslate(trans);
		} else {
			this.mesh.position.addInPlace(trans);
		}
	}

	doScaling(diff)
	{
		this.scale.x = 0;
		this.scale.y = 0;
		this.scale.z = 0;
		let n = this.axisPicked.name;
		if((n == "X") || (n == "XZ") || (n == "YX") || (n == "ALL")) {
			this.scale.x = BABYLON.Vector3.Dot(diff, this.localX) / this.localX.length();
		}
		if((n == "Y") || (n == "ZY") || (n == "YX") || (n == "ALL")) {
			this.scale.y = BABYLON.Vector3.Dot(diff, this.localY) / this.localY.length();
		}
		if((n == "Z") || (n == "XZ") || (n == "ZY") || (n == "ALL")) {
			this.scale.z = BABYLON.Vector3.Dot(diff, this.localZ) / this.localZ.length();
		}
		if(n == "ALL") {
			this.scale.copyFromFloats(this.scale.y, this.scale.y, this.scale.y);
		}
		this.scaleWithSnap(this.mesh, this.scale);
	}
	
	scaleWithSnap(mesh, p)
	{
		if(this.snapS) {
			let snapit = false;
			this.snapSV.addInPlace(p);
			if(Math.abs(this.snapSV.x) > this.scaleSnap) {
				if(p.x > 0) {
					p.x = this.scaleSnap;
				} else {
					p.x = -this.scaleSnap;
				}
				snapit = true;
			}
			if(Math.abs(this.snapSV.y) > this.scaleSnap) {
				if(p.y > 0) {
					p.y = this.scaleSnap;
				} else {
					p.y = -this.scaleSnap;
				}
				snapit = true;
			}
			if(Math.abs(this.snapSV.z) > this.scaleSnap) {
				if(p.z > 0) {
					p.z = this.scaleSnap;
				} else {
					p.z = -this.scaleSnap;
				}
				snapit = true;
			}
			if(!snapit) { return; }
			if((Math.abs(p.x) !== this.scaleSnap) && (p.x !== 0)) {
				p.x = 0;
			}
			if((Math.abs(p.y) !== this.scaleSnap) && (p.y !== 0)) {
				p.y = 0;
			}
			if((Math.abs(p.z) !== this.scaleSnap) && (p.z !== 0)) {
				p.z = 0;
			}
			BABYLON.Vector3.FromFloatsToRef(0, 0, 0, this.snapSV);
			snapit = false;
		}
		mesh.scaling.addInPlace(p);
	}

	doRotation(mesh, axis, newPos)
	{		
		let cN = BABYLON.Vector3.TransformNormal(BABYLON.Axis.Z, this.mainCamera.getWorldMatrix());
		let angle = Manipulator.getAngle(this.prevPos, newPos, mesh.getAbsolutePivotPoint(), cN);
		if(axis == this.rX) {
			if(this.snapR) {
				this.snapRX += angle;
				angle = 0;
				if(Math.abs(this.snapRX) >= this.rotSnap) {
					if((this.snapRX > 0)) {
						angle = this.rotSnap;
					} else {
						angle = -this.rotSnap;
					}
					this.snapRX = 0;
				}
			}
			if(angle !== 0) {
				if(this.local) {
					if(BABYLON.Vector3.Dot(this.localX, cN) < 0) {
						angle = -1 * angle;
					}
					mesh.rotate(BABYLON.Axis.X, angle, BABYLON.Space.LOCAL);
				} else {
					mesh.rotate(new BABYLON.Vector3(cN.x, 0, 0), angle, BABYLON.Space.WORLD);
				}
			}
		}
		else if(axis == this.rY) {
			if(this.snapR) {
				this.snapRY += angle;
				angle = 0;
				if(Math.abs(this.snapRY) >= this.rotSnap) {
					if((this.snapRY > 0)) {
						angle = this.rotSnap;
					} else {
						angle = -this.rotSnap;
					}	
					this.snapRY = 0;
				}
			}
			if(angle !== 0) {
				if(this.local) {
					if(BABYLON.Vector3.Dot(this.localY, cN) < 0) {
						angle = -1 * angle;
					}
					mesh.rotate(BABYLON.Axis.Y, angle, BABYLON.Space.LOCAL);
				} else {
					mesh.rotate(new BABYLON.Vector3(0, cN.y, 0), angle, BABYLON.Space.WORLD);
				}
			}
		}
		else if(axis == this.rZ) {
			if(this.snapR) {
				this.snapRZ += angle;
				angle = 0;
				if(Math.abs(this.snapRZ) >= this.rotSnap) {
					if(this.snapRZ > 0) {
						angle = this.rotSnap;
					} else {
						angle = -this.rotSnap;
					}
					this.snapRZ = 0;
				}
			}
			if(angle !== 0) {
				if(this.local) {
					if(BABYLON.Vector3.Dot(this.localZ, cN) < 0) {
						angle = -1 * angle;
					}
					mesh.rotate(BABYLON.Axis.Z, angle, BABYLON.Space.LOCAL);
				} else {
					mesh.rotate(new BABYLON.Vector3(0, 0, cN.z), angle, BABYLON.Space.WORLD);
				}
			}
		}
		else if(axis == this.rAll) {
			if(this.snapR) {
				this.snapRA += angle;
				angle = 0;
				if(Math.abs(this.snapRA) >= this.rotSnap) {
					if(this.snapRA > 0) {
						angle = this.rotSnap;
					} else {
						angle = -this.rotSnap;
					}
					this.snapRA = 0;
				}
			}
			if(angle !== 0) {
				mesh.rotate(mesh.position.subtract(this.mainCamera.position), angle, BABYLON.Space.WORLD);
			}
		}
		this.setLocalAxes(this.mesh);
		if((this.eulerian)) {
			mesh.rotation = mesh.rotationQuaternion.toEulerAngles();
			mesh.rotationQuaternion = null;
		}
	}
	
	getPickPlane(axis)
	{
		let n = axis.name;
		if(this.transEnabled || this.scaleEnabled) {
			if(n == "XZ") {
				return this.pXZ;
			} else if(n == "ZY") {
				return this.pZY;
			} else if(n == "YX") {
				return this.pYX;
			} else if(n == "ALL") {
				return this.pALL;
			} else {
				let invMat = this.mesh.getWorldMatrix().clone().invert();
				let c = BABYLON.Vector3.TransformCoordinates(this.mainCamera.position, invMat);
				let s = this.mesh.scaling;
				if(n === "X") {
					if(Math.abs(c.y * s.y) > Math.abs(c.z * s.z)) {
						return this.pXZ;
					} else {
						return this.pYX;
					}
				}
				else if(n === "Z") {
					if(Math.abs(c.y * s.y) > Math.abs(c.x * s.x)) {
						return this.pXZ;
					} else {
						return this.pZY;
					}
				}
				else if(n === "Y") {
					if(Math.abs(c.z * s.z) > Math.abs(c.x * s.x)) {
						return this.pYX;
					} else {
						return this.pZY;
					}
				}
			}
		} else if(this.rotEnabled) {
			switch(n) {
				case "X":
					return this.pZY;
				case "Y":
					return this.pXZ;
				case "Z":
					return this.pYX;
				default:
					return this.pALL;
			}
		} else {
			return null;
		}
	}
	
	getPosOnPickPlane() 
	{
		let pickinfo = this.scene.pick(this.scene.pointerX, this.scene.pointerY, (mesh) => {
			return mesh === this.pickPlane;
		}, null, this.mainCamera);
		if(pickinfo.hit) {
			return pickinfo.pickedPoint;
		} else {
			return null;
		}
	}

	hideBaxis() 
	{
		this.bXaxis.visibility = 0;
		this.bYaxis.visibility = 0;
		this.bZaxis.visibility = 0;
	}

	setAxesVisiblity(v)
	{
		if(this.transEnabled) {
			this.tEndX.visibility = v;
			this.tEndY.visibility = v;
			this.tEndZ.visibility = v;
		}
		if(this.rotEnabled) {
			this.rEndX.visibility = v;
			this.rEndY.visibility = v;
			this.rEndZ.visibility = v;
		}
		if(this.scaleEnabled) {
			this.sEndX.visibility = v;
			this.sEndY.visibility = v;
			this.sEndZ.visibility = v;
			this.sEndAll.visibility = v;
		}
	}

	isTranslationEnabled()
	{
		return this.transEnabled;
	}	

	enableTranslation() 
	{
		if(this.tX == null) {
			this.createTransAxes();
			this.tCtl.parent = this.theParent;
		}
		if(!this.transEnabled) {
			this.tEndX.visibility = this.visibility;
			this.tEndY.visibility = this.visibility;
			this.tEndZ.visibility = this.visibility;
			this.tEndXZ.visibility = this.visibility;
			this.tEndZY.visibility = this.visibility;
			this.tEndYX.visibility = this.visibility;
			this.tEndAll.visibility = this.visibility;
			this.transEnabled = true;
			this.disableRotation();
			this.disableScaling();
		}		
	}

	disableTranslation() 
	{
		if(this.transEnabled) {
			this.tEndX.visibility = 0;
			this.tEndY.visibility = 0;
			this.tEndZ.visibility = 0;
			this.tEndXZ.visibility = 0;
			this.tEndZY.visibility = 0;
			this.tEndYX.visibility = 0;
			this.tEndAll.visibility = 0;
			this.transEnabled = false;
		}
	}

	isRotationEnabled()
	{
		return this.rotEnabled;
	}
	
	returnEuler(euler)
	{
        this.eulerian = euler;
    }

	enableRotation() 
	{
		if(this.rX == null) {
			this.createRotAxes();
			this.rCtl.parent = this.theParent;
		}
		if(!this.rotEnabled) {
			this.rEndX.visibility = this.visibility;
			this.rEndY.visibility = this.visibility;
			this.rEndZ.visibility = this.visibility;
			this.rEndAll.visibility = this.visibility;
			this.rotEnabled = true;
			this.disableTranslation();
			this.disableScaling();
		}
	}

	disableRotation() 
	{
		if(this.rotEnabled) {
			this.rEndX.visibility = 0;
			this.rEndY.visibility = 0;
			this.rEndZ.visibility = 0;
			this.rEndAll.visibility = 0;
			this.rotEnabled = false;
		}
	}

	isScaleEnabled()
	{
		return this.scaleEnabled;
	}

	enableScaling() 
	{
		if(this.sX == null) {
			this.createScaleAxes();
			this.sCtl.parent = this.theParent;
		}
		if(!this.scaleEnabled) {
			this.sEndX.visibility = this.visibility;
			this.sEndY.visibility = this.visibility;
			this.sEndZ.visibility = this.visibility;
			this.sEndXZ.visibility = this.visibility;
			this.sEndZY.visibility = this.visibility;
			this.sEndYX.visibility = this.visibility;
			this.sEndAll.visibility = this.visibility;
			this.scaleEnabled = true;
			this.disableTranslation();
			this.disableRotation();
		}
	}

	disableScaling()
	{
		if(this.scaleEnabled) {
			this.sEndX.visibility = 0;
			this.sEndY.visibility = 0;
			this.sEndZ.visibility = 0;
			this.sEndXZ.visibility = 0;
			this.sEndZY.visibility = 0;
			this.sEndYX.visibility = 0;
			this.sEndAll.visibility = 0;
			this.scaleEnabled = false;
		}
	}

	createGuideAxes()
	{
		this.guideCtl = new BABYLON.Mesh("guideCtl", this.scene);
		this.bXaxis = BABYLON.Mesh.CreateLines("bxBABYLON.Axis", [new BABYLON.Vector3(-100, 0, 0), new BABYLON.Vector3(100, 0, 0)], this.scene);
		this.bYaxis = BABYLON.Mesh.CreateLines("byBABYLON.Axis", [new BABYLON.Vector3(0, -100, 0), new BABYLON.Vector3(0, 100, 0)], this.scene);
		this.bZaxis = BABYLON.Mesh.CreateLines("bzBABYLON.Axis", [new BABYLON.Vector3(0, 0, -100), new BABYLON.Vector3(0, 0, 100)], this.scene);
		this.bXaxis.isPickable = false;
		this.bYaxis.isPickable = false;
		this.bZaxis.isPickable = false;
		this.bXaxis.parent = this.guideCtl;
		this.bYaxis.parent = this.guideCtl;
		this.bZaxis.parent = this.guideCtl;
		this.bXaxis.color = BABYLON.Color3.Red();
		this.bYaxis.color = BABYLON.Color3.Green();
		this.bZaxis.color = BABYLON.Color3.Blue();
		this.hideBaxis();
		let al = (this.axesLen * this.axesScale);
		this.xaxis = BABYLON.Mesh.CreateLines("xBABYLON.Axis", [BABYLON.Vector3.Zero(), new BABYLON.Vector3(al, 0, 0)], this.scene);
		this.yaxis = BABYLON.Mesh.CreateLines("yBABYLON.Axis", [BABYLON.Vector3.Zero(), new BABYLON.Vector3(0, al, 0)], this.scene);
		this.zaxis = BABYLON.Mesh.CreateLines("zBABYLON.Axis", [BABYLON.Vector3.Zero(), new BABYLON.Vector3(0, 0, al)], this.scene);
		this.xaxis.isPickable = false;
		this.yaxis.isPickable = false;
		this.zaxis.isPickable = false;
		this.xaxis.parent = this.guideCtl;
		this.yaxis.parent = this.guideCtl;
		this.zaxis.parent = this.guideCtl;
		this.xaxis.color = BABYLON.Color3.Red();
		this.yaxis.color = BABYLON.Color3.Green();
		this.zaxis.color = BABYLON.Color3.Blue();
		this.xaxis.renderingGroupId = 2;
		this.yaxis.renderingGroupId = 2;
		this.zaxis.renderingGroupId = 2;
	}

	createPickPlane()
	{
		this.pALL = BABYLON.Mesh.CreatePlane("pALL", 5, this.scene);
		this.pXZ = BABYLON.Mesh.CreatePlane("pXZ", 5, this.scene);
		this.pZY = BABYLON.Mesh.CreatePlane("pZY", 5, this.scene);
		this.pYX = BABYLON.Mesh.CreatePlane("pYX", 5, this.scene);
		this.pALL.isPickable = false;
		this.pXZ.isPickable = false;
		this.pZY.isPickable = false;
		this.pYX.isPickable = false;
		this.pALL.visibility = 0;
		this.pXZ.visibility = 0;
		this.pZY.visibility = 0;
		this.pYX.visibility = 0;
		this.pALL.renderingGroupId = 1;
		this.pXZ.renderingGroupId = 1;
		this.pZY.renderingGroupId = 1;
		this.pYX.renderingGroupId = 1;
		this.pALL.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;
		this.pXZ.rotate(BABYLON.Axis.X, 1.57);
		this.pZY.rotate(BABYLON.Axis.Y, 1.57);
		this.pickPlanes = new BABYLON.Mesh("pickPlanes", this.scene);
		this.pALL.parent = this.theParent;
		this.pXZ.parent = this.pickPlanes;
		this.pZY.parent = this.pickPlanes;
		this.pYX.parent = this.pickPlanes;
	}

	createTransAxes()
	{
		let r = (0.04 * this.axesScale);
		let l = this.axesLen * this.axesScale;
		this.tCtl = new BABYLON.Mesh("tarnsCtl", this.scene);
		this.tX = this.extrudeBox(r / 2, l);
		this.tX.name = "X";
		this.tY = this.tX.clone("Y");
		this.tZ = this.tX.clone("Z");
		this.tXZ = BABYLON.MeshBuilder.CreatePlane("XZ", { size: r * 2 }, this.scene);
		this.tZY = this.tXZ.clone("ZY");
		this.tYX = this.tXZ.clone("YX");
		this.tXZ.rotation.x = 1.57;
		this.tZY.rotation.y = -1.57;
		this.tXZ.position.x = r;
		this.tXZ.position.z = r;
		this.tZY.position.z = r;
		this.tZY.position.y = r;
		this.tYX.position.y = r;
		this.tYX.position.x = r;
		this.tAll = BABYLON.Mesh.CreateBox("ALL", r * 2, this.scene);
		this.tX.parent = this.tCtl;
		this.tY.parent = this.tCtl;
		this.tZ.parent = this.tCtl;
		this.tXZ.parent = this.tCtl;
		this.tZY.parent = this.tCtl;
		this.tYX.parent = this.tCtl;
		this.tAll.parent = this.tCtl;
		this.tX.rotation.y = 1.57;
		this.tY.rotation.x -= 1.57;
		this.tX.visibility = 0;
		this.tY.visibility = 0;
		this.tZ.visibility = 0;
		this.tXZ.visibility = 0;
		this.tZY.visibility = 0;
		this.tYX.visibility = 0;
		this.tAll.visibility = 0;
		this.tX.renderingGroupId = 1;
		this.tY.renderingGroupId = 1;
		this.tZ.renderingGroupId = 1;
		this.tXZ.renderingGroupId = 1;
		this.tZY.renderingGroupId = 1;
		this.tYX.renderingGroupId = 1;
		this.tAll.renderingGroupId = 1;
		this.tX.isPickable = false;
		this.tY.isPickable = false;
		this.tZ.isPickable = false;
		this.tXZ.isPickable = false;
		this.tZY.isPickable = false;
		this.tYX.isPickable = false;
		this.tAll.isPickable = false;
		let cl = (l / 5);
		let cr = r;
		this.tEndX = BABYLON.Mesh.CreateCylinder("tEndX", cl, 0, cr, 6, 1, this.scene);
		this.tEndY = this.tEndX.clone("tEndY");
		this.tEndZ = this.tEndX.clone("tEndZ");
		this.tEndXZ = this.createTriangle("XZ", (cr * 1.75), this.scene);
		this.tEndZY = this.tEndXZ.clone("ZY");
		this.tEndYX = this.tEndXZ.clone("YX");
		this.tEndAll = BABYLON.MeshBuilder.CreatePolyhedron("tEndAll", { type: 1, size: cr / 2 }, this.scene);
		this.tEndX.rotation.x = 1.57;
		this.tEndY.rotation.x = 1.57;
		this.tEndZ.rotation.x = 1.57;
		this.tEndXZ.rotation.x = -1.57;
		this.tEndZY.rotation.x = -1.57;
		this.tEndYX.rotation.x = -1.57;
		this.tEndX.parent = this.tX;
		this.tEndY.parent = this.tY;
		this.tEndZ.parent = this.tZ;
		this.tEndXZ.parent = this.tXZ;
		this.tEndZY.parent = this.tZY;
		this.tEndYX.parent = this.tYX;
		this.tEndAll.parent = this.tAll;
		this.tEndX.position.z = l - (cl / 2);
		this.tEndY.position.z = l - (cl / 2);
		this.tEndZ.position.z = l - (cl / 2);
		this.tEndX.material = this.redMat;
		this.tEndY.material = this.greenMat;
		this.tEndZ.material = this.blueMat;
		this.tEndXZ.material = this.redMat;
		this.tEndZY.material = this.blueMat;
		this.tEndYX.material = this.greenMat;
		this.tEndAll.material = this.yellowMat;
		this.tEndX.renderingGroupId = 1;
		this.tEndY.renderingGroupId = 1;
		this.tEndZ.renderingGroupId = 1;
		this.tEndXZ.renderingGroupId = 1;
		this.tEndZY.renderingGroupId = 1;
		this.tEndYX.renderingGroupId = 1;
		this.tEndAll.renderingGroupId = 1;
		this.tEndX.isPickable = false;
		this.tEndY.isPickable = false;
		this.tEndZ.isPickable = false;
		this.tEndXZ.isPickable = false;
		this.tEndZY.isPickable = false;
		this.tEndYX.isPickable = false;
		this.tEndAll.isPickable = false;
	}
	
	createTriangle(name, w, scene)
	{
		let p = new BABYLON.Path2(w / 2, -w / 2).addLineTo(w / 2, w / 2).addLineTo(-w / 2, w / 2).addLineTo(w / 2, -w / 2);
		let s = new BABYLON.PolygonMeshBuilder(name, p, scene);
		let t = s.build();
		return t;
	}

	createRotAxes() 
	{
		let d = ((this.axesLen * this.axesScale) * 2);
		this.rCtl = new BABYLON.Mesh("rotCtl", this.scene);
		this.rX = this.createTube((d / 2), 90);
		this.rX.name = "X";
		this.rY = this.rX.clone("Y");
		this.rZ = this.rX.clone("Z");
		this.rAll = this.createTube((d / 1.75), 360);
		this.rAll.name = "ALL";
		this.rX.parent = this.rCtl;
		this.rY.parent = this.rCtl;
		this.rZ.parent = this.rCtl;
		this.rAll.parent = this.pALL;
		this.rX.rotation.z = 1.57;
		this.rZ.rotation.x = -1.57;
		this.rAll.rotation.x = 1.57;
		this.rX.visibility = 0;
		this.rY.visibility = 0;
		this.rZ.visibility = 0;
		this.rAll.visibility = 0;
		this.rX.renderingGroupId = 1;
		this.rY.renderingGroupId = 1;
		this.rZ.renderingGroupId = 1;
		this.rAll.renderingGroupId = 1;
		this.rX.isPickable = false;
		this.rY.isPickable = false;
		this.rZ.isPickable = false;
		this.rAll.isPickable = false;
		let cl = d;
		this.rEndX = this.createCircle((cl / 2), 90);
		this.rEndY = this.rEndX.clone("");
		this.rEndZ = this.rEndX.clone("");
		this.rEndAll = this.createCircle((cl / 1.75), 360);
		this.rEndX.parent = this.rX;
		this.rEndY.parent = this.rY;
		this.rEndZ.parent = this.rZ;
		this.rEndAll.parent = this.rAll;
		this.rEndX.color = BABYLON.Color3.Red();
		this.rEndY.color = BABYLON.Color3.Green();
		this.rEndZ.color = BABYLON.Color3.Blue();
		this.rEndAll.color = BABYLON.Color3.Yellow();
		this.rEndX.renderingGroupId = 1;
		this.rEndY.renderingGroupId = 1;
		this.rEndZ.renderingGroupId = 1;
		this.rEndAll.renderingGroupId = 1;
		this.rEndX.isPickable = false;
		this.rEndY.isPickable = false;
		this.rEndZ.isPickable = false;
		this.rEndAll.isPickable = false;
	}

	extrudeBox(w, l)
	{
		let shape = [new BABYLON.Vector3(w, w, 0), new BABYLON.Vector3(-w, w, 0), new BABYLON.Vector3(-w, -w, 0), new BABYLON.Vector3(w, -w, 0), new BABYLON.Vector3(w, w, 0)];
		let path = [BABYLON.Vector3.Zero(), new BABYLON.Vector3(0, 0, l)];
		let box = BABYLON.Mesh.ExtrudeShape("", shape, path, 1, 0, 2, this.scene);
		return box;
	}

	createCircle(r, t) 
	{
		if(t === null) { t = 360; }
		let points = [];
		let x;
		let z;
		let a = (3.14 / 180);
		let p = 0;
		for(let i = 0; i <= t; i = i + 10) {
			x = (r * Math.cos(i * a));
			if((i == 90)) {
				z = r;
			} else if(i == 270) {
				z = -r;
			} else {
				z = r * Math.sin(i * a);
			}
			points[p] = new BABYLON.Vector3(x, 0, z);
			p++;
		}
		let circle = BABYLON.Mesh.CreateLines("", points, this.scene);
		return circle;
	}
	
	createTube(r, t) {
		if(t === null) {
			t = 360;
		}
		let points = [];
		let x;
		let z;
		let a = (3.14 / 180);
		let p = 0;
		for(let i = 0; i <= t; i = i + 30) {
			x = (r * Math.cos(i * a));
			if(i == 90) {
				z = r;
			} else if(i == 270) {
				z = -r;
			} else {
				z = r * Math.sin(i * a);
			}
			points[p] = new BABYLON.Vector3(x, 0, z);
			p++;
		}
		let tube = BABYLON.Mesh.CreateTube("", points, 0.02, 3, null, BABYLON.Mesh.NO_CAP, this.scene);
		return tube;
	}

	createScaleAxes()
	{
		let r = (0.04 * this.axesScale);
		let l = this.axesLen * this.axesScale;
		this.sCtl = new BABYLON.Mesh("sCtl", this.scene);
		this.sX = this.extrudeBox(r / 2, l);
		this.sX.name = "X";
		this.sY = this.sX.clone("Y");
		this.sZ = this.sX.clone("Z");
		this.sXZ = BABYLON.MeshBuilder.CreatePlane("XZ", { size: (r * 2) }, this.scene);
		this.sZY = this.sXZ.clone("ZY");
		this.sYX = this.sXZ.clone("YX");
		this.sXZ.rotation.x = 1.57;
		this.sZY.rotation.y = -1.57;
		this.sXZ.position.x = r;
		this.sXZ.position.z = r;
		this.sZY.position.z = r;
		this.sZY.position.y = r;
		this.sYX.position.y = r;
		this.sYX.position.x = r;
		this.sAll = BABYLON.Mesh.CreateBox("ALL", (r * 2), this.scene);
		this.sX.material = this.redMat;
		this.sY.material = this.greenMat;
		this.sZ.material = this.blueMat;
		this.sAll.material = this.yellowMat;
		this.sX.parent = this.sCtl;
		this.sY.parent = this.sCtl;
		this.sZ.parent = this.sCtl;
		this.sAll.parent = this.sCtl;
		this.sXZ.parent = this.sCtl;
		this.sZY.parent = this.sCtl;
		this.sYX.parent = this.sCtl;
		this.sX.rotation.y = 1.57;
		this.sY.rotation.x -= 1.57;
		this.sX.visibility = 0;
		this.sY.visibility = 0;
		this.sZ.visibility = 0;
		this.sXZ.visibility = 0;
		this.sZY.visibility = 0;
		this.sYX.visibility = 0;
		this.sAll.visibility = 0;
		this.sX.renderingGroupId = 1;
		this.sY.renderingGroupId = 1;
		this.sZ.renderingGroupId = 1;
		this.sXZ.renderingGroupId = 1;
		this.sZY.renderingGroupId = 1;
		this.sYX.renderingGroupId = 1;
		this.sAll.renderingGroupId = 1;
		this.sX.isPickable = false;
		this.sY.isPickable = false;
		this.sZ.isPickable = false;
		this.sXZ.isPickable = false;
		this.sZY.isPickable = false;
		this.sYX.isPickable = false;
		this.sAll.isPickable = false;
		let cr = r;
		this.sEndX = BABYLON.Mesh.CreateBox("", cr, this.scene);
		this.sEndY = this.sEndX.clone("");
		this.sEndZ = this.sEndX.clone("");
		this.sEndAll = BABYLON.MeshBuilder.CreatePolyhedron("sEndAll", { type: 1, size: (cr / 2) }, this.scene);
		this.sEndXZ = this.createTriangle("XZ", (cr * 1.75), this.scene);
		this.sEndZY = this.sEndXZ.clone("ZY");
		this.sEndYX = this.sEndXZ.clone("YX");
		this.sEndXZ.rotation.x = -1.57;
		this.sEndZY.rotation.x = -1.57;
		this.sEndYX.rotation.x = -1.57;
		this.sEndX.parent = this.sX;
		this.sEndY.parent = this.sY;
		this.sEndZ.parent = this.sZ;
		this.sEndXZ.parent = this.sXZ;
		this.sEndZY.parent = this.sZY;
		this.sEndYX.parent = this.sYX;
		this.sEndAll.parent = this.sAll;
		this.sEndX.position.z = (l - cr / 2);
		this.sEndY.position.z = (l - cr / 2);
		this.sEndZ.position.z = (l - cr / 2);
		this.sEndX.material = this.redMat;
		this.sEndY.material = this.greenMat;
		this.sEndZ.material = this.blueMat;
		this.sEndXZ.material = this.redMat;
		this.sEndZY.material = this.blueMat;
		this.sEndYX.material = this.greenMat;
		this.sEndAll.material = this.yellowMat;
		this.sEndX.renderingGroupId = 1;
		this.sEndY.renderingGroupId = 1;
		this.sEndZ.renderingGroupId = 1;
		this.sEndXZ.renderingGroupId = 1;
		this.sEndZY.renderingGroupId = 1;
		this.sEndYX.renderingGroupId = 1;
		this.sEndAll.renderingGroupId = 1;
		this.sEndX.isPickable = false;
		this.sEndY.isPickable = false;
		this.sEndZ.isPickable = false;
		this.sEndXZ.isPickable = false;
		this.sEndZY.isPickable = false;
		this.sEndYX.isPickable = false;
		this.sEndAll.isPickable = false;
	}

	setLocalAxes(mesh)
	{
		let meshMatrix = mesh.getWorldMatrix();
		BABYLON.Vector3.FromFloatArrayToRef(meshMatrix.asArray(), 0, this.localX);
		BABYLON.Vector3.FromFloatArrayToRef(meshMatrix.asArray(), 4, this.localY);
		BABYLON.Vector3.FromFloatArrayToRef(meshMatrix.asArray(), 8, this.localZ);
	}

	setLocal(l)
	{
		if(this.local == l) { return; }
		this.local = l;
		if(this.local) {
			this.theParent.rotationQuaternion = this.mesh.rotationQuaternion;
		} else {
			this.theParent.rotationQuaternion = BABYLON.Quaternion.Identity();
		}
	}

	isLocal() 
	{
		return this.local;
	}

	setTransSnap(value)
	{
		this.snapT = value;
	}

	setRotSnap(value)
	{
		this.snapR = value;
	}
	
	setScaleSnap(value) 
	{
		this.snapS = s;
	}

	setTransSnapValue(value) 
	{
		this.tSnap = new Vector3(value, value, value);
		this.transSnap = value;
	}

	setRotSnapValue(value)
	{
		this.rotSnap = value;
	}
	
	setScaleSnapValue(value)
	{
        this.scaleSnap = value;
    }

	setAxesScale() 
	{
		this.theParent.position.subtractToRef(this.mainCamera.position, this.toParent);
		BABYLON.Vector3.FromFloatArrayToRef(this.mainCamera.getWorldMatrix().asArray(), 8, this.cameraNormal);
		let parentOnNormal = (BABYLON.Vector3.Dot(this.toParent, this.cameraNormal) / this.cameraNormal.length());
		let s = (parentOnNormal / this.distFromCamera);
		BABYLON.Vector3.FromFloatsToRef(s, s, s, this.theParent.scaling);
		BABYLON.Vector3.FromFloatsToRef(s, s, s, this.pALL.scaling);
	}

	static getAngle(p1, p2, p, cN)
	{
		let v1 = p1.subtract(p);
		let v2 = p2.subtract(p);
		let n = BABYLON.Vector3.Cross(v1, v2);
		let angle = (Math.asin(n.length() / (v1.length() * v2.length())));
		if(BABYLON.Vector3.Dot(n, cN) < 0) {
			angle = (-1 * angle);
		}
		return angle;
	}

	createMaterials(scene)
	{
		this.redMat = Manipulator.getStandardMaterial("redMat", BABYLON.Color3.Red(), scene);
		this.greenMat = Manipulator.getStandardMaterial("greenMat", BABYLON.Color3.Green(), scene);
		this.blueMat = Manipulator.getStandardMaterial("blueMat", BABYLON.Color3.Blue(), scene);
		this.whiteMat = Manipulator.getStandardMaterial("whiteMat", BABYLON.Color3.White(), scene);
		this.yellowMat = Manipulator.getStandardMaterial("whiteMat", BABYLON.Color3.Yellow(), scene);
	}

	disposeMaterials()
	{
		this.redMat.dispose();
		this.greenMat.dispose();
		this.blueMat.dispose();
		this.whiteMat.dispose();
		this.yellowMat.dispose();
	}

	static getStandardMaterial(name, color, scene)
	{
		let mat = new BABYLON.StandardMaterial(name, scene);		
		mat.diffuseColor = color;
		mat.emissiveColor = color;
		mat.specularColor = BABYLON.Color3.Black();
		return mat;
	}
}

class ActHist {
	
	constructor(mesh, capacity) 
	{
		this.lastMax = 10;
		this.acts = [];
		this.last = -1;
		this.current = -1;
		this.mesh = mesh;
		this.lastMax = (capacity - 1);
		if(mesh.rotationQuaternion == null) {
			if(mesh.rotation != null) {
				mesh.rotationQuaternion = BABYLON.Quaternion.RotationYawPitchRoll(mesh.rotation.y, mesh.rotation.x, mesh.rotation.z);
			}
		}
		this.add();
	}

	setCapacity(c) 
	{
		if(c === 0) {
			console.error("Capacity should be more than zero");
			return;
		}
		this.lastMax = c - 1;
		this.last = -1;
		this.current = -1;
		this.acts = [];
		this.add();
	}

	add() 
	{
		let act = new Act(this.mesh);
		if(this.current < this.last) {
			this.acts.splice(this.current + 1);
			this.last = this.current;
		}
		if(this.last === this.lastMax) {
			this.acts.shift();
			this.acts.push(act);
		} else {
			this.acts.push(act);
			this.last++;
			this.current++;
		}
	}

	undo() 
	{
		if(this.current > 0) {
			this.current--;
			this.acts[this.current].perform(this.mesh);
		}
	}

	redo() 
	{
		if(this.current < this.last) {
			this.current++;
			this.acts[this.current].perform(this.mesh);
		}
	}	
}

class Act {
	
	constructor(mesh)
	{
		this.p = mesh.position.clone();
		if(mesh.rotationQuaternion == null) {
			this.r = null;
			this.rE = mesh.rotation.clone();
		} else {
			this.r = mesh.rotationQuaternion.clone();
			this.rE = null;
		}
		this.s = mesh.scaling.clone();
	}

	perform(mesh)
	{
		mesh.position = this.p.clone();
		if(mesh.rotationQuaternion == null) {
			if(this.rE != null) {
				mesh.rotation.copyFrom(this.rE);
			} else {
				mesh.rotation.copyFrom(this.r.toEulerAngles());
			}
		} else {
			if(this.r != null) {
				mesh.rotationQuaternion.copyFrom(this.r);
			} else {
				mesh.rotationQuaternion.copyFrom(this.rE.toQuaternion());
			}
		}
		mesh.scaling = this.s.clone();
	}
}