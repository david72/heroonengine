/// <reference path="../../Engines/babylon.d.ts" />
/*##################################################
 *                                addObject.js
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 30/05/2017
 ###################################################
 */
class addObject {
	
	constructor()
	{
		this.nameTerrain = null;
		this.waterMaterial = null;
		this.slotNumero = 0;
		this.start = null;
		this.end = null;
		this.skyboxMaterial = null;
		this.arrayPoints = [];
		this.arrayZoneForest = [];
		this.connectePoint = null;
		this.selectedIndexGround = 0;
	}	

	_materialGround()
	{
		BABYLONX.ShaderBuilder.InitializeEngine();
		
		let cheminImageGound = "_Projects/"+projet_name+"/scenes/"+global.terrain.name+".png?"+(new Date()).getTime();
		let rangeSt = -0.4;
		let rangePo = 0.1;
		
		this.textureGround = new BABYLON.DynamicTexture("DynamicTexture_"+global.terrain.name, 1024, global.scene, true);
		this.textureSize = this.textureGround.getSize();
		this.textureContext = this.textureGround.getContext();
		this.loadedTexture = new Image();
		this.loadedTexture.crossOrigin = "Anonymous";
		this.loadedTexture.src = cheminImageGound;
		this.loadedTexture.onload = () => {
			this.textureContext.save();
			this.textureContext.clearRect(0, 0, this.textureSize.width, this.textureSize.height);
			this.textureContext.drawImage(this.loadedTexture, 0, 0);
			this.textureContext.restore();
			this.textureGround.update();
		};	
		
		//Create material shader		
		$.ajaxSetup({ async: false});
		$.getJSON("_Projects/"+projet_name+"/scenes/"+global.terrain.name+".data?"+(new Date()).getTime(), (data) => {
			// Assign texture on ground
			let root = "_Projects/"+projet_name+"/textures/terrain/";			
			let sresult = BABYLONX.Shader.Join(["vec2 rotate_xy(vec2 pr1, vec2 pr2, float alpha) {vec2 pp2 = vec2( pr2.x - pr1.x, pr2.y - pr1.y ); return vec2( pr1.x + pp2.x * cos(alpha*3.14159265/180.) - pp2.y * sin(alpha*3.14159265/180.), pr1.y + pp2.x * sin(alpha*3.14159265/180.) + pp2.y * cos  (alpha*3.14159265/180.)); } \n vec3 r_y(vec3 n, float a, vec3 c) {vec3 c1 = vec3( c.x, c.y,  c.z ); c1.x = c1.x; c1.y = c1.z; vec2 p = rotate_xy(vec2(c1.x, c1.y), vec2( n.x, n.z ), a); n.x = p.x; n.z = p.y; return n;  } \n vec3 r_x(vec3 n, float a, vec3 c) {vec3 c1 = vec3( c.x, c.y,  c.z ); c1.x = c1.y; c1.y = c1.z; vec2 p = rotate_xy(vec2(c1.x, c1.y), vec2( n.y, n.z ), a); n.y = p.x; n.z = p.y; return n;  } \n vec3 r_z(vec3 n, float a, vec3 c) { vec3 c1 = vec3( c.x, c.y,  c.z ); vec2 p = rotate_xy(vec2(c1.x, c1.y), vec2( n.x, n.y ), a); n.x = p.x; n.y = p.y; return n;  }", "vec3 normalMap() { vec4 result = vec4(0.);  return result.xyz; }"]);     
				
			global.terrainMaterial = new BABYLON.CustomMaterial('terrainMaterial', global.scene);
			global.terrainMaterial.AddUniform('dyTexture', 'sampler2D');
			global.terrainMaterial.AddUniform('texture1', 'sampler2D');
			global.terrainMaterial.AddUniform('texture2', 'sampler2D');
			global.terrainMaterial.AddUniform('texture3', 'sampler2D');
			global.terrainMaterial.AddUniform('texture4', 'sampler2D');
			global.terrainMaterial.AddUniform('texture5', 'sampler2D');
			global.terrainMaterial.AddUniform('texture6', 'sampler2D');
			global.terrainMaterial.AddUniform('texture7', 'sampler2D'); 
			global.terrainMaterial.AddUniform('texture8', 'sampler2D');		
			
			global.terrainMaterial.AddUniform('scaleTxt1', 'vec2');
			global.terrainMaterial.AddUniform('scaleTxt2', 'vec2');
			global.terrainMaterial.AddUniform('scaleTxt3', 'vec2');
			global.terrainMaterial.AddUniform('scaleTxt4', 'vec2');
			global.terrainMaterial.AddUniform('scaleTxt5', 'vec2');
			global.terrainMaterial.AddUniform('scaleTxt6', 'vec2');
			global.terrainMaterial.AddUniform('scaleTxt7', 'vec2');
			global.terrainMaterial.AddUniform('scaleTxt8', 'vec2');			
			
			global.terrainMaterial.onBindObservable.add(() => {        
                if (global.terrainMaterial.getEffect && global.terrainMaterial.getEffect()) {
					global.terrainMaterial.getEffect().setTexture('dyTexture', this.textureGround);
					global.terrainMaterial.getEffect().setTexture('texture1', new BABYLON.Texture(root+getFile(data.textures._1), global.scene));
					global.terrainMaterial.getEffect().setTexture('texture2', new BABYLON.Texture(root+getFile(data.textures._2), global.scene));
					global.terrainMaterial.getEffect().setTexture('texture3', new BABYLON.Texture(root+getFile(data.textures._3), global.scene));
					global.terrainMaterial.getEffect().setTexture('texture4', new BABYLON.Texture(root+getFile(data.textures._4), global.scene));
					global.terrainMaterial.getEffect().setTexture('texture5', new BABYLON.Texture(root+getFile(data.textures._5), global.scene));
					global.terrainMaterial.getEffect().setTexture('texture6', new BABYLON.Texture(root+getFile(data.textures._6), global.scene));
					global.terrainMaterial.getEffect().setTexture('texture7', new BABYLON.Texture(root+getFile(data.textures._7), global.scene));
					global.terrainMaterial.getEffect().setTexture('texture8', new BABYLON.Texture(root+getFile(data.textures._8), global.scene));					
					
					global.terrain.material.getEffect().setVector2('scaleTxt1', {x: parseFloat(data.uvScale._1), y: parseFloat(data.uvScale._1)});
					global.terrain.material.getEffect().setVector2('scaleTxt2', {x: parseFloat(data.uvScale._2), y: parseFloat(data.uvScale._2)});
					global.terrain.material.getEffect().setVector2('scaleTxt3', {x: parseFloat(data.uvScale._3), y: parseFloat(data.uvScale._3)});
					global.terrain.material.getEffect().setVector2('scaleTxt4', {x: parseFloat(data.uvScale._4), y: parseFloat(data.uvScale._4)});
					global.terrain.material.getEffect().setVector2('scaleTxt5', {x: parseFloat(data.uvScale._5), y: parseFloat(data.uvScale._5)});
					global.terrain.material.getEffect().setVector2('scaleTxt6', {x: parseFloat(data.uvScale._6), y: parseFloat(data.uvScale._6)});
					global.terrain.material.getEffect().setVector2('scaleTxt7', {x: parseFloat(data.uvScale._7), y: parseFloat(data.uvScale._7)});
					global.terrain.material.getEffect().setVector2('scaleTxt8', {x: parseFloat(data.uvScale._8), y: parseFloat(data.uvScale._8)});					
				}
			});
			
			global.terrainMaterial.Fragment_Definitions(sresult+' vec4 SB(vec3 pos, vec3 nrm, vec2 vuv) { vec4 result = vec4(0.); vec3 center = vec3(0.); '+
			new BABYLONX.ShaderBuilder().Map({alpha: true, index: 'dyTexture', bias:0.}).Reference(1)
			.SetUniform('dyTexture', 'sampler2D').SetUniform('texture1', 'sampler2D').SetUniform('texture2', 'sampler2D').SetUniform('texture3', 'sampler2D').SetUniform('texture4', 'sampler2D').SetUniform('texture5', 'sampler2D').SetUniform('texture6', 'sampler2D').SetUniform('texture7', 'sampler2D').SetUniform('texture8', 'sampler2D')
			.SetUniform('scaleTxt1', 'vec2').SetUniform('scaleTxt2', 'vec2').SetUniform('scaleTxt3', 'vec2').SetUniform('scaleTxt4', 'vec2').SetUniform('scaleTxt5', 'vec2').SetUniform('scaleTxt6', 'vec2').SetUniform('scaleTxt7', 'vec2').SetUniform('scaleTxt8', 'vec2')
			.Black(1, BABYLONX.Helper().Map({alpha: true, index: 'texture1', uv: 'vec2(vuv*'+String(data.uvScale._8)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
			.Red(1, BABYLONX.Helper().Map({alpha: true, index: 'texture2', uv:'vec2(vuv*'+String(data.uvScale._1)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
			.Green(1, BABYLONX.Helper().Map({alpha: true, index: 'texture3', uv: 'vec2(vuv*'+String(data.uvScale._2)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
			.Blue(1, BABYLONX.Helper().Map({alpha: true, index: 'texture4', uv: 'vec2(vuv*'+String(data.uvScale._3)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
			.Yellow(1, BABYLONX.Helper().Map({alpha: true, index: 'texture5', uv: 'vec2(vuv*'+String(data.uvScale._4)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
			.Cyan(1, BABYLONX.Helper().Map({alpha: true, index: 'texture6', uv: 'vec2(vuv*'+String(data.uvScale._5)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
			.Pink(1, BABYLONX.Helper().Map({alpha: true, index: 'texture7', uv: 'vec2(vuv*'+String(data.uvScale._6)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
			.White(1, BABYLONX.Helper().Map({alpha: true, index: 'texture8', uv: 'vec2(vuv*'+String(data.uvScale._7)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
			.DisableAlphaTesting().Body +' return result; }');
			
			global.terrainMaterial.diffuseTexture = new BABYLON.Texture(root+"blank.png", global.scene);			
			global.terrainMaterial.Fragment_Custom_Diffuse('result = SB(vPositionW,vNormalW,vDiffuseUV).xyz;'); 
			
			global.terrain.material = global.terrainMaterial;
		});
		$.ajaxSetup({ async: true});		
	}

	loadGround(nameTerrain)
	{
		global.engine.displayLoadingUI();
		if(nameTerrain == "0") { return; }
		if(nameTerrain == this.nameTerrain) { return; }
		this.disposeSceneAndRecreate();
		this.selectedIndexGround = $("#select-zone").prop('selectedIndex');
		this.nameTerrain = nameTerrain;
		BABYLON.SceneLoader.ImportMesh("", "_Projects/"+projet_name+"/scenes/", nameTerrain+"_ground.babylon?"+(new Date()).getTime(), global.scene, (newMeshes) => {
			this.loadMeshOnGround(nameTerrain);
			global.terrain = newMeshes[0];
			let options = null;
			if(global.terrain.metadata) {
				options = global.terrain.metadata;
			} else {
				let boundingInfo = global.terrain.getBoundingInfo();
				options = {size: boundingInfo.maximum.x, type: "ground"};
			}
			global.scene.activeCamera.position.z = -(parseInt(options.size)/2);
			global.editor.resizeGrilleLevelWater(parseInt(options.size) * 2);
			global.terrain.position.y = 0.1;
			global.grassControl = new GrassControl(global.terrain);
			global.terrain.isPickable = false;
			this._materialGround();
			global.environement.loadEnvironnementZone(nameTerrain);
			global.editor.addSceneExplorer("Terrain-"+nameTerrain);
			global.terrain.markVerticesDataAsUpdatable(BABYLON.VertexBuffer.PositionKind, true);				
			global.terrain.markVerticesDataAsUpdatable(BABYLON.VertexBuffer.NormalKind, true);
			//global.terrain.updateCoordinateHeights();			
		});
	}
	
	loadScene(nameTerrain)
	{
		if(nameTerrain == "0") { return; }
		if(nameTerrain == this.nameTerrain) { return; }
		this.disposeSceneAndRecreate();
		this.selectedIndexGround = $("#select-zone").prop('selectedIndex');
		this.nameTerrain = nameTerrain;
		BABYLON.SceneLoader.Append("data/terrain/", nameTerrain+"_ground.babylon?" + (new Date()).getTime(), global.scene, (newScene) => {
			newScene.cameras[1].dispose();
			newScene.activeCamera = global.scene.activeCamera;
			if(this.terrain == null) {
				global.terrain = newScene.getMeshByName(nameTerrain);
				global.terrain.layerMask = 1;
				global.terrain.name = nameTerrain;
				//global.terrain.scaling = new BABYLON.Vector3(0.05, 0.05, 0.05);										
				let PositionKind = BABYLON.VertexBuffer.PositionKind;
				let NormalKind = BABYLON.VertexBuffer.NormalKind;
				global.terrain.setVerticesData(PositionKind, global.terrain.getVerticesData(PositionKind), true);
				global.terrain.setVerticesData(NormalKind, global.terrain.getVerticesData(NormalKind), true);
				//global.terrain.updateCoordinateHeights();					
			}
			global.scene = newScene;
		});
	}

	disposeSceneAndRecreate()
	{
		if(global.terrain) {
			global.editor.disposeCompass();
			global.engine.stopRenderLoop();
			global.scene.dispose();
			global.terrain = null;
			global.editor.reinitialiseVariableGlobal();
			global.editor.createScene();
		}
	}

	loadMeshOnGround(nameTerrain)
	{
		$.ajaxSetup({ async: false});
		let rootScene = '_Projects/'+projet_name+'/scenes/'+nameTerrain+'/';
		let assetsManager = new BABYLON.AssetsManager(global.scene);		
		$.ajax({type: "POST", url: "./PHP/listeMeshOnGround.php", data: "root="+rootScene,
			success: (liste) => {
				liste = liste.split(";");
				let listes = null, meshTask = null, mesh = null, countMesh = 0, resultJson = null; 
				for(let i = 0, countListe = liste.length - 1; i < countListe; i++) {
					listes = liste[i]+'?'+(new Date()).getTime();
					$.getJSON(rootScene+listes, function(data) { resultJson = data; });
					if(resultJson.type == "point" || resultJson.type == "spot") {
						this.createLight(resultJson.type, resultJson, true);
					} else {
						meshTask = assetsManager.addMeshTask("task"+i, "", rootScene, listes);
						meshTask.onSuccess = (task) => {
							mesh = task.loadedMeshes[0];
							global.editor.addSceneExplorer(mesh.name);
							if(mesh.material.name == "materialPortail") {								
								this.createEtiquete(mesh, mesh.name);
							} else if(mesh.metadata.type == "skybox") {
								global.skybox = mesh;
							} else if(mesh.metadata.type == "zoneInteret") {
								global.zoneInterest[global.zoneInterest.length] = mesh;
							} else {
								countMesh = global.objets.length;
								global.objets[countMesh] = mesh;
								global.objets[countMesh].freezeWorldMatrix();
							}							
						}
					}
				}				
			}
		});		
		assetsManager.onFinish = function (tasks) {
			global.engine.hideLoadingUI();
		};
		assetsManager.load();
		$.ajaxSetup({ async: true});
	}

	createOcean(size)
	{
		if(!global.water) {
			if(global.terrain) size = global.terrain.metadata.size || (global.terrain._boundingInfo.boundingBox.maximum.x * 2);
			global.water = BABYLON.Mesh.CreateDisc("Water-ocean", size, 32, global.scene, false);
			global.water.checkCollisions = false;
			global.water.rotation.x = 1.57;
			global.water.position.y = -0.01;
			global.water.isPickable = false;

			global.water.metadata = {type: "water"};
			this.waterMaterial = new BABYLON.WaterMaterial("water_material", global.scene, new BABYLON.Vector2(1024, 1024));
			this.waterMaterial.backFaceCulling = false;
			// Image Bump de l'eau
			this.waterMaterial.bumpTexture = new BABYLON.Texture("_Projects/"+projet_name+"/textures/Materials/waterbump.png", global.scene); // Set the bump texture
			this.waterMaterial.bumpSuperimpose = true;
			this.waterMaterial.bumpAffectsReflection = true;
			// Reglage de l'eau				
			this.waterMaterial.windForce = -6;
			this.waterMaterial.waveHeight = 0.3;
			this.waterMaterial.windDirection = new BABYLON.Vector2(1, 1);
			this.waterMaterial.waterColor = new BABYLON.Color3(0.1, 0.1, 0.6);
			this.waterMaterial.colorBlendFactor = 0.1;
			this.waterMaterial.colorBlendFactor = 0.3;
			this.waterMaterial.bumpHeight = 0.1;
			this.waterMaterial.waveLength = 0.1;		
			this.waterMaterial.colorBlendFactor2 = 0.9;

			// Objets a faire reflechir sur l'eau	
			this.waterMaterial.refractionTexture.activeCamera = global.scene.activeCamera;
			this.waterMaterial.reflectionTexture.activeCamera = global.scene.activeCamera;
			if(global.skybox) { this.waterMaterial.addToRenderList(global.skybox); }
			if(global.terrain) { this.waterMaterial.addToRenderList(global.terrain); }				

			global.water.material = this.waterMaterial;

			this.autoSkyBox(size);
			global.editor.addSceneExplorer('Water-ocean');
		} else {
			jAlert(lang.zones.js.alertOceanDejaCreer, "Attention");
		}
	}

	autoSkyBox(size)
	{
		// On créer une skybox automatiquement si une est pas déjé créer (pour la creation d'un ocean)
		if(global.skybox == null) {
			this.createSkyBox(size * 2);
			global.editor.saveScene(false, true, false, false);
			heroon.log(lang.zones.js.logSkyCreer);
		}
	}

	createSkyBox(size)
	{
		if(global.skybox) { // Si un ciel à deja été créer, on stop la fonction et on informe
			jAlert(lang.zones.js.alertSkyExist, lang.zones.js.titleError);
			return;
		} else if(global.terrain == null) { // Si un terrain n'existe pas, on stop la fonction et on informe
			jAlert(lang.zones.js.alertNoTerrainToEdit, lang.zones.js.titleError);
			return;
		} else if(size == undefined && global.terrain) { // Si un terrain exist mais et que l'on ne connait pas la taille du skybox, on prend la taille du terrain muliplier par 2
			size = (global.terrain.metadata.size * 2);
		}
		// Skybox		
		let envTexture = new BABYLON.CubeTexture("_Projects/"+projet_name+"/textures/skybox/TropicalSunnyDay", global.scene);
		global.scene.environmentTexture = envTexture;
		global.skybox = this.scene.createDefaultSkybox(envTexture, true, size);		
		global.skybox.metadata = {"type": "skybox"};
		if(this.waterMaterial) { this.waterMaterial.addToRenderList(global.skybox); } // Si on a créer de l'eau dans la zone.
		global.editor.saveScene(false, true, false, false);
	}

	createPrimitif(type)
	{
		let countMesh = global.objets.length;
		let typeOfSelect = $("#addObjetPrimitifTo :selected").val();
		let positionVector3 = null;
		if(typeOfSelect == "faceCam") {
			positionVector3 = global.scene.activeCamera.getFrontPosition(25);
			if(global.terrain) {
				let y = global.terrain.getHeightAtCoordinates(positionVector3.x, positionVector3.z);
				positionVector3.y = y;
			}
		} else if(typeOfSelect == "centreMap") {
			positionVector3 = BABYLON.Vector3.Zero();
		} else if(typeOfSelect == "onObjectSelected") {
			positionVector3 = new BABYLON.Vector3.Vecor3(global.objetSelected.position.x, global.objetSelected.position.y + 2.5, global.objetSelected.position.z);
		}
		let matPrimitif = new BABYLON.StandardMaterial("MaterialBasePrimitifs", global.scene);
		switch(type)
		{
			case "box":
				global.objets[countMesh] = BABYLON.Mesh.CreateBox("box"+countMesh, 5.0, global.scene, false);
				global.objets[countMesh].position = positionVector3;
				global.objets[countMesh].position.y += 2.5;
				matPrimitif.diffuseColor = new BABYLON.Color3(0.46, 0.09, 0.06);
				global.editor.addSceneExplorer("box"+countMesh);
				global.objetSelected = global.objets[countMesh];
				global.objets[countMesh].checkCollisions = true;
				//equipe.send("box");
			break;
			case "sphere":
				global.objets[countMesh] = BABYLON.Mesh.CreateSphere("sphere"+countMesh, 32, 5.0, global.scene, false);
				global.objets[countMesh].position = positionVector3;
				global.objets[countMesh].position.y += 2.5;
				matPrimitif.diffuseColor = new BABYLON.Color3(0.33, 0.37, 0.02);
				global.editor.addSceneExplorer("sphere"+countMesh);
				global.objetSelected = global.objets[countMesh];
				global.objets[countMesh].checkCollisions = true;
				//equipe.send("sphere");
			break;
			case "cylindre":
				global.objets[countMesh] = BABYLON.Mesh.CreateCylinder("cylinder"+countMesh, 5, 3, 3, 32, 1, global.scene, false);
				global.objets[countMesh].position = positionVector3;
				global.objets[countMesh].position.y += 2.5;
				matPrimitif.diffuseColor = new BABYLON.Color3(0.08, 0.11, 0.43);
				global.editor.addSceneExplorer("cylinder"+countMesh);
				global.objetSelected = global.objets[countMesh];
				global.objets[countMesh].checkCollisions = true;
				//equipe.send("cylindre");
			break;
			case "cone":
				global.objets[countMesh] = BABYLON.Mesh.CreateCylinder("cone"+countMesh, 5, 0, 3, 32, 1, global.scene, false);
				global.objets[countMesh].position = positionVector3;
				global.objets[countMesh].position.y += 2.5;
				matPrimitif.diffuseColor = new BABYLON.Color3(0.62, 0.62, 0);
				global.editor.addSceneExplorer("cone"+countMesh);
				global.objetSelected = global.objets[countMesh];
				global.objets[countMesh].checkCollisions = true;
				//equipe.send("cone");
			break;
			case "pyramide":
				global.objets[countMesh] = BABYLON.Mesh.CreateCylinder("pyramide"+countMesh, 3, 0, 5, 4, 1, global.scene, false);
				global.objets[countMesh].position = positionVector3;
				global.objets[countMesh].position.y += 1.5;
				matPrimitif.diffuseColor = new BABYLON.Color3(0.74, 0.38, 0);
				global.editor.addSceneExplorer("pyramide"+countMesh);
				global.objetSelected = global.objets[countMesh];
				global.objets[countMesh].checkCollisions = true;
				//equipe.send("pyramide");
			break;
			case "torus":
				global.objets[countMesh] = BABYLON.Mesh.CreateTorus("torus"+countMesh, 5, 1, 32, global.scene, false);
				global.objets[countMesh].position = positionVector3;
				global.objets[countMesh].position.y += 2.5;
				matPrimitif.diffuseColor = new BABYLON.Color3(0, 0.61, 0);
				global.editor.addSceneExplorer("torus"+countMesh);
				global.objetSelected = global.objets[countMesh];
				global.objets[countMesh].checkCollisions = true;
				//equipe.send("torus");
			break;
			case "plan":
				global.objets[countMesh] = BABYLON.Mesh.CreatePlane("plane"+countMesh, 5.0, global.scene, false);
				global.objets[countMesh].position = positionVector3;
				global.objets[countMesh].position.y += 2.5;
				matPrimitif.backFaceCulling = false;
				matPrimitif.diffuseColor = new BABYLON.Color3(0.57, 0.57, 0.62);
				global.editor.addSceneExplorer("plane"+countMesh);
				global.objetSelected = global.objets[countMesh];
				global.objets[countMesh].checkCollisions = false;
				//equipe.send("plan");
			break;
			case "disque":
				global.objets[countMesh] = BABYLON.Mesh.CreateDisc("disque"+countMesh, 5, 32, global.scene, false);
				global.objets[countMesh].position = positionVector3;
				global.objets[countMesh].position.y += 2.5;
				matPrimitif.backFaceCulling = false;
				matPrimitif.diffuseColor = new BABYLON.Color3(0.50, 0.50, 0.50);
				global.editor.addSceneExplorer("disque"+countMesh);
				global.objetSelected = global.objets[countMesh];
				global.objets[countMesh].checkCollisions = false;
				//equipe.send("disque");
			break;
		}
		global.objets[countMesh].metadata = {type: "primitif"};
		global.objets[countMesh].freezeWorldMatrix();
		global.objets[countMesh].material = matPrimitif;
		//global.editor.openProperty('Maillage');
		loadDataProperty('Maillage');
		heroon.log(lang.zones.js.logPrimitifCreer);
		global.editor.saveScene(true, false, false, false);
	}

	createZoneInteret()
	{
		let countZoneInterest = global.zoneInterest.length;
		positionVector3 = global.scene.activeCamera.getFrontPosition(50);
		if(global.terrain) { 
			let y = global.terrain.getHeightAtCoordinates(positionVector3.x, positionVector3.z); 
		}
		global.zoneInterest[countZoneInterest] = BABYLON.Mesh.CreateBox("ZoneInteret"+countZoneInterest, 100.0, global.scene, false);
		global.zoneInterest[countZoneInterest].position = positionVector3;
		global.zoneInterest[countZoneInterest].position.y = y;
		global.zoneInterest[countZoneInterest].metadata = {type: "zoneInteret"};
		global.zoneInterest[countZoneInterest].checkCollisions = false;
		let materialZoneInteret = new BABYLON.StandardMaterial("materialZoneInteret", global.scene);
		materialZoneInteret.diffuseColor = new BABYLON.Color3(0.5, 1, 1); /*bleux ciel */
		materialZoneInteret.alpha = 0.4;
		global.zoneInterest[countZoneInterest].material = materialZoneInteret;
		global.editor.addSceneExplorer("ZoneInteret"+countZoneInterest);
		global.objetSelected = global.zoneInterest[countZoneInterest];
		global.editor.saveScene(true, false, false, false);
	}

	createMesh()
	{
		let countMesh = global.objets.length;
		let typeOfSelect = $("#addObjetComplexTo :selected").val();
		let positionVector3 = null;
		if(typeOfSelect == "faceCam") {
			positionVector3 = global.scene.activeCamera.getFrontPosition(25);
			if(global.terrain) {
				let y = global.terrain.getHeightAtCoordinates(positionVector3.x, positionVector3.z);
				positionVector3.y = y;
			}
		} else if(typeOfSelect == "centreMap") {
			positionVector3 = BABYLON.Vector3.Zero();
		} else if(typeOfSelect == "onObjectSelected") {
			positionVector3 = new BABYLON.Vector3.Vecor3(global.objetSelected.position.x, global.objetSelected.position.y, global.objetSelected.position.z);
		}
		BABYLON.SceneLoader.ImportMesh("", global.assets.rootObjet, global.assets.fichierObjet, global.scene, function(newMeshes) {
			global.objets[countMesh] = newMeshes[0]
			global.objets[countMesh].isPickable = true;
			global.objets[countMesh].metadata = {type: "mesh"};
			// verifie si le mesh est un pnj ayant un skelette
			if(global.objets[countMesh].skeleton) {
				global.objets[countMesh].metadata = {type: "pnj"};
			}
			//verifier si on créer un item ou pas
			$.ajaxSetup({ async: false});
			$.getJSON("_Projects/"+projet_name+"/game data/items.json", function(data) {
				let countItems = Object.keys(data.items).length;
				for(let i = 0; i < countItems; i++) {
					if(getFile(data.items[i].mesh) == global.assets.fichierObjet) {
						global.objets[countMesh].name = data.items[i].name;
						global.objets[countMesh].metadata = {type: "item", data: data.items[i]};
						break;
					}
				}
			});
			$.ajaxSetup({ async: true});
			global.objets[countMesh].freezeWorldMatrix();
			global.objets[countMesh].position = positionVector3;
			if(global.objets[countMesh].skeleton) global.objets[countMesh].updatePoseMatrix(BABYLON.Matrix.Identity());
			global.editor.addSceneExplorer(global.objets[countMesh].name);
			global.objetSelected = global.objets[countMesh];
			//global.editor.openProperty('Maillage');
			loadDataProperty('Maillage');
			heroon.log(lang.zones.js.logMeshCreer);
			global.editor.saveScene(true, false, false, false);
		});
	}

	createZoneSound()
	{
		let countSound = global.sound.length;
		global.soundMesh[countSound] = BABYLON.MeshBuilder.CreateSphere("SoundMesh"+countSound, {diameter: 8, slice: 0.5, updatable: false}, global.scene);
		global.soundMesh[countSound].checkCollisions = false;
		global.soundMesh[countSound].freezeWorldMatrix();
		global.soundMesh[countSound].metadata = {type: "sound"};
		global.soundMesh[countSound].position = global.scene.activeCamera.getFrontPosition(25);
		if(global.terrain) {
			let y = global.terrain.getHeightAtCoordinates(global.soundMesh[countSound].position.x, global.soundMesh[countSound].position.z);
		}
		global.soundMesh[countSound].position.y = (y + 4);

		global.sound[countSound] = new BABYLON.Sound("Sound"+countSound, "", global.scene, null, { volume: 1.0, loop: true, autoplay: true, spatialSound: true, maxDistance: global.soundMesh[countSound].getBoundingInfo().boundingBox.maximumWorld.x, streaming: true });
		global.sound[countSound].setPosition(global.soundMesh[countSound].position);
		global.sound[countSound].attachToMesh(global.soundMesh[countSound]);

		let materialSound = new BABYLON.StandardMaterial("materialSound", global.scene);
		materialSound.diffuseColor = new BABYLON.Color3(1.0, 0.63, 0.63); /*rose*/
		materialSound.alpha = 0.4;
		global.soundMesh[countSound].material = materialSound;
		global.objetSelected = global.soundMesh[countSound];
		global.editor.addSceneExplorer("Sound"+countSound);
		global.editor.openProperty('Sound');
		//loadDataProperty('Sound');
		heroon.log(lang.zones.js.logZoneSoundCreer);
		global.editor.saveScene(true, false, false, false);
	}

	createEtiquete(portail, name)
	{		
		let planeEtiquete = BABYLON.MeshBuilder.CreatePlane("EP", {width: 5, height: 2.0, subdivisions: 1}, global.scene);
		planeEtiquete.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;
		planeEtiquete.parent = portail;
		planeEtiquete.position.y += 3.0;		
		let advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateForMesh(planeEtiquete, 512, 512);
		let etiquete = new BABYLON.GUI.TextBlock();
		etiquete.text = name;
		etiquete.fontName = "bold Calibri";
		etiquete.color = "red";
		etiquete.fontSize = 240;
		advancedTexture.addControl(etiquete);		
	}	
	
	createZonePortail()
	{
		jPrompt(lang.zones.js.labelNamePortal+" :", "", "Information", (portailName) => {
			if(portailName) {
				let countPortail = global.portail.length;
				global.portail[countPortail] = BABYLON.MeshBuilder.CreateBox(portailName, {width: 3, height: 4, depth: 0.6, updatable: false}, global.scene);
				global.portail[countPortail].checkCollisions = false;
				global.portail[countPortail].metadata = {type: "portail"};
				global.portail[countPortail].position = global.scene.activeCamera.getFrontPosition(25);
				if(global.terrain) {
					let y = global.terrain.getHeightAtCoordinates(global.portail[countPortail].position.x, global.portail[countPortail].position.z);
				}
				global.portail[countPortail].position.y = y + 2;
				global.portail[countPortail].freezeWorldMatrix();
				let materialPortail = new BABYLON.StandardMaterial("materialPortail", global.scene);
				materialPortail.diffuseColor = BABYLON.Color3.White(); /*blanc */
				materialPortail.emissiveColor = BABYLON.Color3.White();
				materialPortail.alpha = 0.4;
				global.portail[countPortail].material = materialPortail;
				global.objetSelected = global.portail[countPortail];
				this.createEtiquete(global.portail[countPortail], portailName);
				global.editor.addSceneExplorer(portailName);
				//global.editor.openProperty('Portail');
				loadDataProperty('Portail');
				heroon.log(lang.zones.js.logZonePortailCreer);
				global.editor.saveScene(true, false, false, false);
				//savePortailListeForActor(portailName);
			} else {
				jAlert(lang.zones.js.MessageErrorPortal+" !", lang.zones.js.titleError);
			}
		});
	}

	createZoneCollisionBox() 
	{
		let countCollisionBox = global.collisionBox.length;
		global.collisionBox[countCollisionBox] = BABYLON.MeshBuilder.CreateBox("CollisionBox"+countCollisionBox, {width: 8, height: 4, depth: 0.1, updatable: false}, global.scene);
		global.collisionBox[countCollisionBox].checkCollisions = true;
		global.collisionBox[countCollisionBox].metadata = {type: "collisionbox"};
		global.collisionBox[countCollisionBox].position = global.scene.activeCamera.getFrontPosition(25);
		if(global.terrain) {
			let y = global.terrain.getHeightAtCoordinates(global.collisionBox[countCollisionBox].position.x, global.collisionBox[countCollisionBox].position.z);
		}
		global.collisionBox[countCollisionBox].position.y = y + 2;
		global.collisionBox[countCollisionBox].freezeWorldMatrix();
		let materialCollisionBox = new BABYLON.StandardMaterial("materialCollisionBox", global.scene);
		materialCollisionBox.diffuseColor = new BABYLON.Color3(0.41, 0.59, 1.0); /*blue */
		materialCollisionBox.alpha = 0.5;
		global.collisionBox[countCollisionBox].material = materialCollisionBox;
		global.objetSelected = global.collisionBox[countCollisionBox];
		global.editor.addSceneExplorer("CollisionBox"+countCollisionBox);
		//global.editor.openProperty('CollisionBox');
		loadDataProperty('CollisionBox');
		heroon.log(lang.zones.js.logCollisionBoxCreer);
		global.editor.saveScene(true, false, false, false);
	}

	createZoneTriggerScript()
	{
		let countTrigger = global.trigger.length;
		global.trigger[countTrigger] = BABYLON.MeshBuilder.CreateBox("Trigger"+countTrigger, {size: 5, updatable: false}, global.scene);
		global.trigger[countTrigger].checkCollisions = false;
		global.trigger[countTrigger].metadata = {type: "trigger"};
		global.trigger[countTrigger].position = global.scene.activeCamera.getFrontPosition(25);
		if(global.terrain) {
			let y = global.terrain.getHeightAtCoordinates(global.trigger[countTrigger].position.x, global.trigger[countTrigger].position.z);
		}
		global.trigger[countTrigger].position.y = y + 2.5;
		global.trigger[countTrigger].freezeWorldMatrix();
		let materialTrigger = new BABYLON.StandardMaterial("materialTrigger", global.scene);
		materialTrigger.diffuseColor = new BABYLON.Color3(0.7, 0.64, 0); /*vert */
		materialTrigger.alpha = 0.4;
		global.trigger[countTrigger].material = materialTrigger;
		global.objetSelected = global.trigger[countTrigger];
		global.editor.addSceneExplorer("Trigger"+countTrigger);
		//global.editor.openProperty('Declancheur');
		loadDataProperty('Declancheur');
		heroon.log(lang.zones.js.logZoneTriggerCreer);
		global.editor.saveScene(true, false, false, false);
	}

	createFlagWaypoint(finish)
	{
		let countWaypoint = global.waypoint.length;
		let countSpawnpoint = global.spawnpoint.length;
		if(!finish) {
			global.waypoint[countWaypoint] = BABYLON.MeshBuilder.CreateBox("Waypoint"+countWaypoint, {height: 4, width: 0.2, depth: 0.2, updatable: false}, global.scene);
			global.waypoint[countWaypoint].freezeWorldMatrix();
			global.waypoint[countWaypoint].metadata = {type: "flagwaypoint"};
			global.waypoint[countWaypoint].position = global.scene.activeCamera.getFrontPosition(25);
			if(global.terrain) {
				let y = global.terrain.getHeightAtCoordinates(global.waypoint[countWaypoint].position.x, global.waypoint[countWaypoint].position.z);
			}
			global.waypoint[countWaypoint].position.y = (y + 2);
			let materialWaypoint = new BABYLON.StandardMaterial("materialWaypoint", global.scene);
			materialWaypoint.diffuseColor = BABYLON.Color3.Red(); /*rouge */
			materialWaypoint.alpha = 0.6;
			global.waypoint[countWaypoint].material = materialWaypoint;
		}
		if(global.waypoint[countWaypoint] && this.start == null) {
			$("#waypointsEnd").css({"display":"inline"}).show();
			this.start = global.waypoint[countWaypoint].position;
			this.arrayPoints.push(this.start);
			heroon.log(lang.zones.js.logCreateFlag);
		} else if(global.waypoint[countWaypoint] && this.start != null) {
			this.end = global.waypoint[countWaypoint].position;
			this.arrayPoints.push(this.end);
		}
		if(this.start && this.end && finish) {
			global.spawnpoint[countSpawnpoint] = BABYLON.Mesh.CreateLines("Spawnpoint"+countSpawnpoint, this.arrayPoints, global.scene, true);
			global.spawnpoint[countSpawnpoint].metadata = {type: "flagspanpoint", arrayPoints: this.arrayPoints};
			global.spawnpoint[countSpawnpoint].color = BABYLON.Color3.Red(); /*rouge */
			this.start = null;
			this.end = null;
			$("#waypointsEnd").css({"display":"none"}).hide();
			heroon.log(lang.zones.js.logAjusterFlag);
			global.spawnpointSelected = global.spawnpoint[countSpawnpoint];
			global.editor.addSceneExplorer("Waypoint"+countWaypoint);
			global.editor.saveScene(true, false, false, false);
			//global.editor.openProperty('Balisage');
			loadDataProperty('Balisage');
		}			
	}

	createZoneForest()
	{
		if(global.terrain) {
			let facePosition = global.scene.activeCamera.getFrontPosition(25);
			let countZoneForest = global.ZoneForest.length;

			let materialZoneForest = new BABYLON.StandardMaterial("materialZoneForest", global.scene);
			materialZoneForest.diffuseColor = BABYLON.Color3.Green(); /*vert */
			materialZoneForest.alpha = 0.6;

			global.ZoneForest[countZoneForest] = BABYLON.MeshBuilder.CreateBox("ZoneForest0", {height: 4, width: 0.2, depth: 0.2, updatable: false}, global.scene);
			global.ZoneForest[countZoneForest].position = new BABYLON.Vector3(facePosition.x, 0, facePosition.z);
			
			let y1 = global.terrain.getHeightAtCoordinates(facePosition.x, facePosition.z);
			global.ZoneForest[countZoneForest].position.y = (y1 + 2);
			global.ZoneForest[countZoneForest].material = materialZoneForest;

			global.ZoneForest[countZoneForest+1] = BABYLON.MeshBuilder.CreateBox("ZoneForest1", {height: 4, width: 0.2, depth: 0.2, updatable: false}, global.scene);
			global.ZoneForest[countZoneForest+1].position = new BABYLON.Vector3(facePosition.x -= 5, 0, facePosition.z);
			let y2 = global.terrain.getHeightAtCoordinates(facePosition.x, facePosition.z);
			global.ZoneForest[countZoneForest+1].position.y = (y2 + 2);
			global.ZoneForest[countZoneForest+1].material = materialZoneForest;

			global.ZoneForest[countZoneForest+2] = BABYLON.MeshBuilder.CreateBox("ZoneForest2", {height: 4, width: 0.2, depth: 0.2, updatable: false}, global.scene);
			global.ZoneForest[countZoneForest+2].position = new BABYLON.Vector3(facePosition.x -= 5, 0, facePosition.z);
			let y3 = global.terrain.getHeightAtCoordinates(facePosition.x, facePosition.z);
			global.ZoneForest[countZoneForest+2].position.y = (y3 + 2);
			global.ZoneForest[countZoneForest+2].material = materialZoneForest;

			global.ZoneForest[countZoneForest+3] = BABYLON.MeshBuilder.CreateBox("ZoneForest3", {height: 4, width: 0.2, depth: 0.2, updatable: false}, global.scene);
			global.ZoneForest[countZoneForest+3].position = new BABYLON.Vector3(facePosition.x, 0, facePosition.z -= 5);
			let y4 = global.terrain.getHeightAtCoordinates(facePosition.x, facePosition.z);
			global.ZoneForest[countZoneForest+3].position.y = (y4 + 2);
			global.ZoneForest[countZoneForest+3].material = materialZoneForest;

			global.ZoneForest[countZoneForest+4] = BABYLON.MeshBuilder.CreateBox("ZoneForest4", {height: 4, width: 0.2, depth: 0.2, updatable: false}, global.scene);
			global.ZoneForest[countZoneForest+4].position = new BABYLON.Vector3(facePosition.x += 10, 0, facePosition.z);
			let y5 = global.terrain.getHeightAtCoordinates(facePosition.x, facePosition.z);
			global.ZoneForest[countZoneForest+4].position.y = (y5 + 2);
			global.ZoneForest[countZoneForest+4].material = materialZoneForest;

			global.ZoneForest[countZoneForest+5] = BABYLON.MeshBuilder.CreateBox("ZoneForest5", {height: 4, width: 0.2, depth: 0.2, updatable: false}, global.scene);
			global.ZoneForest[countZoneForest+5].position = new BABYLON.Vector3(facePosition.x, 0, facePosition.z -= 5);
			let y6 = global.terrain.getHeightAtCoordinates(facePosition.x, facePosition.z);
			global.ZoneForest[countZoneForest+5].position.y = (y6 + 2);
			global.ZoneForest[countZoneForest+5].material = materialZoneForest;

			global.ZoneForest[countZoneForest+6] = BABYLON.MeshBuilder.CreateBox("ZoneForest6", {height: 4, width: 0.2, depth: 0.2, updatable: false}, global.scene);
			global.ZoneForest[countZoneForest+6].position = new BABYLON.Vector3(facePosition.x -= 5, 0, facePosition.z);
			let y7 = global.terrain.getHeightAtCoordinates(facePosition.x, facePosition.z);
			global.ZoneForest[countZoneForest+6].position.y = (y7 + 2);
			global.ZoneForest[countZoneForest+6].material = materialZoneForest;

			global.ZoneForest[countZoneForest+7] = BABYLON.MeshBuilder.CreateBox("ZoneForest7", {height: 4, width: 0.2, depth: 0.2, updatable: false}, global.scene);
			global.ZoneForest[countZoneForest+7].position = new BABYLON.Vector3(facePosition.x -= 5, 0, facePosition.z);
			let y8 = global.terrain.getHeightAtCoordinates(facePosition.x, facePosition.z);
			global.ZoneForest[countZoneForest+7].position.y = (y8 + 2);
			global.ZoneForest[countZoneForest+7].material = materialZoneForest;

			this.arrayZoneForest.push(global.ZoneForest[countZoneForest].position);
			this.arrayZoneForest.push(global.ZoneForest[countZoneForest+1].position);
			this.arrayZoneForest.push(global.ZoneForest[countZoneForest+2].position);
			this.arrayZoneForest.push(global.ZoneForest[countZoneForest+3].position);
			this.arrayZoneForest.push(global.ZoneForest[countZoneForest+7].position);
			this.arrayZoneForest.push(global.ZoneForest[countZoneForest+6].position);
			this.arrayZoneForest.push(global.ZoneForest[countZoneForest+5].position);
			this.arrayZoneForest.push(global.ZoneForest[countZoneForest+4].position);
			this.arrayZoneForest.push(global.ZoneForest[countZoneForest].position);

			this.connectePoint = BABYLON.Mesh.CreateLines("connectePointZoneForest", this.arrayZoneForest, global.scene, true);
			this.connectePoint.color = BABYLON.Color3.Green(); /*vert */

			heroon.log(lang.zones.js.logCreateForest);
		}
	}

	generateForestInZone()
	{
		let rotateTreeRand = false;
		let heightTreeRand = false;
		let DensityTreeRand = false;
		let root = null;
		let file = null;
		let meshTree = $("#meshTree").val();
		if(meshTree == "Aucun modele") { // Création d'un arbre par defaut si le champs n'est pas remplie (permet de tester rapidement)
			root = "Data Project/Default/meshes/Tree/";
			file = "tree.babylon";
		} else {
			root = getPath(meshTree);
			file = getFile(meshTree);
		}
		if($("#rotateTreeRand").is(":checked")) { rotateTreeRand = true; }
		if($("#heightTreeRand").is(":checked"))	{ heightTreeRand = true; }
		if($("#densityTree").is(":checked")) { DensityTreeRand = true; }
		let heightTreeMin = $("#heightTreeMin").val();
		let heightTreeMax = $("#heightTreeMax").val();

		BABYLON.SceneLoader.ImportMesh("", root, file, global.scene, (newMeshes) => {
			// Creation d'un arbre de base
			let meshTree = newMeshes[0];
			meshTree.scaling = rescaleMesh(meshTree);
			meshTree.checkCollisions = false;
			if(global.terrain) {
				let y = global.terrain.getHeightAtCoordinates(global.ZoneForest[7].position.x + 1, global.ZoneForest[7].position.z + 1);
			}
			meshTree.position = new BABYLON.Vector3(global.ZoneForest[7].position.x + 1, y, global.ZoneForest[7].position.z + 1);
			meshTree.isPickable = false;

			// On ajoute un collideur pour les collisions sur les arbres
			let collider = BABYLON.Mesh.CreateCylinder("cylinder"+countMesh, 3, 0.7, 0.7, 16, 1, global.scene, false);
			collider.position = meshTree.position;
			collider.scaling = rescaleMesh(collider);
			collider.checkCollisions = true;
			collider.isVisible = false;

			//collider.parent = meshTree;

			let range = 65;
			if(DensityTreeRand) {
				range = 50;
			}
			let count = 300;
			let mergeTree = [];
			mergeTree.push(meshTree);
            for (let index = 0; index < count; index++) {
				// Creation d'instances de l'arbre de base
                let newTree = meshTree.clone("tree"+index);
				let newCollider = collider.clone("colliderTree"+index);
				let x, y, z;
				if(DensityTreeRand) {
					x = (this.arrayZoneForest[4].x) + range - Math.random() * range + 1;
					z = (this.arrayZoneForest[4].z) + range - Math.random() * range + 1;
				} else {
					x = (this.arrayZoneForest[4].x) + range - Math.random() * range + 2;
					z = (this.arrayZoneForest[4].z) + range - Math.random() * range + 2;
				}
				if(global.terrain) {
					y = global.terrain.getHeightAtCoordinates(x, z);
				}
                newTree.position = new BABYLON.Vector3(x, y, z);
				newCollider.position = newTree.position;
				if(rotateTreeRand) {
					newTree.rotate(BABYLON.Axis.Y, Math.random() * 1.57, BABYLON.Space.WORLD);
				}
				if(heightTreeRand) {
					let scale = 0.8 + Math.random() * 1.57;
					newTree.scaling.addInPlace(new BABYLON.Vector3(scale, Math.floor((Math.random() + heightTreeMin) + heightTreeMax), scale));
				} else {
					newTree.scaling.addInPlace(new BABYLON.Vector3(0.5, 0.5, 0.5));
				}
				newCollider.isVisible = false;
				newCollider.checkCollisions = false;

				// Supression des arbres qui depasse de la zone
				if(newTree.position.x > global.ZoneForest[5].position.x) { newTree.dispose(); newCollider.dispose();}
				else if(newTree.position.x > global.ZoneForest[4].position.x) { newTree.dispose(); newCollider.dispose(); }
				else if(newTree.position.x > global.ZoneForest[0].position.x) { newTree.dispose(); newCollider.dispose(); }
				if(newTree.position.z > global.ZoneForest[1].position.z) { newTree.dispose(); newCollider.dispose(); }
				else if(newTree.position.z > global.ZoneForest[2].position.z) { newTree.dispose(); newCollider.dispose(); }

				// On ajoute les arbres dans le tabeau pour la fusion pour des fins d'optimisation.
				mergeTree.push(newTree);
            }

			// Fusion des arbres
			let countMesh = global.objets.length;
			global.objets[countMesh] = BABYLON.Mesh.MergeMeshes(mergeTree, true);
			global.objets[countMesh].metadata = {type: "mesh"};
			// Replacement du point de pivot
			let boundingInfo = global.objets[countMesh].getBoundingInfo().boundingBox;
			let middle = new BABYLON.Vector3(boundingInfo.center.x, boundingInfo.center.y, boundingInfo.center.z);
			global.objets[countMesh].setPivotPoint(middle);
			global.objets[countMesh].checkCollisions = false;
			// Optimisation
			global.objets[countMesh].freezeWorldMatrix();

			// Supprime les gizmos de la zone de foret et le gizmo de transformation
			setTimeout(function() {
				global.ZoneForest[0].dispose();
				global.ZoneForest[1].dispose();
				global.ZoneForest[2].dispose();
				global.ZoneForest[3].dispose();
				global.ZoneForest[4].dispose();
				global.ZoneForest[5].dispose();
				global.ZoneForest[6].dispose();
				global.ZoneForest[7].dispose();
				that.connectePoint.dispose();
				if(global.editor.manipulator){
					global.editor.manipulator.detach();
					global.editor.manipulator = null;
				}
				global.objetSelected = null;
				global.editor.addSceneExplorer("Foret"+countMesh);
				$("#dialog-generate-forest").dialog('close');
				heroon.log(lang.zones.js.logForestGenerer);
			}, 333);
		});
	}

	createLight(typeLight, options, isLoaded)
	{
		let typeOfSelect = $("#addObjetLightTo :selected").val();
		let PositionLumiere = BABYLON.Vector3.Zero();

		let matObjectLight = new BABYLON.StandardMaterial("MaterialObjectLight", global.scene);
		matObjectLight.diffuseColor = new BABYLON.Color3(0.95, 0.91, 0.1);
		matObjectLight.specularColor = BABYLON.Color3.Black();
		matObjectLight.alpha = 0.8;

		let haloLightTexture = new BABYLON.StandardMaterial("MaterialHaloLight", global.scene);
		haloLightTexture.diffuseColor = new BABYLON.Color3(0.95, 0.91, 0.1);
		haloLightTexture.specularColor = BABYLON.Color3.Black();
		haloLightTexture.alpha = 0.2;

		if(typeOfSelect == "faceCam") {
			PositionLumiere = global.scene.activeCamera.getFrontPosition(25);
		} else if(typeOfSelect == "centreMap") {
			PositionLumiere = BABYLON.Vector3.Zero();
		} else if(typeOfSelect == "onObjectSelected") {
			PositionLumiere = new BABYLON.Vector3(global.objetSelected.position.x, global.objetSelected.position.y, global.objetSelected.position.z);
		}
		switch (typeLight) {
			case "point":
				let countLightPoint = global.LightPoint.length;
				//## Gizmo
				global.parentLightPoint[countLightPoint] = BABYLON.Mesh.CreateSphere("LightPoint"+countLightPoint+"Gizmo", 25, 1.0, global.scene, false);
				global.parentLightPoint[countLightPoint].isPickable = true;
				global.parentLightPoint[countLightPoint].checkCollisions = false;
				global.parentLightPoint[countLightPoint].material = matObjectLight;
				let haloLightPoint = BABYLON.Mesh.CreateSphere("halo_Point"+countLightPoint, 25, 1.5, global.scene);
				haloLightPoint.isPickable = false;
				haloLightPoint.material = haloLightTexture;
				haloLightPoint.parent = global.parentLightPoint[countLightPoint];
				//#####
				global.LightPoint[countLightPoint] = new BABYLON.PointLight("LightPoint"+countLightPoint, new BABYLON.Vector3(0, -1, 0), global.scene);
				global.LightPoint[countLightPoint].diffuse = new BABYLON.Color3(0.8, 0.8, 0.8);
				global.LightPoint[countLightPoint].specular = BABYLON.Color3.Black();
				global.LightPoint[countLightPoint].intensity = 1.0;
				global.LightPoint[countLightPoint].parent = global.parentLightPoint[countLightPoint];
				global.LightPoint[countLightPoint].parent.position = PositionLumiere;
				global.LightPoint[countLightPoint].position = PositionLumiere;
				global.parentLightPoint[countLightPoint].metadata = {type: "light"};
				global.lightPointSelected = global.parentLightPoint[countLightPoint];
				global.lightSelected = global.LightPoint[countLightPoint];
				global.objetSelected = global.parentLightPoint[countLightPoint];
				global.editor.addSceneExplorer("LightPoint"+countLightPoint);
			break;
			case "spot":
				let countLightSpot = global.LightSpot.length;
				//## Gizmo
				global.parentLightSpot[countLightSpot] = BABYLON.Mesh.CreateCylinder("LightSpot"+countLightSpot+"Gizmo", 1, 0.1, 1, 10, global.scene, false);
				global.parentLightSpot[countLightSpot].isPickable = true;
				global.parentLightSpot[countLightSpot].checkCollisions = false;
				global.parentLightSpot[countLightSpot].material = matObjectLight;
				let haloLightSpot = BABYLON.Mesh.CreateCylinder("halo_Spot"+countLightSpot, 1, 1, 2, 10, global.scene);
				haloLightSpot.isPickable = false;
				haloLightSpot.position.y = -1;
				haloLightSpot.material = haloLightTexture;
				haloLightSpot.parent = global.parentLightSpot[countLightSpot];
				//#####
				global.LightSpot[countLightSpot] = new BABYLON.SpotLight("LightSpot"+countLightSpot, new BABYLON.Vector3(0, 0, 0), new BABYLON.Vector3(0, -1, 0), 1, 1, global.scene);
				global.LightSpot[countLightSpot].diffuse = new BABYLON.Color3(0.8, 0.8, 0.8);
				global.LightSpot[countLightSpot].specular = BABYLON.Color3.Black();
				global.LightSpot[countLightSpot].intensity = 1.0;
				global.LightSpot[countLightSpot].parent = global.parentLightSpot[countLightSpot];
				global.LightSpot[countLightSpot].parent.position = PositionLumiere;
				global.LightSpot[countLightSpot].position = PositionLumiere;
				global.parentLightSpot[countLightSpot].metadata = {type: "light"};
				global.lightSpotSelected = global.parentLightSpot[countLightSpot];
				global.lightSelected = global.LightSpot[countLightSpot];
				global.objetSelected = global.parentLightSpot[countLightSpot];
				global.editor.addSceneExplorer("LightSpot"+countLightSpot);
			break;
		}
		if(options) {
			global.lightSelected.diffuse = new BABYLON.Color3(options.diffuse.r, options.diffuse.g, options.diffuse.b);
			global.lightSelected.specular = new BABYLON.Color3(options.specular.r, options.specular.g, options.specular.b);
			global.lightSelected.intensity = parseFloat(options.intensity);
			global.lightSelected.includeOnlyWithLayerMask = options.includeOnlyWithLayerMask;
			global.lightSelected.excludeWithLayerMask = options.excludeWithLayerMask;
			global.lightSelected.includedOnlyMeshes = options.includedOnlyMeshes;
			global.lightSelected.excludedMeshes = options.excludedMeshes;
			global.lightSelected.direction = new BABYLON.Vector3(options.direction.x, options.direction.y, options.direction.z);
			global.lightSelected.angleLight = options.angleLight;
			global.lightSelected.exponent = parseFloat(options.exponent);
			let position = new BABYLON.Vector3(options.positionLight.x, options.positionLight.y, options.positionLight.z);
			global.lightSelected.parent.position = position;
			global.lightSelected.position = position;
			let rotation = new BABYLON.Vector3(options.rotationLight.x, options.rotationLight.y, options.rotationLight.z);
			global.lightSelected.parent.rotation = rotation;
			global.lightSelected.rotation = rotation;
		}
		if(isLoaded == undefined) {
			//global.editor.openProperty('Light');
			loadDataProperty('Light');
			global.editor.saveScene(false, false, false, true);
		} else {
			global.lightSelected = null;
		}
	}

	createMaterial()
	{
		global.countMatObject = global.matObject.length;
		global.matObject[global.countMatObject] = new BABYLON.StandardMaterial("Material"+global.countMatObject, global.editor.scene_for_material);
		global.matObject[global.countMatObject].metadata = {type: "material"};
		global.matObject[global.countMatObject].doNotSerialize = true;
		global.matObject[global.countMatObject].diffuseColor = new BABYLON.Color3(0.5, 0.5, 0.5);
		global.selectMaterial = global.matObject[global.countMatObject],
		$("#HE-liste-Material").append("<a href='javascript:void(0);' onClick='global.editor.addObject.createMaterialOnSphere(\"Material"+global.countMatObject+"\");'> - "+$("#HE_materialName").val()+"</a>");
		$("#HE_materiel").append("<option value='"+$("#HE_materialName").val()+"'>"+$("#HE_materialName").val()+"</option>");
		saveMaterial($("#HE_materialName").val(), global.matObject[global.countMatObject]);
		global.editor.addObject.createMaterialOnSphere("Material"+global.countMatObject);
	}

	createSceneForScreenshot(asset)
	{
		if(this.scene_for_screenshot) this.scene_for_screenshot.dispose();
		this.scene_for_screenshot = new BABYLON.Scene(global.engine_for_screenshot);
		this.scene_for_screenshot.ambientColor = new BABYLON.Color3(0.33, 0.33, 0.33);
		this.scene_for_screenshot.clearColor = new BABYLON.Color3(0.39, 0.58, 0.93);
		this.scene_for_screenshot.doNotSerialize = true;
		if(this.light_for_screenshot) this.light_for_screenshot.dispose();
		this.light_for_screenshot = new BABYLON.HemisphericLight("Hemi", new BABYLON.Vector3(0, 1, 0), this.scene_for_screenshot);
		this.light_for_screenshot.diffuse = BABYLON.Color3.White();
		this.light_for_screenshot.specular = BABYLON.Color3.White();
		this.light_for_screenshot.groundColor = BABYLON.Color3.Black();
		this.light_for_screenshot.doNotSerialize = true;
		if(this.camera_for_screenshot) this.camera_for_screenshot.dispose();
		this.camera_for_screenshot = new BABYLON.ArcRotateCamera("ArcRotateCamera", -1.57, 1.57, 3, BABYLON.Vector3.Zero(), this.scene_for_screenshot);
		this.camera_for_screenshot.attachControl(global.canvas_for_screenshot);
		this.camera_for_screenshot.doNotSerialize = true;
		this.camera_for_screenshot.wheelPrecision = 50;
		if(this.mesh_for_screenshot) this.mesh_for_screenshot.dispose();
		let rootObjet = getPath(asset);
		this.fichierObjet = getFile(asset);
		this.rootObjet = rootObjet.replace("..", ".");
		this.boxForUnitMeasur = null;
		BABYLON.SceneLoader.ImportMesh("", this.rootObjet, this.fichierObjet+"?" + (new Date()).getTime(), this.scene_for_screenshot, (newMeshes) => {
			this.boxForUnitMeasur = BABYLON.Mesh.CreateBox("box", 1.0, this.scene_for_screenshot);
			this.boxForUnitMeasur.position.x = -1.5;			
			this.mesh_for_screenshot = newMeshes[0];
			this.mesh_for_screenshot.type = "Mesh";
			this.mesh_for_screenshot.position.y = -0.5;	
			$("#changeScaleImportMesh").val(this.mesh_for_screenshot.scaling.y);			
			$("#block-generation-image, #block-position-generation-image").show();
			$("#progress_import").css({"display":"none"}).hide();
			global.assets.scene.dispose();			
		});
		global.engine_for_screenshot.runRenderLoop(() => {
			if(this.scene_for_screenshot) {
				that.scene_for_screenshot.render();
			}
		});
    }

	changeScaleImportMesh() 
	{
		if(this.mesh_for_screenshot) {
			let scale = parseFloat($("#changeScaleImportMesh").val());
			this.mesh_for_screenshot.scaling = new BABYLON.Vector3(scale, scale, scale);
		}
	}

	saveScaleImportMesh()
	{
		if(this.mesh_for_screenshot) {
			let scale = parseFloat($("#changeScaleImportMesh").val());
			this.mesh_for_screenshot.scaling = new BABYLON.Vector3(scale, scale, scale);			
			let fileRoot = this.rootObjet+this.fichierObjet;
			let serialization = BABYLON.SceneSerializer.SerializeMesh(this.mesh_for_screenshot);
			let str_serialized = JSON.stringify(serialization);
			str_serialized = str_serialized.replace("e+", "Eplus");
			$.ajax({ type: "POST", url: 'PHP/updateScaleMesh.php', data: "rootFile="+fileRoot+"&value="+str_serialized+"&scale="+scale});
		}
	}

	createMiniatureScreenshot(asset)
	{
		this.boxForUnitMeasur.isVisible = false;		
		let mimeType = "image/png";
		let base64Image;
		let width = Math.round(global.engine_for_screenshot.getRenderWidth());
        let height = Math.round(width / global.engine_for_screenshot.getAspectRatio(this.camera_for_screenshot));
		let texture = new BABYLON.RenderTargetTexture("screenShot", 512, this.scene_for_screenshot, false, false);
		texture.renderList = this.scene_for_screenshot.meshes;
		texture.onAfterRenderObservable.add(function () {
			mimeType = "image/png";
            let numberOfChannelsByLine = (width * 4);
            let halfHeight = (height / 2);
            let data = global.engine_for_screenshot.readPixels(0, 0, width, height);
            for (let i = 0; i < halfHeight; i++) {
                for (let j = 0; j < numberOfChannelsByLine; j++) {
                    let currentCell = j + i * numberOfChannelsByLine;
                    let targetLine = height - i - 1;
                    let targetCell = j + targetLine * numberOfChannelsByLine;
                    let temp = data[currentCell];
                    data[currentCell] = data[targetCell];
                    data[targetCell] = temp;
                }
            }
            let screenshotCanvas = document.createElement('canvas');
			screenshotCanvas.id = "canvas_temp";
            screenshotCanvas.width = width;
            screenshotCanvas.height = height;
            let context = screenshotCanvas.getContext('2d');
            let imageData = context.createImageData(width, height);
            let castData = (imageData.data);
            castData.set(data);
            context.putImageData(imageData, 0, 0);
            base64Image = screenshotCanvas.toDataURL(mimeType);
		});
		this.scene_for_screenshot.incrementRenderId();
		this.scene_for_screenshot.resetCachedMaterial();
		texture.render(true);
		texture.dispose();
		this.camera_for_screenshot.getProjectionMatrix(true);
		$.ajax({ type: "POST", url: 'PHP/genereImage.php', data: "imgbase64="+base64Image+"&root=."+this.rootObjet+"&nameImage="+no_extention(this.fichierObjet),
			success: (msg) => {
				this.rootObjet = this.fichierObjet = null;
				this.scene_for_screenshot.dispose();
				$("canvas#canvas_temp").remove();
				$("#block-generation-image, #block-position-generation-image").hide();
				$("#dialog-import-mesh").dialog("close");
			}
		});
	}

	createSceneMaterial() 
	{
		this.scene_for_material = new BABYLON.Scene(global.engine_for_material);
		this.scene_for_material.ambientColor = new BABYLON.Color3(0.33, 0.33, 0.33);
		this.scene_for_material.clearColor = new BABYLON.Color3(0.39, 0.58, 0.93);
		this.scene_for_material.doNotSerialize = true;

		this.light_for_material = new BABYLON.HemisphericLight("Hemi", new BABYLON.Vector3(0, 1, 0), this.scene_for_material);
		this.light_for_material.diffuse = new BABYLON.Color3.White();
		this.light_for_material.specular = new BABYLON.Color3.White();
		this.light_for_material.addObjectColor = new BABYLON.Color3.Black();
		this.light_for_material.doNotSerialize = true;

		this.camera_for_material = new BABYLON.ArcRotateCamera("ArcRotateCamera", Math.PI + 1.57, Math.PI / 2, 10, BABYLON.Vector3.Zero(), this.scene_for_material);
		this.camera_for_material.attachControl(global.canvas_for_material);
		this.camera_for_material.doNotSerialize = true;

		this.sphere_for_material = BABYLON.Mesh.CreateSphere("sphere_for_material", 32, 6.0, this.scene_for_material, false);
		this.sphere_for_material.doNotSerialize = true;
		this.sphere_for_material.checkCollisions = false;

		global.engine_for_material.runRenderLoop(() => {
			this.scene_for_material.render();
		});
	}

	createMaterialOnSphere(materialNameSelected)
	{
		let materialSelected = this.scene_for_material.getMaterialByName(materialNameSelected);
		this.sphere_for_material.material = materialSelected;
		this.sphere_for_material.material.doNotSerialize = true;
		//global.editor.openProperty('Materiel');
		loadDataProperty('Materiel');
	}

	saveTextureGround(slot, element)
	{
		$.ajax({ type: "POST", url: 'PHP/save_textureGround.php', data: "slot="+slot+"&value="+element.src});
	}	

}

var onKeyDown = function (evt) {
	if(evt.keyCode == 32 && MODE_CONTROLE == 1) {
		if($("textarea, input").is(":focus")) return;
		if(global.scene.activeCamera) global.scene.activeCamera.attachControl(global.canvas, true);
		if(global.grassControl) global.grassControl.detachControl(global.canvas);
	}
	if(evt.keyCode == 46 && global.objetSelected) {
		if($("textarea, input").is(":focus")) return;
		global.editor.deleteMesh();
	}
};

var onKeyUp = function (evt) {
	if(evt.keyCode == 32 && MODE_CONTROLE == 1) {
		if(global.scene.activeCamera) global.scene.activeCamera.detachControl(global.canvas);
		if(global.grassControl) global.grassControl.attachControl(global.canvas);
	}
};

window.addEventListener("keydown", onKeyDown, false);
window.addEventListener("keyup", onKeyUp, false);