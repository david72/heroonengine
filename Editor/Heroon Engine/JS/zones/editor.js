/// <reference path="../../Engines/babylon.d.ts" />
/*##################################################
 *                                editor.js
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 30/05/2017
 ###################################################
 */
class sceneEditor {
	
	constructor()
	{
		this.idCompass = "compassOfEditor";
		this.compass();
		this.jquery();

		global.engine.enableOfflineSupport = false;
		BABYLON.SceneLoader.ForceFullSceneLoadingForIncremental = true;
		BABYLON.SceneLoader.ShowLoadingScreen = false;
		BABYLON.Engine.audioEngine.setGlobalVolume(1.0);
		BABYLON.Engine.ShadersRepository = "_Projects/"+projet_name+"/shaders/";
		BABYLON.DebugLayer.InspectorURL = "";

		//this.physique = new BABYLON.CannonJSPlugin();
		this.addObject = new addObject();
		this.addObject.createSceneMaterial();

		this.cameraOfEditor = null;
		this.skybox = null;
		this.lines = [];
		this.grid = null;
		this.light = null;
		this.getSizeGrid = 0;
		this.manipulator = null;

		// Pour le screenshots pour l'import de meshes
		this.scene_for_screenshot = null;
		this.light_for_screenshot = null;
		this.camera_for_screenshot = null;
		this.mesh_for_screenshot = null;

		// Pour l'apercue de la creation de materiels
		this.scene_for_material = null;
		this.light_for_material = null;
		this.camera_for_material = null;
		this.sphere_for_material = null;

		this.rootObjet = '';
		this.fichierObjet = '';
		this.show_stats_fps = false;
		this.show_stats_scene = false;
		this.zoneInterestIsVisible = true;
	}

	createScene() 
	{
		global.scene = new BABYLON.Scene(global.engine);
		global.scene.ambientColor = new BABYLON.Color3(0.33, 0.33, 0.33);
		global.scene.clearColor = new BABYLON.Color3(0.39, 0.58, 0.93);
		global.scene.shadowsEnabled = true;
		
		//this.helper = global.scene.createDefaultEnvironment();

		this.light = new BABYLON.DirectionalLight("light_defaut", new BABYLON.Vector3(-2, -1, 2), global.scene);
		this.light.position.y = 200;
		this.light.diffuse = BABYLON.Color3.White();
		this.light.specular = new BABYLON.Color3(0.3, 0.3, 0.3);
		this.light.intensity = 1.1;
		this.light.shadowAngle = Math.PI/2;
		this.light.shadowMinZ = 2;
		this.light.shadowMaxZ = 2000;
		this.light.doNotSerialize = true;

		let light2 = new BABYLON.DirectionalLight("light_defaut", new BABYLON.Vector3(0.1, -1, -0.1), global.scene);
		light2.position.y = 200;
		light2.diffuse = BABYLON.Color3.White();
		light2.specular = new BABYLON.Color3(0.3, 0.3, 0.3);
		light2.intensity = 1.1;
		light2.doNotSerialize = true;		

		this.shadowGenerator = new BABYLON.ShadowGenerator(1024, this.light);
		this.shadowGenerator.setDarkness(0.5);
		this.shadowGenerator.useBlurCloseExponentialShadowMap = true;
		this.shadowGenerator.blurKernel = 64;
		this.shadowGenerator.blurScale = 2;

		this.createGrilleLevelWater(128);
		this.createCameraEditorModeFly();

		global.scene.registerBeforeRender(() => {
			this.BeforeRender(this);
		});

		global.engine.runRenderLoop(() => {
			if(global.scene.isReady()) {
				global.scene.render();
				if(global.skybox) {
					global.skybox.rotation.y += 0.0003 * global.scene.getAnimationRatio();// rotation du ciel
				}
				if(this.show_stats_scene === true) {
					this.statistiquesGlobal(global.engine, global.scene);
				}
				if(this.show_stats_fps === true){
					this.FPS(global.engine, global.scene);
				}
			}
        });

		global.scene.onPointerDown = (evt, pickResult) => {
			if (evt.button !== 0) { return; }
			if(pickResult.hit) {
				if (evt.ctrlKey) {
					if(this.manipulator && this.manipulator.isPointerOver()) return;
					this.pointerCloseManipulator();
					this.selectEntity(pickResult.pickedMesh, "meshes");
				} else if (evt.altKey) {
					if(this.manipulator && this.manipulator.isPointerOver()) return;
					let addPickedMesh = true;
					let objetSelected = true;
					for (let i = 0; i < global.objetMultiSelected.length; i++) {
						if(pickResult.pickedMesh == global.objetMultiSelected[i]) {
							addPickedMesh = false;
						}
						if(global.objetMultiSelected[i] == global.objetSelected) {
							objetSelected = false;
						}
					}
					if(objetSelected) global.objetMultiSelected.push(global.objetSelected);
					if(addPickedMesh) global.objetMultiSelected.push(pickResult.pickedMesh);
					this.selectMultiEntity(global.objetMultiSelected);
				} else {
					if(this.manipulator && this.manipulator.isPointerOver()) return;
					let nameObjet = pickResult.pickedMesh.name;
					if(nameObjet !== "X" && nameObjet !== "Y" && nameObjet !== "Z" && nameObjet !== "xAxis"  && nameObjet !== "yAxis" && nameObjet !== "zAxis") {
						if(nameObjet.indexOf("Gizmo") > 0) {
							if(nameObjet.indexOf("LightSpot") >= 0) this.selectEntity(pickResult.pickedMesh, "lights_spot");
							else if(nameObjet.indexOf("LightPoint") >= 0) this.selectEntity(pickResult.pickedMesh, "lights_point");
						} else {
							if(pickResult.pickedMesh.metadata.type != "zoneInteret") {
								this.pointerCloseManipulator();
								this.selectEntity(pickResult.pickedMesh, "meshes");
							}
						}
					}
				}
			}
		};

		//BABYLON.SIMDHelper.DisableSIMD(); //Optimisation	SIMD desactiver
		BABYLON.SIMDHelper.EnableSIMD(); //Optimisation	SIMD activer

		//let that = this;
		global.canvas.addEventListener("mouseup", this.clickRight.bind(null, this), false);
	}

	createCameraEditorModeFly() 
	{
		this.cameraOfEditor = new BABYLON.FreeCamera("cameraOfEditor", new BABYLON.Vector3(0, 70, -256), global.scene);
		global.scene.activeCamera = this.cameraOfEditor;
		this.cameraOfEditor.attachControl(global.canvas, true);
		this.cameraOfEditor.doNotSerialize = true;
		global.scene.cameraToUseForPointers = this.cameraOfEditor;
		global.scene.collisionsEnabled = false;
		if(global.terrain) { global.terrain.checkCollisions = false; }
		this.cameraOfEditor.applyGravity = false;
		this.cameraOfEditor.ellipsoid = null;
		if(getCookie("langHeroonEngine")  == "English") {
			removeArray(this.cameraOfEditor.keysUp, 90);
			removeArray(this.cameraOfEditor.keysLeft, 81);
			this.cameraOfEditor.keysUp.push(87); // W
			this.cameraOfEditor.keysLeft.push(65); // A
		} else {
			removeArray(this.cameraOfEditor.keysUp, 87);
			removeArray(this.cameraOfEditor.keysLeft, 65);
			this.cameraOfEditor.keysUp.push(90); // Z
			this.cameraOfEditor.keysLeft.push(81); // Q
		}
		this.cameraOfEditor.keysDown.push(83); // S
		this.cameraOfEditor.keysRight.push(68); // D
		this.cameraOfEditor.minZ = 0;
		this.cameraOfEditor.maxZ = 6000;
	}

	createCameraEditorModeWalk() {
		if(global.scene.activeCamera == this.cameraOfEditorOrbital) {
			global.scene.activeCamera = this.cameraOfEditor;
		}
		global.scene.collisionsEnabled = true;
		this.cameraOfEditor.checkCollisions = true;
		this.cameraOfEditor.applyGravity = true;
		this.cameraOfEditor.ellipsoid = new BABYLON.Vector3(1, 1, 1);
		if(global.terrain) global.terrain.checkCollisions = true;
	}

	createCameraEditorModeOrbit() {
		let position = null;
		if(global.objetSelected) { position = global.objetSelected.position; }
		else { position = BABYLON.Vector3.Zero(); }
		this.cameraOfEditorOrbital = new BABYLON.ArcRotateCamera("cameraOfEditorOrbital", -1.57, 0.75, 15, position, global.scene);
		this.cameraOfEditorOrbital.minZ = 0;
		this.cameraOfEditorOrbital.maxZ = 6000;
		this.cameraOfEditorOrbital.doNotSerialize = true;
		this.cameraOfEditorOrbital.attachControl(global.canvas, true);
		global.scene.activeCamera = this.cameraOfEditorOrbital;
		global.scene.collisionsEnabled = false;
		if(global.terrain) global.terrain.checkCollisions = false;
		this.cameraOfEditor.applyGravity = false;
		this.cameraOfEditor.ellipsoid = null;
	}

	selectMultiEntity(multiMedia) 
	{
		let camera = global.scene.activeCamera;
		if(this.manipulator) {
			this.manipulator.detach();
			delete this.manipulator;
			for (let i = 0; i < multiMedia.length; i++) {
				multiMedia[i].showBoundingBox = false;
				multiMedia[i].freezeWorldMatrix();
				if(i > 0) { multiMedia[0].removeChild(multiMedia[i]); }
 			}
		}
		for (let i = 0; i < multiMedia.length; i++) {
			multiMedia[i].unfreezeWorldMatrix();
			multiMedia[i].showBoundingBox = true;
			if(i > 0) { multiMedia[0].addChild(multiMedia[i]); }
		}
		global.objetSelected = multiMedia[0];
		this.manipulator = new Manipulator(multiMedia[0], camera, global.canvas, 1.0, global.scene);
		this.manipulator.setLocal(false);
		if(this.manipulator) {
			this.manipulator.enableTranslation();// gizmo de transformation par defaut
			this.manipulator.doNotSerialize = true;
		}
	}

	selectEntity(media, type) 
	{
		if(this.manipulator) {
			if(global.lightSelected) {
				this.saveScene(false, false, false, true);
			} else {
				this.saveScene(true, false, false, false);
			}
			this.manipulator.detach();
			delete this.manipulator;
			global.objetSelected.showBoundingBox = false;
			global.objetSelected.freezeWorldMatrix();
			global.objetSelected = null;
			global.terrainIsSelected = false;
			global.spawnpointSelected = null;
		}
		let camera = global.scene.activeCamera;
		if(!media) {
			console.log("Une erreur est survenue a la selection de l'objet: "+ media);
			return;
		}
		if(media.position == undefined) {
			media = global.scene.getMeshByName(media);
		}
		if(media.name == "Water-ocean") {
			this.openProperty('Water');
			return;
		}
		if(media.name) {
			switch(type) {
				// objets 3D
				case "box": case "sphere": case "cylindre": case "torus": case "plan": case "ground": case "meshes":
					global.objetSelected = media;
					global.lightSpotSelected = null;
					global.lightPointSelected = null;
					global.objetSelected.unfreezeWorldMatrix();	
					if(global.objetSelected.name) {
						this.manipulator = new Manipulator(global.objetSelected, camera, global.canvas);						
						// Select type de propriété
						switch(global.objetSelected.metadata.type) {
							case "water":
								loadDataProperty('Transformation');
								loadDataProperty('Water');
								this.openProperty("Transformation");
								this.openProperty("Water");
								global.objetSelected.showBoundingBox = true;
							break;
							case "primitif": case "mesh":
								loadDataProperty('Transformation');
								loadDataProperty('Maillage');
								this.openProperty("Transformation");
								this.openProperty("Maillage");
								global.objetSelected.showBoundingBox = true;
							break;
							case "sound":
								loadDataProperty('Transformation');
								loadDataProperty('Sound');
								this.openProperty("Transformation");
								this.openProperty("Sound");
								global.objetSelected.showBoundingBox = true;
							break;
							case "video":
								loadDataProperty('Transformation');
								loadDataProperty('Video');
								this.openProperty("Transformation");
								this.openProperty("Video");
								global.objetSelected.showBoundingBox = true;
							break;
							case "portail":
								loadDataProperty('Transformation');
								loadDataProperty('Portail');
								this.openProperty("Transformation");
								this.openProperty("Portail");
								global.objetSelected.showBoundingBox = true;
							break;
							case "zoneInteret":
								loadDataProperty('Transformation');
								this.openProperty("Transformation");
								global.objetSelected.showBoundingBox = false;
							break;
							case "collisionbox":
								loadDataProperty('Transformation');
								loadDataProperty('CollisionBox');
								this.openProperty("Transformation");
								this.openProperty("CollisionBox");
								global.objetSelected.showBoundingBox = true;
							break;
							case "trigger":
								loadDataProperty('Transformation');
								loadDataProperty('Declancheur');
								this.openProperty("Transformation");
								this.openProperty("Declancheur");
							break;
							case "flagwaypoint":
								loadDataProperty('Balisage');
								this.openProperty("Balisage");
							break;
							case "light":
								loadDataProperty('Transformation');
								loadDataProperty('Light');
								this.openProperty("Transformation");
								this.openProperty("Light");
							break;
							case "material":
								loadDataProperty('Materiel');
								this.openProperty("Materiel");
							break;
						}
					}
				break;
				// Lights
				case "lights_spot":
					global.lightSpotSelected = media;
					global.lightSelected = media;
					this.manipulator = new Manipulator(global.lightSpotSelected, camera, global.canvas);
					// Ouvre les propriétées de lumière
					loadDataProperty('Transformation');
					loadDataProperty('Light');
					this.openProperty("Transformation");
					this.openProperty("Light");
				break;
				case "lights_point":
					global.lightPointSelected = media;
					global.lightSelected = media;
					this.manipulator = new Manipulator(global.lightPointSelected, camera, global.canvas);
					// Ouvre les propriétées de lumière
					loadDataProperty('Transformation');
					loadDataProperty('Light');
					this.openProperty("Transformation");
					this.openProperty("Light");
				break;
			}
		}		
		if(this.manipulator) {
			this.manipulator.enableTranslation();// gizmo de transformation par defaut
			this.manipulator.doNotSerialize = true;
		}		
	}

	BeforeRender(that)
	{
		if(global.scene.isReady()) {
			if(that.manipulator) {
				if(that.manipulator.isChanging() && global.objetSelected.name.indexOf("ZoneForest") >= 0){
					let arrayPoints = [];
					let countZoneForest = 0;
					arrayPoints.push(global.ZoneForest[countZoneForest].position);
					arrayPoints.push(global.ZoneForest[countZoneForest+1].position);
					arrayPoints.push(global.ZoneForest[countZoneForest+2].position);
					arrayPoints.push(global.ZoneForest[countZoneForest+3].position);
					arrayPoints.push(global.ZoneForest[countZoneForest+7].position);
					arrayPoints.push(global.ZoneForest[countZoneForest+6].position);
					arrayPoints.push(global.ZoneForest[countZoneForest+5].position);
					arrayPoints.push(global.ZoneForest[countZoneForest+4].position);
					arrayPoints.push(global.ZoneForest[countZoneForest].position);
					global.editor.updateLine(that.addObject.connectePoint, arrayPoints);
				}
				else if(that.manipulator.isChanging() && global.objetSelected.name.indexOf("Waypoint") >= 0){
					let arrayPoints = [];
					for(let i = 0; i < global.waypoint.length; i++) {
						arrayPoints.push(global.waypoint[i].position);
					}
					global.editor.updateLine(global.spawnpoint[0], arrayPoints);					
				}
			}
			if($("input").is(":focus") === false) {
				if(global.objetSelected) {
					$('#HE_position').val(global.objetSelected.position.x.toFixed(3)+","+global.objetSelected.position.y.toFixed(3)+","+global.objetSelected.position.z.toFixed(3));
					if(global.objetSelected.rotationQuaternion) {
						$('#HE_rotation').val(global.objetSelected.rotationQuaternion.toEulerAngles().x.toFixed(3)+","+global.objetSelected.rotationQuaternion.toEulerAngles().y.toFixed(3)+","+global.objetSelected.rotationQuaternion.toEulerAngles().z.toFixed(3));
					} else {
						$('#HE_rotation').val(global.objetSelected.rotation.x.toFixed(3)+","+global.objetSelected.rotation.y.toFixed(3)+","+global.objetSelected.rotation.z.toFixed(3));
					}
					$('#HE_scaling').val(global.objetSelected.scaling.x.toFixed(3)+","+global.objetSelected.scaling.y.toFixed(3)+","+global.objetSelected.scaling.z.toFixed(3));
				} else if(global.lightSpotSelected) {
					$('#HE_position').val(global.lightSpotSelected.position.x.toFixed(3)+","+global.lightSpotSelected.position.y.toFixed(3)+","+global.lightSpotSelected.position.z.toFixed(3));
					if(global.lightSelected) $('#HE_direction').val(global.lightSelected.direction.x.toFixed(3)+","+global.lightSelected.direction.y.toFixed(3)+","+global.lightSelected.direction.z.toFixed(3));
				} else if(global.lightPointSelected) {
					$('#HE_position').val(global.lightPointSelected.position.x.toFixed(3)+","+global.lightPointSelected.position.y.toFixed(3)+","+global.lightPointSelected.position.z.toFixed(3));
					if(global.lightSelected) $('#HE_direction').val(global.lightSelected.direction.x.toFixed(3)+","+global.lightSelected.direction.y.toFixed(3)+","+global.lightSelected.direction.z.toFixed(3));
				}
			}
			if(global.scene.activeCamera == that.cameraOfEditor) {
				$("#"+that.idCompass).css({"background-position": ((that.cameraOfEditor.rotation.y - 27.1) * 25)}); // pour la camera free
			} else if(global.scene.activeCamera == that.cameraOfEditorOrbital) {
				$("#"+that.idCompass).css({"background-position": ((that.cameraOfEditorOrbital.alpha - 27.1) * 25)}); // pour la camera rotative
			}
		}
	}

	updateLine(lines, points)
	{
		let cheminNum = 0;
		global.spawnpoint[cheminNum] = BABYLON.Mesh.CreateLines("Spawnpoint"+cheminNum, points, global.scene, true, lines);
		global.spawnpoint[cheminNum].metadata.arrayPoints = points;	
		global.spawnpointSelected = global.spawnpoint[cheminNum];
	}

	pointerCloseManipulator(type)
	{
		if(this.manipulator && global.objetSelected) {
			this.manipulator.detach();
			this.manipulator = null;
			global.objetSelected.showBoundingBox = false;
			global.objetSelected.freezeWorldMatrix();
			if(global.objetMultiSelected.length > 0) {
				for (let i = 0; i < global.objetMultiSelected.length; i++) {
					global.objetMultiSelected[i].showBoundingBox = false;
					global.objetMultiSelected[i].freezeWorldMatrix();
					if(i > 0) { this.unparentMesh(global.objetMultiSelected[i]); }
				}
			}
			global.objetMultiSelected = [];
			global.objetSelected = null;
			global.terrainIsSelected = false;
			global.spawnpointSelected = null;
			global.lightSpotSelected = null;
			global.lightPointSelected = null;
			global.lightSelected = null;
			hideAllProperty();
			global.innerLayout.close("east");
		}
	}

	manipulatorControl(type)
	{
		if(this.manipulator) {
			this.manipulator.setLocal(true);
			switch(type) {
				case "pointer":
					if(global.lightSelected) {
						this.saveScene(false, false, false, true);
					} else {
						this.saveScene(true, false, false, false);
					}
					this.pointerCloseManipulator();
				break;
				case "deplacement":					
					this.manipulator.enableTranslation();
				break;
				case "rotation":					
					this.manipulator.enableRotation();					
				break;
				case "scale":										
					this.manipulator.enableScaling();
				break;
				case "local":
					if($("#local").is(":checked")) {
						this.manipulator.setLocal(true);
					} else {
						this.manipulator.setLocal(false);
					}
				break;
				case "snap":
					if($("#snap").is(":checked")) {
						this.manipulator.setTransSnap(true);
						this.manipulator.setRotSnap(true);
					} else {
						this.manipulator.setTransSnap(false);
						this.manipulator.setRotSnap(false);
					}
				break;
				case "undo":
					this.manipulator.undo();
				break;
				case "redo":
					this.manipulator.redo();
				break;
				case "focus":
					if(global.objetSelected) {
						global.scene.activeCamera.setTarget(global.objetSelected.position);
						global.scene.activeCamera.position = global.scene.activeCamera.getFrontPosition(10);
					}
				break;
			}
		}
	}

	openProperty(group)
	{
		//On masque toutes les propriétés au cas ou un groupe serait ouvert
		hideAllProperty();
		//On afficher le groupe de propriétés souhaiter
		$(".pgGroupRow_"+group+", .pgRow_"+group+"").show();
		$(".pgGroupRow_Transformation, .pgRow_Transformation").show();
		//On ouvre le panel de droite
		global.innerLayout.open("east");
	}

	saveScene(isObject, sky, water, light)
	{
		if(global.terrain) {
			$("#saveSceneEnCour").css({"display":'inline-block'});
			let colorCurrent = $("#title-panel-scene").css("background");
			$("#title-panel-scene").css({"background": "red"});
			let nameTerrain = global.terrain.name || null;
			let cheminProject = "_Projects/"+projet_name+"/";		
			let rootSaveScene = cheminProject+"scenes/";		
				let serialization = null, nameFile = null;
			if(isObject) {				
				if(global.spawnpointSelected) {
					nameFile = global.spawnpointSelected.name;
					serialization = BABYLON.SceneSerializer.SerializeMesh(global.spawnpointSelected);
				} else {				
					nameFile = global.objetSelected.name;
					serialization = BABYLON.SceneSerializer.SerializeMesh(global.objetSelected);
				}
			} else if(sky) {
				nameFile = global.skybox.name;
				serialization = BABYLON.SceneSerializer.SerializeMesh(global.skybox);
			} else if(water) {
				nameFile = global.water.name;
				serialization = BABYLON.SceneSerializer.SerializeMesh(global.water);
			} else if(light) {
				let serializeLight = null;
				if(global.lightPointSelected) {
					serializeLight = {type: "point", positionLight: global.lightPointSelected.position, rotationLight: global.lightPointSelected.rotation, diffuse: global.lightSelected.diffuse, specular: global.lightSelected.specular, intensity: global.lightSelected.intensity, includeOnlyWithLayerMask: global.lightSelected.includeOnlyWithLayerMask, excludeWithLayerMask: global.lightSelected.excludeWithLayerMask, includedOnlyMeshes: global.lightSelected.includedOnlyMeshes, excludedMeshes: global.lightSelected.excludedMeshes, direction: global.lightSelected.direction, angleLight: global.lightSelected.angleLight, exponent: global.lightSelected.exponent};
				} else if(global.lightSpotSelected) {
					serializeLight = {type: "spot", positionLight: global.lightSpotSelected.position, rotationLight: global.lightSpotSelected.rotation, diffuse: global.lightSelected.diffuse, specular: global.lightSelected.specular, intensity: global.lightSelected.intensity, includeOnlyWithLayerMask: global.lightSelected.includeOnlyWithLayerMask, excludeWithLayerMask: global.lightSelected.excludeWithLayerMask, includedOnlyMeshes: global.lightSelected.includedOnlyMeshes, excludedMeshes: global.lightSelected.excludedMeshes, direction: global.lightSelected.direction, angleLight: global.lightSelected.angleLight, exponent: global.lightSelected.exponent};
				}
				nameFile = global.lightSelected.name;
				serialization = serializeLight;
			}
			let str_serialized = JSON.stringify(serialization);
			str_serialized = str_serialized.replace("e+", "Eplus");
			if(str_serialized) {
				if(global.objetSelected.name == nameTerrain) {
					global.terrainIsSelected = true;
				}
				$.ajax({type: "POST", url: './PHP/save_scene.php', data: "terrainName="+nameTerrain+"&file="+nameFile+"&value="+str_serialized+"&root="+rootSaveScene+"&cheminProject="+cheminProject+"&terrainSelected="+global.terrainIsSelected,
					success : function() {
						$("#saveSceneEnCour").css({"display": 'none'});
						$("#title-panel-scene").css({"background": colorCurrent});						
						global.saved = true;						
					}
				});
			} else {
				global.saved = true;
			}
		}
	}

	updateTerrain(groundMesh, oldNameTerrain, newNameTerrain, isDelete)
	{
		let nameTerrain = groundMesh.name;
		global.terrain = groundMesh;		
		let cheminImageGound = "_Projects/"+projet_name+"/scenes/"+nameTerrain+".png?" + (new Date()).getTime();
		if(isDelete === true) {
			$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "project="+projet_name+"&type=delete&value="+nameTerrain});
		} else if(oldNameTerrain != newNameTerrain) {
			$("#select-zone").find("#"+oldNameTerrain).val(newNameTerrain).html(newNameTerrain);
			$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "project="+projet_name+"&type=rename&oldValue="+oldNameTerrain+"&newValue="+newNameTerrain});
		} else if($("#select-zone").find("#"+nameTerrain).val() == undefined || $("#select-zone").find("#"+nameTerrain).val() == null || $("#select-zone").find("#"+nameTerrain).val() === false) {
			$("#select-zone").append('<option id="'+nameTerrain+'" value="'+nameTerrain+'">'+nameTerrain+'</option>');
			$.ajax({ type: "POST", url: 'PHP/updateEnvironnement.php', data: "project="+projet_name+"&type=create&value="+nameTerrain});
		}
		if(isDelete === false && global.terrain) {
			$.ajaxSetup({ async: false});
			global.editor.addObject.loadedTexture = new Image();
			global.editor.addObject.loadedTexture.crossOrigin = "Anonymous";
			global.editor.addObject.loadedTexture.src = cheminImageGound;
			global.editor.addObject.loadedTexture.onload = function() {
				global.editor.addObject.textureContext.save();
				global.editor.addObject.textureContext.clearRect(0, 0, global.editor.addObject.textureSize.width, global.editor.addObject.textureSize.height);
				global.editor.addObject.textureContext.drawImage(global.editor.addObject.loadedTexture, 0, 0);
				global.editor.addObject.textureContext.restore();
				global.editor.addObject.textureGround.update();
			};

			$.getJSON("_Projects/"+projet_name+"/scenes/"+nameTerrain+".data?" + (new Date()).getTime(), function(data) {				
				let root = "_Projects/"+projet_name+"/textures/terrain/";			
				// Assign texture on ground				
				global.terrain.material.getEffect().setTexture('texture1', new BABYLON.Texture(root+getFile(data.textures._1), global.scene));
				global.terrain.material.getEffect().setTexture('texture2', new BABYLON.Texture(root+getFile(data.textures._2), global.scene));
				global.terrain.material.getEffect().setTexture('texture3', new BABYLON.Texture(root+getFile(data.textures._3), global.scene));
				global.terrain.material.getEffect().setTexture('texture4', new BABYLON.Texture(root+getFile(data.textures._4), global.scene));
				global.terrain.material.getEffect().setTexture('texture5', new BABYLON.Texture(root+getFile(data.textures._5), global.scene));
				global.terrain.material.getEffect().setTexture('texture6', new BABYLON.Texture(root+getFile(data.textures._6), global.scene));
				global.terrain.material.getEffect().setTexture('texture7', new BABYLON.Texture(root+getFile(data.textures._7), global.scene));
				global.terrain.material.getEffect().setTexture('texture8', new BABYLON.Texture(root+getFile(data.textures._8), global.scene));
				// Assign scaling texture
				global.terrain.material.getEffect().setVector2('scaleTxt1', {x: parseFloat(data.uvScale._1), y: parseFloat(data.uvScale._1)});
				global.terrain.material.getEffect().setVector2('scaleTxt2', {x: parseFloat(data.uvScale._2), y: parseFloat(data.uvScale._2)});
				global.terrain.material.getEffect().setVector2('scaleTxt3', {x: parseFloat(data.uvScale._3), y: parseFloat(data.uvScale._3)});
				global.terrain.material.getEffect().setVector2('scaleTxt4', {x: parseFloat(data.uvScale._4), y: parseFloat(data.uvScale._4)});
				global.terrain.material.getEffect().setVector2('scaleTxt5', {x: parseFloat(data.uvScale._5), y: parseFloat(data.uvScale._5)});
				global.terrain.material.getEffect().setVector2('scaleTxt6', {x: parseFloat(data.uvScale._6), y: parseFloat(data.uvScale._6)});
				global.terrain.material.getEffect().setVector2('scaleTxt7', {x: parseFloat(data.uvScale._7), y: parseFloat(data.uvScale._7)});
				global.terrain.material.getEffect().setVector2('scaleTxt8', {x: parseFloat(data.uvScale._8), y: parseFloat(data.uvScale._8)});				
				
				let PositionKind = BABYLON.VertexBuffer.PositionKind;
				let NormalKind = BABYLON.VertexBuffer.NormalKind;
				global.terrain.setVerticesData(PositionKind, global.terrain.getVerticesData(PositionKind), true);			
				global.terrain.setVerticesData(NormalKind, global.terrain.getVerticesData(NormalKind), true);	
				global.terrain.updateCoordinateHeights();
			});
			$.ajaxSetup({ async: true});

			global.editor.addObject.textureContext.save();
			global.editor.addObject.textureContext.clearRect(0, 0, global.editor.addObject.textureSize.width, global.editor.addObject.textureSize.height);
			global.editor.addObject.textureContext.drawImage(global.editor.addObject.loadedTexture, 0, 0);
			global.editor.addObject.textureContext.restore();
			global.editor.addObject.textureGround.update();
			global.terrain.material.setTexture('dyTexture', global.editor.addObject.textureGround);			
		}
	}

	addSceneExplorer(objectName)
	{
		$("#explorer").append("<span>- </span><span id='"+objectName+"' class='edit_area_"+objectName+"' onClick='global.editor.selectEntity(\""+objectName+"\", \"meshes\");' style='margin-left:10px;margin-bottom:6px;cursor:pointer;width:100px;' title='DblClick for edit name. Click for select object'>"+objectName+"</span><br />");
		$('.edit_area_'+objectName).editable("dblclick", function(e){
			$(this).html(e.value);
			global.objetSelected.name = e.value;
		});		
	}

	removeSceneExplorer(objectName)
	{
		$("#"+objectName).remove();
	}

	renameSceneExplorer(oldObjetName, objectName)
	{
		$("#"+oldObjetName).attr('onClick', 'global.editor.selectEntity("'+objectName+'", "meshes")');
		$("#"+oldObjetName).text(" - "+objectName+"");
	}

	clickRight(that, ev)
	{
		let rightclick;
		let e = window.event || ev;
		e.preventDefault();
		if(e.which) rightclick = (e.which == 3);
		else if(e.button) rightclick = (e.button == 2);
		if(rightclick === true && global.objetSelected == null) {
			$.contextMenu('destroy', '#HE-canvas-scene');
		} else if(rightclick === true && global.objetSelected) {
			that.contextMenu();
		}
	}

	resetGizmo() 
	{
		global.objetSelected = null;
		if(this.manipulator) {
			this.manipulator.detach();
			delete this.manipulator;
		}
	}

	// Réinitialisation des letiables global l'ors de changement de zone ou de suppression
	reinitialiseVariableGlobal() 
	{
		// select
		global.objetSelected = null, global.spawnpointSelected = null, global.lightSelected= null, global.lightSpotSelected = null, global.lightPointSelected = null, global.selectMaterial = null;
		// scene
		global.countMatObject = 0, global.fire = null, global.lava = null, global.saved = true, global.terrain = null, global.water = null, global.skybox = null, global.musicZone = null,
		global.grassControl = null, global.soundMesh = null;
		// array
		global.objets = [], global.zoneInterest = [], global.sound = [], global.portail = [], global.collisionBox = [], global.trigger = [], global.waypoint = [], global.spawnpoint = [],
		global.parentLightPoint = [], global.parentLightSpot = [], global.LightSpot = [], global.LightPoint = [], global.matObject = [], global.objectsFreeze = [], global.ZoneForest = [],
		global.fur = [], global.cellShading = [], global.objetMultiSelected = [], global.meshInShadow = [];
	}

	switchDebugLayer() 
	{		
		let themeColorInspector = {
			black: {
				backgroundColor: '#eee',
				backgroundColorLighter: '#fff',
				backgroundColorLighter2: '#fff',
				backgroundColorLighter3: '#fff',
				color: '#333',
				colorTop:'red', 
				colorBottom:'blue'
			},
			white: {
				backgroundColor: '#fff',
				backgroundColorLighter: '#efefef',
				backgroundColorLighter2: '#efefef',
				backgroundColorLighter3: '#efefef',
				color: '#000',
				colorTop:'red', 
				colorBottom:'blue'
			},		
			blue: {
				backgroundColor: '#bfdbff',
				backgroundColorLighter: '#acc3da',
				backgroundColorLighter2: '#fff',
				backgroundColorLighter3: '#fff',
				color: '#000',
				colorTop:'red', 
				colorBottom:'blue'
			}
		};
		let showTheme = {newColors: themeColorInspector.black};
		if(theme == "Black") {
			showTheme = {newColors: themeColorInspector.black};
		} else if(theme == "White") {
			showTheme = {newColors: themeColorInspector.white};
		} else if(theme == "Blue") {
			showTheme = {newColors: themeColorInspector.blue};
		}		
		if(global.scene) {
        	if(global.scene.debugLayer.isVisible()) {
        		global.scene.debugLayer.hide();
            } else {
                global.scene.debugLayer.show(showTheme);
				$('#show_stats_fps').hide();
				$('#show_stats_scene').hide();
            }
        }
	}

	switchGrid() 
	{
		if(this.grid.visibility === true){ this.grid.visibility = false; }
		else{ this.grid.visibility = true; }
	}

	switchStats() 
	{
		if(this.show_stats_scene === false) {
			this.show_stats_scene = true;
			$('#show_stats_scene').show();
			if(global.scene.debugLayer.isVisible()) {
        		global.scene.debugLayer.hide();
            }
			if(this.show_stats_fps === true) {
				this.show_stats_fps = false;
				$('#show_stats_fps').hide();
			}
		} else {
			this.show_stats_scene = false;
			$('#show_stats_scene').hide();
		}
	}

	switchFPS()
	{
		if(this.show_stats_fps === false) {
			this.show_stats_fps = true;
			$('#show_stats_fps').show();
			if(global.scene.debugLayer.isVisible()) {
        		global.scene.debugLayer.hide();
            }
			if(this.show_stats_scene === true) {
				$('#show_stats_scene').hide();
			}
		} else {
			this.show_stats_fps = false;
			$('#show_stats_fps').hide();
		}
	}

	statistiquesGlobal(engine, scene)
	{
		$('#show_stats_scene').html("Real FPS: " + BABYLON.Tools.Format(engine.getFps(), 0) + "/60<br />"+
			"Potential FPS: " + BABYLON.Tools.Format(1000.0 / scene.getLastFrameDuration(), 0) + "<br /><br />"+
			"<b>Count</b><br />"+
			"Total meshes: " + scene.meshes.length + "<br />"+
			"Total vertices: " + scene.getTotalVertices() + "<br />"+
			"Total materials: " + scene.materials.length + "<br />"+
			"Total textures: " + scene.textures.length + "<br />"+
			"Active meshes: " + scene.getActiveMeshes().length + "<br />"+
			"Active indices: " + scene.getActiveIndices() + "<br />"+
			"Active bones: " + scene.getActiveBones() + "<br />"+
			"Active particles: " + scene.getActiveParticles() + "<br />"+
			"Draw calls: " + engine.drawCalls + "<br /><br />"+
			"<b>Duration</b><br />"+
			"Meshes selection:</i> " + BABYLON.Tools.Format(scene.getEvaluateActiveMeshesDuration()) + " ms<br />"+
			"Render Targets: " + BABYLON.Tools.Format(scene.getRenderTargetsDuration()) + " ms<br />"+
			"Particles: " + BABYLON.Tools.Format(scene.getParticlesDuration()) + " ms<br />"+
			"Sprites: " + BABYLON.Tools.Format(scene.getSpritesDuration()) + " ms<br />"+
			"Render: " + BABYLON.Tools.Format(scene.getRenderDuration()) + " ms<br />"+
			"Frame: " + BABYLON.Tools.Format(scene.getLastFrameDuration()) + " ms<br />"
		);
	}

	FPS(engine, scene)
	{
		$('#show_stats_fps').html("Real FPS: "+BABYLON.Tools.Format(engine.getFps(), 0) + "/60 <br />Potential FPS: " + BABYLON.Tools.Format(1000.0 / scene.getLastFrameDuration(), 0) + "<br />");
	}

	hidePanel(idNode)
	{
		$("#"+idNode).hide();
		let form = null;
		switch(idNode) {
			case "explorer_window":
				form = "etatExplorer";
			break;
			case "chat_window":
				$("#problems_window").css({"width":"100%"});
				form = "etatChat";
				global.chat.disabledChat();
			break;
			case "problems_window":
				$("#chat_window").css({"width":"100%"});
				form = "etatError";
			break;
		}
		$.ajax({ type: "POST", url: 'PHP/updateInterface.php', data: "layout_form="+form+"&updateForm=1&etat=close"});
	}

	showPanel(idNode)
	{
		$("#"+idNode).show();
		let form = null;
		switch(idNode) {
			case "explorer_window":
				$("#explorer_window").css({"height":"100%"});
				form = "etatExplorer";
			break;
			case "chat_window":
				$("#chat_window").css({"width":"100%"});
				if($("#problems_window").is(":visible")) {
					$("#problems_window").css({"width":"49.76%"});
					$("#chat_window").css({"width":"49.76%"});
				}
				form = "etatChat";
				global.chat.enabledChat();
			break;
			case "problems_window":
				$("#problems_window").css({"width":"100%"});
				if($("#chat_window").is(":visible")) {
					$("#chat_window").css({"width":"49.76%"});
					$("#problems_window").css({"width":"49.76%"});
				}
				form = "etatError";
			break;
		}
		$.ajax({ type: "POST", url: 'PHP/updateInterface.php', data: "layout_form="+form+"&updateForm=1&etat=open"});
	}

	showZoneInterest()
	{
		let countZoneInterest = global.zoneInterest.length;
		if(this.zoneInterestIsVisible === true) {
			for(let i = 0; i < countZoneInterest; i++) {
				global.zoneInterest[i].isVisible = false;
			}
			this.zoneInterestIsVisible = false;
		} else {
			for(let i = 0; i < countZoneInterest; i++) {
				global.zoneInterest[i].isVisible = true;
			}
			this.zoneInterestIsVisible = true;
		}
	}

	resizeGrilleLevelWater(newSize)
	{
		this.grid.dispose();
		this.createGrilleLevelWater(newSize);
		this.getSizeGrid = newSize;
	}

	upTarget()
	{
		if(global.editor.addObject.mesh_for_screenshot) {
			global.editor.addObject.mesh_for_screenshot.position.y -= 0.1;
		}
	}
	
	leftTarget() 
	{
		if(global.editor.addObject.mesh_for_screenshot) {
			global.editor.addObject.mesh_for_screenshot.position.x -= 0.1;
		}
	}
	
	centreTarget()
	{
		if(global.editor.addObject.mesh_for_screenshot) {
			global.editor.addObject.mesh_for_screenshot.position.y = -0.5;
			global.editor.addObject.mesh_for_screenshot.position.x = 0;
			global.editor.addObject.mesh_for_screenshot.position.z = 0;
		}
	}
	
	rightTarget()
	{
		if(global.editor.addObject.mesh_for_screenshot) {
			global.editor.addObject.mesh_for_screenshot.position.x += 0.1;
		}
	}
	
	downTarget() 
	{
		if(global.editor.addObject.mesh_for_screenshot) {
			global.editor.addObject.mesh_for_screenshot.position.y += 0.1;
		}
	}

	createGrilleLevelWater(size)
	{
		let sizeGrid = (size / 2);
		this.getSizeGrid = sizeGrid;
		for (let i1 = -sizeGrid; i1 <= sizeGrid; i1+=6.4) {
			this.lines.push([new BABYLON.Vector3(i1, 0, -sizeGrid), new BABYLON.Vector3(i1, 0, sizeGrid)]);
		}
		for (let i2 = -sizeGrid; i2 <= sizeGrid; i2+=6.4) {
			this.lines.push([new BABYLON.Vector3(-sizeGrid, 0, i2), new BABYLON.Vector3(sizeGrid, 0, i2)]);
		}
		this.grid = BABYLON.MeshBuilder.CreateLineSystem("Grid", {lines: this.lines}, global.scene);
		this.grid.color = BABYLON.Color3.Blue();
		this.grid.isPickable = false;
		this.grid.freezeWorldMatrix();
		this.grid.doNotSerialize = true;
	}

	static openDialogEnvironement() {
		if(global.terrain) {
			$("#dialog-environement").dialog({
				modal: false,
				closeText: "",
				autoOpen: true,
				width: 1130,
				position: { my: "top+60", at: "top+60", of: window },
				buttons: [{
					text: lang.button.js.ok, click: function() {
						$(this).dialog("close");
					}
				}]
			});
		} else {
			jAlert(lang.zones.js.alertNoTerrainToEdit, lang.zones.js.titleActionNotPossible);
		}
	}

	jquery() 
	{
		$("#nameProject").val(projet_name);
		$("#DockingManager").css({"height":($("body").height() - 141)+"px"});
		
		$("#toolbarsTop").sortable({
			update: function(e, ui) {
				// Enregistrement des modifications de position dans la toolbar
				let order = [], positionsToolbars;
                $('#toolbarsTop li').each( function(e) { 
					order.push($(this).attr('id') + '=' + ($(this).index() + 1) );
				});				
				positionsToolbars = order.join(';');
				$.ajax({ type: "POST", url: 'PHP/updateInterface.php', data: "positionsToolbar="+positionsToolbar+"&updateTools=1"});
			},
			create: function( event, ui ) {
				// Repositionnement des elements de la tollbar dans l'ordre de l'enregistrement
				$.getJSON("Data Project/editor.json?" + (new Date()).getTime(), function(data) {					
					$("#toolbarsTop li:eq(0)").insertAfter($("#toolbarsTop li:eq("+(data.interface.tools.positionControl - 1)+")"));
					$("#toolbarsTop li:eq(1)").insertAfter($("#toolbarsTop li:eq("+(data.interface.tools.positionAdd - 1)+")"));
					$("#toolbarsTop li:eq(2)").insertAfter($("#toolbarsTop li:eq("+(data.interface.tools.positionTerrain - 1)+")"));
					$("#toolbarsTop li:eq(3)").insertAfter($("#toolbarsTop li:eq("+(data.interface.tools.positionZone - 1)+")"));					
				});
			}
		}).disableSelection();
		
		$("#toolbarsBottom").sortable({
			update: function(e, ui) {
				// Enregistrement des modifications de position dans la toolbar
				let order = [], positionsToolbars;               
				$('#toolbarsBottom li').each( function(e) {
					order.push($(this).attr('id') + '=' + ($(this).index() + 1) ); 
				});
				positionsToolbars = order.join(';');
				$.ajax({ type: "POST", url: 'PHP/updateInterface.php', data: "positionsToolbar="+positionsToolbar+"&updateTools=1"});
			},
			create: function( event, ui ) {
				// Repositionnement des elements de la tollbar dans l'ordre de l'enregistrement
				$.getJSON("Data Project/editor.json?" + (new Date()).getTime(), function(data) {									
					$("#toolbarsBottom li:eq(0)").insertAfter($("#toolbarsBottom li:eq("+(data.interface.tools.positionOptions - 1)+")"));					
					$("#toolbarsBottom li:eq(1)").insertAfter($("#toolbarsBottom li:eq("+(data.interface.tools.positionPanel - 1)+")"));				
					$("#toolbarsBottom li:eq(2)").insertAfter($("#toolbarsBottom li:eq("+(data.interface.tools.positionCollaboration - 1)+")"));
				});
			}
		}).disableSelection();

		$("#HE-zones-onglet").tabs();

		global.innerLayout = $('#DockingManager').layout({
			center__paneSelector:	".middle-center",
			west__paneSelector:		".middle-left",
			east__paneSelector:		".middle-east",
			south__paneSelector:	".middle-bottom",
			west__size:				290,
			east__size:				320,
			south__size:			170,
			spacing_open:			6,
			spacing_closed:			16,
			center__onresize:		"innerLayout.resizeAll",
			onclose: function(e) {
				global.engine.resize();
				let pane = null, state = global.innerLayout.state;
				if(e == "west") { pane = "etat_left"; }
				else if(e == "east") { pane = "etat_right"; }
				else if(e == "south") { pane = "etat_bottom"; global.chat.disabledChat(); }
				$.ajax({ type: "POST", url: 'PHP/updateInterface.php', data: "paneLayout="+pane+"&etatLayout=close&updateLayout=1", success: function (msg) { }});
			},
			onopen: function(e) {
				global.engine.resize();
				let pane = null, state = global.innerLayout.state;
				if(e == "west") { pane = "etat_left"; }
				else if(e == "east") { pane = "etat_right"; }
				else if(e == "south") { pane = "etat_bottom"; global.chat.enabledChat(); }
				$.ajax({ type: "POST", url: 'PHP/updateInterface.php', data: "paneLayout="+pane+"&etatLayout=open&updateLayout=1", success: function (msg) { }});
			},
			onresize: function() {
				global.engine.resize();
			}
		});
		$.getJSON( "Data Project/editor.json", (data) => {
			//Layout
			if(data.interface.layout_panel.etat_left == "close") { global.innerLayout.close("west"); global.engine.resize(); }
			if(data.interface.layout_panel.etat_right == "close") { global.innerLayout.close("east"); global.engine.resize(); }
			if(data.interface.layout_panel.etat_bottom == "close") { global.innerLayout.close("south"); global.engine.resize(); }
			//Window
			if(data.interface.layout_form.etatExplorer == "close") { this.hidePanel("explorer_window"); }
			if(data.interface.layout_form.etatChat == "close") { this.hidePanel("chat_window"); global.chat.disabledChat(); }
			if(data.interface.layout_form.etatError == "close") { this.hidePanel("problems_window"); }
			//Layout and Window
			if(data.interface.layout_form.etatChat == "close" && data.interface.layout_form.etatError == "close") { global.innerLayout.close("south"); global.chat.disabledChat(); global.engine.resize(); }
			// Options
			if(data.options.hideInfoBull == "true") { $("#hideInfoBull").attr("checked", true); }
		});
		window.onresize = function() { global.engine.resize(); };
		global.innerLayout.close("east");

		$("#dialog-create-ground").dialog({
			modal: true,
			autoOpen: false,
			closeText: "",
			width: 560,
			draggable: false,
			position: { my: "top+60", at: "top+60", of: window },
			buttons: [{
				id: "button-import-zone",
				text: lang.button.js.createGround, click: function() {
					if(global.terrain == null) {
						if($("#groundName").val() != "") {
							let nameZone = $("#groundName").val(), file = $("#importGround").val();
							global.editor.addObject.importGround(nameZone, file);
							$(this).dialog("close");
						} else {
							jAlert(lang.zones.js.MessageErrorGround, lang.zones.js.titleActionNotPossible);
						}
					} else {
						jAlert(lang.zones.js.alertGroundIsDejaCreate, lang.zones.js.titleActionNotPossible);
					}
				}
			}]
		});

		$("#dialog-grass-editor").dialog({
			modal: false,
			autoOpen: false,
			closeText: "",
			width: 300,
			draggable: false,
			open: function() {
				if(global.grassControl) {
					global.grassControl.detachControl(global.canvas);
					global.grassControl.attachControl(global.canvas);
					MODE_CONTROLE_GRASS = 0;
					global.grassControl.direction = 0;
				}				
				MODE_CONTROLE_GRASS = 1;				
			},
			position: { my: "top-300", at: "left+10", of: window },
			buttons: [{
				text: "OK", click: function() {
					global.scene.activeCamera.attachControl(global.canvas, true);
					if(global.grassControl) {
						global.grassControl.detachControl(global.canvas);
						if(global.grassControl.gizmo) {
							global.grassControl.gizmo.dispose();
							global.grassControl.gizmo = null;
						}
						MODE_CONTROLE = 0;
						MODE_CONTROLE_GRASS = 0;
						global.grassControl.direction = 0;
					}
					$(this).dialog("close");
				}
			}]
		});

		$("#importZoneFile").click(function() {
			$("#button-import-zone").attr('disabled', true);
		});

		$("#dialog-options").dialog({
			modal: true,
			autoOpen: false,
			closeText: "",
			width: 500,
			position: { my: "top+60", at: "top+60", of: window },
			buttons: [{
				text: "Ok", click: function() {
					$(this).dialog("close");
				}
			}]
		});

		// Modification des options
		$("#viewCamera").change(() => {
			this.cameraOfEditor.maxZ = parseFloat($("#viewCamera").val());
			$.ajax({ type: "POST", url: 'PHP/updateOptions.php', data: "projet="+projet_name+"&type=viewCamera&value="+$("#viewCamera").val()});
		});
		$("#speedCamera").change(() => {
			this.cameraOfEditor.speed = parseFloat($("#speedCamera").val());
			$.ajax({ type: "POST", url: 'PHP/updateOptions.php', data: "type=speedCamera&value="+$("#speedCamera").val()});
		});
		$("#hideInfoBull").change(function() {
			if($("#hideInfoBull").is(":checked")) { $(document).tooltip("disable"); }
			else { $(document).tooltip("enable"); }
			$.ajax({ type: "POST", url: 'PHP/updateOptions.php', data: "type=hideInfoBull&value="+$("#hideInfoBull").is(":checked")});
		});		
		$.getJSON("Data Project/editor.json", function(data) {
			$("#viewCamera").val(data.options.viewCamera);
			$("#speedCamera").val(data.options.speedCamera);
		});

		$("#dialog-generate-forest").dialog({
			modal: false,
			autoOpen: false,
			closeText: "",
			width: 350,
			position: { my: "top-300", at: "left+10", of: window },
			buttons: [{
				text: lang.button.js.ok, click: function() {
					$(this).dialog("close");
				}
			}]
		});

		$("#dialog-yearSetup").dialog({
			modal: false,
			autoOpen: false,
			width: 350,
			closeText: "",
			position: { my: "top+60", at: "top+60", of: window },
			buttons: [{
				text: lang.button.js.ok, click: function() {
					$(this).dialog("close");
				}
			}]
		});
		$("#dialog-add-primitif").dialog({
			modal: false,
			autoOpen: false,
			width: 550,
			closeText: "",
			position: { my: "top+60", at: "top+60", of: window },
			buttons: [{
				text: lang.button.js.ok, click: function() {
					$(this).dialog("close");
				}
			}]
		});
		$("#dialog-add-medias").dialog({
			modal: false,
			autoOpen: false,
			width: 610,
			closeText: "",
			buttons: [{
				text: lang.button.js.add, click: function() {
					global.editor.addObject.createMesh();
					if($("#closeLibsAfterAdd").is(":checked") === true) {
						if(global.assets.scene) {
							global.assets.engine.stopRenderLoop(function() {
								global.assets.scene.dispose();
							});
						}
						$(this).dialog("close");
					}
				}
			}], close: function( event, ui ) {
				if(global.assets.scene) {
					global.assets.engine.stopRenderLoop(function() {
						global.assets.scene.dispose();
					});
				}
				$(this).dialog("close");
			}
		});
		$("#dialog-add-light").dialog({
			modal: false,
			autoOpen: false,
			closeText: "",
			width: 395,
			buttons: [{
				text: lang.button.js.ok, click: function() {
					$(this).dialog("close");
				}
			}]
		});
		$("#dialog-import-scene").dialog({
			modal: false,
			autoOpen: false,
			closeText: "",
			width: 550,
			buttons: [{
				text: lang.button.js.ok, click: function() {
					$(this).dialog("close");
				}
			}]
		});
		$("#dialog-import-mesh").dialog({
			modal: false,
			autoOpen: false,
			closeText: "",
			width: 550,
			buttons: [{
				text: lang.button.js.ok, click: function() {
					$(this).dialog("close");
				}
			}]
		});
		$("#dialog-import-image").dialog({
			modal: false,
			autoOpen: false,
			closeText: "",
			width: 290,
			buttons: [{
				text: lang.button.js.ok, click: function() {
					$(this).dialog("close");
				}
			}]
		});
		$("#dialog-import-music").dialog({
			modal: false,
			autoOpen: false,
			width: 290,
			closeText: "",
			buttons: [{
				text: lang.button.js.importer, click: function() {
					$(this).dialog("close");
				}
			}]
		});
		$("#dialog-import-sound").dialog({
			modal: false,
			autoOpen: false,
			closeText: "",
			width: 290,
			buttons: [{
				text: lang.button.js.ok, click: function() {
					$(this).dialog("close");
				}
			}]
		});
		$("#dialog-import-video").dialog({
			modal: false,
			autoOpen: false,
			closeText: "",
			width: 290,
			buttons: [{
				text: lang.button.js.ok, click: function() {
					$(this).dialog("close");
				}
			}]
		});
		$("#dialog-materiel").dialog({
			modal: false,
			autoOpen: false,
			closeText: "",
			width: 445,
			buttons: [{
				text: lang.button.js.ok, click: function() {
					$(this).dialog("close");
				}
			}]
		});

		$("#displayBoxUnitOfMeseur").change(function() {
			if($("#displayBoxUnitOfMeseur").is(":checked")) {
				global.editor.addObject.boxForUnitMeasur.isVisible = true;
			} else {
				global.editor.addObject.boxForUnitMeasur.isVisible = false;
			}
		});

		$("#goBoutique").click(function() {
			alert("Store not aviable");
			//window.open("http:\/\/www.heroonengine.actifgames.com/boutique/");
		});

		$("#select-cameraMode").change(() =>{
			if($("#select-cameraMode :selected").val() == "fly") {
				this.createCameraEditorModeFly();
			}
			if($("#select-cameraMode :selected").val() == "walk") {
				this.createCameraEditorModeWalk();
			}
			if($("#select-cameraMode :selected").val() == "orbital") {
				this.createCameraEditorModeOrbit();
			}
		});
	}

	deleteMesh()
	{
		if(global.objetMultiSelected.length > 1) {
			for (let i = 0; i < global.objetMultiSelected.length; i++){
				global.objetMultiSelected[i].showBoundingBox = false;
				global.editor.removeSceneExplorer(global.objetMultiSelected[i].name);
				global.objetMultiSelected[i].dispose();
				$.ajax({ type: "POST", url: 'PHP/delete.php', data: "projetSelected="+projet_name+"&terrainSelected="+global.terrain.name+"&type=deleteMesh&value="+global.objetMultiSelected[i].name});
			}
		} else {
			global.objetSelected.showBoundingBox = false;
			global.editor.removeSceneExplorer(global.objetSelected.name);
			if(global.objetSelected) global.objetSelected.dispose();
			$.ajax({ type: "POST", url: 'PHP/delete.php', data: "projetSelected="+projet_name+"&terrainSelected="+global.terrain.name+"&type=deleteMesh&value="+global.objetSelected.name});
		}
		if(global.lightSelected) {
			global.objetSelected.dispose(false);
			if(global.lightSelected) global.lightSelected.dispose();
			$.ajax({ type: "POST", url: 'PHP/delete.php', data: "projetSelected="+projet_name+"&terrainSelected="+global.terrain.name+"&type=deleteMesh&value="+global.objetSelected.name});
		}
		if(global.editor.manipulator) {
			global.editor.manipulator.detach();
			delete global.editor.manipulator;
			global.objetSelected = null;
			global.objetMultiSelected = [];
			global.editor.manipulator = null;
		}
		hideAllProperty();
		global.innerLayout.close("east");
	}

	contextMenuClick(key)
	{
		switch(key) {
			case "copy":
				if(global.objetSelected) {
					let countMesh = global.objets.length;
					if(global.objetMultiSelected.length > 1) {
						heroon.error(lang.zones.js.logClone);
					} else {
						global.objets[countMesh] = global.objetSelected.clone(global.objetSelected.name+"_clone"+countMesh);
						global.editor.addSceneExplorer(global.objets[countMesh].name);
						global.objetSelected = global.objets[countMesh];
					}
				} else {
					heroon.error(lang.zones.js.logNoSelecteObjet);
				}
			break;
			case "instance":
				if(global.objetSelected) {
					let countMesh = global.objets.length;
					if(global.objetMultiSelected.length > 1) {
						heroon.error(lang.zones.js.logInstance);
					} else {
						global.objets[countMesh] = global.objetSelected.createInstance(global.objetSelected.name+"_clone"+countMesh);
						global.editor.addSceneExplorer(global.objets[countMesh].name);
						global.objetSelected = global.objets[countMesh];
					}
				} else {
					heroon.error(lang.zones.js.logNoSelecteObjet);
				}
			break;
			case "group":
				if(global.objetMultiSelected.length > 1) {
					let countGroup = this.group.length;
					this.group[countGroup] = global.objetMultiSelected;
					for (let i = 0; i < this.group[countGroup].length; i++){
						this.group[countGroup][i].unfreezeWorldMatrix();
						this.group[countGroup][i].showBoundingBox = true;
						if(i > 0) { this.group[countGroup][0].addChild(this.group[countGroup][i]); }
					}
				} else {
					heroon.error(lang.zones.js.logGroup);
				}
			break;
			case "ungroup":
				if(this.manipulator && this.group.length > 0) {
					this.manipulator.detach();
					delete this.manipulator;
					let index = -1;
					for (let i = 0; i < this.group.length; i++) {
						for (let a = 0; a < this.group[i].length; a++) {
							if(global.objetSelected.name == this.group[i][a].name) {
								index = i;
								break;
							}
						}
						if(index >= 0) break;
					}
					let groupSelectd = index || null;
					if(groupSelectd) {
						for (let i = 0; i < this.group[groupSelectd].length; i++) {
							this.group[groupSelectd].showBoundingBox = false;
							this.group[groupSelectd].freezeWorldMatrix();
							if(i > 0) { this.group[groupSelectd][0].removeChild(this.group[groupSelectd][i]); }
						}
					}
				}
			break;
			case "merge":
				if(global.objetMultiSelected.length > 1) {
					try {
						let mergeMesh = [];
						for (let i = 0; i < global.objetMultiSelected.length; i++){
							mergeMesh.push(global.objetMultiSelected[i]);
							global.editor.removeSceneExplorer(global.objetMultiSelected[i].name);
						}
						let countMesh = global.objets.length;
						global.objets[countMesh] = BABYLON.Mesh.MergeMeshes(mergeMesh, true);
						let boundingInfo = global.objets[countMesh].getBoundingInfo().boundingBox;
						let middle = new BABYLON.Vector3(boundingInfo.center.x, boundingInfo.center.y, boundingInfo.center.z);
						global.objets[countMesh].setPivotPoint(middle);
						global.objets[countMesh].checkCollisions = false;
						global.objets[countMesh].freezeWorldMatrix();
						global.editor.addSceneExplorer(global.objets[countMesh].name);
						global.objetSelected = global.objets[countMesh];
					} catch(e) {
						heroon.error(lang.zones.js.logMergeMateriaux);
					}
				} else {
					heroon.error(lang.zones.js.logMergeMesh);
				}
			break;
			case "delete":
				if(global.objetSelected) {
					this.deleteMesh();
				} else {
					heroon.error(lang.zones.js.logNoSelecteObjet);
				}
			break;
		}
	}

	contextMenu() 
	{
		if(this.manipulator) {
			this.menuListe = {
				"copy": {"name": lang.zones.js.contextMenuCopy, "icon": "edit"}, "step1":"--------------",
				"instance": {"name": lang.zones.js.contextMenuInstance, "icon": "edit"}, "step2":"--------------",
				"group": {"name": lang.zones.js.contextMenuGroup, "icon": "edit"}, "step3":"--------------",
				"ungroup": {"name": lang.zones.js.contextMenuUngroup, "icon": "edit"}, "step4":"--------------",
				"merge": {"name": lang.zones.js.contextMenuMerge, "icon": "edit"}, "step5":"--------------",
				"delete": {"name": lang.zones.js.contextMenuDelete, "icon": "edit"}, "step6":"--------------"
			};
			$.contextMenu({
				selector: '#HE-canvas-scene',
				callback: (key, options) => { this.contextMenuClick(key); },
				items: this.menuListe,
				autoHide: true,
			});
		}
	}

	compass() 
	{
		this.divOverlay = document.createElement("div");
		this.divOverlay.style.backgroundImage = "url('Ressources/Compass-overlay.png')";
		this.divOverlay.style.backgroundSize = "100% 100%";
		this.divOverlay.style.width = "160px";
		this.divOverlay.style.height = "30px";
		this.divOverlay.style.top = "25px";
		this.divOverlay.style.left = "10px";
		this.divOverlay.style.position = "absolute";
		this.divOverlay.style.zIndex = "97";
		this.divOverlay.id = this.idCompass;
		this.divOverlay.className = this.idCompass;

		this.divNeedle = document.createElement("div");
		this.divNeedle.style.backgroundImage = "url('Ressources/Compass-needle.png')";
		this.divNeedle.style.backgroundSize = "100% 100%";
		this.divNeedle.style.width = "160px";
		this.divNeedle.style.height = "30px";
		this.divNeedle.style.top = "0px";
		this.divNeedle.style.left = "0px";
		this.divNeedle.style.position = "absolute";
		this.divNeedle.style.zIndex = "98";

		document.getElementById("scene_window").appendChild(this.divOverlay);
		this.divOverlay.appendChild(this.divNeedle);
	}

	disposeCompass()
	{
		let compass = document.getElementById(this.idCompass) || null;
		if(compass) compass.remove();
	}	

}

if(MODE == "dist") {		
	global.chat.getMessages();// Appelle les messages du chat si on est en mode distribution (car inutile en mode developpement ou demo)
}

// Affichage des log, error et warning dans la console d'erreur du GE de l'onglet zone
var heroon = heroon || {};
heroon.log = function(log) {
	$("#errors").append("<span style='color:black'>- "+log+"</span><br />");
};

heroon.error = function(error) {
	$("#errors").append("<span style='color:red'>- "+error+"</span><br />");
};

heroon.warning = function(warning) {
	$("#errors").append("<span style='color:orangered'>- "+warning+"</span><br />");
};