/// <reference path="../../Engines/babylon.d.ts" />
/*##################################################
 *                                assets.js
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 30/05/2017
 ###################################################
 */
class assets {

	constructor()
	{
		this.rootGameData = "_Projects/"+projet_name+"/game data/";
		this.path = null;
		this.scene = null;
		this.light = null;
		this.camera = null;
		this.mesh = null;
		this.ecran == null;
		this.img = null;
		this.canvas = null;
		this.engine = null;		
		this.rootObjet = null;
		this.fichierObjet = null;
		this.autoRotation = true;
		this.upload();
	}
	
	assetZone() // Contruction du moteur pour le canvas de l'onglet "Zones"
	{		
		this.canvas = document.getElementById("HE-canvas-apercu-media");
		this.engine = new BABYLON.Engine(this.canvas, true, {stencil: true, preserveDrawingBuffer: true});
		this.engine.enableOfflineSupport = false;
		this.categorieId = "CategorieMediaZone";
		this.listeMediasId = "listeMediasZone";
		this.isTabZone = true;		
	}
	
	assetMedia() // Contruction du moteur pour le canvas de l'onglet "Media"
	{		
		this.canvas = document.getElementById("HE-canvas-media");
		this.engine = new BABYLON.Engine(this.canvas, true, {stencil: true, preserveDrawingBuffer: true});
		this.engine.enableOfflineSupport = false;		
		this.categorieId = "CategorieMedia";
		this.listeMediasId = "listeMedias";
		this.isTabZone = false;
	}

	appercuAsset(asset)
	{
		if(this.isTabZone) {
			$("#HE-canvas-apercu-media").show();
			$("#HE-canvas-apercu-media-image").hide();
			
		} else {	
			$("#HE-canvas-media").show();
			$("#HE-canvas-media-image").hide();
		}		
		let extentionFile = extention(getFile(asset));

		if(this.scene) {
			this.scene.dispose();
			this.light.dispose();
			this.camera.dispose();
		}

		this.scene = new BABYLON.Scene(this.engine);
		this.scene.doNotSerialize = true;
		this.scene.ambientColor = new BABYLON.Color3(0.33, 0.33, 0.33);
		this.scene.clearColor = new BABYLON.Color3(0.39, 0.58, 0.93);

		this.light = new BABYLON.HemisphericLight("Hemi", new BABYLON.Vector3(0, 1, 0), this.scene);
		this.light.diffuse = BABYLON.Color3.White();
		this.light.specular = BABYLON.Color3.White();
		this.light.groundColor = BABYLON.Color3.Black();
		this.light.doNotSerialize = true;

		this.camera = new BABYLON.ArcRotateCamera("ArcRotateCamera", -1.57, 1.57, 3, BABYLON.Vector3.Zero(), this.scene);
		this.camera.attachControl(this.canvas);
		this.camera.doNotSerialize = true;
		this.camera.wheelPrecision = 50;
		this.camera.zoomStopsAnimation = true;
		//this.camera.idleRotationSpeed = 15;
		this.camera.useAutoRotationBehavior = true;

		if(extentionFile == "babylon") {
			this.engine.displayLoadingUI();	
			if(this.mesh) { this.mesh.dispose(); }
			this.rootObjet = getPath(asset);
			this.fichierObjet = getFile(asset);
			setTimeout(() => {				
				BABYLON.SceneLoader.ImportMesh("", this.rootObjet, this.fichierObjet+"?" + (new Date()).getTime(), this.scene, (newMeshes) => {					
					this.mesh = newMeshes[0];
					this.mesh.position.y = -1;
					if(this.mesh.skeleton) { this.mesh.updatePoseMatrix(BABYLON.Matrix.Identity()); }
					if(this.mesh.material) { this.mesh.material.doNotSerialize = true; }
					this.mesh.doNotSerialize = true;
					this.engine.resize();
					this.engine.hideLoadingUI();
				});
			}, 100);
		}
		else if(extentionFile == "jpeg" || extentionFile == "jpg" || extentionFile == "gif" || extentionFile == "png") {
			this.engine.stopRenderLoop(() => {
				if(this.scene) { this.scene.dispose(); }
			});
			let canvas = null;
			if(this.isTabZone) {
				$("#HE-canvas-apercu-media").hide();
				$("#HE-canvas-apercu-media-image").show();
				canvas = document.getElementById("HE-canvas-apercu-media-image");
			} else {			
				$("#HE-canvas-media").hide();
				$("#HE-canvas-media-image").show();
				canvas = document.getElementById("HE-canvas-media-image");
			}			
			let context = canvas.getContext('2d');
			if(this.img) context.clearRect(0, 0, canvas.width, canvas.height);
			this.img = new Image();
			this.img.src = asset;
			this.img.onload = () => {
				context.drawImage(this.img, 0, 0, this.img.width, this.img.height, 0, 0, canvas.width, canvas.height);
			};			
		}
		else if(extentionFile == "mp3" || extentionFile == "ogg") {
			if(this.music) { this.music.dispose(); }
			this.music = new BABYLON.Sound("Music", asset, this.scene, null, { loop: true, autoplay: true });
		}
		else if(extentionFile == "mp4" || extentionFile == "ogv") {
			if(this.ecran) { this.ecran.dispose(); }
			this.ecran = BABYLON.Mesh.CreatePlane("plane", 10.0, this.scene, false, BABYLON.Mesh.DEFAULTSIDE);
			this.ecran.material.diffuseTexture = new BABYLON.VideoTexture("video", [asset], this.scene, true);
		}

		this.engine.runRenderLoop(() => {
			if(this.scene && this.camera) {
				this.scene.render();
			}
		});
	}

	jquery()
	{
		//auto rotation
		$("#autoRotation").click(()	=> {
			if($("#autoRotation").is(":checked")) {
				this.camera.useAutoRotationBehavior = true;
			} else {
				this.camera.useAutoRotationBehavior = false;
			}
		});
		
		//Categories
		$('#'+this.categorieId).jstree({
			"core": {
				"check_callback": true,
				'data' : {
					"url" : this.rootGameData+"assets.json?" + (new Date()).getTime()
				}
			}
		}).on('select_node.jstree', (e, data) => {
			this.path = data.instance.get_path(data.node, '/');// Chemin complet du dossier clicker
			//Dans un premier temps, on vide la liste des media
			$("#"+this.listeMediasId).html("");
			// Ensuite on vas chercher le contenue du dossier selectionner
			$.ajaxSetup({ async: false});
			let liste = '';
			$.ajax({ type: "POST",url: 'PHP/assets.php', data: "categoriesSelect="+this.path+"&nameProject="+projet_name,
				success: (msg) => {
					// Et on affiche le contenue dans des liens.
					msg = msg.split(";");
					let type = this.path.split("/");
					let myImage = null, myAsset = null;
					$("#optionAddObjet").show();
					let meshToAdd = false;
					for(let i = 0; i < msg.length -1; i++) {
						if(type[0] == "meshes")
						{
							myImage = "_Projects/"+projet_name+"/"+this.path+"/"+no_extention(msg[i])+".png";
							myAsset = "_Projects/"+projet_name+"/"+this.path+"/"+msg[i];
							if(!file_exists(myImage)) myImage = "Ressources/no_image.png";
							meshToAdd = true;
						}
						else if(type[0] == "images" || type[0] == "textures") {
							$("#optionAddObjet").hide();
							myImage = "_Projects/"+projet_name+"/"+this.path+"/"+msg[i];
							myAsset = myImage;
						}
						else if(type[0] == "musics") {
							$("#optionAddObjet").hide();
							myImage = "Ressources/music.png";
							myAsset = "_Projects/"+projet_name+"/"+this.path+"/"+msg[i];
						}
						else if(type[0] == "sounds") {
							$("#optionAddObjet").hide();
							myImage = "Ressources/default_sound.png";
							myAsset = "_Projects/"+projet_name+"/"+this.path+"/"+msg[i];
						}
						else if(type[0] == "video") {
							$("#optionAddObjet").hide();
							myImage = "Ressources/default_cam.png";
							myAsset = "_Projects/"+projet_name+"/"+this.path+"/"+msg[i];
						}
						let action = null;
						if(meshToAdd === true) {
							if(this.isTabZone) {
								action = "<a href='javascript:void(0);' onClick='global.assets.appercuAsset(\""+myAsset+"\");' ondblclick='global.editor.addObject.createMesh()'>";	
							} else {
								action = "<a href='javascript:void(0);' onClick='mediaGlobal.media.appercuAsset(\""+myAsset+"\");'>";
							}
						} else {
							if(this.isTabZone) {
								action = "<a href='javascript:void(0);' onClick='global.assets.appercuAsset(\""+myAsset+"\");'>";
							} else {
								action = "<a href='javascript:void(0);' onClick='mediaGlobal.media.appercuAsset(\""+myAsset+"\");'>";
							}
						}
						liste += "<div style='display:inline-block;width:90px;height:90px;margin:10px;text-align:center;'>"+
									action+
										"<img src='"+myImage+"' style='width:90px;height:90px;' />"+
									"</a><br />"+
									"<small>"+no_extention(msg[i])+"</small>"+
								"</div>";
					}
					$("#"+this.listeMediasId).append(liste);
				}
			});
			$.ajaxSetup({ async: true});
		});

		this.loadMeshesTree();
		this.loadTexturesTree();
		this.loadImagesTree();
		this.loadMusicsTree();
		this.loadSoundsTree();
		this.loadVideosTree();

		$("#newCategorieMedia").click(() => {
			jPrompt(lang.zones.js.assetCategorieName+" :", "", lang.zones.js.titleRenseigne, (folder) => {
				if(folder) {
					let idFolderSelected = $('#'+this.categorieId).jstree('get_selected');
					let folderSelected = $('#'+idFolderSelected[0]).text();
					if(this.path == null) this.path = folderSelected;
					$.ajax({ type: "POST", url: 'PHP/assets.php', data: "categoriesRange="+this.path+"&addFolderCategories="+folder+"&nameProject="+projet_name,
						success: (msg) => {
							$('#'+this.categorieId).jstree('create_node', "#"+idFolderSelected[0], {"id":"child_node_"+folder, "text":folder});
							this.path = this.path.split("/");
							if(this.path[0] == "meshes") { $('#importMeshIn').jstree('destroy', true); this.loadMeshesTree(); }
							else if(this.path[0] == "textures") { $('#importTextureIn').jstree('destroy', true); this.loadTexturesTree(); }
							else if(this.path[0] == "images") { $('#importImageIn').jstree('destroy', true); this.loadImagesTree(); }
							else if(this.path[0] == "musics") { $('#importMusicIn').jstree('destroy', true); this.loadMusicsTree(); }
							else if(this.path[0] == "sounds") { $('#importSoundIn').jstree('destroy', true); this.loadSoundsTree(); }
							else if(this.path[0] == "videos") { $('#importVideoIn').jstree('destroy', true); this.loadVideosTree(); }
						}
					});
				}
			});
		});

		$("#deleteCategorieMedia").click(() => {
			jConfirm(lang.zones.js.confirmDeleteAssetCategorie, "Confirmation", () => {
				let idFolderSelected = $('#'+this.categorieId).jstree('get_selected');
				let folderSelected = $('#'+idFolderSelected[0]).text();
				if(this.path == null) this.path = folderSelected;
				let baseRoot = folderSelected.split("/");
				if(folderSelected != baseRoot[0] && folderSelected != baseRoot[1]) {
					$.ajax({ type: "POST", url: 'PHP/assets.php', data: "categoriesRange="+this.path+"&deleteFolderCategories="+folderSelected+"&nameProject="+projet_name,
						success: (msg) => {
							$('#'+this.categorieId).jstree('delete_node', idFolderSelected);
							this.path = this.path.split("/");
							if(this.path[0] == "meshes") { $('#importMeshIn').jstree('destroy', true); this.loadMeshesTree(); }
							else if(this.path[0] == "textures") { $('#importTextureIn').jstree('destroy', true); this.loadTexturesTree(); }
							else if(this.path[0] == "images") { $('#importImageIn').jstree('destroy', true); this.loadImagesTree(); }
							else if(this.path[0] == "musics") { $('#importMusicIn').jstree('destroy', true); this.loadMusicsTree(); }
							else if(this.path[0] == "sounds") { $('#importSoundIn').jstree('destroy', true); this.loadSoundsTree(); }
							else if(this.path[0] == "videos") { $('#importVideoIn').jstree('destroy', true); this.loadVideosTree(); }
						}
					});
				} else {
					jAlert(lang.zones.js.noDeleteFolderRoot, lang.zones.js.titleError);
				}
			});
		});
	}

	loadMeshesTree()
	{
		$.getJSON(this.rootGameData+"assets.json?" + (new Date()).getTime(), function(data) {
			$('#importMeshIn').jstree({
				"core": {
					"check_callback": true,
					'data' : function (obj, cb) {
						cb.call(this, data[0]);
					}
				}
			}).on('select_node.jstree', function (e, data) {
				let path = data.instance.get_path(data.node, '/');
				$("#range_importMeshFile").val(path);
				$("#nameProject").val(projet_name);
			}).bind("loaded.jstree", function (e, data) { $(this).jstree("open_all"); });
		});
	}

	loadTexturesTree()
	{
		$.getJSON(this.rootGameData+"assets.json?" + (new Date()).getTime(), function(data) {
			$('#importTextureIn').jstree({
				"core": {
					"check_callback": true,
					'data' : function (obj, cb) {
						cb.call(this, data[1]);
					}
				}
			}).on('select_node.jstree', function (e, data) {
				let path = data.instance.get_path(data.node, '/');
				$("#range_importTextureFile").val(path);
				$("#nameProject").val(projet_name);
			}).bind("loaded.jstree", function (e, data) { $(this).jstree("open_all"); });
		});
	}

	loadImagesTree()
	{
		$.getJSON(this.rootGameData+"assets.json?" + (new Date()).getTime(), function(data) {
			$('#importImageIn').jstree({
				"core": {
					"check_callback": true,
					'data' : function (obj, cb) {
						cb.call(this, data[2]);
					}
				}
			}).on('select_node.jstree', function (e, data) {
				let path = data.instance.get_path(data.node, '/');
				$("#range_importImageFile").val(path);
				$("#nameProject").val(projet_name);
			}).bind("loaded.jstree", function (e, data) { $(this).jstree("open_all"); });
		});
	}

	loadMusicsTree()
	{
		$.getJSON(this.rootGameData+"assets.json?" + (new Date()).getTime(), function(data) {
			$('#importMusicIn').jstree({
				"core": {
					"check_callback": true,
					'data' : function (obj, cb) {
						cb.call(this, data[3]);
					}
				}
			}).on('select_node.jstree', function (e, data) {
				let path = data.instance.get_path(data.node, '/');
				$("#range_importMusicFile").val(path);
				$("#nameProject").val(projet_name);
			}).bind("loaded.jstree", function (e, data) { $(this).jstree("open_all"); });
		});
	}

	loadSoundsTree()
	{
		$.getJSON(this.rootGameData+"assets.json?" + (new Date()).getTime(), function(data) {
			$('#importSoundIn').jstree({
				"core": {
					"check_callback": true,
					'data' : function (obj, cb) {
						cb.call(this, data[4]);
					}
				}
			}).on('select_node.jstree', function (e, data) {
				let path = data.instance.get_path(data.node, '/');
				$("#range_importSoundFile").val(path);
				$("#nameProject").val(projet_name);
			}).bind("loaded.jstree", function (e, data) { $(this).jstree("open_all"); });
		});
	}

	loadVideosTree()
	{
		$.getJSON(this.rootGameData+"assets.json?" + (new Date()).getTime(), function(data) {
			$('#importVideoIn').jstree({
				"core": {
					"check_callback": true,
					'data' : function (obj, cb) {
						cb.call(this, data[5]);
					}
				}
			}).on('select_node.jstree', function (e, data) {
				let path = data.instance.get_path(data.node, '/');
				$("#range_importVideoFile").val(path);
				$("#nameProject").val(projet_name);
			}).bind("loaded.jstree", function (e, data) { $(this).jstree("open_all"); });
		});
	}

	upload()
	{
		$('#my_form_meshes').on('submit', function(e) {
			e.preventDefault();
			if($("#range_importMeshFile").val() != "") {
				$("#progress_import").css({"display":"inline-block"}).show();
				let $form = $(this);
				let formdata = (window.FormData) ? new FormData($form[0]) : null;
				let data = (formdata !== null) ? formdata : $form.serialize();
				$.ajax({
					url: $form.attr('action'), type: $form.attr('method'), contentType: false, processData: false, dataType: 'json', data: data,
					success: function (response) {
						for(let i = 0; i < response.length; i++) {
							let fichier = getFile(response[i]);
							if(extention(fichier) == "babylon") {
								response = response[i];
								break;
							}
						}
						global.editor.addObject.createSceneForScreenshot(response);
					}
				});
			} else {
				jAlert(lang.zones.js.alertSelectDestinationUpload, "Attention");
			}
		});
		$('#my_form_scene').on('submit', function(e) {
			e.preventDefault();
			$("#progress_import_Scene").css({"display":"inline-block"}).show();
			let $form = $(this);
			let formdata = (window.FormData) ? new FormData($form[0]) : null;
			let data = (formdata !== null) ? formdata : $form.serialize();
			$.ajax({
				url: $form.attr('action'), type: $form.attr('method'), contentType: false, processData: false, dataType: 'json', data: data,
				success: function (response) {
					for(let i = 0; i < response.length; i++) {
						let fichier = getFile(response[i]);
						if(extention(fichier) == "babylon") {
							response = response[i];
							break;
						}
					}
				}
			});
		});
		$('#my_form_image').on('submit', function (e) {
			e.preventDefault();
			let $form = $(this);
			let formdata = (window.FormData) ? new FormData($form[0]) : null;
			let data = (formdata !== null) ? formdata : $form.serialize();
			$.ajax({
				url: $form.attr('action'), type: $form.attr('method'), contentType: false, processData: false, dataType: 'json', data: data,
				success: function (response) {
					$("#dialog-import-image").dialog("close");
				}
			});
		});

		$('#my_form_music').on('submit', function (e) {
			e.preventDefault();
			let $form = $(this);
			let formdata = (window.FormData) ? new FormData($form[0]) : null;
			let data = (formdata !== null) ? formdata : $form.serialize();
			$.ajax({
				url: $form.attr('action'), type: $form.attr('method'), contentType: false, processData: false, dataType: 'json', data: data,
				success: function (response) {
					$("#dialog-import-music").dialog("close");
				}
			});
		});

		$('#my_form_sound').on('submit', function (e) {
			e.preventDefault();
			let $form = $(this);
			let formdata = (window.FormData) ? new FormData($form[0]) : null;
			let data = (formdata !== null) ? formdata : $form.serialize();
			$.ajax({
				url: $form.attr('action'), type: $form.attr('method'), contentType: false, processData: false, dataType: 'json', data: data,
				success: function (response) {
					$("#dialog-import-sound").dialog("close");
				}
			});
		});

		$('#my_form_video').on('submit', function (e) {
			e.preventDefault();
			let $form = $(this);
			let formdata = (window.FormData) ? new FormData($form[0]) : null;
			let data = (formdata !== null) ? formdata : $form.serialize();
			$.ajax({
				url: $form.attr('action'), type: $form.attr('method'), contentType: false, processData: false, dataType: 'json', data: data,
				success: function (response) {
					$("#dialog-import-video").dialog("close");
				}
			});
		});
	}
}