/*##################################################
 *                             addons.js
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 13/05/2017
 ###################################################
 */

class addonManager {

	constructor()
	{
		this.listeModule = [];
		this.rooModule = [];
		this.config = [];
	}

	loadModule() {
		$.ajaxSetup({ async: false});
		$.ajax({type: "POST", url: "./PHP/listeAddon.php", success:
			(msg) => {
				if(msg) {
					msg = msg.split(";");
					for(let i = 0, count = msg.length - 1; i < count; i++) {
						this.listeModule.push(msg[i]);
						this.rooModule.push("Addons/"+msg[i]+"/config.json");
						this.configModule("Addons/"+msg[i]+"/config.json?"+(new Date()).getTime());
					}
				}
			}
		});
		$.ajaxSetup({ async: true});
		return this.listeModule;
	}

	addModuleInterface() {
		let countModule = this.listeModule.length;
		if(countModule > 0) {
			for(let i = 0; i < countModule; i++) {
				let desinstall = '<td align="center"><p style="margin-top:10px;">-</p></td>';
				let install = '<td align="center"><p style="margin-top:10px;">-</p></td>';				
				if(this.getModuleInstalled(this.listeModule[i]) === true) {
					desinstall = '<td align="center"><p style="margin-top:10px;"><img src="Ressources/cross.png" onCLick="addon.desinstallModule(\''+this.listeModule[i]+'\');" /></p></td>';
				} else {
					install = '<td align="center"><p style="margin-top:10px;"><img src="Ressources/notes.png" onCLick="addon.installModule(\''+this.listeModule[i]+'\');" /></p></td>'
				}
				$("#liste_addon").append('<tr>'+
											'<td align="center" height="40px"><p style="margin-top:10px;">'+this.listeModule[i]+'</p></td>'+
											'<td align="left">'+this.config[i].description+'</td>'+
											install+
											desinstall+
										'</tr>');
				if(this.getModuleInstalled(this.listeModule[i]) === true) {						
					$("body").append('<script src="Addons/'+this.listeModule[i]+'/'+this.listeModule[i]+'.js?'+(new Date()).getTime()+' type="text/javascript"></script>');
				}
			}
		}
	}

	configModule(root) {
		$.getJSON(root, (data) => {
			this.config.push(data);
		});
	}

	saveModule(root, value) {
		$.ajax({type: "POST", url: "./PHP/saveAddon.php", data: "root="+root+"&isInstalled="+value});
	}

	installModule(nameModule) {
		let id = this.getIDModule(nameModule);
		this.config[id].isInstalled = true;
		this.saveModule(this.rooModule[id], true);
		jAlert(lang.projet.js.alertInstallModule, "Information");
	}

	desinstallModule(nameModule) {
		let id = this.getIDModule(nameModule);
		this.config[id].isInstalled = false;
		this.saveModule(this.rooModule[id], false);
		jAlert(lang.projet.js.alertDesinstallModule, "Information");
	}

	getModuleInstalled(nameModule) {
		let id = this.getIDModule(nameModule);		
		return this.config[id].isInstalled;
	}

	getIDModule(nameModule) {
		let idModule = 0;
		for (let i = 0, count = this.listeModule.length; i <= count; i++) {
			if(this.listeModule[i] == nameModule) {
				idModule = i;
				break;
			}
		}
		return idModule;
	}

}