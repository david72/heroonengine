/*##################################################
 *                                GE.js
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 01/01/2017
 ###################################################
 */
var lic_HE = getCookie("lic_HE"), prevKey="";
if(projet_name === false || projet_name == undefined || projet_name == null) {
    if(lic_HE === false || lic_HE == undefined || lic_HE == null) {
        window.location.href="../index.php";
    }
}
var ConfirmLeave = function() {
	$.ajax({ url: "PHP/updateInterface.php", type:'post', data:"deconnexion=1&user="+user});
	return "Are you sure?";
};
var selectedMedia = function(media) {
	mediaSelected = media;
};
var useMediaSelected = function(media) {
	mediaSelected = media;
	$("#"+assigneIn).html(mediaSelected);
	if($('#dialog-add-meshes').dialog('isOpen')) $("#dialog-add-meshes").dialog('close');
	else if($('#dialog-add-textures').dialog('isOpen')) $("#dialog-add-textures").dialog('close');
	else if($('#dialog-add-images').dialog('isOpen')) $("#dialog-add-images").dialog('close');
	else if($('#dialog-add-sounds').dialog('isOpen')) $("#dialog-add-sounds").dialog('close');
	else if($('#dialog-add-music').dialog('isOpen')) $("#dialog-add-music").dialog('close');
};
$(document).ready(function() {
	
	$.getJSON( "Data Project/editor.json", function( data ) {
		$(document).tooltip({
			classes: { "ui-tooltip": "tooltip"},
			show: { effect: "blind", duration: 200 }
		});
		if(data.options.hideInfoBull == "true") { $(document).tooltip("disable"); }
		else { $(document).tooltip("enable"); }		
	});	
	
	$.ajax({
		type: "POST", url: 'PHP/project.php', data: "projet_selected="+projet_name,
		success: function (msg) { return true; }
	});
	
	// Tabs	
	$("#HE-GE-onglet").tabs({ heightStyle: "fill", active: 4});
	$("#HE-GE-content-actors-tabs").tabs({active: 0}).css({"height":"99%"});
	$("#HE-GE-content-particules-tabs").tabs({active: 0}).css({"height":"99%"});
	$(".height-content").css({"height": $("body").height() - 40 +"px"});	
	
	if(MODE == "dist" ) {	
		$(window).on('mouseout', (function () {		
			$(document).keydown(function(e) {            
				if(e.key == "F5") {
					window.onbeforeunload = ConfirmLeave;
				} else if(e.key.toUpperCase() == "W" && prevKey == "CONTROL") {                
					window.onbeforeunload = ConfirmLeave;   
				} else if(e.key.toUpperCase() == "R" && prevKey == "CONTROL") {
					window.onbeforeunload = ConfirmLeave;
				} else if(e.key.toUpperCase() == "F4" && (prevKey == "ALT" || prevKey == "CONTROL")) {
					window.onbeforeunload = ConfirmLeave;
				}
				prevKey = e.key.toUpperCase();
			});
		}));
	}
	
	$.getJSON("_Projects/"+projet_name+"/game data/assets.json?" + (new Date()).getTime(), function(data) {	
		$('#CategorieMeshes').jstree({
			"core": {
				"check_callback": true,
				'data' : function (obj, cb) {
					cb.call(this, data[0]);				
				}
			}
		}).on('select_node.jstree', function (e, data) {
			let path = data.instance.get_path(data.node, '/');// Chemin complet du dossier clicker
			//Dans un premier temps, on vide la liste des media
			$("#listeMeshGE").html("");
			// Ensuite on vas chercher le contenue du dossier selectionner
			$.ajaxSetup({ async: false});
			let liste = '';
			$.ajax({ type: "POST",url: 'PHP/assets.php', data: "categoriesSelect="+path+"&nameProject="+projet_name,
				success: function (msg) {
					// Et on affiche le contenue dans des liens.
					msg = msg.split(";");
					let type = path.split("/");
					let myImage = null, myAsset = null;
					for(let i = 0; i < msg.length -1; i++) {
						if(type[0] == "meshes") {
							myImage = "_Projects/"+projet_name+"/"+path+"/"+no_extention(msg[i])+".png";
							myAsset = "_Projects/"+projet_name+"/"+path+"/"+msg[i];
							if(!file_exists(myImage)) myImage = "Ressources/no_image.png";
						}									
						liste += "<div style='display:inline-block;width:90px;height:90px;margin:10px;text-align:center;'>"+
									"<img src='"+myImage+"' style='width:90px;height:90px;' onclick='selectedMedia(\""+myAsset+"\");' ondblclick='useMediaSelected(\""+myAsset+"\");' /><br />"+
									"<small>"+no_extention(msg[i])+"</small>"+
								"</div>";
					}
					$("#listeMeshGE").append(liste);

				}
			});
			$.ajaxSetup({ async: true});
		}).bind("loaded.jstree", function (e, data) { $(this).jstree("open_all"); });
		
		$('#CategorieTextures').jstree({
			"core": {
				"check_callback": true,
				'data' : function (obj, cb) {
					cb.call(this, data[1]);				
				}
			}
		}).on('select_node.jstree', function (e, data) {
			let path = data.instance.get_path(data.node, '/');// Chemin complet du dossier clicker
			//Dans un premier temps, on vide la liste des media
			$("#listeTexturesGE").html("");
			// Ensuite on vas chercher le contenue du dossier selectionner
			$.ajaxSetup({ async: false});
			let liste = '';
			$.ajax({ type: "POST",url: 'PHP/assets.php', data: "categoriesSelect="+path+"&nameProject="+projet_name,
				success: function (msg) {
					// Et on affiche le contenue dans des liens.
					msg = msg.split(";");
					let type = path.split("/");
					let myImage = null;
					for(let i = 0, count = msg.length -1; i < count; i++) {					
						if(type[0] == "textures") {
							myImage = "_Projects/"+projet_name+"/"+path+"/"+msg[i];
						}					
						liste += "<div style='display:inline-block;width:90px;height:90px;margin:10px;text-align:center;'>"+
									"<img src='"+myImage+"' style='width:90px;height:90px;' onclick='selectedMedia(\""+myImage+"\");' ondblclick='useMediaSelected(\""+myImage+"\");' /><br />"+
									"<small>"+no_extention(msg[i])+"</small>"+
								"</div>";
					}
					$("#listeTexturesGE").append(liste);

				}
			});
			$.ajaxSetup({ async: true});
		}).bind("loaded.jstree", function (e, data) { $(this).jstree("open_all"); });
		
		$('#CategorieImage').jstree({
			"core": {
				"check_callback": true,
				'data' : function (obj, cb) {
					cb.call(this, data[2]);				
				}
			}
		}).on('select_node.jstree', function (e, data) {
			let path = data.instance.get_path(data.node, '/');// Chemin complet du dossier clicker
			//Dans un premier temps, on vide la liste des media
			$("#listeImagesGE").html("");
			// Ensuite on vas chercher le contenue du dossier selectionner
			$.ajaxSetup({ async: false});
			let liste = '';
			$.ajax({ type: "POST",url: 'PHP/assets.php', data: "categoriesSelect="+path+"&nameProject="+projet_name,
				success: function (msg) {
					// Et on affiche le contenue dans des liens.
					msg = msg.split(";");
					let type = path.split("/");
					let myImage = null;
					for(let i = 0, count = msg.length -1; i < count; i++) {					
						if(type[0] == "images") {
							myImage = "_Projects/"+projet_name+"/"+path+"/"+msg[i];
						}					
						liste += "<div style='display:inline-block;width:90px;height:90px;margin:10px;text-align:center;'>"+
									"<img src='"+myImage+"' style='width:90px;height:90px;' onclick='selectedMedia(\""+myImage+"\");' ondblclick='useMediaSelected(\""+myImage+"\");' /><br />"+
									"<small>"+no_extention(msg[i])+"</small>"+
								"</div>";
					}
					$("#listeImagesGE").append(liste);

				}
			});
			$.ajaxSetup({ async: true});
		}).bind("loaded.jstree", function (e, data) { $(this).jstree("open_all"); });
		
		$('#CategorieSound').jstree({
			"core": {
				"check_callback": true,
				'data' : function (obj, cb) {
					cb.call(this, data[4]);
				}			
			}
		}).on('select_node.jstree', function (e, data) {
			let path = data.instance.get_path(data.node, '/');// Chemin complet du dossier clicker
			//Dans un premier temps, on vide la liste des media
			$("#listeSoundGE").html("");
			// Ensuite on vas chercher le contenue du dossier selectionner
			$.ajaxSetup({ async: false});
			let liste = '';
			$.ajax({ type: "POST",url: 'PHP/assets.php', data: "categoriesSelect="+path+"&nameProject="+projet_name,
				success: function (msg) {
					// Et on affiche le contenue dans des liens.
					msg = msg.split(";");
					let type = path.split("/");
					let myImage = null, myAsset = null;
					for(let i = 0, count = msg.length -1; i < count; i++) {					
						if(type[0] == "sounds") {
							myImage = "Ressources/default_sound.png";
							myAsset = "_Projects/"+projet_name+"/"+path+"/"+msg[i];
						}					
						liste += "<div style='display:inline-block;width:90px;height:90px;margin:10px;text-align:center;'>"+
									"<img src='"+myImage+"' style='width:90px;height:90px;' onclick='selectedMedia(\""+myAsset+"\");' ondblclick='useMediaSelected(\""+myAsset+"\");' /><br />"+
									"<small>"+no_extention(msg[i])+"</small>"+
								"</div>";
					}
					$("#listeSoundGE").append(liste);

				}
			});
			$.ajaxSetup({ async: true});
		}).bind("loaded.jstree", function (e, data) { $(this).jstree("open_all"); });
		
		$('#CategorieMusic').jstree({
			"core": {
				"check_callback": true,			
				'data' : function (obj, cb) {
					cb.call(this, data[3]);
				}			
			}
		}).on('select_node.jstree', function (e, data) {
			let path = data.instance.get_path(data.node, '/');// Chemin complet du dossier clicker
			//Dans un premier temps, on vide la liste des media
			$("#listeMusicGE").html("");
			// Ensuite on vas chercher le contenue du dossier selectionner
			$.ajaxSetup({ async: false});
			let liste = '';
			$.ajax({ type: "POST",url: 'PHP/assets.php', data: "categoriesSelect="+path+"&nameProject="+projet_name,
				success: function (msg) {
					// Et on affiche le contenue dans des liens.
					msg = msg.split(";");
					let type = path.split("/");
					let myImage = null, myAsset = null;
					for(let i = 0, count = msg.length -1; i < count; i++) {					
						if(type[0] == "musics") {
							myImage = "Ressources/music.png";
							myAsset = "_Projects/"+projet_name+"/"+path+"/"+msg[i];
						}									
						liste += "<div style='display:inline-block;width:90px;height:90px;margin:10px;text-align:center;'>"+
									"<img src='"+myImage+"' style='width:90px;height:90px;' onclick='selectedMedia(\""+myAsset+"\");' ondblclick='useMediaSelected(\""+myAsset+"\");' /><br />"+
									"<small>"+no_extention(msg[i])+"</small>"+
								"</div>";
					}
					$("#listeMusicGE").append(liste);

				}
			});
			$.ajaxSetup({ async: true});
		}).bind("loaded.jstree", function (e, data) { $(this).jstree("open_all"); });
	});
		
});
try {
	equipe = new HEROONENGINE.serveur();
	equipe.searchUpdate();
} catch(e) { equipe = null; }