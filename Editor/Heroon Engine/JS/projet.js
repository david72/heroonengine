var intUserSelected = -1;
// Changer de language
var formatStateLang = function(opt) {
	if (!opt.id) { return opt.text; }
	let optimage = $(opt.element).data('image');
	if(!optimage){ return opt.text; }
	else { return $opt = $('<span><img src="' + optimage + '" width="23px" /> ' + opt.text + '</span>'); }
};
// Changer de theme
var formatStateTheme = function(opt) {
	if (!opt.id) { return opt.text; }
	let optimage = $(opt.element).data('color');
	if(!optimage){ return opt.text; }
	else { return $opt = $('<span> <div style="background:'+optimage+';width:20px;height:15px;display:inline-block;border:1px solid black; margin-right:5px;"></div>' + opt.text + '</span>'); }
};
$(function() {		
	
	$.getJSON("Data Project/editor.json?" + (new Date()).getTime(), function(data) {
		// Editor options
		$("#HE-LanguageDefaut").select2({
			templateResult: formatStateLang,
			templateSelection: formatStateLang,
			selectOnClose: true			
		}).change(function() {			
			$.ajax({type: "POST", url: './PHP/save.php', data: "file=editor&type=null&element=lang&value="+$("#HE-LanguageDefaut").val()+"&root=Data Project/",
				success: function (msg) {
					setCookie("HE-Lang", $("#HE-LanguageDefaut").val(), 365);
					location.reload();				
				}
			});			
		});		
		$("#HE-theme").select2({
			templateResult: formatStateTheme,
			templateSelection: formatStateTheme,
			selectOnClose: true
		}).change(function() {			
			$.ajax({type: "POST", url: './PHP/save.php', data: "file=editor&type=null&element=theme&value="+$("#HE-theme").val()+"&root=Data Project/",
				success: function (msg) {
					location.reload();
				}
			});			
		});			
		$("#HE-LanguageDefaut option").filter('[value='+String(data.lang)+']').attr('selected', true);
		if(String(data.lang) == "French") {
			$("#select2-HE-LanguageDefaut-container").attr("title", String(data.lang)).find("span").html('<img src="Lang/images/French.png" width="23px"> '+ String(data.lang));
		}
		if(String(data.lang) == "English") {
			$("#select2-HE-LanguageDefaut-container").attr("title", String(data.lang)).find("span").html('<img src="Lang/images/English.png" width="23px"> '+ String(data.lang));
		}			
		$("#HE-theme option").filter('[value='+String(data.theme)+']').attr('selected', true);
		if(String(data.theme) == "Black") {
			$("#select2-HE-theme-container").attr("title", String(data.theme)).find("span").html('<div style="background:#383838;width:20px;height:15px;display:inline-block;border:1px solid black; margin-right:5px;"></div> '+ String(data.theme));
		}
		if(String(data.theme) == "Blue") {
			$("#select2-HE-theme-container").attr("title", String(data.theme)).find("span").html('<div style="background:#c3d4e6;width:20px;height:15px;display:inline-block;border:1px solid black; margin-right:5px;"></div> '+ String(data.theme));
		}
		if(String(data.theme) == "White") {
			$("#select2-HE-theme-container").attr("title", String(data.theme)).find("span").html('<div style="background:white;width:20px;height:15px;display:inline-block;border:1px solid black; margin-right:5px;"></div> '+ String(data.theme));
		}		
		$("#HE-personnal-note").val(String(data.note));
	});		
	
	$.getJSON("_Projects/"+projet_name+"/game data/projet.json?" + (new Date()).getTime(), function(data) {		
		$("#HE-folder-public").val(String(data.update.public_folder));
		$("#HE-folder-beta").val(String(data.update.beta_folder));
		$("#HE-nbrActor-public").val(String(data.update.nbrActor_public));
		$("#HE-nbrActor-beta").val(String(data.update.nbrActor_beta));
	});
	
	//Start game	
	$("#HE-start-game").click(function() {
		window.open("_Projects/"+projet_name+"/index.php", '_blank');
	});

	//Update public
	$("#HE-update-public").click(function() {
		$.ajax({type: "POST", url: './PHP/update_game.php', data: "type=updatePublic&project="+projet_name});
	});
	$("#HE-folder-public").change(function() {
		$.ajax({type: "POST", url: './PHP/save.php', data: "file=projet&type=update&element=public_folder&value="+$("#HE-folder-public").val()+"&root=_Projects/"+projet_name+"/game data/"});
	});
	$("#HE-nbrActor-public").change(function() {
		$.ajax({type: "POST", url: './PHP/save.php', data: "file=projet&type=update&element=nbrActor_public&value="+$("#HE-nbrActor-public").val()+"&root=_Projects/"+projet_name+"/game data/"});
	});
	

	//Update bata
	$("#HE-update-beta").click(function() {
		$.ajax({type: "POST", url: './PHP/update_game.php', data: "type=updateBeta&project="+projet_name});
	});
	$("#HE-folder-beta").change(function() {
		$.ajax({type: "POST", url: './PHP/save.php', data: "file=projet&type=update&element=beta_folder&value="+$("#HE-folder-beta").val()+"&root=_Projects/"+projet_name+"/game data/"});
	});
	$("HE-nbrActor-beta").change(function() {
		$.ajax({type: "POST", url: './PHP/save.php', data: "file=projet&type=update&element=nbrActor_beta&value="+$("#HE-nbrActor-beta").val()+"&root=_Projects/"+projet_name+"/game data/"});
	});		

	//Collaborateurs
	$("#HE-add-collaborateur").click(function() {
		$("#dialog-add-collaborateur").dialog('open');
	});
	$("#dialog-add-collaborateur").dialog({
		modal: false,
		autoOpen: false,
		closeText: "",
		width: 400,
		buttons: [{
			text: "Ajouter", click: function() {				
				var newUser = $("#collaborateur_name").val();
				var passUser = $("#collaborateur_pass").val();
				$.ajax({type: "POST", url: './PHP/updateInterface.php', data: "updateCollaborateur=1&user="+newUser+"&pass="+passUser,
					success: (msg) => {
						$("#liste_collaborateur > tbody#config-collaborateur_"+$("#liste_collaborateur").find("tbody").length).append('<tr><td align="center">'+newUser+'</td>'+
						'<td align="center"><input type="checkbox" id="canEditeActeurs" value="0" /></td>'+
						'<td align="center"><input type="checkbox" id="canEditeItems" value="0" /></td>'+
						'<td align="center"><input type="checkbox" id="canEditeZones" value="1" /></td>'+
						'<td align="center"><input type="checkbox" id="canEditeParticules" value="0" /></td>'+
						'<td align="center"><input type="checkbox" id="canEditeScripts" value="0" /></td></tr>');
						$(this).dialog("close");
					}
				});
			}
		}]
	});
	
	$("#dialog-addon").dialog({
		modal: false,
		autoOpen: false,
		closeText: "",
		width: 800,
		buttons: [{
			text: "Valider", click: function() {
				
				$(this).dialog("close");
			}
		}]
	});

	$("#liste_collaborateur > tbody").click(function() {
		$(".conf").css({"background": "white"});
		idSelected = $(this).attr("id");
		$("#"+idSelected).css({"background": "#bcff8e"});
		intUserSelected = idSelected.replace("config-collaborateur_", "");
	});
	$("#HE-config-collaborateur").click(function() {
		$("#dialog-config-collaborateur").dialog('open');
	});
	$("#dialog-config-collaborateur").dialog({
		modal: false,
		autoOpen: false,
		closeText: "",
		width: 600,
		buttons: [{
			text: lang.button.js.valider, click: function() {			
				var rootElement = $("tbody#config-collaborateur_"+intUserSelected);
				$.ajax({type: "POST", url: './PHP/updateInterface.php', data: "configCollaborateur=1&userInt="+intUserSelected+"&canEditeProject="+rootElement.find("#canEditeProject").is(":checked")+"&canEditeActeurs="+rootElement.find("#canEditeActeurs").is(":checked")+"&canEditeItems="+rootElement.find("#canEditeItems").is(":checked")+"&canEditeZones="+rootElement.find("#canEditeZones").is(":checked")+"&canEditeParticules="+rootElement.find("#canEditeParticules").is(":checked")+"&canEditeInterface="+rootElement.find("#canEditeInterface").is(":checked")+"&canEditeScripts="+rootElement.find("#canEditeScripts").is(":checked"),
					success: (msg) => { $(this).dialog("close"); }
				});
			}
		}]
	});

	$.getJSON("Data Project/editor.json?" + (new Date()).getTime(), function( data ) {
		$(data.collaborateurs.users).each(function(index) {
			if(data.collaborateurs.users[index].pseudo == user) {
				if(data.collaborateurs.users[index].canEditeProject == "false") {
					$("#HE-GE-content-projet").find("input").attr("disabled", "true");
					$("#HE-GE-content-projet").find("select").attr("disabled", "true");
					$("#HE-GE-content-projet").find("button").attr("disabled", "true");
					$("#HE-GE-content-projet").find("textarea").attr("disabled", "true");
					$("#HE-GE-content-projet").find("li").attr("disabled", "true");
				}
				if(data.collaborateurs.users[index].canEditeActeurs == "false") {
					$("#HE-GE-content-actors").find("input").attr("disabled", "true");
					$("#HE-GE-content-actors").find("select").attr("disabled", "true");
					$("#HE-GE-content-actors").find("button").attr("disabled", "true");
					$("#HE-GE-content-actors").find("textarea").attr("disabled", "true");
					$("#HE-GE-content-actors").find("li").attr("disabled", "true");
					$('#hairColor').spectrum("option", "disabled", true);

					$("#HE-GE-content-articles").find("input").attr("disabled", "true");
					$("#HE-GE-content-articles").find("select").attr("disabled", "true");
					$("#HE-GE-content-articles").find("button").attr("disabled", "true");
					$("#HE-GE-content-articles").find("textarea").attr("disabled", "true");
					$("#HE-GE-content-articles").find("li").attr("disabled", "true");
					$('#color-article-diffuse').spectrum("option", "disabled", true);
					$('#color-article-spec').spectrum("option", "disabled", true);

					$("#HE-GE-content-animations").find("input").attr("disabled", "true");
					$("#HE-GE-content-animations").find("select").attr("disabled", "true");
					$("#HE-GE-content-animations").find("button").attr("disabled", "true");
					$("#HE-GE-content-animations").find("textarea").attr("disabled", "true");
					$("#HE-GE-content-animations").find("li").attr("disabled", "true");

					$("#HE-GE-content-competences").find("input").attr("disabled", "true");
					$("#HE-GE-content-competences").find("select").attr("disabled", "true");
					$("#HE-GE-content-competences").find("button").attr("disabled", "true");
					$("#HE-GE-content-competences").find("textarea").attr("disabled", "true");
					$("#HE-GE-content-competences").find("li").attr("disabled", "true");

					$("#HE-GE-content-factions").find("input").attr("disabled", "true");
					$("#HE-GE-content-factions").find("select").attr("disabled", "true");
					$("#HE-GE-content-factions").find("button").attr("disabled", "true");
					$("#HE-GE-content-factions").find("textarea").attr("disabled", "true");
					$("#HE-GE-content-factions").find("li").attr("disabled", "true");

					$("#HE-GE-content-autres").find("input").attr("disabled", "true");
					$("#HE-GE-content-autres").find("select").attr("disabled", "true");
					$("#HE-GE-content-autres").find("button").attr("disabled", "true");
					$("#HE-GE-content-autres").find("textarea").attr("disabled", "true");
					$("#HE-GE-content-autres").find("li").attr("disabled", "true");
					$('#color-text').spectrum("option", "disabled", true);
					$('#color-fond').spectrum("option", "disabled", true);
				}
				if(data.collaborateurs.users[index].canEditeItems == "false") {
					$("#HE-GE-content-items").find("input").attr("disabled", "true");
					$("#HE-GE-content-items").find("select").attr("disabled", "true");
					$("#HE-GE-content-items").find("button").attr("disabled", "true");
					$("#HE-GE-content-items").find("textarea").attr("disabled", "true");
					$("#HE-GE-content-items").find("li").attr("disabled", "true");
				}
				if(data.collaborateurs.users[index].canEditeParticules == "false") {
					$("#HE-GE-content-particules").find("input").attr("disabled", "true");
					$("#HE-GE-content-particules").find("select").attr("disabled", "true");
					$("#HE-GE-content-particules").find("button").attr("disabled", "true");
					$("#HE-GE-content-particules").find("textarea").attr("disabled", "true");
					$("#HE-GE-content-particules").find("li").attr("disabled", "true");
					$('#color-particle1').spectrum("option", "disabled", true);
					$('#color-particle2').spectrum("option", "disabled", true);
					$('#color-particle3').spectrum("option", "disabled", true);
					$('#masqueFond').spectrum("option", "disabled", true);
				}				
				if(data.collaborateurs.users[index].canEditeScripts == "false") {
					$("#HE-GE-content-scripts").find("input").attr("disabled", "true");
					$("#HE-GE-content-scripts").find("select").attr("disabled", "true");
					$("#HE-GE-content-scripts").find("button").attr("disabled", "true");
					$("#HE-GE-content-scripts").find("textarea").attr("disabled", "true");
					$("#HE-GE-content-scripts").find("li").attr("disabled", "true");
				}
				return;
			}
		});
	});

	// Note personnelle
	$("#HE-personnal-note").change(function() {
		let note = $("#HE-personnal-note").val() || $("#HE-personnal-note").text();
		$.ajax({type: "POST", url: './PHP/save.php', data: "file=editor&type=null&element=note&value="+note+"&root=Data Project/"});
	});
	
});