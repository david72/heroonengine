<?php 
/*##################################################
 *                                CheckProjet.php
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 01/01/2017
 ###################################################
 Verifie que l'utilisateur/visiteur est connecter, sinon on n'affiche pas la page du GE par securité
 Verifie qu'un projet à été créer, sinon on créer une redirection vers le manager de projet pour eviter les erreurs du GE
 */
if(!$_SESSION['user']) {
	exit("You are not allowed to access this page!");
}
if(@$_COOKIE["projet_name"] === false) {
	header("location: ../manager.php");
	exit();
}
?>