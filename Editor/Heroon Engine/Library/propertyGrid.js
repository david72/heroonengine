(function($) {
	var OTHER_GROUP_NAME = 'Autres';
	var GET_VALS_FUNC_KEY = 'pg.getValues';

	$.fn.PropertyGrid = function(obj, meta) {
		if (typeof obj === 'string' && obj === 'get') {
			if (typeof this.data(GET_VALS_FUNC_KEY) === 'function') {
				return this.data(GET_VALS_FUNC_KEY)();
			}
			return null;
		} else if (typeof obj === 'string') {
			console.error('PropertyGrid a obtenu une option non valide:', obj);
			return;
		} else if (typeof obj !== 'object' || obj === null) {
			console.error('PropertyGrid doit obtenir un objet afin d\'initialiser la grille.');
			return;
		}

		// Seems like we are ok to create the grid
		meta = meta && typeof meta === 'object' ? meta : {};
		var propertyRowsHTML = { OTHER_GROUP_NAME: '' };
		var groupsHeaderRowHTML = {};
		var postCreateInitFuncs = [];
		var getValueFuncs = {};
		var pgId = 'HE_';
		var currGroup;

		for (var prop in obj) {
			// Skip if this is not a direct property, a function, or its meta says it's non browsable
			if (!obj.hasOwnProperty(prop) || typeof obj[prop] === 'function' || (meta[prop] && meta[prop].browsable === false)) {
				continue;
			}
			// Check what is the group of the current property or use the default 'Other' group
			currGroup = (meta[prop] && meta[prop].group) || OTHER_GROUP_NAME;
			// If this is the first time we run into this group create the group row
			if (currGroup !== OTHER_GROUP_NAME && !groupsHeaderRowHTML[currGroup]) {
				groupsHeaderRowHTML[currGroup] = getGroupHeaderRowHtml(currGroup);
			}
			// Initialize the group cells html
			propertyRowsHTML[currGroup] = propertyRowsHTML[currGroup] || '';
			// Append the current cell html into the group html
			propertyRowsHTML[currGroup] += getPropertyRowHtml(pgId, prop, obj[prop], meta[prop], postCreateInitFuncs, getValueFuncs, currGroup);
		}

		// Now we have all the html we need, just assemble it
		var innerHTML = '<table class="pgTable">';
		for (var group in groupsHeaderRowHTML) {
			// Add the group row
			innerHTML += groupsHeaderRowHTML[group];
			// Add the group cells
			innerHTML += propertyRowsHTML[group];
		}
		// Close the table and apply it to the div
		innerHTML += '</table>';
		this.html(innerHTML);
		// Call the post init functions
		for (var i = 0; i < postCreateInitFuncs.length; ++i) {
			if (typeof postCreateInitFuncs[i] === 'function') {
				postCreateInitFuncs[i]();
				// just in case make sure we are not holding any reference to the functions
				postCreateInitFuncs[i] = null;
			}
		}
		// Create a function that will return tha values back from the property grid
		var getValues = function() {
			var result = {};
			for (var prop in getValueFuncs) {
				if (typeof getValueFuncs[prop] !== 'function') {continue;}
				result[prop] = getValueFuncs[prop]();
			}
			return result;
		};
		this.data(GET_VALS_FUNC_KEY, getValues);
	};

	function getGroupHeaderRowHtml(displayName) {
		return '<tr id="pgGroupRow" class="pgGroupRow_'+displayName+' PGGrid"><td colspan="2" class="pgGroupCell">' + displayName + '</td></tr>';
	}

	function getPropertyRowHtml(pgId, name, value, meta, postCreateInitFuncs, getValueFuncs, currGroup) {
		if (!name) {return '';}
		meta = meta || {};
		// We use the name in the meta if available
		var displayName = meta.name || name,
		type = meta.type || '',
		disabled = meta.disabled || '',
		multiple = meta.multiple || '',
		callback = meta.callback || '',
		elemId = pgId + name,
		valueHTML = null;
		// If boolean create checkbox
		if (type === 'boolean' || (type === '' && typeof value === 'boolean')) {
			valueHTML = '<input type="checkbox" id="' + elemId + '" value="' + name + '"' + (value ? ' checked' : '') + ' '+callback+' />';
			if (getValueFuncs) { getValueFuncs[name] = function() {return $('#'+elemId).prop('checked');}; }
		// If options create drop-down list
		} else if (type === 'options') {
			valueHTML = getSelectOptionHtml(elemId, value, meta.options, multiple, name, callback);
			if (getValueFuncs) { getValueFuncs[name] = function() {return $('#'+elemId+" option:selected").val();}; }
		// If number is loaded use it
		} else if (type === 'number') {
			var step ='step="0.01"';
			var min = max = null;
			if(meta.options) {
				if(meta.options.min || typeof meta.options.min != undefined) {
					min = 'min="'+meta.options.min+'"';
				}
				if(meta.options.max || typeof meta.options.max != undefined) {
					max = 'max="'+meta.options.max+'"';
				}
				if(meta.options.step || typeof meta.options.step != undefined) {
					step = 'step="'+meta.options.step+'"';
				}
			}
			valueHTML = '<input type="number" id="' + elemId + '" class="spinner" value="' + value + '" style="width:157px;" '+step+' '+min+' '+max+' '+callback+' />';
			//if (postCreateInitFuncs) { postCreateInitFuncs.push(initSpinner(elemId, meta.options)); }
			if (getValueFuncs) { getValueFuncs[name] = function() {return $('#'+elemId).val();}; }

		// If color and we have the spectrum color picker use it
		} else if (type === 'color' && typeof $.fn.spectrum === 'function') {
			valueHTML = '<input type="text" class="he-color" id="' + elemId + '" '+callback+' />';
			if (postCreateInitFuncs) { postCreateInitFuncs.push(initColorPicker(elemId, value, meta.options)); }
			if (getValueFuncs) {
				getValueFuncs[name] = function() {
					return $('#'+elemId).spectrum({
						showAlpha: true,
						preferredFormat: "hex"
					});
				};
			}
		// If label (for read-only)
        } else if (type === 'label') {
			if (typeof meta.description === 'string' && meta.description) {
	            valueHTML = '<label for="' + elemId + '" title="' + meta.description + '">' + value + '</label>';
	        } else {
		        valueHTML = '<label for="' + elemId + '">' + value + '</label>';
			}
		// If liste
		}
		else if (type === 'liste') {
			valueHTML = '<input type="button" id="button_'+meta.id+'" style="border:1px solid #000;width:157px;height:25px;background-color:#CCC;border-radius:6px;" value="None" '+callback+' />';
			if (getValueFuncs) { getValueFuncs[name] = function() {return $('#'+elemId).val();}; }
		// If file
		}
		else if (type === 'file') {
			valueHTML = '<input type="file" id="' + elemId + '" value="' + value + '" style="display:none;visibility:hidden" /><input type="button" style="border:1px solid #000;width:157px;height:25px;background-color:#CCC;border-radius:6px;" value="Importer un fichier" '+callback+' />';
			if (getValueFuncs) { getValueFuncs[name] = function() {return $('#'+elemId).val();}; }
		// Default is textbox
		} else {
			valueHTML = '<input type="text" id="' + elemId + '" value="' + value + '" style="width:157px;" '+callback+' />';
			if (getValueFuncs) { getValueFuncs[name] = function() {return $('#'+elemId).val();}; }
		}
		if (typeof meta.description === 'string' && meta.description && (typeof meta.showHelp === 'undefined' || meta.showHelp)) {
			displayName += '<span class="pgTooltip" title="' + meta.description + '" style="cursor:pointer;">(?)</span>';
		}
		return '<tr class="pgRow_'+currGroup+' PGGrid">'+
					'<td class="pgCell-left" id="pgCell-left-'+name+'">' + displayName + '</td>'+
					'<td class="pgCell-right" id="pgCell-right-'+name+'">' + valueHTML + '</td>'+
				'</tr>';
	}

	function getSelectOptionHtml(id, selectedValue, options, multi, name, callback) {
		var multiple = '';
		if(multi == true) multiple = "multiple";
		var html = '<select id="' + id + '" '+multiple+' style="width:157px;" '+callback+'>';
		var text, value;
		for (var i = 0; i < options.length; i++) {
			value = typeof options[i] === 'object' ? options[i].value : options[i];
			text = typeof options[i] === 'object' ? options[i].text : options[i];
			html += '<option value="' + value + '"' + (selectedValue === value ? ' selected>' : '>');
			html += text + '</option>';
		}
		html += '</select>';
		return html;
	}

	function initSpinner(id, options) {
		if (!id) {return null;}
		// Copy the options so we won't change the user "copy"
		var opts = {};
		$.extend(opts, options);

		// Add a handler to the change event to verify the min/max (only if not provided by the user)
		opts.change = typeof opts.change === 'undefined' ? onSpinnerChange : opts.change;

		return function() {
			$('#' + id).spinner(opts);
		};
	}

	function initColorPicker(id, color, options) {
		if (!id) {return null;}
		var opts = {};
		$.extend(opts, options);
		if (typeof color === 'string') {opts.color = color;}
		return function() {
			$('#' + id).spectrum(opts);
		};
	}

	function onSpinnerChange() {
		var $spinner = $(this);
		var value = $spinner.spinner('value');
		// If the value is null and the real value in the textbox is string we empty the textbox
		if (value === null && typeof $spinner.val() === 'string') {
			$spinner.val('');
			return;
		}
		// Now check that the number is in the min/max range.
		var min = $spinner.spinner('option', 'min');
		var max = $spinner.spinner('option', 'max');
		if (typeof min === 'number' && this.value < min) {
			this.value = min;
			return;
		}
		if (typeof max === 'number' && this.value > max) {
			this.value = max;
		}
	}

	function showGroup(groupName) {
		$("#pgGroupRow_"+groupName).show();
	}

	function hideGroup(groupName) {
		$("#pgGroupRow_"+groupName).hide();
	}

})(window.$);