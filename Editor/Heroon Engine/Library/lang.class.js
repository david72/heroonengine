var LANG = LANG || {};
(function ()
{	
	LANG.load = function()
	{			
		this.lang = this.getCookie("lang");
	};

	LANG.load.prototype.search = function(titre, value, composant)
	{	
		$.ajaxSetup({ scriptCharset: "ISO-8859-15", async: false});
		var resultat;
		this.data = $.getJSON("./Lang/"+this.lang+".lng.json", function(json) {				
			if(titre != "inspector") {
				$.each(json[titre], function (key, data) {			
					if(key == value) {					
						resultat = data;					
					}
				});	
			} else {
				$.each(json[titre][composant], function (key, data) {			
					if(key == value) {					
						resultat = data;					
					}
				});
			}
		});	
		return resultat;
		$.ajaxSetup({ async: true});
	};

	LANG.load.prototype.getCookie = function(name) {
		if(document.cookie.length == 0) return null;

		var regSepCookie = new RegExp('(; )', 'g');
		var cookies = document.cookie.split(regSepCookie);

		for(var i = 0; i < cookies.length; i++){
		    var regInfo = new RegExp('=', 'g');
		    var infos = cookies[i].split(regInfo);
		    if(infos[0] == name){
				return unescape(infos[1]);
		    }
		}
		return null;
   };
   
})();