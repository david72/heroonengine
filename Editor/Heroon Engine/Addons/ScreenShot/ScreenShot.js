// Namespace
var SCREEN = SCREEN || {};

// Function anonyme
(function () {
	// function in this namespace
	SCREEN.createScreenshot = function() {
		console.log("Creation d'une copie ecran");
		var engine = global.engine;		
		var camera = global.scene.activeCamera;		
		var precision = { precision: 1};
		BABYLON.Tools.CreateScreenshotUsingRenderTarget(engine, camera, precision);		
	};
	
	// Append button or append tab
	$("#positionZone > fieldset").append('<img src="Ressources/screenshot.png" style="width:22px;height:22px;display:inline;cursor:pointer;" title="ScreenShot du terrain" onCLick="SCREEN.createScreenshot();" />');
	
})();