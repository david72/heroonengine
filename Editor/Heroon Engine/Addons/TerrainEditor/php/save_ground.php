<?php
ini_set("memory_limit","1024M");
ini_set("post_max_size","1024M");
ini_set("upload_max_filesize","1024M");
ini_set("max_execution_time","300");
$type = $_POST['type'];
$ground = $_POST['terrainName'];
$element = $_POST['element'];
$extention = "_ground.babylon";
$root = "../".$_POST['root'];
$fichier = $_POST['terrainName'].$extention;
@mkdir($root.$fichier, 0777);
$value = stripslashes($_POST['value']);
$value = str_replace("Eplus", "e+", $value);
$value = str_replace("DynamicTexture_".$ground ."", "", $value);
if(!file_exists($root.$fichier)) {
	file_put_contents($root.$fichier, "{}");
}
$json = json_decode($value, true);
$save = json_encode($json);
@chmod($root.$fichier, 0777);
file_put_contents($root.$fichier, $save);
?>