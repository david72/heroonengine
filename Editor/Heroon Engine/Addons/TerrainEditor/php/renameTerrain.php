<?php
$oldTerrainName = $_POST['oldTerrainName'];
$newTerrainName = $_POST['newTerrainName'];
if(file_exists("../data/".$oldTerrainName."_ground_copy.babylon")) {
	@rename("../data/".$oldTerrainName."_ground_copy.babylon", "../data/".$newTerrainName."_ground.babylon");
	@rename("../data/".$oldTerrainName."_copy.png", "../data/".$newTerrainName.".png");
	@rename("../data/".$oldTerrainName."_copy.data", "../data/".$newTerrainName.".data");
} else {
	@rename("../data/".$oldTerrainName."_ground.babylon", "../data/".$newTerrainName."_ground.babylon");
	@rename("../data/".$oldTerrainName.".png", "../data/".$newTerrainName.".png");
	@rename("../data/".$oldTerrainName.".data", "../data/".$newTerrainName.".data");
}
?>