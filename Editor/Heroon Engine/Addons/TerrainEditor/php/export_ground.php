<?php
require_once("zip.class.php");
$terrain = $_POST['terrainName'];
$dirname = "../zip/".$terrain;
@mkdir("../zip/".$terrain, 0777);
@copy("../data/".$terrain.".png", $dirname."/".$terrain.".png");
@copy("../libs/Babylonx.ShaderBuilder.js", $dirname."/Babylonx.ShaderBuilder.js");
@copy("../libs/jquery.min.js", $dirname."/jquery.min.js");
@copy("../data/".$terrain."_ground.babylon", $dirname."/".$terrain."_ground.babylon");
@copy("../data/BaseZip/index.html", $dirname."/index.html");
$fileHTML = file_get_contents($dirname."/index.html");
$fileHTML = str_replace("var nameTerrain = null;", "var nameTerrain = \"".$terrain."\";", $fileHTML);
chmod($dirname."/index.html", 0777);
file_put_contents($dirname."/index.html", $fileHTML);
chmod($dirname."/index.html", 644);
$json = @json_decode(file_get_contents("../data/".$terrain.".data"), true);
copy("../".$json["textures"]["_1"], $dirname."/".$json["textures"]["_1"]);
@copy("../".$json["textures"]["_2"], $dirname."/".$json["textures"]["_2"]);
@copy("../".$json["textures"]["_3"], $dirname."/".$json["textures"]["_3"]);
@copy("../".$json["textures"]["_4"], $dirname."/".$json["textures"]["_4"]);
@copy("../".$json["textures"]["_5"], $dirname."/".$json["textures"]["_5"]);
@copy("../".$json["textures"]["_6"], $dirname."/".$json["textures"]["_6"]);
@copy("../".$json["textures"]["_7"], $dirname."/".$json["textures"]["_7"]);
@copy("../".$json["textures"]["_8"], $dirname."/".$json["textures"]["_8"]);
@copy("../data/".$terrain.".data", $dirname."/".$terrain.".data");
HZip::zipDir($dirname, "../".$terrain.".zip");
?>