<?php
if(@$_POST['importImage']) {
	$retour = array();
	$uploaddir = "../images/textures/".$_POST['range_importImageFile'];
	foreach ($_FILES["importImageFile"]["error"] as $key => $error) {
		if ($error == UPLOAD_ERR_OK) {
			$tmp_name = $_FILES["importImageFile"]["tmp_name"][$key];
			$name = $_FILES["importImageFile"]["name"][$key];
			move_uploaded_file($tmp_name, "$uploaddir/$name");
			$retour[] = $uploaddir."/".$name;
		}
	}
	echo json_encode($retour);
}
if(@$_POST['importMesh']) {
	$retour = array();
	$uploaddir = "../data";
	$tmp_name = $_FILES["importMeshFile"]["tmp_name"];
	$name = $_FILES["importMeshFile"]["name"];
	$name = str_replace(".babylon", "_ground.babylon", $name);
	if(!file_exists($uploaddir."/".$name)) {
		move_uploaded_file($tmp_name, "$uploaddir/$name");	
		$retour = $uploaddir."/".$name;	
		echo json_encode($retour);
	} else {
		echo json_encode("file_exists");
	}
}

?>