<?php
$terrainToCopy = $_POST['terrainToCopy'];
copy("../data/".$terrainToCopy."_ground.babylon", "../data/".$terrainToCopy."_copy_ground.babylon");
copy("../data/".$terrainToCopy.".png", "../data/".$terrainToCopy."_copy.png");
copy("../data/".$terrainToCopy.".data", "../data/".$terrainToCopy."_copy.data");
chmod("../data/".$terrainToCopy."_copy_ground.babylon", 0777);
chmod("../data/".$terrainToCopy."_copy.png", 0777);
chmod("../data/".$terrainToCopy."_copy.data", 0777);
?>