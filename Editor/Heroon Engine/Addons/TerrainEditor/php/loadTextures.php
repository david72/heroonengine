<?php
$dir = "../images/textures/";
if (is_dir($dir)) {
	if ($dh = opendir($dir)) {
		while (($file = readdir($dh)) !== false) {
			if($file != '..' && $file != '.') {
				if(!is_dir($dir.$file) && is_file($dir.$file)) {
					echo '<img src="images/textures/'.$file.'" style="width:68px;height:68px;cursor:pointer;margin-left:5px;margin-bottom:5px;border:1px solid black;" ondblClick="useMediaSelected(this.src);" />';
				}
			}
		}
		closedir($dh);
	}
}
?>