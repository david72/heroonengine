<?php
$type = @$_POST['type'];
$slotSelect = @$_POST['slotSelect'];
$root = "../data/";
$fichier = $_POST['terrainName'].".data";
if($type == false) {
	$save = '{"uvScale":{"_1":21,"_2":17,"_3":19,"_4":32,"_5":16,"_6":28,"_7":"55","_8":16},"textures":{"_1":"images\/textures\/grass_0.jpg","_2":"images\/textures\/rock_0.png","_3":"images\/textures\/rock_4.jpg","_4":"images\/textures\/ground_0.png","_5":"images\/textures\/snow_0.jpg","_6":"images\/textures\/path_0.png","_7":"images\/textures\/path_2.jpg","_8":"images\/textures\/sand_0.png"}}';
	file_put_contents($root.$fichier, $save);
} else {
	$json = @json_decode(file_get_contents($root.$fichier), true);
	$value = substr($_POST['value'], strrpos($_POST['value'], 'images/'));
	if($type == "uvScale") {
		$json["uvScale"]["_".$slotSelect] = $_POST['value'];
	}
	else if($type == "textures") {
		$json["textures"]["_".$slotSelect] = $value;
	}
	$save = json_encode($json);
	@chmod($root.$fichier, 0777);
	file_put_contents($root.$fichier, $save);
}
?>