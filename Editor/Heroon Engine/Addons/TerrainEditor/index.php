<!DOCTYPE HTML>
<html xmlns="http://wwww.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<title>TerrainEditor</title>
	<meta name="robots" content="noindex">
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<link rel="shortcut icon" href="app.ico">
	<!-- Style -->
	<link rel="stylesheet" type="text/css" href="css/editor.css">
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="css/jquery.alerts.css">
	<!-- libs JQuery -->
	<script src="libs/jquery.min.js" type="text/javascript"></script>
	<script src="libs/jquery-ui.min.js" type="text/javascript"></script>
	<script src="libs/jquery.alerts.js" type="text/javascript"></script>	
	<!-- libs Engine -->
	<script src="https://preview.babylonjs.com/babylon.js?<?php echo time();?>" type="text/javascript"></script>
	<script src="https://preview.babylonjs.com/materialsLibrary/babylon.customMaterial.min.js?<?php echo time();?>"></script>
	<script src="http://cdn.rawgit.com/NasimiAsl/Extensions/master/ShaderBuilder/Babylonx.ShaderBuilder.js" type="text/javascript"></script>	
	<script>
	var global = { canvas: null, engine: null, scene: null, editor: null, terrain: null, elevation: null, nameTerrain: null, terrainMaterial: null};
	var assigneIn = null, mediaSelected = null;
	var useMediaSelected = function(media) {
		mediaSelected = media;
		$("#"+assigneIn).attr("src", mediaSelected);
		$("#dialog-add-textures").dialog('close');
	};
	</script>
</head>
<body>
	<!-- Dialog import image -->
	<div id="dialog-import-texture" title="Import textures">
		<p><span style="color:red;">Extentions :</span> .png, .jpg, .jpeg, .gif</p>
		<form id="my_form_image" method="post" action="php/upload.php" enctype="multipart/form-data">
			<input type="text" name="range_importImageFile" id="range_importImageFile" value="" style="display:none;" />
			<input type="text" name="importImage" value="1" style="display:none;" />
			<input type="text" name="nameProject" id="nameProject" value="" style="display:none;" />
			<input type="file" name="importImageFile[]" id="importImageFile" accept=".png, .jpg, .jpeg, .gif" multiple="true" /><br />
			<button type="submit" style="margin-top:7px;">Validate</button>
		</form>
	</div>
	<!-- Dialog import mesh -->
	<div id="dialog-import-mesh" title="Import mesh terrain">
		<p><span style="color:red;">Extentions :</span> .babylon</p>
		<form id="my_form_mesh" method="post" action="php/upload.php" enctype="multipart/form-data">			
			<input type="text" name="importMesh" value="1" style="display:none;" />
			<input type="file" name="importMeshFile" id="importImageFile" accept=".babylon" multiple="false" /><br />
			<button type="submit" style="margin-top:7px;">Validate</button>
		</form>
	</div>
	<!-- Dialog add image in slot -->
	<div id="dialog-add-textures" title="Textures" style="display:none;">
		<div id="loadTextures"></div>
		<hr /><br /><button style="margin-bottom:5px;" onCLick="$('#dialog-import-texture').dialog('open')">Import textures into the library</button>
	</div>
	<!-- Menu tools ans window control terrain -->
	<div class="menu">
		<div class="tools">
			<img src="images/default_cam.png" onClick="global.editor.manipulatorGroundEditor('camera');" class="button-tools" title="Camera mode" id="tools-cam" />
			<img src="images/up_terrain.png" onClick="global.editor.manipulatorGroundEditor('up');" class="button-tools" title="Up vertex" id="tools-up" />
			<img src="images/down_terrain.png" onClick="global.editor.manipulatorGroundEditor('down');" class="button-tools" title="Down vertex" style="display:none;" id="tools-down" />
			<img src="images/plane_down_32.png" onClick="global.editor.manipulatorGroundEditor('plane');" class="button-tools" title="Plane vertex" id="tools-plane" />
			<img src="images/ramp.png" onClick="global.editor.manipulatorGroundEditor('ramp');" class="button-tools" title="create ramp" id="tools-ramp" />
			<img src="images/jaggies_32.png" onClick="global.editor.manipulatorGroundEditor('smooth');" class="button-tools" title="Smooth" id="tools-smooth" />
			<img src="images/hole.png" onClick="global.editor.manipulatorGroundEditor('hole');" class="button-tools" title="Create hole" id="tools-hole" />
			<img src="images/texture_library_32.png" onClick="global.editor.manipulatorGroundEditor('textures');" class="button-tools" title="Paint mode" id="tools-textures" />
			<img src="images/Download.png" onClick="global.editor.manipulatorGroundEditor('export');" id="exportTerrain" class="button-tools" title="Export terrain" />
			<img src="images/32_save.png" class="button-tools" title="Save terrain" style="width:28px;height:28px;" id="save_blue" />
			<img src="images/files_mini_red.gif" onClick="global.editor.manipulatorGroundEditor('save');" class="button-tools" title="Save terrain" id="save_red" style="width:28px;height:28px;display:none;" />
		</div>
		<div class="content-tools">
			<fieldset style="margin-top:5px;margin-bottom:5px;"><legend>Terrain</legend>
				<center><button style="width:90%;" id="enter-edit-mode-ground">Enter mode edite ground</button></center>
			</fieldset>
			<div id="editor-ground-tools" style="width:100%;display:none;">
				<fieldset style="margin-bottom:5px;"><legend>Manipulator</legend>
					<div id="radius-slider" style="width:100%;display:none;">
						<center style="font-size:12px;">Radius :</center>
						<div id="slider-radius" class="slider"><div id="custom-handle-radius" class="ui-slider-handle" style="text-align:center;"></div></div>
						<br />
					</div>
					<div id="all-slider">
						<center style="font-size:12px;">Hardness :</center>
						<div id="slider-hardness" class="slider"><div id="custom-handle-hardness" class="ui-slider-handle" style="text-align:center;"></div></div>
						<br />
						<center style="font-size:12px;">Strength :</center>
						<div id="slider-strength" class="slider"><div id="custom-handle-strength" class="ui-slider-handle" style="text-align:center;"></div></div>
						<br />
						<div id="height-slider">
							<center style="font-size:12px;">Height min :</center>
							<div id="slider-height-min" class="slider"><div id="custom-handle-height-min" class="ui-slider-handle" style="text-align:center;"></div></div>
							<br />
							<center style="font-size:12px;">Height max :</center>
							<div id="slider-height-max" class="slider"><div id="custom-handle-height-max" class="ui-slider-handle" style="text-align:center;"></div></div>
						</div>
						<div id="scale-slider">
							<center style="font-size:12px;">Scale :</center>
							<div id="slider-scale" class="slider"><div id="custom-handle-scale" class="ui-slider-handle" style="text-align:center;"></div></div>
						</div>
					</div>
				</fieldset>
			</div>
			<div id="editor-ground-textures" style="width:100%;display:none;">
				<fieldset><legend>Textures</legend>
					<img src="images/textures/grass_0.jpg" id="textureGround1" class="slotTexture" style="width:68px;height:68px;cursor:pointer;" ondblClick="$('#dialog-add-textures').data('opener', this).dialog('open');" />
					<img src="images/textures/rock_0.png" id="textureGround2" class="slotTexture" style="width:68px;height:68px;cursor:pointer;" ondblClick="$('#dialog-add-textures').data('opener', this).dialog('open');" />
					<img src="images/textures/ground_0.png" id="textureGround3" class="slotTexture" style="width:68px;height:68px;cursor:pointer;" ondblClick="$('#dialog-add-textures').data('opener', this).dialog('open');" />
					<img src="images/textures/grass_1.jpg" id="textureGround4" class="slotTexture" style="width:68px;height:68px;cursor:pointer;" ondblClick="$('#dialog-add-textures').data('opener', this).dialog('open');" />
					<img src="images/textures/snow_0.jpg" id="textureGround5" class="slotTexture" style="width:68px;height:68px;cursor:pointer;" ondblClick="$('#dialog-add-textures').data('opener', this).dialog('open');" />
					<img src="images/textures/path_0.png" id="textureGround6" class="slotTexture" style="width:68px;height:68px;cursor:pointer;" ondblClick="$('#dialog-add-textures').data('opener', this).dialog('open');" />
					<img src="images/textures/path_1.jpg" id="textureGround7" class="slotTexture" style="width:68px;height:68px;cursor:pointer;" ondblClick="$('#dialog-add-textures').data('opener', this).dialog('open');" />
					<img src="images/textures/sand_0.png" id="textureGround8" class="slotTexture" style="width:68px;height:68px;cursor:pointer;" ondblClick="$('#dialog-add-textures').data('opener', this).dialog('open');" />
				</fieldset>
				<fieldset><legend>Bruches</legend>
					<img src="images/brushes/brush_defaut.png" id="bruchesGround1" class="slotBruches" style="width:52px;height:52px;cursor:pointer;" />
					<img src="images/brushes/brush_0.png" id="bruchesGround2" class="slotBruches" style="width:52px;height:52px;cursor:pointer;" />
					<img src="images/brushes/brush_3.png" id="bruchesGround3" class="slotBruches" style="width:52px;height:52px;cursor:pointer;" />
					<img src="images/brushes/brush_5.png" id="bruchesGround4" class="slotBruches" style="width:52px;height:52px;cursor:pointer;" />
					<img src="images/brushes/brush_6.png" id="bruchesGround5" class="slotBruches" style="width:52px;height:52px;cursor:pointer;" />
					<img src="images/brushes/brush_7.png" id="bruchesGround6" class="slotBruches" style="width:52px;height:52px;cursor:pointer;" />
					<img src="images/brushes/brush_8.png" id="bruchesGround7" class="slotBruches" style="width:52px;height:52px;cursor:pointer;" />
					<img src="images/brushes/brush_9.png" id="bruchesGround8" class="slotBruches" style="width:52px;height:52px;cursor:pointer;" />
					<img src="images/brushes/brush_10.png" id="bruchesGround9" class="slotBruches" style="width:52px;height:52px;cursor:pointer;" />
					<img src="images/brushes/brush_11.png" id="bruchesGround10" class="slotBruches" style="width:52px;height:52px;cursor:pointer;" />
					<img src="images/brushes/brush_12.png" id="bruchesGround11" class="slotBruches" style="width:52px;height:52px;cursor:pointer;" />
					<img src="images/brushes/brush_13.png" id="bruchesGround12" class="slotBruches" style="width:52px;height:52px;cursor:pointer;" />
					<img src="images/brushes/brush_14.png" id="bruchesGround13" class="slotBruches" style="width:52px;height:52px;cursor:pointer;" />
					<img src="images/brushes/brush_16.png" id="bruchesGround14" class="slotBruches" style="width:52px;height:52px;cursor:pointer;" />
					<img src="images/brushes/brush_17.png" id="bruchesGround15" class="slotBruches" style="width:52px;height:52px;cursor:pointer;" />
				</fieldset>
			</div>
		</div>
	</div>
	<!-- Menu create terrain ans window canvas -->
	<div class="content-canvas">
		<div class="tools-canvas">
			<div style="margin-top:15px;">
			&nbsp;  Name :	<input type="text" id="name_terrain" placeholder="Island" size="5" />
					Size :  <select id="size_terrain"/>								
								<option value="250">250*250</option>
								<option value="500" selected>500*500</option>
								<option value="1000">1000*1000</option>
								<option value="2000">2000*2000</option>
							</select>
					<button id="btnCreateTerrain">Create</button>
					| Import : <button onCLick="$('#dialog-import-mesh').dialog('open')">Mesh</button>	
					| Load :  <select id="load_terrain"/>
								<option value="0">None</option>
								<?php
								$dir = "data/";
								if (is_dir($dir)) {
									if ($dh = opendir($dir)) {
										while (($file = readdir($dh)) !== false) {
											if($file != '..' && $file != '.') {
												if(!is_dir($dir.$file) && is_file($dir.$file)) {
													$fileName = pathinfo($file, PATHINFO_FILENAME);
													$fileExtention = pathinfo($file, PATHINFO_EXTENSION);
													if($fileExtention == "data") {
														echo "<option value=\"".$fileName."\">".$fileName."</option>";
													}
												}
											}
										}
										closedir($dh);
									}
								}
								?>
							</select>
					<button id="btnCopyTerrain">Copy</button>
					<button id="btnDeleteTerrain">Delete</button>											
					Rename : <input type="text" id="oldTxtRenameTerrain" value="" hidden /><input type="text" id="newTxtRenameTerrain" value=""  size="5" /> 
					<button id="btnScreenshotTerrain">Create Screenshot</button>
					| Speed camera : <input type="text" id="speedCamera" value="1.0"  size="2" />					
					<span id="throbber" style="display:none;">&nbsp;  Current record : <img src="images/throbber.gif" id="Current_record" /></span>
					<span style="float:right">Version: <?php echo file_get_contents('version.dat');?> &nbsp;  &nbsp; </span>
			</div>
		</div>
		<div class="tools-canvas-content">
			<canvas id="renderCanvas"></canvas>
		</div>
	</div>
	<script src="js/refrechDiv.js" type="text/javascript"></script>
	<script src="js/terrainControl.js" type="text/javascript"></script>
	<script src="js/terrainEditor.js" type="text/javascript"></script>	
	<script>
	global.canvas = document.getElementById("renderCanvas");
	BABYLON.SceneLoader.ShowLoadingScreen = true;
	global.engine = new BABYLON.Engine(global.canvas, true, {preserveDrawingBuffer: true});
	global.editor = new terrainEditor();
	global.editor.createScene();
	</script>
</body>
</html>