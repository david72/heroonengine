# TerrainEditor Version: 1.4.0 BETA
Editor of terrain for BabylonJS


# How to use ?
The TerrainEditor uses PHP to save data and XmlHttpRequest for load file, you must use a web server (http://). For local use, you can download EasyPHP or Wamp server.


# Functionality:

- Create new terrain.
- Sizes terrain of: 250x250, 500x500, 1000x1000 and 2000x2000. (Superior to 2000, performance problems appear.)
- Import terrain custom to paint.
- Export the terrain loaded with textures splatmap and texture painted in a zip with file html and setting.
- Support with CustomMaterial + StandardMaterial : Shadow, fog and light, color Ambiant.. on the terrain
- Delete terrain loaded.
- Copy terrain loaded.
- Rename terrain loaded.
- ScreenShot of terrain.
- Up and down vertex, Key "CTRL" for down.
- Creating plane vertex.
- Create holes Key "ALT" for filling a holes.
- Create ramps.
- Smooth meshes ground.
- Paint the terrain with an 8 textures diffuse. (Create one splatmap).
- Assign textures for paint with save and load setting. (dblClick on slot for change texture).
- Import textures in the library 
- Register your work.
- Load a terrain recorded.
- Moving the camera in "edit mode" with the keyboard spacebar and the keyboard keys "ZQSD" (FR) or "QWSD" (ENG).
- Change speed camera.


Test online [TerrainEditor](http://www.babylon.actifgames.com/TerrainEditor/index.php)

![Alt TerrainEditor](http://www.babylon.actifgames.com/TerrainEditor/TerrainEditor_v2.jpg)
![Alt TerrainWithShadow](http://www.html5gamedevs.com/uploads/monthly_2017_05/shadow_ground.jpg.3f7648345de597663307826cd6ee8d47.jpg)
