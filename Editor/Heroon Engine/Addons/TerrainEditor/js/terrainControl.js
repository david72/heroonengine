var MODE_CONTROLE = 0;
var MODE_CONTROLE_CAMERA = 1;
var MODE_CONTROLE_ELEVATE = 0;
var MODE_CONTROLE_PLANE = 0;
var MODE_CONTROLE_ELEVATE = 0;
var MODE_CONTROLE_PAINT = 0;
var MODE_CONTROLE_RAMP = 0;
var MODE_CONTROLE_HOLE = 0;
var direction = 0;

class terrainControl {

	constructor(ground)
	{
		this.ground = ground;
		this.scene = this.ground.getScene();

		this.groundSize = this.ground.metadata.size;
		if(this.groundSize <= 64) { this.multiplicateur = 5.2;}
		else if(this.groundSize <= 128) { this.multiplicateur = 2.6;}
		else if(this.groundSize <= 256) { this.multiplicateur = 1.3; }
		else if(this.groundSize <= 512) { this.multiplicateur = 0.65; }
		else if(this.groundSize <= 1024) { this.multiplicateur = 0.325; }
		else if(this.groundSize > 1024) { this.multiplicateur = 0.325; }

		this.textureSize = {};
		this.vertices_toremove = [];
		this.faces_toswitch = [];
		this.brushcanvas = null;
		this.maskcanvas = null;
		this.brushcontext = null;
		this.textureContext = null;
		this.maskcontext = null;
		this.mask = null;
		this.img = null;
		this.loadedTexture = null;
		this.textureGround = null;
		this.ctrl = false;
		this.down = false;
		this.pickFrom = true;
		this.fromPick = null;
		this.invertDirection = 1.0;
		this.textureResolution = 1024;
		this.radius = 5;
		this.opacity = 1;
		this.speed = 5;
		this.cheminImageGound = "data/"+ground.name+".png?" + (new Date()).getTime();		
	}

	speed(speed) {
		this.speed = speed;
	}
	
	attachControl()
	{
		let currentPosition = null,	pickInfo = null, timeoutMove = 0;
		// Gizmo radius bruch
		if(this.gizmo == null && MODE_CONTROLE == 1 && MODE_CONTROLE_CAMERA == 0) {
			this.radius = $("#slider-radius").slider("value");
			this.createGizmo(this.radius);
		}
		this.onBeforeRender = () => {
			this.radius = $("#slider-radius").slider("value");
			pickInfo = this.scene.pick(this.scene.pointerX, this.scene.pointerY);
			if(this.gizmo && pickInfo) {
				this.gizmo.angle = (this.radius / 79);
				if(pickInfo.pickedPoint) this.gizmo.position = new BABYLON.Vector3(pickInfo.pickedPoint.x, 50, pickInfo.pickedPoint.z);
			}
			if(this.down && pickInfo.hit && this.gizmo) {
				if (!pickInfo.hit || pickInfo.pickedMesh != this.ground) return;
				if(MODE_CONTROLE_ELEVATE > 0 && this.move) { // Up or Down vertex
					this.radius = $("#slider-radius").slider("value");
					this.elevateFaces(pickInfo, this.radius / 4, (this.speed / 25));
					global.saved = false;
				}
				else if(MODE_CONTROLE_PLANE > 0 && this.move) { // Plane vertex
					this.radius = $("#slider-radius").slider("value");
					this.elevateFaces(pickInfo, this.radius / 4, (this.speed / 25));
					global.saved = false;
				}
				else if(MODE_CONTROLE_PAINT > 0) { // Paint terrain.
					this.radius = $("#slider-radius").slider("value") * this.multiplicateur;
					if(this.img) this.paintTexture(pickInfo);					
					global.saved = false;
				}
				else if(MODE_CONTROLE_HOLE > 0 && this.move) { // Hole terrain
					this.radius = $("#slider-radius").slider("value");
					this.createHole(pickInfo, this.radius / 4, this.ctrl);
					global.saved = false;
				}
			}
			if(global.saved == false) {
				$("#save_red").show();
				$("#save_blue").hide();
			}
		};

		// Keybord
		this.onKeyDown = (evt) => {
			if(evt.keyCode == 17) {
				$("#elevationButtonUp").hide();
				$("#elevationButtonDown").show();
				this.ctrl = true;
				this.direction = -1;
			}
		};
		this.onKeyUp = (evt) => {
			if(evt.keyCode == 17) {
				$("#elevationButtonUp").show();
				$("#elevationButtonDown").hide();
				this.ctrl = false;
				this.direction = 1;
			}
		};
		// Mouse
		this.scene.onPointerDown = (evt, pickResult) => {
			if(pickResult.hit) {
				currentPosition = { x: evt.clientX, y: evt.clientY };
				this.down = true;
				if(MODE_CONTROLE_RAMP > 0) {
					if (this.pickFrom) {
						this.fromPick = pickResult;
						this.pickFrom = false;
					} else {
						this.toPick = pickResult;
						this.createRamp(this.ground, this.fromPick.pickedPoint, this.toPick.pickedPoint);  // Ramp on terrain
						this.pickFrom = true;
						global.saved = false;
					}
				}
			}
		};
		this.scene.onPointerUp = (evt, pickResult) => {
			this.ctrl = false;
			this.down = false;
			this.move = false;
		};
		this.scene.onPointerMove = (evt, pickResult) => {
			evt.preventDefault();
            if (!currentPosition) return;
			this.invertDirection = evt.button == 2 ? -1 : 1;
			currentPosition = { x: evt.clientX, y: evt.clientY };
			this.move = true;
			clearTimeout(timeoutMove);
			timeoutMove = setTimeout(function(){ this.move = false; }, 100);
		};

		//EventListener
		window.addEventListener("keydown", this.onKeyDown, false);
		window.addEventListener("keyup", this.onKeyUp, false);

		this.scene.registerBeforeRender(this.onBeforeRender);
	}

	detachControl()
	{
		this.scene.unregisterBeforeRender(this.onBeforeRender);
		if(this.gizmo) {
			this.gizmo.dispose();
			this.gizmo = null;
		}
		window.removeEventListener("keydown", this.onKeyDown);
		window.removeEventListener("keyup", this.onKeyUp);
	}

	//********************
	//Gizmo radius bruch
	//********************

	createGizmo(taille)
	{
		let size = parseInt(taille)/100;
		this.gizmo = new BABYLON.SpotLight("SpotGizmo", new BABYLON.Vector3(0, 16, 0), new BABYLON.Vector3(0, -1, 0), size, 0, global.scene);
		this.gizmo.diffuse = new BABYLON.Color3(0.76, 0, 0);
		this.gizmo.specular = new BABYLON.Color3(0.76, 0, 0);
	}

	//********************
	//UP/Down Vertices
	//********************

	prepareDataModelForElevation()
	{
		if (this.facesOfVertices == null) {
			this.facesOfVertices = [];
			this.groundVerticesPositions = this.ground.getVerticesData(BABYLON.VertexBuffer.PositionKind);
			this.groundVerticesNormals = this.ground.getVerticesData(BABYLON.VertexBuffer.NormalKind);
			this.groundIndices = this.ground.getIndices();
			this.groundPositions = [];
			for(let index = 0, countVerticesPositions = this.groundVerticesPositions.length; index < countVerticesPositions; index += 3) {
				 this.groundPositions.push(new BABYLON.Vector3(this.groundVerticesPositions[index], this.groundVerticesPositions[index + 1], this.groundVerticesPositions[index + 2]));
			}
			this.groundFacesNormals = [];
			for(let index = 0, countTotalIndice = this.ground.getTotalIndices() / 3; index < countTotalIndice; index++) {
				 this.computeFaceNormal(index);
			}
			this.getFacesOfVertices();
		}
	}

	getFaceVerticesIndex(faceID)
	{
		return {
			v1: this.groundIndices[faceID * 3],
			v2: this.groundIndices[faceID * 3 + 1],
			v3: this.groundIndices[faceID * 3 + 2]
		};
	}

	computeFaceNormal(face)
	{
		let faceInfo = this.getFaceVerticesIndex(face);
		let v1v2 = this.groundPositions[faceInfo.v1].subtract(this.groundPositions[faceInfo.v2]);
		let v3v2 = this.groundPositions[faceInfo.v3].subtract(this.groundPositions[faceInfo.v2]);
		this.groundFacesNormals[face] = BABYLON.Vector3.Normalize(BABYLON.Vector3.Cross(v1v2, v3v2));
	}

	isBoxSphereIntersected(box, sphereCenter, sphereRadius)
	{
		let vector = BABYLON.Vector3.Clamp(sphereCenter, box.minimumWorld, box.maximumWorld);
		let num = BABYLON.Vector3.DistanceSquared(sphereCenter, vector);
		return (num <= (sphereRadius * sphereRadius));
	}

	elevateFaces(pickInfo, radius, height)
	{
		this.prepareDataModelForElevation();
        this.selectedVertices = [];
        // Impact zone
        let sphereCenter = pickInfo.pickedPoint;
		sphereCenter.y = 0;
		let distance = 0;
		let position = null;
		let fullHeight = null;
		// Determine list of vertices
		for (let subIndex = 0, countSubMesh = this.ground.subMeshes.length; subIndex < countSubMesh; subIndex++)
		{
			let subMesh = this.ground.subMeshes[subIndex];
			if (!this.isBoxSphereIntersected(subMesh.getBoundingInfo().boundingBox, sphereCenter, radius)) continue;
			for(let index = subMesh.verticesStart, countVerticeStart = subMesh.verticesStart + subMesh.verticesCount; index < countVerticeStart; index++) {
				position = this.groundPositions[index];
				sphereCenter.y = position.y;
				distance = BABYLON.Vector3.Distance(position, sphereCenter);
				if (distance < radius) {
					this.selectedVertices[index] = distance;
				}
			}
		}
		if(this.direction == 0) { // Smooth vertices
			for(let selectedVertice in this.selectedVertices) {
				position = this.groundPositions[selectedVertice];
				distance = this.selectedVertices[selectedVertice];
				fullHeight = height * this.invertDirection;
				if (position.y >= distance) {
					position.y -= (fullHeight / 8) * (1.0 + (1/9 - distance / (radius * 0.9)));
				}
				this.groundVerticesPositions[selectedVertice * 3 + 1] = position.y;
				this.updateSubdivisions(selectedVertice);
			}
		} else { // Elevate vertices
			for(let selectedVertice in this.selectedVertices) {
				position = this.groundPositions[selectedVertice];
				distance = this.selectedVertices[selectedVertice];
				fullHeight = height * this.direction * this.invertDirection;

				if(MODE_CONTROLE_PLANE > 0) {
					position.y += fullHeight;
				} else {
					if (distance < radius * 0.3) {
						position.y += fullHeight;
					} else {
						position.y += fullHeight * (1.0 - (distance - radius * 0.3) / (radius * 0.7));
					}
				}
				if(position.y > this.heightMax) position.y = this.heightMax;
				else if(position.y < this.heightMin) position.y = this.heightMin;
				this.groundVerticesPositions[selectedVertice * 3 + 1] = position.y;
				this.updateSubdivisions(selectedVertice);
			}
		}
		// Normals
		this.reComputeNormals();
		// Update vertex buffer
		this.ground.updateVerticesData(BABYLON.VertexBuffer.PositionKind, this.groundVerticesPositions, true);
		this.ground.updateVerticesData(BABYLON.VertexBuffer.NormalKind, this.groundVerticesNormals, true);
	}

	reComputeNormals()
	{
		let faces = [], face;
		for(let selectedVertice in this.selectedVertices) {
			let faceOfVertices = this.facesOfVertices[selectedVertice];
			for (let index = 0; index < faceOfVertices.length; index++) faces[faceOfVertices[index]] = true;
		}
		for(let face in faces) {
			this.computeFaceNormal(face);
		}
		for(let face in faces) {
			let faceInfo = this.getFaceVerticesIndex(face);
			this.computeNormal(faceInfo.v1);
			this.computeNormal(faceInfo.v2);
			this.computeNormal(faceInfo.v3);
		}
	}

	computeNormal(vertexIndex)
	{
		let faces = this.facesOfVertices[vertexIndex], normal = BABYLON.Vector3.Zero();
		let countFaces = faces.length;
		for(let index = 0; index < countFaces; index++) {
			normal = normal.add(this.groundFacesNormals[faces[index]]);
		}
		normal = BABYLON.Vector3.Normalize(normal.scale(1.0 / countFaces));
		this.groundVerticesNormals[vertexIndex * 3] = normal.x;
		this.groundVerticesNormals[vertexIndex * 3 + 1] = normal.y;
		this.groundVerticesNormals[vertexIndex * 3 + 2] = normal.z;
	}

	updateSubdivisions(vertexIndex)
	{
		let boundingBox = null;
		let boundingSphere = null;
		for(let index = 0, countSubdivition = this.subdivisionsOfVertices[vertexIndex].length; index < countSubdivition; index++) {
			let sub = this.subdivisionsOfVertices[vertexIndex][index];
			boundingBox = sub.getBoundingInfo().boundingBox;
			boundingSphere = sub.getBoundingInfo().boundingSphere;
			if(this.groundPositions[vertexIndex].y < boundingBox.minimum.y) {
				boundingSphere.radius += Math.abs(this.groundPositions[vertexIndex].y - boundingBox.minimum.y);
				boundingBox.minimum.y = this.groundPositions[vertexIndex].y;
			} else if(this.groundPositions[vertexIndex].y > boundingBox.maximum.y) {
				boundingBox.maximum.y = this.groundPositions[vertexIndex].y;
			}
		}
		boundingBox = this.ground.getBoundingInfo().boundingBox;
		boundingSphere = this.ground.getBoundingInfo().boundingSphere;
		if(this.groundPositions[vertexIndex].y < boundingBox.minimum.y) {
			boundingSphere.Radius += Math.abs(this.groundPositions[vertexIndex].y - boundingBox.minimum.y);
			boundingBox.minimum.y = this.groundPositions[vertexIndex].y;
		} else if (this.groundPositions[vertexIndex].y > boundingBox.maximum.y) {
			boundingBox.maximum.y = this.groundPositions[vertexIndex].y;
		}
	};

	getFacesOfVertices()
	{
		this.facesOfVertices = [];
		this.subdivisionsOfVertices = [];
		for(let index = 0, countPositions = this.groundPositions.length; index < countPositions; index++) {
			this.facesOfVertices[index] = [];
			this.subdivisionsOfVertices[index] = [];
		}
		for(let index = 0, countIndice = this.groundIndices.length; index < countIndice; index++) {
			this.facesOfVertices[this.groundIndices[index]].push((index / 3) | 0);
		}
		for(let subIndex = 0, countSubMesh = this.ground.subMeshes.length; subIndex < countSubMesh; subIndex++) {
			let subMesh = this.ground.subMeshes[subIndex];
			for (index = subMesh.verticesStart; index < subMesh.verticesStart + subMesh.verticesCount; index++) {
				this.subdivisionsOfVertices[index].push(subMesh);
			}
		}
	}

	//********************
	//Ramp
	//********************

	createRamp(ground, frm, to)
	{
		let distance = BABYLON.Vector3.Distance(frm, to);
		let rampShape = BABYLON.Mesh.CreateGround("rampShape_tmp", this.radius/1.75, distance, 1, this.scene);
		let pivot = new BABYLON.Matrix();
		BABYLON.Matrix.TranslationToRef(0, 0, -distance / 2, pivot);
		rampShape.setPivotMatrix(pivot);
		rampShape.position = frm.clone();
		rampShape.lookAt(to);
		this.pullGroundOver(ground, rampShape, false);
	}

	pullGroundOver(ground, object, dontLowerGround)
	{
		object.bakeCurrentTransformIntoVertices();
		object.refreshBoundingInfo();

		let groundVertices = ground.getVerticesData(BABYLON.VertexBuffer.PositionKind);
		let objectVertices = object.getVerticesData(BABYLON.VertexBuffer.PositionKind);
		let objectIndices = object.getIndices();
		let box = object.getBoundingInfo().boundingBox;
		let outerSubtract = box.maximum.subtract(box.minimum);
		let boxWidth = Math.abs(outerSubtract.x);
		let boxHeight = Math.abs(outerSubtract.z);
		let boxLowerX = box.center.x - boxWidth / 2;
		let boxUpperX = box.center.x + boxWidth / 2;
		let boxLowerZ = box.center.z - boxHeight / 2;
		let boxUpperZ = box.center.z + boxHeight / 2;

		let candidateVertexIndices = [];
		for (let i = 0, countVerices = groundVertices.length; i < countVerices; i += 3) {
			if (groundVertices[i + 0] >= boxLowerX && groundVertices[i + 0] <= boxUpperX &&
				groundVertices[i + 2] >= boxLowerZ && groundVertices[i + 2] <= boxUpperZ) {
				candidateVertexIndices.push(i);
			}
		}
		let countCandidateVertexIndices = candidateVertexIndices.length;
		let newVerticesHeights = new Array(countCandidateVertexIndices);
		let groundVertex = BABYLON.Vector3.Zero();
		let objectVertex1 = BABYLON.Vector3.Zero();
		let objectVertex2 = BABYLON.Vector3.Zero();
		let objectVertex3 = BABYLON.Vector3.Zero();
		let rays = [];
		let rayVertexIndex = [];
		let up = new BABYLON.Vector3(0, 1, 0);
		let world = BABYLON.Matrix.Identity();

		for(let i = 0; i < countCandidateVertexIndices; i++) {
			groundVertex.x = groundVertices[candidateVertexIndices[i] + 0];
			groundVertex.y = groundVertices[candidateVertexIndices[i] + 1];
			groundVertex.z = groundVertices[candidateVertexIndices[i] + 2];
			let length = BABYLON.Vector3.Distance(groundVertex, box.center) + Math.abs(outerSubtract.y);
			let ray = new BABYLON.Ray(groundVertex, up, length);
			rays.push(BABYLON.Ray.Transform(ray, world));
			rayVertexIndex.push(i);
		}
		for(let i = 0, countObjectIndices = objectIndices.length; i < countObjectIndices; i += 3) {
			objectVertex1.x = objectVertices[objectIndices[i + 0] * 3 + 0];
			objectVertex1.y = objectVertices[objectIndices[i + 0] * 3 + 1];
			objectVertex1.z = objectVertices[objectIndices[i + 0] * 3 + 2];
			objectVertex2.x = objectVertices[objectIndices[i + 1] * 3 + 0];
			objectVertex2.y = objectVertices[objectIndices[i + 1] * 3 + 1];
			objectVertex2.z = objectVertices[objectIndices[i + 1] * 3 + 2];
			objectVertex3.x = objectVertices[objectIndices[i + 2] * 3 + 0];
			objectVertex3.y = objectVertices[objectIndices[i + 2] * 3 + 1];
			objectVertex3.z = objectVertices[objectIndices[i + 2] * 3 + 2];

			let result;
			for(let j = 0, countRays = rays.length; j < countRays; j++) {
				if (result = rays[j].intersectsTriangle(objectVertex1, objectVertex2, objectVertex3)) {
					let vertexIndex = rayVertexIndex[j];
					let newHeight = rays[j].origin.y + result.distance;
					let currentHeight = groundVertices[candidateVertexIndices[vertexIndex] + 1];
					if(dontLowerGround && newHeight < currentHeight){
						continue;
					}
					if (newVerticesHeights[vertexIndex] && newVerticesHeights[vertexIndex] > newHeight) {
						continue;
					}
					groundVertices[candidateVertexIndices[vertexIndex] + 1] = newHeight;
					newVerticesHeights[vertexIndex] = newHeight;
				}
			}
		}
		ground.updateVerticesData(BABYLON.VertexBuffer.PositionKind, groundVertices, true);		
		ground.refreshBoundingInfo();
		object.dispose();
	}

	//********************
	//Hole
	//********************

	createHole(pickInfo, radius, ctrl)
	{
        let indices_array = this.ground.getIndices();
        let positions_array = this.ground.getVerticesData(BABYLON.VertexBuffer.PositionKind);
        let index;
        let face_id;
        let safety = 10000;
        let pickpoint = pickInfo.pickedPoint;
        let current_v = BABYLON.Vector3.Zero();
        let worldmatrix = this.ground.getWorldMatrix();

		if(!this.ground.original_indices) {
			this.ground.original_indices = indices_array.slice(0);
		}
		for(let i = 0, countPositionsArray = positions_array.length/3; i < countPositionsArray; i++) {
			current_v.x = positions_array[i*3];
			current_v.y = positions_array[i*3+1];
			current_v.z = positions_array[i*3+2];
			current_v = BABYLON.Vector3.TransformCoordinates(current_v, worldmatrix);
			if(BABYLON.Vector3.Distance(pickpoint, current_v) < (radius)) {
				this.vertices_toremove.push(i);
			}
		}
		for(let i = 0, countVerticeToRemove = this.vertices_toremove.length; i < countVerticeToRemove; i++) {
			while((index = indices_array.indexOf(this.vertices_toremove[i], index+1)) != -1)
			{
				face_id = index - index%3;
				this.faces_toswitch.push(face_id);
				safety--;
				if(safety <= 0) { break; }
			}
			if(safety <= 0) { break; }
		}
		let countFacesToWitch = this.faces_toswitch.length;
		if(ctrl == false) {
			for(let i = 0; i < countFacesToWitch; i++) {
				indices_array[this.faces_toswitch[i]+2] = this.ground.original_indices[this.faces_toswitch[i]+1];
				indices_array[this.faces_toswitch[i]+1] = this.ground.original_indices[this.faces_toswitch[i]+2];
			}
		} else {
			for(let i = 0; i < countFacesToWitch; i++) {
				indices_array[this.faces_toswitch[i]+1] = this.ground.original_indices[this.faces_toswitch[i]+1];
				indices_array[this.faces_toswitch[i]+2] = this.ground.original_indices[this.faces_toswitch[i]+2];
			}
		}
		this.ground.setIndices(indices_array);
	}

	//********************
	//Paint
	//********************

	createBrush(cheminFile)
	{
		this.brushcanvas = document.createElement('canvas');
		this.brushcanvas.width = this.textureSize.width;
		this.brushcanvas.height = this.textureSize.height;
		this.brushcontext = this.brushcanvas.getContext('2d');

		this.mask = new Image();
		this.mask.crossOrigin = "Anonymous";
		this.mask.src = cheminFile;
	}

	createTextureCanvas(cheminFile)
	{
		if(this.textureGround == null) {
			this.textureGround = new BABYLON.DynamicTexture("DynamicTexture_"+global.terrain.name, this.textureResolution, global.scene, true);
			this.textureSize = this.textureGround.getSize();
			this.textureContext = this.textureGround.getContext();			
		}
		if(this.textureSize && this.textureGround) {
			this.maskcanvas = document.createElement('canvas');
			this.maskcanvas.width = this.textureSize.width;
			this.maskcanvas.height = this.textureSize.height;
			this.maskcontext = this.maskcanvas.getContext('2d');
		}
		this.textureGround.update();
		this.createBrush('images/brushes/brush_defaut.png');
		this.createTextureToPaint(cheminFile);
	}

	createTextureToPaint(cheminFile)
	{
		if(cheminFile) {
			this.img = new Image();
			this.img.crossOrigin = "Anonymous";
			this.img.src = cheminFile;
			this.img.onload = function() {};
		}
	}

	paintTexture(pickInfo)
	{
		let x = pickInfo.pickedPoint.x, z = pickInfo.pickedPoint.z;

		x = (x + (this.groundSize/2));
		x = (x - (this.ground.position.x));
		z = (z - (this.ground.position.z));
		z = ((this.groundSize/2) - z);
		x = (x/this.groundSize)*this.textureSize.width;
		z = (z/this.groundSize)*this.textureSize.height;

		if(this.brushcontext) {
			this.brushcontext.save();
			this.brushcontext.globalAlpha = this.opacity;
			this.brushcontext.clearRect(0, 0, this.textureResolution, this.textureResolution);
			//this.brushcontext.globalCompositeOperation = 'multiply';
			for(let i = 0; i < 1.0 ; i++) {
				for(let j = 0; j < 1.0 ; j++) {
					this.brushcontext.drawImage(this.img, (this.textureResolution * i), (this.textureResolution * j), this.textureResolution, this.textureResolution);
				}
			}
			this.brushcontext.restore();
		}

		this.maskcontext.save();
		this.maskcontext.clearRect(0, 0, this.textureSize.width, this.textureSize.height);
		this.maskcontext.globalAlpha = this.opacity;
		this.maskcontext.drawImage(this.mask, 0, 0, this.mask.width, this.mask.height, (x - this.radius), (z - this.radius), this.radius*2, this.radius*2);		
		this.maskcontext.globalCompositeOperation = 'source-in';
		this.maskcontext.drawImage(this.brushcanvas, 0, 0);
		//this.maskcontext.globalCompositeOperation = 'multiply';
		this.maskcontext.restore();

		this.textureContext.save();
		this.textureContext.globalAlpha = this.opacity;
		this.textureContext.drawImage(this.maskcanvas, 0, 0);		
		//this.textureContext.globalCompositeOperation = 'multiply';
		this.textureContext.restore();

		this.textureGround.update();
	}
}