var refrechDiv = function(id, url)
{
	let xhr_object = null;
	let e = document.getElementById(id);	
	if(window.XMLHttpRequest) xhr_object = new XMLHttpRequest();
	else if (window.ActiveXObject) xhr_object = new ActiveXObject("Microsoft.XMLHTTP");	
	xhr_object.open("GET", url, true);
	xhr_object.onreadystatechange = function() {
		if(xhr_object.readyState < 4) {}	
		if(xhr_object.readyState == 4 && xhr_object.status == 200)
		{	
			let sourceHTML = xhr_object.responseText;			
			sourceHTML = sourceHTML.replace(/&(l|g|quo)t;/g, function(a,b){ return {l:'<', g:'>', quo:'"' }[b];});			
			e.innerHTML = sourceHTML;		
			let scripts = e.getElementsByTagName('script');
			for(let i = 0, count = scripts.length; i < count; i++) {				
				if (window.execScript) {				
					window.execScript(scripts[i].text.replace('<!--','')); 
				} else {
					window.eval(scripts[i].text);
				}
			}
		}		
	};	
	window.onunload = function() {
		delete(xhr_object);
	};
	xhr_object.send(null);
};