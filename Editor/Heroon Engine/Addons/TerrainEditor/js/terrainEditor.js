class terrainEditor {

	constructor() 
	{
		global.engine.enableOfflineSupport = false;
		BABYLON.Engine.ShadersRepository = "shaders/";
		BABYLONX.ShaderBuilder.InitializeEngine();

		this.jquery();
		this.cameraOfEditor = null;
		this.skybox = null;
		this.light = null;
		this.nameTerrain = null;
		this.selectedIndexGround = 0;
		this.lines = [];
		this.slotSelecte = 1;
		this.slotNumero = 0;
		this.grid = null;
	}

	createScene() 
	{
		global.scene = new BABYLON.Scene(global.engine);
		global.scene.ambientColor = new BABYLON.Color3(0.33, 0.33, 0.33);
		global.scene.clearColor = new BABYLON.Color3(0.39, 0.58, 0.93);

		this.light = new BABYLON.DirectionalLight("light_defaut1", new BABYLON.Vector3(-2, -2, 2), global.scene);
		this.light.position.y = 200;
		this.light.diffuse = new BABYLON.Color3(1, 1, 1);
		this.light.specular = new BABYLON.Color3(0.3, 0.3, 0.3);
		this.light.intensity = 1.1;

		let light2 = new BABYLON.DirectionalLight("light_defaut2", new BABYLON.Vector3(0.2, -2, -0.2), global.scene);
		light2.position.y = 200;
		light2.diffuse = new BABYLON.Color3(1, 1, 1);
		light2.specular = new BABYLON.Color3(0.3, 0.3, 0.3);
		light2.intensity = 1.1;

		//this.shadowGenerator = new BABYLON.ShadowGenerator(1024, this.light);
		//this.shadowGenerator.setDarkness(0.5);

		this.createCameraEditor();
		this.createSkyBox(2048);
		this.createGrilleLevelWater(128);

		global.engine.runRenderLoop(() => {
			if(global.scene.isReady()) {
				global.scene.render();
				if(this.skybox) {
					this.skybox.rotation.y += 0.0003 * global.scene.getAnimationRatio();// rotation du ciel
				}
			}
        });

		window.onresize = function() { global.engine.resize(); };
	}

	createCameraEditor() {
		this.cameraOfEditor = new BABYLON.FreeCamera("cameraOfEditor", new BABYLON.Vector3(0, 70, -256), global.scene);
		global.scene.activeCamera = this.cameraOfEditor;
		this.cameraOfEditor.attachControl(global.canvas, true);
		this.cameraOfEditor.doNotSerialize = true;
		this.cameraOfEditor.ellipsoid = null;
		// ENG
		this.cameraOfEditor.keysUp.push(87); // W
		this.cameraOfEditor.keysLeft.push(65); // A
		// FR
		this.cameraOfEditor.keysUp.push(90); // Z
		this.cameraOfEditor.keysLeft.push(81); // Q
		this.cameraOfEditor.keysDown.push(83); // S
		this.cameraOfEditor.keysRight.push(68); // D
		this.cameraOfEditor.minZ = 0;
		this.cameraOfEditor.maxZ = 600000;
	}
	
	getFile(fullPath) { 
		if(fullPath) {
			let filename = fullPath.replace(/^.*[\\\/]/, '');
			return filename;
		}
	}

	_materialGround(isLoaded) 
	{
		let cheminImageGound = null;
		let rangeSt = -0.4;
		let rangePo = 0.1;
		
		$.ajaxSetup({ async: false});
		if(isLoaded == false) {
			cheminImageGound = "images/textures/empty.png";
		} else {
			cheminImageGound = global.elevation.cheminImageGound;
		}
		
		global.elevation.createTextureCanvas("images/color/red.jpg");
		
		global.elevation.loadedTexture = new Image();
		global.elevation.loadedTexture.crossOrigin = "Anonymous";
		global.elevation.loadedTexture.src = cheminImageGound;
		global.elevation.loadedTexture.onload = () =>
		{
			global.elevation.textureContext.save();
			global.elevation.textureContext.clearRect(0, 0, global.elevation.textureSize.width, global.elevation.textureSize.height);
			global.elevation.textureContext.drawImage(global.elevation.loadedTexture, 0, 0);
			global.elevation.textureContext.restore();
			global.elevation.textureGround.update();					
		
			global.terrainMaterial = new BABYLON.CustomMaterial('terrainMaterial', global.scene);
			
			$.getJSON("data/"+global.terrain.name+".data?"+(new Date()).getTime(), (data) => {							
				let root = "images/textures/";			
				let sresult = BABYLONX.Shader.Join(["vec2 rotate_xy(vec2 pr1, vec2 pr2, float alpha) {vec2 pp2 = vec2( pr2.x - pr1.x, pr2.y - pr1.y ); return vec2( pr1.x + pp2.x * cos(alpha*3.14159265/180.) - pp2.y * sin(alpha*3.14159265/180.), pr1.y + pp2.x * sin(alpha*3.14159265/180.) + pp2.y * cos  (alpha*3.14159265/180.)); } \n vec3 r_y(vec3 n, float a, vec3 c) {vec3 c1 = vec3( c.x, c.y,  c.z ); c1.x = c1.x; c1.y = c1.z; vec2 p = rotate_xy(vec2(c1.x, c1.y), vec2( n.x, n.z ), a); n.x = p.x; n.z = p.y; return n;  } \n vec3 r_x(vec3 n, float a, vec3 c) {vec3 c1 = vec3( c.x, c.y,  c.z ); c1.x = c1.y; c1.y = c1.z; vec2 p = rotate_xy(vec2(c1.x, c1.y), vec2( n.y, n.z ), a); n.y = p.x; n.z = p.y; return n;  } \n vec3 r_z(vec3 n, float a, vec3 c) { vec3 c1 = vec3( c.x, c.y,  c.z ); vec2 p = rotate_xy(vec2(c1.x, c1.y), vec2( n.x, n.y ), a); n.x = p.x; n.y = p.y; return n;  }", "vec3 normalMap() { vec4 result = vec4(0.);  return result.xyz; }"]);     
						
				global.terrainMaterial.AddUniform('dyTexture', 'sampler2D');				
				global.terrainMaterial.AddUniform('texture1', 'sampler2D');
				global.terrainMaterial.AddUniform('texture2', 'sampler2D');
				global.terrainMaterial.AddUniform('texture3', 'sampler2D');
				global.terrainMaterial.AddUniform('texture4', 'sampler2D');
				global.terrainMaterial.AddUniform('texture5', 'sampler2D');
				global.terrainMaterial.AddUniform('texture6', 'sampler2D');
				global.terrainMaterial.AddUniform('texture7', 'sampler2D'); 
				global.terrainMaterial.AddUniform('texture8', 'sampler2D');		
				
				global.terrainMaterial.AddUniform('scaleTxt1', 'vec2');
				global.terrainMaterial.AddUniform('scaleTxt2', 'vec2');
				global.terrainMaterial.AddUniform('scaleTxt3', 'vec2');
				global.terrainMaterial.AddUniform('scaleTxt4', 'vec2');
				global.terrainMaterial.AddUniform('scaleTxt5', 'vec2');
				global.terrainMaterial.AddUniform('scaleTxt6', 'vec2');
				global.terrainMaterial.AddUniform('scaleTxt7', 'vec2');
				global.terrainMaterial.AddUniform('scaleTxt8', 'vec2');			
			
				global.terrainMaterial.onBindObservable.add(() => {        
					if (global.terrainMaterial.getEffect && global.terrainMaterial.getEffect()) {
						global.terrainMaterial.getEffect().setTexture('dyTexture', global.elevation.textureGround);
						global.terrainMaterial.getEffect().setTexture('texture1', new BABYLON.Texture(root+this.getFile(data.textures._1), global.scene));
						global.terrainMaterial.getEffect().setTexture('texture2', new BABYLON.Texture(root+this.getFile(data.textures._2), global.scene));
						global.terrainMaterial.getEffect().setTexture('texture3', new BABYLON.Texture(root+this.getFile(data.textures._3), global.scene));
						global.terrainMaterial.getEffect().setTexture('texture4', new BABYLON.Texture(root+this.getFile(data.textures._4), global.scene));
						global.terrainMaterial.getEffect().setTexture('texture5', new BABYLON.Texture(root+this.getFile(data.textures._5), global.scene));
						global.terrainMaterial.getEffect().setTexture('texture6', new BABYLON.Texture(root+this.getFile(data.textures._6), global.scene));
						global.terrainMaterial.getEffect().setTexture('texture7', new BABYLON.Texture(root+this.getFile(data.textures._7), global.scene));
						global.terrainMaterial.getEffect().setTexture('texture8', new BABYLON.Texture(root+this.getFile(data.textures._8), global.scene));					
						
						global.terrain.material.getEffect().setVector2('scaleTxt1', {x: parseFloat(data.uvScale._1), y: parseFloat(data.uvScale._1)});
						global.terrain.material.getEffect().setVector2('scaleTxt2', {x: parseFloat(data.uvScale._2), y: parseFloat(data.uvScale._2)});
						global.terrain.material.getEffect().setVector2('scaleTxt3', {x: parseFloat(data.uvScale._3), y: parseFloat(data.uvScale._3)});
						global.terrain.material.getEffect().setVector2('scaleTxt4', {x: parseFloat(data.uvScale._4), y: parseFloat(data.uvScale._4)});
						global.terrain.material.getEffect().setVector2('scaleTxt5', {x: parseFloat(data.uvScale._5), y: parseFloat(data.uvScale._5)});
						global.terrain.material.getEffect().setVector2('scaleTxt6', {x: parseFloat(data.uvScale._6), y: parseFloat(data.uvScale._6)});
						global.terrain.material.getEffect().setVector2('scaleTxt7', {x: parseFloat(data.uvScale._7), y: parseFloat(data.uvScale._7)});
						global.terrain.material.getEffect().setVector2('scaleTxt8', {x: parseFloat(data.uvScale._8), y: parseFloat(data.uvScale._8)});					
					}
				});
				
				global.terrainMaterial.Fragment_Definitions(sresult+' vec4 SB(vec3 pos, vec3 nrm, vec2 vuv) { vec4 result = vec4(0.); vec3 center = vec3(0.); '+
				new BABYLONX.ShaderBuilder().Map({alpha: true, index: 'dyTexture', bias:0.}).Reference(1)
				.SetUniform('dyTexture', 'sampler2D').SetUniform('texture1', 'sampler2D').SetUniform('texture2', 'sampler2D').SetUniform('texture3', 'sampler2D').SetUniform('texture4', 'sampler2D').SetUniform('texture5', 'sampler2D').SetUniform('texture6', 'sampler2D').SetUniform('texture7', 'sampler2D').SetUniform('texture8', 'sampler2D')
				.SetUniform('scaleTxt1', 'vec2').SetUniform('scaleTxt2', 'vec2').SetUniform('scaleTxt3', 'vec2').SetUniform('scaleTxt4', 'vec2').SetUniform('scaleTxt5', 'vec2').SetUniform('scaleTxt6', 'vec2').SetUniform('scaleTxt7', 'vec2').SetUniform('scaleTxt8', 'vec2')
				.Black(1, BABYLONX.Helper().Map({alpha: true, index: 'texture1', uv: 'vec2(vuv*'+String(data.uvScale._8)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
				.Red(1, BABYLONX.Helper().Map({alpha: true, index: 'texture2', uv:'vec2(vuv*'+String(data.uvScale._1)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
				.Green(1, BABYLONX.Helper().Map({alpha: true, index: 'texture3', uv: 'vec2(vuv*'+String(data.uvScale._2)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
				.Blue(1, BABYLONX.Helper().Map({alpha: true, index: 'texture4', uv: 'vec2(vuv*'+String(data.uvScale._3)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
				.Yellow(1, BABYLONX.Helper().Map({alpha: true, index: 'texture5', uv: 'vec2(vuv*'+String(data.uvScale._4)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
				.Cyan(1, BABYLONX.Helper().Map({alpha: true, index: 'texture6', uv: 'vec2(vuv*'+String(data.uvScale._5)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
				.Pink(1, BABYLONX.Helper().Map({alpha: true, index: 'texture7', uv: 'vec2(vuv*'+String(data.uvScale._6)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
				.White(1, BABYLONX.Helper().Map({alpha: true, index: 'texture8', uv: 'vec2(vuv*'+String(data.uvScale._7)+'.)'}).Build(), {rangeStep: rangeSt, rangePower: rangePo})
				.DisableAlphaTesting().Body +' return result; }');
				
				global.terrainMaterial.diffuseTexture = new BABYLON.Texture(root+"blank.png", global.scene);			
				global.terrainMaterial.Fragment_Custom_Diffuse('result = SB(vPositionW,vNormalW,vDiffuseUV).xyz;'); 
				
				global.terrain.material = global.terrainMaterial;
				
				// Assign slot used
				$("#textureGround1").attr("src", root+this.getFile(data.textures._1));
				$("#textureGround2").attr("src", root+this.getFile(data.textures._2));
				$("#textureGround3").attr("src", root+this.getFile(data.textures._3));
				$("#textureGround4").attr("src", root+this.getFile(data.textures._4));
				$("#textureGround5").attr("src", root+this.getFile(data.textures._5));
				$("#textureGround6").attr("src", root+this.getFile(data.textures._6));
				$("#textureGround7").attr("src", root+this.getFile(data.textures._7));
				$("#textureGround8").attr("src", root+this.getFile(data.textures._8));				
			});
		}
		$.ajaxSetup({ async: true});		
		
		$("#textureGround1").css({"border":"1px solid red"});
	}

	loadGround(nameTerrain)
	{
		if(this.getSaved() == false) return;
		if(nameTerrain == "0") return;
		if(nameTerrain == global.nameTerrain) return;
		this.selectedIndexGround = $("#load_terrain").prop('selectedIndex');
		//global.engine.displayLoadingUI();
        this.disposeGroundAndRecreate();		
		let fileBabylon = null;
		if(file_exists("data/"+nameTerrain+"_ground.babylon")) {
			fileBabylon = nameTerrain+"_ground.babylon";
			global.nameTerrain = nameTerrain;
			this.nameTerrain = nameTerrain;		
		} else if(file_exists("data/"+nameTerrain+"_copy_ground.babylon")) {
			fileBabylon = nameTerrain+"_copy_ground.babylon";
			global.nameTerrain = nameTerrain+"_copy";
			this.nameTerrain = nameTerrain+"_copy";			
		}
		BABYLON.SceneLoader.ImportMesh("", "data/", fileBabylon+"?" + (new Date()).getTime(), global.scene, (newMeshes) => {
			global.terrain = newMeshes[0];			
			global.terrain.name = global.nameTerrain;			
			if(global.terrain.metadata !== null) {
				let options = global.terrain.metadata;
				this.cameraOfEditor.position.z = -(parseInt(options.size)/2);
				this.resizeGrilleLevelWater(parseInt(options.size) * 2);
			} else {
				let size = parseInt(global.terrain.getBoundingInfo().boundingBox.extendSizeWorld.x);
				global.terrain.metadata = {size: parseInt(global.terrain.getBoundingInfo().boundingBox.extendSizeWorld.x)};	
				this.cameraOfEditor.position.z = -(parseInt(size)/2);
				this.resizeGrilleLevelWater(parseInt(size));
				this.skybox.dispose();
				this.skybox = null;
				let sizeSkybox = parseInt(global.terrain.getBoundingInfo().boundingBox.extendSizeWorld.x)*3;
				this.createSkyBox(sizeSkybox);
			}			
			global.terrain.position.y = 0;
			global.terrain.isPickable = false;
			global.elevation = new terrainControl(global.terrain);
			this._materialGround(true);
			global.terrain.markVerticesDataAsUpdatable(BABYLON.VertexBuffer.PositionKind, true);
			global.terrain.markVerticesDataAsUpdatable(BABYLON.VertexBuffer.NormalKind, true);			
			$("#oldTxtRenameTerrain").val(global.terrain.name);
			$("#newTxtRenameTerrain").val(global.terrain.name);			
			global.engine.hideLoadingUI();			
		});
	}

	createGround(nameTerrain, options)
	{
		if(this.getSaved() == false) return;
        this.disposeGroundAndRecreate();
		let subdivision = parseInt(options.size) / 4;
		this.cameraOfEditor.position.z = -(parseInt(options.size)/2);
		global.terrain = BABYLON.Mesh.CreateGround(nameTerrain, parseInt(options.size), parseInt(options.size), subdivision, global.scene, true);
		this.resizeGrilleLevelWater(parseInt(options.size) * 2);
		global.terrain.checkCollisions = false;
		global.terrain.isPickable = false;
		global.terrain.position.y = 0;
		global.terrain.metadata = {size: parseInt(options.size)};
		this.nameTerrain = nameTerrain;
		global.nameTerrain = this.nameTerrain;	
		this.createParamTerrain();
		global.elevation = new terrainControl(global.terrain);
		this._materialGround(false);
		$("#load_terrain").append('<option id="'+nameTerrain+'" value="'+nameTerrain+'">'+nameTerrain+'</option>');
		$("#oldTxtRenameTerrain").val(nameTerrain);
		$("#newTxtRenameTerrain").val(nameTerrain);
	}
	
	importGround(nameTerrain, root, file)
	{		
		if(this.getSaved() == false) return;
		global.engine.displayLoadingUI();
        this.disposeGroundAndRecreate();		
		BABYLON.SceneLoader.ImportMesh("", root, file+"?" + (new Date()).getTime(), global.scene, (newMeshes) => {
			global.terrain = newMeshes[0];
			global.terrain.name = nameTerrain;
			global.terrain.position.y = 0;
			//global.terrain.scaling = new BABYLON.Vector3(0.3, 0.3, 0.3);		
			this.cameraOfEditor.position.z = 0;
			global.terrain.checkCollisions = false;			
			global.terrain.metadata = {size: parseInt(global.terrain.getBoundingInfo().boundingBox.extendSizeWorld.x)};			
			this.skybox.dispose();
			this.skybox = null;
			let size = parseInt(global.terrain.getBoundingInfo().boundingBox.extendSizeWorld.x)*3;
			this.createSkyBox(size);
			global.terrain.isPickable = false;
			this.nameTerrain = nameTerrain;
			global.nameTerrain = this.nameTerrain;	
			this.createParamTerrain();
			global.elevation = new terrainControl(global.terrain);			
			this._materialGround(false);			
			global.terrain.markVerticesDataAsUpdatable(BABYLON.VertexBuffer.PositionKind, true);
			global.terrain.markVerticesDataAsUpdatable(BABYLON.VertexBuffer.NormalKind, true);
			global.engine.hideLoadingUI();
			$("#load_terrain").append('<option id="'+nameTerrain+'" value="'+nameTerrain+'">'+nameTerrain+'</option>');
			$("#oldTxtRenameTerrain").val(nameTerrain);
			$("#newTxtRenameTerrain").val(nameTerrain);	
			this.resizeGrilleLevelWater(parseInt(global.terrain.getBoundingInfo().boundingBox.extendSizeWorld.x));
		});
	}

	disposeGroundAndRecreate() 
	{
		if(global.terrain) {			
			global.terrain.dispose();
			global.terrain = null;
		}
	}

	getSaved()
	{
		if(global.saved == false) {
			jConfirm("Would you like to register before ?", "Confirmation", (conf) => {
				if(conf) {
					$("#select-zone").prop('selectedIndex', parseInt(this.selectedIndexGround));
					this.saveScene(false);
					return true;
				} else {
					$("#select-zone").prop('selectedIndex', parseInt(this.selectedIndexGround));
					return false;
				}
			});
			return false;
		} else {
			return true;
		}
	}
	
	createSkyBox(size)
	{
		this.skybox = BABYLON.Mesh.CreateBox("skyBox", size, global.scene);
		let skyboxMaterial = new BABYLON.StandardMaterial("skyBox", global.scene);
		skyboxMaterial.backFaceCulling = false;
		skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("data/skybox/TropicalSunnyDay", global.scene);
		skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
		skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
		skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
		skyboxMaterial.disableLighting = true;
		this.skybox.material = skyboxMaterial;
	}

	manipulatorGroundEditor(type) 
	{
		if(global.terrain == null) return;
		if(type == "update") {
			this.updateScene();
		}
		if(global.terrain.isPickable == false) return;
		if(type == "export") {
			this.saveScene(true);
		} else if(type == "save") {
			this.saveScene(false);
		}
		if(global.elevation) {
			MODE_CONTROLE = 0;
			MODE_CONTROLE_CAMERA = 1;
			MODE_CONTROLE_ELEVATE = 0;
			MODE_CONTROLE_PLANE = 0;
			MODE_CONTROLE_ELEVATE = 0;
			MODE_CONTROLE_PAINT = 0;
			MODE_CONTROLE_RAMP = 0;
			global.elevation.direction = 0;
		}
		switch(type) {
			case "camera":
				global.elevation.detachControl();
				global.scene.activeCamera.attachControl(global.canvas, true);
				$("#editor-ground-tools, #editor-ground-textures").hide();
			break;
			case "up":
				$("#editor-ground-tools, #all-slider, #height-slider, #radius-slider").show();
				$("#editor-ground-textures, #scale-slider").hide();
				MODE_CONTROLE = 1;
				MODE_CONTROLE_ELEVATE = 1;
				MODE_CONTROLE_CAMERA = 0;
				global.elevation.direction = 1;
				global.elevation.attachControl();
				global.scene.activeCamera.detachControl(global.canvas);
			break;
			case "down":
				$("#editor-ground-tools, #all-slider, #height-slider, #radius-slider").show();
				$("#editor-ground-textures, #scale-slider").hide();
				MODE_CONTROLE = 1;
				MODE_CONTROLE_ELEVATE = 1;
				MODE_CONTROLE_CAMERA = 0;
				global.elevation.direction = -1;
				global.elevation.attachControl();
				global.scene.activeCamera.detachControl(global.canvas);
			break;
			case "plane":
				$("#editor-ground-tools, #all-slider, #height-slider, #radius-slider").show();
				$("#editor-ground-textures, #scale-slider").hide();
				MODE_CONTROLE = 1;
				MODE_CONTROLE_PLANE = 1;
				MODE_CONTROLE_CAMERA = 0;
				global.elevation.direction = 1;
				global.elevation.attachControl();
				global.scene.activeCamera.detachControl(global.canvas);
			break;
			case "smooth":
				$("#editor-ground-tools, #all-slider, #radius-slider").show();
				$("#editor-ground-textures, #scale-slider, #height-slider").hide();
				MODE_CONTROLE = 1;
				MODE_CONTROLE_ELEVATE = 1;
				MODE_CONTROLE_CAMERA = 0;
				global.elevation.attachControl();
				global.scene.activeCamera.detachControl(global.canvas);
			break;
			case "textures":
				$("#editor-ground-tools, #all-slider, #editor-ground-textures, #scale-slider, #radius-slider").show();
				$("#height-slider").hide();
				MODE_CONTROLE = 1;
				MODE_CONTROLE_PAINT = 1;
				MODE_CONTROLE_CAMERA = 0;
				global.elevation.attachControl();
				global.scene.activeCamera.detachControl(global.canvas);
			break;
			case "hole":
				$("#editor-ground-tools, #scale-slider, #radius-slider").show();
				$("#all-slider, #editor-ground-textures").hide();
				MODE_CONTROLE = 1;
				MODE_CONTROLE_HOLE = 1;
				MODE_CONTROLE_CAMERA = 0;
				global.elevation.attachControl();
				global.scene.activeCamera.detachControl(global.canvas);
				jAlert('Mode create hole active', 'Information');
			break;
			case "ramp":
				$("#editor-ground-tools, #radius-slider").show();
				$("#all-slider, #editor-ground-textures").hide();
				MODE_CONTROLE = 1;
				MODE_CONTROLE_RAMP = 1;
				MODE_CONTROLE_CAMERA = 0;
				global.elevation.attachControl();
				global.scene.activeCamera.detachControl(global.canvas);
			break;
		}
	}

	saveScene(isExport) 
	{
		$("#throbber").show();
		global.nameTerrain = this.nameTerrain;		
		let rootSaveScene = "data/";
		let rootSaveImageGround = "data/";
		if(global.nameTerrain) {
			let serialization = BABYLON.SceneSerializer.SerializeMesh(global.terrain);
			serialization.materials = [];
			let str_serialized = JSON.stringify(serialization);
			str_serialized = str_serialized.replace("e+", "Eplus");
			$.ajaxSetup({ async: false});
			// Save terrain
			$.ajax({type: "POST", url: './php/save_ground.php', data: "terrainName="+global.nameTerrain+"&type=null&element=null&value="+str_serialized+"&root="+rootSaveScene,
				success : () => {
					$("#throbber, #save_red").hide();
					$("#save_blue").show();
					// Save image paint of terrain
					if(global.elevation.textureGround) {
						$.ajax({type: "POST", url: "./php/genereImage.php", data: "nameImage="+global.nameTerrain+"&imgbase64="+global.elevation.textureGround._canvas.toDataURL()+"&root="+rootSaveImageGround,
							success : () => {
								global.saved = true;
								if(isExport == true) { this.exportScene(global.nameTerrain); }
							}
						});
					}
				}
			});
			$.ajaxSetup({ async: true});
		}
	}

	// For the module HeroonEngine
	updateScene() 
	{
		$.ajaxSetup({ async: false});
		this.saveScene(false);
		$.ajax({type: "POST", url: "./php/updateHeroonEngine.php", data: "terrainName="+global.nameTerrain,
			success : function() {
				let oldNameTerrain = $("#oldTxtRenameTerrain").val();
				let newNameTerrain = $("#newTxtRenameTerrain").val();
				window.opener.global.editor.updateTerrain(global.terrain, oldNameTerrain, newNameTerrain, false);
			}
		});
		$.ajaxSetup({ async: true});
	}

	exportScene(nameTerrain)
	{
		// Export terrain and texture in zip
		$.ajax({type: "POST", url: "./php/export_ground.php", data: "terrainName="+nameTerrain,
			success: function() {
				let link = document.createElement("a");
				link.id = "export";
				link.download = nameTerrain+".zip";
				link.href = nameTerrain+".zip";
				link.click();
				$.ajax({type: "POST", url: "./php/clear_export.php", data: "terrainName="+nameTerrain, success: function() { $("#export").remove(); }});
			}
		});
	}

	closeEditeGround() 
	{
		if(global.terrain.isPickable == true) {
			$("#editor-ground-tools, #editor-ground-textures").hide();
			$("#enter-edit-mode-ground").text("Enter mode edite ground").css({"background":"#e4e4e4"});
			global.terrain.isPickable = false;
			global.scene.activeCamera.attachControl(global.canvas, true);
			if(global.elevation) {
				global.elevation.detachControl();
				MODE_CONTROLE = 0;
				MODE_CONTROLE_CAMERA = 1;
				MODE_CONTROLE_PLANE = 0;
				MODE_CONTROLE_ELEVATE = 0;
				MODE_CONTROLE_PAINT = 0;
				MODE_CONTROLE_HOLE = 0;
				MODE_CONTROLE_RAMP = 0;
				direction = 0;
				$(".button-tools").css({"border": "none"});
			}
		}
	}

	createGrilleLevelWater(size)
	{
		this.grid = null;
		this.lines = [];
		let sizeGrid = (size / 2);
		this.getSizeGrid = sizeGrid;
		for(let i1 = -sizeGrid; i1 <= sizeGrid; i1+=6.4) {
			this.lines.push([new BABYLON.Vector3(i1, 0, -sizeGrid), new BABYLON.Vector3(i1, 0, sizeGrid)]);
		}
		for(let i2 =-sizeGrid; i2 <= sizeGrid; i2+=6.4) {
			this.lines.push([new BABYLON.Vector3(-sizeGrid, 0, i2), new BABYLON.Vector3(sizeGrid, 0, i2)]);
		}
		this.grid = BABYLON.MeshBuilder.CreateLineSystem("Grid", {lines: this.lines}, global.scene, false);
		this.grid.color = BABYLON.Color3.Blue();
		this.grid.isPickable = false;		
	}

	resizeGrilleLevelWater(newSize)
	{	
		this.grid.dispose();
		setTimeout(() => {
			this.createGrilleLevelWater(newSize);
		}, 100);
	}
	
	saveParamTerrain(type, value)
	{
		$.ajax({type: "POST", url: "./php/save_param.php", data: "terrainName="+global.nameTerrain+"&type="+type+"&slotSelect="+this.slotSelecte+"&value="+value});
	}
	
	createParamTerrain()
	{
		$.ajax({type: "POST", url: "./php/save_param.php", data: "terrainName="+global.nameTerrain});
	}

	jquery()
	{
		$("#height-slider, #grass-editor, #scale-slider").hide();
		let handle_radius = $("#custom-handle-radius");
		$("#slider-radius").slider({min: 1, max : 500, step: 1,
			create: function() {
				$(this).slider("value", 50);
				handle_radius.text($(this).slider("value"));
				if(global.elevation) {
					global.elevation.radius = 50;
					global.elevation.saveRadius = 50;
				}
			},
			slide: function(event, ui) {
				handle_radius.text(ui.value);
				global.elevation.radius = ui.value;
				global.elevation.saveRadius = ui.value;
			}
		});
		let handle_hardness = $("#custom-handle-hardness");
		$("#slider-hardness").slider({min: 0.22,  max: 1, step: 0.01,
			create: function() {
				$(this).slider("value", 1.0);
				handle_hardness.text($(this).slider("value"));
				if(global.elevation) global.elevation.opacity = 1.0;
			},
			slide: function(event, ui) {
				handle_hardness.text(ui.value);
				global.elevation.opacity = ui.value;
			}
		});
		let handle_strength = $("#custom-handle-strength");
		$("#slider-strength").slider({ min: 1,  max: 100, step: 1,
			create: function() {
				$(this).slider("value", 5);
				handle_strength.text($(this).slider("value"));
				if(global.elevation) global.elevation.speed = 5;
			},
			slide: function(event, ui) {
				handle_strength.text(ui.value);
				global.elevation.speed = ui.value;
			}
		});
		let handle_height_min = $("#custom-handle-height-min");
		$("#slider-height-min").slider({min: 0, max: 20, step: 1,
			create: function() {
				$(this).slider("value", 10);
				handle_height_min.text("-"+$(this).slider("value"));
				if(global.elevation) global.elevation.heightMin = -10;
			},
			slide: function(event, ui) {
				handle_height_min.text("-"+ui.value);
				global.elevation.heightMin = -ui.value;
			}
		});
		let handle_height_max = $("#custom-handle-height-max");
		$("#slider-height-max").slider({ min: 1, max: 100, step: 1,
			create: function() {
				$(this).slider("value", 50);
				handle_height_max.text($(this).slider("value"));
				if(global.elevation) global.elevation.heightMax = 50;
			},
			slide: function(event, ui) {
				handle_height_max.text(ui.value);
				global.elevation.heightMax = ui.value;
			}
		});
		let handle_scale = $("#custom-handle-scale");
		let that = this;
		$("#slider-scale").slider({ min: 1.0,  max: 100.0, step: 1.0,
			create: function() {
				$(this).slider("value", 16.0);
				handle_scale.text($(this).slider("value"));
				$.ajaxSetup({ async: false});
				if(global.elevation && global.terrain) {
					$.getJSON("data/"+global.nameTerrain+".data?" + (new Date()).getTime(), function(data) {
						global.elevation.uvScale1 = data.uvScale._1;
						global.elevation.uvScale2 = data.uvScale._2;
						global.elevation.uvScale3 = data.uvScale._3;
						global.elevation.uvScale4 = data.uvScale._4;
						global.elevation.uvScale5 = data.uvScale._5;
						global.elevation.uvScale6 = data.uvScale._6;
						global.elevation.uvScale7 = data.uvScale._7;
						global.elevation.uvScale8 = data.uvScale._8;
						if(that.slotSelecte == 1) global.terrain.material.getEffect().setVector2('scaleTxt1',{x: global.elevation.uvScale1, y: global.elevation.uvScale1});
						else if(that.slotSelecte == 2) global.terrain.material.getEffect().setVector2('scaleTxt2',{x: global.elevation.uvScale2, y: global.elevation.uvScale2});
						else if(that.slotSelecte == 3) global.terrain.material.getEffect().setVector2('scaleTxt3',{x: global.elevation.uvScale3, y: global.elevation.uvScale3});
						else if(that.slotSelecte == 4) global.terrain.material.getEffect().setVector2('scaleTxt4',{x: global.elevation.uvScale4, y: global.elevation.uvScale4});
						else if(that.slotSelecte == 5) global.terrain.material.getEffect().setVector2('scaleTxt5',{x: global.elevation.uvScale5, y: global.elevation.uvScale5});
						else if(that.slotSelecte == 6) global.terrain.material.getEffect().setVector2('scaleTxt6',{x: global.elevation.uvScale6, y: global.elevation.uvScale6});
						else if(that.slotSelecte == 7) global.terrain.material.getEffect().setVector2('scaleTxt7',{x: global.elevation.uvScale7, y: global.elevation.uvScale7});
						else if(that.slotSelecte == 8) global.terrain.material.getEffect().setVector2('scaleTxt8',{x: global.elevation.uvScale8, y: global.elevation.uvScale8});
					});
				}
				$.ajaxSetup({ async: true});
			},
			slide: function(event, ui) {
				handle_scale.text(ui.value);
				if(that.slotSelecte == 1) global.elevation.uvScale1 = ui.value;
				if(that.slotSelecte == 2) global.elevation.uvScale2 = ui.value;
				if(that.slotSelecte == 3) global.elevation.uvScale3 = ui.value;
				if(that.slotSelecte == 4) global.elevation.uvScale4 = ui.value;
				if(that.slotSelecte == 5) global.elevation.uvScale5 = ui.value;
				if(that.slotSelecte == 6) global.elevation.uvScale6 = ui.value;
				if(that.slotSelecte == 7) global.elevation.uvScale7 = ui.value;
				if(that.slotSelecte == 8) global.elevation.uvScale8 = ui.value;
				if(global.terrain) {
					if(that.slotSelecte == 1) global.terrain.material.getEffect().setVector2('scaleTxt1', {x: ui.value, y: ui.value});
					else if(that.slotSelecte == 2) global.terrain.material.getEffect().setVector2('scaleTxt2',{x: ui.value, y: ui.value});
					else if(that.slotSelecte == 3) global.terrain.material.getEffect().setVector2('scaleTxt3',{x: ui.value, y: ui.value});
					else if(that.slotSelecte == 4) global.terrain.material.getEffect().setVector2('scaleTxt4',{x: ui.value, y: ui.value});
					else if(that.slotSelecte == 5) global.terrain.material.getEffect().setVector2('scaleTxt5',{x: ui.value, y: ui.value});
					else if(that.slotSelecte == 6) global.terrain.material.getEffect().setVector2('scaleTxt6',{x: ui.value, y: ui.value});
					else if(that.slotSelecte == 7) global.terrain.material.getEffect().setVector2('scaleTxt7',{x: ui.value, y: ui.value});
					else if(that.slotSelecte == 8) global.terrain.material.getEffect().setVector2('scaleTxt8',{x: ui.value, y: ui.value});
				}
			},
			change: function(event, ui) {
				if(global.elevation && global.terrain) {
					that.saveParamTerrain("uvScale", ui.value);
				}
			}
		});

		$("#enter-edit-mode-ground").click(() =>{
			if(global.terrain.isPickable == true) {
				this.closeEditeGround();
			} else {
				if(global.terrain) {
					$("#enter-edit-mode-ground").text("Close mode edite ground").css({"background":"#a1e0a7"});
					global.terrain.isPickable = true;
					MODE_CONTROLE = 0;
					MODE_CONTROLE_CAMERA = 1;
					$("#tools-cam").css({"border": "1px solid red"});
				} else {
					jAlert("No terrain to edite", 'Attention');
				}
			}
		});

		$(".slotTexture").click(() =>
		{
			MODE_CONTROLE_ELEVATE = 0;
			MODE_CONTROLE_PLANE = 0;
			MODE_CONTROLE_ELEVATE = 0;
			MODE_CONTROLE_RAMP = 0;
			MODE_CONTROLE_CAMERA = 0;
			$(".slotTexture").css({"border":"0px"});
			$(this).css({"border":"1px solid red"});
			let imageSelect = $(this).attr("src");
			switch(this.id) {
				case "textureGround1":
					imageToPaint = "images/color/black.jpg";
					global.elevation.texture1 = imageSelect;
					this.slotSelecte = 1;
					global.terrain.material.getEffect().setTexture('texture1', new BABYLON.Texture(global.elevation.texture1, global.scene));
					this.saveParamTerrain("textures", global.elevation.texture1);
				break;
				case "textureGround2":
					imageToPaint = "images/color/red.jpg";
					global.elevation.texture2 = imageSelect;
					this.slotSelecte = 2;
					global.terrain.material.getEffect().setTexture('texture2', new BABYLON.Texture(global.elevation.texture2, global.scene));
					this.saveParamTerrain("textures", global.elevation.texture2);
				break;
				case  "textureGround3":
					imageToPaint = "images/color/green.jpg";
					global.elevation.texture3 = imageSelect;
					this.slotSelecte = 3;
					global.terrain.material.getEffect().setTexture('texture3', new BABYLON.Texture(global.elevation.texture3, global.scene));
					this.saveParamTerrain("textures", global.elevation.texture3);
				break;
				case "textureGround4":
					imageToPaint = "images/color/blue.jpg";
					global.elevation.texture4 = imageSelect;
					this.slotSelecte = 4;
					global.terrain.material.getEffect().setTexture('texture4', new BABYLON.Texture(global.elevation.texture4, global.scene));
					this.saveParamTerrain("textures", global.elevation.texture4);
				break;
				case "textureGround5":
					imageToPaint = "images/color/yellow.jpg";
					global.elevation.texture5 = imageSelect;
					this.slotSelecte = 5;
					global.terrain.material.getEffect().setTexture('texture5', new BABYLON.Texture(global.elevation.texture5, global.scene));
					this.saveParamTerrain("textures", global.elevation.texture5);
				break;
				case "textureGround6":
					imageToPaint = "images/color/cyan.jpg";
					global.elevation.texture6 = imageSelect;
					this.slotSelecte = 6;
					global.terrain.material.getEffect().setTexture('texture6', new BABYLON.Texture(global.elevation.texture6, global.scene));
					this.saveParamTerrain("textures", global.elevation.texture6);
				break;
				case "textureGround7":
					imageToPaint = "images/color/pink.jpg";
					global.elevation.texture7 = imageSelect;
					this.slotSelecte = 7;
					global.terrain.material.getEffect().setTexture('texture7', new BABYLON.Texture(global.elevation.texture7, global.scene));
					this.saveParamTerrain("textures", global.elevation.texture7);
				break;
				case "textureGround8":
					imageToPaint = "images/color/white.jpg";
					global.elevation.texture8 = imageSelect;
					this.slotSelecte = 8;
					global.terrain.material.getEffect().setTexture('texture8', new BABYLON.Texture(global.elevation.texture8, global.scene));
					this.saveParamTerrain("textures", global.elevation.texture8);
				break;
			}
			global.elevation.createTextureToPaint(imageToPaint);
		});

		$(".slotBruches").click(function()
		{
			$(".slotBruches").css({"border":"0px"});
			$(this).css({"border":"1px solid red"});
			if(global.elevation) global.elevation.createBrush($(this).attr("src"));
		});

		$(document).tooltip({
			classes: { "ui-tooltip": "tooltip"},
			show: { effect: "blind", duration: 200 }
		});

		$("#btnCreateTerrain").click(() => {
			let nameTerrain = $("#name_terrain").val();
				if(nameTerrain != "") {
				let sizeTerrain = $("#size_terrain :selected").val();
				this.createGround(nameTerrain, {size: sizeTerrain});
			}
		});

		$("#btnCopyTerrain").click(function() {
			let terrainToCopy = $("#load_terrain :selected").val();
			if(terrainToCopy != "") {
				$("#load_terrain").append('<option id="'+terrainToCopy+"_copy"+'" value="'+terrainToCopy+"_copy"+'">'+terrainToCopy+"_copy"+'</option>');
				$.ajax({type: "POST", url: "./php/copyTerrain.php", data: "terrainToCopy="+terrainToCopy});
			}
		});

		$("#btnDeleteTerrain").click(function() {
			let nameTerrain = $("#load_terrain :selected").val();
			if(nameTerrain != 0) {
				$("#load_terrain option[value="+nameTerrain+"]").remove();
				$.ajax({type: "POST", url: "./php/deleteTerrain.php", data: "terrainName="+nameTerrain});
				if(window.opener) {
					window.opener.global.editor.updateTerrain(global.terrain, nameTerrain, nameTerrain, true, false);
				}
				global.terrain.dispose();
			}
		});

		$("#newTxtRenameTerrain").change(function() {
			let oldNameTerrain = $("#oldTxtRenameTerrain").val();
			let newNameTerrain = $("#newTxtRenameTerrain").val();
			global.terrain.name = newNameTerrain;
			$("#load_terrain option[value='"+oldNameTerrain+"']").attr('value', newNameTerrain).html(newNameTerrain);
			$.ajax({type: "POST", url: "./php/renameTerrain.php", data: "oldTerrainName="+oldNameTerrain+"&newTerrainName="+newNameTerrain});
		});
		
		$("#speedCamera").change(() => {
			this.cameraOfEditor.speed = $("#speedCamera").val();
		});
		
		$("#btnScreenshotTerrain").click(function() {			
			console.log("Create a screenshot and download");
			let engine = global.engine;
			let camera = global.scene.activeCamera;
			let precision = { precision: 1};
			BABYLON.Tools.CreateScreenshotUsingRenderTarget(engine, camera, precision);			
		});		

		$("#load_terrain").change(() => {
			let nameTerrain = $("#load_terrain :selected").val();
			this.loadGround(nameTerrain);
		});

		$(".button-tools").click(function() {
			if(global.terrain == null) return;
			if(global.terrain.isPickable == false) return;
			$(".button-tools").css({"border": "none"});
			$(this).css({"border": "1px solid red"});
		});

		$("#dialog-add-textures").dialog({
			modal: true,
			autoOpen: false,
			width: 560,
			closeText: "",
			draggable: true,
			open: function( event, ui ) {
				refrechDiv("loadTextures", "php/loadTextures.php");
				assigneIn = $.data(this, 'opener').id;
			},
			close: function( event, ui ) {
				if(mediaSelected) {
					$("#"+assigneIn).attr("src", mediaSelected);
					$("#"+assigneIn).val(mediaSelected);
					$("#"+assigneIn).change();
					$("#"+assigneIn).click();
				}
			},
			buttons: [{
				text: "Close", click: function() {
					$( this ).dialog("close");
				}
			}]
		});
		
		$("#dialog-import-texture").dialog({
			modal: true,
			autoOpen: false,
			width: 550,
			closeText: "",
			draggable: true,			
			buttons: [{
				text: "Close", click: function() {
					$( this ).dialog("close");
				}
			}]
		});
		
		$("#dialog-import-mesh").dialog({
			modal: true,
			autoOpen: false,
			width: 550,
			closeText: "",
			draggable: true,			
			buttons: [{
				text: "Close", click: function() {
					$( this ).dialog("close");
				}
			}]
		});
		
		$('#my_form_image').on('submit', function (e) {
			e.preventDefault();
			let $form = $(this);
			let formdata = (window.FormData) ? new FormData($form[0]) : null;
			let data = (formdata !== null) ? formdata : $form.serialize();
			$.ajax({
				url: $form.attr('action'),
				type: $form.attr('method'),
				contentType: false,
				processData: false,
				dataType: 'json',
				data: data,
				success: function (response) {					
					refrechDiv("loadTextures", "php/loadTextures.php");					
					$("#dialog-import-texture").dialog("close");
				}
			});
		});
		
		$('#my_form_mesh').on('submit', function (e) {
			e.preventDefault();
			let $form = $(this);
			let formdata = (window.FormData) ? new FormData($form[0]) : null;
			let data = (formdata !== null) ? formdata : $form.serialize();
			$.ajax({
				url: $form.attr('action'),
				type: $form.attr('method'),
				contentType: false,
				processData: false,
				dataType: 'json',
				data: data,
				success: function (response) {					
					if(response == "file_exists") {
						console.log("A file with that name already exists!");
						alert("A file with that name already exists!");
					} else {						
						response = response.replace("../data/", "");
						let nameTerrain = response.replace("_ground.babylon", "");						
						global.editor.importGround(nameTerrain, "data/", response);
						$("#dialog-import-mesh").dialog("close");
					}					
				}
			});
		});
	}		
}

var onKeyDown = function (evt) {
	if(evt.keyCode == 32 && MODE_CONTROLE == 1) {
		if($("textarea, input").is(":focus")) return;
		if(global.scene.activeCamera) global.scene.activeCamera.attachControl(global.canvas, true);
		if(global.elevation) global.elevation.detachControl();
	}
};

var onKeyUp = function (evt) {
	if(evt.keyCode == 32 && MODE_CONTROLE == 1) {
		if(global.scene.activeCamera) global.scene.activeCamera.detachControl(global.canvas);
		if(global.elevation) global.elevation.attachControl();
	}
};

var file_exists = function(file) {
	let isExist = false;
	let http = new XMLHttpRequest();
	http.open('HEAD', file, false);
	http.send();
	if(http.status != 404) {
		isExist = true;
	}
	return isExist;
};

window.addEventListener("keydown", onKeyDown, false);
window.addEventListener("keyup", onKeyUp, false);