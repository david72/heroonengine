<?php
/*##################################################
 *                                GE.php
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 01/01/2017
 ###################################################

 Interface du game editor
 */
$durerChargement = 4000;
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" /> 
	<meta name="robots" content="noindex"> 
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
	<meta http-equiv="Expires" content="0" />	
	<link rel="shortcut icon" href="app.ico">
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<title>HeroonEngine - Game Editor</title>
	<script src="JS/utilities.js?<?php echo time();?>" type="text/javascript"></script>
	<script>	
	var user = "<?php echo @$_SESSION['user'];?>",
		MODE = "<?php echo file_get_contents("mode.dat");?>",
		getLang =  getCookie("HE-Lang") || "French",
		projet_name = getCookie("projet_name") || "",
		equipe = null,
		lang = null,
		initval = 100,
		addon = null;
	document.oncontextmenu = function() { return false };
	</script>
	<?php	
	$json_get = json_decode(file_get_contents("Data Project/editor.json"), true);
	$getlang = $json_get["lang"];	
	$theme = $json_get["theme"];
	$projet_name = @$_COOKIE["projet_name"];
	if($projet_name == false) header('location: ../');
	$MODE = file_get_contents("mode.dat");
	$lang = json_decode(file_get_contents("Lang/".$getlang.".lng.json"), true);
	require_once("meta.php");		
	?>	
</head>
<body onselectstart="return false"  oncontextmenu="return false">	
	<script>	
	var theme = "<?php echo $theme;?>";
	$(function() {
		$({value: 0}).animate({value: initval},{
			duration: <?php echo $durerChargement;?>,
			easing: "swing",
			step: function(){ 
				$(".knob").val(this.value).trigger("change");
			} 
		});
		$(".knob").knob().show();
		setTimeout(function() {
			$("#HELoad").show(); 
			$("#chargementHE").remove();
		}, <?php echo $durerChargement;?>);
	});
	$(function(){		
		$.ajaxSetup({ async: false});	
		$.getJSON('Lang/'+getLang+'.lng.json', function(data) { lang = data; });
		$.ajaxSetup({ async: true});
	});
	</script>	
	<div id="chargementHE" class="SplashScreen">
		<div class="Frame_SplashScreen">
			<div class="splash"></div><br />
			<input class="knob" data-fgColor="#383838" data-width="150" data-displayInput="false" style="display:none;"><br />
			<?php echo $lang["GE"]["loading"];?>
		</div>
	</div>	
	<div id="dialog-add-meshes" title="<?php echo $lang["GE"]["myModele"];?>" style="display:none;">
		<table>
			<tr>
				<td width="200px">
					<div id="CategorieMeshes" style="width:202px;height:533px;border:1px solid black;overflow-y:auto;"></div>
				</td>
				<td width="416px">
					<div id="listeMeshGE" style="width:365px;height:533px;border:1px solid black;overflow-y:auto;"></div>
				</td>
			</tr>
		</table>
	</div>
	<div id="dialog-add-textures" title="<?php echo $lang["GE"]["myModele"];?>" style="display:none;">
		<table>
			<tr>
				<td width="200px">
					<div id="CategorieTextures" style="width:202px;height:533px;border:1px solid black;overflow-y:auto;"></div>
				</td>
				<td width="416px">
					<div id="listeTexturesGE" style="width:365px;height:533px;border:1px solid black;overflow-y:auto;"></div>
				</td>
			</tr>
		</table>
	</div>
	<div id="dialog-add-images" title="<?php echo $lang["GE"]["myImages"];?>" style="display:none;">
		<table>
			<tr>
				<td width="200px">
					<div id="CategorieImage" style="width:202px;height:533px;border:1px solid black;overflow-y:auto;"></div>
				</td>
				<td width="416px">
					<div id="listeImagesGE" style="width:365px;height:533px;border:1px solid black;overflow-y:auto;"></div>
				</td>
			</tr>
		</table>
	</div>	
	<div id="dialog-add-sounds" title="<?php echo $lang["GE"]["mySound"];?>" style="display:none;">
		<table>
			<tr>
				<td width="200px">
					<div id="CategorieSound" style="width:202px;height:533px;border:1px solid black;overflow-y:auto;"></div>
				</td>
				<td width="416px">
					<div id="listeSoundGE" style="width:365px;height:533px;border:1px solid black;overflow-y:auto;"></div>
				</td>
			</tr>
		</table>
	</div>
	<div id="dialog-add-music" title="<?php echo $lang["GE"]["myMucic"];?>" style="display:none;">
		<table>
			<tr>
				<td width="200px">
					<div id="CategorieMusic" style="width:202px;height:533px;border:1px solid black;overflow-y:auto;"></div>
				</td>
				<td width="416px">
					<div id="listeMusicGE" style="width:365px;height:533px;border:1px solid black;overflow-y:auto;"></div>
				</td>
			</tr>
		</table>
	</div>
	<script>
		var assigneIn = null, mediaSelected = null;
		$(function() {
			$("#dialog-add-meshes").dialog({
				modal: true,
				autoOpen: false,
				width: 610,
				closeText: "",
				draggable: true,
				open: function( event, ui ) {					
					assigneIn = $.data(this, 'opener').id; 	
				},
				close: function( event, ui ) {
					if(mediaSelected) { 
						$("#"+assigneIn).val(mediaSelected);
						$("#"+assigneIn).change();
					}
					$( this ).dialog("close");					
				},
				buttons: [{
					text: lang.button.js.ok, click: function() {					
						$( this ).dialog("close");
					}
				}]
			});
			$("#dialog-add-textures").dialog({
				modal: true,
				autoOpen: false,
				width: 610,
				closeText: "",
				draggable: true,
				open: function( event, ui ) {					
					assigneIn = $.data(this, 'opener').id; 
				},
				close: function( event, ui ) {
					if(mediaSelected) {
						if(basename(mediaSelected) == "_None.png") mediaSelected = "None";
						$("#"+assigneIn).attr("src", mediaSelected);
						$("#"+assigneIn).val(mediaSelected);
						$("#"+assigneIn).change();
					}
					$( this ).dialog("close");					
				},
				buttons: [{
					text: lang.button.js.ok, click: function() {
						$( this ).dialog("close");
					}
				}]
			});	
			$("#dialog-add-images").dialog({
				modal: true,
				autoOpen: false,
				width: 610,
				closeText: "",
				draggable: true,
				open: function( event, ui ) {					
					assigneIn = $.data(this, 'opener').id; 
				},
				close: function( event, ui ) {
					if(mediaSelected) {		
						if(basename(mediaSelected) == "_None.png") mediaSelected = "None";
						$("#"+assigneIn).attr("src", mediaSelected);
						$("#"+assigneIn).val(mediaSelected);
						$("#"+assigneIn).change();
					}
					$( this ).dialog("close");					
				},
				buttons: [{
					text: lang.button.js.ok, click: function() {						
						$( this ).dialog("close");
					}
				}]
			});			
			$("#dialog-add-sounds").dialog({
				modal: true,
				autoOpen: false,
				width: 610,
				closeText: "",
				draggable: true,
				open: function( event, ui ) {
					assigneIn = $.data(this, 'opener').id; 					
				},
				close: function( event, ui ) {
					if(mediaSelected) { 
						$("#"+assigneIn).val(mediaSelected);
						$("#"+assigneIn).change();
					}
					$( this ).dialog("close");					
				},
				buttons: [{
					text: lang.button.js.ok, click: function() {					
						$( this ).dialog("close");
					}
				}]
			});
			$("#dialog-add-music").dialog({
				modal: true,
				autoOpen: false,
				width: 610,
				closeText: "",
				draggable: true,
				open: function( event, ui ) {
					assigneIn = $.data(this, 'opener').id; 					
				},
				close: function( event, ui ) {
					if(mediaSelected) { 
						$("#"+assigneIn).val(mediaSelected);
						$("#"+assigneIn).change();
					}
					$( this ).dialog("close");					
				},
				buttons: [{
					text: lang.button.js.ok, click: function() {						
						$( this ).dialog("close");
					}
				}]
			});
		});
	</script>
	<?php
	require_once("PHP/listeParticles.php");
	require_once("PHP/listeScripts.php");
	?>
	<div id='HE-GE-onglet'>
		<ul>
			<li><a href="#HE-GE-content-projet"><?php echo $lang["GE"]["tabProject"];?></a></li>
			<li><a href="#HE-GE-content-media"><?php echo $lang["GE"]["tabMedia"];?></a></li>
			<li><a href="#HE-GE-content-actors-all"><?php echo $lang["GE"]["tabActors"];?></a></li>
			<li><a href="#HE-GE-content-items"><?php echo $lang["GE"]["tabItems"];?></a></li>
			<li><a href="#HE-GE-content-zones"><?php echo $lang["GE"]["tabZones"];?></a></li>
			<li><a href="#HE-GE-content-particules-all"><?php echo $lang["GE"]["tabParticle"];?></a></li>
			<li><a href="#HE-GE-content-scripts"><?php echo $lang["GE"]["tabScripts"];?></a></li>
		</ul>
		<div id='HE-GE-content-projet'><?php require_once("Form/projet.php");?></div>
		<div id='HE-GE-content-media'><?php require_once("Form/media.php");?></div>		
		<div id='HE-GE-content-actors-all'>
			<div id='HE-GE-content-actors-tabs'>
				<ul>
					<li><a href="#HE-GE-content-actors"><?php echo $lang["GE"]["tabActors"];?></a></li>
					<li><a href="#HE-GE-content-articles"><?php echo $lang["GE"]["tabArticles"];?></a></li>
					<li><a href="#HE-GE-content-animations"><?php echo $lang["GE"]["tabAnimation"];?></a></li>
					<li><a href="#HE-GE-content-competences"><?php echo $lang["GE"]["tabCompetance"];?></a></li>
					<li><a href="#HE-GE-content-factions"><?php echo $lang["GE"]["tabFactions"];?></a></li>
					<li><a href="#HE-GE-content-autres"><?php echo $lang["GE"]["tabAutres"];?></a></li>
				</ul>
				<div id='HE-GE-content-actors'><?php require_once("Form/actors.php");?></div>
				<div id='HE-GE-content-articles'><?php require_once("Form/articles.php");?></div>
				<div id='HE-GE-content-animations'><?php require_once("Form/animations.php");?></div>
				<div id='HE-GE-content-competences'><?php require_once("Form/competences.php");?></div>
				<div id='HE-GE-content-factions'><?php require_once("Form/factions.php");?></div>
				<div id='HE-GE-content-autres'><?php require_once("Form/autres.php");?></div>
			</div>
		</div>
		<div id='HE-GE-content-items'><?php require_once("Form/items.php");?></div>
		<div id='HE-GE-content-zones' style="padding:0px;"><?php require_once("Form/zones.php");?></div>
		<div id='HE-GE-content-particules-all'>
			<div id='HE-GE-content-particules-tabs'>
				<ul>
					<li><a href="#HE-GE-content-particules2d"><?php echo $lang["GE"]["tabParticle2d"];?></a></li>
					<li><a href="#HE-GE-content-particules3d"><?php echo $lang["GE"]["tabParticle3d"];?></a></li>					
				</ul>			
				<div id='HE-GE-content-particules2d'><?php require_once("Form/particules2d.php");?></div>
				<div id='HE-GE-content-particules3d'><?php require_once("Form/particules3d.php");?></div>
			</div>
		</div>
		<div id='HE-GE-content-scripts'><?php require_once("Form/scripts.php");?></div>
	</div>
	<script>
	$(function() {
		$(".height-content").css({"height": ($("#HE-GE-onglet").height() - 80) +"px"});
		$("td").css({"vertical-align":"top"});		
	});	
	addon = new addonManager();
	addon.loadModule();
	addon.addModuleInterface();	
	</script>
	<script src="JS/GE.js?<?php echo time();?>" type="text/javascript"></script>
</body>
</html>