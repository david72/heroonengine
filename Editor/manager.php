<?php
session_start();
/*##################################################
 *                                manager.php
 *                            -------------------
 *   copyright            : (C) 2017 Pellier David (dad72)
 *   email                : dad72@heroonengine.com
 *   Revision             : 01/10/2016
 ###################################################

 Interface du manager de projet
 */
if(!$_SESSION['user']) {
	header("location: index.php");
}
$mode = @file_get_contents("mode.dat");
if($mode != "dev") {
	ini_set("display_errors", 0);
	error_reporting(0);
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Manager Editor</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<link rel="shortcut icon" href="app.ico">
	<link rel="stylesheet" type="text/css" href="Styles/editor.css">
	<link rel="stylesheet" type="text/css" href="Styles/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="Styles/jquery.alerts.css">
	<!-- libs JQuery -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js" type="text/javascript"></script>
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
	<script src="Heroon Engine/Library/jquery.alerts.js" type="text/javascript"></script>
</head>
<body>
	<?php
	include_once("Heroon Engine/PHP/foldersize.php");
	$version = file_get_contents("Heroon Engine/Version.dat");
	?>
	<script>
	var editorVersion = "Free";
	var numVersion = "<?php echo $version;?>";
	</script>
	<div id='HE-manager' style="box-shadow:8px 8px 12px #1f1f1f;border-radius:6px;">
		<div id='HE-manager-menu' style="height:38px;">
			<span style="margin-left:30px;display:inline-block;font-size:23px;">Project Manager <?php echo $version;?></span>
			<button id="HENewUpdate" style="float:right;display:none;font-size:20px;background-color:#eb2323;color:white;">Nouvelle version disponnible</button>
		</div>
		<?php
		$urlSplash = "http://www.heroonengine.actifgames.com/HEROONENGINE/splash.png";
		if(!file_exists($urlSplash)) $urlSplash = "splash-default.png";
		?>
		<div id='HE-manager-splash' style="background-image: url('<?php echo $urlSplash;?>');"></div>
		<div id='HE-manager-onglet'>
			<ul>
				<li><a href="#HE-manager-content-projet">Projects</a></li>
				<li><a href="#HE-manager-content-outils">Tools</a></li>
			</ul>
			<div id='HE-manager-content-projet'>
				<table style="width:100%;" border="1" id="listeproject">
					<thead>
						<tr>
							<th style="width:45%;height:10px;">Name</th>
							<th style="width:15%;">Version</th>
							<th style="width:20%;">Date</th>
							<th style="width:20%;">Poids</th>
						</tr>
					</thead>
					<?php
					$dir = "Heroon Engine/_Projects/";
					$i = 1;
					if (is_dir($dir)) {
						if ($dh = opendir($dir)) {
							while ($file = readdir($dh)) {
								if (is_dir($dir.$file) && $file != "." && $file != "..") {
									$versionOfProject = file_get_contents($dir.$file."/Version.dat");
									$total_size = foldersize($dir.$file);
									$poids = format_size($total_size, 2);
									$date = file_get_contents($dir.$file."/date.dat");
									?>
									<tbody id="projet-<?php echo $i;?>" class="projet">
										<tr>
											<td style="text-align:left;height:10px;" name="<?php echo $file;?>" id="<?php echo $file;?>"><img id="deleteproject_<?php echo $file;?>" src="Heroon Engine/Ressources/throbber.gif" style="display:none;margin-left:10px;margin-right:5px;" /> <?php echo $file;?></td>
											<td style="text-align:center;"><?php echo $versionOfProject;?></td>
											<td style="text-align:center;"><?php echo $date;?></td>
											<td style="text-align:center;"><?php echo $poids;?></td>
										</tr>
									</tbody>
									<?php
									$i++;
								}
							}
							closedir($dh);
						}
					}
					?>
					<tr id="throbber" style="display:none;">
						<td colspan="4"><img src="Heroon Engine/Ressources/throbber.gif" style="margin-left:10px;" /></td>
					</tr>
				</table>
			</div>
			<div id='HE-manager-content-outils'>
				<a href="https://github.com/BabylonJS/Exporters/tree/master/3ds%20Max" target="_blank">Download exporter for 3ds max (2015, 2016, 2017)</a><br />
				<a href="https://github.com/BabylonJS/Exporters/tree/master/Blender" target="_blank">Download exporter for blender</a><br />
				<a href="https://nodejs.org/en/download/" target="_blank">Download NodeJS for windows or mac</a><br />
				<a href="commande_install_nodeJS.html" target="_blank">Install NodeJS for linux</a><br />
			</div>
		</div>
		<div id='HE-manager-control'>
			<button id="HE-manager-control-open">Open</button>
			<button id="HE-manager-control-new">New</button>
			<button id="HE-manager-control-copy">Copy</button>
			<button id="HE-manager-control-rename">Rename</button>
			<button id="HE-manager-control-delete">Delete</button>
			&nbsp;
		</div>
		<div id='HE-manager-version'>&nbsp; Version : <?php echo $version;?></div>
	</div>
	<script src="manager.js?<?php echo time();?>" type="text/javascript" id="managerScript"></script>
</body>
</html>