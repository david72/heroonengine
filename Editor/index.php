<?php
session_start();
if(file_exists("install.dat")) {
	$install = @file_get_contents("install.dat");
	if($install == "0") {
		header("location: Install/index.php");
	}
} else {
	header("location: Install/index.php");
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Connexion</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<link rel="shortcut icon" href="app.ico">
	<link rel="stylesheet" type="text/css" href="Styles/editor.css">
	<link rel="stylesheet" type="text/css" href="Heroon Engine/Styles/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="Heroon Engine/Styles/jquery.alerts.css">
	<!-- libs JQuery -->
	<script src="Heroon Engine/Library/jquery.min.js" type="text/javascript"></script>
	<script src="Heroon Engine/Library/jquery-ui.min.js" type="text/javascript"></script>
	<script src="Heroon Engine/Library/jquery.alerts.js" type="text/javascript"></script>
</head>
<body>
	<?php
	// SI on utilise l'editeur en local
	$isLocal = ($_SERVER["SERVER_NAME"]=="127.0.0.1");
	if($isLocal) {
		$_SESSION['user'] = "Admin";
		setcookie("user", "Admin", time()+31536000);		
		header('Location: manager.php');
	} else { // Sinon on utilise un serveur d'hebergement ou dedier
		if(@$_POST['user'] && @$_POST['password']) {
			$json = json_decode(file_get_contents("Heroon Engine/Data Project/editor.json"), true);		
			for($i = 0; $i < count($json["collaborateurs"]["users"]); $i++) {
				$pass = base64_encode($_POST['password']);
				if($json["collaborateurs"]["users"][$i]["pseudo"] == $_POST['user'] && $json["collaborateurs"]["users"][$i]["password"] == md5($pass)) {
					$_SESSION['user'] = $_POST['user'];
					$json["collaborateurs"]["users"][$i]["connecter"] = 1;
					$save = json_encode($json);
					file_put_contents("Heroon Engine/Data Project/editor.json", $save);
					if($_POST['loginauto'])	{
						setcookie("user", $_SESSION['user'], time()+31536000);
						setcookie("loginauto", $_POST['loginauto'], time()+31536000);
					}
					header('Location: manager.php');
					break;
				} else {
					echo "<script>jAlert('Votre nom d\'utilisateur ou votre mot de passe ne corespond pas!', 'Attention');</script>";
				}			
			}
		}
		if(@$_COOKIE["loginauto"] == 1) {
			$json = json_decode(file_get_contents("Heroon Engine/Data Project/editor.json"), true);
			for($i = 0; $i < count($json["collaborateurs"]["users"]); $i++) {
				if($json["collaborateurs"]["users"][$i]["pseudo"] == $_COOKIE["user"]) {
					$_SESSION['user'] = $_COOKIE["user"];
					$json["collaborateurs"]["users"][$i]["connecter"] = 1;
					$save = json_encode($json);
					file_put_contents("Heroon Engine/Data Project/editor.json", $save);				
					header('Location: manager.php');
					break;
				} else {
					echo "<script>jAlert('Votre nom d'utilisateur n\'existe pas!', 'Attention');</script>";
				}
			}
		} else {			
			?>
			<script>
			function passOublier() {
				jPrompt('E-mail : ', '', 'Entrez votre Email pour recevoir votre mot de passe', function(adresse) {
					if(adresse) {
						$.ajax({ type: 'POST', url: 'mail.php', data: 'adress='+adresse+'&getMail=1',
							success: function(msg) {
								if(msg != "") {	
									var user = msg;
									jPrompt('Nouveau mot de passe : ', '', 'Changer de mot de passe', function(newpass) {
										$.ajax({ type: 'POST', url: 'mail.php', data: 'user='+user+'&changepass=1&pass='+newpass,
											success: function(msg) {
												jAlert('Votre mot de passe à bien été modifier!', 'Information');
											}
										});
									});								
								} else {
									jAlert('Cette adresse mail n\'existe pas!', 'Information');
								}
							}
						});	
					}
				});
			}
			</script>
			<div id="login" style="box-shadow:8px 8px 12px #424242;">
			<br />
				<center class="titre-login">Heroon Engine</center>
				<br />
				<form method="POST">
					<label class="base">&nbsp; User : </label><input type="text" name="user" value=""><br />
					<label class="base">&nbsp; Password : </label><input type="password" name="password" value=""><br />
					<label class="base">&nbsp; Remember me : </label><input type="checkbox" name="loginauto" value="1" checked> <small>(Automatic login)</small><br />
					<center><a href="javascript:void(0);" onClick="passOublier();"><small>Reset my password !</small></a></center>
					<br /><input type="submit" value="Login" style="float:right;margin-right:22px;">				
				</form>
			<div>
			<?php
		}
	}
	?>
</body>
</html>