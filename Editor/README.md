# **HeroonEngine Editor*- #

## Que pouvez faire, créer avec l’éditeur ? ##

- Des acteurs joueur, non joueur, des animaux, des montures... Chaque acteurs peut recevoir des articles (élément à attacher sur les os : une épée par exemple). Vous pouvez modifier les aptitudes des acteurs, leurs compétences, factions au quelle ils appartiennent au d'autres.
- Des items qui sont des éléments interactifs dissimuler sur le monde. Cela peut être des objets, images à ramasser au sol.
- Des zones qui correspond à chaque monde de vos jeux raccorder par des portails. Un éditeur de terrain permet de créer des mondes pour chaque zone. Chaque zone peut avoir des objets 3d de votre choix que vous pouvez importer, vous pouvez créer de l'eau, un ciel et plein d'autre chose pour composer des mondes à votre idée.
- Des systèmes de particules pour créer des effets spéciaux animer. Par exemple, de la fumer, des effets de magies, de la pluie...
- Des scripts PHP et Javascript pour personnaliser votre jeu ou modifier les caractéristiques des items, des mondes, des capacités des acteurs et autres. Vous pouvez créer des scripts attacher à des boites cible invisible sur vos mondes qui sont déclencher quand un acteur entre en collision avec l'un deux.

## Caracteristique supporter de l'editer et du jeu ##

### General ###

- Création MMORPG
- Conception orientée objet
- Facilité d'utilisation
- Addon Architecture
- Save/Load System
- Meilleur éclairage - Un éclairage dynamique illimité signifie que votre jeu peut avoir un éclairage qui sera diffusé sur les paysages et les joueurs. Cela signifie également des torches réalistes travaillant à la nuit.
- Bloom & Tone Mapping - Les zones lumineuses, brillantes et les zones sombres apparaissent sombres!
- Lens Flares
- Particle System
- Sky box
- Eau realiste avec refraction et reflexion
- Feux
- Explosion
- Brouillard
- Ombres dynamique et statique
- Météo (pluit, neige, orage, jour, nuit…)
- SDK pour GUI avancée
- Mise à jour constante
- Aucune compétence linguistique de programmation requise
- Langage de script pour que vous puissiez personnaliser votre jeu.
- Editeurs faciles à utiliser pour créer et éditer votre monde de jeu.
- Les joueurs peuvent avoir des animaux domestiques (montures).
- Définissez vos propres attributs, compétences et types de dégâts.


### Lighting ###

- Per-vertex
- Per-pixel
- Lightmapping
- Anisotropic
- Suppore les lumiere directionelle, spot, points et hemispherique.


### Shadows ###

- Cartographie de l'ombre
- Dynamic Shadows - Les acteurs mettront leurs ombres sur le sol et decor.


### Shaders ###

- Vertex
- Pixel
- Customizable Shaders
- Water
- Fure
- Fire
- Lava


### Meshes ###

- Mesh Loading - extention: .babylon, .glb, .gltf, .obj, .stl
- Exporter .babylon pour 3ds max et blender
- Skinning, physique


### Textures ###

- Support extention: tga, dds, png, gif, jpeg, jpg, bmp


### Animation ###

- Keyframe Animation
- Skeletal Animation


### Terrain ###

- Rendering
- Heightmap


### Physics ###

- Basic Physics (CannonsJS)
- Collision Detection


### Networking ###

- Client-Server NodeJS avec Cluster et zone d'interet


### Artificial Intelligence ###

- Pathfinding 2d et 3d
- Prise de decision
- Scripted


### Sound ###

- 3D Sound
- Streaming
- .wav, .ogg, .mp3 format


### Video ###

- .webm, .ogv, .mp4 format


### Scripting ###

- Éditeur de script Javascript (Ajax et Jquery possible) et PHP ET MYSQL


## Vous souhaitez contribuer, que faut-il faire ? ##

Avant toutes chose, n'ayez pas peur, vous n'avez pas besoin d’être un gourou de la programmation ou expert pour participer. Tout le monde peut participer à sa façon et avec ses moyens, connaissance. Ce projet a besoin de participant comme vous, expert ou non. Alors lancer vous.

Dans un premier temps, vous devez télécharger et installer un logiciel fournit sur ce site "bitbucket" qui permet de communiquer entre vos fichiers local (sur votre ordinateur) et ce dépôt distant. Ce logiciel c'est "[SourceTree](https://www.sourcetreeapp.com/?utm_source=internal&utm_medium=link&utm_campaign=clone_repo_win)".

Ensuite, vous devez créer une branche (Forks) pour créer votre dépôt à partir du dépôt principal (master) pour ensuite le cloner sur votre ordinateur.

Puis il vous suffira de modifier les fichiers sur votre ordinateur pour faire ce que vous souhaitez comme correctif, fonctionnalités... Vous devrez ensuite créer un "Pull requests" pour soumettre vos modifications. Votre "Pull requests" (PR) sera étudier, puis fusionner sur master.

## Quelque recommandation pour les Pull Request. ##

- vous ne devez jamais supprimer de fonction dans le code. Si vous expédiez une fonction supprimer, elle ne sera pas validée. Toutefois, vous pouvez modifier une fonction si vous pensez qu'elle sera mieux ou plus complète.
- Vous devrez vérifier que ce que vous avez fait fonctionne normalement avant de soumettre votre PR.
- N’hésitez pas à poser vos questions pour proposer votre idée de modification, vous pourriez recueillir des avis divers qui pourrait faire évoluer votre idée première.
- Essayer de garder au maximum la simplicité pour les utilisateurs lors de vos ajouts de fonctionnalités. HeroonEngine ce veux simple pour s'adresser aux amateurs débutant en premier.

## Nos directives de codage ##

Nous ne sommes pas trop exigent, mais nous avons quelque recommandation pour l’écriture de code de façons à garder toujours le même style d’écriture du code pour tout le monde. Voici nos quelques directives pour que nous validions vos contributions :

- Les variables et fonctions privées commence par un underscore ( _ ) : *_myVariable*, *_myFunction()*
- Les noms de variables, fonctions commence par une minuscule. Dans le cas de mot composer, Une majuscule à chaque mot : *myVariableComposed*, *myFunctionComposed()*
- Les nom de class commence par une majuscule.
- Toute les fonctions statiques doivent être précéder du mot clef *static*
- Les variables fixe, dont les valeurs ne changent jamais, doivent être précéder du mot clef *const*
- Nous utilisons Javascirpt ES6, vous devez donc utiliser le mot clef "let" plutôt que "var" pour déclarer vos variables. "var" est si vous voulez une variable globale en mémoire.
- Les accolades de fonctions, conditions "*{ }*" doivent être utilisées pour chaque bloc, même s'il n'y a qu'une seule ligne. L’accolade d'ouverture est sur la même ligne après la fermeture de parenthèse.
- Des commentaires peuvent être apprécier pour relire plus facilement votre code (En français de préférence)
- Le code doit être indenter et aérer (sans trop d'exagération) pour une relecture plus facile.
- Dans le cas de nouvelle classe, créer un nouveau fichier et l'ajouter dans le fichier .php correspondant. Un fichier = une classe.

## Des idées de contributions ##

- Créer, améliorer les documentations, tutoriels...
- Corriger des fautes d’orthographe.
- Créer, corriger ou compléter la démo officielle.
- Des corrections de bug mineur ou majeur.
- Ajouter de nouvelles fonctionnalités.

## Comment est organiser HeroonEngine ? ##

Il est important pour prendre en main les sources-code de savoir comment est organiser tout ça. Voici comment :

A la racine, c’est **le manager de projet**. C’est le point d'arriver quand on ouvre HeroonEngine.

Dedans ce trouve quatre dossiers :

- le premier dossier *Heroon Engine/- est l’éditeur, quand on ouvre un projet sur le manager.
- Le dossier suivant *Install/- contient tous les fichiers de l'installation pour les premiers utilisateurs qui télécharge HeroonEngine
- Le suivant est un dossier de *Log/- pour récupérer les erreurs éventuel (ce dossier est vides, le système de capture d'erreur n’est pas encore réaliser)
- Et enfin le dossier *Styles/- est tout ce qui est Css pour mettre en forme l'interface du manager.

Pour modifier le manager, il y a deux fichiers qui peuvent vous intéresser :  *manager.j*s et *manager.php*. Le *.php- est la partie graphique et le *.js- est la partie qui exécute les actions venant de l'interface.

Le dossier le plus important de l’éditeur est **Heroon Engine/*- qui a une organisation un peu plus complexe, mais qui reste simple. Un développeur devrait pouvoir apprendre rapidement et facilement à se repérer.

Je vais expliquer les dossiers que l'on y trouve. Les trois dossiers les plus important qui sont les noyaux de l’éditeur sont *Form/, JS/- et *PHP/*.

- *Form/- est le diminutif de Formulaire, il contient toutes la partie HTML : l'interface de l’éditeur.
- *JS/- contient tous les fichiers Javascript de chaque onglet.
- *PHP/- contient toutes la partie qui permet de lire et écrire dans des fichiers JSon, pour enregistrer les données de l’éditeur pour le jeu.

Généralement vous modifierez ce qui se trouve dans ses trois dossiers ou juste dans un seule.

Voyons voir aussi à quoi servent les autres dossiers pour bien comprendre toutes les sources :

- Le dossier *_Projects/- contient les projets que l'utilisateur créer via le manager.
- Le dossier *Data Project/- contient le projet par défaut qui sert de référence quand un projet est créé. Ce dossier contient aussi des scripts de base par défaut et le SDK pour l'utilisateur.
- Le dossier *Library/- contient toutes les librairies annexe de l’éditeur, comme JQuery par exemple.
- Le dossier *Log/- contient les erreurs éventuelles de l’éditeur (ce dossier est vide pour le moment, le système de capture d'erreur n’est pas encore réalisé)
- Le dossier *Ressources/- contient toutes les images de l’éditeur.
- Le dossier *Scripts/- contient tous les scripts créer par l'utilisateur par projet.
- Le dossier *Lang/- contient toutes les langues de l’éditeur dans un format Json
- Le dossier *Styles/- contient tout ce qui est Css pour mettre en forme l'interface de l’éditeur
- Le dossier *Temps/- est vides, il sert de dossier temporaire.
- Le dossier *Addons/- contient toutes les petits programmes qui peuvent être ajouter sous forme de modules additionnelle.

A la racine de ce dossier, ce trouve l'index.php qui est le point d'entrer de l’éditeur qui inclues des fichiers. Notamment le fichier *GE.php- qui contient toutes les onglets. C’est la page principale de l’éditeur qui inclue à son tour le contenu de chaque onglet disponible dans le dossier *Form/*

Vous avez vue l'organisation des dossiers, mais vous devez aussi comprendre l'organisation des fichiers du dossier *JS/*

Dans ce dossier, il y a deux sous-dossiers, un qui concerne tout ce qui est de l'onglet zones/ et l'autre tout ce qui concerne l'onglets et sous-onglets actors/.
Chaque fichier correspond à un onglet qui contient généralement à une classe.

Si vous allez dans le dossier zones/ vous verrais plusieurs fichiers qui ont été organiser par fonctionnalités cette fois :

- *addObject.js- est tout ce qui permet d'ajouter des objets sur la zone/terrain
- *assets.js- est le gestionnaire d'importation de médias
- *chat.js- est simplement le chat de l’éditeur pour le système collaboratif
- *editor.js- regroupe tous les contrôle de l’éditeur, la création de la scène, des caméras... C’est la classe principale ou tout lui est relier.
- *environement.js- est tout ce qui permet d'enregistrer les modifications de l’environnement pour le jeu
- *grassControl.js- est la classe qui permet de créer de l’herbes sur le terrain
- *manipulator.js- est la classe qui permet tous les contrôle sur les objets, les différents gizmo autrement dit.
- *property.js- est l’éditeur de propriétés des objets sélectionner. Cette classe créer, modifie et enregistre chaque propriété des objets.

Voilà, vous savez tout. Je ne vais pas vous éplucher le code de chaque fichier, mais je vous invite à les ouvrir pour jeter un œil. Tous développeurs devraient comprendre rapidement de quoi il s'agit. J'écris le code de façons qu'il soit compréhensible par le plus grand monde.

Si vous avez besoin d'une meilleur compréhension d'une fonction, n’hésitez pas pour poser vos questions dans "Signaler un bug" disponible dans le menu de gauche.

Nous remercions d'avance tous les utilisateurs qui contribuerons à ce projet. Vous êtes tous les bienvenues.

![HeroonEngine.jpg](https://bitbucket.org/repo/Krngy6x/images/2983051436-HeroonEngine.jpg)
