<?php
session_start();
error_reporting(0);
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Install</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<link rel="shortcut icon" href="app.ico">
	<link rel="stylesheet" type="text/css" href="Styles/install.css?<?php echo time();?>">
	<link rel="stylesheet" type="text/css" href="../Heroon Engine/Styles/jquery.alerts.css">
	<link rel="stylesheet" type="text/css" href="../Heroon Engine/Styles/select2.css">
	<script src="../Heroon Engine/Library/jquery.min.js" type="text/javascript"></script>
	<script src="../Heroon Engine/Library/jquery.alerts.js" type="text/javascript"></script>
	<script src="../Heroon Engine/Library/select2.full.min.js" type="text/javascript"></script>
	<script src="../Heroon Engine/JS/utilities.js" type="text/javascript"></script>	
	<script src="https://preview.babylonjs.com/babylon.js?<?php echo time();?>" type="text/javascript"></script>
</head>
<body>
	<script>
	var getLang =  getCookie("HE-Lang") || "French";
	var lang = null;
	$(function(){		
		$.ajaxSetup({ async: false});	
		$.getJSON('Lang/'+getLang+'.lng.json', function(data) { lang = data; });
		$.ajaxSetup({ async: true});
	});
	</script>
	<?php
	if(@$_SESSION['HE-Lang'] == false) $_SESSION['HE-Lang'] = "French";	
	$lang = json_decode(file_get_contents("Lang/".$_SESSION['HE-Lang'].".lng.json"), true);	
	function chaine_aleatoire($nb_car, $chaine, $compteExist, $enterMail, $recieveMail)
	{	
		$json = json_decode(file_get_contents("../Heroon Engine/Data Project/editor.json"), true);			
		if(count($json["collaborateurs"]["users"]) == 0) {			
			$nb_lettres = strlen($chaine) - 1;
			$generation = '';
			for($i=0; $i < $nb_car; $i++) {
				$pos = mt_rand(0, $nb_lettres);
				$car = $chaine[$pos];
				$generation .= $car;
			}				
			echo "<script>
			jPrompt('E-mail : ', '', '".$enterMail."', function(adresse) {
				if(adresse) {
					$.ajax({ type: 'POST', url: '../mail.php', data: 'adress='+adresse+'&pass=".$generation."',
						success: function(msg) {
							if(msg) {
								jAlert(msg, 'Information');
							} else{
								jAlert('".$recieveMail."', 'Information');
							}
						}
					});	
				}
			});			
			</script>";			
		} else {
			echo "<script>jAlert('".$compteExist."', 'Information');</script>";	
		}
	}
	function getFilePermission($file)
	{
		$length = strlen(decoct(fileperms($file)))-4;
		return substr(decoct(fileperms($file)), $length);
	}
	?>
	<script>
	$(function() {			
		$("#licence, #configuration, #finalisation, #admin").hide();
		$("#li-preambule").css({"background":"green"});
	});	
	var current_etape = 1;	
	</script>	
	<div id="menu-left">
		<div id="content-menu">
			<h3 class="titre" title="HerronEngine">Heroon engine</h3>
			<?php echo $lang['slogan'];?><br />	
			<br />
			<h3><?php echo $lang['changeLangue'];?></h3>	
				<select id="HE-LanguageDefaut" name="HE-LanguageDefaut" style="width:200px;">
				<?php
				$dirname = 'Lang/';
				$dir = opendir($dirname);
				while($file = readdir($dir)) {
					if($file != '.' && $file != '..' && $file != 'images') {
						$file = str_replace('.lng.json', '', $file);
						$extension=pathinfo($dirname.$file, PATHINFO_EXTENSION);
						if($extension != "json") {
							echo '<option value="'.$file.'" data-image="'.$dirname.'images/'.$file.'.png">'.$file.'</option>';
						}
					}
				} closedir($dir);
				?>
				</select>
			<h3><?php echo $lang['listeEtape'];?></h3>
				<ul>
					<li id="li-preambule"><?php echo $lang['Preambule'];?></li>
					<li id="li-licence"><?php echo $lang['Licence'];?></li>
					<li id="li-configuration"><?php echo $lang['Configuration'];?></li>
					<li id="li-admin"><?php echo $lang['Administrator'];?></li>
					<li id="li-finalisation"><?php echo $lang['FinInstal'];?></li>
				</ul>
			<h3><?php echo $lang['Progress'];?></h3>
			<progress style="width:95%;" max="100" value="0" id="progress-install"></progress>			
			</div>
		</div>
	<div id="content">	
		<div id="preambule">
			<h1><?php echo $lang['Preambule'];?></h1>
			<h2><?php echo $lang['welcome'];?></h2>
			<?php echo join('', $lang['PreambuleText']);?>	
			<br /><center><input type="button" value=">>" id="next1" class="button-control"></center>
		</div>
		<div id="licence">
			<h1><?php echo $lang['Licence'];?></h1>
			<h2><?php echo $lang['accepteLicence'];?></h2>
			<p><?php echo $lang['readLicence'];?></p>
			<div id="licence-block"><?php echo nl2br(file_get_contents("../LICENCE_".$_SESSION['HE-Lang'].".txt"));?></div>
			<br />
			<div class="widget"><span><?php echo $lang['checkLicence'];?></span><label for="accepteLicence">&nbsp;</label><input type="checkbox" name="accepteLicence" id="accepteLicence"></div>
			<br /><center><input type="button" value="<<" id="previous1" class="button-control"> &nbsp; <input type="button" value=">>" id="next2" class="button-control"></center>
		</div>
		<div id="configuration">
			<h1><?php echo $lang['Configuration'];?></h1>
			<h2><?php echo $lang['VerifConfig'];?></h2>		
			<?php echo join('', $lang['TextConfig']);?>	
			&nbsp; - <label class="config">WebGL compatible</label><div id="webGLIsSupported" class="yes">Oui</div><br />
			<!--&nbsp; - <label class="config">Navigateur compatible</label><div id="browsersIsCompatible" class="yes">Oui</div> <a id="browsers" style="display:none;" href='https://www.google.fr/chrome/browser/desktop/' target='_blank'>Download Google Chrome</a><br />-->
			&nbsp; - <label class="config">WebGL version</label><div id="webGLVersion" class="yes">0</div><br />
			&nbsp; - <label class="config">Engine version</label><div id="engineVersion" class="yes">0</div><br />
			<canvas id="canvas" style="display:none;"></canvas>
			<script>
			/*
			var isChrome = !!window.chrome && !!window.chrome.webstore;
			if(isChrome == false) {
				$("#browsersIsCompatible").attr("class", "no").html("Non");
				$("#browsers").show();
			}
			*/
			if(BABYLON.Engine.isSupported() == false) {
				$("#webGLIsSupported").attr("class", "no").html("Non");
				$("#webGLVersion").attr("class", "no");
				$("#engineVersion").attr("class", "no");				
			} else {
				var engine = new BABYLON.Engine(document.getElementById("canvas"));
				$("#webGLVersion").html(engine.webGLVersion);
				$("#engineVersion").html(BABYLON.Engine.Version);
				engine.dispose();
			}				
			</script>
			<br />
			<b><?php echo $lang['autorizeFolder'];?></b><br />
			<?php echo join('', $lang['TextAutorizeFolder']);
			chmod("../Logs", 0777);
			chmod("../Heroon Engine/_Projects", 0777);
			chmod("../Heroon Engine/Logs", 0777);
			chmod("../Heroon Engine/Scripts", 0777);
			chmod("../Heroon Engine/Temp", 0777);
			chmod("../Heroon Engine/Data Project/chat.txt", 0777);			
			chmod("../Heroon Engine/Data Project/selected.dat", 0777);
			if(getFilePermission("../Logs") == "0777") $folder1 = "<div class=\"yes\">Oui</div>";
			else $folder1 = "<div class=\"no\">Non</div>";			
			if(getFilePermission("../Heroon Engine/_Projects") == "0777") $folder2 = "<div class=\"yes\">Oui</div>";
			else $folder2 = "<div class=\"no\">Non</div>";			
			if(getFilePermission("../Heroon Engine/Logs") == "0777") $folder3 = "<div class=\"yes\">Oui</div>";
			else $folder3 = "<div class=\"no\">Non</div>";			
			if(getFilePermission("../Heroon Engine/Scripts") == "0777") $folder4 = "<div class=\"yes\">Oui</div>";
			else $folder4 = "<div class=\"no\">Non</div>";			
			if(getFilePermission("../Heroon Engine/Temp") == "0777") $folder5 = "<div class=\"yes\">Oui</div>";
			else $folder5 = "<div class=\"no\">Non</div>";			
			if(getFilePermission("../Heroon Engine/Data Project/chat.txt") == "0666" || getFilePermission("../Heroon Engine/Data Project/chat.txt") == "0777") $folder6 = "<div class=\"yes\">Oui</div>";
			else $folder6 = "<div class=\"no\">Non</div>";							
			if(getFilePermission("../Heroon Engine/Data Project/selected.dat") == "0666" || getFilePermission("../Heroon Engine/Data Project/selected.dat") == "0777") $folder8 = "<div class=\"yes\">Oui</div>";
			else $folder8 = "<div class=\"no\">Non</div>";
			?>				
			<b style="font-size:12px"><?php echo $lang['Folders'];?>:</b><br />
				&nbsp; - <label class="config">Logs/</label><?php echo $folder1;?><br />
				&nbsp; - <label class="config">Heroon Engine/_Projects/</label><?php echo $folder2;?><br />
				&nbsp; - <label class="config">Heroon Engine/Logs/</label><?php echo $folder3;?><br />
				&nbsp; - <label class="config">Heroon Engine/Scripts/</label><?php echo $folder4;?><br />
				&nbsp; - <label class="config">Heroon Engine/Temp/</label><?php echo $folder5;?><br />
			<br />
			<b style="font-size:12px"><?php echo $lang['Files'];?>:</b><br />
				&nbsp; - <label class="config">Heroon Engine/Data Project/chat.txt</label><?php echo $folder6;?><br />				
				&nbsp; - <label class="config">Heroon Engine/Data Project/selected.dat</label><?php echo $folder8;?><br />			
			<br />
			<br /><center><input type="button" value="<<" id="previous2" class="button-control"> &nbsp; <form method="POST" style="display:inline-block;"><input type="submit" value=">>" id="next3" name="next3" class="button-control"></form></center>
		<br /><br />
		</div>
		<div id="admin">			
			<h1><?php echo $lang['Administrator'];?></h1>			
			<h2><?php echo $lang['infoAdmin'];?></h2>
			<?php			
			if(@$_POST['next3']) {
				$chaine = 'azertyuiopqsdfghjklmwxcvbn0123456789';
				$compteExist = $lang['compteExist'];
				$enterMail = $lang['enterMail'];
				$recieveMail = $lang['revieveMail'];				
				chaine_aleatoire(8, $chaine, $compteExist, $enterMail, $recieveMail);				
			}
			?>
			<p><?php echo $lang['etapeNext'];?></p><br />		
			<br /><center><input type="button" value="<<" id="previous3" class="button-control"> &nbsp; <input type="button" value=">>" id="next4" class="button-control"></center>
		</div>
		<div id="finalisation">
			<h1><?php echo $lang['FinInstal'];?></h1>
			<h2><?php echo $lang['FinalInstall'];?></h2>	
			<?php echo join('', $lang['TextFinalInstall']);?>			
			<br /><center><input type="button" value="<<" id="previous4" class="button-control"> &nbsp; <input type="button" value=">>" id="next5" class="button-control"></center>
		</div>
	</div>
	<span style="float:right;margin-top:10px;margin-right:10px;"><?php echo $lang['propulseBy'];?><span class="titre-small" title="HerronEngine">herron engine</span></span>
	<script>
	$("#next1,#next2,#next3,#next4,#next5").click(function() {
		current_etape += 1;			
		if(current_etape == 1) {
			$("#licence, #configuration, #finalisation, #admin").hide();
			$("#preambule").show();
			$("#li-preambule").css({"background":"green"});
			$("#progress-install").val(0);
		}
		else if(current_etape == 2) {
			$("#preambule, #configuration, #finalisation, #admin").hide();
			$("#licence").show();
			$("#li-licence").css({"background":"green"});
			$("#progress-install").val(25);				
		}
		else if(current_etape == 3) {
			if($("#accepteLicence").is(":checked")) {
				$("#preambule, #licence, #finalisation, #admin").hide();
				$("#configuration").show();
				$("#li-configuration").css({"background":"green"});
				$("#progress-install").val(50);
			} else {
				current_etape -= 1;	
				jAlert(lang.js.acceptLicenceForNext, "Attention");
			}
		}
		else if(current_etape == 4) {
			$("#preambule, #licence, #configuration, #finalisation").hide();
			$("#admin").show();
			$("#li-admin").css({"background":"green"});
			$("#progress-install").val(75);
		}
		else if(current_etape == 5) {
			$("#preambule, #licence, #configuration, #admin").hide();
			$("#finalisation").show();
			$("#li-finalisation").css({"background":"green"});
			$("#progress-install").val(100);
		}
		else if(current_etape == 6) {				
			window.location.href = "../index.php";
		}
	});
	$("#previous1,#previous2,#previous3,#previous4").click(function() {
		current_etape -= 1;
		if(current_etape == 1) {
			$("#licence, #configuration, #finalisation, #admin").hide();
			$("#preambule").show();
			$("#progress-install").val(0);
		}
		else if(current_etape == 2) {
			$("#preambule, #configuration, #finalisation, #admin").hide();
			$("#licence").show();
			$("#progress-install").val(25);
		}
		else if(current_etape == 3) {
			$("#preambule, #licence, #finalisation, #admin").hide();
			$("#configuration").show();
			$("#progress-install").val(50);
		}
		else if(current_etape == 4) {
			$("#preambule, #licence, #finalisation, #configuration").hide();
			$("#admin").show();
			$("#progress-install").val(75);
		}
		else if(current_etape == 5) {
			$("#preambule, #licence, #configuration, #admin").hide();
			$("#finalisation").show();
			$("#progress-install").val(100);
		}
	});	
	var formatStateLang = function(opt) {
		if (!opt.id) { return opt.text; }
		var optimage = $(opt.element).data('image');
		if(!optimage){ return opt.text; }
		else { return $opt = $('<span><img src="' + optimage + '" width="23px" /> ' + opt.text + '</span>'); }
	};
	$("#HE-LanguageDefaut").select2({
		templateResult: formatStateLang,
		templateSelection: formatStateLang,
		selectOnClose: true			
	}).change(function() {			
		$.ajax({type: "POST", url: '../Heroon Engine/PHP/save.php', data: "file=editor&type=null&element=lang&value="+$("#HE-LanguageDefaut").val()+"&root=../Heroon Engine/Data Project/",
			success: function (msg) {
				setCookie("HE-Lang", $("#HE-LanguageDefaut").val(), 365);
				location.reload();				
			}
		});			
	});
	$.getJSON("../Heroon Engine/Data Project/editor.json", function(data) {
		$("#HE-LanguageDefaut option").filter('[value='+String(data.lang)+']').attr('selected', true);
		if(String(data.lang) == "French") {
			$("#select2-HE-LanguageDefaut-container").attr("title", String(data.lang)).find("span").html('<img src="Lang/images/French.png" width="23px"> '+ String(data.lang));
		}
		if(String(data.lang) == "English") {
			$("#select2-HE-LanguageDefaut-container").attr("title", String(data.lang)).find("span").html('<img src="Lang/images/English.png" width="23px"> '+ String(data.lang));		
		}
	});
	</script>
	<?php
	if(@$_POST['next3']) {				
		echo '<script>
		current_etape = 4;				
		$(function() {
			$("#licence, #configuration, #preambule, #finalisation").hide();					
			$("#li-preambule").css({"background":"green"});
			$("#li-licence").css({"background":"green"});
			$("#li-configuration").css({"background":"green"});	
			$("#li-admin").css({"background":"green"});
			$("#progress-install").val(75);
			$("#admin").show();
		});
		</script>';	
	}
	$_SESSION['user'] = false;
	?>
</body>
</html>